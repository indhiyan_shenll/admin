<?php
$parent_directory=basename(dirname($_SERVER["PHP_SELF"]));
$filename=basename($_SERVER["PHP_SELF"]);
include("../includes/configure.php");
include("includes/cmm_functions.php");
include("../includes/session_check.php");
$id=$_SESSION['user_id'];
$user_name=$_SESSION['user_name'];
$personsql="select * from  tbl_sales_person where salesperson='$user_name'";
$getpersonRes=mysql_query($personsql);
$getsalepersonCnt=mysql_num_rows($getpersonRes);
$managersql="select * from   tbl_salesmanager  where sales_manager='$user_name'";
$getmanagerRes=mysql_query($managersql);
$getmanagerCnt=mysql_num_rows($getmanagerRes);
$feat="trail_form";
$typeandfeature=checklogin($id,$feat);
$usrArr=explode("*",$typeandfeature);
$user_type=$usrArr[0];
$feature=$usrArr[1];
$dowfeature=$usrArr[2];
$paymentf=$usrArr[3];
if($feature!="yes"&&$user_type!="admin"){
	header("Location:noauthorised.php");
	exit;
}
else{
	if($getsalepersonCnt>0||$getmanagerCnt>0||$user_type=="admin")
	{
	$prospect_id=$_GET["prospect_id"];
	if($prospect_id!=""){
		$getprospectQry="select * from tbl_prospecting_list where prospect_id='".$prospect_id."'";
		$getprospectRes=mysql_query($getprospectQry);
		$getprospectRow=mysql_fetch_array($getprospectRes);
		$business_name=stripslashes($getprospectRow["business_name"]);
		$street_address=stripslashes($getprospectRow["street_address"]);
		$street_address2=stripslashes($getprospectRow["street_address2"]);
		$suburb=stripslashes($getprospectRow["suburb"]);
		$state=stripslashes($getprospectRow["state"]);
		$postcode=stripslashes($getprospectRow["postcode"]);
		$country=stripslashes($getprospectRow["country"]);
		$full_address=stripslashes($getprospectRow["full_address"]);
		$phone=stripslashes($getprospectRow["phone"]);
		$mobile=stripslashes($getprospectRow["mobile"]);
		$email=stripslashes($getprospectRow["email"]);
		$import_date=date('d-m-Y',strtotime(stripslashes($getprospectRow["import_date"])));
		$sales_person=stripslashes($getprospectRow["sales_person"]);
		$sales_manager=stripslashes($getprospectRow["sales_manager"]);
		$callstatus=stripslashes($getprospectRow["callstatus"]);
		$last_contact=stripslashes($getprospectRow["last_contact"]);
		$mode="Edit";
		$value="Update";
	}
	else{
		$mode="Add";
		$value="Create";
	}
	if(isset($_POST["business_name"])){
		$business_name=addslashes(trim($_POST["business_name"]));
		$street_address=addslashes(trim($_POST["street_address"]));
		$street_address2=addslashes(trim($_POST["street_address2"]));
		$suburb=addslashes(trim($_POST["suburb"]));
		$state=addslashes(trim($_POST["state"]));
		$postcode=addslashes(trim($_POST["postcode"]));
		$country=addslashes(trim($_POST["country"]));
		$full_address=addslashes(trim($_POST["full_address"]));
		$phone=addslashes(trim($_POST["phone"]));
		$mobile=addslashes(trim($_POST["mobile"]));
		$email=addslashes(trim($_POST["email"]));
		$import_date=date('Y-m-d',strtotime(addslashes(trim($_POST["import_date"]))));
		$salesperson=addslashes(trim($_POST["salesperson"]));
		$salesmanager=addslashes(trim($_POST["salesmanager"]));
		$callstatus=addslashes(trim($_POST["callstatus"]));
		$last_contact=addslashes(trim($_POST["last_contact"]));
		if($mode=="Edit"){
			$updateQry="update tbl_prospecting_list set business_name='".$business_name."',street_address='".$street_address."',street_address2='".$street_address2."',suburb='".$suburb."',state='".$state."',postcode='".$postcode."',country='".$country."',full_address='".$full_address."',phone='".$phone."',mobile='".$mobile."',email='".$email."',import_date='".$import_date."',sales_person='".$salesperson."',sales_manager='".$salesmanager."',callstatus='".$callstatus."',last_contact='".$last_contact."',modified_date=now() where prospect_id='".$prospect_id."'";
			$trialqry=mysql_query($updateQry);
			
		}
		else{
			echo $insertQry="insert into tbl_prospecting_list(business_name,street_address,street_address2,suburb,state,postcode,country,full_address,phone,mobile,email,import_date,sales_person,sales_manager,callstatus,last_contact,added_date,modified_date) values('".$business_name."','".$street_address."','".$street_address2."','".$suburb."','".$state."','".$postcode."','".$country."','".$full_address."','".$phone."','".$mobile."','".$email."','".$import_date."','".$salesperson."','".$salesmanager."','".$callstatus."','".$last_contact."',now(),now())";
			exit;
			$trialqry=mysql_query($insertQry);
		}
		if($trialqry)
		{ 
			header("Location:admin_features.php");
			exit;
		}
	}
?>
	<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
	<html>
		<head>
			<title>MAR Pipeline System</title>
			<link rel="shortcut icon" href="images/Fav.ico" type="image/ico">
			<meta name="Generator" content="EditPlus">
			<meta name="Author" content="">
			<meta name="Keywords" content="">
			<meta name="Description" content="">
			<script type="text/javascript" src="js/jquery.js"></script>
			<script>
			var mode="<?php echo $mode;?>";
			</script>
			<script type="text/javascript" src="js/validate.js"></script>
			<style>
				body{
					margin:0;
					color:#D9D9D9;
					background:#455A68;
					font-family:arial;
				}
				.header{
					height:70px;
					background:#1C242A;
				}
				.content{
					background:#455A68;
					min-height:600px;
				}
				
				.form_actions{
					padding-top:15px;
					/*padding-left:5px;*/
					padding-bottom:30px;
				}
				.form_actions .add_btn{
					cursor:pointer;
					border-radius:0px;
					background:#0D0D0D;
					color:#D9D9D9;
					border-color:#D9D9D9;
					padding:5px 15px 5px 15px;
				}
				.list_content{
					width:950px;
					margin-left:40px;
					/*margin-left:auto;
					margin-right:auto;*/
				}
				.tbl_header th{
					font-size:13px;
					border-bottom:1px solid #D9D9D9;
					text-align:left;
				}
				.tbl-body{
					font-size:12px;
				}
				a{
					color:black;
				}
				.inp_feild{
					border-radius:2px;
					border:none;
					width:100%;
				}
			</style>
			<link href="css/slate.css" rel="stylesheet"> 
		    <link href="css/ui-lightness/jquery-ui-1.8.21.custom.css" rel="stylesheet">
			<script src="js/jquery-1.7.2.min.js"></script>
			<script src="js/jquery-ui-1.8.21.custom.min.js"></script>
			 <script src="js/demo/demo.calendar.js"></script>
		</head>
		<body>
			<div>
				<div style="margin-left:auto;margin-right:auto;">
					
					<div class="header">
						<span style="float:right;margin-right:20px;margin-top:5px;"><a href="admin/logout.php" style="color:white;text-decoration:none;">Logout</a></span>
						<div style="width:990px;height:80px;">
							 <div style="float:left;width:55%;">
								<img src="images/myappyrestaurants.png" style="margin-top:10px;margin-left:40px;">
								
							 </div>
							
						</div> 
					</div>
					<div class="content">
						<div class="list_content">
							<h1 style="font-size:25px;padding-top:15px;padding-bottom:15px;margin:0px;">Filter Prospecting List</h1>
							<form name="prospect_form" id="prospect_form" method="post" action="prospectinglist.php">
							<table cellspacing="15" cellpadding="0" border="0" width="75%">
								
								<tr>
								<td style="width:168px;">
									Import Date:
								</td>
								<td>
									<table cellspacing="0" cellpadding="0" width="100%">
										<tr>
											<td width="50%">
												<input type="text" name="import_date1" id="import_date1" class="inp_feild" style="width:97%;" value="<?php echo $res3_state;?>">
											</td>
											<td align="right" width="50%">
												<input type="text" name="import_date2" id="import_date2" class="inp_feild" style="width:97%;" value="<?php echo $res3_postcode;?>">
											</td>
										</tr>
									</table>
								</td>
								</tr>
								<tr>
									<td valign="top">
										Salesperson:
									</td>
									<td>
										
										<select name="salesperson[]" id="salesperson" ondblclick="clearSelected();" class="inp_feild" style="width:50%;" multiple>
										<?php
											$getmQry="select * from  tbl_sales_person order by sales_person_id asc";
											$getmRes=mysql_query($getmQry);
											while($getmRow=mysql_fetch_array($getmRes)){
											?>
											<option value="<?php echo $getmRow["salesperson"]?>"><?php echo $getmRow["salesperson"]?></option>
										<?php
											}
										?>
										
									</select>
									</td>
								</tr>
								<script>
                                       function clearSelected(){
										
                                     var elements = document.getElementById("salesperson").options;
											for(var i = 0; i < elements.length; i++){
															   if(elements[i].selected)
																elements[i].selected = false;
															}
  }
										</script>
								<tr>
									<td valign="top">
										Sales Manager:
									</td>
									<td>
										
										<select name="salesmanager[]" id="salesmanager" ondblclick="salesmanagerSelected();" class="inp_feild" style="width:50%;" multiple>
										<?php
											$getmQry="select * from  tbl_salesmanager order by sales_manager_id asc";
											$getmRes=mysql_query($getmQry);
											while($getmRow=mysql_fetch_array($getmRes)){
											?>
											<option value="<?php echo $getmRow["sales_manager"]?>"><?php echo $getmRow["sales_manager"]?></option>
										<?php
											}
										?>
										
									</select>
									</td>
								</tr>
								 <script>
                                       function salesmanagerSelected(){
										
                                     var elements = document.getElementById("salesmanager").options;
											for(var i = 0; i < elements.length; i++){
															   if(elements[i].selected)
																elements[i].selected = false;
															}
  }
										</script>
								<tr>
									<td valign="top">
										Callstatus:
									</td>
									<td>
										
										<select name="callstatus[]" id="callstatus"  ondblclick="statuselected();" class="inp_feild" style="width:50%;" multiple>
										<?php
											$getmQry="select * from  tbl_callstatus where callstatus!='0' order by callstatus_id asc";
											$getmRes=mysql_query($getmQry);
											while($getmRow=mysql_fetch_array($getmRes)){
											?>
											<option value="<?php echo $getmRow["callstatus"]?>"><?php echo $getmRow["callstatus"]?></option>
										<?php
											}
										?>
										
									</select>
									</td>
								</tr>
								<tr>
								 <script>
                                       function statuselected(){
										
                                     var elements = document.getElementById("callstatus").options;
											for(var i = 0; i < elements.length; i++){
															   if(elements[i].selected)
																elements[i].selected = false;
															}
  }
										</script>
									<td>
										Suburb:
									</td>
									<td>
										<select name="suburb" id="suburb" class="inp_feild" style="width:50%;">
										<option value="0">Select</option>
										<?php
											$getmQry="select * from  tbl_prospecting_list where suburb!=''";
											$getmRes=mysql_query($getmQry);
											$getSubArr=array();
											$m=0;
											while($getmRow=mysql_fetch_array($getmRes)){
												$getSubArr[]=$getmRow["suburb"];
												$m++;
											}
											
											$getSubArr1=array_unique($getSubArr);
											foreach($getSubArr1 as $value){
											?>
											<option value="<?php echo $value;?>"><?php echo $value;?></option>
										<?php
											}
											
										?>
										
									</select>
									
									</td>
								</tr>
								<tr>
									<td>
										State:
									</td>
									<td>
										<select name="state" id="state" class="inp_feild" style="width:50%;">
										<option value="0">Select</option>
										<?php
											$getmQry="select * from  tbl_prospecting_list where state!=''";
											$getmRes=mysql_query($getmQry);
											$getSubArr=array();
											while($getmRow=mysql_fetch_array($getmRes)){
												$getSubArr[]=$getmRow["state"];
											}
											
											$getSubArr1=array_unique($getSubArr);
											foreach($getSubArr1 as $value){
											?>
											<option value="<?php echo $value;?>"><?php echo $value;?></option>
										<?php
											}
											
										?>
										
									</select>
									</td>
								</tr>
								<tr>
									<td>
										Last Contact:
									</td>
									<td>
									<table cellspacing="0" cellpadding="0" width="100%">
										<tr>
											<td width="50%">
												<input type="text" name="last_contact1" id="last_contact1" class="inp_feild" style="width:97%;" value="<?php echo $res3_state;?>">
											</td>
											<td align="right" width="50%">
												<input type="text" name="last_contact2" id="last_contact2" class="inp_feild" style="width:97%;" value="<?php echo $res3_postcode;?>">
											</td>
										</tr>
									</table>
								</td>
								</tr>
																					
								<tr>
									<td colspan="2">
										<table cellspacing="0" cellpadding="0" width="100%">
											<tr>
												<td>
													<div class="form_actions">
														<input type="button" value="Back to Prospecting List" onclick="document.location='prospectinglist.php'" class="add_btn">
													</div>
												</td>
												<td align="center">
													<div class="form_actions">
														<input type="reset" value="Clear all filters" class="add_btn">
													</div>
												</td>
												<td>
													<div class="form_actions" style="text-align:right;">
														<input type="button" value="Filter Prospecting List" class="add_btn" id="filter_prospect">
													</div>
												</td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
							</form>
						</div>
						
					</div>
				</div>
			</div>
		</body>
	</html>
<?php
}
else{
	header("Location:noauthorised.php");
	exit;
}
}
?>