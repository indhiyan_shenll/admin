<?php
include("../includes/configure.php");
include("../includes/session_check.php");

set_time_limit (0);
$sql = "SHOW TABLES FROM ".DATABASE;
$result = $DBCONN->prepare($sql);
$result->execute();
while($tblnms=$result->fetch()){
	$table_name[] = $tblnms[0];
}
function backup_tables($host,$user,$pass,$name,$tables)
{
    $link = mysql_connect($host,$user,$pass);
    mysql_select_db($name,$link);
    $return = "";
    // Get all of the tables
    if($tables == '*') {
        $tables = array();
        $result = mysql_query('SHOW TABLES');
        while($row = mysql_fetch_row($result)) {
            $tables[] = $row[0];
        }
    } else {
        if (is_array($tables)) {
            $tables = explode(',', $tables);
        }
    }
 
    // Cycle through each provided table
    foreach($tables as $table) {
        $result = mysql_query('SELECT * FROM '.$table);
        $num_fields = mysql_num_fields($result);
 
        // First part of the output � remove the table
       // $return .= 'DROP TABLE ' . $table . ';<|||||||>';
 
        // Second part of the output � create table
        $row2 = mysql_fetch_row(mysql_query('SHOW CREATE TABLE '.$table));
        $return .= "\n\n" . $row2[1] . ";\n\n";
 
        // Third part of the output � insert values into new table
        for ($i = 0; $i < $num_fields; $i++) {
            while($row = mysql_fetch_row($result)) {
                $return.= 'INSERT INTO '.$table.' VALUES(';
                for($j=0; $j<$num_fields; $j++) {
                    $row[$j] = addslashes($row[$j]);
                    $row[$j] = str_replace("\n","\\n",$row[$j]);
                    if (isset($row[$j])) {
                        $return .= '"' . $row[$j] . '"';
                    } else {
                        $return .= '""';
                    }
                    if ($j<($num_fields-1)) {
                        $return.= ',';
                    }
                }
                $return.= ");\n";
            }
        }
        $return.="\n\n\n";
    }
 
    // Generate the filename for the sql file
    $filess ='dbdumps/dbbackup_' . date('dmY').'_'.time() . '.sql';
	$zipfilenm='dbbackup_' . date('dmY').'_'.time() . '.zip'; 
    // Save the sql file
    $handle = fopen($filess,'w+');
    fwrite($handle,$return);
    fclose($handle);
	
	if(extension_loaded('zip'))
	{ 
		// Checking ZIP extension is available
		$zip = new ZipArchive(); // Load zip library 
		$zip_name = 'dbdumps/'.$zipfilenm;
	if($zip->open($zip_name, ZIPARCHIVE::CREATE)!==TRUE)
	{ 
	 // Opening zip file to load files
		$error .= "* Sorry ZIP creation failed at this time";
	}
	
		$zip->addFile($filess); // Adding files into zip

		$zip->close();
	if(file_exists($zip_name))
	{
		// push to download the zip
		header('Content-type: application/zip');
		header('Content-Disposition: attachment; filename="'.$zip_name.'"');
		readfile($zip_name);
		// remove zip file is exists in temp path
		unlink($zip_name);
	}
	}
	else
		$error .= "* You dont have ZIP extension";

    mysql_close();
}
backup_tables(DB_HOST_NAME,USERNAME,PASSWORD,DATABASE,"*");
?>
