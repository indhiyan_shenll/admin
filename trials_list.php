<?php
include("../includes/configure.php");
include("../includes/session_check.php");
$trial_id=$_GET["id"];
if($trial_id!=""){
	$deleteQry="delete from tbl_free_trials where trial_id=:trial_id";
	$prepdeleteQry=$DBCONN->prepare($deleteQry);
	$deleteRes=$prepdeleteQry->execute(array(":trial_id"=>$trial_id));
	if($deleteRes){
		header("Location:trials_list.php");
		exit;
	}
}
if(isset($_POST['HdnPage']) && $_POST['HdnPage']!="" && $_POST['HdnPage']!="0")
	$Page=$_POST['HdnPage'];
else
	$Page=1;
 
$themes_array =array(
					 '4'=>'Verve Circle',
					 '1'=>'Verve Square',
					 '5'=>'Metro Circle',
                     '2'=>'Metro Square',
					 '3'=>'Solstice Square',
					 '6'=>'Solstice Circle'
					 );
 include("includes/header.php");
?>
 <body>
		<div>
			<div style="margin-left:auto;margin-right:auto;">
				 
				<div class="content">
					<div class="list_content">
						<div class="form_actions" style="padding-bottom:45px;">
							<input type="button" value="Back To Admin Features" class="add_btn" onclick="document.location='admin_features.php'" style="float:left;">
						</div>
						 <form name="demo_list" method="post">
							<input type="hidden" name="HiddenMode" id="HiddenMode" value="">
							<input type="hidden" name="HdnPage" id="HdnPage" value="">
							<div class="header_div">
								<table cellspacing="0" cellpadding="0" width="100%" class="tbl_header" border="0">
								  <tr>
										
										<th width="10%">Trial Date</th>
										<th width="15%" >business Name&nbsp;&nbsp;</th>
										<th width="15%">Logo</th>
										<th width="15%">Business Type&nbsp;&nbsp;</th>
										<th width="15%" >Chosen Theme &nbsp;&nbsp;</th> 
										<th width="20%">Email Address&nbsp;&nbsp;</th> 
										<th width="10%">Delete?</th>
									
								 </tr>
								</table>
						</div>
						<div class="gap" ></div> 
						<table cellspacing="0" cellpadding="0" width="100%" class="tbl-body">
                         <?php
					        $qryCondition="";
							$getQry="select * from tbl_free_trials where demo_status='0' order by modified_date desc";
							$prepgetQry=$DBCONN->prepare($getQry);
							$prepgetQry->execute();
							$count =$prepgetQry->rowCount();
							if($count>0){
								$records_perpage=25;
								$TotalRecords	=$count;
								if($TotalRecords <= (($Page * $records_perpage)-$records_perpage))
								$Page	=	$Page-1;
								$TotalPages		=	ceil($TotalRecords/$records_perpage);
								$Start			=	($Page-1)*$records_perpage;
								$getQry.=" limit $Start,$records_perpage";
								$prepgetQry=$DBCONN->prepare($getQry);
								$prepgetQry->execute();
								$count =$prepgetQry->rowCount();
								$sno=$Start+1;
                              if($count>0){
							  $rowno=1;
				               while($getRow=$prepgetQry->fetch()){
                                    if($rowno%2==1){
										$bgcolor="#a5a5a5";
									}
									else{
										$bgcolor="#d2d1d1";
									}
									$image_src="http://myappybusiness.com/uploaded_special_logo/".$getRow["logo"];
									$key=$getRow["theme"];
									$trial_date=date('d/m/Y',strtotime($getRow["created_date"]));
									
                                     
						?>
 					   <tr bgcolor="<?php echo $bgcolor;?>">
									<td  width="10%"><?php echo $trial_date;?></td>
									<td width="15%"><?php echo stripslashes($getRow["business_name"]);?></td> 
									<td width="15%"><?php echo "<img src=\"$image_src\" style=\"height:15px;width:15px;\">";?></td> 
									<td width="15%" ><?php echo stripslashes($getRow["business_type"]);?></td> 
									<td width="15%"> <?php echo $themes_array[$key];?></td> 
									<td width="20%"><?php echo stripslashes($getRow["email"]);?></td> 
									<td width="10%"><a href="edit_trial.php?id=<?php echo $getRow["trial_id"];?>">Edit</a>&nbsp;&nbsp;/&nbsp;&nbsp;<a href="trials_list.php?id=<?php echo $getRow["trial_id"];?>" onclick="return confirm('Are you sure want to delete this trial? ')">Delete</a></td>
									 
															
							</tr>
							<?php
							$rowno++;
							$sno++;
								}
							if($TotalPages > 1){

									echo "<tr><td align='center' colspan='7' valign='middle' class='pagination'>";

									if($TotalPages>1){

											$FormName = "demo_list";
									       include("../includes/paging.php");
									 
									}

									echo "</td></tr>";

									  }
								}
								else{
									echo "<tr style=\"background-color:#f6f6f6;text-align:center;\"><td colspan=\"7\">No Free List found.</td></tr>";
								}

							
							}
							else{
								echo "<tr style=\"background-color:#f6f6f6;text-align:center;\"><td colspan=\"7\">No Free List found.</td></tr>";
							}
						?>
						<tr>
								<td colspan="7">
								<div class="form_actions" style="text-align:left;position:relative;">
								<input type="button" value="Back To Admin Features" class="add_btn" onclick="document.location='admin_features.php'">
								</td>
							</tr>
						</table>
						</form>
					</div>
				</div>
			</div>
		</div>
<?php
include("includes/footer.php");
?>
<script type="text/javascript">
/****function for paging statrs*******/
function pagetransfer(pagenumber,formname)
{	
	with(document.forms[formname])
	{ 
		HdnPage.value=pagenumber;
		HiddenMode.value="paging";
		submit();
	}
}
/****function for paging ends*******/
</script>
