<?php
include("../includes/configure.php");
include("includes/cmm_functions.php");
include("../includes/session_check.php");

$userID = (int) $_SESSION['user_id'];
if( $userID != 3 && $userID != 173){
	header("Location:noauthorised.php");
	exit;			 
}

$user_id=$_GET["user_id"];
if($user_id!=""){
	$deleteQry="delete from  tbl_users where user_id='".$user_id."'";
	$deleteRes=mysql_query($deleteQry);
	if($deleteRes){
		header("Location:userslist.php");
		exit;
	}
}

$getusrQry="select * from  tbl_users".$order;
$getusrRes=mysql_query($getusrQry);
$getusrCnt=mysql_num_rows($getusrRes);
include("includes/header.php");
?>
<body>
		<div>
			<div style="margin-left:auto;margin-right:auto;">
				
				<div class="content">
					<div class="list_content">
						<div class="form_actions" style="width:1038px;padding-bottom:45px;">
							<div style="height:40px;">
								<input type="button" value="Back To Admin Features" class="add_btn" onclick="document.location='admin_features.php'" style="float:left;">
								<input type="button" value="Add User" class="add_btn" onclick="document.location='edit_user.php'" style="float:right;">
							</div>
							<div style="width:143px;float:right;text-align:right;padding-top:10px;">
								<span style="font-size:12px;font-weight:bolder;color:white;" id="demo_count">Users Count:&nbsp;<?php echo $getusrCnt;?></span>
							</div><br>
						</div>
						<div class="gap1">
							<table cellspacing="0" cellpadding="0" width="110%" class="tbl_header" style="border-radius: 80px;">
								
								<tr>
									<th width="6%" align="left" style="border-top-left-radius: 10px;border-bottom-left-radius: 10px;">&nbsp;No</th>
									<th width="10%" align="left">Username</th>
									<th width="10%" align="left">Password</th>
									<th width="10%" align="left">User Type</th>
									<th width="11%">Prospect/Trial</th>
									<th width="11%">Demo Page</th>
									<th width="12%">Master List</th>
									<th width="12%">Download Lists</th>
									<th width="12%">Payment Info</th>
									<th width="10%" align="left" style="border-top-right-radius: 10px;border-bottom-right-radius: 10px;">Delete?</th>
								</tr>
							</table>
						</div>
						<table cellspacing="0" cellpadding="0" width="110%" class="tbl-body">
						<?php
							
							if($getusrCnt>0){
								$i=1;
								while($getusrRow=mysql_fetch_array($getusrRes)){
									if($getusrRow["trail_form"]=="yes"){
										$trail_form="<img src=\"images/success.png\">";
									}
									else{
										$trail_form="";
									}
									if($getusrRow["demo_page"]=="yes"){
										$demo_page="<img src=\"images/success.png\">";
									}
									else{
										$demo_page="";
									}
									if($getusrRow["master_list"]=="yes"){
										$master_list="<img src=\"images/success.png\">";
									}
									else{
										$master_list="";
									}
									if($getusrRow["download_list"]=="yes"){
										$download_list="<img src=\"images/success.png\">";
									}
									else{
										$download_list="";
									}
									if($getusrRow["payment_info"]=="yes"){
										$payment_info="<img src=\"images/success.png\">";
									}
									else{
										$payment_info="";
									}
									if($i%2==1){
										$bgcolor="#a5a5a5";
									}
									else{
										$bgcolor="#d2d1d1";
									}
						?>
							<tr bgcolor="<?php echo $bgcolor;?>">
								<td width="6%"><?php echo $i;?></td>
								<td width="10%"><?php echo stripslashes($getusrRow["username"]);?></td>
								<td width="10%"><?php echo stripslashes($getusrRow["password"]);?></td>
								<td width="6%"><?php echo ucwords(stripslashes($getusrRow["user_type"]));?></td>
								<td width="11%" align="center"><?php echo $trail_form;?></td>
								<td width="11%" align="center"><?php echo $demo_page;?></td>
								<td width="12%" align="center"><?php echo $master_list;?></td>
								<td width="12%" align="center"><?php echo $download_list;?></td>
								<td width="12%" align="center"><?php echo $payment_info;?></td>
								<td width="10%"><a href="edit_user.php?user_id=<?php echo $getusrRow["user_id"];?>">Edit</a>&nbsp;&nbsp;/&nbsp;&nbsp;<a href="userslist.php?user_id=<?php echo $getusrRow["user_id"];?>" onclick="return confirm('Are you sure want to delete this user?')">Delete</a></td>
							</tr>
							
						<?php
							$i++;
								}
								?>
							<tr><td height="10px" colspan="10"></td></tr>
							
								<?php
							}
							else{
								echo "<tr bgcolor='#a5a5a5'><td colspan=\"10\"><center>No user(s) found.</center></td></tr>";
							}
						?>
						<tr>
								<td colspan="10">
								<div class="form_actions" style="text-align:left;position:relative;">
								<input type="button" value="Back To Admin Features" class="add_btn" onclick="document.location='admin_features.php'">
								</td>
							</tr>
						</table>
					</div>
				</div>
			</div>
		</div>
	<?php
	include("includes/footer.php");
	?>