<?php
include("../includes/configure.php");
//include("../includes/session_check.php");
$message='';
$demoId=$_GET["demo_id"];
$msg=$_GET["msg"];
$mode=$_GET["mode"];
$getDemoQry="select * from tbl_demo where demo_id='".$demoId."'";
$getDemoRes=mysql_query($getDemoQry);
$getDemoRow=mysql_fetch_array($getDemoRes);
$business_name=stripslashes($getDemoRow["business_name"]);
$first_name=stripslashes($getDemoRow["first_name"]);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>My Appy Restaurant</title>
	<link rel="shortcut icon" href="images/Fav.ico" type="image/ico">
	<link href="stylesheet.css" rel="stylesheet" type="text/css" />
	<style>
		.card_class{
			padding-right:10px;
			cursor:pointer;
		}
		.copyright {
			font-family: 'arial';
			color: grey;
			font-size: 11px;
			text-align: right;
			width: 790px;
			width:400px;
			margin-left:auto;
			
		}
	</style>
</head>

<body style="background:url('images/Picture1.png') no-repeat;margin:0px;font-family:'kristen_itcregular';color:#8eb4e3;background-size:100% 87%;">
	<div style="min-height:130px;">
		<div style="width:54.5%;float:left;">
			<div style="width:500px;float:right;">
				<p style="font-size:32px;width:330px;margin-right:200px;transform:rotate(-3deg);-ms-transform:rotate(-3deg);-webkit-transform:rotate(-3deg);margin:0px;margin-top:10px;line-height:40px;">hi <?php echo strtolower($first_name);?>, <br>your customized app is ready to <span style="color:#ffc000;">go live!</span></p>
			</div>
		</div>
		<div style="width:45%;float:right;font-family:'Calibri';padding-top:40px;">
			<?php if($msg=="success"){?>
			<div style="margin-top:20px;">
			<table>
				<tr style="background:white;height:40px;"id="error_message">
					<td style="color:green;font-size:20px;font-family:arial;margin-left:10px;" colspan="2"><?php echo "Your payment has been processed successfully.";?></td>
				</tr>
			</table>
			</div>
			<?php } ?>
			<table cellspacing="0" cellpadding="0" style="border:3px solid #ffc000;border-radius:25px;width:420px;padding-left:15px;padding-top:10px;padding-bottom:10px;font-size:15px;">
				<tr>
					<td>Owner Name:</td>
					<td><?php echo $first_name." ".stripslashes($getDemoRow["sur_name"]);?></td>
				</tr>
				<tr>
					<td>Restaurant Name:</td>
					<td><?php echo $business_name;?></td>
				</tr>
				<tr>
					<td>App Name:</td>
					<td><?php echo stripslashes($getDemoRow["app_name"]);?></td>
				</tr>
				<tr>
					<td>iTunes Category:</td>
					<td><?php if($getDemoRow["itunes_category"]!='0'&& $getDemoRow["itunes_category"]!="") { echo stripslashes($getDemoRow["itunes_category"]); }?></td>
				</tr>
				<tr>
					<td>Status:</td>
					<td>Awaiting activation...</td>
				</tr>
			</table>
		</div>
	</div>
	<div style="min-height:500px;">
		<div style="width:54.5%;float:left;padding-top:10px;">
			<div style="width:200px;float:right;margin-top:290px;">
				<?php if($getDemoRow["app_io_link"]!="") { ?>
				<img src="images/arrow_blue.png" style="width:50px;"><br>
				<div style="transform:rotate(-3deg);-ms-transform:rotate(-3deg);-webkit-transform:rotate(-3deg);font-size:18px;color:white;line-height:21px;">click here if you<br>wanna play with me</div>
				<?php } ?>
			</div>
			<div style="width:316px;float:right;">
				<?php if($getDemoRow["app_io_link"]!="") { ?><a href="<?php echo stripslashes($getDemoRow["app_io_link"]);?>" target="_blank"><iframe allowTransparency='true' class='embed' frameborder='0' height='620px' scrolling='no' src='<?php echo stripslashes($getDemoRow["app_io_link"]);?>?orientation=portrait&amp;device=iphone5&amp;chrome=true' width='291px'></iframe></a><?php } ?>
			</div>
			
		</div>
		<div style="width:45%;float:right;padding-top:70px;">
			<div style="width:337px;font-size:18px;color:#ffc000;text-align:center;">
				<p style="width:300px;margin-left:auto;margin-right:auto;padding-bottom:30px;">to activate your customized app simply click on your preferred payment method below</p>
			</div>
			<div>
				<span class="card_class"><img src="images/visa.png" onclick="document.location='secondstep.php?demo_id=<?php echo $demoId;?>&cardtype=visa'"></span><span class="card_class" onclick="document.location='secondstep.php?demo_id=<?php echo $demoId;?>&cardtype=mastercard'"><img src="images/master.png"></span><span class="card_class" onclick="document.location='secondpay.php?demo_id=<?php echo $demoId;?>'"><img src="images/paypal.png"></span>
			</div>
		</div>
	</div>
	<div style="min-height:200px;background:url(images/footer_bg2.jpg);">
		<div style="width:400px;margin-left:auto;padding-top:95px;padding-bottom:10px;">
			<img src="images/applogo.png">
		</div>
		<div class="copyright">
			<div style="width:283px;">
				Copyright &copy; My Appy Restaurant 2013 All rights reserved
			<div>
		</div>
	</div>
	<div style="position:absolute; top:0px; right:0px;"><img src="images/sun.png" /></div>
	
	<!-- <div style="position:absolute; bottom:30px;right:120px;"><img src="images/myappyrestaurants.png" style="width:280px;height:50px;"/></div> -->
</body>
</html>