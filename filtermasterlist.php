<?php
$parent_directory=basename(dirname($_SERVER["PHP_SELF"]));
$filename=basename($_SERVER["PHP_SELF"]);
include("../includes/configure.php");
include("includes/cmm_functions.php");
include("../includes/session_check.php");
$id=$_SESSION['user_id'];
$user_name=$_SESSION['user_name'];
$personsql="select * from  tbl_sales_person where salesperson='$user_name'";
$getpersonRes=mysql_query($personsql);
$getsalepersonCnt=mysql_num_rows($getpersonRes);
$managersql="select * from   tbl_salesmanager  where sales_manager='$user_name'";
$getmanagerRes=mysql_query($managersql);
$getmanagerCnt=mysql_num_rows($getmanagerRes);
$feat="master_list";
$typeandfeature=checklogin($id,$feat);
$usrArr=explode("*",$typeandfeature);
$user_type=$usrArr[0];
$mfeature=$usrArr[1];
$feat="demo_page";
$typeandfeature=checklogin($id,$feat);
$usrArr=explode("*",$typeandfeature);
$user_type=$usrArr[0];
$dfeature=$usrArr[1];
$dowfeature=$usrArr[2];
$trialfeature=$usrArr[4];
$demofeature=$usrArr[6];
if(($mfeature!="yes")&&($user_type!="admin")){
	header("Location:noauthorised.php");
	exit;
}
else{
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
	<html>
		<head>
			<title>MAR Pipeline System</title>
			<link rel="shortcut icon" href="images/Fav.ico" type="image/ico">
			<meta name="Generator" content="EditPlus">
			<meta name="Author" content="">
			<meta name="Keywords" content="">
			<meta name="Description" content="">
			<script type="text/javascript" src="js/jquery.js"></script>
			<script>
			var mode="<?php echo $mode;?>";
			</script>
			<script type="text/javascript" src="js/validate.js"></script>
			<style>
				body{
					margin:0;
					color:#D9D9D9;
					background:#455A68;
					font-family:arial;
				}
				.header{
					height:70px;
					background:#1C242A;
				}
				.content{
					background:#455A68;
					min-height:600px;
				}
				
				.form_actions{
					padding-top:15px;
					/*padding-left:5px;*/
					padding-bottom:30px;
				}
				.form_actions .add_btn{
					cursor:pointer;
					border-radius:0px;
					background:#0D0D0D;
					color:#D9D9D9;
					border-color:#D9D9D9;
					padding:5px 15px 5px 15px;
				}
				.list_content{
					width:950px;
					margin-left:40px;
					/*margin-left:auto;
					margin-right:auto;*/
				}
				.tbl_header th{
					font-size:13px;
					border-bottom:1px solid #D9D9D9;
					text-align:left;
				}
				.tbl-body{
					font-size:12px;
				}
				a{
					color:black;
				}
				.inp_feild{
					border-radius:2px;
					border:none;
					width:100%;
				}
			</style>
			<link href="css/slate.css" rel="stylesheet"> 
		    <link href="css/ui-lightness/jquery-ui-1.8.21.custom.css" rel="stylesheet">
			<script src="js/jquery-1.7.2.min.js"></script>
			<script src="js/jquery-ui-1.8.21.custom.min.js"></script>
			 <script src="js/demo/demo.calendar.js"></script>
		</head>
		<body>
			<div>
				<div style="margin-left:auto;margin-right:auto;">
					
					<div class="header">
						<span style="float:right;margin-right:20px;margin-top:5px;"><a href="admin/logout.php" style="color:white;text-decoration:none;">Logout</a></span>
						<div style="width:990px;height:80px;">
							 <div style="float:left;width:55%;">
								<img src="images/myappyrestaurants.png" style="margin-top:10px;margin-left:40px;">
								
							 </div>
							
						</div> 
					</div>
					<div class="content">
						<div class="list_content">
							<h1 style="font-size:25px;padding-top:15px;padding-bottom:15px;margin:0px;">Filter Master List</h1>
							<form name="filter_form" id="filter_form" method="post" action="masterlist.php">
							<table cellspacing="15" cellpadding="0" border="0" width="75%">
								<tr>
									<td valign="top">
										Salesperson:
									</td>
									<td>
										
										<select name="salesperson[]" id="salesperson"  ondblclick="clearSelected();" class="inp_feild" style="width:50%;" multiple>
										<?php
											$getmQry="select * from  tbl_sales_person order by sales_person_id asc";
											$getmRes=mysql_query($getmQry);
											while($getmRow=mysql_fetch_array($getmRes)){
											?>
											<option value="<?php echo $getmRow["salesperson"]?>"><?php echo $getmRow["salesperson"]?></option>
										<?php
											}
										?>
										
									</select>
									</td>
									<script>
                                       function clearSelected(){
										
                                     var elements = document.getElementById("salesperson").options;
											for(var i = 0; i < elements.length; i++){
															   if(elements[i].selected)
																elements[i].selected = false;
															}
  }
										</script>
								</tr>
								<tr>
									<td valign="top">
										Sales Manager:
									</td>
									<td>
										
										<select name="salesmanager[]" id="salesmanager" ondblclick="salesmanagerSelected();" class="inp_feild" style="width:50%;" multiple>
										<?php
											$getmQry="select * from  tbl_salesmanager order by sales_manager_id asc";
											$getmRes=mysql_query($getmQry);
											while($getmRow=mysql_fetch_array($getmRes)){
											?>
											<option value="<?php echo $getmRow["sales_manager"]?>"><?php echo $getmRow["sales_manager"]?></option>
										<?php
											}
										?>
										
									</select>
									</td>
								</tr>
                                 <tr>
								 <script>
                                       function salesmanagerSelected(){
										
                                     var elements = document.getElementById("salesmanager").options;
											for(var i = 0; i < elements.length; i++){
															   if(elements[i].selected)
																elements[i].selected = false;
															}
  }
										</script>
									<td>
										Trial Date:
									</td>
									<td>
									<table cellspacing="0" cellpadding="0" width="100%">
										<tr>
											<td width="50%">
												<input type="text" name="trial_date" id="trial_date" class="inp_feild" style="width:97%;" value="<?php echo $res3_state;?>">
											</td>
											<td align="right" width="50%">
												<input type="text" name="trial_date2" id="trial_date2" class="inp_feild" style="width:97%;" value="<?php echo $res3_postcode;?>">
											</td>
										</tr>
									</table>
								</td>
								</tr><tr>
									<td>
										Demo Date:
									</td>
									<td>
									<table cellspacing="0" cellpadding="0" width="100%">
										<tr>
											<td width="50%">
												<input type="text" name="demo_date" id="demo_date" class="inp_feild" style="width:97%;" value="<?php echo $res3_state;?>">
											</td>
											<td align="right" width="50%">
												<input type="text" name="demo_date2" id="demo_date2" class="inp_feild" style="width:97%;" value="<?php echo $res3_postcode;?>">
											</td>
										</tr>
									</table>
								</td>
								</tr>

								<tr>
									<td valign="top">
										Status:
									</td>
									<td>
										
										<select name="status[]" id="status" class="inp_feild" ondblclick="statuselected();" style="width:50%;" multiple>
										<?php
											$getmQry="select * from   tbl_status order by status_id asc";
											$getmRes=mysql_query($getmQry);
											while($getmRow=mysql_fetch_array($getmRes)){
											?>
											<option value="<?php echo $getmRow["status"]?>"><?php echo $getmRow["status"]?></option>
										<?php
											}
										?>
										
									</select>
									</td>
								</tr>
								 <script>
                                       function statuselected(){
										
                                     var elements = document.getElementById("status").options;
											for(var i = 0; i < elements.length; i++){
															   if(elements[i].selected)
																elements[i].selected = false;
															}
  }
										</script>
								<tr>
									<td>
										Payment Date:
									</td>
									<td>
									<table cellspacing="0" cellpadding="0" width="100%">
										<tr>
											<td width="50%">
												<input type="text" name="payment_date" id="payment_date" class="inp_feild" style="width:97%;" value="<?php echo $res3_state;?>">
											</td>
											<td align="right" width="50%">
												<input type="text" name="payment_date2" id="payment_date2" class="inp_feild" style="width:97%;" value="<?php echo $res3_postcode;?>">
											</td>
										</tr>
									</table>
								</td>
								</tr>
									<tr>
									<td>
										Commission Date:
									</td>
									<td>
									<table cellspacing="0" cellpadding="0" width="100%">
										<tr>
											<td width="50%">
												<input type="text" name="commission_date" id="commission_date" class="inp_feild" style="width:97%;" value="<?php echo $res3_state;?>">
											</td>
											<td align="right" width="50%">
												<input type="text" name="commission_date2" id="commission_date2" class="inp_feild" style="width:97%;" value="<?php echo $res3_postcode;?>">
											</td>
										</tr>
									</table>
								</td>
								</tr>												
								<tr>
									<td colspan="2">
										<table cellspacing="0" cellpadding="0" width="100%">
											<tr>
												<td>
													<div class="form_actions">
														<input type="button" value="Back to Master List" onclick="document.location='masterlist.php'" class="add_btn">
													</div>
												</td>
												<td align="center">
													<div class="form_actions">
														<input type="reset" value="Clear all filters" class="add_btn">
													</div>
												</td>
												<td>
													<div class="form_actions" style="text-align:right;">
														<input type="button" value=" Filter Master List" class="add_btn" id="filter_master_list">
													</div>
												</td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
							</form>
						</div>
						
					</div>
				</div>
			</div>
		</body>
	</html>
<?php
}
?>