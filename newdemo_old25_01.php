<?php
$simulator='1';
$style="";
$parent_directory=basename(dirname($_SERVER["PHP_SELF"]));
$filename=basename($_SERVER["PHP_SELF"]);

include("../includes/configure.php");
include("includes/cmm_functions.php");
include("../includes/session_check.php");
include("email_function.php");

$id=$_SESSION['user_id'];
$dbdatetime=date('Y-m-d H:i:s',strtotime('now'));
$feat="demo_page";
$typeandfeature=checklogin($id,$feat);
$usrArr=explode("*",$typeandfeature);
$user_type=$usrArr[0];
$feature=$usrArr[1];
$dowfeature=$usrArr[2];
$trialfeature=$usrArr[4];
$paymentf=$usrArr[3];
$demofeature=$usrArr[6];
if($demofeature!="yes"){
	header("Location:noauthorised.php");
	exit;
}
else
{
$demoId=$_GET["demo_id"];
if($demoId!="")
{


$getDemoQry="select * from tbl_demo where demo_id=:demoid";
$pepDemoQry=$DBCONN->prepare($getDemoQry);
$pepDemoQry->execute(array(':demoid'=>$demoId));
$getDemoRow=$pepDemoQry->fetch();

// Stimulator Info Section
$business_name=stripslashes($getDemoRow["business_name"]);
$business_type=stripslashes($getDemoRow['business_type']);
$email=stripslashes($getDemoRow["email"]);
$product_page_val=stripslashes($getDemoRow["product_page"]);
$promo_code =stripslashes($getDemoRow["promo_code"]);


$simulator=$getDemoRow["simulator_feature"];
if($simulator=="1")
	$style="";	
else
	$style="display:none";

$restaurant_logo = stripslashes($getDemoRow["restaurant_logo"]);
$theme=stripslashes($getDemoRow["theme"]);
$created_user_id=$getDemoRow['user_id'];
	
if($created_user_id=="" || $created_user_id=="0")	
  $created_user_id=$_SESSION['user_id'];	

$geUserQry="select * from tbl_users where user_id=:user_id";
$prepgetuserqry=$DBCONN->prepare($geUserQry);
$prepgetuserqry->execute(array(":user_id"=>$created_user_id));
$geUserRow=$prepgetuserqry->fetch();
$created_user=stripslashes($geUserRow["username"]);

// CONTACT INFO
$first_name=stripslashes($getDemoRow["first_name"]);
$res1_address=stripslashes($getDemoRow["res1_address"]);
$res1_suburb=stripslashes($getDemoRow["res1_suburb"]);
$res1_state=stripslashes($getDemoRow["res1_state"]);
$res1_postcode=stripslashes($getDemoRow["res1_postcode"]);
$res1_country=stripslashes($getDemoRow["res1_country"]);
$res1_workphone=stripslashes($getDemoRow["res1_workphone"]);
$mobile_phone=stripslashes($getDemoRow["mobile_phone"]);

// SALES INFO
$callstatus=stripslashes($getDemoRow["call_status"]);

$demo_date="";
if($getDemoRow["demo_date"]!="" && $getDemoRow["demo_date"]!="0000-00-00")
	$demo_date=date('d-m-Y',strtotime(stripslashes($getDemoRow["demo_date"])));

$demo_time="";
if($getDemoRow["demo_time"]!="" && $getDemoRow["demo_time"]!="00:00:00")
	  $demo_time=date('g:i A',strtotime(stripslashes($getDemoRow["demo_time"])));	

// SYSTEM INFO
$CreatedDate = date('d-m-Y',strtotime($getDemoRow["added_date"]));

// DEVELOPMENT INFO
$status=stripslashes($getDemoRow["status"]);
$android_status=stripslashes($getDemoRow["android_status"]);

$iphonedate="";   
if($getDemoRow["iphone_date"]!="" && $getDemoRow["iphone_date"]!="0000-00-00")
	$iphonedate=date('d-m-Y',strtotime(stripslashes($getDemoRow["iphone_date"])));
$androiddate="";
if($getDemoRow["android_date"]!="" && $getDemoRow["android_date"]!="0000-00-00")
	$androiddate=date('d-m-Y',strtotime(stripslashes($getDemoRow["android_date"])));

$developer_id=stripslashes($getDemoRow["developer_id"]);
$business_logo = stripslashes($getDemoRow["business_logo"]);
$app_name=stripslashes($getDemoRow["app_name"]);
$default_langauage=stripslashes($getDemoRow["default_langauage"]);
$itunes_category=stripslashes($getDemoRow["itunes_category"]);
$app_modifications =stripslashes($getDemoRow["app_modifications"]);
$background_image =stripslashes($getDemoRow["background_image"]);
$background_image2 =stripslashes($getDemoRow["background_image2"]);
$background_image3 =stripslashes($getDemoRow["background_image3"]);
$background_image4 =stripslashes($getDemoRow["background_image4"]);


// PAYMENT INFORMATIONS
$paymentdate="";
if($getDemoRow["payment_date"]!="" && $getDemoRow["payment_date"]!="0000-00-00")
	$paymentdate=date('d-m-Y',strtotime(stripslashes($getDemoRow["payment_date"])));

$commisiondate="";
if($getDemoRow["commission_date"]!="" && $getDemoRow["commission_date"]!="0000-00-00")
	$commisiondate=date('d-m-Y',strtotime(stripslashes($getDemoRow["commission_date"])));

$Dev_date = "";
if($getDemoRow["dev_qa_date"]!="" && $getDemoRow["dev_qa_date"]!="0000-00-00")
	$Dev_date=date('d-m-Y',strtotime(stripslashes($getDemoRow["dev_qa_date"])));

$affiliate_id=$getDemoRow['a_id'];
$Licensee=stripslashes($getDemoRow["l_id"]);
$Background_RGB=stripslashes($getDemoRow["Background_RGB"]);
//AD-HOC PAYMENT INFORMATION
$item_description    =stripslashes($getDemoRow["item_description"]);
$one_offprice        =stripslashes($getDemoRow["one_offprice"]);
$recrring_price      =stripslashes($getDemoRow["recrring_price"]);
$currency_code       =stripslashes($getDemoRow["currency"]);
$payment_taxcode     =stripslashes($getDemoRow["payment_taxcode"]);
$payment_taxrate     =stripslashes($getDemoRow["payment_taxrate"]);
$generate_payment_url=stripslashes($getDemoRow["generate_payment_url"]);

$mode="Edit";
$value="Update";

}
else
{
	
$created_user_id = $_SESSION['user_id'];
$geUserQry="select * from  tbl_users where user_id=:user_id";
$prepgetuserqry=$DBCONN->prepare($geUserQry);
$prepgetuserqry->execute(array(":user_id"=>$created_user_id));
$geUserRow=$prepgetuserqry->fetch();
$created_user=stripslashes($geUserRow["username"]);

$mode="Add";
$value="Create";

}


if(isset($_POST["first_name"]))
{
	$DemoTableArray = array();
	
	// Stimulator Info Section
	$business_name=addslashes(trim($_POST["business_name"]));
	$business_type=addslashes(trim($_POST["business_type"]));
	$email=addslashes(trim($_POST["email"]));
	$product_page=addslashes(trim($_POST["product_page"]));
	$promo_code=addslashes(trim($_POST["promo_code"]));

	if(empty($_POST["simulator"]))
		$simulator_value="0";
	else
		$simulator_value="1";  		

	$ResLogoQryCondition = "";
	$file_name=pathinfo($_FILES["restaurant_logo"]["name"], PATHINFO_FILENAME);
	if($file_name != "")
	{
		$file_extension=pathinfo($_FILES["restaurant_logo"]["name"], PATHINFO_EXTENSION);
		$file_path = UPLOAD_PATH.$file_name.time().".".$file_extension;
		
		move_uploaded_file($_FILES["restaurant_logo"]["tmp_name"], "../".$file_path);
		$ResLogoQryCondition = "restaurant_logo=:restaurant_logo, ";
	}

	$theme=addslashes(trim($_POST["theme"]));

	// Contact Info Section
	$first_name=addslashes(trim($_POST["first_name"]));
	$street1_address=addslashes(trim($_POST["street1_address"]));
	$suburb1=addslashes(trim($_POST["suburb1"]));
	$state1=addslashes(trim($_POST["state1"]));
	$country1=addslashes(trim($_POST["country1"]));
	$postcode1=addslashes(trim($_POST["postcode1"]));
	$res1_workphone=addslashes(trim($_POST["res1_workphone"]));
	$mobile_phone=addslashes(trim($_POST["mobile_phone"]));

	// Sales Info Section
	$callstatus=addslashes(trim($_POST["callstatus"]));
	$demo_date=(!empty($_POST["demo_date"]))?date('Y-m-d',strtotime(addslashes(trim($_POST["demo_date"])))):NULL;
	if(!empty($_POST["demo_time"]))
	{
		$demo_timeArr=explode(" ",addslashes(trim($_POST["demo_time"])));
		if($demo_timeArr[1]=="PM")
			$demo_time=date("H:i:s",strtotime($demo_timeArr[0]."pm"));
		else
			$demo_time=date("H:i:s",strtotime($demo_timeArr[0]."am"));
	}
	else
		$demo_time=NULL;

	// Development Info Section
	if($_POST["development_info_status"]!="")
	 $status = addslashes(trim($_POST["development_info_status"])); // This field will represent status in development section
	else
	  $status=NULL;
	
	if($_POST["development_info_android_status"]!="")
	   $android_status = addslashes(trim($_POST["development_info_android_status"]));
	else
	  $android_status=NULL;
	
	if($_POST["iphone_date"]!="")
		$iphone_date=date('Y-m-d',strtotime(addslashes(trim($_POST["iphone_date"]))));
	else
		$iphone_date=NULL;
 
	if($_POST["android_date"]!="")
		$android_date=date('Y-m-d',strtotime(addslashes(trim($_POST["android_date"]))));
	else
		$android_date=NULL;

	$hdnstatus = addslashes(trim($_POST["hdn_development_info_status"]));
	$developer = addslashes(trim($_POST["developer"]));

	$BusinessLogoQryCondition = "";
	$business_logo_file_name = pathinfo($_FILES["business_logo"]["name"], PATHINFO_FILENAME);
	if($business_logo_file_name != "")
	{
		$business_logo_fe = pathinfo($_FILES["business_logo"]["name"], PATHINFO_EXTENSION);
		$business_logo_file_path = UPLOAD_PATH.$business_logo_file_name.time().".".$business_logo_fe;
		
		move_uploaded_file($_FILES["business_logo"]["tmp_name"], "../".$business_logo_file_path);
		$BusinessLogoQryCondition = "business_logo=:business_logo, ";
	}
	$app_name = addslashes(trim($_POST["app_name"]));
	$def_language = addslashes(trim($_POST["def_language"]));
	$itunes_category = addslashes(trim($_POST["itunes_category"]));
	$app_modifications = addslashes(trim($_POST["app_modifications"]));
	$Background_RGB = addslashes(trim($_POST["Background_RGB"])); 

	$BackgroundImageQryCondition = "";
	$background_image_file_name = pathinfo($_FILES["background_image"]["name"], PATHINFO_FILENAME);
	if($background_image_file_name != "")
	{
		$background_image_fe = pathinfo($_FILES["background_image"]["name"], PATHINFO_EXTENSION);
		$background_image_file_path = UPLOAD_PATH.$background_image_file_name.time().".".$background_image_fe;
		
		move_uploaded_file($_FILES["background_image"]["tmp_name"], "../".$background_image_file_path);
		$BackgroundImageQryCondition = "background_image=:background_image, ";
	}
	
	$BackgroundImage2QryCondition = "";
	$background_image_file_name2 = pathinfo($_FILES["background_image2"]["name"], PATHINFO_FILENAME);
	if($background_image_file_name2 != "")
	{
		$background_image_fe2 = pathinfo($_FILES["background_image2"]["name"], PATHINFO_EXTENSION);
		$background_image_file_path2 = UPLOAD_PATH.$background_image_file_name2.time().".".$background_image_fe2;
		
		move_uploaded_file($_FILES["background_image2"]["tmp_name"], "../".$background_image_file_path2);
		$BackgroundImage2QryCondition = "background_image2=:background_image2, ";
	}

	$BackgroundImage3QryCondition = "";
	$background_image_file_name3 = pathinfo($_FILES["background_image3"]["name"], PATHINFO_FILENAME);
	if($background_image_file_name3 != "")
	{
		$background_image_fe3 = pathinfo($_FILES["background_image3"]["name"], PATHINFO_EXTENSION);
		$background_image_file_path3 = UPLOAD_PATH.$background_image_file_name3.time().".".$background_image_fe3;
		
		move_uploaded_file($_FILES["background_image3"]["tmp_name"], "../".$background_image_file_path3);
		$BackgroundImage3QryCondition = "background_image3=:background_image3, ";
	}	

	$BackgroundImage4QryCondition = "";
	$background_image_file_name4 = pathinfo($_FILES["background_image4"]["name"], PATHINFO_FILENAME);
	if($background_image_file_name4 != "")
	{
		$background_image_fe4 = pathinfo($_FILES["background_image4"]["name"], PATHINFO_EXTENSION);
		$background_image_file_path4 = UPLOAD_PATH.$background_image_file_name4.time().".".$background_image_fe4;
		
		move_uploaded_file($_FILES["background_image4"]["tmp_name"], "../".$background_image_file_path4);
		$BackgroundImage4QryCondition = "background_image4=:background_image4, ";
	}


	// Payment Info Section
	if($_POST["payment_date"]!="")
		$paymentdate=date('Y-m-d',strtotime(addslashes(trim($_POST["payment_date"]))));
	else
		$paymentdate=NULL;

	if($_POST["commision_date"]!="")
		$commision_date=date('Y-m-d',strtotime(addslashes(trim($_POST["commision_date"]))));
	else
		$commision_date=NULL;

	 if($_POST["dev_date"]!="")
		$dev_date=date('Y-m-d',strtotime(addslashes(trim($_POST["dev_date"]))));
	else
		$dev_date=NULL;

	$affiliate=$_POST["affiliate"];

	//AD-HOC PAYMENT INFORMATION
	$item_description   =$_POST["item_description"];
	$one_offprice       =$_POST["one_offprice"];
	$recrring_price     =$_POST["recrring_price"];
	$currency           =$_POST["currency"];   
	$tax_code           =$_POST["tax_code"];
	$tax_rate           =$_POST["tax_rate"];

	$CurrentDate = date("Y-m-d H:i:s");
	$DemoTableArray = array(":business_name" => $business_name
							, ":business_type" => $business_type
							, ":email" => $email							
							, ":simulator_feature" => $simulator_value
							, ":theme" => $theme
							, ":first_name" => $first_name
							, ":res1_address" => $street1_address
							, ":res1_suburb" => $suburb1
							, ":res1_state" => $state1
							, ":res1_postcode" => $postcode1
							, ":res1_country" => $country1
							, ":res1_workphone" => $res1_workphone
							, ":mobile_phone" => $mobile_phone							
							, ":demo_date" => $demo_date
							, ":demo_time" => $demo_time
							, ":status" => $status
		                    , ":android_status" => $android_status
							, ":iphone_date" => $iphone_date 
							, ":android_date" => $android_date
							, ":developer_id" => $developer
							, ":app_name" => $app_name
							, ":default_langauage" => $def_language
							, ":itunes_category" => $itunes_category
							, ":app_modifications" => $app_modifications
							, ":payment_date" => $paymentdate
							, ":commission_date" => $commision_date
							, ":dev_qa_date" => $dev_date
							, ":a_id" => $affiliate
							, ":l_id" => $licence_id
							, ":modified_date" => $CurrentDate
							, ":Background_RGB" => $Background_RGB
		                    , ":item_description" => $item_description
							, ":one_offprice" => $one_offprice
							, ":recrring_price" => $recrring_price
							, ":currency" => $currency
							, ":tax_code" => $tax_code
							, ":tax_rate" => $tax_rate
						
							);

	// Add restraurant logo value to array if field uploaded
	$ResLogoQryCondAdd1 = "";
	$ResLogoQryCondAdd2 = "";
	if($ResLogoQryCondition != "")
	{
		$ResLogoQryCondAdd1 = "restaurant_logo, ";
		$ResLogoQryCondAdd2 = ":restaurant_logo, ";
		$DemoTableArray[':restaurant_logo'] = $file_path;
	}

	// Add Business logo value to array if field uploaded
	$BiznsLogoQryCondAdd1 = "";
	$BiznsLogoQryCondAdd2 = "";
	if($BusinessLogoQryCondition != "")
	{
		$BiznsLogoQryCondAdd1 = "business_logo, ";
		$BiznsLogoQryCondAdd2 = ":business_logo, ";
		$DemoTableArray[':business_logo'] = $business_logo_file_path;
	}

	// Add BackgroundImage value to array if field uploaded
	$BackgroundImageQryCondAdd1 = "";
	$BackgroundImageQryCondAdd2 = "";
	if($BackgroundImageQryCondition != "")
	{
		$BackgroundImageQryCondAdd1 = "background_image, ";
		$BackgroundImageQryCondAdd2 = ":background_image, ";
		$DemoTableArray[':background_image'] = $background_image_file_path;
	}

	// Add BackgroundImage 2  value to array if field uploaded
	$BackgroundImage2QryCondAdd1 = "";
	$BackgroundImage2QryCondAdd2 = "";
	if($BackgroundImage2QryCondition != "")
	{
		$BackgroundImage2QryCondAdd1 = "background_image2, ";
		$BackgroundImage2QryCondAdd2 = ":background_image2, ";
		$DemoTableArray[':background_image2'] = $background_image_file_path2;
	}

	// Add BackgroundImage 3  value to array if field uploaded
	$BackgroundImage3QryCondAdd1 = "";
	$BackgroundImage3QryCondAdd2 = "";
	if($BackgroundImage3QryCondition != "")
	{
		$BackgroundImage3QryCondAdd1 = "background_image3, ";
		$BackgroundImage3QryCondAdd2 = ":background_image3, ";
		$DemoTableArray[':background_image3'] = $background_image_file_path3;
	}

	// Add BackgroundImage 4  value to array if field uploaded
	$BackgroundImage4QryCondAdd1 = "";
	$BackgroundImage4QryCondAdd2 = "";
	if($BackgroundImage4QryCondition != "")
	{
		$BackgroundImage4QryCondAdd1 = "background_image4, ";
		$BackgroundImage4QryCondAdd2 = ":background_image4, ";
		$DemoTableArray[':background_image4'] = $background_image_file_path4;
	}


	$Mode = $_POST["hdn_mode"];
	if($Mode == 'Edit')
	{
		$DemoTableArray[':demo_id'] = $demoId;

		$DemoQry = "update tbl_demo set 
		business_name=:business_name,  business_type=:business_type, email=:email, simulator_feature=:simulator_feature, ".$ResLogoQryCondition."theme=:theme,  
		first_name=:first_name, res1_address=:res1_address, res1_suburb=:res1_suburb, res1_state=:res1_state, res1_postcode=:res1_postcode,res1_country=:res1_country,res1_workphone=:res1_workphone, mobile_phone=:mobile_phone,
		demo_date=:demo_date, demo_time=:demo_time,
		status=:status, android_status=:android_status,iphone_date=:iphone_date, android_date=:android_date, developer_id=:developer_id, ".$BusinessLogoQryCondition."app_name=:app_name, default_langauage=:default_langauage, itunes_category=:itunes_category, app_modifications=:app_modifications, 
		payment_date=:payment_date, commission_date=:commission_date, dev_qa_date=:dev_qa_date,
		a_id=:a_id, l_id=:l_id, modified_date=:modified_date, ".$BackgroundImageQryCondition.$BackgroundImage2QryCondition.$BackgroundImage3QryCondition.$BackgroundImage4QryCondition."Background_RGB=:Background_RGB , item_description=:item_description, one_offprice=:one_offprice, recrring_price=:recrring_price, currency=:currency,payment_taxcode=:tax_code,payment_taxrate=:tax_rate   
		where 
		demo_id=:demo_id";	


		
	}
	else
	{
		
		$DemoTableArray[':product_page'] = $product_page;
		$DemoTableArray[':call_status'] = $callstatus;
		$DemoTableArray[':promo_code'] = $promo_code;
		$DemoTableArray[':user_id'] = $_SESSION['user_id'];
		$DemoTableArray[':added_date'] = $CurrentDate;
		
		$DemoQry = "INSERT INTO tbl_demo 
					(business_name, business_type, email, product_page, promo_code, simulator_feature, ".$ResLogoQryCondAdd1." theme, user_id,
					first_name, res1_address, res1_suburb, res1_state, res1_postcode, res1_country, res1_workphone, mobile_phone,
					call_status, demo_date, demo_time, 
					status, android_status, iphone_date, android_date, developer_id, ".$BiznsLogoQryCondAdd1." app_name, default_langauage, itunes_category, app_modifications, 
					payment_date, commission_date, dev_qa_date, a_id, l_id,
					added_date, modified_date, ".$BackgroundImageQryCondAdd1.$BackgroundImage2QryCondAdd1.$BackgroundImage3QryCondAdd1.$BackgroundImage4QryCondAdd1." Background_RGB ,item_description ,	one_offprice, recrring_price, currency, payment_taxcode, payment_taxrate) 
					VALUES 
					(:business_name, :business_type, :email, :product_page, :promo_code, :simulator_feature, ".$ResLogoQryCondAdd2." :theme, :user_id,
					:first_name, :res1_address, :res1_suburb, :res1_state, :res1_postcode, :res1_country, :res1_workphone, :mobile_phone,
					:call_status, :demo_date, :demo_time, 
					:status, :android_status, :iphone_date, :android_date, :developer_id, ".$BiznsLogoQryCondAdd2." :app_name, :default_langauage, :itunes_category, :app_modifications,
					:payment_date, :commission_date, :dev_qa_date, :a_id, :l_id,
					:added_date, :modified_date, ".$BackgroundImageQryCondAdd2.$BackgroundImage2QryCondAdd2.$BackgroundImage3QryCondAdd2.$BackgroundImage4QryCondAdd2." :Background_RGB,:item_description, :one_offprice, :recrring_price,:currency,:tax_code,:tax_rate)";
	}

		
	$PrepareDemoQuery =$DBCONN->prepare($DemoQry);	
	$PrepareDemoQuery->execute($DemoTableArray);
		
	if($PrepareDemoQuery->errorCode()> 0)
	{
		$arrerrors = $PrepareDemoQuery->errorInfo();
		print_r($arrerrors);
		exit;
	}

	// Sending email here if status column changed.
	// Email need to be send in 2 cases. 
	// 1. When the status is changed to "Apps awaiting app store approval", then an automated "Backend Login" email should be sent to the customer
	// 2. When the status is changed to "Apps are live!", then an automated "Apps are live!" email should be sent to the customer
	/*
	if($status != $hdnstatus)
	{
		
		$LogoUrl = 'http://www.myappybusiness.com/images/email-logo.png';
		if($status == 'Apps awaiting app store approval')
			AppsAwaitingAppStoreApprovalEmail($email, $LogoUrl);
		else if($status == 'Apps are live!')
			AppsAreLiveEmail($email, $LogoUrl);
			
	}
	*/
	header("Location:masterlist.php");
	exit;

}

include("includes/header_v1.php");
?>

<style>
a:link, a:visited { 
    color: white;
    text-decoration: underline;
    cursor: auto;
	font-size:12px;
}
</style>
<script>
$(function() {
	<?php
	if($paymentf=="yes"){
	?>
		$("#payment_date").datetimepicker({ format:'d-m-Y',formatDate:'d-m-Y',timepicker:false});
		$("#commision_date").datetimepicker({ format:'d-m-Y',formatDate:'d-m-Y',timepicker:false});
		$("#dev_date").datetimepicker({ format:'d-m-Y',formatDate:'d-m-Y',timepicker:false});
	<?php
	}
	?>
	$("#iphone_date").datetimepicker({ format:'d-m-Y',formatDate:'d-m-Y',timepicker:false});
	$("#android_date").datetimepicker({ format:'d-m-Y',formatDate:'d-m-Y',timepicker:false});
});
</script>
<body>

           
		<div>
			<div style="margin-left:auto;margin-right:auto;">
				 <div class="content">
					<div class="list_content">

					         
					    <table cellspacing="15" cellpadding="0" border="0" width="75%">
							 	<tr>
									<td colspan="2">
										<table cellspacing="0" cellpadding="0" width="100%" border="0">
											<tr>
												<td>
												   <div class="form_actions">
												   <input type="button" value="Back To Master List" class="add_btn"   onclick="document.location='masterlist.php'">
												</div>
												 
												</td>										 
											 	<td>
												<div class="form_actions" style="text-align:right;">
													      <input type="button" value="<?php echo ucfirst($value);?> Record" class="add_btn add_demo" id="add_demo">  
												</div>
												
												</td>
											</tr>
										</table>
									</td>
								</tr>
							</table>				 
						 <form name="demo_form" id="demo_form" method="post" enctype="multipart/form-data">
						 <input type="hidden" name="hdn_mode" id="hdn_mode" value="<?php echo $mode;?>">
						 <input type="hidden" name="hdn_demo_id" id="hdn_demo_id" value="<?php echo $demoId;?>">
						 <input type="hidden" name="hdn_img" id="hdn_img" value="<?php echo $old_special_logo;?>">
							<table cellspacing="15" cellpadding="0" border="0" width="75%">
								<tr>
								<td colspan="2" nowrap>
										<h3><u>SIMULATOR INFO:</u></h3>
									</td>
								</tr>
								<?php
									if($msg!="")
									{
								?>
								<tr bgcolor="white" height="40px" id="error_message">
									<td style="color:black;font-size:20px;font-family:arial;margin-left:10px;" colspan="2">Client trial form added successfully.</td>
								</tr>
								<?php
									}
								?>
								<tr>
									<td style="width:190px;" nowrap>
										Business Name<font color="red">*</font>:
									</td>
									<td>
									    
										<input type="text" name="business_name" id="business_name" class="inp_feild" value="<?php echo $business_name;?>" tabindex="1">
									</td>
								</tr>
								<tr>
									<td nowrap>
										Business Type<font color="red">*</font>:
									</td>
									<td>
										
										<select name="business_type"  id="business_type" class="inp_feild" tabindex="2">
									    <option value="0">Select</option>
										<?php
										$getbusinessQry="select * from  tbl_business_type order by display_order asc";
										$prepget_business_qry=$DBCONN->prepare($getbusinessQry);
										$prepget_business_qry->execute();
										while($getbusinessRow=$prepget_business_qry->fetch()){
										?>
										<option value="<?php echo $getbusinessRow["business_type"];?>" <?php if($business_type==$getbusinessRow["business_type"]){ echo "selected";}?>><?php echo $getbusinessRow["business_type"];?></option>
										<?php
										}
										?>
									</select>
									</td>
								</tr>
								<tr>
									<td nowrap>
										Email Address<font color="red">*</font>:
									</td>
									<td>
									 
										<input type="text" name="email" id="email" class="inp_feild" value="<?php echo $email;?>" tabindex="3">
									</td>
								</tr>
								<tr>
								<td style="width:138px;" valign="top" nowrap>
									Product Page<font color="red">*</font>:
								</td>
								<td>
									<select name="product_page" id="product_page" class="inp_feild" style="width:50%;" tabindex="4" <?php if($mode == 'Edit'){echo 'disabled';} ?>>
									    <option value="0">Select</option>
										<?php
										if($_SESSION['user_id']=="3" || $user_type=="Admin")
										{
												$getQry_1="SELECT  DISTINCT `product_page` FROM `tbl_apps`  order by `product_page` asc";
												$prepgetQry1=$DBCONN->prepare($getQry_1);
												$prepgetQry1->execute();
												while($getRow1=$prepgetQry1->fetch()){
												   $y=$getRow1["product_page"];
												   echo "<option value='".$y."'>".$y."</option>";	
												}

										}
										else
										{
												$getQry_1="SELECT * FROM  tbl_affiliate where a_user_id='".$_SESSION['user_id']."'";
												$prepgetQry1=$DBCONN->prepare($getQry_1);
												$prepgetQry1->execute();
												while($getRow1=$prepgetQry1->fetch()){
												   $x=$getRow1["product_page1"];
												   $y=$getRow1["product_page2"];
												   $z=$getRow1["product_page3"];
												   if(!empty($x)){
												      echo "<option value='".$x."'>".$x."</option>";
												   }
												    if(!empty($y)){
												      echo "<option value='".$y."'>".$y."</option>";
												   }
												     if(!empty($z)){
												      echo "<option value='".$z."'>".$z."</option>";
												   }


												}
										}
										
										?>
										
									</select>
									<?php
									if($product_page_val!=""){
									?>
									<script language="javascript">document.getElementById("product_page").value="<?php echo $product_page_val;?>"</script>
									<?php
									}
									?>
										
								</td>
							</tr>
							<tr>
								<td style="width:138px;" nowrap>
									 Promo Code<font color="red">*</font>:
								</td>
								<td>
									<input type="text" name="promo_code" id="promo_code" class="inp_feild" value="<?php echo $promo_code;?>" style="width:50%" tabindex="5">
								</td>
							</tr>
							<tr>
							<td style="width:138px;" valign="top" nowrap>
									 
								</td>
								<td>
									<input type="checkbox" name="simulator"   id="simulator"  value="1" <?php if($simulator=="1"){echo "checked";}?> onclick="show(this.id)" tabindex="6"> Add Simulator <br>
								</td>
							</tr>

							
							<tr class="theme_div" style="margin-top:5px;<?php echo $style;?>">
								 
							 
								<td valign="top" nowrap>
									Simulator Logo:
								</td>
								<td>
									<input type="file" name="restaurant_logo" id="restaurant_logo" class="inp_feild" value="<?php echo $restaurant_name;?>" tabindex="7">

									<?php 
									if(is_file($restaurant_logo))
									{
										$ExplodeLogo = explode(".", $restaurant_logo);
										$FileExtenstion = strtolower($ExplodeLogo[1]);

										if($FileExtenstion == 'jpg' || $FileExtenstion == 'jpeg' || $FileExtenstion == 'gif' || $FileExtenstion == 'png')
										{
											?>
											<br><br><img src="<?php echo $restaurant_logo?>" style="width:75px;height:75px;">
											<?php
										}
										else											
										{
											?>
											 <br><br>
											 <a href="<?php echo  HTTP_ROOT_FOLDER.$old_restaurant_logo;?>" target="_blank" >Download Stimulator Logo</a>
											<?php
										}
											
                                     }												
							 	 	?>
								</td>
						 
									 
							</tr>
							<tr class="theme_div_demo" style="display:none" >
							<td style="width:138px;" valign="top"> 
							</td>
							<td align="left" id="theme_div_design">
							 </td>
							</tr>
							<tr class="theme_div" style="margin-top:5px;<?php echo $style;?>" >
								<td style="width:138px;" valign="top" nowrap> 
									Design Style<font color="red">*</font>: 
								</td>
								<td>
									
									 
										<select name="theme" id="theme" class="inp_feild" tabindex="8" onchange="show_demo_image(this.value);">
											<option value="0">Select</option>
											<?php
											foreach($themes_array as $val=>$name){ 
											  echo '<option value="'.$val.'">'.$name.'</option>'; 
											} 
											?>
											 
										</select>
										<?php 
										 if($theme!="" && $theme>0){
											?>
										<script type="text/javascript">
										   $("#theme").val('<?php echo $theme;?>')
										</script>
										<?php 
										  }
										?>
								 						 
								</td>
							</tr>
							<tr>
								<td style="width:138px;" nowrap>
									Simulator Creator<font color="red">*</font>:
								</td>
								<td>
									 <input type="text" name="user_id" id="user_id" class="inp_feild" tabindex="9" value="<?php echo stripslashes($created_user);?>"  readonly >
								</td>
							</tr>
							<tr>
								<td colspan="2" nowrap>
										<h3><u>CONTACT INFO:</u></h3>
									</td>
							</tr>
							<tr>
								<td style="width:168px;" nowrap>
										Full Name<?php if($prospect_id==""){echo "<font color=\"red\">*</font>";} ?>:
									</td>
									<td>
									 
										<input type="text" name="first_name" id="first_name" class="inp_feild" value="<?php echo $first_name;?>" tabindex="10">
									</td>
								</tr>
								<tr>
									<td nowrap>
									Address<?php if($prospect_id==""){echo "<font color=\"red\">*</font>";} ?>:
									</td>
									<td>
										<input type="text" name="street1_address" id="street1_address" class="inp_feild" value="<?php echo $res1_address;?>" tabindex="11">
									</td>
								</tr>
								<tr>
									<td nowrap>
										Suburb:
									</td>
									<td>
										<input type="text" name="suburb1" id="suburb1" class="inp_feild" value="<?php echo $res1_suburb;?>" tabindex="12">
									</td>
								</tr>
								<tr>
									<td  valign="top" nowrap>State:</td>
									<td  valign="top">
										<table width="100%" style="margin-left: -3px;">
											<tr>
											<td><input type="text" name="state1" id="state1" class="inp_feild" value="<?php echo $res1_state;?>" tabindex="13"></td>
											<td align="center">Postcode:</td>
											<td><input type="text"  name="postcode1" id="postcode1" class="inp_feild"  maxlength="20" size="20" value="<?php echo stripslashes($res1_postcode);?>"   tabindex="14" ></td>
											</tr>
										</table>

									</td>
								</tr>

								<!-- <tr>
									<td>
										State:
									</td>
									<td>
										<input type="text" name="state1" id="state1" class="inp_feild" style="width:97%;" value="<?php echo $res1_state;?>"  tabindex="13">
									</td>
								</tr> -->
								<tr>
								<td nowrap>
									Country<font color="red">*</font>:
								</td>
								<td>
									<select name="country1" id="country1" class="inp_feild"  tabindex="15">
										
										<option value="0"<?php if($res1_country=="0") echo " selected";?>>Select</option>
										<?php
											$getcountrQry="select * from  tbl_country";
											$getcountryRes=mysql_query($getcountrQry);
											
											while($getcountryRow=mysql_fetch_array($getcountryRes)){
										   ?>
											<option value="<?php echo stripslashes($getcountryRow["country"]);?>"<?php if($res1_country==$getcountryRow["country"]) echo " selected";?>><?php echo stripslashes($getcountryRow["country"]);?></option>
										<?php
											}
										?>
									</select>

								</td>
								
							</tr>
					 			<tr>
									<td nowrap>
										Work Phone<font color="red">*</font>:
									</td>
									<td>
										<input type="text" name="res1_workphone" id="res1_workphone" class="inp_feild" value="<?php echo $res1_workphone;?>" tabindex="16">
									</td>
								</tr>
								<tr>
									<td nowrap>
										Mobile Phone:
									</td>
									<td>
										<input type="text" name="mobile_phone" id="mobile_phone" class="inp_feild" value="<?php echo $mobile_phone;?>" tabindex="17">
									</td>
								</tr>
								<tr>
								<td colspan="2" nowrap>
										<h3><u>SALES INFO:</u></h3>
									</td>
							  </tr>
							  <tr>
									<td nowrap>
										Call Status<font color="red">*</font>:
									</td>
									<td>
										<select name="callstatus" id="callstatus" class="inp_feild" style="width:50%;" tabindex="18"  <?php if($mode == 'Edit'){echo 'disabled';} ?>>
											
											<option value="0">Select</option>
											<?php
												$getcallstatusQry="select * from  tbl_callstatus order by display_order asc";
												$getcallstatusRes=mysql_query($getcallstatusQry);
												$callStsArr="";
												while($getcallstatusRow=mysql_fetch_array($getcallstatusRes)){
													if($dbpush=='yes'){
														if($getcallstatusRow["callstatus"]=='Trial requested' || $getcallstatusRow["callstatus"]=='Activation email sent (follow-up)' || $getcallstatusRow["callstatus"]=='Payment processed' || $getcallstatusRow["callstatus"]=='Trial not sold' ||  $getcallstatusRow["callstatus"]=='Promo email sent (follow-up)'){
															$callStsArr[]=stripslashes($getcallstatusRow["callstatus"]);
														}
													}
													else{
														if($getcallstatusRow["callstatus"]!='Activation email sent (follow-up)' && $getcallstatusRow["callstatus"]!='Trial not sold'){
															$callStsArr[]=stripslashes($getcallstatusRow["callstatus"]);
														}
													}
												}
												foreach($callStsArr as $stsval){
											   ?>
												<option value="<?php echo $stsval;?>"<?php if($callstatus==$stsval) echo " selected";?>><?php echo $stsval;?></option>
											<?php
												}
											?>
										</select>



									</td>
									
								</tr>
								 
								<tr>
									<td nowrap>Callback Date:&nbsp;&nbsp;&nbsp;</td>
									 <td><input type="text" name="demo_date" id="demo_date" class="inp_feild" style="width:25%;" value="<?php echo $demo_date;?>" tabindex="19">&nbsp;&nbsp;&nbsp;Time:&nbsp;&nbsp;&nbsp;<input type="text" name="demo_time" id="demo_time" class="inp_feild" style="width:25%;" value="<?php echo $demo_time?>" tabindex="20"></td>
							   </tr>
									<?php
							if($demoId!="")
							{
									?>
								<tr>
									<td valign="top" nowrap>
										Comments:
									</td>
									<td>
										<table border="0" width="100%" cellspacing="0">
										<tr>
											<td width="80%">
											   <textarea name="prospect_comments" id="prospect_comments" class="inp_feild " style="height:200px;font-family: arial;font-size:13px;width:100%;" tabindex="21" onkeyup="show_comment(this.value);" onkeypress="show_comment(this.value);"></textarea> 
											</td>
											<td  width="20%" valign="top">
												 <div class="form_actions">
												   <input type="button" value="Add Comment" class="add_btn add_coomment_box" onclick="addcomment('<?php echo addslashes($_SESSION['user_id']);?>','<?php echo $demoId;?>')" style="margin-top: -15px;margin-left: 17px;background-color: gray;border-color:gray;" tabindex="22">
												</div>
											</td>
										</tr>
										</table>
										<script>
									   function addcomment(userid,prospect_id){

										comment=$("#prospect_comments").val();
										if($.trim($("#prospect_comments").val())==""){
											alert("Please enter comments");
											$("#prospect_comments").focus();
											return false;

										}
										else{
												$.ajax({
												  type: "POST",
												  url: "ajax_comment1.php",
												  data: { comment: comment, userid: userid, prospect_id: prospect_id }
												}).done(function(r) {
													//alert( "Data Saved: " + msg );
													$('#prospect_comments').val("");
														$("#container").html(r);
														if($(".myTable tr:first td").length>1){
															setwidth();
														}
														else{
															$('#td1').width(150);
															$('#td2').width(395);
															$('#td3').width(150);
														}

												  });


												/*
												
												$.ajax({url:"ajax_comment1.php?comment="+comment+"&userid="+userid+"&prospect_id="+prospect_id+"",success:function(result){
												$('#prospect_comments').val("");
												$("#container").html(result);
												}});
													$.ajax({
													type : 'post',
													url : 'ajax_comment1.php', // in here you should put your query 

													data :  'comment='+comment+'&userid='+userid+'&prospect_id='+prospect_id, // here you pass your id via ajax .
													// in php you should use $_POST['post_id'] to get this value 
													success : function(r)
													{      
														$('#prospect_comments').val("");
														$("#container").html(r);
														if($(".myTable tr:first td").length>1){
															setwidth();
														}
														else{
															$('#td1').width(150);
															$('#td2').width(395);
															$('#td3').width(150);
														}
											 

													}
											}); 
													*/
											// else
										}

                                      }
									</script>
										<script>
													$(document).ready(function(){
													     $("#callstatus").trigger('change');
														  $("#record_type").trigger('change');
														   setwidth();
													 });
													 function setwidth()
													{
															if($(".myTable tr:first td").length>1){
																var table = $(".myTable tr:first");
																var firstColumnWidth = table.find("td:first").width();
																var secodColumnWidth = table.find("td:nth-child(2)").width();
																var thirdColumnWidth = table.find("td:nth-child(3)").width();
																var table_header = $(".mytable1 tr:first");
																$('#td1').width(firstColumnWidth);
																$('#td2').width(secodColumnWidth);
																$('#td3').width(thirdColumnWidth);
															}
															else{
																$('#td1').width(150);
																$('#td2').width(395);
																$('#td3').width(150);
															}

														}
														  </script>
									</td>
								</tr>		
								  <td colspan="2" valign="top" >
								  
									<table  cellpadding= "0 " width= "100% " cellspacing= "0 "  style= "border:1px solid #CAE1F9;font-size:13px;color:black;border-collapse: collapse; " border= "1 " class="mytable1">
									  <tr style="color:white;min-height: 20px;">
										<td valign="top"  align="left"  id="td1" nowrap>Date</td>
										<td valign="top"  align="left"  id="td2" nowrap>Comment</td>
										<td valign="top"  align="left"  id="td3" colspan='2' nowrap>User Name</td>
									</tr>
								</table>
								<div id="container" style="width:100%;overflow: auto;height:100px;">
								<table  cellpadding= "0 " width= "100% " cellspacing= "0 "  style= "border:1px solid black;font-size:13px;color:black;border-collapse: collapse; " border= "1 " class="myTable">
										<?php
										$getQry="select * from  tbl_master_comments where master_id=:master_id  order by comment_id desc";
										$prepgetQry=$DBCONN->prepare($getQry);
										$prepgetQry->execute(array(":master_id"=>$demoId));
										$count =$prepgetQry->rowCount();
										if($count>0){
										$i=1;
										while($getRow=$prepgetQry->fetch()){
										if($i%2==0)
										$display_color="#a5a5a5";
										else
										$display_color="#d2d1d1";
										$user_id=$getRow["user_id"];
										$get_Qry1="select * from  tbl_users where user_id=:user_id";
										$prepget1_Qry=$DBCONN->prepare($get_Qry1);
										$prepget1_Qry->execute(array(":user_id"=>$user_id));
										$get_Row1=$prepget1_Qry->fetch();
										$user_name=$get_Row1['username'];
										?>
										 
										<tr style="background-color:<?php echo $display_color;?>;height: 20px;">	
										<td valign="top"  align="left"  >
											<?php echo date("d-m-Y", strtotime($getRow["created_date"]));?>
										</td>
										<td valign="top"  align="left"   style="width:505px;">
											<?php echo stripslashes($getRow["comment"]);?>
										</td>
										<td valign="top"  align="left"   >
										    <?php echo $user_name;?></td>
										</tr>
										<?php
										$i++;
										}
										}
										else{
										echo "<tr style=\"background-color:#d2d1d1;text-align:center;\"><td colspan=\"3\">No Comment(s) found.</td></tr>";
										}
										?>
										</table>
										</div>
                                </tr>
								<?php
								}
								?>
								<tr>
								<td colspan="2" nowrap>
										<h3><u>SYSTEM INFO:</u></h3>
									</td>
							  </tr>
							  <tr>
									<td nowrap>
										Create Date<?php if($demoId==""){echo "<font color=\"red\">*</font>";} ?>:
									</td>
									<td>
										<input type="text" name="import_date" id="import_date" class="inp_feild" style="width:50%;" value="<?php 
										
									 if($demoId=="" || $CreatedDate==""){echo date("d-m-Y");} else { echo $CreatedDate; }?>" readonly tabindex="23">
									</td>
								</tr>
								<tr style="display:none;">
								<td nowrap>
									Salesperson<font color="red">*</font>:
								</td>
								<td nowrap>
									<select name="sales_person" id="sales_person" class="inp_feild" onchange="getmanger(this.value)" style="width:50%;" tabindex="24">
										<option value="0"<?php if($sales_person=="0") echo " selected";?>>Select</option>
										<?php
											$getBookQry="select * from tbl_sales_person";
											$getBookRes=mysql_query($getBookQry);
											
											while($getBookRow=mysql_fetch_array($getBookRes)){
										?>
											<option value="<?php echo stripslashes($getBookRow["salesperson"]);?>"<?php if($sales_person==$getBookRow["salesperson"]) echo " selected";?>><?php echo stripslashes($getBookRow["salesperson"]);?></option>
										<?php
											}
										?>
										
									</select>
									<label><?php if($sales_person_initial!=''){echo $sales_person_initial;}?></label>

								</td>
								
							</tr>
							<tr style="display:none;">
								<td nowrap>
									Sales Manager<font color="red">*</font>:
								</td>
								<td>
									<select name="sales_manager" id="sales_manager" class="inp_feild" style="width:50%;" tabindex="25">
										<option value="0">Select</option>
										<?php
											$getmQry="select * from  tbl_salesmanager order by sales_manager_id asc";
											$getmRes=mysql_query($getmQry);
											while($getmRow=mysql_fetch_array($getmRes)){
											?>
											<option value="<?php echo $getmRow["sales_manager"]?>"<?php if($sales_manager==$getmRow["sales_manager"]) echo " selected";?>><?php echo $getmRow["sales_manager"]?></option>
										<?php
											}
										?>
										
									</select>
									<label><?php if($sales_manager_initial!=''){echo $sales_manager_initial;}?></label>
								</td>
								<?php
								  if($demoId!=""){
										$label="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
										$width1="45%";
										$width2="47%";
									}
									else {
										$width1="43%";
										$width2="44%";
									}
								?>
							</tr>
								<tr>
								<td colspan="2" nowrap>
									<h3><u>DEVELOPMENT INFO:</u></h3>
								</td>
								
							</tr>
							<tr>
							    <td>
							   </td>
							    <td nowrap>
								 IPHONE APP &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $label;?>ANDROID APP
							   </td>
							  
							   </tr>
							 <tr>
							  
								
								<td nowrap>
									Status:
								</td>								
								<td >
									<input type='hidden' name="hdn_development_info_status" value="<?php echo $status ?>">
									<select name="development_info_status" id="development_info_status" style="width:<?php echo $width1; ?>" class="inp_feild" tabindex="26"> 
										<option value="0">Select</option>
										<?php
											$getituneQry="select * from  tbl_status order by display_order asc";
											$getituneRes=mysql_query($getituneQry);
											while($getituneRow=mysql_fetch_array($getituneRes)){
											?>
											<option value="<?php echo $getituneRow["status"]; ?>"<?php if($status==$getituneRow["status"]) echo " selected";?>><?php echo $getituneRow["status"]; ?></option>
										<?php
										}
										?>
										                               
									</select>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;   
									<input type='hidden' name="hdn_development_info_android_status" value="<?php echo $android_status ?>">
									<select name="development_info_android_status" id="development_info_android_status" style="width:<?php echo $width2; ?>" class="inp_feild" tabindex="27"> 
										<option value="0">Select</option>
										<?php
											$getituneQry="select * from  tbl_status order by display_order asc";
											$getituneRes=mysql_query($getituneQry);
											while($getituneRow=mysql_fetch_array($getituneRes)){
											?>
											<option value="<?php echo $getituneRow["status"]; ?>"<?php if($android_status==$getituneRow["status"]) echo " selected";?>><?php echo $getituneRow["status"]; ?></option>
										<?php
										}
										?>
										
									</select>
								</td>
							</tr>
							<tr>
							    
								
								<td nowrap>
									Submitted Date:
								</td>								
								<td >
								    <input type="text" name="iphone_date" id="iphone_date" class="inp_feild" style="width:<?php echo $width1; ?>" value="<?php echo $iphonedate;?>"> 
									&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
									<input type="text" name="android_date" id="android_date" class="inp_feild" style="width:<?php echo $width2; ?>" value="<?php echo $androiddate;?>">
									
								</td>
							</tr>
							<tr>
								<td nowrap>
									Developer's Name:
								</td>
								<td>
									<select name="developer" id="developer" class="inp_feild" tabindex="28">
										<option value="0">Select</option>
										<?php
											$getdeveloperQry="select * from  tbl_developers order by developer_id asc";
											$getdeveloperRes=mysql_query($getdeveloperQry);
											while($getdeveloperRow=mysql_fetch_array($getdeveloperRes)){
											?>
											<option value="<?php echo $getdeveloperRow["developer_name"]?>"<?php if($developer_id==$getdeveloperRow["developer_name"]) echo " selected";?>><?php echo $getdeveloperRow["developer_name"]?></option>
										<?php
											}
										?>
										
									</select>

								</td>
								
							</tr>
							<tr>
								<td style="width:178px" valign='top' nowrap>
									Business Logo:
								</td>
								<td>
									<input type="file" name="business_logo" id="business_logo" class="inp_feild">
									<?php 
									if(is_file("../".$business_logo))
									{
										$ExplodeBLogo = explode(".", $business_logo);
										$BLFileExtenstion = strtolower($ExplodeBLogo[1]);

										if($BLFileExtenstion == 'jpg' || $BLFileExtenstion == 'jpeg' || $BLFileExtenstion == 'gif' || $BLFileExtenstion == 'png')
										{
											?>
											<br><br><img src="<?php echo "../".$business_logo?>" style="width:75px;height:75px;">
											<?php
										}
										else											
										{
											?>
											 <br><br>
											 <a href="<?php echo  HTTP_ROOT_FOLDER.$business_logo;?>" target="_blank" >Download Business Logo</a>
											<?php
										}
											
                                     }												
							 	 	?>																
								</td>
							</tr>
							
							<tr>
								<td style="width:178px" nowrap>
									App Name:
								</td>
								<td>
									<input type="text" name="app_name" id="app_name" class="inp_feild" value="<?php echo $app_name;?>" tabindex="29">
								</td>
							</tr>
							<?php 
							 if($theme!="" && $theme>0)
							 {
								?>
							<tr class="theme_div_demo">
								<td style="width:138px;" valign="top"> 
								</td>
								<td align="left" id="dev_info_design">
								<img src="themes/<?php echo $theme?>.png" style="margin:auto;height:150px;"> 
								 </td>
							</tr>
							<?php 
							  }
							?>
							<tr class="theme_div" style="margin-top:5px;<?php echo $style;?>" nowrap>
								<td style="width:138px;" valign="top"> 
									Design Style<font color="red">*</font>: 
								</td>
								<td>									 
									<select name="dev_info_theme" id="dev_info_theme" disabled class="inp_feild">
										<option value="0">Select</option>
										<?php
										foreach($themes_array as $val=>$name){ 
										  echo '<option value="'.$val.'">'.$name.'</option>'; 
										} 
										?>
										 
									</select>
									<?php 
									 if($theme!="" && $theme>0)
									 {
										?>
									<script type="text/javascript">
									   $("#dev_info_theme").val('<?php echo $theme;?>')
									</script>
									<?php 
									  }
									?>														 
								</td>
							</tr>
							<tr>
								<td nowrap>
									Default Langauage<font color="red">*</font>:
								</td>
								<td>
									<select name="def_language" id="def_language" class="inp_feild" tabindex="30">
										<option value="0"<?php if($default_langauage=="0") echo " selected";?>>Select</option>
										<?php
											$getBookQry="select * from tbl_language";
											$getBookRes=mysql_query($getBookQry);
											
											while($getBookRow=mysql_fetch_array($getBookRes)){
										?>
											<option value="<?php echo stripslashes($getBookRow["language"]);?>"<?php if($default_langauage==$getBookRow["language"]) echo " selected";?>><?php echo stripslashes($getBookRow["language"]);?></option>
										<?php
											}
										?>
										
										
									</select>

								</td>
								
							</tr>
							<tr>
								<td nowrap>
									Store Category:
								</td>
								<td>
									<select name="itunes_category" id="itunes_category" class="inp_feild" tabindex="31">
										<option value="0">Select</option>
										<?php
											$getituneQry="select * from  tbl_itune_category order by category_id asc";
											$getituneRes=mysql_query($getituneQry);
											while($getituneRow=mysql_fetch_array($getituneRes)){
											?>
											<option value="<?php echo $getituneRow["category"]?>"<?php if($itunes_category==$getituneRow["category"]) echo " selected";?>><?php echo $getituneRow["category"]?></option>
										<?php
											}
										?>
										
									</select>

								</td>
								
							</tr>

							<tr>
								<td valign="top" nowrap>
									App Modifications:
								</td>
								<td width="100%">
								   <textarea name="app_modifications" id="app_modifications" class="inp_feild" style="height:100px;font-family: arial;font-size:13px;width:100%;" tabindex="32" ><?php echo $app_modifications;?></textarea> 
								</td>								
							</tr>
							<tr>
								<td valign="top" nowrap>
									Background RGB:
								</td>
								<td>
									<input type="text" name="Background_RGB" id="Background_RGB" class="inp_feild" value="<?php echo $Background_RGB;?>" tabindex="32">
								</td>							
							</tr>		
							 <tr>
								<td style="width:178px" valign='top' nowrap>
									Background Image1:
								</td>
								<td>
									<input type="file" name="background_image" id="background_image" class="inp_feild">
									<?php 
									if(is_file("../".$background_image))
									{
										$ExplodeBGImage = explode(".", $background_image);
										$BGFileExtenstion = strtolower($ExplodeBGImage[1]);

										if($BGFileExtenstion == 'jpg' || $BGFileExtenstion == 'jpeg' || $BGFileExtenstion == 'gif' || $BGFileExtenstion == 'png')
										{
											?>
											<br><br><img src="<?php echo "../".$background_image?>" style="width:75px;height:75px;">
											<?php
										}									
											
                                     }												
							 	 	?>																
								</td>
							</tr>
							<tr>
								<td style="width:178px" valign='top' nowrap>
									Background Image 2:
								</td>
								<td>
									<input type="file" name="background_image2" id="background_image2" class="inp_feild">
									<?php 
									if(is_file("../".$background_image2))
									{
										$ExplodeBGImage2 = explode(".", $background_image2);
										$BGFileExtenstion2 = strtolower($ExplodeBGImage2[1]);

										if($BGFileExtenstion2 == 'jpg' || $BGFileExtenstion2 == 'jpeg' || $BGFileExtenstion2 == 'gif' || $BGFileExtenstion2 == 'png')
										{
											?>
											<br><br><img src="<?php echo "../".$background_image2?>" style="width:75px;height:75px;">
											<?php
										}									
											
                                     }												
							 	 	?>																
								</td>
							</tr>
							<tr>
								<td style="width:178px" valign='top' nowrap>
									Background Image 3:
								</td>
								<td>
									<input type="file" name="background_image3" id="background_image3" class="inp_feild">
									<?php 
									if(is_file("../".$background_image3))
									{
										$ExplodeBGImage3 = explode(".", $background_image3);
										$BGFileExtenstion3 = strtolower($ExplodeBGImage3[1]);

										if($BGFileExtenstion3 == 'jpg' || $BGFileExtenstion3 == 'jpeg' || $BGFileExtenstion3 == 'gif' || $BGFileExtenstion3 == 'png')
										{
											?>
											<br><br><img src="<?php echo "../".$background_image3?>" style="width:75px;height:75px;">
											<?php
										}									
											
                                     }												
							 	 	?>																
								</td>
							</tr>
							<tr>
								<td style="width:178px" valign='top' nowrap>
									Background Image 4:
								</td>
								<td>
									<input type="file" name="background_image4" id="background_image4" class="inp_feild">
									<?php 
									if(is_file("../".$background_image4))
									{
										$ExplodeBGImage4 = explode(".", $background_image4);
										$BGFileExtenstion4 = strtolower($ExplodeBGImage4[1]);

										if($BGFileExtenstion4 == 'jpg' || $BGFileExtenstion4 == 'jpeg' || $BGFileExtenstion4 == 'gif' || $BGFileExtenstion4 == 'png')
										{
											?>
											<br><br><img src="<?php echo "../".$background_image4?>" style="width:75px;height:75px;">
											<?php
										}									
											
                                     }												
							 	 	?>																
								</td>
							</tr>

								<td colspan="2" nowrap>
									<h3><u>PAYMENT INFORMATION</u></h3>
								</td>
								
							</tr>
							<tr>
								<td nowrap>
									Payment Date:
								</td>
								<td>
									<input type="text" name="payment_date" id="payment_date" class="inp_feild" style="width:50%;" value="<?php echo $paymentdate;?>"<?php if($paymentf!="yes"){ echo " readonly";}?> tabindex="31">
								</td>
							</tr>
							<tr>
								<td nowrap>
									Commission Date:
								</td>
								<td>
									<input type="text" name="commision_date" id="commision_date" class="inp_feild" style="width:50%;" value="<?php echo $commisiondate;?>"<?php if($paymentf!="yes"){ echo " readonly";}?> tabindex="32">
								</td>
							</tr>
							<tr>
								<td nowrap>
									Dev QA Date:
								</td>
								<td>
									<input type="text" name="dev_date" id="dev_date" class="inp_feild" style="width:50%;" value="<?php echo $Dev_date;?>"<?php if($paymentf!="yes"){ echo " readonly";}?> tabindex="33">
								</td>
							</tr>
							<?php
							$getPayQry="select * from tbl_paypal where demo_id='".$demoId."' order by paypal_id desc limit 0,1";
							$getPayRes=mysql_query($getPayQry);
							$getPayRow=mysql_fetch_array($getPayRes);
							$call_status=$getPayRow["call_status"];
							if($user_type=="admin"&&$call_status=="paid"){
													
							?>
							<tr>
								<td nowrap>
									Cancel Subsription:
								</td>
								<td>
									<a href="<?php echo HTTP_ROOT_FOLDER."live/cancel_payment.php?appname=".urlencode(trim(stripslashes($getDemoRow["demo_page_url"])))."&app_id=".urlencode(base64_encode($demoId))?>" style="color:white;text-decoration:none;" target="_blank"><?php echo "www.myappyrestaurant.com/cancel_payment.php?app_id=".urlencode(base64_encode($demoId))?></a>
								</td>
							</tr>
							<?php
								
							}
							?>
							
							<tr>
								<td style="width:138px;" valign="top" nowrap>
									Affiliate:
								</td>
								<td>
									<select name="affiliate" id="affiliate" class="inp_feild"  onchange="get_Licensee_details_v1(this.value)"   style="width:50%;" tabindex="34">
										<option value="0">Select</option>
										<?php
										$get_Qry="select * from tbl_affiliate where 1=1 ";
										
										$get_Res=mysql_query($get_Qry);
										while($get_value=mysql_fetch_array($get_Res))
										{ 
											$SelectedString = "";
											$TblAffiliateId = $get_value['affiliate_id'];

											if($TblAffiliateId == $affiliate_id)
												$SelectedString = "selected";

											?>
										<option value="<?php echo $TblAffiliateId;?>" <?php echo $SelectedString;?>><?php echo stripslashes($get_value["full_name"]);?></option>
										<?php
										}
										?>	
									</select>
									
								</td>
							</tr>
							<tr>
									<td valign="top" nowrap>
										Licensee:
									</td>
									<td>
										<select name="licensee" id="licensee" class="input_field" style="width:50%;" tabindex="35" disabled>
												<option value="0">Select</option>					
												<?php
												$getbusinessQry="select * from  tbl_licensee order by licensee_id asc";
												$prepget_business_qry=$DBCONN->prepare($getbusinessQry);
												$prepget_business_qry->execute();
												while($getbusinessRow=$prepget_business_qry->fetch()){
												?>

												<option value="<?php echo $getbusinessRow["licensee_id"];?>" <?php if($Licensee==$getbusinessRow["licensee_id"]) echo " selected";?>><?php echo $getbusinessRow["business_name"];?></option>
												<?php
												}
												?>
											</select>
											<input type="hidden" id="hdn_licensee" name="hdn_licensee" value="<?php echo $Licensee;?>">
									</td>
								</tr>
								 <tr>
										<td nowrap>
											Upload Logo Link:
										</td>
										<td id="upload_logo_link">
											<a href="<?php echo "http://www.myappyrestaurant.com/upload_logo.php?id=".$demoId?>" target="_blank">http://www.myappyrestaurant.com/upload_logo.php?id=<?php echo $demoId;?></a>&nbsp;&nbsp;&nbsp;
										</td>
										<td>
										
											<div class="form_actions" style="text-align:right;" id="upd_pros_btn">
											        <input type="button" value="SEND WELCOME EMAIL" class="uplaod_url" id="send_mail"  onclick="upload_logo_url();">  
											  </div>
										  </form>
										</td>
										
									 </tr>
									<tr>
									<tr>
										 <td colspan="2" nowrap>
											<h3><u>AD-HOC PAYMENT INFORMATION</u></h3>
										 </td>
									</tr>
									<tr>
										<td valign="top" nowrap>
											Item Description:
										</td>
										<td width="100%">
										   <textarea name="item_description" id="item_description" class="inp_feild" style="height:50px;font-family: arial;font-size:13px;width:100%;" tabindex="36" ><?php echo $item_description;?></textarea> 
										</td>								
									</tr>
									 <tr>
										<td nowrap>
											One-off Price:
										</td>
										<td>
											<input type="text" name="one_offprice" id="one_offprice" class="inp_feild" style="width:50%;" value="<?php echo $one_offprice;?>" tabindex="37"> (including tax)
										</td>
									 </tr>
									 <tr>
										<td nowrap>
											Recurring Price:
										</td>
										<td>
											<input type="text" name="recrring_price" id="recrring_price" class="inp_feild" style="width:50%;" value="<?php echo $recrring_price;?>" tabindex="38"> per month (including tax)
										</td>
										
									 </tr>
									  <tr>
										<td nowrap>
											Currency:
										</td>
										<td>
										    <select name="currency" id="currency" class="input_field" style="width:50%;" tabindex="39">
												<option value="0">Select</option>
												<?php
													foreach($currencyCodes as $currency => $code)
													{
												   ?>
													<option value="<?php echo stripslashes($code);?>"<?php if($code==$pro_currency) echo " selected";?>><?php echo stripslashes($currency);?></option>
												<?php
													}
												?>
											</select>
											 <script>$('#currency').val("<?php echo $currency_code?>")</script>
										</td>
										
									 </tr> 
									 <tr>
										<td nowrap>
											Tax Code / Rate(%):
										</td>
										<td>
											<input type="text" name="tax_code" id="tax_code" class="inp_feild" style="width:20%;" value="<?php echo $payment_taxcode;?>" tabindex="40">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
											<input type="text" name="tax_rate" id="tax_rate" class="inp_feild" style="width:20%;" value="<?php echo $payment_taxrate;?>" tabindex="41"> %
										</td>
									 </tr>
									 <!--  <tr>
										<td nowrap>
											Tax Rate(%):
										</td>
										<td>
											<input type="text" name="tax_rate" id="tax_rate" class="inp_feild" style="width:50%;" value="<?php echo $payment_taxrate;?>" tabindex="41"> %
										</td>
										
									 </tr>		 -->							 
									  <tr>
										<td nowrap colspan='2'>
											<input type="button" name="btn_generate_payment_link" id="btn_generate_payment_link" value="GENERATE PAYMENT URL" onclick="generate_payment_url();">
										</td>
										
									 </tr>
									 <tr>
										<td nowrap>
											Payment Page URL:
										</td>
										<td id="payment_page_url">
											
										</td>
										
									 </tr>
									<tr>
									<td colspan="2">
										<table cellspacing="0" cellpadding="0" width="100%" border="0">
											<tr>
												<td>
												  <div class="form_actions">
												     <input type="button" value="Back To Master List" class="add_btn"   onclick="document.location='masterlist.php'">
												</div>
												  
												</td>
												 
											 
												<td>
												
													<div class="form_actions" style="text-align:right;" id="upd_pros_btn">
													      <input type="button" value="<?php echo ucfirst($value);?> Record" class="add_btn add_demo" id="add_demo">  
														  
													</div>
												
												</td>
											</tr>
										</table>
									</td>
								</tr>
								

								
							</table>
								<?php
								if($affiliate_id!=""){
								?>
								<script> 
								get_Licensee_details('<?php echo $affiliate_id;?>');
								</script>
								<?php
								}
								?>
								



						</form>
					</div>
					
				</div>
			</div>
		</div>
		 <style>
			.uplaod_url{
				cursor: pointer;border-radius: 80px;background: #ff4215;
				color: #D9D9D9;
				border-color: #ff4215;
				padding: 5px 5px 5px 5px;
				outline: none;
				font-weight: bold;
				}
				.fancybox-custom .fancybox-skin {
			box-shadow: 0 0 50px #222;
		}
		.fancybox-close {
			position: absolute;
			top: -1px;
			right: -1px;
			width: 36px;
			height: 36px;
			cursor: pointer;
			z-index: 8040;
     }
		</style>
    
	<p class="fancybox" href="#inline1"></p>
	<!-- <div class="modal fade" id="myModal" aria-hidden="true" style="display:none;top:30%;">
	<div class="modal-dialog">
		<div class="modal-content">
			 
			<div class="modal-body" style="tex-align:center;">
			    <div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true" >&times;</button>
					<h4 class="modal-title">&nbsp;&nbsp;&nbsp;&nbsp;</h4>
				</div>
				<div class="modal-body" style="tex-align:center;">
				   <center>
				       <table>
						<tr style="background:white;height:40px;">
						  <td style="color:rgb(142, 180, 227);font-size:13px;text-align:center;" colspan="2">Sorry, cannot send welcome email until you have completed both status fields and both submit date fields</td>
						</tr>
				       </table>
				  </center>
			</div>
			 
		</div><!-- /.modal-content -->
	<!-- </div><!-- /.modal-dialog -->
<!-- </div> -->

	<div id="inline1" style="width:720px;display: none;height: 62px;">
		<p style="color:rgb(142, 180, 227);font-size:15px;text-align:center;padding: 15px;">
			Sorry, cannot send welcome email until you have completed both status fields and both submit date fields
		</p>
	</div>
<?php
include("includes/footer.php");
}
?>
<script>
 $(document).ready(function() {


	  var generate_payment_url="<?php echo $generate_payment_url?>";
	  if(generate_payment_url!=""){
		  $("#payment_page_url").html('<a href="'+generate_payment_url+'" target="_blank">'+generate_payment_url+'</a>');

	  }
			/*
			 *  Simple image gallery. Uses default settings
			 */

			$('.fancybox').fancybox();
});
function generate_payment_url()
{
	var hdn_demo_id			= $("#hdn_demo_id").val();
	var item_description	= $("#item_description").val();
	var oneoffprice			= $("#one_offprice").val();
	var recrringprice		= $("#recrring_price").val();
	var currency			= $("#currency").val();
	var tax_code			= $("#tax_code").val();
	var taxrate				= $("#tax_rate").val();

	$.ajax({
    type: "POST",
    url: "update_adhoc_payments.php",
    data: { demo_id : hdn_demo_id, item_description : item_description, oneoffprice: oneoffprice, recrringprice: recrringprice, currency: currency, tax_code: tax_code, taxrate: taxrate },
    cache: false,
      success: function(result){
        if(result == 'error')
		{
			$("#payment_page_url").html('Error in genenerating payment URL'); 
		}
		else
		{
			var payment_pageurl = 'http://www.myappybusiness.com/adhoc_payment.php?id='+result;
			$("#payment_page_url").html('<a href="'+payment_pageurl+'" target="_blank">'+payment_pageurl+'</a>');
		}
    }
	});

}


function upload_logo_url()
{
	var hdn_demo_id			            = $("#hdn_demo_id").val();
	var development_info_status			= $("#development_info_status").val();
	var development_info_android_status	= $("#development_info_android_status").val();
	var iphone_date			            = $("#iphone_date").val();
	var android_date			        = $("#android_date").val();
	if( hdn_demo_id!="" && development_info_status!="" && iphone_date!="" && android_date!=""){
	$.ajax({
    type: "POST",
    url: "welcome_email.php",
    data: { demo_id : hdn_demo_id},
    cache: false,
      success: function(result){
        if(result == 'error')
		{
			//$("#payment_page_url").html('Error in genenerating payment URL'); 
		}
		else
		{
			//var payment_pageurl = 'http://www.myappybusiness.com/adhoc_payment.php?id='+result;
			//$("#payment_page_url").html('<a href="'+payment_pageurl+'" target="_blank">'+payment_pageurl+'</a>');
		}
      }
	});
	} else {
		$('.fancybox').trigger('click');	
	}

}

function get_Licensee_details_v1(affiliate_id){
			 
			$("#affiliate").val(affiliate_id);
			$.ajax({url:"get_licensee_details.php?affiliate_id="+affiliate_id,success:function(result)
			{
				 if(result!=""){
                   $("#licensee").val(result);
				   $("#hdn_licensee").val(result);				   
				 }
				
				$("#affiliate").val(affiliate_id);
		    }});
		}
$(document).on("blur","#one_offprice,#recrring_price",function(){
	var oneoffprice=$("#one_offprice").val();
	
	var recrringprice=$("#recrring_price").val();
	var taxrate=$("#tax_rate").val();
	if(oneoffprice!=""){
		$("#one_offprice").val(parseFloat(oneoffprice).toFixed(2));
	}
	if(recrringprice!=""){
		$("#recrring_price").val(parseFloat(recrringprice).toFixed(2));

	}
	/*if(taxrate!=""){
		$("#tax_rate").val(parseFloat(taxrate).toFixed(2));

	}*/
	   
});
</script>