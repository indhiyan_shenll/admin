<?php
include("../includes/configure.php");
include("../includes/session_check.php");
function readmore_view($string,$length){
     $string = strip_tags($string);
	 if (strlen($string) > $length) {
		// truncate string
		$stringCut = substr($string, 0, $length);
	   // make sure it ends in a word so assassinate doesn't become ass...
		$string = substr($stringCut, 0, strrpos($stringCut, ' ')).'...'; 
	}
   return $string;
}
$del_id=$_GET["del_id"];
$App_id=$_GET["app_id"];
$sort=$_GET["sort"];
$field=$_GET["field"];
if($sort==""){
	$sort="asc";
}
if($field==""){
	$field="app_id";
}

/*if($field=="title"){
	$fieldname="app_title";
	$order=" order by ".$fieldname." ".$sort;
	if($sort=="asc")
		$dsort="desc";
	   
	else
		$dsort="asc";
	    
}
if($field=="page"){
	$fieldname="product_page";
	$order=" order by ".$fieldname." ".$sort;
	if($sort=="asc")
		$psort="desc";
	    
	else
		$psort="asc";
	    
}*/
if($field=="title"){
	$fieldname="app_title";
	$order=" order by ".$fieldname." ".$sort;
	if($sort=="asc"){
		$dsort="desc";
		$dpath="images/up.png";
	}
	else{
		$dsort="asc";
		$dpath="images/down.png";
	}
}

if($field=="page"){
	$fieldname="product_page";
	$order=" order by ".$fieldname." ".$sort;
	if($sort=="asc"){
		$rsort="desc";
		$rpath="images/up.png";
	}
	else{
		$rsort="asc";
		$rpath="images/down.png";
	}
	
}
if($del_id!=""){
	$deleteQry="delete from  tbl_apps where app_id='".$del_id."'";
	$deleteRes=mysql_query($deleteQry);
	if($deleteRes){
		header("Location:product_list.php");
		exit;
	}
}
 
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<title>MAR Pipeline System</title>
		<link rel="shortcut icon" href="images/Fav.ico" type="image/ico">
		<meta name="Generator" content="EditPlus">
		<meta name="Author" content="">
		<meta name="Keywords" content="">
		<meta name="Description" content="">
		<style>
			body{
				margin:0;
				color:black;
				background:#455A68;
				font-family:arial;
			}
			.header{
				height:70px;
				background:#1C242A;
			}
			.content{
				background:#455A68;
				min-height:600px;
			}
			
			.form_actions{
				padding-top:15px;
				padding-left:5px;
				padding-bottom:30px;
			}
			.form_actions .add_btn{
				cursor:pointer;
				border-radius:0px;
				background:#0D0D0D;
				color:#D9D9D9;
				border-color:#D9D9D9;
				padding:5px 15px 5px 15px;
				font-family:arial;
			}
			.list_content{
				width:950px;
				margin-left:40px;
				/*margin-left:auto;
				margin-right:auto;*/
			}
			.tbl_header th{
				font-size:13px;
				border-bottom:1px solid #D9D9D9;
				text-align:left;
				font-family:arial;
				color:white;
			}
			.tbl-body{
				font-size:12px;
				line-height:25px;
				font-family:arial;
			}
			a{
				color:black;
			}
		</style>
	</head>
	<body>
		<div>
			<div style="margin-left:auto;margin-right:auto;">
				<div class="header">
					<span style="float:right;margin-right:20px;margin-top:5px;"><a href="logout.php" style="color:white;text-decoration:none;">Logout</a></span>
					<img src="images/myappyrestaurants.png" style="margin-top:10px;margin-left:40px;">
				</div>
				<div class="content">
					<div class="list_content">
						<div class="form_actions" style="padding-bottom:50px;">
							<input type="button" value="Back To Admin Features" class="add_btn" onclick="document.location='admin_features.php'" style="float:left;">
							<input type="button" value="Add Product Page" class="add_btn" onclick="document.location='product.php'" style="float:right;">
						</div>
						<div style="padding-bottom:12px;">
							<table cellspacing="0" cellpadding="0" width="100%" class="tbl_header">
								
								<tr>
									 <!-- onclick="document.location='statuslist.php?sort=<?php echo $dsort;?>&field=sno'" -->
                                    <th width="12%" onclick="document.location='product_list.php?sort=<?php echo $rsort;?>&field=page'" style="cursor:pointer">Product Page&nbsp;&nbsp;<?php if($rpath!=""){?><img src="<?php echo $rpath;?>" style="width:10px;height:10px;"><?php }?></th>
									<th width="46%">Description&nbsp;&nbsp;</th>
									<th width="12%">Country</th>
									<th width="12%">Currency</th>
									<th width="8%">Delete?</th>
								</tr>
							</table>
						</div>
						<table cellspacing="0" cellpadding="0" width="100%" class="tbl-body">
						<?php
							$getQry="select * from  tbl_apps".$order;
							//exit;
							$getRes=mysql_query($getQry);
							$getCnt=mysql_num_rows($getRes);
							if($getCnt>0){
								$i=1;
								while($getRow=mysql_fetch_array($getRes)){
									
									if($i%2==1){
										$bgcolor="#a5a5a5";
									}
									else{
										$bgcolor="#d2d1d1";
									}
								 
									 
						?>
							<tr bgcolor="<?php echo $bgcolor;?>">
								<td width="12%"><?php echo stripslashes($getRow["product_page"]);?></td>
								<td width="46%"><?php echo readmore_view(stripslashes($getRow['app_content']),'75');?></td>
								<td width="12%"><?php echo stripslashes($getRow["country"]);?></td>
								<td width="12%"><?php echo array_search(stripslashes($getRow["currency"]), $currencyCodes);?></td>
								<td width="8%"><a href="product.php?app_id=<?php echo $getRow["app_id"];?>">Edit</a>&nbsp;&nbsp;/&nbsp;&nbsp;<a href="product_list.php?del_id=<?php echo $getRow["app_id"];?>" onclick="return confirm('Are you sure want to delete this product?');">Delete</a></td>
							</tr>
							
						<?php
							$i++;
								}
								?>
							
								<?php
							}
							else{
								echo "<tr bgcolor='#a5a5a5'><td colspan=\"3\"><center>No App(s) found.</center></td></tr>";
							}
						?>
						<tr><td height="10px"></td></tr>
							<tr>
								<td colspan="3">
								<div class="form_actions" style="text-align:left;position:relative;" >
								<input type="button" value="Back To Admin Features" class="add_btn" onclick="document.location='admin_features.php'">
								</td>
							</tr>
						</table>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>