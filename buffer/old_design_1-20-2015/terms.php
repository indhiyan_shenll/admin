<?php
include("../includes/configure.php");
include("../includes/session_check.php");
$getQry="select * from  tbl_terms";
$getRes=mysql_query($getQry);
$getRow=mysql_fetch_array($getRes);
$terms_and_condition=stripslashes($getRow["terms_and_condition"]);
$mode="Edit";
$value="Update";
if(isset($_POST["terms_content"])){
	$Terms_content=addslashes(trim($_POST["terms_content"]));
    if($mode=="Edit"){
		$updateQry="update tbl_terms set terms_and_condition='".$Terms_content."',modified_date=now()";
		$updateRes=mysql_query($updateQry);
		if($updateRes){
			header("Location:terms.php");
			exit;
		}
	}
	
}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<title>MAR Pipeline System</title>
		<link rel="shortcut icon" href="images/Fav.ico" type="image/ico">
		<meta name="Generator" content="EditPlus">
		<meta name="Author" content="">
		<meta name="Keywords" content="">
		<meta name="Description" content="">
		<script type="text/javascript" src="js/jquery.js"></script>
		<script>
		var mode="<?php echo $mode;?>";
		</script>
		<script type="text/javascript" src="js/validate.js"></script>
		<script type="text/javascript" src="js/tiny_mce.js"></script>
		<script type="text/javascript">
		tinyMCE.init({
					// General options
					mode : "textareas",
					theme : "advanced",
					plugins : "autolink,lists,spellchecker,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template",

					// Theme options
					theme_advanced_buttons1 : "save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,styleselect,formatselect,fontselect,fontsizeselect",
					theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,|,insertdate,inserttime,preview,|,forecolor,backcolor",
					theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr,|,print,|,ltr,rtl,|,fullscreen",
					theme_advanced_buttons4 : "insertlayer,moveforward,movebackward,absolute,|,styleprops,spellchecker,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,blockquote,pagebreak,|,insertfile,insertimage",
					theme_advanced_toolbar_location : "top",
					theme_advanced_toolbar_align : "left",
					theme_advanced_statusbar_location : "bottom",
					theme_advanced_resizing : true,

					// Skin options
					skin : "o2k7",
					skin_variant : "silver",

					// Example content CSS (should be your site CSS)
					content_css : "css/example.css",

					// Drop lists for link/image/media/template dialogs
					template_external_list_url : "js/template_list.js",
					external_link_list_url : "js/link_list.js",
					external_image_list_url : "js/image_list.js",
					media_external_list_url : "js/media_list.js",

					// Replace values for the template plugin
					template_replace_values : {
							username : "Some User",
							staffid : "991234"
					}
			});
		</script>
		<style>
			body{
				margin:0;
				color:#D9D9D9;
				background:#455A68;
				font-family:arial;
			}
			.header{
				height:70px;
				background:#1C242A;
			}
			.content{
				background:#455A68;
				min-height:600px;
			}
			
			.form_actions{
				padding-top:15px;
				padding-left:5px;
				padding-bottom:30px;
			}
			.form_actions .add_btn{
				cursor:pointer;
				border-radius:0px;
				background:#0D0D0D;
				color:#D9D9D9;
				border-color:#D9D9D9;
				padding:5px 15px 5px 15px;
				font-family:arial;

			}
			.list_content{
				width:950px;
				margin-left:40px;
				/*margin-left:auto;
				margin-right:auto;*/
			}
			.tbl_header th{
				font-size:13px;
				border-bottom:1px solid #D9D9D9;
				text-align:left;
				font-family:arial;
			}
			.tbl-body{
				font-size:12px;
				font-family:arial;
			}
			a{
				color:black;
			}
			.inp_feild{
				border-radius:2px;
				border:none;
				width:100%;
			}
		</style>
	</head>
	<body>
		<div>
			<div style="margin-left:auto;margin-right:auto;">
				<div class="header">
					<span style="float:right;margin-right:20px;margin-top:5px;"><a href="logout.php" style="color:white;text-decoration:none;">Logout</a></span>
					<img src="images/myappyrestaurants.png" style="margin-top:10px;margin-left:40px;">
				</div>
				<div class="content">
					<div class="list_content">
						<table cellspacing="15" cellpadding="0" border="0" width="70%">
							<tr>
								<td>
									<div class="form_actions" style="text-align:left;position:relative;" >
										<input type="button" value="Back To Admin Features" class="add_btn" onclick="document.location='admin_features.php'">
									</div>
								</td>
							</tr>
						</table>
						<h1 style="font-size:25px;padding-top:15px;padding-bottom:15px;margin:0px;">Terms of Service Content</h1>
						<form name="terms_form" id="terms_form" method="post" enctype="multipart/form-data">
						<table cellspacing="15" cellpadding="0" border="0" width="70%">
							<tr>
								<td  valign="top">
									Terms of Service<font color="red">*</font>:
								</td>
								<td>
									<textarea style="width:500px;height:310px;" id="terms_content" name="terms_content"><?php echo $terms_and_condition;?></textarea>
								</td>
							</tr>
							<tr>
							     <td>
									<div class="form_actions" style="text-align:left;position:relative;" >
								<input type="button" value="Back To Admin Features" class="add_btn" onclick="document.location='admin_features.php'">
								</td>
									
								</td>
								<td>
									  <div class="form_actions" style="text-align:right;">
										<input type="button" value="<?php echo $value;?>Terms of Service Content" class="add_btn" id="add_terms_content">
									</div>
								</td>
							</tr>
						</table>
						</form>
					</div>
					
				</div>
			</div>
		</div>
	</body>
</html>