<?php
include("../includes/configure.php");
include("../includes/session_check.php");
$dbdatetime=date('Y-m-d H:i:s',strtotime('now'));
$emailtemplate_id=$_GET["emailtemplate_id"];
if($emailtemplate_id!=""){
	//$emailtemplate_id
	$getQry="select * from   tbl_emailtemplates where emailtemplate_id=:emailtemplate_id";
	$prepgetQry=$DBCONN->prepare($getQry);
	$prepgetQry->execute(array(":emailtemplate_id"=>$emailtemplate_id));
	//$getRes=mysql_query($getQry);
	$getRow=$prepgetQry->fetch();
	$title=stripslashes($getRow["title"]);
	$to_mail=stripslashes($getRow["to_mail"]);
	$from_email=stripslashes($getRow["from_email"]);
	$subject=stripslashes($getRow["subject"]);
	$message=stripslashes($getRow["message"]);
	$message_val=stripslashes($getRow["sms_message"]);
	$mode="Edit";
	$value="Update";
}
else{
	$mode="Add";
	$value="Create";
}
if(isset($_POST["template_title"])){
	$template_title=addslashes(trim($_POST["template_title"]));
	$template_from=addslashes(trim($_POST["template_from"]));
	$template_to=addslashes(trim($_POST["template_to"]));
	$template_subject=addslashes(trim($_POST["template_subject"]));
	$message=addslashes(trim($_POST["message"]));
	$sms_message=addslashes(trim($_POST["sms_message"]));

	
    if($mode=="Edit"){
		//
		//$updateQry="update tbl_emailtemplates set title='".$template_title."',to_mail='".$template_to."',from_email='".$template_from."',subject='".$template_subject."',message='".$message."',modified_date=now() where emailtemplate_id='".$emailtemplate_id."'";

		$prepupdateQry=$DBCONN->prepare("update tbl_emailtemplates set title=:title,to_mail=:to_mail,from_email=:from_email,subject=:subject,message=:message,sms_message=:sms_message,modified_date=:modified_date where emailtemplate_id=:emailtemplate_id");
		$updateRes=$prepupdateQry->execute(array(":title"=>$template_title,":to_mail"=>$template_to,":from_email"=>$template_from,":subject"=>$template_subject,":message"=>$message,":sms_message"=>$sms_message,":modified_date"=>$dbdatetime,":emailtemplate_id"=>$emailtemplate_id));
		//$updateRes=mysql_query($updateQry);
		if($updateRes){
			header("Location:templates.php");
			exit;
		}
	}
	else{
		//$insertQry="insert into  tbl_emailtemplates(title,to_mail,from_email,subject,message,added_date,modified_date) values('".$template_title."','".$template_to."','".$template_from."','".$template_subject."','".$message."',now(),now())";

		$preinsertQry=$DBCONN->prepare("insert into  tbl_emailtemplates(title,to_mail,from_email,subject,message,sms_message,added_date,modified_date) values(:title,:to_mail,:from_email,:subject,:message,:sms_message,:added_date,:modified_date)");
		$insertRes=$preinsertQry->execute(array(":title"=>$template_title,":to_mail"=>$template_to,":from_email"=>$template_from,":subject"=>$template_subject,":message"=>$message,":sms_message"=>$sms_message,":added_date"=>$dbdatetime,":modified_date"=>$dbdatetime));
		//$insertRes=mysql_query($insertQry);
		if($insertRes){
			header("Location:templates.php");
			exit;
		}
	}
}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<title>MAR Pipeline System</title>
		<link rel="shortcut icon" href="images/Fav.ico" type="image/ico">
		<meta name="Generator" content="EditPlus">
		<meta name="Author" content="">
		<meta name="Keywords" content="">
		<meta name="Description" content="">
		<script type="text/javascript" src="js/jquery.js"></script>
		<script>
		$(document).ready(function() {
		var text_max = 160;
		$('#textarea_feedback').html(text_max + ' characters remaining');
        $('#sms_message').keyup(function() {
		var text_length = $('#sms_message').val().length;
		var text_remaining = text_max - text_length;
        $('#textarea_feedback').html(text_remaining + ' characters remaining');
		});
		
		});
		function load(){
        var text_max = 160;
		var text_length = $('#sms_message').val().length;
		var text_remaining = text_max - text_length;
        $('#textarea_feedback').html(text_remaining + ' characters remaining');
		
		
		}
		var mode="<?php echo $mode;?>";
		</script>
		<script type="text/javascript" src="js/validate.js"></script>
		<script type="text/javascript" src="js/tiny_mce.js"></script>
		<script type="text/javascript">
		tinyMCE.init({
					// General options
					mode : "specific_textareas",
					editor_selector : "mceEditor",
					theme : "advanced",
					plugins : "autolink,lists,spellchecker,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template",
					theme_advanced_font_sizes: "10px,12px,13px,14px,16px,18px,20px",
					font_size_style_values : "10px,12px,13px,14px,16px,18px,20px",
                    document_base_url: "http://myappyrestaurant.com/",
					// Theme options
					theme_advanced_buttons1 : "save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,styleselect,formatselect,fontselect,fontsizeselect",
					theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,|,insertdate,inserttime,preview,|,forecolor,backcolor",
					theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr,|,print,|,ltr,rtl,|,fullscreen",
					theme_advanced_buttons4 : "insertlayer,moveforward,movebackward,absolute,|,styleprops,spellchecker,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,blockquote,pagebreak,|,insertfile,insertimage",
					theme_advanced_toolbar_location : "top",
					theme_advanced_toolbar_align : "left",
					theme_advanced_statusbar_location : "bottom",
					theme_advanced_resizing : true,

					// Skin options
					skin : "o2k7",
					skin_variant : "silver",

					// Example content CSS (should be your site CSS)
					content_css : "css/example.css",

					// Drop lists for link/image/media/template dialogs
					template_external_list_url : "js/template_list.js",
					external_link_list_url : "js/link_list.js",
					external_image_list_url : "js/image_list.js",
					media_external_list_url : "js/media_list.js",

					// Replace values for the template plugin
					template_replace_values : {
							username : "Some User",
							staffid : "991234"
					}
			});
		</script>
		<style>
			body{
				margin:0;
				color:#D9D9D9;
				background:#455A68;
				font-family:arial;
			}
			.header{
				height:70px;
				background:#1C242A;
			}
			.content{
				background:#455A68;
				min-height:600px;
			}
			
			.form_actions{
				padding-top:15px;
				padding-left:5px;
				padding-bottom:30px;
			}
			.form_actions .add_btn{
				cursor:pointer;
				border-radius:0px;
				background:#0D0D0D;
				color:#D9D9D9;
				border-color:#D9D9D9;
				padding:5px 15px 5px 15px;
				font-family:arial;

			}
			.list_content{
				width:950px;
				margin-left:40px;
				/*margin-left:auto;
				margin-right:auto;*/
			}
			.tbl_header th{
				font-size:13px;
				border-bottom:1px solid #D9D9D9;
				text-align:left;
				font-family:arial;
			}
			.tbl-body{
				font-size:12px;
				font-family:arial;
			}
			a{
				color:black;
			}
			.inp_feild{
				border-radius:2px;
				border:none;
				width:100%;
			}
		</style>
	</head>
	<body onload="load()">
		<div>
			<div style="margin-left:auto;margin-right:auto;">
				<div class="header">
					<span style="float:right;margin-right:20px;margin-top:5px;"><a href="logout.php" style="color:white;text-decoration:none;">Logout</a></span>
					<img src="images/myappyrestaurants.png" style="margin-top:10px;margin-left:40px;">
				</div>
				<div class="content">
					<div class="list_content">
						<h1 style="font-size:25px;padding-top:15px;padding-bottom:15px;margin:0px;">New Email Message</h1>
						<form name="template_form" id="template_form" method="post" enctype="multipart/form-data">
						<input type="hidden" name="person_form_id" id="person_form_id" value="<?php echo $saleperson_id;?>">
						<table cellspacing="15" cellpadding="0" border="0" width="70%">
							<tr>
								<td style="width:138px;">
									 Title<font color="red">*</font>:
								</td>
								<td>
									<input type="text" name="template_title" id="template_title" class="inp_feild" value="<?php echo $title;?>">
								</td>
							</tr>
							<tr>
								<td style="width:138px;">
									 From<font color="red">*</font>:
								</td>
								<td>
									<input type="text" name="template_from" id="template_from" class="inp_feild" value="<?php if($from_email!="") echo $from_email; else echo "My Appy Restaurant";?>">
								</td>
							</tr>
							<tr>
								<td style="width:138px;">
									 To<font color="red">*</font>:
								</td>
								<td>
									<input type="text" name="template_to" id="template_to" class="inp_feild" value="<?php echo $to_mail;?>"><br>
									<span style="color:gray;font-size:12px;color:white;">[&nbsp;&nbsp;Multiple emails should be seperated by comma (,)&nbsp;&nbsp;]</span>
								</td>
							</tr>
							<tr>
								<td style="width:138px;">
									 Subject<font color="red">*</font>:
								</td>
								<td>
									<input type="text" name="template_subject" id="template_subject" class="inp_feild" value="<?php echo $subject;?>">
								</td>
							</tr>
							<tr>
								<td style="width:138px;" valign="top">
									 Message<font color="red">*</font>:
								</td>
								<td>
									<textarea style="width:400px;height:100px;" class="mceEditor" id="message" name="message"><?php echo $message;?></textarea>
								</td>
							</tr>
							<tr>
								<td style="width:138px;" valign="top">
									 SMS Message<font color="red">*</font>:
								</td>
								<td>
									<textarea  maxlength="160" style="width: 100%;height:110px;" id="sms_message" name="sms_message"><?php echo $message_val;?></textarea><br><div id="textarea_feedback" style="color:red;font-weight:bold;font-size: 15px;"></div>
								</td>
							</tr>
							<tr>
							     <td>
									<div class="form_actions" style="text-align:left;">
										<input type="button" value="Back To Email Messages" class="add_btn"  onclick="document.location='templates.php'">
									
								</td>
								<td>
									  <div class="form_actions" style="text-align:right;">
										<input type="button" value="<?php echo $value;?> Message" class="add_btn" id="add_template">
									</div>
								</td>
							</tr>
						</table>
						</form>
					</div>
					
				</div>
			</div>
		</div>
	</body>
</html>