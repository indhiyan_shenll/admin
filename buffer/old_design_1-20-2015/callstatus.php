<?php
include("../includes/configure.php");
include("../includes/session_check.php");
$dbdatetime=date('Y-m-d H:i:s',strtotime('now'));
$callstatus_id=$_GET["callstatus_id"];
if($callstatus_id!=""){
	//$callstatus_id
	$getQry="select * from tbl_callstatus where callstatus_id=:callstatus_id";
	$prepgetQry=$DBCONN->prepare($getQry);
	$prepgetQry->execute(array(":callstatus_id"=>$callstatus_id));
	//$getRes=mysql_query($getQry);
	$getRow=$prepgetQry->fetch();
	$callstatus=stripslashes($getRow["callstatus"]);
	$Callstatus_color=stripslashes($getRow["status_color"]);
	$mode="Edit";
	$value="Update";
}
else{
	$mode="Add";
	$value="Create";
}
if(isset($_POST["callstatus"])){
	$callstatus=addslashes(trim($_POST["callstatus"]));
	$call_satuscolor=addslashes(trim($_POST["call_stauts_color"]));
	if($mode=="Edit"){
		//$callstatus   $call_satuscolor    $callstatus_id
		$updateQry="update tbl_callstatus set callstatus=:callstatus,status_color=:status_color,modified_date=:modified_date where callstatus_id=:callstatus_id";
		$prepupdateQry=$DBCONN->prepare($updateQry);
		$updateRes=$prepupdateQry->execute(array(":callstatus"=>$callstatus,":status_color"=>$call_satuscolor,":modified_date"=>$dbdatetime,":callstatus_id"=>$callstatus_id));

		//$updateRes=mysql_query($updateQry);
		if($updateRes){
			header("Location:callstatuslist.php");
			exit;
		}
	}
	else{
		 $sqlqry="select max(display_order) as display_order from  tbl_callstatus";
		$sql=mysql_query($sqlqry);
		if(mysql_num_rows($sql)>0)
		{
			$fetch=mysql_fetch_array($sql);
			$display_order=$fetch["display_order"];
		}
		//$callstatus  $call_satuscolor $display_order
		$insertQry="insert into tbl_callstatus(callstatus,status_color,display_order,added_date,modified_date) values(:callstatus,:status_color,:display_order,:added_date,:modified_date)";
		$prepisnertQry=$DBCONN->prepare($insertQry);
		$insertRes=$prepisnertQry->execute(array(":callstatus"=>$callstatus,":status_color"=>$call_satuscolor,":display_order"=>$display_order,":added_date"=>$dbdatetime,":modified_date"=>$dbdatetime));
		//$insertRes=mysql_query($insertQry);
		if($insertRes){
			header("Location:callstatuslist.php");
			exit;
		}
	}
}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<title>MAR Pipeline System</title>
		<link rel="shortcut icon" href="images/Fav.ico" type="image/ico">
		<meta name="Generator" content="EditPlus">
		<meta name="Author" content="">
		<meta name="Keywords" content="">
		<meta name="Description" content="">
		<link rel="stylesheet" media="screen" type="text/css" href="css/colorpicker.css" />
		<script type="text/javascript" src="js/jquery.js"></script>
		<script type="text/javascript" src="js/colorpicker.js"></script>
		<script>
		var mode="<?php echo $mode;?>";
		</script>
		<script type="text/javascript" src="js/validate.js"></script>
		<style>
			body{
				margin:0;
				color:#D9D9D9;
				background:#455A68;
				font-family:arial;
			}
			.header{
				height:70px;
				background:#1C242A;
			}
			.content{
				background:#455A68;
				min-height:600px;
			}
			
			.form_actions{
				padding-top:15px;
				padding-left:5px;
				padding-bottom:30px;
			}
			.form_actions .add_btn{
				cursor:pointer;
				border-radius:0px;
				background:#0D0D0D;
				color:#D9D9D9;
				border-color:#D9D9D9;
				padding:5px 15px 5px 15px;
				font-family:arial;

			}
			.list_content{
				width:950px;
				margin-left:40px;
				/*margin-left:auto;
				margin-right:auto;*/
			}
			.tbl_header th{
				font-size:13px;
				border-bottom:1px solid #D9D9D9;
				text-align:left;
				font-family:arial;
			}
			.tbl-body{
				font-size:12px;
				font-family:arial;
			}
			a{
				color:black;
			}
			.inp_feild{
				border-radius:2px;
				border:none;
				width:100%;
			}
		</style>
	</head>
	<body>
		<div>
			<div style="margin-left:auto;margin-right:auto;">
				<div class="header">
					<span style="float:right;margin-right:20px;margin-top:5px;"><a href="logout.php" style="color:white;text-decoration:none;">Logout</a></span>
					<img src="images/myappyrestaurants.png" style="margin-top:10px;margin-left:40px;">
				</div>
				<div class="content">
					<div class="list_content">
						<h1 style="font-size:25px;padding-top:15px;padding-bottom:15px;margin:0px;">New CallStatus</h1>
						<form name="callstatus_form" id="callstatus_form" method="post">
						<input type="hidden" name="callstatus_form_id" id="callstatus_form_id" value="<?php echo $callstatus_id;?>">
						<table cellspacing="15" cellpadding="0" border="0" width="70%">
							<tr>
								<td style="width:138px;">
									 Callstatus<font color="red">*</font>:
								</td>
								<td>
									<input type="text" name="callstatus" id="callstatus" class="inp_feild" value="<?php echo $callstatus;?>">
								</td>
							</tr>
							<tr>
								<td style="width:138px;">
									 Callstatus color<font color="red">*</font>: 
								</td>
								<td>
									<input type="text" name="call_stauts_color" id="call_stauts_color" class="inp_feild" value="<?php echo $Callstatus_color;?>">
									<SCRIPT LANGUAGE="JavaScript">
									$('#call_stauts_color').ColorPicker({
									eventName:'focus',
									 onSubmit: function(hsb, hex, rgb, el) {
									  $(el).val(hex);
									  $(el).ColorPickerHide();
									 },
									  onBeforeShow: function () {
									   $(this).ColorPickerSetColor(this.value);
									  }
									 })
									 .bind('keyup', function(){
									  $(this).ColorPickerSetColor(this.value);
									 });
									</SCRIPT>
								</td>
							</tr>
							<tr>
							     <td>
									<div class="form_actions" style="text-align:left;">
										<input type="button" value="Back to Call Status List" class="add_btn"  onclick="document.location='callstatuslist.php'">
									
								</td>
								<td>
									  <div class="form_actions" style="text-align:right;">
										<input type="button" value="<?php echo $value;?> Call Status" class="add_btn" id="add_callstatus">
									</div>
								</td>
							</tr>
						</table>
						</form>
					</div>
					
				</div>
			</div>
		</div>
	</body>
</html>