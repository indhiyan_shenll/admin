<?php
include("../includes/configure.php");
include("../includes/session_check.php");

$Ituneid=$_GET["itune_id"];
$dbdatetime=date('Y-m-d H:i:s',strtotime('now'));

if($Ituneid!=""){
	//$Ituneid
	$geItuneQry="select * from  tbl_itune_category where category_id=:tunesid";
	$prepgetItuneQry=$DBCONN->prepare($geItuneQry);
	$prepgetItuneQry->execute(array(":tunesid"=>$Ituneid));
	//$getItuneRes=mysql_query($geItuneQry);
	$getItuneRow=$prepgetItuneQry->fetch();
	$Itune_name=stripslashes($getItuneRow["category"]);
	$mode="Edit";
	$value="Upadte";
}
else{
	$mode="Add";
	$value="Create";
}
if(isset($_POST["itune_name"])){
	$Itune_name=addslashes(trim($_POST["itune_name"]));
    if($mode=="Edit"){
		//$Itune_name  $Ituneid
		$updateQry="update  tbl_itune_category set category=:category,modified_date=:modified_date where category_id=:category_id";
		$prepupdateQry=$DBCONN->prepare($updateQry);
		$updateRes=$prepupdateQry->execute(array(":category"=>$Itune_name,":modified_date"=>$dbdatetime,":category_id"=>$Ituneid));
		//$updateRes=mysql_query($updateQry);
		if($updateRes){
			header("Location:Itunelist.php");
			exit;
		}
	}
	else{
		//$Itune_name
		$insertQry="insert into tbl_itune_category(category,added_date,modified_date)values(:category,:added_date,:modified_date)";
		$prepinsertQry=$DBCONN->prepare($insertQry);
		$insertRes=$prepinsertQry->execute(array(":category"=>$Itune_name,":added_date"=>$dbdatetime,":modified_date"=>$dbdatetime));
		//$insertRes=mysql_query($insertQry);
		if($insertRes){
			header("Location:Itunelist.php");
			exit;
		}
	}
}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<title>MAR Pipeline System</title>
		<link rel="shortcut icon" href="images/favicon.png" type="image/png">
		<meta name="Generator" content="EditPlus">
		<meta name="Author" content="">
		<meta name="Keywords" content="">
		<meta name="Description" content="">
		<script type="text/javascript" src="js/jquery.js"></script>
		<script>
		var mode="<?php echo $mode;?>";
		</script>
		<script type="text/javascript" src="js/validate.js"></script>
		<style>
			body{
				margin:0;
				color:#D9D9D9;
				background:#455A68;
				font-family:arial;
			}
			.header{
				height:70px;
				background:#1C242A;
			}
			.content{
				background:#455A68;
				min-height:600px;
			}
			
			.form_actions{
				padding-top:15px;
				padding-left:5px;
				padding-bottom:30px;
			}
			.form_actions .add_btn{
				cursor:pointer;
				border-radius:0px;
				background:#0D0D0D;
				color:#D9D9D9;
				border-color:#D9D9D9;
				padding:5px 15px 5px 15px;
				font-family:arial;

			}
			.list_content{
				width:950px;
				margin-left:40px;
				/*margin-left:auto;
				margin-right:auto;*/

			}
			.tbl_header th{
				font-size:13px;
				border-bottom:1px solid #D9D9D9;
				text-align:left;
				font-family:arial;
			}
			.tbl-body{
				font-size:12px;
				font-family:arial;
			}
			a{
				color:black;
			}
			.inp_feild{
				border-radius:2px;
				border:none;
				width:100%;
			}
		</style>
	</head>
	<body>
		<div>
			<div style="margin-left:auto;margin-right:auto;">
				<div class="header">
					<span style="float:right;margin-right:20px;margin-top:5px;"><a href="logout.php" style="color:white;text-decoration:none;">Logout</a></span>
					<img src="images/myappyrestaurants.png" style="margin-top:10px;margin-left:40px;">
				</div>
				<div class="content">
					<div class="list_content">
						<h1 style="font-size:25px;padding-top:15px;padding-bottom:15px;margin:0px;">New 
						Itune Category </h1>
						<form name="itune_form" id="itune_form" method="post" enctype="multipart/form-data">
						<input type="hidden" name="hdn_tune_id" id="hdn_tune_id" value="<?php echo $Ituneid;?>">
						<table cellspacing="15" cellpadding="0" border="0" width="70%">
							<tr>
								<td style="width:138px;">
									Itunes Category: 
								</td>
								<td>
									<input type="text" name="itune_name" id="itune_name" class="inp_feild" value="<?php echo $Itune_name;?>">
								</td>
							</tr>
							
							<tr>
							     <td>
									<div class="form_actions" style="text-align:left;">
										<input type="button" value="Back To iTune List" class="add_btn"  onclick="document.location='Itunelist.php'">
									
								</td>
								<td>
									  <div class="form_actions" style="text-align:right;">
										<input type="button" value="<?php echo $value;?> Itune Category" class="add_btn" id="add_tunelist">
									</div>
								</td>
							</tr>
						</table>
						</form>
					</div>
					
				</div>
			</div>
		</div>
	</body>
</html>