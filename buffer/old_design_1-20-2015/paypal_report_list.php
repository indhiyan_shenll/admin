<?php
include("../includes/configure.php");
include("../includes/session_check.php");
if(isset($_POST["search_name"])||isset($_POST["search_start_date"])||isset($_POST["search_end_date"])){
	$search_name=trim(mysql_real_escape_string($_POST['search_name']));
	$search_start_date=$_POST['search_start_date'];
	$search_end_date=$_POST['search_end_date'];
	
    if($search_name!=""){
       $condition.="  and concat(first_name,' ',last_name)  like '%".$search_name."%'";
    }
	/*if($search_name!=""){
      $condition.=" and demo_id in(select demo_id from  tbl_demo where demo_id!='' and concat(first_name,'',sur_name)  like '%".$search_name."%')";
    }*/
	
	if($search_start_date!=""&&$search_end_date!=""){
      $condition.=" and (paid_on>='".date('Y-m-d',strtotime($search_start_date))."' and paid_on<='".date('Y-m-d H:i:s',strtotime($search_end_date))."')";
    }
	if($search_start_date!=""&&$search_end_date==""){
		$condition.=" and paid_on>='".date('Y-m-d',strtotime($search_start_date))."'";
	}
	if($search_start_date==""&&$search_end_date!=""){
		$condition.=" and paid_on<='".date('Y-m-d',strtotime($search_end_date))."'";
	}
   
 }
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<title>MAR Pipeline System</title>
		<link rel="shortcut icon" href="images/Fav.ico" type="image/ico">
		<meta name="Generator" content="EditPlus">
		<meta name="Author" content="">
		<meta name="Keywords" content="">
		<meta name="Description" content="">

		<style>
			body{
				margin:0;
				color:black;
				background:#455A68;
				font-family:arial;
			}
			.header{
				height:70px;
				background:#1C242A;
			}
			.content{
				background:#455A68;
				min-height:600px;
			}
			
			.form_actions{
				padding-top:15px;
				padding-left:5px;
				padding-bottom:30px;
			}
			.form_actions .add_btn{
				cursor:pointer;
				border-radius:0px;
				background:#0D0D0D;
				color:#D9D9D9;
				border-color:#D9D9D9;
				padding:5px 15px 5px 15px;
				font-family:arial;
			}
			.list_content{
				width:950px;
				margin-left:40px;
				/*margin-left:auto;
				margin-right:auto;*/
			}
			.tbl_header th{
				font-size:13px;
				border-bottom:1px solid #D9D9D9;
				text-align:left;
				font-family:arial;
				color:white;
			}
			.tbl-body{
				font-size:12px;
				line-height:25px;
				font-family:arial;
			}
			a{
				color:black;
			}
		</style>
		 <script src="js/jquery-1.7.2.min.js"></script>
			 <script src="js/jquery-ui-1.8.21.custom.min.js"></script>
		<link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css" rel="stylesheet" type="text/css"/>
 		<link href="css/jquery.datetimepicker.css" rel="stylesheet" type="text/css"/> 
		 <script type="text/javascript" src="js/jquery.datetimepicker.js"></script> 
		<script>
		$(function() {
					$("#search_start_date").datetimepicker({ format:'d-m-Y',formatDate:'d-m-Y',timepicker:false});
					$("#search_end_date").datetimepicker({ format:'d-m-Y',formatDate:'d-m-Y',timepicker:false});
					
										
			 });
			function searchoption(){
				var search_name=$.trim($("#search_name").val());
				var startdate=$.trim($("#search_start_date").val());
				var enddate=$.trim($("#search_end_date").val());
				var startdateday=startdate.split("-")[0];
				var startdatemonth=startdate.split("-")[1];
				var startdateyear=startdate.split("-")[2];
				var enddateday=enddate.split("-")[0];
				var enddatemonth=enddate.split("-")[1];
				var enddateyear=enddate.split("-")[2];
				startdate=new Date(startdateyear, startdatemonth-1, startdateday);
				enddate=new Date(enddateyear, enddatemonth-1, enddateday);
				if($.trim($("#search_name").val())==""&&$.trim($("#search_start_date").val())==""&&$.trim($("#search_end_date").val())==""){

				alert('Please fill atleast one field to search');
				document.getElementById("search_name").focus();
				 return false;
				}
				else if(startdate.getTime()>enddate.getTime())  {
				   alert('Please select payment end date is greater then or equal to payment start date');
				 
					document.getElementById("search_start_date").focus();
						return false;
				}
				else{
					$("#report_list").submit();
				}

	    }
	  </script>
	</head>
	<body>
	
		<div>
			<div style="margin-left:auto;margin-right:auto;">
				<div class="header">
					<span style="float:right;margin-right:20px;margin-top:5px;"><a href="logout.php" style="color:white;text-decoration:none;">Logout</a></span>
					<img src="images/myappyrestaurants.png" style="margin-top:10px;margin-left:40px;">
				</div>
				<div class="content">
					<div class="list_content">
						<div class="form_actions" style="padding-bottom:50px;">
							<input type="button" value="Back To Admin Features" class="add_btn" onclick="document.location='admin_features.php'" style="float:left;">
							
						</div>
                         <div style="color:white;font-size:13px;font-weight:bold;">
						 <form name="report_list" id="report_list" method="post">

							<table cellspacing="0" cellpadding="0" width="100%" border="0" style="border:1px solid #D9D9D9;" >
							    <tr height="30px;" BGCOLOR="gray">
									<td colspan="4" align="left"><span style="">&nbsp;&nbsp;Filter Paypal Report</span>
									</td>
										
								</tr>
								<tr height="40px;">
									<td align="right">Payer Name:</td>
									<td align="left"><input type="text" name="search_name" id="search_name" class="inp_feild" value="<?php echo stripslashes($search_name);?>" style="width:95%"></td>
									<td align="right">Payment Start Date:</td>
									<td align="left"><input type="text" name="search_start_date" id="search_start_date" class="inp_feild" value="<?php echo stripslashes($search_start_date);?>" style="width:95%" readonly></td>
										
								</tr>
								
								<tr height="40px;">
									<td align="right">Payment End Date:</td>
									<td align="left"><input type="text" name="search_end_date" id="search_end_date" class="inp_feild" value="<?php echo stripslashes($search_end_date);?>" style="width:95%" readonly></td>
									<td align="right"></td>
									<td align="left"></td>
										
								</tr>
								<tr height="40px;">
									<td colspan="2" align="right">
									<div class="form_actions" style="padding:0px;">
							            <input type="button" class="add_btn"  value="Search" onclick="searchoption()" style="width:90px;margin-right: 8px;">
							       </div>
									</td>
									<td align="left"  colspan="2">
									    <div class="form_actions" style="padding:0px;">
							            <input type="button" value="Cancel" class="add_btn" onclick="document.location='paypal_report_list.php'" style="width:90px;margin-left: 8px;">
							       </div>
									
									
									
									</td>
										
								</tr>
								</table>
								</form>
							</div>
						<div style="padding-bottom:12px;">
                          <div style="height:50px;"></div>
							<table cellspacing="0" cellpadding="0" width="100%" class="tbl_header">
								
								<tr>
									<th width="10%">Date</th>
									<th width="20%">Payer Name&nbsp;&nbsp;</th>
									<th width="20%">Product Name&nbsp;&nbsp;</th>									
									<th width="15%">Payment Amount</th>
									<th width="15%">Licensee</th>
									<th width="10%">Affiliate</th>
								</tr>
							</table>
						</div>
						<table cellspacing="0" cellpadding="0" width="100%" class="tbl-body">
						<?php
						$getQry="select * from  tbl_paypal where paypal_id!='' and first_name!='' and last_name!='' and p_amount!=''".$condition."order by paypal_id desc";
							$getRes=mysql_query($getQry);
							$getCnt=mysql_num_rows($getRes);
							if($getCnt>0){
								$i=1;
								$j=0;

								while($getRow=mysql_fetch_array($getRes)){
									
									
									$paid_date="";
                                    if($getRow["paid_on"]!=""){
										if($getRow["paid_on"]!="0000-00-00 00:00:00"){
											$paid_date=date('d-m-Y',strtotime(stripslashes($getRow["paid_on"])));
										}
										else{
											$paid_date="";
										}
									}
									$payer_name=stripslashes($getRow["first_name"]." ".$getRow["last_name"]);
									$demo_id=$getRow["demo_id"];
									$getdQry="select * from tbl_demo where demo_id='".$demo_id."'";
									$getdRes=mysql_query($getdQry);
									$getdRow=mysql_fetch_array($getdRes);
									//$payer_name=stripslashes($getdRow["first_name"].$getdRow["sur_name"]);
                                     $affiliate_id=$getdRow["a_id"];
                                     $getafQry="select * from tbl_affiliate where affiliate_id='".$affiliate_id."'";
									 $getafRes=mysql_query($getafQry);
									 $getafRow=mysql_fetch_array($getafRes);
								     $affiliate_name=stripslashes($getafRow["full_name"]);
									 $licecnece_id=stripslashes($getafRow["l_id"]);
                                     $getliQry="select * from tbl_licensee where licensee_id='".$licecnece_id."'";
                                     $getliRes=mysql_query($getliQry);
									 $getliRow=mysql_fetch_array($getliRes);
									 $li_name=stripslashes($getliRow["full_name"]);
                                     $payment_amount='$'.stripslashes($getRow["t_amount"].$getRow["currency"]);

                            if($getRow["paid_on"]!="0000-00-00  00:00:00"&&stripslashes($getRow["t_amount"]!="0")
								&&stripslashes($getRow["t_amount"]!="")){
                              
                             if($paid_date!=""&&$payer_name!=""&&$payment_amount!=""){
                                 
								 if($j%2==0){
										$bgcolor="#a5a5a5";
									}
									else{
										$bgcolor="#d2d1d1";
									}

						?>
							<tr bgcolor="<?php echo $bgcolor;?>">
								<td width="10%"><?php echo $paid_date;?></td>
								<td width="20%"><?php echo $payer_name;?></td>
								<td width="20%">
									<?php
								     $getproductQry="select * from tbl_apps where app_id='".$getRow["product_id"]."'";
                                     $getproductQryres=mysql_query($getproductQry);
									 $getproductQryrow=mysql_fetch_array($getproductQryres);
									 $product_name=stripslashes($getproductQryrow["app_title"]);						
									echo $product_name;?>
								</td>								
								<td width="15%"><?php echo $payment_amount;?></td>
								<td width="15%"><?php echo $li_name;?></td>
								<td width="10%"><?php echo $affiliate_name;?></td>
							</tr>
							  
						<?php
						$j++;
							 }
						       }
							$i++;
								}
								?>
							
								<?php
							}
							else{
								echo "<tr bgcolor='#a5a5a5'><td colspan=\"3\"><center>No Report(s) found.</center></td></tr>";
							}
						?>
						<tr><td height="10px"></td></tr>
							<tr>
								<td colspan="3">
								<div class="form_actions" style="text-align:left;position:relative;" >
								<input type="button" value="Back To Admin Features" class="add_btn" onclick="document.location='admin_features.php'">
								</td>
							</tr>
						</table>
					</div>
				</div>
			</div>
		</div>
		
	</body>
</html>