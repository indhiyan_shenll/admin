<?php
include("../includes/configure.php");
include("../includes/session_check.php");
$emailtemplate_id=$_GET["emailtemplate_id"];
$sort=$_GET["sort"];
$field=$_GET["field"];
if($sort==""){
	$sort="asc";
}
if($field==""){
	$field="sales_person_id";
}
if($field=="sno"){
	$fieldname="sales_person_id";
	$order=" order by ".$fieldname." ".$sort;
	if($sort=="asc"){
		$dsort="desc";
		$dpath="images/up.png";
	}
	else{
		$dsort="asc";
		$dpath="images/down.png";
	}
}
if($field=="person_name"){
	$fieldname="salesperson";
	$order=" order by ".$fieldname." ".$sort;
	if($sort=="asc"){
		$rsort="desc";
		$rpath="images/up.png";
	}
	else{
		$rsort="asc";
		$rpath="images/down.png";
	}
}
if($emailtemplate_id!=""){
	$deleteQry="delete from  tbl_emailtemplates  where emailtemplate_id='".$emailtemplate_id."'";
	$deleteRes=mysql_query($deleteQry);
	if($deleteRes){
		header("Location:templates.php");
		exit;
	}
}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<title>MAR Pipeline System</title>
		<link rel="shortcut icon" href="images/Fav.ico" type="image/ico">
		<meta name="Generator" content="EditPlus">
		<meta name="Author" content="">
		<meta name="Keywords" content="">
		<meta name="Description" content="">
		<style>
			body{
				margin:0;
				color:black;
				background:#455A68;
				font-family:arial;
			}
			.header{
				height:70px;
				background:#1C242A;
			}
			.content{
				background:#455A68;
				min-height:600px;
			}
			
			.form_actions{
				padding-top:15px;
				padding-left:5px;
				padding-bottom:30px;
			}
			.form_actions .add_btn{
				cursor:pointer;
				border-radius:0px;
				background:#0D0D0D;
				color:#D9D9D9;
				border-color:#D9D9D9;
				padding:5px 15px 5px 15px;
				font-family:arial;
			}
			.list_content{
				width:950px;
				margin-left:40px;
				/*
				margin-left:auto;
				margin-right:auto;
				*/
			}
			.tbl_header th{
				font-size:13px;
				border-bottom:1px solid #D9D9D9;
				text-align:left;
				font-family:arial;
				color:white;
			}
			.tbl-body{
				font-size:12px;
				line-height:25px;
				font-family:arial;
			}
			a{
				color:black;
			}
		</style>
	</head>
	<body>
		<div>
			<div style="margin-left:auto;margin-right:auto;">
				<div class="header">
					<span style="float:right;margin-right:20px;margin-top:5px;"><a href="logout.php" style="color:white;text-decoration:none;">Logout</a></span>
					<img src="images/myappyrestaurants.png" style="margin-top:10px;margin-left:40px;">
				</div>
				<div class="content">
					<div class="list_content">
						<div class="form_actions" style="padding-bottom:50px;">
							<input type="button" value="Back To Admin Features" class="add_btn" onclick="document.location='admin_features.php'" style="float:left;">
							<input type="button" value="Add Message" class="add_btn" onclick="document.location='edit_template.php'" style="float:right;">
						</div>
						<div style="padding-bottom:12px;">
							<table cellspacing="0" cellpadding="0" width="100%" class="tbl_header">
								
								<tr>
									<th width="5%">No</th>
									<th width="15%">Title&nbsp;&nbsp;</th>
									<th width="25%">From</th>
									<th width="25%">To</th>
									<th width="20%">Subject</th>
									<th width="10%">Action</th>
								</tr>
							</table>
						</div>
						<table cellspacing="0" cellpadding="0" width="100%" class="tbl-body">
						<?php
							$getlangQry="select * from   tbl_emailtemplates".$order;
							//exit;
							$getlangRes=mysql_query($getlangQry);
							$getlangCnt=mysql_num_rows($getlangRes);
							if($getlangCnt>0){
								$i=1;
								while($getlangRow=mysql_fetch_array($getlangRes)){
									$to_email=str_replace(',','<br>',stripslashes($getlangRow["to_mail"]));
									$to_email=str_replace(';','<br>',$to_email);
									if($i%2==1){
										$bgcolor="#a5a5a5";
									}
									else{
										$bgcolor="#d2d1d1";
									}
						?>
							<tr bgcolor="<?php echo $bgcolor;?>">
								<td width="5%"><?php echo $i;?></td>
								<td width="15%"><?php echo stripslashes($getlangRow["title"]);?></td>
								<td width="25%"><?php echo stripslashes($getlangRow["from_email"]);?></td>
								<td width="25%"><?php echo $to_email;?></td>
								<td width="20%"><?php echo stripslashes($getlangRow["subject"]);?></td>
								<td width="10%"><a href="edit_template.php?emailtemplate_id=<?php echo $getlangRow["emailtemplate_id"];?>">Edit</a>&nbsp;&nbsp;/&nbsp;&nbsp;<a href="templates.php?emailtemplate_id=<?php echo $getlangRow["emailtemplate_id"];?>" onclick="return confirm('Are you sure want to delete this message?')">Delete</a></td>
							</tr>
							
						<?php
							$i++;
								}
								?>
							<tr><td height="10px"></td></tr>
							
								<?php
							}
							else{
								echo "<tr bgcolor='#a5a5a5'><td colspan=\"6\"><center>No Template(s) found.</center></td></tr>";
							}
						?>
						<tr>
								<td colspan="6">
								<div class="form_actions" style="text-align:left;position:relative;">
								<input type="button" value="Back To Admin Features" class="add_btn" onclick="document.location='admin_features.php'">
								</td>
							</tr>
						</table>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>