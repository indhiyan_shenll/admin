<?php
include("../includes/configure.php");
include("../includes/session_check.php");
$country_id=$_GET["country_id"];
$dbdatetime=date('Y-m-d H:i:s',strtotime('now'));

if($country_id!=""){
	//$country_id
	$getQry="select * from   tbl_country where country_id=:country_id";
	$prepgetQry=$DBCONN->prepare($getQry);
	$prepgetQry->execute(array(":country_id"=>$country_id));
	$getRow=$prepgetQry->fetch();
	//$getRes=mysql_query($getQry);
	//$getRow=mysql_fetch_array($getRes);
	$country=stripslashes($getRow["country"]);
	$mode="Edit";
	$value="Update";
}
else{
	$mode="Add";
	$value="Create";
}
if(isset($_POST["country"])){
	$Country=addslashes(trim($_POST["country"]));
    if($mode=="Edit"){
		//$Country    $country_id
		$updateQry="update  tbl_country set country=:country,modified_date=:modified_date where country_id=:countryid";
		$prepupdateQry=$DBCONN->prepare($updateQry);
		$updateRes=$prepupdateQry->execute(array(":country"=>$Country,":modified_date"=>$dbdatetime,":countryid"=>$country_id));
		//$updateRes=mysql_query($updateQry);
		if($updateRes){
			header("Location:country_list.php");
			exit;
		}
	}
	else{
		//$Country 
		$insertQry="insert into tbl_country(country,added_date,modified_date) values(:country,:added_date,:modified_date)";
		//$insertRes=mysql_query($insertQry);
		$prepinsqry=$DBCONN->prepare($insertQry);
		$insertRes=$prepinsqry->execute(array(":country"=>$Country,":added_date"=>$dbdatetime,":modified_date"=>$dbdatetime));
		if($insertRes){
			header("Location:country_list.php");
			exit;
		}
	}
}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<title>MAR Pipeline System</title>
		<link rel="shortcut icon" href="images/Fav.ico" type="image/ico">
		<meta name="Generator" content="EditPlus">
		<meta name="Author" content="">
		<meta name="Keywords" content="">
		<meta name="Description" content="">
		<script type="text/javascript" src="js/jquery.js"></script>
		<script>
		var mode="<?php echo $mode;?>";
		</script>
		<script type="text/javascript" src="js/validate.js"></script>
		<style>
			body{
				margin:0;
				color:#D9D9D9;
				background:#455A68;
				font-family:arial;
			}
			.header{
				height:70px;
				background:#1C242A;
			}
			.content{
				background:#455A68;
				min-height:600px;
			}
			
			.form_actions{
				padding-top:15px;
				padding-left:5px;
				padding-bottom:30px;
			}
			.form_actions .add_btn{
				cursor:pointer;
				border-radius:0px;
				background:#0D0D0D;
				color:#D9D9D9;
				border-color:#D9D9D9;
				padding:5px 15px 5px 15px;
				font-family:arial;

			}
			.list_content{
				width:950px;
				margin-left:40px;
				/*margin-left:auto;
				margin-right:auto;*/
			}
			.tbl_header th{
				font-size:13px;
				border-bottom:1px solid #D9D9D9;
				text-align:left;
				font-family:arial;
			}
			.tbl-body{
				font-size:12px;
				font-family:arial;
			}
			a{
				color:black;
			}
			.inp_feild{
				border-radius:2px;
				border:none;
				width:100%;
			}
		</style>
	</head>
	<body>
		<div>
			<div style="margin-left:auto;margin-right:auto;">
				<div class="header">
					<span style="float:right;margin-right:20px;margin-top:5px;"><a href="logout.php" style="color:white;text-decoration:none;">Logout</a></span>
					<img src="images/myappyrestaurants.png" style="margin-top:10px;margin-left:40px;">
				</div>
				<div class="content">
					<div class="list_content">
						<h1 style="font-size:25px;padding-top:15px;padding-bottom:15px;margin:0px;">New 
						Country </h1>
						<form name="country_form" id="country_form" method="post" enctype="multipart/form-data">
						<input type="hidden" name="country_form_id" id="country_form_id" value="<?php echo $country_id;?>">
						<table cellspacing="15" cellpadding="0" border="0" width="70%">
							<tr>
								<td style="width:138px;">
									Country: 
								</td>
								<td>
									<input type="text" name="country" id="country" class="inp_feild" value="<?php echo $country;?>">
								</td>
							</tr>
							
							<tr>
							     <td>
									<div class="form_actions" style="text-align:left;">
										<input type="button" value="Back To Country List" class="add_btn"  onclick="document.location='country_list.php'">
									
								</td>
								<td>
									  <div class="form_actions" style="text-align:right;">
										<input type="button" value="<?php echo $value;?> Country" class="add_btn" id="add_country">
									</div>
								</td>
							</tr>
						</table>
						</form>
					</div>
					
				</div>
			</div>
		</div>
	</body>
</html>