<?php
include("../includes/configure.php");
include("../includes/session_check.php");
$dbdatetime=date('Y-m-d H:i:s',strtotime('now'));
$App_id=$_GET["app_id"];
if($App_id!=""){
	//$App_id
	$getQry="select * from  tbl_apps where app_id=:app_id";
	$prepgetQry=$DBCONN->prepare($getQry);
	$prepgetQry->execute(array(":app_id"=>$App_id));
	//$getRes=mysql_query($getQry);
	$getRow=$prepgetQry->fetch();
	$App_title=stripslashes($getRow["app_title"]);
	$App_content=stripslashes($getRow["app_content"]);
	$promo_code=stripslashes($getRow["promo_code"]);
	$initial_price=stripslashes($getRow["initial_price"]);
	$monthly_price=stripslashes($getRow["monthly_price"]);
	$country=stripslashes($getRow["country"]);
	$product_page_val=stripslashes($getRow["product_page"]);
	$pro_currency=stripslashes($getRow["currency"]);
	$prepaid_months=stripslashes($getRow["prepaid_months"]);
	$retail_upfront_price=stripslashes($getRow["retail_upfront_price"]);
	$retail_monthly_price=stripslashes($getRow["retail_monthly_price"]);
	$submission_price=stripslashes($getRow["submission_price"]);
    $retail_submission_price=stripslashes($getRow["retail_submission_price"]); 
	$mode="Edit";
	$value="Update";
}
else{
	$mode="Add";
	$value="Create";
	$getQry_1="select max(product_page) as product_page from  tbl_apps";
	$prepgetQry1=$DBCONN->prepare($getQry_1);
	$prepgetQry1->execute(array(":callstatus_id"=>$callstatus_id));
	$getRow1=$prepgetQry1->fetch();
	$page_value=stripslashes($getRow1["product_page"]);
	$product_page_val=$page_value+1;
}
if(isset($_POST["app_content"])){
	$title="";
	$app_content=addslashes(trim($_POST["app_content"]));
	$promo_code="";
	$intial_price=addslashes(trim($_POST["intial_price"]));
	$monthly_price=addslashes(trim($_POST["monthly_price"]));
	$product_country=addslashes(trim($_POST["product_country"]));
    $product_currency=addslashes(trim($_POST["product_currency"]));
	$prepaid_months=addslashes(trim($_POST["prepaid_months"]));
	$retail_upfront_price=addslashes(trim($_POST["retail_upfront_price"]));
	$retail_monthly_price=addslashes(trim($_POST["retail_monthly_price"]));
	$submission_price=addslashes(trim($_POST["submission_price"]));
	$retail_submission_price=addslashes(trim($_POST["retail_submission_price"]));
    if($mode=="Edit"){
		//$title   nl2br($app_content)   $promo_code    $intial_price    $monthly_price    $product_country  $product_currency    $prepaid_months   $App_id
		$updateQry="update tbl_apps set app_title=:app_title,app_content=:app_content,promo_code=:promo_code,initial_price=:initial_price,monthly_price=:monthly_price,modified_date=:modified_date,country=:country,currency=:currency,prepaid_months=:prepaid_months,retail_upfront_price=:retail_upfront_price,retail_monthly_price=:retail_monthly_price,submission_price=:submission_price,retail_submission_price=:retail_submission_price where app_id=:app_id";
		$prepupdateQry=$DBCONN->prepare($updateQry);
		$updateRes=$prepupdateQry->execute(array(":app_title"=>$title,":app_content"=>nl2br($app_content),":promo_code"=>$promo_code,":initial_price"=>$intial_price,":monthly_price"=>$monthly_price,":modified_date"=>$dbdatetime,":country"=>$product_country,":currency"=>$product_currency,":prepaid_months"=>$prepaid_months,":retail_upfront_price"=>$retail_upfront_price,":retail_monthly_price"=>$retail_monthly_price,":submission_price"=>$submission_price,":retail_submission_price"=>$retail_submission_price,":app_id"=>$App_id));
	  if($updateRes){
			header("Location:product_list.php");
			exit;
		}
	}
	else{

		$getQry_1="select max(product_page) as product_page from  tbl_apps";
		$prepgetQry1=$DBCONN->prepare($getQry_1);
		$prepgetQry1->execute(array(":callstatus_id"=>$callstatus_id));
	    $getRow1=$prepgetQry1->fetch();
        $page_value=stripslashes($getRow1["product_page"]);
        $product_page=$page_value+1;
      //$title   nl2br($app_content)     $promo_code  $intial_price   $monthly_price   $product_country   $product_currency
		$insertQry="insert into tbl_apps(app_title,app_content,promo_code,initial_price,monthly_price,country,currency,added_date,modified_date,prepaid_months,product_page,retail_upfront_price,retail_monthly_price,submission_price,retail_submission_price) values(:app_title,:app_content,:promo_code,:initial_price,:monthly_price,:country,:currency,:added_date,:modified_date,:prepaid_months,:product_page,:retail_upfront_price,:retail_monthly_price,:submission_price,:retail_submission_price)";
		$prepinsertQry=$DBCONN->prepare($insertQry);
		$insertRes=$prepinsertQry->execute(array(":app_title"=>$title,":app_content"=>nl2br($app_content),":promo_code"=>$promo_code,":initial_price"=>$intial_price,":monthly_price"=>$monthly_price,":country"=>$product_country,":currency"=>$product_currency,":added_date"=>$dbdatetime,":modified_date"=>$dbdatetime,":product_page"=>$product_page,":prepaid_months"=>$prepaid_months,":retail_upfront_price"=>$retail_upfront_price,":retail_monthly_price"=>$retail_monthly_price,":submission_price"=>$submission_price,":retail_submission_price"=>$retail_submission_price,));
		//$insertRes=mysql_query($insertQry);
		if($insertRes){
			header("Location:product_list.php");
			exit;
		}
	}
}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<title>MAR Pipeline System</title>
		<link rel="shortcut icon" href="images/Fav.ico" type="image/ico">
		<meta name="Generator" content="EditPlus">
		<meta name="Author" content="">
		<meta name="Keywords" content="">
		<meta name="Description" content="">
		<link href="css/jquery-ui-1.10.3.custom.css" rel="stylesheet">
		<link href="css/inputosaurus.css" rel="stylesheet">
		 
		<script type="text/javascript" src="js/jquery.js"></script>
		<script src="js/jquery-ui-1.10.3.custom.js"></script>
		<!-- <script type="text/javascript" src="js/inputosaurus.js"></script> -->
		<script type="text/javascript" src="js/prettify.js"></script>
		<script>
		var mode="<?php echo $mode;?>";
		 /* $(function() {
			$('#promo_code').inputosaurus({
				width : '350px',
				autoCompleteSource : ['alabama', 'illinois', 'kansas', 'kentucky', 'new york'],
				activateFinalResult : true,
				change : function(ev){
					$('#promo_code').val(ev.target.value);
				}
			});
		 });*/
		</script>
		<script type="text/javascript" src="js/validate.js"></script>
		<style>
			body{
				margin:0;
				color:#D9D9D9;
				background:#455A68;
				font-family:arial;
			}
			.header{
				height:70px;
				background:#1C242A;
			}
			.content{
				background:#455A68;
				min-height:600px;
			}
			
			.form_actions{
				padding-top:15px;
				padding-left:5px;
				padding-bottom:30px;
			}
			.form_actions .add_btn{
				cursor:pointer;
				border-radius:0px;
				background:#0D0D0D;
				color:#D9D9D9;
				border-color:#D9D9D9;
				padding:5px 15px 5px 15px;
				font-family:arial;

			}
			.list_content{
				width:950px;
				margin-left:40px;
				/*margin-left:auto;
				margin-right:auto;*/
			}
			.tbl_header th{
				font-size:13px;
				border-bottom:1px solid #D9D9D9;
				text-align:left;
				font-family:arial;
			}
			.tbl-body{
				font-size:12px;
				font-family:arial;
			}
			a{
				color:black;
			}
			.inp_feild{
				border-radius:2px;
				border:none;
				width:100%;
			}
		</style>
	</head>
	<body>
		<div>
			<div style="margin-left:auto;margin-right:auto;">
				<div class="header">
					<span style="float:right;margin-right:20px;margin-top:5px;"><a href="logout.php" style="color:white;text-decoration:none;">Logout</a></span>
					<img src="images/myappyrestaurants.png" style="margin-top:10px;margin-left:40px;">
				</div>
				<div class="content">
					<div class="list_content">
						<h1 style="font-size:25px;padding-top:15px;padding-bottom:15px;margin:0px;">New Product Page</h1>
						<form name="app_form" id="app_form" method="post" enctype="multipart/form-data">
						<input type="hidden" name="hdn_prom" id="hdn_prom" value="<?php echo $App_id;?>">
						<input type="hidden" name="hdn_id" id="hdn_id" value="<?php echo $App_id;?>">
						<input type="hidden" name="hdn_page" id="hdn_page" value="<?php echo $App_id;?>">
						<table cellspacing="15" cellpadding="0" border="0" width="80%">
						 <tr>
								<td valign="top">
								Product Page:
								</td>
								<td>
									<input type="text" name="retail_upfront_price" id="retail_upfront_price" class="inp_feild"   style="width:50%"   value="<?php echo $product_page_val;?>" readonly>  
									
								</td>
							</tr>
                           	<tr>
								<td valign="top">
									Description<font color="red">*</font>:
								</td>
								<td>
									<textarea  id="app_content" rows="10" cols="20" class="inp_feild" name="app_content" style="width:50%;"><?php echo str_replace("<br />","&#013;",$App_content);?></textarea>
									
								</td>
							</tr>
							<tr>
								<td style="width:180px;">
									 Country<font color="red">*</font>:
								</td>
								<td>
									<select name="product_country" id="product_country" class="inp_feild" style="width:50%">
										
										<option value="0">Select</option>
										<?php
											$getcountrQry="select * from  tbl_country";
											$getcountryRes=mysql_query($getcountrQry);
											
											while($getcountryRow=mysql_fetch_array($getcountryRes)){
										   ?>
											<option value="<?php echo stripslashes($getcountryRow["country"]);?>"<?php if($country==$getcountryRow["country"]) echo " selected";?>><?php echo stripslashes($getcountryRow["country"]);?></option>
										<?php
											}
										?>
									</select>
								</td>
							</tr>
								<tr>
								<td style="width:117px;">
									 Currency<font color="red">*</font>:
								</td>
								<td>
									<select name="product_currency" id="product_currency" class="inp_feild" style="width:50%">
										
										<option value="0">Select</option>
										<?php
											foreach($currencyCodes as $currency => $code){
										   ?>
											<option value="<?php echo stripslashes($code);?>"<?php if($code==$pro_currency) echo " selected";?>><?php echo stripslashes($currency);?></option>
										<?php
											}
										?>
									</select>
								</td>
							</tr>
						   
							
							<tr>
								<td>
									 
								</td>
								<td>
									<table cellspacing="0" cellpadding="0" width="50%" border="0">
										<tbody><tr>
											<td width="50%">
												<u><b>RETAIL</b></u>

											</td>
											<td align="left" width="50%">
												&nbsp;&nbsp;<u><b>PROMO</b></u>
											</td>
										</tr>
									</tbody></table>
								</td>
							</tr>
							<tr>
								<td>
									Customisation Fee<font color="red">*</font>:
								</td>
								<td>
									<table cellspacing="0" cellpadding="0" width="50%" border="0">
										<tbody><tr>
											<td width="50%">
												<input type="text" name="retail_upfront_price" id="retail_upfront_price" class="inp_feild"   style="width:97%"   value="<?php echo $retail_upfront_price;?>"> 
											</td>
											<td align="right" width="50%">
												<input type="text" name="intial_price" id="intial_price" class="inp_feild" value="<?php echo $initial_price;?>" style="width:97%">
											</td>
										</tr>
									</tbody></table>
								</td>
							</tr>
							<tr>
								<td>
									Submission Fee<font color="red">*</font>:
								</td>
								<td>
									<table cellspacing="0" cellpadding="0" width="50%" border="0">
										<tbody><tr>
											<td width="50%">
												<input type="text" name="retail_submission_price" id="retail_submission_price" class="inp_feild"   style="width:97%"   value="<?php echo $retail_submission_price;?>">  
											</td>
											<td align="right" width="50%">
												 <input type="text" name="submission_price" id="submission_price" class="inp_feild"   style="width:97%"   value="<?php echo $submission_price;?>"> 
											</td>
										</tr>
									</tbody></table>
								</td>
							</tr>
							<tr>
								<td>
									Monthly Fee<font color="red">*</font>:
								</td>
								<td>
									<table cellspacing="0" cellpadding="0" width="50%" border="0">
										<tbody><tr>
											<td width="50%">
													<input type="text" name="retail_monthly_price" id="retail_monthly_price" class="inp_feild"   style="width:97%"   value="<?php echo $retail_monthly_price;?>">  
											</td>
											<td align="right" width="50%">
												<input type="text" name="monthly_price" id="monthly_price" class="inp_feild" value="<?php echo $monthly_price;?>" style="width:97%">
											</td>
										</tr>
									</tbody></table>
								</td>
							</tr>
							<tr>
								<td valign="top">
									Pre-paid Months<font color="red">*</font>:
								</td>
								<td>
									<input type="text" name="prepaid_months" id="prepaid_months" class="inp_feild" style="width:24%" onkeypress="return onlyNumbers(event)" maxlength="2" value="<?php echo $prepaid_months;?>"> Months
									
								</td>
							</tr>
							
							 <tr>
							     <td>
									<div class="form_actions" style="text-align:left;">
										<input type="button" value="Back to Product Page List" class="add_btn"  onclick="document.location='product_list.php'">
									
								</td>
								<td>
									  <div class="form_actions" style="text-align:right;">
										<input type="button" value="<?php echo $value;?> Product Page" class="add_btn" id="add_app">
									</div>
								</td>
							</tr>
						</table>
						</form>
					</div>
					
				</div>
			</div>
		</div>
	</body>
</html>