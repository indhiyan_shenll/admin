<?php
include("../includes/configure.php");
$statusid=$_GET["staus_id"];
if($statusid!=""){
	$getQry="select * from  tbl_status where status_id='".$statusid."'";
	$getRes=mysql_query($getQry);
	$getRow=mysql_fetch_array($getRes);
	$status=stripslashes($getRow["status"]);
    $Status_color=stripslashes($getRow["status_color"]);
	$email=stripslashes($getRow["email"]);
	$mode="Edit";
	$value="Update";
}
else{
	$mode="Add";
	$value="Create";
}
if(isset($_POST["stauts"])){
	$status=addslashes(trim($_POST["stauts"]));
	$satuscolor=addslashes(trim($_POST["stauts_color"]));
	$Email=addslashes(trim($_POST["email"]));
    if($mode=="Edit"){
		$updateQry="update  tbl_status set status='".$status."',status_color='".$satuscolor."',email='".$Email."',modified_date=now() where status_id='".$statusid."'";
		$updateRes=mysql_query($updateQry);
		if($updateRes){
			header("Location:statuslist.php");
			exit;
		}
	}
	else{
        $sqlqry="select max(display_order) as display_order from  tbl_status";
		$sql=mysql_query($sqlqry);
		if(mysql_num_rows($sql)>0)
		{
			$fetch=mysql_fetch_array($sql);
			$display_order=$fetch["display_order"];
		}

	   $insertQry="insert into tbl_status(status,status_color,email,display_order,added_date,modified_date)values('".$status."','".$satuscolor."','".$Email."'
		,'".++$display_order."',now(),now())";
		
		$insertRes=mysql_query($insertQry);
		if($insertRes){
			header("Location:statuslist.php");
			exit;
		}
	}
}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<title>MAR Pipeline System</title>
		<link rel="shortcut icon" href="images/Fav.ico" type="image/ico">
		<meta name="Generator" content="EditPlus">
		<meta name="Author" content="">
		<meta name="Keywords" content="">
		<meta name="Description" content="">
		<link rel="stylesheet" media="screen" type="text/css" href="css/colorpicker.css" />
		
		<script type="text/javascript" src="js/jquery.js"></script>
		<script type="text/javascript" src="js/colorpicker.js"></script>
		<script>
		var mode="<?php echo $mode;?>";
		
		</script>
		<script type="text/javascript" src="js/validate.js"></script>
		<style>
			body{
				margin:0;
				color:#D9D9D9;
				background:#455A68;
				font-family:arial;
			}
			.header{
				height:70px;
				background:#1C242A;
			}
			.content{
				background:#455A68;
				min-height:600px;
			}
			
			.form_actions{
				padding-top:15px;
				padding-left:5px;
				padding-bottom:30px;
			}
			.form_actions .add_btn{
				cursor:pointer;
				border-radius:0px;
				background:#0D0D0D;
				color:#D9D9D9;
				border-color:#D9D9D9;
				padding:5px 15px 5px 15px;
				font-family:arial;

			}
			.list_content{
				width:950px;
				margin-left:40px;
				/*margin-left:auto;
				margin-right:auto;*/
			}
			.tbl_header th{
				font-size:13px;
				border-bottom:1px solid #D9D9D9;
				text-align:left;
				font-family:arial;
			}
			.tbl-body{
				font-size:12px;
				font-family:arial;
			}
			a{
				color:black;
			}
			.inp_feild{
				border-radius:2px;
				border:none;
				width:100%;
			}
		</style>
	</head>
	<body>
		<div>
			<div style="margin-left:auto;margin-right:auto;">
				<div class="header">
					<span style="float:right;margin-right:20px;margin-top:5px;"><a href="logout.php" style="color:white;text-decoration:none;">Logout</a></span>
					<img src="images/myappyrestaurants.png" style="margin-top:10px;margin-left:40px;">
				</div>
				<div class="content">
					<div class="list_content">
						<h1 style="font-size:25px;padding-top:15px;padding-bottom:15px;margin:0px;">New 
						status </h1>
						<form name="status_form" id="status_form" method="post" enctype="multipart/form-data">
						<input type="hidden" name="status_form_id" id="status_form_id" value="<?php echo $statusid;?>">
						<table cellspacing="15" cellpadding="0" border="0" width="70%">
							<tr>
								<td style="width:138px;">
									status: 
								</td>
								<td>
									<input type="text" name="stauts" id="stauts" class="inp_feild" value="<?php echo $status;?>">
								</td>
							</tr>
							<tr>
								<td style="width:138px;">
									status color: 
								</td>
								<td>
									<input type="text" name="stauts_color" id="stauts_color" class="inp_feild" value="<?php echo $Status_color;?>">
									<SCRIPT LANGUAGE="JavaScript">
									$('#stauts_color').ColorPicker({
									 eventName:'focus',
									 onSubmit: function(hsb, hex, rgb, el) {
									  $(el).val(hex);
									  $(el).ColorPickerHide();
									 },
									  onBeforeShow: function () {
									   $(this).ColorPickerSetColor(this.value);
									  }
									 })
									 .bind('keyup', function(){
									  $(this).ColorPickerSetColor(this.value);
									 });
									</SCRIPT>
								</td>
							</tr>
							<tr>
								<td style="width:138px;">
									Email: 
								</td>
								<td>
									<input type="text" name="email" id="email" class="inp_feild" value="<?php echo $email;?>"><br>
									<span style="color:gray;font-size:12px;color:white;">[&nbsp;&nbsp;Multiple emails should be seperated by comma (,)&nbsp;&nbsp;]</span>
								</td>
							</tr>
							<tr>
							     <td>
									<div class="form_actions" style="text-align:left;">
										<input type="button" value="Back To Status List" class="add_btn"  onclick="document.location='statuslist.php'">
									
								</td>
								<td>
									  <div class="form_actions" style="text-align:right;">
										<input type="button" value="<?php echo $value;?> status" class="add_btn" id="add_status">
									</div>
								</td>
							</tr>
						</table>
						</form>
					</div>
					
				</div>
			</div>
		</div>
	</body>
</html>