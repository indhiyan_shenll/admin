<?php
$parent_directory=basename(dirname($_SERVER["PHP_SELF"]));
$filename=basename($_SERVER["PHP_SELF"]);
include("../includes/configure.php");
include("includes/cmm_functions.php");
include("../includes/session_check.php");
$id=$_SESSION['user_id'];	
$feat="download_list";
$typeandfeature=checklogin($id,$feat);
$usrArr=explode("*",$typeandfeature);
$user_type=$usrArr[0];
$feature=$usrArr[1];
$trialformfeature=$usrArr[4];
$masterlistfeature=$usrArr[5];


//exit;
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<title>MAR Pipeline System</title>
		<link rel="shortcut icon" href="images/Fav.ico" type="image/ico">
		<meta name="Generator" content="EditPlus">
		<meta name="Author" content="">
		<meta name="Keywords" content="">
		<meta name="Description" content="">
		<style>
			body{
				margin:0;
				color:black;
				background:#455A68;
				font-family:arial;
			}
			.header{
				height:70px;
				background:#1C242A;
			}
			.content{
				background:#455A68;
				min-height:600px;
			}
			
			.form_actions{
				padding-top:15px;
				padding-left:5px;
				padding-bottom:30px;
			}
			.form_actions .add_btn{
				cursor:pointer;
				border-radius:0px;
				background:#0D0D0D;
				color:#D9D9D9;
				border-color:#D9D9D9;
				padding:5px 15px 5px 15px;
				font-family:arial;
				min-width:155px;
			}
			.list_content{
				width:950px;
				margin-left:40px;
				/*margin-left:auto;
				margin-right:auto; */
			}
			.tbl_header th{
				font-size:13px;
				border-bottom:1px solid #D9D9D9;
				text-align:left;
				color:white;
			}
			.tbl-body{
				font-size:12px;
				line-height:25px;
			}
			a{
				color:black;
			}
			.tdbtn{
			  padding-left:20px;
			}
		</style>
	</head>
	<body>
	<form name="demo_list" method="post">
	<input type="hidden" name="HiddenMode" id="HiddenMode" value="">
	<input type="hidden" name="HdnPage" id="HdnPage" value="">
		<div>
			<div style="margin-left:auto;margin-right:auto;">
				<div class="header">
					<span style="float:right;margin-right:20px;margin-top:5px;"><a href="logout.php" style="color:white;text-decoration:none;">Logout</a></span>
					<div style="border:1 px solid red;width:34%;float:left;"><img src="images/myappyrestaurants.png" style="margin-top:10px;margin-left:40px;"></div>
					<div style="float:right;border:1 px solid red;width:65%;margin-top:15px;"><font style="font-family:arial;color:white;size:40px;" size="5px;">Admin Features</font></div>
					
				</div>
				<div class="content">
					<div class="list_content">
					<div>
				
			<table cellspacing="0" cellpadding="0" border="0" style="margin-top:50px;">
			<tr>
				<td class="tdbtn">
				</td>
				<td class="tdbtn">
					<div class="form_actions" style="padding:0px;">
						
						<input type="button" value="Take-away List" class="add_btn" onclick="document.location='take_list.php'" style="width:195px;">
						
					</div>
				</td>
				<td class="tdbtn">
				   <div class="form_actions" style="padding:0px;">
							
									  <input type="button" value="Status List" class="add_btn" onclick="document.location='statuslist.php'" style="width:195px;">
							
						</div>

				</td>
				<td  class="tdbtn">
					<div class="form_actions" style="padding:0px;">
						<?php
						if($_SESSION['user_id']=="3"){
						?>
						<input type="button" value="Manage Users" class="add_btn" onclick="document.location='userslist.php'" style="width:195px;">
						<?php
						   }
						?>
					</div>
				</td>
			</tr>
			<tr>
				<td class="tdbtn"></td>
				 <td class="tdbtn">
					 <div class="form_actions" style="padding:0px;">
							
							 <input type="button" value="Book a Table List" class="add_btn" onclick="document.location='book-a-table.php'" style="width:195px;">
							
						</div>
				</td>
				<td class="tdbtn">
					
					<div class="form_actions" style="padding:0px;">
							
							 <input type="button" value="Email Messages" class="add_btn" onclick="document.location='templates.php'" style="width:195px;">
							
						</div> 
					
				</td>
				<td class="tdbtn">
					 <div class="form_actions" style="padding:0px;">
							<?php
							if($_SESSION['user_id']=="3"){
							?>
										<input type="button" value="Download DB Dump" class="add_btn" onclick='funcdbexport()' style="width:195px;">
							<?php
							   }
							?>
						</div> 

				</td>
			</tr>
			<tr>
				<td class="tdbtn"></td>
				<td class="tdbtn">
						 <div class="form_actions" style="padding:0px;">
							
									<input type="button" value="Country List" class="add_btn" onclick="document.location='country_list.php'" style="width:195px;">
							
						</div>
				</td>
				<td class="tdbtn">
					 <div class="form_actions" style="padding:0px;">
							
										<input type="button" value=" Business Type List" class="add_btn" onclick="document.location='business_list.php'" style="width:195px;">
							
						</div>
				</td>
				<td class="tdbtn">
					<div class="form_actions" style="padding:0px;">
							<?php
							if($_SESSION['user_id']=="3"){
							?>
									<input type="button" value="Paypal Report" class="add_btn" onclick="document.location='paypal_report_list.php'" style="width:195px;">
							<?php
							   }
							?>
						</div>
						
				</td>
			</tr>
			<tr>
				<td class="tdbtn"></td>
				<td class="tdbtn">
						 <div class="form_actions" style="padding:0px;">
							
										<input type="button" value="Language List" class="add_btn" onclick="document.location='language_list.php'" style="width:195px;">
							
						</div>
				</td>
				<td class="tdbtn">
					
						<div class="form_actions" style="padding:0px;">
							
									<input type="button" value="Current Specials" class="add_btn" onclick="document.location='special_one_list.php'" style="width:195px;">
							
						</div>
				
				</td>
				<td class="tdbtn">
						<div class="form_actions" style="padding:0px;">
							<?php
							if($_SESSION['user_id']=="3"){
							?>
									  <input type="button" value="Import New Prospecting List" class="add_btn" onclick="document.location='importexcel.php'" style="width:195px;">
							<?php
							   }
							?>
						</div>
				</td>
			</tr>
			<tr>
				<td class="tdbtn"></td>
				<td class="tdbtn">
					   <div class="form_actions" style="padding:0px;">
							
									<input type="button" value="iTunes category List" class="add_btn" onclick="document.location='Itunelist.php'" style="width:195px;">
							
						</div>

				</td>
				<td class="tdbtn">
					<div class="form_actions" style="padding:0px;">
						
						<input type="button" value="Testimonial List" class="add_btn" onclick="document.location='testimonial_list.php'" style="width:195px;"> 
						
					</div>
				</td>
				<td class="tdbtn">
					<div class="form_actions" style="padding:0px;">
							<?php
								if($_SESSION['user_id']=="3"){
							?>
									 <input type="button" value="Download Master List" class="add_btn" onclick="funexport('masterlist');" style="width:195px;">
							<?php
							   }
							?>
						</div>
				</td>
			</tr>
			<tr>
				<td class="tdbtn"></td>
				<td class="tdbtn">
						   <div class="form_actions" style="padding:0px;">
							
									<input type="button" value="Salesperson List" class="add_btn" onclick="document.location='person_list.php'" style="width:195px;">
							
						</div>
				</td>
				<td class="tdbtn">
				 
					<div class="form_actions" style="padding:0px;">
							
								<input type="button" value="Enquiry List" class="add_btn" onclick="document.location='enquiry_list.php'" style="width:195px;">
							
						</div>				
				
				</td>
				<td class="tdbtn">
					<!-- <div class="form_actions" style="padding:0px;">
							<?php
							if($user_type=="Admin"){
							?>
									<input type="button" value="Promo Code List" class="add_btn" onclick="document.location='promocodelist.php'" style="width:195px;">
							<?php
							   }
							?>
						</div> -->
						<div class="form_actions" style="padding:0px;">
							<?php
								if($_SESSION['user_id']=="3"){
							?>
									<input type="button" value="Download Prospecting List" class="add_btn" onclick="funexport('prospectslist');" style="width:195px;">
							<?php
							   }
							?>
						</div>
				</td>	 
			</tr>
			<tr>
				<td class="tdbtn">
				
				   <div class="form_actions" style="padding:0px;">
							<?php
							$getQry="select * from tbl_users where user_id='".$id."'";
							$getRes=mysql_query($getQry);
							$getRow=mysql_fetch_array($getRes);
							$usertype=$getRow["user_type"];
							$licensee_page=$getRow["licensee_list"];
							$affiliate_page=$getRow["alliance_list"];
							if($licensee_page=="yes"||$user_type=="Admin"){
							?>
									  <input type="button" value="Back to Licensee List" class="add_btn" onclick="document.location='licensee_list.php'" style="width:195px;">
							<?php
							   }
							?>
						</div>
				
				
				</td>
				<td class="tdbtn">
					<div class="form_actions" style="padding:0px;">
							
									<input type="button" value="Sales Manager List" class="add_btn" onclick="document.location='manager_list.php'" style="width:195px;">
							
						</div>
				</td>
				<td class="tdbtn">
				
				 
				<div class="form_actions" style="padding:0px;">
							
									<input type="button" value="About Us Content" class="add_btn" onclick="document.location='aboutus.php'" style="width:195px;">
							
						</div>
				
				
				
				
				
				
				</td>
				<td class="tdbtn">
				     
					<?php
					if($_SESSION['user_id']=="3" || $user_type=="Admin"){
					?>
                      <div class="form_actions" style="padding:0px;">
					 <input type="button" value="Email List" class="add_btn" onclick="document.location='mail_list.php'" style="width:195px;"> 
					 </div>
					<?php
					}
					?>
				
				
				
				
				</td>
			</tr>
			<tr>
				<td class="tdbtn">
				  <div class="form_actions" style="padding:0px;">
							<?php
							$getQry="select * from tbl_users where user_id='".$id."'";
							$getRes=mysql_query($getQry);
							$getRow=mysql_fetch_array($getRes);
							$usertype=$getRow["user_type"];
							$licensee_page=$getRow["licensee_list"];
							$affiliate_page=$getRow["alliance_list"];
							if($affiliate_page=="yes"||$user_type=="Admin"){
							?>
									  <input type="button" value="Back to Affiliate List" class="add_btn" onclick="document.location='affiliate_list.php'" style="width:195px;">
							<?php
							   }
							?>
						</div>
				
				
				
				</td>
				<td class="tdbtn">
					<div class="form_actions" style="padding:0px;">
							
									 <input type="button" value="Developer List" class="add_btn" onclick="document.location='developerlist.php'" style="width:195px;">
							
						</div>
				
				</td>
				<td class="tdbtn">
					
						<div class="form_actions" style="padding:0px;">
							
									 <input type="button" value="Terms of Service" class="add_btn" onclick="document.location='terms.php'" style="width:195px;">
							
						</div>
				</td>
				<td class="tdbtn">
				
				
				
				</td>
			</tr>
			<tr>
				<td class="tdbtn">
						   <div class="form_actions" style="padding:0px;">
							<?php
							if($trialformfeature=="yes"||$user_type=="Admin"){
							?>
									  <input type="button" value="Back to Prospecting List" class="add_btn" onclick="document.location='prospectinglist.php'" style="width:195px;">
							<?php
							   }
							?>
						</div>
				 </td>
				<td class="tdbtn">
				   <div class="form_actions" style="padding:0px;">
							
									 <input type="button" value="Homepage Versions" class="add_btn" onclick="document.location='homepage_versions.php'" style="width:195px;">
							
						</div>
				</td>
				<td class="tdbtn">
						
						<div class="form_actions" style="padding:0px;">
							
									<input type="button" value="Product List" class="add_btn" onclick="document.location='product_list.php'" style="width:195px;">
							
						</div>
				</td>
				<td class="tdbtn"></td>
			</tr>
			<tr>
				<td class="tdbtn">
						   <div class="form_actions" style="padding:0px;">
							<?php
							if($masterlistfeature=="yes"||$user_type=="Admin"){
							?>
										<input type="button" value="Back to Master List" class="add_btn" onclick="document.location='masterlist.php'" style="width:195px;">
							<?php
							   }
							?>
						</div>
				 </td>
				<td class="tdbtn">
					<div class="form_actions" style="padding:0px;">
							
									 <input type="button" value="Call Status List" class="add_btn" onclick="document.location='callstatuslist.php'" style="width:195px;">
							
						</div>
				   
				</td>
				<td class="tdbtn">
				   	<div class="form_actions" style="padding:0px;">
					<?php
						if($_SESSION['user_id']=="3" || $user_type=="Admin"){
						?>
							
									 <input type="button" value="Manage Simulator" class="add_btn" onclick="document.location='manage_simulator.php'" style="width:195px;"> 
							<?php
							   }
							?>
							
						</div>
					
				</td>
				<td class="tdbtn"></td>
			</tr>
			</table>
   </div> 
 </div>
</div>
</div>
	</form>
	</body>
</html>
<script>
function funexport(listitem)
{
	try
	{
		with(document.demo_list)
		{
			if(listitem=='masterlist'){
				action='demolist-export.php';
				submit();
				return true;
				action='';
			}
			if(listitem=='prospectslist'){
				action='prospectlist-export.php';
				submit();
				return true;
				action='';
			}
		}
	}
	catch(e)
	{
		alert(e)
	}
}

/****function for paging statrs*******/
function pagetransfer(pagenumber,formname)
{	
	with(document.forms[formname])
	{ 
		HdnPage.value=pagenumber;
		HiddenMode.value="paging";
		submit();
	}
}
/****function for paging ends*******/

function funcdbexport()
{
 try
 {
with(document.demo_list)
  {
	   action='mysqldump.php';
	   submit();
	    return true;
		action='';
  }
 }
 catch(e)
 {
  alert(e)
 }
}
</script>