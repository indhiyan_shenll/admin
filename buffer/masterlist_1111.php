<?php
$parent_directory=basename(dirname($_SERVER["PHP_SELF"]));
$filename=basename($_SERVER["PHP_SELF"]);
include("../includes/configure.php");
include("includes/cmm_functions.php");
include("../includes/session_check.php");
$id=$_SESSION['user_id'];
$user_name=$_SESSION['user_name'];
$feat="master_list";
$typeandfeature=checklogin($id,$feat);
$usrArr=explode("*",$typeandfeature);
$user_type=$usrArr[0];
$mfeature=$usrArr[1];
$feat="demo_page";
$typeandfeature=checklogin($id,$feat);
$usrArr=explode("*",$typeandfeature);
$user_type=$usrArr[0];
$dfeature=$usrArr[1];
$dowfeature=$usrArr[2];
$trialfeature=$usrArr[4];
$demofeature=$usrArr[6];
if(($mfeature!="yes")&&$user_type!="admin"){
	header("Location:noauthorised.php");
	exit;
}
else{
$demo_id=$_GET["demo_id"];
$sort=$_GET["sort"];
$field=$_GET["field"];
if($sort==""){
	$sort="asc";
}
if($field==""){
	$field="trial_date";
}
if($field=="trial_date"){
	$fieldname="added_date";
	$order=" order by ".$fieldname." ".$sort;
	if($sort=="asc"){
		$dsort="desc";
		$dpath="images/up.png";
	}
	else{
		$dsort="asc";
		$dpath="images/down.png";
	}
}
if($field=="demo_date"){
	$fieldname="modified_date";
	$order=" order by ".$fieldname." ".$sort;
	if($sort=="asc"){
		$ddsort="desc";
		$ddpath="images/up.png";
	}
	else{
		$ddsort="asc";
		$ddpath="images/down.png";
	}
}
if($field=="sales_person"){
	$fieldname="sales_person";
	$order=" order by ".$fieldname." ".$sort;
	if($sort=="asc"){
		$spsort="desc";
		$sppath="images/up.png";
	}
	else{
		$spsort="asc";
		$sppath="images/down.png";
	}
}
if($field=="bus_name"){
	$fieldname="business_name";
	$order=" order by ".$fieldname." ".$sort;
	if($sort=="asc"){
		$rsort="desc";
		$rpath="images/up.png";
	}
	else{
		$rsort="asc";
		$rpath="images/down.png";
	}
}
if($field=="app_name"){
	$fieldname="app_name";
	$order=" order by ".$fieldname." ".$sort;
	if($sort=="asc"){
		$asort="desc";
		$apath="images/up.png";
	}
	else{
		$asort="asc";
		$apath="images/down.png";
	}
}
if($field=="demo_page_url"){
	$fieldname="demo_page_url";
	$order=" order by ".$fieldname." ".$sort;
	if($sort=="asc"){
		$usort="desc";
		$upath="images/up.png";
	}
	else{
		$usort="asc";
		$upath="images/down.png";
	}
}
if($field=="status"){
	$fieldname="status";
	$order=" order by ".$fieldname." ".$sort;
	if($sort=="asc"){
		$ssort="desc";
		$spath="images/up.png";
	}
	else{
		$ssort="asc";
		$spath="images/down.png";
	}
}
if($field=="payment_date"){
	$fieldname="payment_date";
	$order=" order by ".$fieldname." ".$sort;
	if($sort=="asc"){
		$pdsort="desc";
		$pdpath="images/up.png";
	}
	else{
		$pdsort="asc";
		$pdpath="images/down.png";
	}
}
if($field=="commission_date"){
	$fieldname="commission_date";
	$order=" order by ".$fieldname." ".$sort;
	if($sort=="asc"){
		$cdsort="desc";
		$cdpath="images/up.png";
	}
	else{
		$cdsort="asc";
		$cdpath="images/down.png";
	}
}
if(isset($_POST['HdnPage']) && $_POST['HdnPage']!="" && $_POST['HdnPage']!="0")
	$Page=$_POST['HdnPage'];
else
	$Page=1;
if($demo_id!=""){
	$getDemoQry="select * from tbl_demo where demo_id=:demoid";
	$prepDemoQry=$DBCONN->prepare($getDemoQry);
	$prepDemoQry->execute(array(':demoid'=>$demo_id));
	//$getDemoRes=mysql_query($getDemoQry);
	$DemoQryres=$prepDemoQry->fetchAll();
	$getDemoRow=$DemoQryres[0];
	$icon=stripslashes($getDemoRow["restaurant_logo"]);
	if(file_exists($icon)){
		unlink($icon);
	}
	$deleteQry="delete from tbl_demo where demo_id=:demoid";
	$preddeleteQry=$DBCONN->prepare($deleteQry);
	$deleteRes=$preddeleteQry->execute(array(':demoid'=>$demo_id));
	//$deleteRes=mysql_query($deleteQry);
	if($deleteRes){
		header("Location:masterlist.php");
		exit;
	}
}
if(isset($_POST["dlete_mode"]) && !empty($_POST["delete_yes"]) && $_POST["delete_yes"]=='yes'){
	$checkIds=$_POST["delete_id"];
	//print_r($checkIds);exit;
	$delStr="";
	for($del=0;$del<count($checkIds);$del++){
		if($delStr=="")
			$delStr=" demo_id=?";
		else
			$delStr.=" or demo_id=?";
	}
	$deleteQry="delete from tbl_demo where".$delStr;
	$prepdeleteQry=$DBCONN->prepare($deleteQry);
	//$deleteRes=$prepdeleteQry->execute(array(':checkids'=>$checkIds[$del]));
	$deleteRes=$prepdeleteQry->execute($checkIds);
	//$deleteRes=mysql_query($deleteQry);
	if($deleteRes){
		header("Location:masterlist.php");
		exit;
	}
}
if(!empty($_POST["update_yes"]) && $_POST["update_yes"]=='yes'){
	$checkIds=$_POST["update_id"];
	$response = implode(",",$checkIds);
    $updateQry="update tbl_demo set view_status=:view_status,modified_date=:modified_date where find_in_set(cast(demo_id as char), :response)";
	$prepupdateQry=$DBCONN->prepare($updateQry);
	$updateRes=$prepupdateQry->execute(array(":view_status"=>"1",":modified_date"=>$dbdatetime,":response"=>$response));
	if($updateRes){
		header("Location:masterlist.php");
		exit;
	}
}


//
if(isset($_POST["demo_date"])){
	$sales_person=$_POST["salesperson"];
	$Sales_manager=$_POST["salesmanager"];
	$trialdate=addslashes(trim($_POST["trial_date"]));
	$trialdate2=addslashes(trim($_POST["trial_date2"]));
	$demodate=addslashes(trim($_POST["demo_date"]));
	$demodate2=addslashes(trim($_POST["demo_date2"]));
	$status=$_POST["status"];
	$paymentdate=addslashes(trim($_POST["payment_date"]));
	$paymentdate2=addslashes(trim($_POST["payment_date2"]));
	$commision_date=addslashes(trim($_POST["commission_date"]));
	$commision_date2=addslashes(trim($_POST["commission_date2"]));

    if(($trialdate!='')&&($trialdate2!=''))
	{
		$condition.=" and (date_format(added_date,'%Y-%m-%d')>='".date('Y-m-d',strtotime($trialdate))."' and date_format(added_date,'%Y-%m-%d')<='".date('Y-m-d',strtotime($trialdate2))."')";
	}
    if(($trialdate!='')&&($trialdate2==''))
	{
		$condition.=" and date_format(added_date,'%Y-%m-%d')='".date('Y-m-d',strtotime($trialdate))."'";
	}
	if(($trialdate=='')&&($trialdate2!='')){
		$condition.=" and date_format(added_date,'%Y-%m-%d')='".date('Y-m-d',strtotime($trialdate2))."'";
	}
	if(($demodate!='')&&($demodate2!=''))
	{
		$condition.=" and (date_format(modified_date,'%Y-%m-%d')>='".date('Y-m-d',strtotime($demodate))."' and date_format(modified_date,'%Y-%m-%d')<='".date('Y-m-d',strtotime($demodate2))."')";
		$set="yes";
	}
    if(($demodate!='')&&($demodate2==''))
	{
		$condition.=" and date_format(modified_date,'%Y-%m-%d')='".date('Y-m-d',strtotime($demodate))."'";
		$set="yes";
	}
	if(($demodate=='')&&($demodate2!='')){
		$condition.=" and date_format(modified_date,'%Y-%m-%d')='".date('Y-m-d',strtotime($demodate2))."'";
		$set="yes";
	}
	if(($paymentdate!='')&&($paymentdate2!=''))
	{
		$condition.=" and (payment_date>='".date('Y-m-d',strtotime($paymentdate))."' and modified_date<='".date('Y-m-d',strtotime($paymentdate2))."')";
	}
    if(($paymentdate!='')&&($paymentdate2==''))
	{
		$condition.=" and payment_date='".date('Y-m-d',strtotime($paymentdate))."'";
	}
	if(($paymentdate=='')&&($paymentdate2!='')){
		$condition.=" and payment_date='".date('Y-m-d',strtotime($paymentdate2))."'";
	}
    if(($commision_date!='')&&($commision_date2!=''))
	{
		$condition.=" and (commission_date>='".date('Y-m-d',strtotime($commision_date))."' and modified_date<='".date('Y-m-d',strtotime($commision_date2))."')";
	}
    if(($commision_date!='')&&($commision_date2==''))
	{
		$condition.=" and commission_date='".date('Y-m-d',strtotime($commision_date))."'";
	}
	if(($commision_date=='')&&($commision_date2!='')){
		$condition.=" and commission_date='".date('Y-m-d',strtotime($commision_date2))."'";
	}
	if(!empty($status))
	{
     $condition.=" and ".setfieldarray("status",$status);
	}
	if(!empty($sales_person))
	{
     $condition.=" and ".setfieldarray("sales_person",$sales_person);
	}
	if(!empty($Sales_manager))
	{
     $condition.=" and ".setfieldarray("sales_manager",$Sales_manager);
	}
}
$usercondn=($user_type=="user")?" and (sales_person='".$_SESSION['user_name']."' OR sales_manager='".$_SESSION['user_name']."')":"";
//$activeConditon=($user_type!="admin")?" and demo_status='active'":"";
$getdQry="select * from tbl_demo where demo_page_url!=''".$condition.$usercondn.$activeConditon;
$getdRes=mysql_query($getdQry);
$getdCnt=mysql_num_rows($getdRes);
if($user_type=="Admin" && $_SESSION['user_id']!="3"){
	 $condition.=" and  view_status='1'";					 
}
$getDemoQry="select * from tbl_demo where 1=1".$condition.$usercondn.$activeConditon.$order;
$getDemoRes=mysql_query($getDemoQry);
$getDemoCnt=mysql_num_rows($getDemoRes);
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<title>MAR Pipeline System</title>
		<link rel="shortcut icon" href="images/Fav.ico" type="image/ico">
		<meta name="Generator" content="EditPlus">
		<meta name="Author" content="">
		<meta name="Keywords" content="">
		<meta name="Description" content="">
		<style>
			body{
				margin:0;
				color:black;
				background:#455A68;
				font-family:arial;
			}
			.header{
				
				background:#1C242A;
				height:100px;
			}
			.content{
				background:#455A68;
				min-height:600px;
			}
			
			.form_actions{
				padding-top:15px;
				padding-bottom:30px;
			}
			.form_actions .add_btn{
				cursor:pointer;
				border-radius:0px;
				background:#0D0D0D;
				color:#D9D9D9;
				border-color:#D9D9D9;
				padding:5px 15px 5px 15px;
				font-family:arial;
				min-width:155px;
			}
			.list_content{
				width:950px;
				margin-left:40px;
				/*margin-left:auto;
				margin-right:auto; */
			}
			.tbl_header{
				font-size:13px;
				border-bottom:1px solid #D9D9D9;
				text-align:left;
				color:white;
			}
			.tbl-body{
				font-size:12px;
				line-height:25px;
			}
			a{
				color:black;
			}
			
		</style>
	<script type="text/javascript" src="js/jquery.js"></script>
	<script>
	function checkallboxes(chkcnt){
		var opt=document.getElementById("checkall").checked;
		var i;
		for(i=1;i<=chkcnt;i++){
			document.getElementById("delete_"+i).checked=opt;
		}
	}
	function checkallboxes1(chkcnt){
		var opt=document.getElementById("checkall_up").checked;
		var i;
		for(i=1;i<=chkcnt;i++){
			document.getElementById("update_"+i).checked=opt;
		}
	}
    function chkind1(totcnt){
		var chkdcnt=0;
		for(i=1;i<=totcnt;i++){
			if(document.getElementById("update_"+i).checked)
				chkdcnt++;
		}
		if(chkdcnt==totcnt)
			document.getElementById("checkall_up").checked=true;
		else
			document.getElementById("checkall_up").checked=false;
	}
	function chkselcnt1(cnt){
		var chkdcnt=0;
		for(i=1;i<=cnt;i++){
			if(document.getElementById("update_"+i).checked)
				chkdcnt++;
		}
		return chkdcnt;
	}




	function chkind(totcnt){
		var chkdcnt=0;
		for(i=1;i<=totcnt;i++){
			if(document.getElementById("delete_"+i).checked)
				chkdcnt++;
		}
		if(chkdcnt==totcnt)
			document.getElementById("checkall").checked=true;
		else
			document.getElementById("checkall").checked=false;
	}
	function chkselcnt(cnt){
		var chkdcnt=0;
		for(i=1;i<=cnt;i++){
			if(document.getElementById("delete_"+i).checked)
				chkdcnt++;
		}
		return chkdcnt;
	}
	function changeStatus(element,demoid){
		var srcvalue=element.getAttribute("src");
		var status,path,title;
		if(srcvalue=="images/success.png"){
			status="active";
			path="images/close.png";
			title="Click here to activate";
		}
		else{
			status="deactive";
			path="images/success.png";
			title="Click here to deactivate";
		}
		$.ajax({url:"change_demostatus.php?demo_id="+demoid+"&status="+status,success:function(result){
			if(result=="success"){
				element.setAttribute("src",path);
				element.setAttribute("title",title);
			}
		}});
		
	}
	</script>
	</head>
	<body>
	
		
			
				<div class="header">
					<span style="float:right;margin-right:20px;margin-top:5px;"><a href="logout.php" style="color:white;text-decoration:none;">Logout</a></span>
				    <div style="width:1280px;height:80px;">
						 <div style="float:left;width:55%;">
							<img src="images/myappyrestaurants.png" style="margin-top:10px;margin-left:40px;">
							<div style="float:right;width:65%;margin-top:20px;width:32%;"><font style="font-family:arial;color:white;size:40px;" size="5px;">Master List</font></div>
						 </div>
						  <?php
							 if($user_type=="admin"||$dowfeature=="yes"||$trialfeature=="yes"){
						 ?>
						 <div class="form_actions" style="float:right;border:1 px solid red;width:30%;text-align:right;">
							   <input type="button" value="Admin Features" class="add_btn" onclick="document.location='admin_features.php'"  style="margin-top:20px;">
						  </div>
						 <?php
							}
						 ?>
					</div> 
				</div>
				<div class="content">
					<div class="list_content">
							<div class="form_actions" style="width:1240px;padding-bottom:80px;">
						   <div style="width:50%;float:left;"> 
						   <?php
							if($user_type=="admin"||$demofeature=="yes"){
							?>
						   <input type="button" style="width:170px;" value="Add a Demo Page" class="add_btn" onclick="document.location='newdemo.php'" ><br>
						   <?php
						   }
							if($user_type=="admin"||$trialfeature=="yes"){
							?>
						   <input type="button" style="width:170px;" value="Go To Prospecting List" class="add_btn" onclick="document.location='prospectinglist.php'">
						   <?php
						   }
							?>
							</div>
						   <div style="width:40%;float:right;text-align:right;">
						   <input type="button" value="Filter Master List" class="add_btn" onclick="document.location='filtermasterlist.php'"><br>
						   <input type="button" value="Remove Filters" class="add_btn" onclick="document.location='masterlist.php'"><br><br>
							<span style="font-size:12px;font-weight:bolder;color:white;" id="demo_count">Trial Count:&nbsp;<?php echo $getDemoCnt;?></span><br>
							<span style="font-size:12px;font-weight:bolder;color:white;" id="demo_count">Demo Count:&nbsp;<?php echo $getdCnt;?></span>
						   </div><br><br>
                      
          
                </div>
						<div style="padding-bottom:12px;margin-top:15px;">
							<form name="demo_list" method="post">
							<input type="hidden" name="HiddenMode" id="HiddenMode" value="">
							<input type="hidden" name="HdnPage" id="HdnPage" value="">
							<input type="hidden" name="dlete_mode" id="dlete_mode">
							<input type="hidden" name="delete_yes" id="delete_yes" value="">
							<input type="hidden" name="update_yes" id="update_yes" value="">
						</div>
						<table cellspacing="0" cellpadding="0" width="135%" class="tbl-body" border="0">
							<?php								
							$records_perpage=100;
							$TotalRecords	=	$getDemoCnt;
							if($TotalRecords <= (($Page * $records_perpage)-$records_perpage))
								$Page	=	$Page-1;
							$TotalPages		=	ceil($TotalRecords/$records_perpage);
							$Start			=	($Page-1)*$records_perpage;
							if($TotalPages>1)
							  {
							?>
							<tr>
								<td align="center" colspan="11" style="border-bottom:1px solid grey;">
									<?php
									$FormName="demo_list";
									include("../includes/paging.php");
									?>
								</td>
							</tr>
							<?php
							  }
							?>
							<tr class="tbl_header">
									<th style="cursor:pointer" onclick="document.location='masterlist.php?sort=<?php echo $dsort;?>&field=trial_date'">Trial Date&nbsp;<?php if($dpath!=""){?><img src="<?php echo $dpath;?>" style="width:10px;height:10px;"><?php }?></th>
									<th style="cursor:pointer" onclick="document.location='masterlist.php?sort=<?php echo $rsort;?>&field=bus_name'">Business Name&nbsp;&nbsp;<?php if($rpath!=""){?><img src="<?php echo $rpath;?>" style="width:10px;height:10px;"><?php }?></th>
									<th style="cursor:pointer" onclick="document.location='masterlist.php?sort=<?php echo $spsort;?>&field=sales_person'">Salesperson&nbsp;&nbsp;<?php if($sppath!=""){?><img src="<?php echo $sppath;?>" style="width:10px;height:10px;"><?php }?></th>
									<th style="cursor:pointer;" onclick="document.location='masterlist.php?sort=<?php echo $ddsort;?>&field=demo_date'">Demo Date&nbsp;&nbsp;<?php if($ddpath!=""){?><img src="<?php echo $ddpath;?>" style="width:10px;height:10px;"><?php }?></th>
									<th style="width:18%;cursor:pointer;" onclick="document.location='masterlist.php?sort=<?php echo $usort;?>&field=demo_page_url'">Demo Page URL&nbsp;&nbsp;<?php if($upath!=""){?><img src="<?php echo $upath;?>" style="width:10px;height:10px;"><?php }?></th>
									<th style="cursor:pointer;width:18%;" onclick="document.location='masterlist.php?sort=<?php echo $ssort;?>&field=status'">Status&nbsp;&nbsp;<?php if($spath!=""){?><img src="<?php echo $spath;?>" style="width:10px;height:10px;"><?php }?></th>
									<th style="cursor:pointer;" onclick="document.location='masterlist.php?sort=<?php echo $pdsort;?>&field=payment_date'">Payment Date&nbsp;&nbsp;<?php if($pdpath!=""){?><img src="<?php echo $pdpath;?>" style="width:10px;height:10px;"><?php }?></th>
									<th style="cursor:pointer;width:10%;" onclick="document.location='masterlist.php?sort=<?php echo $cdsort;?>&field=commission_date'">Comm.Date&nbsp;&nbsp;<?php if($cdpath!=""){?><img src="<?php echo $cdpath;?>" style="width:10px;height:10px;"><?php }?></th>
									<th style="width:3%" align="left">QA</th>
									<th style="width:7%;cursor:pointer;"><span onclick="deleteRecords('<?php echo $user_type;?>')">Delete?</span>&nbsp;<input type="checkbox" name="checkall" id="checkall" onclick="checkallboxes(document.getElementById('recordcnt').value)"></th>
                                     <?php
						if($user_type=="Admin" && $_SESSION['user_id']=="3"){
						?>
					    <th style="cursor:pointer;width:7%;"><center><span onclick="UpdateRecords('<?php echo $_SESSION['user_id'];?>')"> Status</span>&nbsp;<input type="checkbox" name="checkall_up" id="checkall_up" onclick="checkallboxes1(document.getElementById('recordcnt').value)"></center></th>
						<?php
						   }
						?>
									<th style="background-color:one;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
								</tr>
							<tr style="height:20px;">
								<td colspan="11" style="border-top:1px solid #D9D9D9;"></td>
							</tr>
						<?php
							if($getDemoCnt>0){
								$records_perpage=100;
								$TotalRecords	=	$getDemoCnt;
								if($TotalRecords <= (($Page * $records_perpage)-$records_perpage))
									$Page	=	$Page-1;
								$TotalPages		=	ceil($TotalRecords/$records_perpage);
								$Start			=	($Page-1)*$records_perpage;
								
								//code for paging ends
								$getDemoQry.=" limit $Start,$records_perpage";
								$getDemoRes = mysql_query($getDemoQry);
								$getDemoCnt = mysql_num_rows($getDemoRes);

								$i=1;
								$j=0;
								while($getDemoRow=mysql_fetch_array($getDemoRes)){
									$demoid=$getDemoRow["demo_id"];
									$date=explode(" ",$getDemoRow["modified_date"]);
									$added_date=explode(" ",$getDemoRow["added_date"]);
									$statusid=$getDemoRow["status"];
									$bgcolor='';
									/*--set the color   --*/
									$getcolorQry="select * from  tbl_status where status='".$statusid."'";
									$getcolorRes=mysql_query($getcolorQry);
									$getcolorRow=mysql_fetch_array($getcolorRes);
									$Status_color=stripslashes($getcolorRow["status_color"]);
									$bgcolor='#'.$Status_color;
									$push_status=($getDemoRow["pushing_status"]);
									$demo_status=$getDemoRow["demo_status"];
									$demo_view_status=$getDemoRow["view_status"];
									
									/*Set Status image*/
									if($user_type=="admin"){
										if($demo_status=="active"){
											$statusimg="<img src=\"images/success.png\" onclick=\"changeStatus(this,'".$getDemoRow["demo_id"]."')\" style=\"cursor:pointer;width:16px;height:14px;\" title=\"Click here to deactivate\">";
										}
										else{
											$statusimg="<img src=\"images/close.png\" onclick=\"changeStatus(this,'".$getDemoRow["demo_id"]."')\" style=\"cursor:pointer;width:16px;height:14px;\" title=\"Click here to activate\">";
										}
									}
									else{
										$statusimg="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
									}
									/*End Status image*/
									$sales_id=stripslashes($getDemoRow["sales_person"]);
                                
								 if(!empty($getDemoRow["demo_page_url"]))
								{     
									 $date=date("d-m-Y",strtotime($date[0]));
									 

								}

								else{
									 $date="";
								}
								 if(!empty($getDemoRow["demo_page_url"])){


								 }
								if($getDemoRow["payment_date"]!=""){
									if($getDemoRow["payment_date"]!="0000-00-00"){
										$payment_date=date('d-m-Y',strtotime(stripslashes($getDemoRow["payment_date"])));
									}
									else{
										$payment_date="";
									}
								}
								else{
									$payment_date="";
								}
								if($getDemoRow["commission_date"]!=""){
									if($getDemoRow["commission_date"]!="0000-00-00"){
										$commission_date=date('d-m-Y',strtotime(stripslashes($getDemoRow["commission_date"])));
									}
									else{
										$commission_date="";
									}
								}
								else{
									$commission_date="";
								}
								$ndot=$udot=$sdot=$bdot='';
								
							if(strlen($getDemoRow["business_name"])>20)
								$bdot="...";
							if(strlen($sales_id)>20)
								$sdot="...";
							if(strlen($srow["status"])>10)
								$ndot="...";
							$stdot="";
							if(strlen($statusid)>20)
								$stdot="...";
                                	
                                    $font_color='black';

									
                                    
									if($set=="yes"&&empty($getDemoRow["demo_page_url"])){
										$display=";display:none";
									}
									else{
										$display="";
									}
							
						?>
							<tr  style="background:<?php echo $bgcolor.$display;?>">
								<td style="border-bottom:1px solid grey;color:<?php echo $font_color;?>"><?php echo date("d-m-Y",strtotime($added_date[0]));?></td>
								<td style="border-bottom:1px solid grey;color:<?php echo $font_color;?>"><?php echo substr(stripslashes($getDemoRow["business_name"]),0,20).$bdot;?></td>
								<td style="border-bottom:1px solid grey;color:<?php echo $font_color;?>"><?php echo substr($sales_id,0,20).$sdot; ?></td>
								<td style="border-bottom:1px solid grey;color:<?php echo $font_color;?>"><?php echo $date;?></td>
								
								<?php
								$url="";
                              
						         $trailcnt=0;
								if(!empty($getDemoRow["demo_page_url"]))
								{
									$demourl="www.myappyrestaurant.com/live/appname=".trim(stripslashes($getDemoRow["demo_page_url"]))."&app_id=".$getDemoRow["demo_id"];
									if(strlen($demourl)>30)
										//$udot="...";
									$url="<a href=\"".HTTP_ROOT_FOLDER."live/liveapp.php?appname=".urlencode(trim(stripslashes($getDemoRow["demo_page_url"])))."&app_id=$demoid\" style='color:<?php echo $font_color;?>' target=\"_blank\">".$udot.substr($demourl,25,35);"</a>";
									$trailcnt++;
								}
								if($getDemoRow["dev_qa_date"]!=""&&$getDemoRow["dev_qa_date"]!="0000-00-00")
									$qasrc="<img src=\"images/Dot.png\">";
								else
									$qasrc="";
								?>
								<td style="border-bottom:1px solid grey;color:<?php echo $font_color;?>"><?php echo $url;?></a></td>
								<td style="border-bottom:1px solid grey;color:<?php echo $font_color;?>"><?php echo substr($statusid,0,20).$stdot;?></td>
								<td style="border-bottom:1px solid grey;color:<?php echo $font_color;?>"><?php echo $payment_date;?></td>
								<td style="border-bottom:1px solid grey;color:<?php echo $font_color;?>"><?php echo $commission_date;?></td>
								<td style="border-bottom:1px solid grey;color:<?php echo $font_color;?>"><?php echo $qasrc;?></td>
								<td style="border-bottom:1px solid grey;color:<?php echo $font_color;?>"><a href="newdemo.php?demo_id=<?php echo $getDemoRow["demo_id"];?>" style="color:<?php echo $font_color;?>">Edit</a>&nbsp;&nbsp;&nbsp;<?php echo $statusimg;?>&nbsp;&nbsp;&nbsp;<input type="checkbox" name="delete_id[]" value="<?php echo $getDemoRow["demo_id"];?>" id="delete_<?php echo $i;?>" onclick="chkind('<?php echo $getDemoCnt;?>')"></td>
								<?php
								if($user_type=="Admin" && $_SESSION['user_id']=="3"){
								?>
								    <td style="border-bottom:1px solid grey;color:<?php echo $font_color;?>"><center><input type="checkbox" name="update_id[]" value="<?php echo $getDemoRow["demo_id"];?>" id="update_<?php echo $i;?>" onclick="chkind1('<?php echo $getDemoCnt;?>')" <?php if($demo_view_status=="1"){echo " checked";}?> ><center></td>
								<?php
								}
								?>
								<td style="background-color:#455A68;width:2px;height:2px;">&nbsp;&nbsp;&nbsp;&nbsp;<?php if($push_status!=''){echo "<img src=\"images/man.png\" style=\"height:16px;\"\">";  }else{}?>   </td>
							</tr> 

						<?php
						
							$i++;
								}
								
								if($TotalPages>1)
								  {
								?>
								<tr>
									<td align="center" colspan="11" style="border-bottom:1px solid grey;">
										<?php
										$FormName="demo_list";
										include("../includes/paging.php");
										?>
									</td>
								</tr>
								<?php
								  }
								echo "</table>";
							}
							else{
								echo "<tr bgcolor='#a5a5a5'><td colspan=\"11\" style=\"border-bottom:1px solid grey;\"><center>No demo(s) found.</center></td></tr>";
							}
							echo "<script>document.getElementById(\"demo_count\").innnerHTML=\"Demo Pages Count:&nbsp;$getDemoCnt\";</script>";
						?>
						<input type="hidden" name="recordcnt" id="recordcnt" value="<?php echo $getDemoCnt;?>">
						</form>
					</div>
					<div style="height:50px;border:0px solid red;"></div>
				</div>
				
			
	
	</body>
</html>
<script>
function funexport()
{
	try
	{
		with(document.demo_list)
		{
			action='demolist-export.php';
			submit();
			return true;
			action='';
		}
	}
	catch(e)
	{
		alert(e)
	}
}

/****function for paging statrs*******/
function pagetransfer(pagenumber,formname)
{	
	with(document.forms[formname])
	{ 
		HdnPage.value=pagenumber;
		HiddenMode.value="paging";
		submit();
	}
}
/****function for paging ends*******/


function deleteRecords(usertype){
	try
	{
		with(document.demo_list)
		{
			if(usertype=="Admin"){
				var recordscnt=chkselcnt(document.getElementById("recordcnt").value);
				if(recordscnt==0){
					alert("Please select at least one demo");
					return false;
				}
				else if(confirm('Are you sure want to delete these '+recordscnt+' demos?'))
				{
					document.getElementById('delete_yes').value='yes';
					submit();
				}else{
					document.getElementById("checkall").checked=false;
					var i;
					for(i=1;i<=recordscnt;i++){
						document.getElementById("delete_"+i).checked=false;
					}
				
				}
			}
			else{
				alert("You are not authorised to delete this item");
				return false;
			}
		}
	}
	catch(e)
	{
		alert(e)
	}
}
function UpdateRecords(userid){
	try
	{
		with(document.demo_list)
		{
			if(userid=="3"){
				var recordscnt=chkselcnt1(document.getElementById("recordcnt").value);
				if(recordscnt==0){
					alert("Please select at least one demo");
					return false;
				}
				else if(confirm('Are you sure want to change  status for '+recordscnt+' demos?'))
				{
					document.getElementById('update_yes').value='yes';
					submit();
				}else{
					document.getElementById("checkall_up").checked=false;
					var i;
					for(i=1;i<=recordscnt;i++){
						document.getElementById("update_"+i).checked=false;
					}
				
				}
			}
			else{
				alert("You are not authorised to change this status");
				return false;
			}
		}
	}
	catch(e)
	{
		alert(e)
	}
}
</script>
<?php
}
?>