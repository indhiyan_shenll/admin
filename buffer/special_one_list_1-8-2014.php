<?php
session_start();
include("../includes/configure.php");
include("../includes/session_check.php");
include("includes/cmm_functions.php");
include("../includes/session_check.php");
$id=$_SESSION['user_id'];	
$feat="download_list";
$typeandfeature=checklogin($id,$feat);
$usrArr=explode("*",$typeandfeature);
$user_type=$usrArr[0];
$feature=$usrArr[1];
$trialformfeature=$usrArr[4];
$masterlistfeature=$usrArr[5];
$special_id=$_GET["special_id"];
$sort=$_GET["sort"];
$field=$_GET["field"];
if($field=="page"){
	$fieldname="product_page";
	$order="  order by ".$fieldname." ".$sort;
	if($sort=="asc"){
		$rsort="desc";
		$rpath="images/up.png";
	}
	else{
		$rsort="asc";
		$rpath="images/down.png";
	}
	
}
if(isset($_POST['HdnPage']) && $_POST['HdnPage']!="" && $_POST['HdnPage']!="0")
	$Page=$_POST['HdnPage'];
else
	$Page=1;
if($special_id!=""){
	$deleteQry="delete from tbl_special_one where special_one_id=:special_one_id";
	$prepdeleteQry=$DBCONN->prepare($deleteQry);
	$deleteRes=$prepdeleteQry->execute(array(":special_one_id"=>$special_id));
	if($deleteRes){
		header("Location:special_one_list.php");
		exit;
	}
}
if(isset($_GET["dord"]) && $_GET["dord"]!="")
{
	$dorder=$_GET["dord"];
	$arr1=explode("*",$dorder);
	
	foreach($arr1 as $value)
	{
		$arr2=explode("^",$value);
		$id=$arr2[0];
		$order=$arr2[1];
		//mysql_query("update  tbl_special_one set display_order='".$order."' where special_one_id='".$id."'");
		$update_qry="update tbl_special_one set display_order=:dispaly_order where special_one_id=:special_one_id";
        $prepupdate_qry=$DBCONN->prepare($update_qry);
		$updateresults=$prepupdate_qry->execute(array(":dispaly_order"=>$order,":special_one_id"=>$id));
	}
	header("Location:special_one_list.php");
	exit;
}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<title>MAR Pipeline System</title>
		<link rel="shortcut icon" href="images/Fav.ico" type="image/ico">
		<meta name="Generator" content="EditPlus">
		<meta name="Author" content="">
		<meta name="Keywords" content="">
		<meta name="Description" content="">
		<style>
			body{
				margin:0;
				color:black;
				background:#455A68;
				font-family:arial;
			}
			.header{
				height:70px;
				background:#1C242A;
			}
			.content{
				background:#455A68;
				min-height:600px;
			}
			
			.form_actions{
				padding-top:15px;
				padding-left:5px;
				padding-bottom:30px;
			}
			.form_actions .add_btn{
				cursor:pointer;
				border-radius:0px;
				background:#0D0D0D;
				color:#D9D9D9;
				border-color:#D9D9D9;
				padding:5px 15px 5px 15px;
				font-family:arial;
			}
			.list_content{
				width:950px;
				margin-left:40px;
				/*
				margin-left:auto;
				margin-right:auto;
				*/
			}
			.tbl_header th{
				font-size:13px;
				border-bottom:1px solid #D9D9D9;
				text-align:left;
				font-family:arial;
				color:white;
			}
			.tbl-body{
				font-size:12px;
				line-height:25px;
				font-family:arial;
			}
			a{
				color:black;
			}
		</style>
	</head>
	<body>
		<div>
			<div style="margin-left:auto;margin-right:auto;">
				<div class="header">
					<span style="float:right;margin-right:20px;margin-top:5px;"><a href="logout.php" style="color:white;text-decoration:none;">Logout</a></span>
					<img src="images/myappyrestaurants.png" style="margin-top:10px;margin-left:40px;">
				</div>
				<div class="content">
					<div class="list_content">
						<div class="form_actions" style="padding-bottom:45px;">
						  <?php 
						  if($user_type=="Admin"){
						  ?>
							<input type="button" value="Back To Admin Features" class="add_btn" onclick="document.location='admin_features.php'" style="float:left;">
							<?php 
		                    }
						  ?>
							<input type="button" value="Add Simulator" class="add_btn" onclick="document.location='edit_special_one.php'" style="float:right;">
						</div>
						<div style="padding-bottom:12px;">
						<form name="demo_list" method="post">
							<input type="hidden" name="HiddenMode" id="HiddenMode" value="">
							<input type="hidden" name="HdnPage" id="HdnPage" value="">
							<table cellspacing="0" cellpadding="0" width="100%" class="tbl_header" border="0">
							  <tr>
									
									<!-- <th width="5%">No</th> -->
									<th width="24%"  style="cursor:pointer">Business Name&nbsp;&nbsp;</th>
									<th width="16%" onclick="document.location='special_one_list.php?sort=<?php echo $rsort;?>&field=page'" style="cursor:pointer">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbspProduct Page <?php if($rpath!=""){?><img src="<?php echo $rpath;?>" style="width:10px;height:10px;"><?php }?></th>
									<th width="10%"  style="cursor:pointer"  >Special Logo</th>
									<th width="13%"  style="cursor:pointer"  >Promo Code&nbsp;&nbsp;</th>
									<th width="17%"  style="cursor:pointer" >Simulator Creator&nbsp;&nbsp;</th>
								    <th width="10%" style="cursor:pointer" >Sort Order&nbsp;&nbsp;</th>
									<th width="10%">Delete?</th>
								
							 </tr>
							</table>
						</div>
							<table cellspacing="0" cellpadding="0" width="100%" class="tbl-body">
                         <?php
					        if($user_type=="Admin"){
								if($_GET["field"]!=""){
								  $get_business_qry="select * from tbl_special_one".$order;
								}
								else{
								 $get_business_qry="select * from tbl_special_one order by display_order asc";
								}
							}
							else{
								 if($_GET["field"]!=""){
									 $get_business_qry="select * from tbl_special_one where user_id='".$_SESSION['user_id']."'".$order;
								}
								else{
								 $get_business_qry="select * from tbl_special_one where user_id='".$_SESSION['user_id']."' order by display_order asc";
								}
							}
							$prepgetQry=$DBCONN->prepare($get_business_qry);
							$prepgetQry->execute();
							$getCnt =$prepgetQry->rowCount();
							if($getCnt>0){
								$records_perpage=200;
								$TotalRecords	=$getCnt;
								if($TotalRecords <= (($Page * $records_perpage)-$records_perpage))
								$Page	=	$Page-1;
								$TotalPages		=	ceil($TotalRecords/$records_perpage);
								$Start			=	($Page-1)*$records_perpage;
								$get_business_qry.=" limit $Start,$records_perpage";
								$prepgetQry=$DBCONN->prepare($get_business_qry);
								$prepgetQry->execute();
								$getCnt =$prepgetQry->rowCount();
								$sno=$Start+1;
                              if($getCnt>0){
							  $rowno=1;
							  $i=1;
				     		while($getRow=$prepgetQry->fetch()){
									$id = $getRow['special_one_id'];
									$display_order=stripslashes($getRow['display_order']);
									if($i%2==1){
										$bgcolor="#a5a5a5";
									}
									else{
										$bgcolor="#d2d1d1";
									}
                                    if($getCnt>2)
										{

										    $sqlqry	="select display_order,special_one_id from tbl_special_one where display_order>:display_order order by display_order asc limit 1";
                                            $prepget_qry1=$DBCONN->prepare($sqlqry);
						                    $prepget_qry1->execute(array(":display_order"=>$display_order));
                                            $dispaly_count =$prepget_qry1->rowCount();
											if($dispaly_count>0)
												{
														$fetch=$prepget_qry1->fetch();
														$nextorder=$fetch["display_order"];
														$nextid=$fetch["special_one_id"];
												}
											
										}

									$uparrow="<img src='images/arrow_up.png' border='0'>";
									$downarrow="<img src='images/arrow_down.png' border='0'>";
									$enduparrow="<img src='images/arrow_up.png' border='0' style=\"margin-left:10px;\">";
									$startdownarrow="<img src='images/arrow_down.png' border='0' style=\"margin-left:10px;\">";
									if($i==$getCnt && $getCnt==1)
										  $display="";
									else if($i==1)
										 $display="<a href='special_one_list.php?dord=$nextid^$display_order*$id^$nextorder'>".$startdownarrow."</a>";
									else if($i>1 && $i<>$getCnt)
										  $display="<a href='special_one_list.php?dord=$nextid^$display_order*$id^$nextorder'>".$downarrow."</a>&nbsp;&nbsp;<a href='special_one_list.php?dord=$previd^$display_order*$id^$prevorder'>".$uparrow."</a>";
									else if($i==$getCnt)
										  $display="<a href='special_one_list.php?dord=$previd^$display_order*$id^$prevorder'>".$enduparrow."</a>";
										$prevorder=$display_order;
										$previd=$id;
										$image_src=$getRow["logo"];

										
										
										
							         
						?>
							<tr bgcolor="<?php echo $bgcolor;?>">
								


								 <!-- <td width="5%"><?php echo $i;?></td> -->
									<td width="24%"  style="cursor:pointer"  ><?php echo stripslashes($getRow["business_name"]);?></td>
									<td width="16%"><center><?php echo stripslashes($getRow["product_page"]);?></center></td>
									<td width="10%"  style="cursor:pointer"  ><?php if($image_src!=""){ echo "<img src=\"$image_src\" style=\"height:15px;width:15px;margin-left:30px;\">";}?></td>
									<td width="13%"  style="cursor:pointer"><?php echo stripslashes($getRow["promo_code"]);?>
									
									
									</td>
									<td width="17%"  style="cursor:pointer"><?php echo stripslashes(getuser($getRow["user_id"]));?></td>
								    <td width="10%" style="cursor:pointer"  ><span style="margin-left:10px;"><?php echo $display;?></span></td>
									<td width="10%"><a href="edit_special_one.php?special_id=<?php echo $getRow["special_one_id"];?>">Edit</a>&nbsp;&nbsp;/&nbsp;&nbsp;<a href="special_one_list.php?special_id=<?php echo $getRow["special_one_id"];?>" onclick="return confirm('Are you sure want to delete this special one? ')">Delete</a></td>
							</tr>
							
						<?php
							$i++;
                            $rowno++;
							$sno++;
								}
							if($TotalPages > 1){

									echo "<tr><td align='center' colspan='7' valign='middle' class='pagination'>";

									if($TotalPages>1){

											$FormName = "demo_list";
									       include("../includes/paging.php");
									 
									}

									echo "</td></tr>";

									  }
								}
								else{
									echo "<tr style=\"background-color:#f6f6f6;text-align:center;\"><td colspan=\"7\">No special one(s) found.</td></tr>";
								}

							
							}
							else{
								echo "<tr style=\"background-color:#f6f6f6;text-align:center;\"><td colspan=\"7\">No special one(s) found.</td></tr>";
							}
						?>
						<tr>
					   <td colspan="4">
								<div class="form_actions" style="text-align:left;position:relative;">
								  <?php 
						  if($user_type=="Admin" ||$user_type=="Affiliate"){
						  ?>
								<input type="button" value="Back To Admin Features" class="add_btn" onclick="document.location='admin_features.php'">
								  <?php 
						}
						  ?>
								</td>
							</tr>
						</table>
						</form>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>
<script type="text/javascript">
/****function for paging statrs*******/
function pagetransfer(pagenumber,formname)
{	
	with(document.forms[formname])
	{ 
		HdnPage.value=pagenumber;
		HiddenMode.value="paging";
		submit();
	}
}
/****function for paging ends*******/
</script>
