
<?php
include("../includes/configure.php");
include("../includes/session_check.php");
$dbdatetime=date('Y-m-d H:i:s',strtotime('now'));
$user_id=$_GET["user_id"];
if($user_id!=""){
	//$user_id
	$geUserQry="select * from  tbl_users where user_id=:user_id";
	$prepgetuserqry=$DBCONN->prepare($geUserQry);
	$prepgetuserqry->execute(array(":user_id"=>$user_id));
	//$geUserRes=mysql_query($geUserQry);
	$geUserRow=$prepgetuserqry->fetch();
	$username=stripslashes($geUserRow["username"]);
	$password=stripslashes($geUserRow["password"]);
	$email=stripslashes($geUserRow["email"]);
	$user_type=stripslashes($geUserRow["user_type"]);
	$trail_form=stripslashes($geUserRow["trail_form"]);
	$demo_page=stripslashes($geUserRow["demo_page"]);
	$master_list=stripslashes($geUserRow["master_list"]);
	$download_list=stripslashes($geUserRow["download_list"]);
	$payment_info=stripslashes($geUserRow["payment_info"]);
	$licensee_list=stripslashes($geUserRow["licensee_list"]);
	$alliance_list=stripslashes($geUserRow["alliance_list"]);
	$tbc=stripslashes($geUserRow["tbc"]);
	$mode="Edit";
	$value="Update";  
}
else{
	$mode="Add";
	$value="Create";
}
if($user_type==""){
	$user_type=0;
}

if(isset($_POST["username"])){  
	$username=addslashes(trim($_POST["username"]));
	$password=addslashes(trim($_POST["password"]));
	$user_email=addslashes(trim($_POST["user_email"]));
	$usertype=addslashes(trim($_POST["usertype"]));
	$trial_form=addslashes(trim($_POST["trial_form"]));
	$demo_page=addslashes(trim($_POST["demo_page"]));
	$master_list=addslashes(trim($_POST["master_list"]));
	$download_list=addslashes(trim($_POST["download_list"]));
	$payment_info=addslashes(trim($_POST["payment_info"]));
	$licensee_list=addslashes(trim($_POST["licensee_list"]));
	$alliance_list=addslashes(trim($_POST["alliance_info"]));
	$tbc=addslashes(trim($_POST["tbc"]));
	if($mode=="Edit"){
		//
		//$updateQry="update  tbl_users set username='".$username."',password='".$password."',email='".$user_email."',user_type='".$usertype."',trail_form='".$trial_form."',demo_page='".$demo_page."',master_list='".$master_list."',download_list='".$download_list."',payment_info='".$payment_info."',modified_date=now() where user_id='".$user_id."'";
		
		$updateQry="update  tbl_users set username=:username,password=:password,email=:email,user_type=:user_type,trail_form=:trail_form,demo_page=:demo_page,master_list=:master_list,download_list=:download_list,payment_info=:payment_info,licensee_list=:licensee_list,alliance_list=:alliance_list,tbc=:tbc,modified_date=:modified_date where user_id=:user_id";
		$prepupdateQry=$DBCONN->prepare($updateQry);
		$updateRes=$prepupdateQry->execute(array(":username"=>$username,":password"=>$password,":email"=>$user_email,":user_type"=>$usertype,":trail_form"=>$trial_form,":demo_page"=>$demo_page,":master_list"=>$master_list,":download_list"=>$download_list,":payment_info"=>$payment_info,":licensee_list"=>$licensee_list,":alliance_list"=>$alliance_list,":tbc"=>$tbc,":modified_date"=>$dbdatetime,":user_id"=>$user_id));
		//$updateRes=mysql_query($updateQry);
		if($updateRes){
			header("Location:userslist.php");
			exit;
		}
	}
	else{
		//$insertQry="insert into tbl_users(username,password,email,user_type,trail_form,demo_page,master_list,download_list,payment_info,added_date,modified_date) values('".$username."','".$password."','".$user_email."','".$usertype."','".$trial_form."','".$demo_page."','".$master_list."','".$download_list."','".$payment_info."',now(),now())";
		$prepareinsqry=$DBCONN->prepare("insert into tbl_users(username,password,email,user_type,trail_form,demo_page,master_list,download_list,payment_info,added_date,modified_date,licensee_list,alliance_list,tbc) values(:username,:password,:email,:user_type,:trail_form,:demo_page,:master_list,:download_list,:payment_info,:added_date,:modified_date,:licensee_list,:alliance_list,:tbc)");
		$insertRes=$prepareinsqry->execute(array(":username"=>$username,":password"=>$password,":email"=>$user_email,":user_type"=>$usertype,":trail_form"=>$trial_form,":demo_page"=>$demo_page,":master_list"=>$master_list,":download_list"=>$download_list,":payment_info"=>$payment_info,":added_date"=>$dbdatetime,":modified_date"=>$dbdatetime,":licensee_list"=>$licensee_list,":alliance_list"=>$alliance_list,":tbc"=>$tbc));
		//$insertRes=mysql_query($insertQry);
		if($insertRes){
			header("Location:userslist.php");
			exit;
		}
	}
}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<title>MAR Pipeline System</title>
		<link rel="shortcut icon" href="images/Fav.ico" type="image/ico">
		<meta name="Generator" content="EditPlus">
		<meta name="Author" content="">
		<meta name="Keywords" content="">
		<meta name="Description" content="">
		<script type="text/javascript" src="js/jquery.js"></script>
		<script>
		var mode="<?php echo $mode;?>";
		</script>
		<script type="text/javascript" src="js/validate.js"></script>
		<style>
			body{
				margin:0;
				color:#D9D9D9;
				background:#455A68;
				font-family:arial;
			}
			.header{
				height:70px;
				background:#1C242A;
			}
			.content{
				background:#455A68;
				min-height:600px;
			}
			
			.form_actions{
				padding-top:15px;
				padding-left:5px;
				padding-bottom:30px;
			}
			.form_actions .add_btn{
				cursor:pointer;
				border-radius:0px;
				background:#0D0D0D;
				color:#D9D9D9;
				border-color:#D9D9D9;
				padding:5px 15px 5px 15px;
				font-family:arial;

			}
			.list_content{
				width:950px;
				margin-left:40px;
				/*margin-left:auto;
				margin-right:auto;*/

			}
			.tbl_header th{
				font-size:13px;
				border-bottom:1px solid #D9D9D9;
				text-align:left;
				font-family:arial;
			}
			.tbl-body{
				font-size:12px;
				font-family:arial;
			}
			a{
				color:black;
			}
			.inp_feild{
				border-radius:2px;
				border:none;
				width:100%;
			}
		</style>
	</head>
	<body>
		<div>
			<div style="margin-left:auto;margin-right:auto;">
				<div class="header">
					<span style="float:right;margin-right:20px;margin-top:5px;"><a href="logout.php" style="color:white;text-decoration:none;">Logout</a></span>
					<img src="images/myappyrestaurants.png" style="margin-top:10px;margin-left:40px;">
				</div>
				<div class="content">
					<div class="list_content">
						<h1 style="font-size:25px;padding-top:15px;padding-bottom:15px;margin:0px;">New User</h1>
						<form name="user_form" id="user_form" method="post">
						<input type="hidden" name="hdn_userid" id="hdn_userid" value="<?php echo $user_id;?>">
						<table cellspacing="15" cellpadding="0" border="0" width="70%">
							<tr>
								<td style="width:138px;">
									Username<font color="red">*</font>: 
								</td>
								<td>
									<input type="text" name="username" id="username" class="inp_feild" value="<?php echo $username;?>">
								</td>
							</tr>
							<tr>
								<td style="width:138px;">
									Password<font color="red">*</font>: 
								</td>
								<td>
									<input type="text" name="password" id="password" class="inp_feild" value="<?php echo $password;?>">
								</td>
							</tr>
							<tr>
								<td style="width:138px;">
									Email<font color="red">*</font>: 
								</td>
								<td>
									<input type="text" name="user_email" id="user_email" class="inp_feild" value="<?php echo $email;?>">
								</td>
							</tr>
							<tr>
								<td style="width:138px;">
									User Type<font color="red">*</font>: 
								</td>
								<td>
									<select name="usertype" id="usertype" class="inp_feild">
										<option value="0">Select</option>
										<option value="User">User</option>
										<option value="Admin">Admin</option>
										<option value="Developer">Developer</option>
										<option value="Licensee">Licensee</option>
										<option value="Affiliate">Affiliate</option>
										<option value="Agent">Agent</option>
									</select>
									<script>
									$("#usertype").val("<?php echo $user_type;?>");
									</script>
								</td>
							</tr>
							<tr>
								<td style="width:138px;" valign="top">
									User Privileges: 
								</td>
								<td>
									<input type="checkbox" name="trial_form" id="trial_form" value="yes"<?php if($trail_form=="yes") echo " checked";?>>Prospect/Trial<br>
									<input type="checkbox" name="demo_page" id="demo_page" value="yes"<?php if($demo_page=="yes") echo " checked";?>>Demo Page<br>
									<input type="checkbox" name="master_list" id="master_list" value="yes"<?php if($master_list=="yes") echo " checked";?>>Master List<br>
									<input type="checkbox" name="download_list" id="download_list" value="yes"<?php if($download_list=="yes") echo " checked";?>>Download Lists<br>
									<input type="checkbox" name="payment_info" id="payment_info" value="yes"<?php if($payment_info=="yes") echo " checked";?>>Payment Info<br>
									<input type="checkbox" name="licensee_list" id="licensee_list" value="yes"<?php if($licensee_list=="yes") echo " checked";?>>Licensee List<br>
									<input type="checkbox" name="alliance_info" id="alliance_info" value="yes"<?php if($alliance_list=="yes") echo " checked";?>>Simulator list<br>
									<input type="checkbox" name="tbc" id="tbc" value="yes"<?php if($tbc=="yes") echo " checked";?>>TBC<br>
								</td> 
							</tr>
							<tr>
							     <td>
									<div class="form_actions" style="text-align:left;">
										<input type="button" value="Back To Users List" class="add_btn"  onclick="document.location='userslist.php'">
									
								</td>
								<td>
									  <div class="form_actions" style="text-align:right;">
										<input type="button" value="<?php echo $value;?> User" class="add_btn" id="add_user">
									</div>
								</td>
							</tr>
						</table>
						</form>
					</div>
					
				</div>
			</div>
		</div>
	</body>
</html>