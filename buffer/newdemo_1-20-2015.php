<?php
$simulator='1';
$parent_directory=basename(dirname($_SERVER["PHP_SELF"]));
$filename=basename($_SERVER["PHP_SELF"]);
include("../includes/configure.php");
include("includes/cmm_functions.php");
include("../includes/session_check.php");
$id=$_SESSION['user_id'];
$dbdatetime=date('Y-m-d H:i:s',strtotime('now'));
$feat="demo_page";
$typeandfeature=checklogin($id,$feat);
$usrArr=explode("*",$typeandfeature);
$user_type=$usrArr[0];
$feature=$usrArr[1];
$dowfeature=$usrArr[2];
$trialfeature=$usrArr[4];
$paymentf=$usrArr[3];
$demofeature=$usrArr[6];
if($demofeature!="yes"){
	header("Location:noauthorised.php");
	exit;
}
else{
$demoId=$_GET["demo_id"];
if($demoId!=""){
	//$demoId
	$getDemoQry="select * from tbl_demo where demo_id=:demoid";
	$pepDemoQry=$DBCONN->prepare($getDemoQry);
	$pepDemoQry->execute(array(':demoid'=>$demoId));
	//$getDemoRes=mysql_query($getDemoQry);
	$getDemoRow=$pepDemoQry->fetch();
	$first_name=stripslashes($getDemoRow["first_name"]);
	$sur_name=stripslashes($getDemoRow["sur_name"]);
	$business_name=stripslashes($getDemoRow["business_name"]);
	$work_phone=stripslashes($getDemoRow["work_phone"]);
	$mobile_phone=stripslashes($getDemoRow["mobile_phone"]);
	$email=stripslashes($getDemoRow["email"]);
	$facebook_link=stripslashes($getDemoRow["facebook_link"]);
	$twitter_link=stripslashes($getDemoRow["twitter_link"]);
	$old_restaurant_logo=stripslashes($getDemoRow["restaurant_logo"]);
	$book_table=stripslashes($getDemoRow["book_table"]);
	$take_away=stripslashes($getDemoRow["take_away"]);
	$res1_name=stripslashes($getDemoRow["res1_name"]);
	$res1_address=stripslashes($getDemoRow["res1_address"]);
	$res1_suburb=stripslashes($getDemoRow["res1_suburb"]);
	$res1_state=stripslashes($getDemoRow["res1_state"]);
	$res1_postcode=stripslashes($getDemoRow["res1_postcode"]);
	$res1_country=stripslashes($getDemoRow["res1_country"]);
	$res1_workphone=stripslashes($getDemoRow["res1_workphone"]);
	$res1_email=stripslashes($getDemoRow["res1_email"]);
	$res2_name=stripslashes($getDemoRow["res2_name"]);
	$res2_address=stripslashes($getDemoRow["res2_address"]);
	$res2_suburb=stripslashes($getDemoRow["res2_suburb"]);
	$res2_state=stripslashes($getDemoRow["res2_state"]);
	$res2_postcode=stripslashes($getDemoRow["res2_postcode"]);
	$res2_country=stripslashes($getDemoRow["res2_country"]);
	$res2_workphone	=stripslashes($getDemoRow["res2_workphone"]);
	$res1_workphone=stripslashes($getDemoRow["res1_workphone"]);
	$res2_email=stripslashes($getDemoRow["res2_email"]);
	$res3_name=stripslashes($getDemoRow["res3_name"]);
	$res3_address=stripslashes($getDemoRow["res3_address"]);
	$res3_suburb=stripslashes($getDemoRow["res3_suburb"]);
	$res3_state=stripslashes($getDemoRow["res3_state"]);
	$res3_postcode=stripslashes($getDemoRow["res3_postcode"]);
	$res3_country=stripslashes($getDemoRow["res3_country"]);
	$res3_workphone	=stripslashes($getDemoRow["res3_workphone"]);
	$res3_email=stripslashes($getDemoRow["res3_email"]);
	$udid1=stripslashes($getDemoRow["udid1"]);
	$udid2=stripslashes($getDemoRow["udid2"]);
	$udid3=stripslashes($getDemoRow["udid3"]);
	$default_langauage=stripslashes($getDemoRow["default_langauage"]);
	$sales_person=stripslashes($getDemoRow["sales_person"]);
	$app_name=stripslashes($getDemoRow["app_name"]);
	$itunes_category=stripslashes($getDemoRow["itunes_category"]);
	//$old_itunes_category=$itunes_category;
	$demo_page_url=stripslashes($getDemoRow["demo_page_url"]);
	$app_io_link=stripslashes($getDemoRow["app_io_link"]);
	$app_io_password=stripslashes($getDemoRow["app_io_password"]);
	$diawi_link=stripslashes($getDemoRow["diawi_link"]);
	$back_end_password=stripslashes($getDemoRow["back_end_password"]);
	$itunes_url=stripslashes($getDemoRow["itunes_url"]);
	$status=stripslashes($getDemoRow["status"]);
	$old_itunes_category=$status;
	$sales_person_initial_name=stripslashes($getDemoRow["sales_person_initial"]);
    $sales_manager_initial_name=stripslashes($getDemoRow["sales_manager_initial"]);
	$developer_id=stripslashes($getDemoRow["developer_id"]);
	$website_link=stripslashes($getDemoRow["website_link"]);
	$development_notes=stripslashes($getDemoRow["development_notes"]);
	$itunes_description=stripslashes($getDemoRow["itunes_description"]);
	$homepageversion=stripslashes($getDemoRow["homepageversion"]);
	$territory=stripslashes($getDemoRow["territory"]);
	$product_page_val=stripslashes($getDemoRow["product_page"]);
    $demo_time=stripslashes($getDemoRow["demo_time"]);
	$old_theme_logo=stripslashes($getDemoRow["theme_logo"]);
	$theme_logo=stripslashes($getDemoRow["theme_logo"]);
	$business_type=stripslashes($getDemoRow['business_type']);
	$theme=stripslashes($getDemoRow["theme"]);
	$affiliate_id=$getDemoRow['a_id'];
	$geUserQry="select * from  tbl_affiliate where affiliate_id=:affiliate_id";
	$prepgetuserqry=$DBCONN->prepare($geUserQry);
	$prepgetuserqry->execute(array(":affiliate_id"=>$affiliate_id));
	//$geUserRes=mysql_query($geUserQry);
	$geUserRow=$prepgetuserqry->fetch();
	$Licensee=stripslashes($geUserRow["l_id"]);

    if($demo_time!="" && $demo_time!="00:00:00"){
	  $demo_time=date('g:i A',strtotime(stripslashes($getDemoRow["demo_time"])));
	}
	else{
      $demo_time="";
	}
	$oldeveloper=$developer_id;
	$oldstatus=$status;
	if($getDemoRow["payment_date"]!=""){
		if($getDemoRow["payment_date"]!="0000-00-00"){
			$paymentdate=date('d-m-Y',strtotime(stripslashes($getDemoRow["payment_date"])));
		}
		else{
			$paymentdate="";
		}
	}
	else{
		$paymentdate="";
	}
	if($getDemoRow["commission_date"]!=""){
		if($getDemoRow["commission_date"]!="0000-00-00"){
			$commisiondate=date('d-m-Y',strtotime(stripslashes($getDemoRow["commission_date"])));
		}
		else{
			$commisiondate="";
		}
	}
	else{
		$commisiondate="";
	}
	if($getDemoRow["dev_qa_date"]!=""){
		if($getDemoRow["dev_qa_date"]!="0000-00-00"){
			$Dev_date=date('d-m-Y',strtotime(stripslashes($getDemoRow["dev_qa_date"])));
		}
		else{
			$Dev_date="";
		}
	}
	else{
		$Dev_date="";
	}
	if($getDemoRow["demo_date"]!=""){
		if($getDemoRow["demo_date"]!="0000-00-00"){
			$demo_date=date('d-m-Y',strtotime(stripslashes($getDemoRow["demo_date"])));
		}
		else{
			$demo_date="";
		}
	}
	else{
		$demo_date="";
	}
	
	$sales_manager=stripslashes($getDemoRow["sales_manager"]);
	$sales_person_initial_id=stripslashes($getDemoRow["sales_person_initial"]);
    $sales_manager_initial_id=stripslashes($getDemoRow["sales_manager_initial"]);
   	$sales_person_initial=$sales_person_initial_id;
	$sales_manager_initial=$sales_manager_initial_id;
	$created_user_id=$getDemoRow['user_id'];
	$geUserQry="select * from  tbl_users where user_id=:user_id";
	$prepgetuserqry=$DBCONN->prepare($geUserQry);
	$prepgetuserqry->execute(array(":user_id"=>$created_user_id));
	//$geUserRes=mysql_query($geUserQry);
	$geUserRow=$prepgetuserqry->fetch();
	$created_user=stripslashes($geUserRow["username"]);
	//$sales_manger_initial
	$mode="Edit";
	$value="Update";
}
else{
	$mode="Add";
	$value="Create";
	$created_user=$_SESSION['user_name'];
}
if(isset($_POST["first_name"])){
	$first_name=addslashes(trim($_POST["first_name"]));
	$sur_name=addslashes(trim($_POST["sur_name"]));
	$business_type=addslashes(trim($_POST["business_type"]));
	$business_name=addslashes(trim($_POST["business_name"]));
	$work_phone=addslashes(trim($_POST["work_phone"]));
	$mobile_phone=addslashes(trim($_POST["mobile_phone"]));
	$email=addslashes(trim($_POST["email"]));
	$Sales_manager=addslashes(trim($_POST["sales_manager"]));
	$facebook_link=addslashes(trim($_POST["facebook_link"]));
	$twitter_link=addslashes(trim($_POST["twitter_link"]));
	$restaurant_logo=addslashes(trim($_POST["restaurant_logo"]));
	$book_a_table_f=addslashes(trim($_POST["book_a_table_f"]));
	$take_away_f=addslashes(trim($_POST["take_away_f"]));
	$restaurant1_name=addslashes(trim($_POST["restaurant1_name"]));
	$street1_address=addslashes(trim($_POST["street1_address"]));
	$suburb1=addslashes(trim($_POST["suburb1"]));
	$state1=addslashes(trim($_POST["state1"]));
	$country1=addslashes(trim($_POST["country1"]));
	$postcode1=addslashes(trim($_POST["postcode1"]));
	$res1_workphone=addslashes(trim($_POST["res1_workphone"]));
	$email1=addslashes(trim($_POST["email1"]));
	$restaurant2_name=addslashes(trim($_POST["restaurant2_name"]));
	$street2_address=addslashes(trim($_POST["street2_address"]));
	$suburb2=addslashes(trim($_POST["suburb2"]));
	$state2=addslashes(trim($_POST["state2"]));
	$country2=addslashes(trim($_POST["country2"]));
	$postcode2=addslashes(trim($_POST["postcode2"]));
	$res2_workphone=addslashes(trim($_POST["res2_workphone"]));
	$email2=addslashes(trim($_POST["email2"]));
	$restaurant3_name=addslashes(trim($_POST["restaurant3_name"]));
	$street3_address=addslashes(trim($_POST["street3_address"]));
	$suburb3=addslashes(trim($_POST["suburb3"]));
	$state3=addslashes(trim($_POST["state3"]));
	$country3=addslashes(trim($_POST["country3"]));
	$postcode3=addslashes(trim($_POST["postcode3"]));
	$res3_workphone=addslashes(trim($_POST["res3_workphone"]));
	$email3=addslashes(trim($_POST["email3"]));
	$udid1=addslashes(trim($_POST["udid1"]));
	$udid2=addslashes(trim($_POST["udid2"]));
	$udid3=addslashes(trim($_POST["udid3"]));
	$def_language=addslashes(trim($_POST["def_language"]));
	$sales_person=addslashes(trim($_POST["sales_person"]));
	$app_name=addslashes(trim($_POST["app_name"]));
	$app_cat=addslashes(trim($_POST["app_cat"]));
	$demo_url=addslashes(trim($_POST["demo_url"]));
	$embd_link=addslashes(trim($_POST["embd_link"]));
	$app_io_password=addslashes(trim($_POST["app_io_password"]));
	$diawi_link=addslashes(trim($_POST["diawi_link"]));
	$backend_password=addslashes(trim($_POST["backend_password"]));
	$itune_url=addslashes(trim($_POST["itune_url"]));
	$itunes_category=addslashes(trim($_POST["itunes_category"]));
	$developer=addslashes(trim($_POST["developer"]));
	$website_link=addslashes(trim($_POST["website_link"]));
	$development_notes=addslashes(trim($_POST["development_notes"]));
	$itunesdescription=addslashes(trim($_POST["itunesdescription"]));
	$homepage_version=addslashes(trim($_POST["homepage_version"]));
	$territory=addslashes(trim($_POST["territory"]));
    $product_page=addslashes(trim($_POST["product_page"]));
	$demo_date=(!empty($_POST["demo_date"]))?date('Y-m-d',strtotime(addslashes(trim($_POST["demo_date"])))):NULL;
	$homepage_version=addslashes(trim($_POST["homepage_version"]));
	$theme=addslashes(trim($_POST["theme"]));
	if(!file_exists("../uploaded_themes")){
			mkdir("../uploaded_themes",0777);
	}
	$upload_theme_name=pathinfo($_FILES["theme_logo"]["name"], PATHINFO_FILENAME);
	$upload_theme_extension=pathinfo($_FILES["theme_logo"]["name"], PATHINFO_EXTENSION);
     if($upload_theme_name!=""){
		 if($upload_theme_name!="$old_theme_logo"){
				if(file_exists($old_theme_logo)){
						unlink($old_theme_logo);
				  }

		 }
		 $renamed_theme_filename=time().".".$upload_theme_extension;
		 $upload_theme=MOVE_THEME_PATH.$renamed_theme_filename;
		 $upload_theme__path=DISPLAY_THEME_PATH.$renamed_theme_filename;
		
		move_uploaded_file($_FILES["theme_logo"]["tmp_name"],$upload_theme);
	}
	else
	{
      $upload_theme__path=$old_theme_logo;
	}
	if(!empty($_POST["demo_time"]))
	{
		$demo_timeArr=explode(" ",addslashes(trim($_POST["demo_time"])));
		if($demo_timeArr[1]=="PM")
			$demo_time=date("H:i:s",strtotime($demo_timeArr[0]."pm"));
		else
			$demo_time=date("H:i:s",strtotime($demo_timeArr[0]."am"));
	}
	else
		$demo_time=NULL;
	if($_POST["payment_date"]!="")
		$paymentdate=date('Y-m-d',strtotime(addslashes(trim($_POST["payment_date"]))));
	else
		$paymentdate=NULL;
	if($_POST["commision_date"]!="")
		$commision_date=date('Y-m-d',strtotime(addslashes(trim($_POST["commision_date"]))));
	else
		$commision_date=NULL;
	 if($_POST["dev_date"]!="")
		$dev_date=date('Y-m-d',strtotime(addslashes(trim($_POST["dev_date"]))));
	else
		$dev_date=NULL;
	if(!file_exists("../".UPLOAD_PATH)){
			mkdir("../".UPLOAD_PATH,0777);
	}
   
	$file_name=pathinfo($_FILES["restaurant_logo"]["name"], PATHINFO_FILENAME);
	$file_extension=pathinfo($_FILES["restaurant_logo"]["name"], PATHINFO_EXTENSION);
	$renamed_filename=$file_name.time().".".$file_extension;
	$file_path=UPLOAD_PATH.$renamed_filename;
	$file_upload_path="../".UPLOAD_PATH.$renamed_filename;
	if($mode=="Edit"&&$file_name!=""){
		if(file_exists($old_restaurant_logo)){
			unlink($old_restaurant_logo);
		}
		move_uploaded_file($_FILES["restaurant_logo"]["tmp_name"],$file_upload_path);
	}
	else if($mode=="Add"){
		move_uploaded_file($_FILES["restaurant_logo"]["tmp_name"],$file_upload_path);
	}
	else{
		$file_path=$old_restaurant_logo;
	}
	$affiliate=$_POST["affiliate"];
	if($mode=="Edit"){
		/*$editcondition="";
		if($sales_person_initial==""){
			$editcondition.=",sales_person_initial='".$sales_person."'";
		}
		if($sales_person_initial==""){
			$editcondition.=",sales_manager_initial='".$Sales_manager."'";
		}
		*/
		    $salesperson_condition="";
			$salesmanager_condition="";
			$bind_values=array(":first_name"=>$first_name,":sur_name"=>$sur_name,":business_name"=>$business_name,":work_phone"=>$work_phone,":mobile_phone"=>$mobile_phone,":email"=>$email,":website_link"=>$website_link,":facebook_link"=>$facebook_link,":twitter_link"=>$twitter_link,":restaurant_logo"=>$file_path,":book_table"=>$book_a_table_f,":take_away"=>$take_away_f,":res1_name"=>$restaurant1_name,":res1_address"=>$street1_address,":res1_suburb"=>$suburb1,":res1_state"=>$state1,":res1_postcode"=>$postcode1,":res1_country"=>$country1,":res1_workphone"=>$res1_workphone,":res1_email"=>$email1,":res2_name"=>$restaurant2_name,":res2_address"=>$street2_address,":res2_suburb"=>$suburb2,":res2_state"=>$state2,":res2_postcode"=>$postcode2,":res2_country"=>$country2,":res2_workphone"=>$res2_workphone,":res2_email"=>$email2,":res3_name"=>$restaurant3_name,":res3_address"=>$street3_address,":res3_suburb"=>$suburb3,":res3_state"=>$state3,":res3_postcode"=>$postcode3,":res3_country"=>$country3,":res3_workphone"=>$res3_workphone,":res3_email"=>$email3,":udid1"=>$udid1,":udid2"=>$udid2,":udid3"=>$udid3,":default_langauage"=>$def_language,":app_name"=>$app_name,":itunes_category"=>$app_cat,":demo_page_url"=>$demo_url,":app_io_link"=>$embd_link,":app_io_password"=>$app_io_password,":diawi_link"=>$diawi_link,":back_end_password"=>$backend_password,":itunes_url"=>$itune_url,":status"=>$itunes_category,":payment_date"=>$paymentdate,":commission_date"=>$commision_date,":dev_qa_date"=>$dev_date,":developer_id"=>$developer,":modified_date"=>$dbdatetime,":development_notes"=>$development_notes,":itunes_description"=>$itunesdescription,":homepageversion"=>$homepage_version,":territory"=>$territory,":demo_date"=>$demo_date,":demo_time"=>$demo_time,":theme"=>$theme,":business_type"=>$business_type,":theme_logo"=>$upload_theme__path,":product_page"=>$product_page,":demo_id"=>$demoId);

			if(($sales_person!="")&&($sales_person_initial_name==NULL)||($sales_person_initial_name=="")||(empty($sales_person_initial_name)))
			{
				//$sales_person
				 $salesperson_condition.="sales_person=:sales_person,sales_person_initial=:sales_person_initial";
				 $bind_values[":sales_person"]=$sales_person;
				 $bind_values[":sales_person_initial"]=$sales_person;
			}
			else
			{
                   $salesperson_condition.="sales_person=:sales_person";
				   $bind_values[":sales_person"]=$sales_person;

			}
			if(($Sales_manager!="")&&($sales_manager_initial_name==NULL)||($sales_manager_initial_name=="")||(empty($sales_manager_initial_name)))
			{
				//$Sales_manager
				 $salesmanager_condition.="sales_manager=:sales_manager,sales_manager_initial=:sales_manager_initial";
				 $bind_values[":sales_manager"]=$Sales_manager;
				$bind_values[":sales_manager_initial"]=$Sales_manager;

			}
			else
			{
                $salesmanager_condition.="sales_manager=:sales_manager";
				$bind_values[":sales_manager"]=$Sales_manager;
			}
			
			
		//$updateQry="update tbl_demo set first_name='".$first_name."',sur_name='".$sur_name."',business_name='".$business_name."',work_phone='".$work_phone."',mobile_phone='".$mobile_phone."',email='".$email."',website_link='".$website_link."',facebook_link='".$facebook_link."',twitter_link='".$twitter_link."',restaurant_logo='".$file_path."',book_table='".$book_a_table_f."',take_away='".$take_away_f."',res1_name='".$restaurant1_name."',res1_address='".$street1_address."',res1_suburb='".$suburb1."',res1_state='".$state1."',res1_postcode='".$postcode1."',res1_country='".$country1."',res1_workphone='".$res1_workphone."',res1_email='".$email1."',res2_name='".$restaurant2_name."',res2_address='".$street2_address."',res2_suburb='".$suburb2."',res2_state='".$state2."',res2_postcode='".$postcode2."',res2_country='".$country2."',res2_workphone='".$res2_workphone."',res2_email='".$email2."',res3_name='".$restaurant3_name."',res3_address='".$street3_address."',res3_suburb='".$suburb3."',res3_state='".$state3."',res3_postcode='".$postcode3."',res3_country='".$country3."',res3_workphone='".$res3_workphone."',res3_email='".$email3."',udid1='".$udid1."',udid2='".$udid2."',udid3='".$udid3."',default_langauage='".$def_language."',app_name='".$app_name."',itunes_category='".$app_cat."',demo_page_url='".$demo_url."',app_io_link='".$embd_link."',app_io_password='".$app_io_password."',diawi_link='".$diawi_link."',back_end_password='".$backend_password."',itunes_url='".$itune_url."',status='".$itunes_category."',payment_date='".$paymentdate."',commission_date='".$commision_date."',dev_qa_date='".$dev_date."',$salesperson_condition,$salesmanager_condition,developer_id='".$developer."',modified_date=now(),development_notes='".$development_notes."',itunes_description='".$itunesdescription."',homepageversion='".$homepage_version."',territory='".$territory."',demo_date='".$demo_date."',demo_time='".$demo_time."' where demo_id='".$demoId."'";
		
		$updt_raw_qry="update tbl_demo set first_name=:first_name,sur_name=:sur_name,business_name=:business_name,work_phone=:work_phone,mobile_phone=:mobile_phone,email=:email,website_link=:website_link,facebook_link=:facebook_link,twitter_link=:twitter_link,restaurant_logo=:restaurant_logo,book_table=:book_table,take_away=:take_away,res1_name=:res1_name,res1_address=:res1_address,res1_suburb=:res1_suburb,res1_state=:res1_state,res1_postcode=:res1_postcode,res1_country=:res1_country,res1_workphone=:res1_workphone,res1_email=:res1_email,res2_name=:res2_name,res2_address=:res2_address,res2_suburb=:res2_suburb,res2_state=:res2_state,res2_postcode=:res2_postcode,res2_country=:res2_country,res2_workphone=:res2_workphone,res2_email=:res2_email,res3_name=:res3_name,res3_address=:res3_address,res3_suburb=:res3_suburb,res3_state=:res3_state,res3_postcode=:res3_postcode,res3_country=:res3_country,res3_workphone=:res3_workphone,res3_email=:res3_email,udid1=:udid1,udid2=:udid2,udid3=:udid3,default_langauage=:default_langauage,app_name=:app_name,itunes_category=:itunes_category,demo_page_url=:demo_page_url,app_io_link=:app_io_link,app_io_password=:app_io_password,diawi_link=:diawi_link,back_end_password=:back_end_password,itunes_url=:itunes_url,status=:status,payment_date=:payment_date,commission_date=:commission_date,dev_qa_date=:dev_qa_date,developer_id=:developer_id,{$salesperson_condition},{$salesmanager_condition},modified_date=:modified_date,development_notes=:development_notes,itunes_description=:itunes_description,homepageversion=:homepageversion,territory=:territory,demo_date=:demo_date,demo_time=:demo_time,theme=:theme,theme_logo=:theme_logo,business_type=:business_type,product_page=:product_page where demo_id=:demo_id";

		$prepupdateQry=$DBCONN->prepare($updt_raw_qry);

		$demores=$prepupdateQry->execute($bind_values);
		
		//$demoqry=mysql_query($updateQry);
		
		if($demores){
				header("Location:masterlist.php");
		exit;
		}
		
	}
	else{
	//$insertQry="insert into tbl_demo(first_name,sur_name,business_name,work_phone,mobile_phone,email,website_link,facebook_link,twitter_link,restaurant_logo,book_table,take_away,res1_name,res1_address,res1_suburb,res1_state,res1_postcode,res1_country,res1_workphone,res1_email,res2_name,res2_address,res2_suburb,res2_state,res2_postcode,res2_country,res2_workphone,res2_email,res3_name,res3_address,res3_suburb,res3_state,res3_postcode,res3_country,res3_workphone,res3_email,udid1,udid2,udid3,default_langauage,sales_person,app_name,itunes_category,demo_page_url,app_io_link,app_io_password,diawi_link,back_end_password,itunes_url,status,payment_date,commission_date,dev_qa_date,sales_manager,developer_id,sales_person_initial,sales_manager_initial,added_date,modified_date,development_notes,itunes_description,homepageversion,territory,demo_date,demo_time) values('".$first_name."','".$sur_name."','".$business_name."','".$work_phone."','".$mobile_phone."','".$email."','".$website_link."','".$facebook_link."','".$twitter_link."','".$file_path."','".$book_a_table_f."','".$take_away_f."','".$restaurant1_name."','".$street1_address."','".$suburb1."','".$state1."','".$postcode1."','".$country1."','".$res1_workphone."','".$email1."','".$restaurant2_name."','".$street2_address."','".$suburb2."','".$state2."','".$postcode2."','".$country2."','".$res2_workphone."','".$email2."','".$restaurant3_name."','".$street3_address."','".$suburb3."','".$state3."','".$postcode3."','".$country3."','".$res3_workphone."','".$email3."','".$udid1."','".$udid2."','".$udid3."','".$def_language."','".$sales_person."','".$app_name."','".$app_cat."','".$demo_url."','".$embd_link."','".$app_io_password."','".$diawi_link."','".$backend_password."','".$itune_url."','".$itunes_category."','".$paymentdate."','".$commission_date."','".$dev_date."','".$Sales_manager."','".$developer."','".$sales_person."','".$Sales_manager."',now(),now(),'".$development_notes."','".$itunesdescription."','".$homepage_version."','".$territory."','".$demo_date."','".$demo_time."')";



	$prepinsertQry=$DBCONN->prepare("insert into tbl_demo(first_name,sur_name,business_name,work_phone,mobile_phone,email,website_link,facebook_link,twitter_link,restaurant_logo,book_table,take_away,res1_name,res1_address,res1_suburb,res1_state,res1_postcode,res1_country,res1_workphone,res1_email,res2_name,res2_address,res2_suburb,res2_state,res2_postcode,res2_country,res2_workphone,res2_email,res3_name,res3_address,res3_suburb,res3_state,res3_postcode,res3_country,res3_workphone,res3_email,udid1,udid2,udid3,default_langauage,sales_person,app_name,itunes_category,demo_page_url,app_io_link,app_io_password,diawi_link,back_end_password,itunes_url,status,payment_date,commission_date,dev_qa_date,sales_manager,developer_id,sales_person_initial,sales_manager_initial,added_date,modified_date,development_notes,itunes_description,homepageversion,territory,demo_date,demo_time,theme,theme_logo,product_page,a_id,user_id,business_type) values(:first_name,:sur_name,:business_name,:work_phone,:mobile_phone,:email,:website_link,:facebook_link,:twitter_link,:restaurant_logo,:book_table,:take_away,:res1_name,:res1_address,:res1_suburb,:res1_state,:res1_postcode,:res1_country,:res1_workphone,:res1_email,:res2_name,:res2_address,:res2_suburb,:res2_state,:res2_postcode,:res2_country,:res2_workphone,:res2_email,:res3_name,:res3_address,:res3_suburb,:res3_state,:res3_postcode,:res3_country,:res3_workphone,:res3_email,:udid1,:udid2,:udid3,:default_langauage,:sales_person,:app_name,:itunes_category,:demo_page_url,:app_io_link,:app_io_password,:diawi_link,:back_end_password,:itunes_url,:status,:payment_date,:commission_date,:dev_qa_date,:sales_manager,:developer_id,:sales_person_initial,:sales_manager_initial,:added_date,:modified_date,:development_notes,:itunes_description,:homepageversion,:territory,:demo_date,:demo_time,:theme,:theme_logo,:product_page,:a_id,:user_id,:business_type)");
	
	
	$demoqry=$prepinsertQry->execute(array(":first_name"=>$first_name,
		":sur_name"=>$sur_name,
		":user_id"=>$_SESSION['user_id'],
		":business_name"=>$business_name,
		":work_phone"=>$work_phone,
		":mobile_phone"=>$mobile_phone,
		":email"=>$email,
		":website_link"=>$website_link,
		":facebook_link"=>$facebook_link,
		":twitter_link"=>$twitter_link,
		":restaurant_logo"=>$file_path,
		":book_table"=>$book_a_table_f,
		":take_away"=>$take_away_f,
		":res1_name"=>$restaurant1_name,
		":res1_address"=>$street1_address,
		":res1_suburb"=>$suburb1,
		":res1_state"=>$state1,
		":res1_postcode"=>$postcode1,
		":res1_country"=>$country1,
		":res1_workphone"=>$res1_workphone,
		":res1_email"=>$email1,
		":res2_name"=>$restaurant2_name,
		":res2_address"=>$street2_address,
		":res2_suburb"=>$suburb2,
		":res2_state"=>$state2,
		":res2_postcode"=>$postcode2,
		":res2_country"=>$country2,
		":res2_workphone"=>$res2_workphone,
		":res2_email"=>$email2,
		":res3_name"=>$restaurant3_name,
		":res3_address"=>$street3_address,
		":res3_suburb"=>$suburb3,
		":res3_state"=>$state3,
		":res3_postcode"=>$postcode3,
		":res3_country"=>$country3,
		":res3_workphone"=>$res3_workphone,
		":res3_email"=>$email3,
		":udid1"=>$udid1,
		":udid2"=>$udid2,
		":udid3"=>$udid3,
		":default_langauage"=>$def_language,
		":sales_person"=>$sales_person,
		":app_name"=>$app_name,
		":itunes_category"=>$app_cat,
		":demo_page_url"=>$demo_url,
		":app_io_link"=>$embd_link,
		":app_io_password"=>$app_io_password,
		":diawi_link"=>$diawi_link,
		":back_end_password"=>$backend_password,
		":itunes_url"=>$itune_url,
		":status"=>$itunes_category,
		":payment_date"=>$paymentdate,
		":commission_date"=>$commission_date,
		":dev_qa_date"=>$dev_date,
		":sales_manager"=>$Sales_manager,
		":developer_id"=>$developer,
		":sales_person_initial"=>$sales_person,
		":sales_manager_initial"=>$Sales_manager,
		":added_date"=>$dbdatetime,
		":modified_date"=>$dbdatetime,
		":development_notes"=>$development_notes,
		":itunes_description"=>$itunesdescription,
		":homepageversion"=>$homepage_version,
		":territory"=>$territory,
		":demo_date"=>$demo_date,
		":demo_time"=>$demo_time,
		":theme"=>$theme,
		":theme_logo"=>$upload_theme__path,
		":product_page"=>$product_page,
		 ":a_id"=>$affiliate,
		 ":business_type"=>$business_type));
		
		if($prepinsertQry->errorCode()> 0)
		{
			$arrerrors = $prepinsertQry->errorInfo();
			//print_r($arrerrors);
		}
		$demoId=$DBCONN->lastInsertId();
		//$demoqry=mysql_query($insertQry);
		//$demoId=mysql_insert_id();
	}

	if($demoqry)
	{
		
		//$sales_person
		$sqlqry	="select salesperson from tbl_sales_person where salesperson=:sales_person";
		$prepsqlqry=$DBCONN->prepare($sqlqry);
		//$sql	=mysql_query($sqlqry);
		$prepsqlqry->execute(array(':sales_person'=>$sales_person));
		if($prepsqlqry->rowCount() > 0)
		{
			$fetch	= $prepsqlqry->fetch();
			$salesperson=$fetch["salesperson"];
		}
		$chkall=0;
		$postedcnt=count($_POST);
		foreach($_POST as $postedvalue){
			if(empty($postedvalue))
				$chkall++;
		}
		$mailflag=0;
		if($itunes_category!=$old_itunes_category){
			//$itunes_category
            $emailqry="select * from   tbl_status where status=:itemcategory";
			$prepemailqry=$DBCONN->prepare($emailqry);
			$emailres=$prepemailqry->execute(array(':itemcategory'=>$itunes_category));
			//$emailres=mysql_query($emailqry);
			//$emailrow=mysql_fetch_array($emailres);
			$emailrow=$prepemailqry->fetch();
			$email=stripslashes($emailrow["email"]);

            $templateqry="select * from   tbl_emailtemplates where title='Status mail'";
			$templateres=mysql_query($templateqry);
			$templaterow=mysql_fetch_array($templateres);
			$from='cedric@myappyrestaurant.com';
			$to=$email;
			$subject=stripslashes($templaterow["subject"]);
			$subject=str_replace('$businessname',$business_name,$subject);
			$subject=str_replace('$status',$itunes_category,$subject);
			$message=stripslashes($templaterow["message"]);
			$message=str_replace('$businessname',$business_name,$message);
			$message=str_replace('$status',$itunes_category,$message);
			$mailflag=1;
		}
		if($mailflag==1)
		sendEmail($from,$to,$subject,$message);
		header("Location:masterlist.php");
		exit;
		
	}
}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<title>MAR Pipeline System</title>
		<link rel="shortcut icon" href="images/Fav.ico" type="image/ico">
		<meta name="Generator" content="EditPlus">
		<meta name="Author" content="">
		<meta name="Keywords" content="">
		<meta name="Description" content="">
		<script type="text/javascript" src="js/jquery.js"></script>
		<script>
		var mode="<?php echo $mode;?>";
		</script>
		<script type="text/javascript" src="js/validate.js"></script>
		 <link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css" rel="stylesheet" type="text/css"/> 
		 <link href="css/jquery.datetimepicker.css" rel="stylesheet" type="text/css"/> 
		   <!-- <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.5/jquery.min.js"></script>  
		   <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/jquery-ui.min.js"></script>  -->
		  <script type="text/javascript" src="js/jquery.datetimepicker.js"></script> 
		  <link href="css/new.css" rel="stylesheet" type="text/css"/> 
			
		   <script>
				
			   $(function() {
				   $('#demo_time').datetimepicker({
					datepicker:false,
					formatTime:'g:i A',
					format:'g:i A',
					step:5
					
					});
						<?php
						if($paymentf=="yes"){
						?>
					   $("#payment_date").datetimepicker({ format:'d-m-Y',formatDate:'d-m-Y',timepicker:false});
					   $("#commision_date").datetimepicker({ format:'d-m-Y',formatDate:'d-m-Y',timepicker:false});
					   $("#dev_date").datetimepicker({ format:'d-m-Y',formatDate:'d-m-Y',timepicker:false});
					    <?php
						}
						  ?>
					$("#demo_date").datetimepicker({ format:'d-m-Y',formatDate:'d-m-Y',timepicker:false});
					
			   });
			function show(id){
			if($("#"+id).is(':checked'))
			$(".theme_div").toggle();
			else
			$(".theme_div").toggle();


			}
			   
		   </script>
			  
		
		 
		<script>
		function getmanger(salespersonid){
			//alert(salespersonid);
			$.ajax({url:"getmanger.php?salespersonid="+salespersonid,success:function(result){
				$("#sales_manager").val(result);
		    }});
		}
		function get_Licensee(affiliate_id){
			$.ajax({url:"get_licensee.php?affiliate_id="+affiliate_id,success:function(result){
				$("#licensee").val(result);
				$("#affiliate").val(affiliate_id);
		    }});
		}
		function setcode(val) {
			res = val.replace(/([~!@#$%^&*()_+=`{}\[\]\|\\:;'<>,.\/? ])+/g, '').replace(/^(-)+|(-)+$/g,'');
			var res = res.substring(0,4);
			var res = res.toUpperCase();
			var chars = "0123456789";
			var string_length = 3;
			var randomstring = '';
			for (var i=0; i<string_length; i++) {
				var rnum = Math.floor(Math.random() * chars.length);
				randomstring += chars.substring(rnum,rnum+1);
				
			}
			 var extra="0";
		     var promocode=res+randomstring+extra;
			 var promocode_id=$("#hidden_special").val();
		     if(promocode_id==""){
			    promocode_id=0;
		     }
			 $.ajax({url:"check_promo.php?name="+promocode+"&id="+promocode_id,success:function(result){
			 if(parseInt(result)>0){
               var business_val=$("#business_name").val();
				  setcode(business_val) ;    
			}
			else{
				
				$('#promo_code').val(promocode);
			  }
			  }});
			}
		 
		</script>
	</head>
	<body>
		<div>
			<div style="margin-left:auto;margin-right:auto;">
				<!-- <div class="header">
					<img src="images/myappyrestaurants.png" style="margin-top:10px;margin-left:40px;">
				</div> -->
				<div class="header">
					<span style="float:right;margin-right:20px;margin-top:5px;"><a href="logout.php" style="color:white;text-decoration:none;">Logout</a></span>
					<div style="width:990px;height:80px;">
					
						 <div style="float:left;width:55%;">
							<img src="images/myappyrestaurants.png">
							 <div style="float:right;width:65%;margin-top:20px;width:32%;"><font style="font-family:arial;color:white;size:40px;" size="5px;">Master Details</font></div> 
						 </div>
						 <?php
							 if($user_type=="admin"||$dowfeature=="yes"||$trialfeature=="yes"){
						 ?>
						 <div class="form_actions" style="float:right;border:1 px solid red;width:30%;text-align:right;">
							   <input type="button" value="Admin Features" class="add_btn" onclick="document.location='admin_features.php'"  >
						  </div>
						 <?php
						 }
						 ?>
					</div> 
				</div>
				<div class="content">
					<div class="list_content">
					    <table cellspacing="15" cellpadding="0" border="0" width="75%">
							<!-- <tr>
									<td>
										<table cellspacing="0" cellpadding="0" width="100%" border="0">
											<tr>
												<td>
													<div class="form_actions">
														<input type="button" value="Back to Prospecting List" onclick="document.location='prospectinglist.php'" class="add_btn">
													</div>
												</td>
											</tr>
										</table>
										</td>
									</tr> -->
									<tr>
									<td colspan="2">
										<table cellspacing="0" cellpadding="0" width="100%" border="0">
											<tr>
												<td>
												   <div class="form_actions">
												   <input type="button" value="BACK TO MASTER LIST" class="add_btn"   onclick="document.location='masterlist.php'">
												</div>
												 
												</td>										 
											 	<td>
												<div class="form_actions" style="text-align:right;">
													      <input type="button" value="<?php echo ucfirst($value);?> Record" class="add_btn add_demo" id="add_demo">  
												</div>
												
												</td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
						<!-- <table cellspacing="15" cellpadding="0" border="0" width="90%">
							<tr>
								<td>
								<div class="form_actions" style="text-align:left;position:relative;">
								<input type="button" value="BACK TO MASTER LIST" class="add_btn" onclick="document.location='masterlist.php'">

								</td>
								
							</tr>
						</table> -->
						 
						<form name="demo_form" id="demo_form" method="post" enctype="multipart/form-data">
						<input type="hidden" name="hdn_img" id="hdn_img" value="<?php echo $old_special_logo;?>">
							<table cellspacing="15" cellpadding="0" border="0" width="75%">
								<tr>
								<td colspan="2">
										<h3><u>SIMULATOR INFO:</u></h3>
									</td>
								</tr>
								<?php
									if($msg!="")
									{
								?>
								<tr bgcolor="white" height="40px" id="error_message">
									<td style="color:black;font-size:20px;font-family:arial;margin-left:10px;" colspan="2">Client trial form added successfully.</td>
								</tr>
								<?php
									}
								?>
								<tr>
									<td style="width:168px;">
										Business Name<font color="red">*</font>:
									</td>
									<td>
									    
										<input type="text" name="business_name" id="business_name" class="inp_feild" value="<?php echo $business_name;?>" tabindex="1"  onkeyup="setcode(this.value)"  >
									</td>
								</tr>
								<tr>
									<td>
										Business Type<font color="red">*</font>:
									</td>
									<td>
										
										<select name="business_type"  id="business_type" class="inp_feild" tabindex="2">
									    <option value="0">Select</option>
										<?php
										$getbusinessQry="select * from  tbl_business_type order by display_order asc";
										$prepget_business_qry=$DBCONN->prepare($getbusinessQry);
										$prepget_business_qry->execute();
										while($getbusinessRow=$prepget_business_qry->fetch()){
										?>
										<option value="<?php echo $getbusinessRow["business_type"];?>" <?php if($business_type==$getbusinessRow["business_type"]){ echo "selected";}?>><?php echo $getbusinessRow["business_type"];?></option>
										<?php
										}
										?>
									</select>
									</td>
								</tr>
								<tr>
									<td>
										Email Address<font color="red">*</font>:
									</td>
									<td>
									 
										<input type="text" name="email" id="email" class="inp_feild" value="<?php echo $email;?>" tabindex="3">
									</td>
								</tr>
								<tr>
								<td style="width:138px;" valign="top">
									Product Page<font color="red">*</font>:
								</td>
								<td>
									<select name="product_page" id="product_page" class="inp_feild" style="width:50%;" tabindex="4">
									    <option value="0">Select</option>
										<?php
										if($_SESSION['user_id']=="3" || $user_type=="Admin"){
												$getQry_1="SELECT  DISTINCT `product_page` FROM `tbl_apps`  order by `product_page` asc";
												$prepgetQry1=$DBCONN->prepare($getQry_1);
												$prepgetQry1->execute();
												while($getRow1=$prepgetQry1->fetch()){
												   $y=$getRow1["product_page"];
												   echo "<option value='".$y."'>".$y."</option>";	
												}

								}
										else{
												$getQry_1="SELECT * FROM  tbl_affiliate where a_user_id='".$_SESSION['user_id']."'";
												$prepgetQry1=$DBCONN->prepare($getQry_1);
												$prepgetQry1->execute();
												while($getRow1=$prepgetQry1->fetch()){
												   $x=$getRow1["product_page1"];
												   $y=$getRow1["product_page2"];
												   $z=$getRow1["product_page3"];
												   if(!empty($x)){
												      echo "<option value='".$x."'>".$x."</option>";
												   }
												    if(!empty($y)){
												      echo "<option value='".$y."'>".$y."</option>";
												   }
												     if(!empty($z)){
												      echo "<option value='".$z."'>".$z."</option>";
												   }


												}
										}
										
										?>
										
									</select>
									<?php
									if($product_page_val!=""){
									?>
									<script language="javascript">document.getElementById("product_page").value="<?php echo $product_page_val;?>"</script>
									<?php
									}
									?>
										
								</td>
							</tr>
							<tr>
								<td style="width:117px;">
									 Promo Code<font color="red">*</font>:
								</td>
								<td>
									<input type="text" name="promo_code" id="promo_code" class="inp_feild" value="<?php echo $promo_code;?>" style="width:50%" tabindex="5">
								</td>
							</tr>
							<tr>
							<td style="width:138px;" valign="top">
									 
								</td>
								<td>
									<input type="checkbox" name="simulator"   id="simulator"  value="1" <?php if($simulator=="1"){echo "checked";}?> onclick="show(this.id)" tabindex="6"> Add Simulator <br>
								</td>
							</tr>

							
							<tr class="theme_div" style="margin-top:5px;<?php echo $style;?>">
								 
							 
								<td valign="top">
									Business Logo:
								</td>
								<td>
									<input type="file" name="restaurant_logo" id="restaurant_logo" class="inp_feild" value="<?php echo $restaurant_name;?>" tabindex="7">
									<?php 
									if(is_file("../".$old_restaurant_logo)){
											$pieces_old_restaurant_logo = explode(".", $old_restaurant_logo);
											$photo_length=count($pieces_old_restaurant_logo);
											$photo_upload_extention_db=$pieces_old_restaurant_logo[$photo_length-1];
											if(strtolower($photo_upload_extention_db)=="pdf"){
												?>
												 <br>
												 <a href="<?php echo  HTTP_ROOT_FOLDER.$old_restaurant_logo;?>" target="_blank" ><img src="http://myappyrestaurant.com/admin/images/pdf.jpg" style="width: 35px;height: 35px;cursor_pointer;padding-top: 8px;"></a>

												<?php
                                             }
											else if(is_file("../".$old_restaurant_logo)){ 
												?>
                                                  <br>
										            <img src="<?php echo "../".$old_restaurant_logo?>" style="width:75px;height:75px;">

												<?php
											}
                                     }
												
							 	 	?>
								</td>
						 
									 
							</tr>
							<tr class="theme_div" style="margin-top:5px;<?php echo $style;?>" >
								<td style="width:138px;" valign="top"> 
									Design Style<font color="red">*</font>: 
								</td>
								<td>
									
									 
										<select name="theme" id="theme" class="inp_feild" tabindex="8">
											<option value=" ">Select</option>
											<?php
											foreach($themes_array as $val=>$name){ 
											  echo '<option value="'.$val.'">'.$name.'</option>'; 
											} 
											?>
											 
										</select>
										<?php 
										 if($business_theme!="" && $business_theme>0){
											?>
										<script type="text/javascript">
										   $("#theme").val('<?php echo $business_theme;?>')
										</script>
										<?php 
										  }
										?>
								 						 
								</td>
							</tr>
							<tr>
								<td style="width:138px;">
									Simulator Creator<font color="red">*</font>:
								</td>
								<td>
									 <input type="text" name="user_id" id="user_id" class="inp_feild" tabindex="9" value="<?php echo stripslashes($created_user);?>"  readonly >
								</td>
							</tr>
							<tr>
								<td colspan="2">
										<h3><u>CONTACT INFO:</u></h3>
									</td>
							</tr>
							<tr>
								<td style="width:168px;">
										Full Name<?php if($prospect_id==""){echo "<font color=\"red\">*</font>";} ?>:
									</td>
									<td>
									 
										<input type="text" name="first_name" id="first_name" class="inp_feild" value="<?php echo $first_name;?>" tabindex="10">
									</td>
								</tr>
								<tr>
									<td>
									Address<?php if($prospect_id==""){echo "<font color=\"red\">*</font>";} ?>:
									</td>
									<td>
										<input type="text" name="street1_address" id="street1_address" class="inp_feild" value="<?php echo $res1_address;?>" tabindex="11">
									</td>
								</tr>
								<tr>
									<td>
										Suburb:
									</td>
									<td>
										<input type="text" name="suburb1" id="suburb1" class="inp_feild" value="<?php echo $res1_suburb;?>" tabindex="12">
									</td>
								</tr>
								<tr>
									<td>
										State:
									</td>
									<td>
										<input type="text" name="state1" id="state1" class="inp_feild" style="width:97%;" value="<?php echo $res1_state;?>"  tabindex="13">
									</td>
								</tr>
								<tr>
								<td>
									Country<font color="red">*</font>:
								</td>
								<td>
									<select name="country1" id="country1" class="inp_feild"  tabindex="14">
										
										<option value="0"<?php if($res1_country=="0") echo " selected";?>>Select</option>
										<?php
											$getcountrQry="select * from  tbl_country";
											$getcountryRes=mysql_query($getcountrQry);
											
											while($getcountryRow=mysql_fetch_array($getcountryRes)){
										   ?>
											<option value="<?php echo stripslashes($getcountryRow["country"]);?>"<?php if($res1_country==$getcountryRow["country"]) echo " selected";?>><?php echo stripslashes($getcountryRow["country"]);?></option>
										<?php
											}
										?>
									</select>

								</td>
								
							</tr>
					 			<tr>
									<td>
										Work Phone<font color="red">*</font>:
									</td>
									<td>
										<input type="text" name="res1_workphone" id="res1_workphone" class="inp_feild" value="<?php echo $res1_workphone;?>" tabindex="15">
									</td>
								</tr>
								<tr>
									<td>
										Mobile Phone:
									</td>
									<td>
										<input type="text" name="mobile_phone" id="mobile_phone" class="inp_feild" value="<?php echo $mobile_phone;?>" tabindex="16">
									</td>
								</tr>
								<tr>
								<td colspan="2">
										<h3><u>SALES INFO:</u></h3>
									</td>
							  </tr>
							  <tr>
									<td>
										Call Status<font color="red">*</font>:
									</td>
									<td>
										<select name="callstatus" id="callstatus" class="inp_feild" style="width:50%;" tabindex="17"  >
											
											<option value="0">Select</option>
											<?php
												$getcallstatusQry="select * from  tbl_callstatus order by display_order asc";
												$getcallstatusRes=mysql_query($getcallstatusQry);
												$callStsArr="";
												while($getcallstatusRow=mysql_fetch_array($getcallstatusRes)){
													if($dbpush=='yes'){
														if($getcallstatusRow["callstatus"]=='Trial requested' || $getcallstatusRow["callstatus"]=='Activation email sent (follow-up)' || $getcallstatusRow["callstatus"]=='Payment processed' || $getcallstatusRow["callstatus"]=='Trial not sold' ||  $getcallstatusRow["callstatus"]=='Promo email sent (follow-up)'){
															$callStsArr[]=stripslashes($getcallstatusRow["callstatus"]);
														}
													}
													else{
														if($getcallstatusRow["callstatus"]!='Activation email sent (follow-up)' && $getcallstatusRow["callstatus"]!='Trial not sold'){
															$callStsArr[]=stripslashes($getcallstatusRow["callstatus"]);
														}
													}
												}
												foreach($callStsArr as $stsval){
											   ?>
												<option value="<?php echo $stsval;?>"<?php if($callstatus==$stsval) echo " selected";?>><?php echo $stsval;?></option>
											<?php
												}
											?>
										</select>



									</td>
									
								</tr>
								 
								<tr>
									<td>Callback Date:&nbsp;&nbsp;&nbsp;</td>
									 <td><input type="text" name="demo_date" id="demo_date" class="inp_feild" style="width:25%;" value="<?php echo $demo_date;?>" tabindex="18">&nbsp;&nbsp;&nbsp;Time:&nbsp;&nbsp;&nbsp;<input type="text" name="demo_time" id="demo_time" class="inp_feild" style="width:25%;" value="<?php echo $demo_time?>" tabindex="19"></td>
							   </tr>
									<?php
							if($demoId!=""){
									?>
								<tr>
									<td valign="top">
										Comments:
									</td>
									<td>
										<table border="0" width="100%" cellspacing="5">
										<tr>
											<td width="80%">
											   <textarea name="prospect_comments" id="prospect_comments" class="inp_feild" style="height:100px;font-family: arial;font-size:13px;width:100%;" tabindex="20"></textarea> 
											</td>
											<td  width="20%" valign="top">
												 <div class="form_actions">
												   <input type="button" value="Add Comment" class="add_btn" onclick="addcomment('<?php echo addslashes($_SESSION['user_id']);?>','<?php echo $demoId;?>')" style="margin-top: -15px;" tabindex="21">
												</div>
											</td>
										</tr>
										</table>
										<script>
									   function addcomment(userid,prospect_id){
										comment=$("#prospect_comments").val();
										if($.trim($("#prospect_comments").val())==""){
											alert("Please enter comments");
											$("#prospect_comments").focus();
											return false;

										}
										else{
											$.ajax({url:"ajax_comment1.php?comment="+comment+"&userid="+userid+"&prospect_id="+prospect_id+"",success:function(result){
											$('#prospect_comments').val("");
											$("#container").html(result);
										}});
										}

                                      }
									</script>
										<script>
													$(document).ready(function(){
													   
													     $("#callstatus").trigger('change');
														  $("#record_type").trigger('change');
													  
													});
												  </script>
									</td>
								</tr>		
								  <td colspan="2" valign="top" >
								  <div id="container" style="width:100%;max-height:250px;overflow:auto;">
										<table  cellpadding="0" width="100%" cellspacing="0"  style="border:1px solid #CAE1F9;font-size:13px;color:black;" border="1">
										<tr style="color:white;height: 20px;">
										<td valign="top"  align="left"  width="15%">Date</td>
										<td valign="top"  align="left"  width="70%">Comment</td>
										<td valign="top"  align="left"  width="15%">User Name</td></tr>
										<?php
										$getQry="select * from  tbl_prospect_comments where prospect_id=:prospect_id  order by comment_id desc";
										$prepgetQry=$DBCONN->prepare($getQry);
										$prepgetQry->execute(array(":prospect_id"=>$prospect_id));
										$count =$prepgetQry->rowCount();
										if($count>0){
										$i=1;
										while($getRow=$prepgetQry->fetch()){
										if($i%2==0)
										$display_color="#a5a5a5";
										else
										$display_color="#d2d1d1";
										$user_id=$getRow["user_id"];
										$get_Qry1="select * from  tbl_users where user_id=:user_id";
										$prepget1_Qry=$DBCONN->prepare($get_Qry1);
										$prepget1_Qry->execute(array(":user_id"=>$user_id));
										$get_Row1=$prepget1_Qry->fetch();
										$user_name=$get_Row1['username'];
										?>
										<tr style="background-color:<?php echo $display_color;?>;height: 20px;">	
										<td valign="top"  align="left" style=" width: 30px;">
											<?php echo date("d-m-Y", strtotime($getRow["created_date"]));?>
										</td>
										<td valign="top"  align="left"  style="">
											<?php echo stripslashes($getRow["comment"]);?>
										</td>
										<td valign="top"  align="left" style=""><?php echo $user_name;?></td>
										</tr>
										<?php
										$i++;
										}
										}
										else{
										echo "<tr style=\"background-color:#d2d1d1;text-align:center;\"><td colspan=\"3\">No Comment(s) found.</td></tr>";
										}
										?>
										</table>
										</div>
                                </tr>
								<?php
								}
									?>
								<tr>
								<td colspan="2">
										<h3><u>SYSTEM INFO:</u></h3>
									</td>
							  </tr>
							  <tr>
									<td>
										Create Date<?php if($demoId==""){echo "<font color=\"red\">*</font>";} ?>:
									</td>
									<td>
										<input type="text" name="import_date" id="import_date" class="inp_feild" style="width:50%;" value="<?php 
										
									 if($demoId==""){echo date("d-m-Y");} else { echo $import_date; }?>" readonly tabindex="22">
									</td>
								</tr>
								<tr>
								<td>
									Salesperson<font color="red">*</font>:
								</td>
								<td>
									<select name="sales_person" id="sales_person" class="inp_feild" onchange="getmanger(this.value)" style="width:50%;" tabindex="23">
										<option value="0"<?php if($sales_person=="0") echo " selected";?>>Select</option>
										<?php
											$getBookQry="select * from tbl_sales_person";
											$getBookRes=mysql_query($getBookQry);
											
											while($getBookRow=mysql_fetch_array($getBookRes)){
										?>
											<option value="<?php echo stripslashes($getBookRow["salesperson"]);?>"<?php if($sales_person==$getBookRow["salesperson"]) echo " selected";?>><?php echo stripslashes($getBookRow["salesperson"]);?></option>
										<?php
											}
										?>
										
									</select>
									<label><?php if($sales_person_initial!=''){echo $sales_person_initial;}?></label>

								</td>
								
							</tr>
							<tr>
								<td>
									Sales Manager<font color="red">*</font>:
								</td>
								<td>
									<select name="sales_manager" id="sales_manager" class="inp_feild" style="width:50%;" tabindex="24">
										<option value="0">Select</option>
										<?php
											$getmQry="select * from  tbl_salesmanager order by sales_manager_id asc";
											$getmRes=mysql_query($getmQry);
											while($getmRow=mysql_fetch_array($getmRes)){
											?>
											<option value="<?php echo $getmRow["sales_manager"]?>"<?php if($sales_manager==$getmRow["sales_manager"]) echo " selected";?>><?php echo $getmRow["sales_manager"]?></option>
										<?php
											}
										?>
										
									</select>
									<label><?php if($sales_manager_initial!=''){echo $sales_manager_initial;}?></label>
								</td>
								
							</tr>
								<tr>
								<td colspan="2">
									<h3><u>DEVELOPMENT INFO:</u></h3>
								</td>
								
							</tr>
							 <tr>
								<td>
									Status:
								</td>
								
								<td>
									<select name="itunes_category" id="itunes_category" class="inp_feild" tabindex="25"> 
										<option value="0">Select</option>
										<?php
											$getituneQry="select * from  tbl_status order by display_order asc";
											$getituneRes=mysql_query($getituneQry);
											while($getituneRow=mysql_fetch_array($getituneRes)){
											?>
											<option value="<?php echo $getituneRow["status"]; ?>"<?php if($status==$getituneRow["status"]) echo " selected";?>><?php echo $getituneRow["status"]; ?></option>
										<?php
										}
										?>
										
									</select>
								</td>
							</tr>
							<tr>
								<td>
									Developer's Name:
								</td>
								<td>
									<select name="developer" id="developer" class="inp_feild" tabindex="26">
										<option value="0">Select</option>
										<?php
											$getdeveloperQry="select * from  tbl_developers order by developer_id asc";
											$getdeveloperRes=mysql_query($getdeveloperQry);
											while($getdeveloperRow=mysql_fetch_array($getdeveloperRes)){
											?>
											<option value="<?php echo $getdeveloperRow["developer_name"]?>"<?php if($developer_id==$getdeveloperRow["developer_name"]) echo " selected";?>><?php echo $getdeveloperRow["developer_name"]?></option>
										<?php
											}
										?>
										
									</select>

								</td>
								
							</tr>
							
							<tr>
								<td style="width:178px">
									App Name:
								</td>
								<td>
									<input type="text" name="app_name" id="app_name" class="inp_feild" value="<?php echo $app_name;?>" tabindex="27">
								</td>
							</tr>
							<tr>
								<td>
									Default Langauage<font color="red">*</font>:
								</td>
								<td>
									<select name="def_language" id="def_language" class="inp_feild" tabindex="28">
										<option value="0"<?php if($default_langauage=="0") echo " selected";?>>Select</option>
										<?php
											$getBookQry="select * from tbl_language";
											$getBookRes=mysql_query($getBookQry);
											
											while($getBookRow=mysql_fetch_array($getBookRes)){
										?>
											<option value="<?php echo stripslashes($getBookRow["language"]);?>"<?php if($default_langauage==$getBookRow["language"]) echo " selected";?>><?php echo stripslashes($getBookRow["language"]);?></option>
										<?php
											}
										?>
										
										
									</select>

								</td>
								
							</tr>
							<tr>
								<td>
									Store Category:
								</td>
								<td>
									<select name="app_cat" id="app_cat" class="inp_feild" tabindex="29">
										<option value="0">Select</option>
										<?php
											$getituneQry="select * from  tbl_itune_category order by category_id asc";
											$getituneRes=mysql_query($getituneQry);
											while($getituneRow=mysql_fetch_array($getituneRes)){
											?>
											<option value="<?php echo $getituneRow["category"]?>"<?php if($itunes_category==$getituneRow["category"]) echo " selected";?>><?php echo $getituneRow["category"]?></option>
										<?php
											}
										?>
										
									</select>

								</td>
								
							</tr>
							 

								<td colspan="2">
									<h3><u>PAYMENT INFORMATION</u></h3>
								</td>
								
							</tr>
							<tr>
								<td>
									Payment Date:
								</td>
								<td>
									<input type="text" name="payment_date" id="payment_date" class="inp_feild" style="width:50%;" value="<?php echo $paymentdate;?>"<?php if($paymentf!="yes"){ echo " readonly";}?> tabindex="30">
								</td>
							</tr>
							<tr>
								<td>
									Commission Date:
								</td>
								<td>
									<input type="text" name="commision_date" id="commision_date" class="inp_feild" style="width:50%;" value="<?php echo $commisiondate;?>"<?php if($paymentf!="yes"){ echo " readonly";}?> tabindex="31">
								</td>
							</tr>
							<tr>
								<td>
									Dev QA Date:
								</td>
								<td>
									<input type="text" name="dev_date" id="dev_date" class="inp_feild" style="width:50%;" value="<?php echo $Dev_date;?>"<?php if($paymentf!="yes"){ echo " readonly";}?> tabindex="32">
								</td>
							</tr>
							<?php
							$getPayQry="select * from tbl_paypal where demo_id='".$demoId."' order by paypal_id desc limit 0,1";
							$getPayRes=mysql_query($getPayQry);
							$getPayRow=mysql_fetch_array($getPayRes);
							$call_status=$getPayRow["call_status"];
							if($user_type=="admin"&&$call_status=="paid"){
													
							?>
							<tr>
								<td>
									Cancel Subsription:
								</td>
								<td>
									<a href="<?php echo HTTP_ROOT_FOLDER."live/cancel_payment.php?appname=".urlencode(trim(stripslashes($getDemoRow["demo_page_url"])))."&app_id=".urlencode(base64_encode($demoId))?>" style="color:white;text-decoration:none;" target="_blank"><?php echo "www.myappyrestaurant.com/live/cancel_payment.php?appname=".urlencode(trim(stripslashes($getDemoRow["demo_page_url"])))."&app_id=".urlencode(base64_encode($demoId))?></a>
								</td>
							</tr>
							<?php
								
							}
							?>
							
							<tr>
								<td style="width:138px;" valign="top">
									Affiliate:
								</td>
								<td>
									<select name="affiliate" id="affiliate" class="inp_feild" onchange="get_Licensee(this.value)" style="width:50%;" <?php 
									if($Licensee!=""||$affiliate_id!=""||$demoId!=""){echo "disabled";}?> tabindex="33">
										<option value="0">Select</option>
										<?php
											$get_Qry="select * from  tbl_affiliate";
											$get_Res=mysql_query($get_Qry);
											
											while($get_value=mysql_fetch_array($get_Res)){
										   ?>
											<option value="<?php echo stripslashes($get_value["affiliate_id"]);?>"<?php if($affiliate_id==$get_value["affiliate_id"]) echo " selected";?>><?php echo stripslashes($get_value["full_name"]);?></option>
										<?php
											}
										?>
									</select>
									
								</td>
							</tr>
							<tr>
									<td valign="top">
										Licensee:
									</td>
									<td>
										<select name="licensee" id="licensee" class="input_field"   style="width:50%;" <?php if($Licensee!=""||$affiliate_id!=""||$demoId!=""){echo "disabled";}?> tabindex="34">
												<option value="0">Select</option>					
												<?php
												$getbusinessQry="select * from  tbl_licensee order by licensee_id asc";
												$prepget_business_qry=$DBCONN->prepare($getbusinessQry);
												$prepget_business_qry->execute();
												while($getbusinessRow=$prepget_business_qry->fetch()){
												?>

												<option value="<?php echo $getbusinessRow["licensee_id"];?>" <?php if($Licensee==$getbusinessRow["licensee_id"]) echo " selected";?>><?php echo $getbusinessRow["full_name"];?></option>
												<?php
												}
												?>
											</select>
									</td>
								</tr>
									<tr>
									<td colspan="2">
										<table cellspacing="0" cellpadding="0" width="100%" border="0">
											<tr>
												<td>
												  <div class="form_actions">
												     <input type="button" value="BACK TO MASTER LIST" class="add_btn"   onclick="document.location='masterlist.php'">
												</div>
												  <!-- <input type="button" value="Back"  onclick="document.location='prospectinglist.php'" class="btn btn-standard btn-medium btn-main" style="background-color:#E46c0A;color:white;font-weight:bold;"> -->
													<!-- <div class="form_actions">
														<input type="button" value="Back" onclick="document.location='prospectinglist.php'" class="add_btn">
													</div> -->
												</td>
												 
											 
												<td>
												
													<div class="form_actions" style="text-align:right;" id="upd_pros_btn">
													      <input type="button" value="<?php echo ucfirst($value);?> Record" class="add_btn add_demo" id="add_demo">  
														 <!-- <input type="button" value="<?php echo $mode_value;?> Record" id="update_prospect"   class="btn btn-standard btn-medium btn-main update_prospect" style="background-color:#E46c0A;color:white;font-weight:bold;"> -->
													</div>
												
												</td>
											</tr>
										</table>
									</td>
								</tr>

								
							</table>
															
								



						</form>
					</div>
					
				</div>
			</div>
		</div>
	</body>
</html>
<?php
}
?>
 