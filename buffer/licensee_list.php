<?php
$parent_directory=basename(dirname($_SERVER["PHP_SELF"]));
$filename=basename($_SERVER["PHP_SELF"]);
include("../includes/configure.php");
include("includes/cmm_functions.php");
include("../includes/session_check.php");
$id=$_SESSION['user_id'];
$getQry="select * from tbl_users where user_id='".$id."'";
$getRes=mysql_query($getQry);
$getRow=mysql_fetch_array($getRes);
$usertype=$getRow["user_type"];
$feature=$getRow[$feat];
$download=$getRow["download_list"];
$payment=$getRow["payment_info"];
$trialfeature=$getRow["trail_form"];
$master_list=$getRow["master_list"];
$demo_page=$getRow["demo_page"];
$licensee_page=$getRow["licensee_list"];
$affiliate_page=$getRow["alliance_list"];
if(($licensee_page!="yes")&&$usertype!="Admin"&&$affiliate_page!="yes"){
	header("Location:noauthorised.php");
	exit;
}
else{
$licensee_id=$_GET["licensee_id"];
$sort=$_GET["sort"];
$field=$_GET["field"];
if($sort==""){
	$sort="asc";
}
if($field==""){
	$field="affiliate_id";
}
if($field=="join_date"){
	$fieldname="joined_date";
	$order=" order by ".$fieldname." ".$sort;
	if($sort=="asc"){
		$dsort="desc";
		$dpath="images/up.png";
	}
	else{
		$dsort="asc";
		$dpath="images/down.png";
	}
}
if($field=="name"){
	$fieldname="full_name";    
	$order=" order by ".$fieldname." ".$sort;
	if($sort=="asc"){
		$rsort="desc";
		$rpath="images/up.png";
	}
	else{
		$rsort="asc";
		$rpath="images/down.png";
	}
}
if($field=="bus_name"){
	$fieldname="business_name";   
	$order=" order by ".$fieldname." ".$sort;
	if($sort=="asc"){
		$spsort="desc";
		$sppath="images/up.png";
	}
	else{
		$spsort="asc";
		$sppath="images/down.png";
	}
}
if($field=="bus_type"){ 
	$fieldname="business_type";
	$order=" order by ".$fieldname." ".$sort;
	if($sort=="asc"){
		$dbsort="desc";
		$dbpath="images/up.png";
	}
	else{
		$dbsort="asc";
		$dbpath="images/down.png";
	}
}
if($field=="sales_person"){ 
	$fieldname="sales_person";
	$order=" order by ".$fieldname." ".$sort;
	if($sort=="asc"){
		$usort="desc";
		$upath="images/up.png";
	}
	else{
		$usort="asc";
		$upath="images/down.png";
	}
}

if(isset($_POST['HdnPage']) && $_POST['HdnPage']!="" && $_POST['HdnPage']!="0")
	$Page=$_POST['HdnPage'];
else
	$Page=1;
if($licensee_id!=""){	
	$deleteQry="delete from tbl_licensee where licensee_id=:licensee_id";
	$preddeleteQry=$DBCONN->prepare($deleteQry);
	$deleteRes=$preddeleteQry->execute(array(':licensee_id'=>$licensee_id));
	//$deleteRes=mysql_query($deleteQry);
	if($deleteRes){
		header("Location:licensee_list.php");
		exit;
	}
}

if($usertype=="Admin"){
   $condtion="";
  
}
if($usertype!="Admin"){
 $condtion=" and logged_user_id='".$_SESSION['user_id']."'";
}
$getDemoQry="select * from tbl_licensee where licensee_id!=''".$condtion .$order;
$getDemoRes=mysql_query($getDemoQry);
$getDemoCnt=mysql_num_rows($getDemoRes);
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<title>MAR Pipeline System</title>
		<link rel="shortcut icon" href="images/Fav.ico" type="image/ico">
		<meta name="Generator" content="EditPlus">
		<meta name="Author" content="">
		<meta name="Keywords" content="">
		<meta name="Description" content="">
		<style>
			body{
				margin:0;
				color:black;
				background:#455A68;
				font-family:arial;
			}
			.header{
				
				background:#1C242A;
				height:100px;
			}
			.content{
				background:#455A68;
				min-height:600px;
			}
			
			.form_actions{
				padding-top:15px;
				padding-bottom:30px;
			}
			.form_actions .add_btn{
				cursor:pointer;
				border-radius:0px;
				background:#0D0D0D;
				color:#D9D9D9;
				border-color:#D9D9D9;
				padding:5px 15px 5px 15px;
				font-family:arial;
				min-width:155px;
			}
			.list_content{
				width:950px;
				margin-left:40px;
				/*margin-left:auto;
				margin-right:auto; */
			}
			.tbl_header{
				font-size:13px;
				border-bottom:1px solid #D9D9D9;
				text-align:left;
				color:white;
			}
			.tbl-body{
				font-size:12px;
				line-height:25px;
			}
			a{
				color:black;
			}
			
		</style>
	
	</head>
	<body>
	
		
			
				<div class="header">
					<span style="float:right;margin-right:20px;"><a href="logout.php" style="color:white;text-decoration:none;">Logout</a></span>
				    <div style="width:1280px;height:80px;">
						 <div style="float:left;width:55%;">
							<img src="images/myappyrestaurants.png" style="margin-top:10px;margin-left:40px;">
							<div style="float:right;width:65%;margin-top:20px;width:32%;"><font style="font-family:arial;color:white;size:40px;" size="5px;">Licensee List</font></div>
						 </div>
						  <?php
							 if($usertype=="Admin"){
						 ?>
						 <div class="form_actions" style="float:right;border:1 px solid red;width:30%;text-align:right;">
							   <input type="button" value="Admin Features" class="add_btn" onclick="document.location='admin_features.php'"  style="margin-top:20px;">
						  </div>
						 <?php
							}
						 ?>
					</div> 
				</div>
				<div class="content">
					<div class="list_content">
							<div class="form_actions" style="width:1240px;">
						   <div style="width:50%;float:left;"> 
						  
						   <input type="button" style="width:170px;" value="Add a Licensee Page" class="add_btn" onclick="document.location='licensee_form.php'" ><br>
						  
							
							</div>
						  <br><br>
                      
          
                </div>
						<div style="padding-bottom:12px;">
							<form name="licensee_form" method="post">
							<input type="hidden" name="HiddenMode" id="HiddenMode" value="">
							<input type="hidden" name="HdnPage" id="HdnPage" value="<?php echo $Page;?>">
							<input type="hidden" name="dlete_mode" id="dlete_mode">
							<input type="hidden" name="delete_yes" id="delete_yes" value="">
						</div>
						<table cellspacing="0" cellpadding="0" width="120%" class="tbl-body" border="0">
							<?php								
							$records_perpage=100;
							$TotalRecords	=	$getDemoCnt;
							if($TotalRecords <= (($Page * $records_perpage)-$records_perpage))
								$Page	=	$Page-1;
							$TotalPages		=	ceil($TotalRecords/$records_perpage);
							$Start			=	($Page-1)*$records_perpage;
							if($TotalPages>1)
							  {
							?>
							<tr>
								<td align="center" colspan="9" style="border-bottom:1px solid grey;">
									<?php
									$FormName="licensee_form";
									include("../includes/paging.php");
									?>
								</td>
							</tr>
							<?php
							  }
							?>
							<tr class="tbl_header">
									<th style="cursor:pointer" onclick="document.location='licensee_list.php?sort=<?php echo $dsort;?>&field=join_date'">Date Joined&nbsp;<?php if($dpath!=""){?><img src="<?php echo $dpath;?>" style="width:10px;height:10px;"><?php }?></th>
                                    <th style="cursor:pointer" onclick="document.location='licensee_list.php?sort=<?php echo $rsort;?>&field=name'">Full Name&nbsp;<?php if($rpath!=""){?><img src="<?php echo $rpath;?>" style="width:10px;height:10px;"><?php }?></th>
                                    <th style="cursor:pointer" onclick="document.location='licensee_list.php?sort=<?php echo $spsort;?>&field=bus_name'">Business Name&nbsp;&nbsp;<?php if($sppath!=""){?><img src="<?php echo $sppath;?>" style="width:10px;height:10px;"><?php }?></th>
									<th style="cursor:pointer;" onclick="document.location='licensee_list.php?sort=<?php echo $dbsort;?>&field=bus_type'">Business Type &nbsp;&nbsp;<?php if($dbpath!=""){?><img src="<?php echo $dbpath;?>" style="width:10px;height:10px;"><?php }?></th>
									<th style="width:18%;cursor:pointer;" onclick="document.location='licensee_list.php?sort=<?php echo $usort;?>&field=sales_person'">Salesperson&nbsp;&nbsp;<?php if($upath!=""){?><img src="<?php echo $upath;?>" style="width:10px;height:10px;"><?php }?></th>
									<!-- <th style="cursor:pointer;width:18%;" onclick="document.location='affiliate_list.php?sort=<?php echo $ssort;?>&field=tbc'">TBC&nbsp;&nbsp;<?php if($spath!=""){?><img src="<?php echo $spath;?>" style="width:10px;height:10px;"><?php }?></th> -->
									
									
									<th style="width:7%;cursor:pointer;"><span>Delete?</span>&nbsp;</th>
									
								</tr>
							<tr style="height:5px;">
								<td colspan="10" style="border-top:1px solid #D9D9D9;"></td>
							</tr>
						<?php
							if($getDemoCnt>0){
								$records_perpage=100;
								$TotalRecords	=	$getDemoCnt;
								if($TotalRecords <= (($Page * $records_perpage)-$records_perpage))
									$Page	=	$Page-1;
								$TotalPages		=	ceil($TotalRecords/$records_perpage);
								$Start			=	($Page-1)*$records_perpage;
								
								//code for paging ends
								$getDemoQry.=" limit $Start,$records_perpage";
								$getDemoRes = mysql_query($getDemoQry);
								$getDemoCnt = mysql_num_rows($getDemoRes);

								$i=1;
								$j=0;
								while($getRow=mysql_fetch_array($getDemoRes)){
									if($getRow["joined_date"]!=""){
											if($getRow["joined_date"]!="0000-00-00"){
												$joined_date=date('d-m-Y',strtotime(stripslashes($getRow["joined_date"])));
											}
											else{
												$joined_date="";
											}
									}
									 if($i%2==1){
										$bgcolor="#a5a5a5";
									}
									else{
										$bgcolor="#d2d1d1";
									}


							
						?>
							<tr  style="background:<?php echo $bgcolor.$display;?>">
								<td style="border-bottom:1px solid grey;color:<?php echo $font_color;?>"><?php echo $joined_date;?></td>
								<td style="border-bottom:1px solid grey;color:<?php echo $font_color;?>"><?php echo stripslashes($getRow["full_name"]);?></td>
								<td style="border-bottom:1px solid grey;color:<?php echo $font_color;?>"><?php echo stripslashes($getRow["business_name"]);?></td>
								<td style="border-bottom:1px solid grey;color:<?php echo $font_color;?>"><?php echo stripslashes($getRow["business_type"]);?></td>
								
							
								<td style="border-bottom:1px solid grey;color:<?php echo $font_color;?>">
								<?php echo stripslashes($getRow["sales_person"]);?></a></td>
								<!-- <td style="border-bottom:1px solid grey;color:<?php echo $font_color;?>"><?php echo stripslashes($getRow["tbc"]);?></td> -->
								
								
								<td style="border-bottom:1px solid grey;color:<?php echo $font_color;?>"><a href="licensee_form.php?licensee_id=<?php echo $getRow["licensee_id"];?>" style="color:<?php echo $font_color;?>">Edit</a>&nbsp;&nbsp;&nbsp;/&nbsp;&nbsp;&nbsp;<a href="licensee_list.php?licensee_id=<?php echo $getRow["licensee_id"];?>" style="color:<?php echo $font_color;?>" onclick="return confirm('Are you sure want to delete this record? ')">Delete</a>&nbsp;&nbsp;&nbsp;</td>
								
							</tr> 

						<?php
						
							$i++;
								}
								
								if($TotalPages>1)
								  {
								?>
								<tr>
									<td align="center" colspan="9" style="border-bottom:1px solid grey;">
										<?php
										$FormName="licensee_form";
										include("../includes/paging.php");
										?>
									</td>
								</tr>
								<?php
								  }
								echo "</table>";
							}
							else{
								echo "<tr bgcolor='#a5a5a5'><td colspan=\"9\" style=\"border-bottom:1px solid grey;\"><center>No Record(s) found.</center></td></tr>";
							}
							
						?>
						
						</form>
					</div>
					<div style="height:50px;border:0px solid red;"></div>
				</div>
				
			
	
	</body>
</html>
<script>


/****function for paging statrs*******/
function pagetransfer(pagenumber,formname)
{	
	with(document.forms[formname])
	{ 
		HdnPage.value=pagenumber;
		HiddenMode.value="paging";
		submit();
	}
}
/****function for paging ends*******/
</script>
<?php
}
?>