<?php
$parent_directory=basename(dirname($_SERVER["PHP_SELF"]));
$filename=basename($_SERVER["PHP_SELF"]);
include("../includes/configure.php");
include("includes/cmm_functions.php");
include("../includes/session_check.php");
$id=$_SESSION['user_id'];
$getQry="select * from tbl_users where user_id='".$id."'";
$getRes=mysql_query($getQry);
$getRow=mysql_fetch_array($getRes);
$usertype=$getRow["user_type"];
$feature=$getRow[$feat];
$download=$getRow["download_list"];
$payment=$getRow["payment_info"];
$trialfeature=$getRow["trail_form"];
$master_list=$getRow["master_list"];
$demo_page=$getRow["demo_page"];
$licensee_page=$getRow["licensee_list"];
$affiliate_page=$getRow["alliance_list"];
if(($licensee_page!="yes")&&$usertype!="Admin"&&$affiliate_page!="yes"){
	header("Location:noauthorised.php");
	exit;
}
else{
$licensee_id=$_GET["licensee_id"];
$sort=$_GET["sort"];
$field=$_GET["field"];
if($sort==""){
	$sort="asc";
}
if($field==""){
	$field="affiliate_id";
}
if($field=="join_date"){
	$fieldname="joined_date";
	$order=" order by ".$fieldname." ".$sort;
	if($sort=="asc"){
		$dsort="desc";
		$dpath="images/up.png";
	}
	else{
		$dsort="asc";
		$dpath="images/down.png";
	}
}
if($field=="name"){
	$fieldname="full_name";    
	$order=" order by ".$fieldname." ".$sort;
	if($sort=="asc"){
		$rsort="desc";
		$rpath="images/up.png";
	}
	else{
		$rsort="asc";
		$rpath="images/down.png";
	}
}
if($field=="bus_name"){
	$fieldname="business_name";   
	$order=" order by ".$fieldname." ".$sort;
	if($sort=="asc"){
		$spsort="desc";
		$sppath="images/up.png";
	}
	else{
		$spsort="asc";
		$sppath="images/down.png";
	}
}
if($field=="bus_type"){ 
	$fieldname="business_type";
	$order=" order by ".$fieldname." ".$sort;
	if($sort=="asc"){
		$dbsort="desc";
		$dbpath="images/up.png";
	}
	else{
		$dbsort="asc";
		$dbpath="images/down.png";
	}
}
if($field=="sales_person"){ 
	$fieldname="sales_person";
	$order=" order by ".$fieldname." ".$sort;
	if($sort=="asc"){
		$usort="desc";
		$upath="images/up.png";
	}
	else{
		$usort="asc";
		$upath="images/down.png";
	}
}

if(isset($_POST['HdnPage']) && $_POST['HdnPage']!="" && $_POST['HdnPage']!="0")
	$Page=$_POST['HdnPage'];
else
	$Page=1;
if($licensee_id!=""){	
	$deleteQry="delete from tbl_licensee where licensee_id=:licensee_id";
	$preddeleteQry=$DBCONN->prepare($deleteQry);
	$deleteRes=$preddeleteQry->execute(array(':licensee_id'=>$licensee_id));
	//$deleteRes=mysql_query($deleteQry);
	if($deleteRes){
		header("Location:licensee_list.php");
		exit;
	}
}

if($usertype=="Admin"){
   $condtion="";
  
}
if($usertype!="Admin"){
 $condtion=" and logged_user_id='".$_SESSION['user_id']."'";
}
$getDemoQry="select * from tbl_licensee where licensee_id!=''".$condtion .$order;
$getDemoRes=mysql_query($getDemoQry);
$getDemoCnt=mysql_num_rows($getDemoRes);
include("includes/header.php");
?>
 <body>
	 <div class="content">
					<div class="list_content">
							<div class="form_actions" style="width:1240px;">
						   <div style="width:50%;float:left;display:none;"> 
						  
						   <input type="button" style="width:170px;" value="Add a Licensee Page" class="add_btn" onclick="document.location='licensee_form.php'" ><br>
						  
							
							</div>
						  <br><br>
                      
          
                </div>
						<div style="padding-bottom:12px;">
							<form name="licensee_form" method="post">
							<input type="hidden" name="HiddenMode" id="HiddenMode" value="">
							<input type="hidden" name="HdnPage" id="HdnPage" value="<?php echo $Page;?>">
							<input type="hidden" name="dlete_mode" id="dlete_mode">
							<input type="hidden" name="delete_yes" id="delete_yes" value="">
						</div>
						<table cellspacing="0" cellpadding="0" width="120%" class="tbl-body" border="0">
							<?php								
							$records_perpage=100;
							$TotalRecords	=	$getDemoCnt;
							if($TotalRecords <= (($Page * $records_perpage)-$records_perpage))
								$Page	=	$Page-1;
							$TotalPages		=	ceil($TotalRecords/$records_perpage);
							$Start			=	($Page-1)*$records_perpage;
							if($TotalPages>1)
							  {
							?>
							<tr>
								<td align="center" colspan="9" style="border-bottom:1px solid grey;">
									<?php
									$FormName="licensee_form";
									include("../includes/paging.php");
									?>
								</td>
							</tr>
							<?php
							  }
							?>
							<tr class="tbl_header" style="border-radius:80px;">
									<th style="cursor:pointer;border-top-left-radius:10px;border-bottom-left-radius:10px;" onclick="document.location='licensee_list.php?sort=<?php echo $dsort;?>&field=join_date'">&nbsp;Date Joined&nbsp;<?php if($dpath!=""){?><img src="<?php echo $dpath;?>" style="width:10px;height:10px;"><?php }?></th>
                                    <th style="cursor:pointer" onclick="document.location='licensee_list.php?sort=<?php echo $rsort;?>&field=name'">Full Name&nbsp;<?php if($rpath!=""){?><img src="<?php echo $rpath;?>" style="width:10px;height:10px;"><?php }?></th>
                                    <th style="cursor:pointer" onclick="document.location='licensee_list.php?sort=<?php echo $spsort;?>&field=bus_name'">Business Name&nbsp;&nbsp;<?php if($sppath!=""){?><img src="<?php echo $sppath;?>" style="width:10px;height:10px;"><?php }?></th>
									<th style="cursor:pointer;" onclick="document.location='licensee_list.php?sort=<?php echo $dbsort;?>&field=bus_type'">Business Type &nbsp;&nbsp;<?php if($dbpath!=""){?><img src="<?php echo $dbpath;?>" style="width:10px;height:10px;"><?php }?></th>
									<!-- <th style="width:18%;cursor:pointer;" onclick="document.location='licensee_list.php?sort=<?php echo $usort;?>&field=sales_person'">Salesperson&nbsp;&nbsp;<?php if($upath!=""){?><img src="<?php echo $upath;?>" style="width:10px;height:10px;"><?php }?></th>	 -->							
									<th style="width:7%;cursor:pointer;border-top-right-radius: 10px;border-bottom-right-radius: 10px;"><span>Delete?</span>&nbsp;</th>
									
								</tr>
							<tr class="gap">
								<td colspan="10" style=""></td>
							</tr>
						<?php
							if($getDemoCnt>0){
								$records_perpage=100;
								$TotalRecords	=	$getDemoCnt;
								if($TotalRecords <= (($Page * $records_perpage)-$records_perpage))
									$Page	=	$Page-1;
								$TotalPages		=	ceil($TotalRecords/$records_perpage);
								$Start			=	($Page-1)*$records_perpage;
								
								//code for paging ends
								$getDemoQry.=" limit $Start,$records_perpage";
								$getDemoRes = mysql_query($getDemoQry);
								$getDemoCnt = mysql_num_rows($getDemoRes);

								$i=1;
								$j=0;
								while($getRow=mysql_fetch_array($getDemoRes)){
									if($getRow["joined_date"]!=""){
											if($getRow["joined_date"]!="0000-00-00"){
												$joined_date=date('d-m-Y',strtotime(stripslashes($getRow["joined_date"])));
											}
											else{
												$joined_date="";
											}
									}
									 if($i%2==1){
										$bgcolor="#a5a5a5";
									}
									else{
										$bgcolor="#d2d1d1";
									}


							
						?>
							<tr  style="background:<?php echo $bgcolor.$display;?>">
								<td style="border-bottom:1px solid grey;cursor:pointer;color:<?php echo $font_color;?>" onclick="edit_record('<?php echo $getRow["licensee_id"];?> ');"><?php echo $joined_date;?></td>
								<td style="border-bottom:1px solid grey;cursor:pointer;color:<?php echo $font_color;?>" onclick="edit_record('<?php echo $getRow["licensee_id"];?> ');"><?php echo readmore_view(stripslashes($getRow["full_name"]),"50");?></td>
								<td style="border-bottom:1px solid grey;cursor:pointer;color:<?php echo $font_color;?>" onclick="edit_record('<?php echo $getRow["licensee_id"];?> ');"><?php echo readmore_view(stripslashes($getRow["business_name"]),"50");?></td>
								<td style="border-bottom:1px solid grey;cursor:pointer;color:<?php echo $font_color;?>" onclick="edit_record('<?php echo $getRow["licensee_id"];?> ');"><?php echo readmore_view(stripslashes($getRow["business_type"]),"50");?></td>
								
							
								<!-- <td style="border-bottom:1px solid grey;color:<?php echo $font_color;?>">
								<?php echo stripslashes($getRow["sales_person"]);?></a></td> -->
								<!-- <td style="border-bottom:1px solid grey;color:<?php echo $font_color;?>"><?php echo stripslashes($getRow["tbc"]);?></td> -->
								
								
								<td style="border-bottom:1px solid grey;color:<?php echo $font_color;?>" ><!-- <a href="licensee_form.php?licensee_id=<?php echo $getRow["licensee_id"];?>" style="color:<?php echo $font_color;?>">Edit</a>&nbsp;&nbsp;&nbsp;/ -->&nbsp;&nbsp;&nbsp;<a href="licensee_list.php?licensee_id=<?php echo $getRow["licensee_id"];?>" style="color:<?php echo $font_color;?>" onclick="return confirm('Are you sure want to delete this record? ')">Delete</a>&nbsp;&nbsp;&nbsp;</td>
								
							</tr> 

						<?php
						
							$i++;
								}
								
								if($TotalPages>1)
								  {
								?>
								<tr>
									<td align="center" colspan="9" style="border-bottom:1px solid grey;">
										<?php
										$FormName="licensee_form";
										include("../includes/paging.php");
										?>
									</td>
								</tr>
								<?php
								  }
								echo "</table>";
							}
							else{
								echo "<tr bgcolor='#a5a5a5'><td colspan=\"9\" style=\"border-bottom:1px solid grey;\"><center>No Record(s) found.</center></td></tr>";
							}
							
						?>
						
						</form>
					</div>
					<div style="height:50px;border:0px solid red;"></div>
				</div>
				
			
	
<?php
	include("includes/footer.php");
	?>
<script>
 
function edit_record(val){
  document.location.href="licensee_form.php?licensee_id="+val;
}

/****function for paging statrs*******/
function pagetransfer(pagenumber,formname)
{	
	with(document.forms[formname])
	{ 
		HdnPage.value=pagenumber;
		HiddenMode.value="paging";
		submit();
	}
}
/****function for paging ends*******/
</script>
<?php
}
?>