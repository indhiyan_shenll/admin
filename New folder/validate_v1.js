/*
 * validate_v1.js
 *
 * Demo JavaScript used on Validation-pages.
 */
$(document).ready(function(){
	var emailPattern=/^[a-zA-Z0-9\-_]+(\.[a-zA-Z0-9\-_]+)*@[a-z0-9]+(\-[a-z0-9]+)*(\.[a-z0-9]+(\-[a-z0-9]+)*)*\.[a-z]{2,4}$/;
	var multipleEmailRegex = /^(([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5}){1,25})+([;,.](([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5}){1,25})+)*$/;
	var testexp=/^[A-Z0-9\._%-]+@[A-Z0-9\.-]+\.[A-Z]{2,4}(?:(?:[,;][A-Z0-9\._%-]+@[A-Z0-9\.-]+))?$/i
	var intPattern=/^-?\d\d*$/;
	var find=/^[\s()-+]*([0-9][\s()-]*){6,20}$/;
	var re = /(http(s)?:\\)?([\w-]+\.)+[\w-]+[.com|.in|.org]+(\[\?%&=]*)?/;
	var decimal= /^\d*\.?\d*$/;
   $("input[type='text']:first", document.forms[0]).focus();
		/*
		*  
		*Validation for demo page.
		*/
		$(".add_demo").click(function(){
		var photoarr=$("#restaurant_logo").val().split(".");
		var photolen=photoarr.length;
		var extension=photoarr[photolen-1];
		var find=/^[\s()-]*([0-9][\s()-]*){6,20}$/;
        var find=/^[\s()-+]*([0-9][\s()-]*){6,20}$/;

        //apk file


        var iosapkarr=$("#ios_app_ipa").val().split(".");
		var iosaplen=iosapkarr.length;
		var iosextension=iosapkarr[iosaplen-1].toLowerCase();
		    //alert(iosextension);
		var andapkarr=$("#andorid_app_ipa").val().split(".");
		var andaplen=andapkarr.length;
		var andextension=andapkarr[andaplen-1].toLowerCase();
		//alert(andextension);
        
        if($.trim($("#business_name").val())==""){
			alert("Please enter business name");
			$("#business_name").focus();
			return false;
		}
		else if($.trim($("#first_name").val())==""){
			alert("Please enter full name");
			$("#first_name").focus();
			return false;
		}
		else if($.trim($("#street1_address").val())==""){
			alert("Please enter address");
			$("#street1_address").focus();
			return false;
		}
		else if($.trim($("#country1").val())=="0"){
			alert("Please select country");
			$("#country1").focus();
			return false;
		}
		else if($.trim($("#email").val())==""){
			alert("Please enter email address");
			$("#email").focus();
			return false;
		}
		else if(!emailPattern.test($.trim($("#email").val()))){
			alert("Please enter valid email address");
			$("#email").focus();
			return false;
		}
		else if($.trim($("#res1_workphone").val())==""){
			alert("Please enter work phone");
			$("#res1_workphone").focus();
			return false;
		}
		else   if(!find.test($.trim($("#res1_workphone").val()))){
			alert("Please enter valid  work phone");
			$("#res1_workphone").focus();
			return false;
		}
		else if($.trim($("#business_type").val())=="0"){
			alert("Please select business type");
			$("#business_type").focus();
			return false;
		}
		else if($.trim($("#def_language").val())=="0"){
			alert("Please select default language");
			$("#def_language").focus();
			return false;
		}
		/*else if($.trim($("#product_page").val())=="0"){
			 alert("Please select product page");
			 $("#product_page").focus();
			 return false;
	    }*/
		else if($.trim($("#promo_code").val())==""){
			alert("Please enter promo code");
			$("#promo_code").focus();
			return false;
		}
		else if($.trim($("#restaurant_logo").val())!="" &&extension!="jpg" && extension!="jpeg" && extension!="png" && document.getElementById("simulator").checked==true ){
		   alert('Please upload valid business logo');
		   $("#restaurant_logo").focus();
		   return false;
    
        }

	    else if(document.getElementById("simulator").checked==true && $.trim($("#theme").val())=="0"){
			alert("Please select design style");
			$("#theme").focus();
			return false;
	    }
		
		
		/* else if($("#callstatus").val()=="0"){
			alert("Please select call status");
			$("#callstatus").focus();
			return false;
		}
		else if($.trim($("#sales_person").val())=="0"){
			alert("Please select sales person");
			$("#sales_person").focus();
			return false;
		}
		else if($.trim($("#sales_manager").val())=="0"){
			alert("Please select sales manager");
			$("#sales_manager").focus();
			return false;
		}*/
		
		else if($.trim($("#ios_app_ipa").val())!="" && iosextension!="ipa"){
		   alert('Please upload valid IPA file ');
		   $("#ios_app_ipa").focus();
		   return false;
        }
        else if($.trim($("#andorid_app_ipa").val())!="" && andextension!="apk"){
		   alert('Please upload valid APK file');
		   $("#andorid_app_ipa").focus();
		   return false;
        }
		else{
			$("#demo_form").submit();
		}
		
	});
	/*
	*  
	*Validation for prospecting page 
	*/
	$(".update_prospect").click(function(){
		$("#prospect_form").submit();
		return true;
		var photoarr=$("#special_logo").val().split(".");
		var photolen=photoarr.length;
		var extension=photoarr[photolen-1].toLowerCase();
		var promocode=$.trim($("#promocode").val());
		var promocode_id=$("#hidden_special").val();
		var mode_vale=$("#hidden_logo").val();
		if(promocode_id==""){
			promocode_id=0;
		}
		var hdn_img=$("#hdn_img").val();
		var user_type=$("#user_type").val();
		//alert(user_type);
        if($.trim($("#business_name").val())=="" && user_type=="Agent"){
			alert("Please enter business name");
			$("#business_name").focus();
			return false;
		}
		else if($.trim($("#business_type").val())=="0"  && user_type=="Agent"){
			alert("Please select business type");
			$("#business_type").focus();
			return false;
		}
		else if($.trim($("#email").val())==""  && user_type=="Agent"){
			alert("Please enter email address");
			$("#email").focus();
			return false;
		}
		else if(!emailPattern.test($.trim($("#email").val()))  && user_type=="Agent"){
			alert("Please enter valid email address");
			$("#email").focus();
			return false;
		}
		else if($.trim($("#phone").val())==""  && user_type=="Agent"){
			alert("Please enter work phone");
			$("#phone").focus();
			return false;
		}
		else   if(!find.test($.trim($("#phone").val()))  && user_type=="Agent"){
			alert("Please enter valid  work phone");
			$("#phone").focus();
			return false;
		}
		else if($.trim($("#product_page").val())=="0"  && user_type=="Agent"){
			 alert("Please select product page");
			 $("#product_page").focus();
			 return false;
	    }
		else if($.trim($("#promo_code").val())==""  && user_type=="Agent"){
			alert("Please enter promo code");
			$("#promo_code").focus();
			return false;
		}
		 else if($.trim($("#special_logo").val())=="" && hdn_img=="" && document.getElementById("simulator").checked==true  && user_type=="Agent"){
			alert("Please upload business logo");
			$("#special_logo").focus();
			return false;
		}
		else if($.trim($("#special_logo").val())!=""  && user_type=="Agent" &&extension!="jpg" && extension!="jpeg" && extension!="png" && document.getElementById("simulator").checked==true ){
		   alert('Please upload valid business logo');
		   $("#special_logo").focus();
		   return false;
    
        }
	    else if(document.getElementById("simulator").checked==true && $.trim($("#theme").val())=="0"  && user_type=="Agent"){
			alert("Please select design style");
			$("#theme").focus();
			return false;
	    }
		else if($.trim($("#first_name").val())==""  && user_type=="Agent"){
			alert("Please enter full name");
			$("#first_name").focus();
			return false;
		}
		 
		else if($.trim($("#street_address").val())==""  && user_type=="Agent"){
			alert("Please enter address");
			$("#street_address").focus();
			return false;
		}
		else if($.trim($("#country").val())=="0"  && user_type=="Agent"){
			alert("Please select country");
			$("#country").focus();
			return false;
		}
		
		 else if($("#callstatus").val()=="0"  && user_type!="Agent"){
			alert("Please select call status");
			$("#callstatus").focus();
			return false;
		}
		else if($.trim($("#import_date").val())==""  && user_type!="Agent"){
			alert("Please enter create date");
			$("#import_date").focus();
			return false;
		}
		else if($.trim($("#affiliate").val())=="0" && user_type!="Agent"){
			alert("Please select affiliate");
			$("#affiliate").focus();
			return false;
		}
		else if($.trim($("#licensee").val())=="0" && user_type!="Agent"){
			alert("Please select licensee");
			$("#licensee").focus();
			return false;
		}
		
		else{
			$.ajax({url:"check_promo.php?name="+promocode+"&id="+promocode_id,success:function(result){
			 if(parseInt(result)>0){
				        alert('Promo code already exists.Please enter another promo code.')
						$("#promocode").focus(); 
						return false;
					}
					else{
						$("#prospect_form").submit();
					}
				  }});
				
			  }

			 
					
	});
	/*
	*  
	*Validation Add users.
	*/
	$("#add_user").click(function(){
		 var username=$.trim($("#username").val());
		 var id=$("#hdn_userid").val();
		 if(id==""){
			id=0;
		 }
		 if($.trim($("#username").val())==""){
			 alert("Please enter username");
			 $("#username").focus();
			 return false;
		 }
		 else if($.trim($("#password").val())==""){
			 alert("Please enter password");
			 $("#password").focus();
			 return false;
		 }
		 else if($.trim($("#user_email").val())==""){
			 alert("Please enter email");
			 $("#user_email").focus();
			 return false;
		 }
		 else if(!emailPattern.test($.trim($("#user_email").val()))){
			 alert("Please enter valid email");
			 $("#user_email").focus();
			 return false;
		 }
		 
		 else if(parseInt($.trim($("#usertype").val()))==0){
			 alert("Please select usertype");
			 $("#usertype").focus();
			 return false;
		 }
		 else if(!checkPrevilages()){
			 alert("Please check at least one privilege");
			 $("#trial_form").focus();
			 return false;
		 }
		 else{
			 $.ajax({url:"user_check.php?name="+username+"&id="+id,success:function(result){
				if(parseInt(result)>0){
					alert('Username already exists.Please enter another username.')
					$("#username").focus(); 
					return false;
				}
				else{
					$("#user_form").submit();
				}
			}});
    
		 }
	 });
	/*
	*  
	*Validation Add developer.
	*/
	$("#add_developer").click(function(){
	var did=$("#developer_form_id").val();
	if(did==""){
	  did=0;
	}
	var developer_name=$.trim($("#developer_name").val());
	if(developer_name==""){
		alert("Please enter developer name");
		$("#developer_name").focus();
		return false;
	}
	else if($.trim($("#developer_email").val())==""){
		alert("Please enter developer email");
		$("#developer_email").focus();
		return false;
	}
	else if(!emailPattern.test($.trim($("#developer_email").val()))){
		alert("Please enter valid developer email");
		$("#developer_email").focus();
		return false;
	}
	else{
		$.ajax({url:"developer_check.php?name="+developer_name+"&id="+did+"",success:function(result){
		if(parseInt(result)>0){
			alert('Developer already exists.Please enter another developer.')
			$("#developer_name").focus(); 
			return false;
		 }
		 else{
			$("#developer_form").submit();
		 }
	  }});

	 }
	});
	/*
	*  
	*Validation Add Affiliate.
	*/
    $("#add_affiliate").click(function(){ 
		var username=$.trim($("#full_name").val());
        var hdn_special_id=$("#hdn_id").val();
		var page1=$.trim($("#product_page1").val());
		var page2=$.trim($("#product_page2").val());
		var page3=$.trim($("#product_page3").val());
		if(hdn_special_id==""){
			hdn_special_id=0;
		}

        /*if($.trim($("#joined_date").val())==""){
			alert("Please enter joined_date");
			$("#joined_date").focus();
			return false;
		}
		else */
		if($.trim($("#full_name").val())==""){
			alert("Please enter full name");
			$("#full_name").focus();
			return false;
		}
		/*else if($.trim($("#business_name").val())==""){
			alert("Please enter business name");
			$("#business_name").focus();
			return false;
		}
		else if($.trim($("#phone").val())==""){
			alert("Please enter work phone");
			$("#phone").focus();
			return false;
		}
	     else if(!find.test($.trim($("#phone").val()))){
			alert("Please enter valid work phone");
			$("#phone").focus();
			return false;
		}
		else if($.trim($("#mobile").val())==""){
			alert("Please enter mobile phone");
			$("#mobile").focus();
			return false;
		}
	     else if(!find.test($.trim($("#mobile").val()))){
			alert("Please enter valid mobile phone");
			$("#mobile").focus();
			return false;
		}*/
		else if($.trim($("#email").val())==""){
		 alert("Please enter email");
		 $("#email").focus();
		 return false;
	  }
	 else if(!emailPattern.test($.trim($("#email").val()))){
		alert("Please enter valid email");
		$("#email").focus();
		return false;
	 }
	 else if($.trim($("#product_page1").val())=="0"){
			alert("Please select affiliate product page 1");
			$("#product_page1").focus();
			return false;
	}
	/*else if($.trim($("#product_page2").val())=="0"){
			alert("Please select affiliate product page 2");
			$("#product_page2").focus();
			return false;
	}
	else if($.trim($("#product_page3").val())=="0"){
			alert("Please select affiliate product page 3");
			$("#product_page3").focus();
			return false;
	}
	else if($.trim($("#product_page1").val())==$.trim($("#product_page2").val())){
			alert("Affiliate product page 1 and affiliate product page 2 should not be equal");
			$("#product_page1").focus();
			return false;
	}
	else if($.trim($("#product_page2").val())==$.trim($("#product_page3").val())){
			alert("Affiliate product page 2 and affiliate product page 3 should not be equal");
			$("#product_page2").focus();
			return false;
	}
	else if($.trim($("#product_page3").val())==$.trim($("#product_page1").val())){
			alert("Affiliate product page 1 and affiliate product page 3 should not be equal");
			$("#product_page1").focus();
			return false;
	}
   else if($.trim($("#address1").val())==""){
			alert("Please enter address1");
			$("#address1").focus();
			return false;
		}
		
		else if($.trim($("#suburb").val())==""){
			alert("Please enter suburb");
			$("#suburb").focus();
			return false;
		}
		else if($.trim($("#state").val())==""){
			alert("Please enter state");
			$("#state").focus();
			return false;
		}
		else if($.trim($("#postcode").val())==""){
			alert("Please enter postcode");
			$("#postcode").focus();
			return false;
		}*/
		else if($.trim($("#country").val())=="0"){
			alert("Please select country");
			$("#country").focus();
			return false;
		}
		/*else if($.trim($("#website").val())==""){
			alert("Please enter website");
			$("#website").focus();
			return false;
		}
		else if(!re.test($.trim($("#website").val()))){
			alert("Please enter valid  website");
			$("#website").focus();
			return false;
		}*/
		else if($.trim($("#business_type").val())==" "){
			alert("Please enter business type");
			$("#business_type").focus();
			return false;
		}
      /*  else if($.trim($("#currency").val())=="0"){
			alert("Please select currency");
			$("#currency").focus();
			return false;
		}
		 else if($.trim($("#up_kick_back").val())==""){
			alert("Please enter upfront kickback");
			$("#up_kick_back").focus();
			return false;
		}
		else if(!decimal.test($.trim($("#up_kick_back").val()))){
			alert("Please enter valid  upfront kickback");
			$("#up_kick_back").focus();
			return false;
		}
		 else if($.trim($("#monthly_kick_back").val())==""){
			alert("Please enter monthly kickback");
			$("#monthly_kick_back").focus();
			return false;
		}
		else if(!decimal.test($.trim($("#monthly_kick_back").val()))){
			alert("Please enter valid  monthly kickback");
			$("#monthly_kick_back").focus();
			return false;
		}*/
		else if($.trim($("#tbc").val())=="0"){
			alert("Please select affiliate status");
			$("#tbc").focus();
			return false;
		}
		else if($.trim($("#licensee").val())=="0"){
			alert("Please select licensee ");
			$("#licensee").focus();
			return false;
		}
	    else{
				$.ajax({url:"check_affiliate1.php?page1="+page1+"&id="+hdn_special_id,success:function(result){
				 //alert(result);
				if(parseInt(result)>0){
				   alert('Affiliate product page 1 already exists.Please select another Affiliate product page 1.')
				   $("#product_page1").focus(); 
				   return false;
				}
				else{
						$.ajax({url:"check_affiliate2.php?page2="+page2+"&id="+hdn_special_id,success:function(result){
							 //alert(result);
						 if(parseInt(result)>0){
								  alert('Affiliate product page 2 already exists.Please select another Affiliate product page 2.')
				                  $("#product_page2").focus();
								return false;
						}
						else{
									$.ajax({url:"check_affiliate3.php?page3="+page3+"&id="+hdn_special_id,success:function(result){
									  //alert(result);
									if(parseInt(result)>0){
									      alert('Affiliate product page 3  already exists.Please select another Affiliate product page 3.')
				                         $("#product_page3").focus();
									     return false;
									}
									else{
												$.ajax({url:"user_check.php?name="+username+"&id=0",success:function(result){
													if(parseInt(result)>0 && hdn_special_id=="0"){
														alert('Full name already exists.Please enter another full name.')
														$("#full_name").focus(); 
														return false;
													}
													else{
														$("#affiliate_form").submit();
													}
												}});















										//$("#affiliate_form").submit();
									}
									}});	
						  }
						}});		    
				   }
				}});
		  
	} 
	});
	/*
	*  
	*Validation Add Licensee.
	*/
	 
       $("#add_licensee").click(function(){
		var username=$.trim($("#full_name").val());
		 var user_id=$("#hdn_id").val();
		 if(user_id==""){
			user_id=0;
		 }
       /* if($.trim($("#joined_date").val())==""){
			alert("Please enter joined_date");
			$("#joined_date").focus();
			return false;
		}
		else*/ 
		if($.trim($("#full_name").val())==""){
			alert("Please enter full name");
			$("#full_name").focus();
			return false;
		}
		/*else if($.trim($("#business_name").val())==""){
			alert("Please enter business name");
			$("#business_name").focus();
			return false;
		}
		else if($.trim($("#phone").val())==""){
			alert("Please enter work phone");
			$("#phone").focus();
			return false;
		}
	     else if(!find.test($.trim($("#phone").val()))){
			alert("Please enter valid work phone");
			$("#phone").focus();
			return false;
		}
		else if($.trim($("#mobile").val())==""){
			alert("Please enter mobile phone");
			$("#mobile").focus();
			return false;
		}
	     else if(!find.test($.trim($("#mobile").val()))){
			alert("Please enter valid mobile phone");
			$("#mobile").focus();
			return false;
		}*/
		else if($.trim($("#email").val())==""){
		 alert("Please enter email");
		 $("#email").focus();
		 return false;
	  }
	 else if(!emailPattern.test($.trim($("#email").val()))){
		alert("Please enter valid email");
		$("#email").focus();
		return false;
	 }
	 /* else if($.trim($("#address1").val())==""){
			alert("Please enter address 1");
			$("#address1").focus();
			return false;
		}
		else if($.trim($("#address2").val())==""){
			alert("Please enter address2");
			$("#address2").focus();
			return false;
		} 
		else if($.trim($("#suburb").val())==""){
			alert("Please enter suburb");
			$("#suburb").focus();
			return false;
		}
		else if($.trim($("#state").val())==""){
			alert("Please enter state");
			$("#state").focus();
			return false;
		}
		else if($.trim($("#postcode").val())==""){
			alert("Please enter postcode");
			$("#postcode").focus();
			return false;
		}*/
		
		
		else if($.trim($("#country").val())=="0"){
			alert("Please select country");
			$("#country").focus();
			return false;
		}
		/*else if($.trim($("#website").val())==""){
			alert("Please enter website");
			$("#website").focus();
			return false;
		}
		else if(!re.test($.trim($("#website").val()))){
			alert("Please enter valid  website");
			$("#website").focus();
			return false;
		}*/
		else if($.trim($("#business_type").val())==""){
			alert("Please enter business type");
			$("#business_type").focus();
			return false;
		}
        /*else if($.trim($("#currency").val())=="0"){
			alert("Please select currency");
			$("#currency").focus();
			return false;
		}
		 else if($.trim($("#up_kick_back").val())==""){
			alert("Please enter upfront kickback");
			$("#up_kick_back").focus();
			return false;
		}
		else if(!decimal.test($.trim($("#up_kick_back").val()))){
			alert("Please enter valid  upfront kickback");
			$("#up_kick_back").focus();
			return false;
		}
		 else if($.trim($("#monthly_kick_back").val())==""){
			alert("Please enter monthly kickback");
			$("#monthly_kick_back").focus();
			return false;
		}
		else if(!decimal.test($.trim($("#monthly_kick_back").val()))){
			alert("Please enter valid  monthly kickback");
			$("#monthly_kick_back").focus();
			return false;
		}
		else if($.trim($("#sales_person").val())=="0"){
			alert("Please select salesperson");
			$("#business_type").focus();
			return false;
		} 
		
		else{
			$("#licencee_form").submit();
		}*/
		else{
		     $.ajax({url:"user_check.php?name="+username+"&id=0",success:function(result){
				if(parseInt(result)>0 && user_id=="0"){
					alert('Full name already exists.Please enter another full name.')
					$("#full_name").focus(); 
					return false;
				}
				else{
					$("#licencee_form").submit();
				}
			}});
		}

	});
	/*
	*  
	*Validation Add Terms.
	*/
	 $("#add_terms_content").click(function(){
	   if($.trim(tinyMCE.get('terms_content').getContent())==""){
		alert("Please enter terms of service");
		$("#terms_content").focus();
		return false;
	   }
	   else{
		$("#terms_form").submit();
	   }
	   
  });
  $("#add_affiliate_status").click(function(){
		var cid=$("#callstatus_form_id").val();
		if(cid==""){
		  cid=0;
		}
		var callstatus=$.trim($("#callstatus").val());
		var callstatuscolor=$.trim($("#call_stauts_color").val());
		if(callstatus==""){
			alert("Please enter  affiliate status");
			$("#callstatus").focus();
			return false;
		}
		else if(callstatuscolor==""){
			alert("Please enter  affiliate status color");
			$("#call_stauts_color").focus();
			return false;
		}
		else{
			$.ajax({url:"affiliate_check.php?name="+callstatus+"&id="+cid+"",success:function(result){
			if(parseInt(result)>0){
				alert('Affiliate status already exists.Please enter another Affiliate status.')
				$("#callstatus").focus(); 
				return false;
			 }
			 else{
				$("#affiliate_status_form").submit();
			 }
		  }});

		 }
	});


	
	$("#add_tunelist").click(function(){
		 var tuneid=$("#hdn_tune_id").val();
			if(tuneid==""){
				tuneid=0;
			}
		var category=$.trim($("#itune_name").val());
		if(category==""){
			alert("Please enter itune name");
			$("#itune_name").focus();
			return false;
		}
		else{
				$.ajax({url:"ajax.php?name="+category+"&id="+tuneid+"",success:function(result){
					if(parseInt(result)>0){
						alert('itunes category already exists.Please enter another itunes category.')
						$("#itune_name").focus(); 
						return false;
					}
					else{
						$("#itune_form").submit();
					}
				  }});
				
			  }
					
		
	});
	$("#add_versionlist").click(function(){
		var versionid=$("#hdn_version_id").val();
			if(versionid==""){
				 versionid=0;
			}
		var app_version=$.trim($("#app_version").val());
		if(app_version==""){
			alert("Please enter app version");
			$("#app_version").focus();
			return false;
		}
		var version_des=$.trim($("#version_des").val());
		if(version_des==""){
			alert("Please enter version description");
			$("#version_des").focus();
			return false;
		}
		else{
				$.ajax({url:"ajax_version.php?name="+app_version+"&id="+versionid+"",success:function(result){
					if(parseInt(result)>0){
						alert('App version already exists.Please enter another app version.')
						$("#version_form").focus(); 
						return false;
					}
					else{
						$("#version_form").submit();
					}
				  }});
				
			  }
					
		
	});
	$("#add_status").click(function(){
		 var statusid=$("#status_form_id").val();
			if(statusid==""){
				statusid=0;
			}
		var status=$.trim($("#stauts").val());
		var statuscolor=$.trim($("#stauts_color").val());
		if(status==""){
			alert("Please enter status");
			$("#stauts").focus();
			return false;
		}
		if(statuscolor==""){
			alert("Please enter status color");
			$("#stauts_color").focus();
			return false;
		}
		else if($.trim($("#email").val())==""){
			alert("Please enter email");
			$("#email").focus();
			return false;
		}
		else if(!emailPattern.test($.trim($("#email").val()))){
			alert("Please enter valid email");
			$("#email").focus();
			return false;
		}
		else if(!testEmails($.trim($("#email").val()))){
			 alert("Please enter valid emails");
			 $("#email").focus();
			 return false;
		 }
		else{
			$.ajax({url:"action.php?name="+status+"&id="+statusid+"",success:function(result){
				
					if(parseInt(result)>0){
						alert('stauts name already exists.Please enter another stauts .')
						$("#stauts").focus(); 
						return false;
					}
					else{
						$("#status_form").submit();
					}
				  }});
				
			  }
					
		
	});
	$("#add_booking").click(function(){
		var bookid=$("#hdn_book_id").val();
		if(bookid==""){
			bookid=0;
		}
		var book_table=$.trim($("#book_table").val());
		if(book_table==""){
			alert("Please enter book a table");
			$("#book_table").focus();
			return false;
		}
		else{
			$.ajax({url:"check_booking.php?name="+book_table+"&id="+bookid,success:function(result){
				if(parseInt(result)>0){
						alert('Book a table already exists.Please enter another book a table.')
						$("#book_table").focus(); 
						return false;
					}
					else{
						$("#book_form").submit();
					}
				  }});
				
			  }
					
		
	});
	$("#add_language").click(function(){
   var langid=$("#language_form_id").val();
   if(langid==""){
    langid=0;
   }
  var language=$.trim($("#language").val());
  if(language==""){
   alert("Please enter language");
   $("#language").focus();
   return false;
  }
  else{
   $.ajax({url:"language.php?name="+language+"&id="+langid+"",success:function(result){
    
     if(parseInt(result)>0){
      alert('Language name already exists.Please enter another language .')
      $("#language").focus(); 
      return false;
     }
     else{
      $("#language_form").submit();
     }
      }});
    
     }
     
  
 });
 $("#add_person").click(function(){
   var pid=$("#person_form_id").val();
   if(pid==""){
    pid=0;
   }
  var person=$.trim($("#person").val());
  if(person==""){
   alert("Please enter salesperson");
   $("#person").focus();
   return false;
  }
  else if($.trim($("#person_email").val())==""){
   alert("Please enter salesperson email");
   $("#person_email").focus();
   return false;
  }
  else if(!emailPattern.test($.trim($("#person_email").val()))){
   alert("Please enter valid salesperson email");
   $("#person_email").focus();
   return false;
  }
  else if($.trim($("#sales_manager").val())=="0"){
   alert("Please select sales manager");
   $("#sales_manager").focus();
   return false;
  }
  else{
   $.ajax({url:"person.php?name="+person+"&id="+pid+"",success:function(result){
    
     if(parseInt(result)>0){
      alert('Salesperson  already exists.Please enter another salesperson .')
      $("#person").focus(); 
      return false;
     }
     else{
      $("#person_form").submit();
     }
      }});
    
     }
     
  
 });
 $("#add_take").click(function(){
   var id=$("#take_form_id").val();
   if(id==""){
    id=0;
   }
  var take=$.trim($("#take").val());
  if(take==""){
   alert("Please enter take-away");
   $("#take").focus();
   return false;
  }
  else{
   $.ajax({url:"take.php?name="+take+"&id="+id+"",success:function(result){
    
     if(parseInt(result)>0){
      alert('Take-away already exists.Please enter another take-away .')
      $("#take").focus(); 
      return false;
     }
     else{
      $("#take_form").submit();
     }
      }});
    
     }
     
  
 });
  $("#add_country").click(function(){
   var countryid=$("#country_form_id").val();
   //alert(countryid);
   if(countryid==""){
    countryid=0;
   }
  var country=$.trim($("#country").val());
  if(country==""){
   alert("Please enter country");
   $("#country").focus();
   return false;
  }
  else{
   $.ajax({url:"county_check.php?name="+country+"&id="+countryid,success:function(result){
    
     if(parseInt(result)>0){
      alert('country already exists.Please enter another country.')
      $("#country").focus(); 
      return false;
     }
     else{
       $("#country_form").submit();
     }
      }});
    
     }
     
  
 });
	 
	 $("#add_template").click(function(){
		  if($.trim($("#template_title").val())==""){
			 alert("Please enter message title");
			 $("#template_title").focus();
			 return false;
		 }
		 else if($.trim($("#template_from").val())==""){
			 alert("Please enter from email");
			 $("#template_from").focus();
			 return false;
		 }
		
		 else if($.trim($("#template_to").val())==""){
			 alert("Please enter to email");
			 $("#template_to").focus();
			 return false;
		 }
		 else if(!testEmails($.trim($("#template_to").val()))){
			 alert("Please enter valid to email");
			 $("#template_to").focus();
			 return false;
		 }
		 else if($.trim($("#template_subject").val())==""){
			 alert("Please enter subject");
			 $("#template_subject").focus();
			 return false;
		 }
		 else if($.trim(tinyMCE.get('message').getContent())==""){
			 alert("Please enter message");
			 $("#message").focus();
			 return false;
		 }
                else if($.trim($("#sms_message").val())==""){
			 alert("Please enter sms message");
			 $("#sms_message").focus();
			 return false;
		 }
		 else{
			 $("#template_form").submit();
		 }
		 
	 });
	 $("#validate_card").click(function(){
		 var cardtype=$("#cardtype").val();
		 if($.trim($("#first_name").val())==""){
			 alert("Please enter first name");
			 $("#first_name").focus();
			 return false;
		 }
		 else if($.trim($("#last_name").val())==""){
			 alert("Please enter last name");
			 $("#last_name").focus();
			 return false;
		 }
		 else if($.trim($("#address1").val())==""){
			 alert("Please enter address 1");
			 $("#address1").focus();
			 return false;
		 }
		 else if($.trim($("#address1").val())==""){
			 alert("Please enter address 2");
			 $("#address1").focus();
			 return false;
		 }
		 else if($.trim($("#city").val())==""){
			 alert("Please enter city");
			 $("#city").focus();
			 return false;
		 } else if($.trim($("#state").val())==""){
			 alert("Please enter state");
			 $("#state").focus();
			 return false;
		 }
		 else if($.trim($("#selectCountry").val())=="0"){
			 alert("Please select coutry");
			 $("#selectCountry").focus();
			 return false;
		 }
		 else if($.trim($("#zip").val())==""){
			 alert("Please enter zip");
			 $("#zip").focus();
			 return false;
		 }
		  else if($.trim($("#card_no").val())==""){
			 alert("Please enter card number");
			 $("#card_no").focus();
			 return false;
		 }
		 else if(!validateCardNumber(cardtype,$.trim($("#card_no").val()))){
			 alert("Please enter valid "+cardtype+"card number");
			 $("#card_no").focus();
			 return false;
		 }
		 else if($.trim($("#exp_month").val())=="0"){
			 alert("Please select expire month");
			 $("#exp_month").focus();
			 return false;
		 }
		 else if($.trim($("#exp_month").val())=="0"){
			 alert("Please select expire month");
			 $("#exp_month").focus();
			 return false;
		 }
		 else if($.trim($("#exp_year").val())=="0"){
			 alert("Please select expire year");
			 $("#exp_year").focus();
			 return false;
		 }
		 else if(parseInt($.trim($("#exp_year").val()))==parseInt($.trim($("#curyear").val())) && parseInt($.trim($("#exp_month").val()))<parseInt($.trim($("#curmonth").val()))){
			alert("Please select valid exp date");
			$("#exp_month").focus();
			return false;
		 }
		 else if($.trim($("#cvc_no").val())==""){
			 alert("Please enter cvc number");
			 $("#cvc_no").focus();
			 return false;
		 }
		 else{
			
			 $("#paypal_form").submit();
		 }
	 });
$("#add_manager").click(function(){
	var mid=$("#manager_form_id").val();
   if(mid==""){
      mid=0;
   }
  var manager=$.trim($("#manager").val());
  if(manager==""){
   alert("Please enter sales manager");
   $("#manager").focus();
   return false;
  }
  else if($.trim($("#manager_email").val())==""){
   alert("Please enter sales manager email ");
   $("#manager_email").focus();
   return false;
  }
  else if(!emailPattern.test($.trim($("#manager_email").val()))){
   alert("Please enter valid sales manager email ");
   $("#manager_email").focus();
   return false;
  }
  else{
   $.ajax({url:"manager.php?name="+manager+"&id="+mid+"",success:function(result){
    
     if(parseInt(result)>0){
      alert('Sales manager  already exists.Please enter another sales manager .')
      $("#manager").focus(); 
      return false;
     }
     else{
      $("#manager_form").submit();
     }
      }});
    
     }
    });
	
	$("#add_callstatus").click(function(){
		var cid=$("#callstatus_form_id").val();
		if(cid==""){
		  cid=0;
		}
		var callstatus=$.trim($("#callstatus").val());
		var callstatuscolor=$.trim($("#call_stauts_color").val());
		if(callstatus==""){
			alert("Please enter callstatus");
			$("#callstatus").focus();
			return false;
		}
		else if(callstatuscolor==""){
			alert("Please enter callstatus color");
			$("#call_stauts_color").focus();
			return false;
		}
		else{
			$.ajax({url:"callstatus_check.php?name="+callstatus+"&id="+cid+"",success:function(result){
			if(parseInt(result)>0){
				alert('Callstatus already exists.Please enter another callstatus.')
				$("#callstatus").focus(); 
				return false;
			 }
			 else{
				$("#callstatus_form").submit();
			 }
		  }});

		 }
	});
	$("#get_list").click(function(){
		var filename=$("#import_file").val();
		var filenameArr=filename.split(".");
		var extension=filenameArr.pop();
		if($("#import_file").val()==""){
			alert("Please select a file to upload");
			$("#import_file").focus();
			return false;
		}
		else if(extension!="xlsx"&&extension!="xlsm"&&extension!="xlsb"&&extension!="xltx"&&extension!="xltm"&&extension!="xls"&&extension!="xlt"&&extension!="xml"&&extension!="xlam"&&extension!="xla"&&extension!="xlw"&&extension!="csv"){
			alert("Please upload valid file");
			$("#import_file").focus();
			return false;
		}
		else{
			$("#importexcel_form").submit();
		}
	});
	/*$("#update_prospect").click(function(){
		
		 if($("#callstatus").val()=="0"){
			alert("Please select Call Status");
			$("#callstatus").focus();
			return false;
		}
		
		else{
			$("#prospect_form").submit();
		}
	});*/

	$("#filter_prospect").click(function(){
		var mysqlqry;
		var condition="";
		if($.trim($("#import_date1").val())==""&&$.trim($("#import_date2").val())==""&&multiselect("salesperson")&&multiselect("salesmanager")&&multiselect("callstatus")&&$("#suburb").val()=="0"&&$("#state").val()=="0"&&$("#last_contact1").val()==""&&$("#last_contact2").val()==""){
			alert("Please fill at least one field");
			$("#import_date1").focus();
			return false;
		}
		else{
			mysqlqry="select * from tbl_prospects";
			if($("#import_date1").val()!=""&&$("#import_date2").val()!=""){
				condition+=" and (import_date>='"+dateformatyyyymmdd($("#import_date1").val())+"' and import_date<='"+dateformatyyyymmdd($("#import_date2").val())+"')";
			}
			if($("#import_date1").val()!=""&&$("#import_date2").val()==""){
				condition+=" and import_date='"+dateformatyyyymmdd($("#import_date1").val())+"'";
			}
			if($("#import_date1").val()==""&&$("#import_date2").val()!=""){
				condition+=" and import_date='"+dateformatyyyymmdd($("#import_date2").val())+"'";
			}
			if(!multiselect("salesperson")){
				condition+=" and "+getMultiplevalues("salesperson","sales_person");
			}
			if(!multiselect("salesmanager")){
				condition+=" and "+getMultiplevalues("salesmanager","sales_manager");
			}
			if(!multiselect("callstatus")){

				condition+=" and "+getMultiplevalues("callstatus","callstatus");
			}
			if($("#suburb").val()!="0"){
				condition+=" and suburb='"+$("#suburb").val()+"'";
			}
			if($("#state").val()!="0"){
				condition+=" and state='"+$("#state").val()+"'";
			}
			if($("#last_contact1").val()!=""&&$("#last_contact2").val()!=""){
				condition+=" and (last_contact>='"+dateformatyyyymmdd($("#last_contact1").val())+"' and last_contact<='"+dateformatyyyymmdd($("#last_contact2").val())+"')";
			}
			if($("#last_contact1").val()!=""&&$("#last_contact2").val()==""){
				condition+=" and last_contact='"+dateformatyyyymmdd($("#last_contact1").val())+"'";
			}
			if($("#last_contact1").val()==""&&$("#last_contact2").val()!=""){
				condition+=" and last_contact='"+dateformatyyyymmdd($("#last_contact2").val())+"'";
			}
			//alert(mysqlqry+=(" where 1=1"+condition));
			mysqlqry+=encodeURIComponent(" where 1=1"+condition);
			//mysqlqry+=" where 1=1"+condition;
			//alert(mysqlqry);
			$.ajax({url:"checkresults.php?qry="+mysqlqry,success:function(result){
				//alert(result);
				if(parseInt(result)>0){
					$("#prospect_form").submit();
				 }
				 else{
					alert("There was no match to your filter. Please try again");
					return false;
				 }
		  }});
		}
	});
	$("#filter_master_list").click(function(){
		var condition='';
	var trial_date=$.trim($("#trial_date").val());
	var trial_date2=$.trim($("#trial_date2").val());
	var demo_date=$.trim($("#demo_date").val());
	var demo_date2=$.trim($("#demo_date2").val());
	var payment_date=$.trim($("#payment_date").val());
	var payment_date2=$.trim($("#payment_date2").val());
	var commission_date=$.trim($("#commission_date").val());
	var commission_date2=$.trim($("#commission_date2").val());
	var set;
    if((multiSelectedLength(document.getElementById("salesperson"))==0)&&(multiSelectedLength(document.getElementById("salesmanager"))==0)&&(multiSelectedLength(document.getElementById("status"))==0)&&(trial_date=='')&&(trial_date2=='')&&(demo_date=='')&&(demo_date2=='')&&(payment_date=='')&&(payment_date2=='')&&(commission_date=='')&&(commission_date2==''))
	{
	   alert('Please fill at least one field');
       $("#salesperson").focus();
	    return false;
		 
	}
    else{
		getQry="select * from tbl_demo";
		if($("#trial_date").val()!=""&&$("#trial_date2").val()!=""){
					condition+=" and (date_format(added_date,'%Y-%m-%d')>='"+dateformatyyyymmdd($("#trial_date").val())+"' and date_format(added_date,'%Y-%m-%d')<='"+dateformatyyyymmdd($("#trial_date2").val())+"')";
		 }
		 if($("#trial_date").val()!=""&&$("#trial_date2").val()==""){
					condition+=" and date_format(added_date,'%Y-%m-%d')='"+dateformatyyyymmdd($("#trial_date").val())+"'";
		 }
         if($("#trial_date2").val()!=""&&$("#trial_date").val()==""){
					condition+=" and date_format(added_date,'%Y-%m-%d')='"+dateformatyyyymmdd($("#trial_date2").val())+"'";
		 }
		 if($("#demo_date").val()!=""&&$("#demo_date2").val()!=""){
					condition+=" and (date_format(modified_date,'%Y-%m-%d')>='"+dateformatyyyymmdd($("#demo_date").val())+"' and date_format(modified_date,'%Y-%m-%d')<='"+dateformatyyyymmdd($("#demo_date2").val())+"')";
					set="yes";
		 }
		 if($("#demo_date").val()!=""&&$("#demo_date2").val()==""){
					condition+=" and date_format(modified_date,'%Y-%m-%d')='"+dateformatyyyymmdd($("#demo_date").val())+"'";
					set="yes";
		 }
         if($("#demo_date2").val()!=""&&$("#demo_date").val()==""){
					condition+=" and date_format(modified_date,'%Y-%m-%d')='"+dateformatyyyymmdd($("#demo_date2").val())+"'";
					set="yes";
		 }
		 if($("#payment_date").val()!=""&&$("#payment_date2").val()!=""){
					condition+=" and (payment_date>='"+dateformatyyyymmdd($("#payment_date").val())+"' and payment_date<='"+dateformatyyyymmdd($("#payment_date2").val())+"')";
		 }
		 if($("#payment_date").val()!=""&&$("#payment_date2").val()==""){
					condition+=" and payment_date='"+dateformatyyyymmdd($("#payment_date").val())+"'";
		 }
         if($("#payment_date2").val()!=""&&$("#payment_date").val()==""){
					condition+=" and payment_date='"+dateformatyyyymmdd($("#payment_date2").val())+"'";
		 }
		 if($("#commission_date").val()!=""&&$("#commission_date2").val()!=""){
					condition+=" and (commission_date>='"+dateformatyyyymmdd($("#commission_date").val())+"' and commission_date<='"+dateformatyyyymmdd($("#commission_date2").val())+"')";
		 }
		 if($("#commission_date").val()!=""&&$("#commission_date2").val()==""){
					condition+=" and commission_date='"+dateformatyyyymmdd($("#commission_date").val())+"'";
		 }
         if($("#commission_date2").val()!=""&&$("#commission_date").val()==""){
					condition+=" and commission_date='"+dateformatyyyymmdd($("#commission_date2").val())+"'";
		 }
         if(!multiselect("salesperson")){
           condition+=" and "+getMultiplevalues("salesperson","sales_person");
         }
         if(!multiselect("salesmanager")){
		  condition+=" and "+getMultiplevalues("salesmanager","sales_manager");
         }
	     if(!multiselect("status")){
           condition+=" and "+getMultiplevalues("status","status");
         }
		 
		 getQry+=encodeURIComponent(" where 1=1"+condition);
		 
		$.ajax({url:"checkresults1.php?qry="+getQry+"&set="+set,success:function(result){
		  //alert(result);
			if(parseInt(result)<=0){
			 alert('There was no match to your filter. Please try again');
			  return false;
			}
			else{
			 $("#filter_form").submit();
			}
        }});
	}
});
$("#add_enquiry").click(function(){
   var enquiryid=$("#enquiry_form_id").val();
   if(enquiryid==""){
    enquiryid=0;
   }
  var enquiry=$.trim($("#enquiry").val());
  if(enquiry==""){
   alert("Please enter enquiry");
   $("#enquiry").focus();
   return false;
  }
else if($.trim($("#enquiry_email").val())==""){
		alert("Please enter email");
		$("#enquiry_email").focus();
		return false;
	}
	else if(!emailPattern.test($.trim($("#enquiry_email").val()))){
		alert("Please enter valid email");
		$("#enquiry_email").focus();
		return false;
	}
  else{
   $.ajax({url:"enquiry_check.php?name="+enquiry+"&id="+enquiryid+"",success:function(result){
    
     if(parseInt(result)>0){
      alert('Enquiry name already exists.Please enter another enquiry .')
      $("#enquiry").focus(); 
      return false;
     }
     else{
      $("#enquiry_form").submit();
     }
      }});
    
     }
     
  
 });
 $("#add_testimonial").click(function(){
  var photoarr=$("#logo").val().split(".");
  var photolen=photoarr.length;
  var extension=photoarr[photolen-1].toLowerCase();;
   if($.trim($("#logo").val())==""&&mode=="Add"){
    alert('Please upload logo');
    $("#logo").focus();
    return false;
   }
   else if($("#logo").val()!=""&&extension!="jpg"&&extension!="jpeg"&&extension!="png"&&extension!="gif"&&extension!="bmp"){
    
     alert('Please upload valid logo');
     $("#logo").focus();
     return false;
    
   }
    else if($.trim($("#client_name").val())==""){
    alert("Please enter name");
    $("#client_name").focus();
    return false;
   }
    else if($.trim($("#res_name").val())==""){
    alert("Please enter restaurant name");
    $("#res_name").focus();
    return false;
   }
   else if($.trim($("#testimonial").val())==""){
    alert("Please enter testimonial");
    $("#testimonial").focus();
    return false;
   }
   else{
    $("#testimonial_form").submit();
   }
   
 });
 $("#add_aboutus_content").click(function(){
   if($.trim(tinyMCE.get('content').getContent())==""){
    alert("Please enter content");
    $("#content").focus();
    return false;
   }
   else{
    $("#aboutus_form").submit();
   }
   
  });
 
 $("#paypal_btn").click(function(){
		
	   if($.trim($("#card_no").val())==""){
		alert("Please enter card number");
		$("#card_no").focus();
		return false;
	   }
	   else if(!validateCardNumber($.trim($("#card_type").val()),$.trim($("#card_no").val()))){
			 alert("Please enter valid "+$.trim($("#card_type").val())+"card number");
			 $("#card_no").focus();
			 return false;
	  }
	   else if($.trim($("#exp_month").val())=="0"){
		alert("Please select expiry month");
		$("#exp_month").focus();
		return false;
	   }
	   else if($.trim($("#exp_year").val())=="0"){
		alert("Please select expiry year");
		$("#exp_year").focus();
		return false;
	   }
	   else if($.trim($("#ccv_no").val())==""){
		alert("Please enter ccv number");
		$("#ccv_no").focus();
		return false;
	   }
	   else if($.trim($("#card_holder").val())==""){
		alert("Please enter name on card");
		$("#card_holder").focus();
		return false;
	   }
	   if(appid=='1' || appid=='4'){
		  if($.trim($("#promo_code").val())==""){
			  alert("Sorry, a Promo Code is required to order this discounted product");
			  $("#promo_code").focus();
			  return false;
		  }
		  else if($.trim($("#promo_code").val())!=""&&$.trim($("#promo_code").val())!=promo_code){
			  alert("Sorry, that Promo Code is not recognized");
			  $("#promo_code").focus();
			  return false;
		  }
		  else{
			  $("#order_form").submit();
		  }
		}
	   else{
		$("#order_form").submit();
	   }
	   
  });

    $("#add_app").click(function(){
		var appid=$("#hdn_prom").val();
		var app_id=$("#hdn_id").val();
		if(app_id==""){
			app_id=0;
		}
		/*var page=$.trim($("#product_page").val());
	   if($.trim($("#title").val())==""){
		alert("Please enter title");
		$("#title").focus();
		return false;
	   }*/
	 if($.trim($("#app_content").val())==""){
		alert("Please enter description");
		$("#app_content").focus();
		 return false;
	   }
	   else if($.trim($("#product_country").val())=="0"){
		alert("Please select country");
		$("#product_country").focus();
		return false;
	   }
    /* else if($.trim($("#product_page").val())=="0"){
		alert("Please select product page");
		$("#product_page").focus();
		return false;
	   }*/
	   else if($.trim($("#product_currency").val())=="0"){
		alert("Please select currency");
		$("#product_currency").focus();
		return false;
	   }
	   
	   else if($.trim($("#retail_upfront_price").val())==""){
		alert("Please enter retail customisation fee");
		$("#retail_upfront_price").focus();
		return false;
	   }
	   else if(isNaN($.trim($("#retail_upfront_price").val()))){
		alert("Please enter valid retail customisation fee");
		$("#retail_upfront_price").focus();
		return false;
	   }
	   /* else if(!intPattern.test($.trim($("#retail_upfront_price").val()))){
		alert("Please enter valid retail customisation fee");
		$("#retail_upfront_price").focus();
		return false;
	   }*/
       else if($.trim($("#intial_price").val())==""){
		alert("Please enter promo customisation fee");
		$("#intial_price").focus();
		return false;
	   }
	   else if(isNaN($.trim($("#intial_price").val()))){
		alert("Please enter valid promo customisation fee");
		$("#intial_price").focus();
		return false;
	   }
	   /*else if(!intPattern.test($.trim($("#intial_price").val()))){
		alert("Please enter valid promo customisation fee");
		$("#intial_price").focus();
		return false;
	   } */
	   else if($.trim($("#retail_submission_price").val())==""){
		alert("Please enter retail submission fee");
		$("#retail_submission_price").focus();
		return false;
	   }
	   else if(isNaN($.trim($("#retail_submission_price").val()))){
		alert("Please enter valid retail submission fee");
		$("#retail_submission_price").focus();
		return false;
	   }
	   /*else if(!intPattern.test($.trim($("#retail_submission_price").val()))){
			alert("Please enter valid retail submission fee");
			$("#retail_submission_price").focus();
			return false;
	   }*/
	    else if($.trim($("#submission_price").val())==""){
		alert("Please enter promo submission fee");
		$("#submission_price").focus();
		return false;
	   }
	   else if(isNaN($.trim($("#submission_price").val()))){
		alert("Please enter valid promo submission fee");
		$("#submission_price").focus();
		return false;
	   }
	  /* else if(!intPattern.test($.trim($("#submission_price").val()))){
			alert("Please enter valid promo submission fee");
			$("#submission_price").focus();
			return false;
	   }*/
	   else if($.trim($("#retail_monthly_price").val())==""){
		alert("Please enter retail monthly fee");
		$("#retail_monthly_price").focus();
		return false;
	   }
	   else if(isNaN($.trim($("#retail_monthly_price").val()))){
		alert("Please enter valid retail monthly fee");
		$("#retail_monthly_price").focus();
		return false;
	   }
	  /* else if(!intPattern.test($.trim($("#retail_monthly_price").val()))){
			alert("Please enter valid retail monthly fee");
			$("#retail_monthly_price").focus();
			return false;
	   }*/
       else if($.trim($("#monthly_price").val())==""){
			alert("Please enter promo monthly fee");
			$("#monthly_price").focus();
			return false;
	   }
	   else if(isNaN($.trim($("#monthly_price").val()))){
		alert("Please enter valid promo monthly fee");
		$("#monthly_price").focus();
		return false;
	   }
	  /* else if(!intPattern.test($.trim($("#monthly_price").val()))){
			alert("Please enter valid promo monthly fee");
			$("#monthly_price").focus();
			return false;
	   }*/
	   else if($.trim($("#prepaid_months").val())==""){
			alert("Please enter prepaid months");
			$("#prepaid_months").focus();
			return false;
	   }

	   /*else if(parseInt($.trim($("#prepaid_months").val()))<=0){
			alert("Prepaid months should not be zero");
			$("#prepaid_months").focus();
			return false;
	   }*/
	   else{
						$("#app_form").submit();
		}
	 /*else{
			$.ajax({url:"check_page.php?page="+page+"&id="+app_id,success:function(result){
				//alert(result);
				if(parseInt(result)>0){
						alert('Product page already exists.Please select another product page.')
						$("#product_page").focus(); 
						return false;
					}
					else{
						$("#app_form").submit();
					}
				  }});
				
			  }*/
   
 });
 $("#add_promocode").click(function(){
		var promocode_id=$("#promocode_id").val();
		if(promocode_id==""){
			promocode_id=0;
		}
		var promocode=$.trim($("#promocode").val());
		if($.trim($("#promocode").val())==""){
			alert("Please enter promo code");
			$("#promocode").focus();
			return false;
		}
		else{
			$.ajax({url:"check_code.php?name="+promocode+"&id="+promocode_id,success:function(result){
				if(parseInt(result)>0){
						alert('Promo code already exists.Please enter another promo code.')
						$("#promocode").focus(); 
						return false;
					}
					else{
						$("#promocode_form").submit();
					}
				  }});
				
			  }
					
		
	});
	$("#add_homepage").click(function(){
		var hid=$("#homepage_form_id").val();
		if(hid==""){
		  hid=0;
		}
		var homepage_version=$.trim($("#homepage_version").val());
		if(homepage_version==""){
			alert("Please enter homepage version");
			$("#homepage_version").focus();
			return false;
		}
		else{
			$.ajax({url:"homepage_check.php?name="+homepage_version+"&id="+hid+"",success:function(result){
			if(parseInt(result)>0){
				alert('Homepage version already exists.Please enter another homepage version.')
				$("#homepage_version").focus(); 
				return false;
			 }
			 else{
				$("#homepage_form").submit();
			 }
		  }});

		 }
	});
	$("#add_business_type").click(function(){
		var hdn_business_id=$("#hdn_business_id").val();
		if(hdn_business_id==""){
			hdn_business_id=0;
		}
		var business_type=$.trim($("#business_type").val());
		if(business_type==""){
			alert("Please enter business type");
			$("#business_type").focus();
			return false;
		}
		else{
			$.ajax({url:"check_business.php?name="+business_type+"&id="+hdn_business_id,success:function(result){
				if(parseInt(result)>0){
						alert('Business type already exists.Please enter another business type.')
						$("#business_type").focus(); 
						return false;
					}
					else{
						$("#business_form").submit();
					}
				  }});
				
			  }
					
		
	});
	$("#add_special_one").click(function(){
		var hdn_special_id=$("#hdn_special_id").val();
		var special_one=$.trim($("#special_id_value").val());
		var page=$.trim($("#product_page").val());
		if(hdn_special_id==""){
			hdn_special_id=0;
		}
		var hdn_img=$("#hdn_img").val();
 
		var photoarr=$("#special_logo").val().split(".");
		var photolen=photoarr.length;
		var extension=photoarr[photolen-1].toLowerCase();
		/*if($.trim($("#special_id_value").val())==""){
			alert("Please enter special id");
			$("#special_id_value").focus();
			return false;
		}
                else if($.trim($("#territory").val())=="0"){
		 alert("Please select territory");
		 $("#territory").focus();
		 return false;
	       }*/
	    if($.trim($("#business_name").val())==""){
			alert("Please enter business name");
			$("#business_name").focus();
			return false;
		}
		else if($.trim($("#business_type").val())==""){
			alert("Please select business type");
			$("#business_type").focus();
			return false;
		}
		else if($.trim($("#product_page").val())=="0"){
		 alert("Please select product page");
		 $("#product_page").focus();
		 return false;
	    }
		else if($.trim($("#promo_code").val())==""){
			alert("Please enter promo code");
			$("#promo_code").focus();
			return false;
		}
        else if($.trim($("#special_logo").val())=="" && hdn_img=="" && document.getElementById("simulator").checked==true){
			alert("Please upload business logo");
			$("#special_logo").focus();
			return false;
		}
		else if($.trim($("#special_logo").val())!="" &&extension!="jpg" && extension!="jpeg" && extension!="png" && document.getElementById("simulator").checked==true ){
		   alert('Please upload valid business logo');
		   $("#special_logo").focus();
		   return false;
    
        }
	    else if(document.getElementById("simulator").checked==true && $.trim($("#theme").val())==""){
			alert("Please select design style");
			$("#theme").focus();
			return false;
	    }
		/*else if($.trim($("#heading").val())==""){
			alert("Please enter special heading");
			$("#heading").focus();
			return false;
		}

		else if($.trim(tinyMCE.get('content').getContent())==""){
			alert("Please enter special content");
			$("#content").focus();
			return false;
		}
		else if($.trim($("#spl_bt1").val())==""){
			alert("Please enter special button 1");
			$("#spl_bt1").focus();
			return false;
		}
		else if($.trim($("#spl_bt2").val())==""){
			alert("Please enter special button 2");
			$("#spl_bt2").focus();
			return false;
		}*/
		
		/*else if($.trim(tinyMCE.get('confirm_msg').getContent())==""){
			alert("Please enter confirmation message");
			$("#confirm_msg").focus();
			return false;
		}
		else if($.trim(tinyMCE.get('bottom_quote').getContent())==""){
			alert("Please enter bottom quote");
			 $("#bottom_quote").focus();
			
			return false;
		}
		
		else if($.trim(tinyMCE.get('side_quote').getContent())==""){
			alert("Please enter side quote");
		     $("#side_quote").focus();
			return false;
		}*/
		else{
			 
			$("#special_form").submit();
						 
                }
		
				 
	});
	
});
function checkPrevilages(){
	var check1=document.getElementById("trial_form").checked;
	var check2=document.getElementById("demo_page").checked;
	var check3=document.getElementById("master_list").checked;
	var check4=document.getElementById("download_list").checked;
	var check5=document.getElementById("payment_info").checked;
	var check6=document.getElementById("licensee_list").checked;
	/*var check7=document.getElementById("alliance_info").checked;
	var check8=document.getElementById("tbc").checked;*/
	if(!check1&&!check2&&!check3&&!check4&&!check5&&!check6){
		return false;
	}
	else
		return true;
}
function testEmails(emails){
	var emailPattern=/^[a-zA-Z0-9\-_]+(\.[a-zA-Z0-9\-_]+)*@[a-z0-9]+(\-[a-z0-9]+)*(\.[a-z0-9]+(\-[a-z0-9]+)*)*\.[a-z]{2,4}$/;
	var emailsArr=emails.split(",");
	var novalid=0;
	var valid=0;
	var emailsArrlen=emailsArr.length;
	for(i=0;i<emailsArr.length;i++){
		email=$.trim(emailsArr[i]);
		if(i==0){
			if(!emailPattern.test(email)){
				novalid++;
			}
			else{
				valid++;
			}
		}
		else{
			if($.trim(email)!=""){
				if(!emailPattern.test(email)){
					novalid++;
				}
				else{
					valid++;
				}
			}
			else{
				valid++;
			}
		}
	}
	if(valid==emailsArrlen){
		return true;
	}
	else{
		return false;
	}
}
function validateCardNumber(cardtype,cardnumber){
	var visapattern= /^4/;
	var mastercardpattern= /^5[1-5]/;
	if(cardtype=="visa"){
		if(!visapattern.test(cardnumber)){
			return false;
		}
		else{
			return true;
		}
	}
	else if(cardtype=="mastercard"){
		if(!mastercardpattern.test(cardnumber)){
			return false;
		}
		else{
			return true;
		}
	}
}
function multiselect(elementid){
  var element=document.getElementById(elementid);
  var eleoptions=element.options;
  var cnt=0;
  for(var m=0;m<eleoptions.length;m++){
	if(eleoptions[m].selected){
		cnt++;
	}
  }
 if(cnt>0)
	return false;
 else
	return true;
}
function getMultiplevalues(elementid,fieldname){
	var element=document.getElementById(elementid);
	var eleoptions=element.options;
	var str="";
	var i=0;
	var retstr;
	var strArr=new Array();
	for(var m=0;m<eleoptions.length;m++){
		if(eleoptions[m].selected){
			strArr[i]=fieldname+"='"+eleoptions[m].value+"'";
			//alert(strArr[i]);
			i++;
		}
	}
	retstr="("+strArr.join(" OR ")+")";
	return retstr;
}
function dateformatyyyymmdd(givendate){
	givendateArr=givendate.split("-");
	retdate=givendateArr[2]+"-"+givendateArr[1]+"-"+givendateArr[0];
	return retdate;
}
function multiSelectedLength(element){
	var count=0;
	for (var i=0; i < element.options.length; i++) {
	  if (element.options[i].selected) 
		  count++;
	}
	return count;
}
function matchPromoCode(promovalue){
	var pr;
	var set=false;
	for(pr=0;pr<promoCodeArr.length;pr++){
		if(promovalue==promoCodeArr[pr])
			set=true;
	}
	return set;
}
function onlyNumbers(event) {
	var charCode = (event.which) ? event.which : event.keyCode
	if (charCode > 31 && (charCode < 48 || charCode > 57))
		return false;

	return true;
}