<?php

$file_name = basename($_SERVER["PHP_SELF"]);
function readmore_view($string,$length){
     $string = strip_tags($string);
	 if (strlen($string) > $length) {
		$stringCut = substr($string, 0, $length);
		 $string =$stringCut.'...';
	 }
   return $string;
}

$Current_PHP_File = basename($_SERVER['SCRIPT_NAME']);
$postedcuntry = "";
$engselected = $swdselected = "";
if(isset($_POST["langcountry"])) {
	$postedcuntry = $_POST["langcountry"];
	if ($postedcuntry == "English") {
		$engselected = "selected";
		$_SESSION["language"] = $postedcuntry;
		include("lang/eng_lang.php");
		$getLangContent = funLangContent($postedcuntry);	
	} else {
		$swdselected = "selected";
		$_SESSION["language"] = $postedcuntry;
		include("lang/swd_lang.php");
		$getLangContent  = funLangContent($postedcuntry);
	}
} else {
	if(isset($_SESSION["language"])) {
		$postedcuntry = $_SESSION["language"];
		include("lang/".($postedcuntry=="SEK" ? "swd" : "eng")."_lang.php");
		if ($postedcuntry=="SEK")
			$swdselected = "selected";
		else
			$engselected = "selected";

		$getLangContent = funLangContent($postedcuntry);
	}
	else if($currency_flag=="SEK"){
		$swdselected = "selected";
		$_SESSION["language"] = 'SEK';
		include("lang/swd_lang.php");
		$getLangContent = funLangContent('SEK');
     
	} else{
		$engselected = "selected";
		$_SESSION["language"] = 'English';
		include("lang/eng_lang.php");
		$getLangContent = funLangContent('English');
	}
}

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<title>Salon Emotion Consulting Tool</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="shortcut icon" href="../images/Fav.ico" type="image/ico">
	<link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/bootstrap-datepicker.min.css" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>    
    <script type="text/javascript" src="js/bootstrap-datepicker.min.js"></script>
    <link rel="stylesheet" type="text/css" href="css/msdropdown/flags.css" />
    <link rel="stylesheet" type="text/css" href="css/loreal_style.css" />
    <!-- <msdropdown> -->
    <link rel="stylesheet" type="text/css" href="css/msdropdown/dd.css" />
    <script src="js/msdropdown/jquery.dd.js"></script>
    <link rel="stylesheet" type="text/css" href="css/msdropdown/skin2.css" />
    
    <!-- </msdropdown> -->
	<meta name="Generator" content="EditPlus">
	<meta name="Author" content="">
	<meta name="Keywords" content="">
	<meta name="Description" content="">
</head>

<body>

    <div class="col-md-12 col-sm-12 col-xs-12 main_header">
        <div class="container">
            <div class="col-md-3 col-sm-3 col-xs-3 col-lg-3">
        	    <h1 class="headerlogotxt">L'Oréal</h1>
        	</div>
        	<div class="col-md-7 col-sm-7 col-xs-7 col-lg-7">
			<?php
			if($Current_PHP_File!="login.php" || $Current_PHP_File!="forgotpassword.php"){
				?>
						<p class="session_name"><strong><?php echo readmore_view($_SESSION['user_name'],"20");?></strong></p>
						<p class="logout" ><a href="logout.php" class="logout" >Logout</a></p>
				<?php
			}
			?>
        	</div>
        	<div class="col-md-2 col-sm-2 col-xs-2 col-lg-2">
        	    <form method="post" id="loreallaguageForms" name="loreallaguageForms">
                    <input type="hidden" id="langcountry" name="langcountry" value="">
                    <select name="lorealcountries" id="lorealcountries" style="width: 150px; outline: transparent;background: black;border:1px solid black;">
	                      <option  value="" <?php echo $engselected; ?> data-image="images/icons/english.png" data-imagecss="flag ad" data-title="English" >English</option>
	                      <option value=""  <?php echo $swdselected; ?> data-image="images/icons/sweden.png" data-imagecss="flag ad" data-title="Svenska">Svenska</option>
                    </select>
                </form>
        	</div>
        </div>
    </div>