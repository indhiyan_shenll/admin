<?php
    $list_id=$demoId;
    $getServicesQry="select * from tbl_prospectinglist_services where list_id=:demoid and reset='0'";
    $pepServiceQry=$DBCONN->prepare($getServicesQry);
    $pepServiceQry->execute(array(':demoid'=>$list_id));
    $getserviceCount=$pepServiceQry->rowcount();
    $getserviceRow=$pepServiceQry->fetch();
    $service_id          = stripslashes($getserviceRow["services_id"]);
    $app_design_log      = stripslashes($getserviceRow["app_design_log"]);
    $app_design_date     = stripslashes($getserviceRow["app_design_date"]);
    list($year,$month, $day) =explode("-",$app_design_date);
    $app_design_date     =$day."/".$month."/".$year ;
    if(($app_design_log=="" || $app_design_date=="00/00/0000" || $app_design_date=="" )&&($user_type=="Admin"||$user_type=="Affiliate" ||$user_type=="DEM")){
        $app_design_date="";
        $app_design_btn_disabled="logdisplayshow";
        $ios_built_btn_disabled="logdisplay";
        $android_built_btn_disabled="logdisplay";
    }
    elseif($app_design_log=="" && $app_design_date=="00/00/0000" && $user_type=="Admin"){
        $app_design_date="";
        $app_design_btn_disabled="logdisplayshow";
        $ios_built_btn_disabled="logdisplay";
        $android_built_btn_disabled="logdisplay";
    }
    elseif($app_design_log=="" && $app_design_date=="00/00/0000" && $user_type=="Developer"){
    	$app_design_date="";
        $app_design_btn_disabled="logdisplay";
        $ios_built_btn_disabled="logdisplay";
        $android_built_btn_disabled="logdisplay";
    }
    elseif($app_design_log=="" && $app_design_date=="00/00/0000" && $user_type=="Affiliate"){
        $app_design_date="";
        $app_design_btn_disabled="logdisplayshow";

    }
	elseif($app_design_log=="" && $app_design_date=="00/00/0000" && $user_type=="DEM"){
        $app_design_date="";
        $app_design_btn_disabled="logdisplayshow";
    }
	elseif($app_design_log!="" && $app_design_date!="00/00/0000"  && $user_type=="Admin") {
        $app_design_btn_disabled="logdisplayshow";
        $ios_built_btn_disabled="logdisplayshow";
        $android_built_btn_disabled="logdisplayshow";
    }elseif($app_design_log!="" && $app_design_date!="00/00/0000" && $user_type=="Developer"){
        $app_design_btn_disabled="logdisplay";
        $ios_built_btn_disabled="logdisplayshow";
        $android_built_btn_disabled="logdisplayshow";
    }
    elseif($app_design_log!="" && $app_design_date!="00/00/0000" && $user_type=="Affiliate"){
        $app_design_btn_disabled="logdisplay";
    }
	elseif($app_design_log!="" && $app_design_date!="00/00/0000" && $user_type=="DEM"){
        $app_design_btn_disabled="logdisplay";
    }
	else{
    	$app_design_date="";
    	$app_design_btn_disabled="logdisplay";
        $ios_built_btn_disabled="logdisplay";
        $android_built_btn_disabled="logdisplay";
    }

    $ios_built_by        = stripslashes($getserviceRow["ios_built_by"]);
    $ios_built_date      = stripslashes($getserviceRow["ios_built_date"]);
    list($year,$month, $day) =explode("-",$ios_built_date);
    $ios_built_date     =$day."/".$month."/".$year ;

    if($ios_built_by=="" || $ios_built_date=="00/00/0000" || $ios_built_date=="" ){
        $ios_built_date="";
        $ios_tested_btn_disabled="logdisplay";
    }elseif($ios_built_by!="" && $ios_built_date!="00/00/0000"  && $user_type=="Admin"){
        $ios_built_btn_disabled="logdisplayshow";
        $ios_tested_btn_disabled="logdisplayshow";
    }
    elseif($ios_built_by!="" && $ios_built_date!="00/00/0000"  && $user_type=="Affiliate"){
        $ios_built_btn_disabled="logdisplay";
        $ios_tested_btn_disabled="logdisplayshow";
    }
	 elseif($ios_built_by!="" && $ios_built_date!="00/00/0000"  && $user_type=="DEM"){
        $ios_built_btn_disabled="logdisplay";
        $ios_tested_btn_disabled="logdisplayshow";
    }
    elseif($ios_built_by!="" && $ios_built_date!="00/00/0000"  && $user_type=="Developer"){
        $ios_built_btn_disabled="logdisplay";
        $ios_submitted_btn_disabled="logdisplayshow";
    } else{
        $ios_built_btn_disabled="logdisplay";
        $ios_tested_btn_disabled="logdisplay";
        $ios_submitted_btn_disabled="logdisplay";
    }

    $android_built_by    = stripslashes($getserviceRow["android_built_by"]);
    $android_built_date  = stripslashes($getserviceRow["android_built_date"]);
    list($year,$month, $day) =explode("-",$android_built_date);
    $android_built_date     =$day."/".$month."/".$year ;

    if($android_built_by=="" || $android_built_date=="00/00/0000" || $android_built_date=="" ){
        $android_built_date="";
        $android_tested_btn_disabled="logdisplay";
    }elseif($ios_built_by!="" && $ios_built_date!="00/00/0000"  && $user_type=="Admin"){
        $android_built_btn_disabled="logdisplayshow";
        $android_tested_btn_disabled="logdisplayshow";
    }
    elseif($ios_built_by!="" && $ios_built_date!="00/00/0000"  && $user_type=="Affiliate"){
        $android_built_btn_disabled="logdisplay";
        $android_tested_btn_disabled="logdisplayshow";
    }
	 elseif($ios_built_by!="" && $ios_built_date!="00/00/0000"  && $user_type=="DEM"){
        $android_built_btn_disabled="logdisplay";
        $android_tested_btn_disabled="logdisplayshow";
    }
    elseif($ios_built_by!="" && $ios_built_date!="00/00/0000"  && $user_type=="Developer"){
        $android_built_btn_disabled="logdisplay";
        $android_submitted_btn_disabled="logdisplayshow";
    }else{
        $android_built_btn_disabled="logdisplay";
        $android_tested_btn_disabled="logdisplay";
        $android_submitted_btn_disabled="logdisplay";
    }


    $devpayment_amount   = stripslashes($getserviceRow["devpayment_amount"]);
    $devpayment_date     = stripslashes($getserviceRow["devpayment_date"]);
    list($year,$month, $day) =explode("-",$devpayment_date);
    $devpayment_date     =$day."/".$month."/".$year ;
    $devpayment_made_by  = stripslashes($getserviceRow["devpayment_made_by"]);
   
    if($devpayment_amount=="" || $devpayment_date=="00/00/0000" || $devpayment_made_by==""){
        $devpayment_date="";
        $devpayment_disabled="logdisplayshow";
    } elseif($devpayment_amount!="" && $devpayment_date!="00/00/0000" && $user_type=="Admin") {
        $devpayment_disabled="logdisplayshow";
    }else{
        $devpayment_disabled="logdisplay"; 
    }

    $ios_tested_by       = stripslashes($getserviceRow["ios_tested_by"]);
    $ios_tested_date     = stripslashes($getserviceRow["ios_tested_date"]);
    list($year,$month, $day) =explode("-",$ios_tested_date);
    $ios_tested_date     =$day."/".$month."/".$year ;

    if($ios_tested_by=="" || $ios_tested_date=="00/00/0000" || $ios_tested_date==""){
        $ios_tested_date="";
        $ios_submitted_btn_disabled="logdisplay";
    } elseif($ios_tested_by!="" && $ios_tested_date!="00/00/0000" && $user_type=="Admin") {
        $ios_tested_btn_disabled="logdisplayshow";
        $ios_submitted_btn_disabled="logdisplayshow";
    }
    elseif($ios_tested_by!="" && $ios_tested_date!="00/00/0000" && $user_type=="Affiliate") {
        $ios_tested_btn_disabled="logdisplay";
    }
    elseif($ios_tested_by!="" && $ios_tested_date!="00/00/0000" && $user_type=="Developer") {
        $ios_tested_btn_disabled="logdisplay";
        $ios_submitted_btn_disabled="logdisplayshow";
    }
	elseif($ios_tested_by!="" && $ios_tested_date!="00/00/0000" && $user_type=="DEM") {
        $ios_tested_btn_disabled="logdisplay";
        $ios_submitted_btn_disabled="logdisplayshow";
    }
	else{
        $ios_tested_btn_disabled="logdisplay";
        $ios_submitted_btn_disabled="logdisplay";
    }

    $android_tested_by   = stripslashes($getserviceRow["android_tested_by"]);
    $android_tested_date = stripslashes($getserviceRow["android_tested_date"]);
    list($year,$month, $day) =explode("-",$android_tested_date);
    $android_tested_date     =$day."/".$month."/".$year ;
    if($android_tested_by=="" || $android_tested_date=="00/00/0000" || $android_tested_date==""){
        $android_tested_date="";
        $android_submitted_btn_disabled="logdisplay";
    } elseif($android_tested_by!="" && $android_tested_date!="00/00/0000" && $user_type=="Admin" ) {
        $android_tested_btn_disabled="logdisplayshow";
        $android_submitted_btn_disabled="logdisplayshow";
    }
    elseif($android_tested_by!="" && $android_tested_date!="00/00/0000" && $user_type=="Affiliate" ) {
        $android_tested_btn_disabled="logdisplay";
    }
    elseif($android_tested_by!="" && $android_tested_date!="00/00/0000" && $user_type=="Developer") {
        $android_tested_btn_disabled="logdisplay";
        $android_submitted_btn_disabled="logdisplayshow";
    }
	 elseif($android_tested_by!="" && $android_tested_date!="00/00/0000" && $user_type=="DEM") {
        $android_tested_btn_disabled="logdisplay";
        $android_submitted_btn_disabled="logdisplayshow";
    }
    else{
         $android_tested_btn_disabled="logdisplay";
        $android_submitted_btn_disabled="logdisplay";
    }

    $app_populated_by    = stripslashes($getserviceRow["app_populated_by"]);
    $app_populated_date  = stripslashes($getserviceRow["app_populated_date"]);
    list($year,$month, $day) =explode("-",$app_populated_date);
    $app_populated_date     =$day."/".$month."/".$year ;
    
    if($app_populated_by=="" || $app_populated_date=="00/00/0000" || $app_populated_date==""){
        $app_populated_date="";
        $app_populated_disabled="";
    } elseif($app_populated_by!="" && $app_populated_date!="00/00/0000" && $user_type=="Admin") {
        $app_populated_disabled="logdisplayshow";
    } else{
        $app_populated_disabled="logdisplay";
    }

    $ios_submitted_by    = stripslashes($getserviceRow["ios_submitted_by"]);
    $ios_submitted_date  = stripslashes($getserviceRow["ios_submitted_date"]);
    list($year,$month, $day) =explode("-",$ios_submitted_date);
    $ios_submitted_date     =$day."/".$month."/".$year ;
    
    if($ios_submitted_by=="" || $ios_submitted_date=="00/00/0000" || $ios_submitted_date==""){
        $ios_submitted_date="";
        $ios_live_disabled="logdisplay";
    } elseif($ios_submitted_by!="" && $ios_submitted_date!="00/00/0000" && $user_type=="Admin") {
        $ios_submitted_btn_disabled="logdisplayshow";
        $ios_live_disabled="logdisplayshow";
    } elseif($ios_submitted_by!="" && $ios_submitted_date!="00/00/0000" && $user_type=="Developer"){
        $ios_submitted_btn_disabled="logdisplay";
        $ios_live_disabled="logdisplayshow";
    }else{
        $ios_submitted_btn_disabled="logdisplay";
        $ios_live_disabled="logdisplay";
    }

    $android_submitted_by = stripslashes($getserviceRow["android_submitted_by"]);
    $android_submitted_date  = stripslashes($getserviceRow["android_submitted_date"]);
    list($year,$month, $day) =explode("-",$android_submitted_date);
    $android_submitted_date     =$day."/".$month."/".$year ;
    

    if($android_submitted_by=="" || $android_submitted_date=="00/00/0000" || $android_submitted_date==""){
        $android_submitted_date="";
        $android_live_disabled="logdisplay";
    } elseif($android_submitted_by!="" && $android_submitted_date!="00/00/0000" && $user_type=="Admin") {
        $android_submitted_btn_disabled="logdisplayshow";
        $android_live_disabled="logdisplayshow";
    }
    elseif($android_submitted_by!="" && $android_submitted_date!="00/00/0000" && $user_type=="Developer") {
        $android_submitted_btn_disabled="logdisplay";
        $android_live_disabled="logdisplayshow";
    }else{
        $android_submitted_btn_disabled="logdisplay";
        $android_live_disabled="logdisplay";
    }

    $dempayment_amount    = stripslashes($getserviceRow["dempayment_amount"]);
    $dempayment_date      = stripslashes($getserviceRow["dempayment_date"]);
    list($year,$month, $day) =explode("-",$dempayment_date);
    $dempayment_date     =$day."/".$month."/".$year ;
    $dempayment_made_by   = stripslashes($getserviceRow["dempayment_made_by"]);

    if($dempayment_amount=="" || $dempayment_date=="00/00/0000" || $dempayment_date==""){
        $dempayment_date="";
        $dempayment_disabled="";
    } elseif($dempayment_amount!="" && $dempayment_date!="00/00/0000" && $user_type=="Admin") {
        $dempayment_disabled="logdisplayshow";
    } else{
       $dempayment_disabled="logdisplay"; 
    }

    $ios_live_by          = stripslashes($getserviceRow["ios_live_by"]);
    $ios_live_date        = stripslashes($getserviceRow["ios_live_date"]);
    list($year,$month, $day) =explode("-",$ios_live_date);
    $ios_live_date     =$day."/".$month."/".$year ;
     
    if($ios_live_by=="" || $ios_live_date=="00/00/0000" || $ios_live_date==""){
        $ios_live_date="";
       // $ios_live_disabled="logdisplayshow";
    }elseif($ios_live_by!="" && $ios_live_by!="00/00/0000" && $user_type=="Admin") {
        $ios_live_disabled="logdisplayshow";
    }elseif($ios_live_by!="" && $ios_live_by!="00/00/0000" && $user_type=="Developer") {
        $ios_live_disabled="logdisplay";
    }else{
        $ios_live_disabled="logdisplay";
    }

    $android_live_by      = stripslashes($getserviceRow["android_live_by"]);
    $android_live_date    = stripslashes($getserviceRow["android_live_date"]);
    list($year,$month, $day) =explode("-",$android_live_date);
    $android_live_date     =$day."/".$month."/".$year ;

    if($android_live_by=="" || $android_live_date=="00/00/0000" || $android_live_date==""){
        $android_live_date="";
        //$android_live_disabled="logdisplayshow";
    } elseif($android_live_by!="" && $android_live_date!="00/00/0000" && $user_type=="Admin") {
        $android_live_disabled="logdisplayshow";
    }elseif($android_live_by!="" && $android_live_date!="00/00/0000" && $user_type=="Developer"){
         $android_live_disabled="logdisplay";
    }else{
        $android_live_disabled="logdisplay";
    }

    $appwelcome_call      = stripslashes($getserviceRow["appwelcome_call"]);
    $appwelcome_date      = stripslashes($getserviceRow["appwelcome_date"]);
    list($year,$month, $day) =explode("-",$appwelcome_date);
    $appwelcome_date     =$day."/".$month."/".$year ;
    if($appwelcome_call=="" || $appwelcome_date=="00/00/0000" || $appwelcome_date==""){
        $appwelcome_date="";
        $welcome_btn_disabled="";
    } elseif($appwelcome_call!="" && $appwelcome_date!="00/00/0000" && $user_type=="Admin") {
        $welcome_btn_disabled="logdisplayshow";
    }else{
       $welcome_btn_disabled="logdisplay"; 
    }
    $apptraining          = stripslashes($getserviceRow["apptraining"]);
    $apptraining_date     = stripslashes($getserviceRow["apptraining_date"]);
    list($year,$month, $day) =explode("-",$apptraining_date);
    $apptraining_date     =$day."/".$month."/".$year ;
    if($apptraining=="" || $apptraining_date=="00/00/0000" || $apptraining_date==""){
        $apptraining_date="";
        $training_disabled="";
    }elseif($apptraining!="" && $apptraining_date!="00/00/0000" && $user_type=="Admin") {
        $training_disabled="logdisplayshow";
    } 
    else {
        $training_disabled="logdisplay";
    }
    $cmspayment_amount    = stripslashes($getserviceRow["cmspayment_amount"]);
    $cmspayment_date      = stripslashes($getserviceRow["cmspayment_date"]);
    list($year,$month, $day) =explode("-",$cmspayment_date);
    $cmspayment_date     =$day."/".$month."/".$year ;
    $cmspayment_made_by   = stripslashes($getserviceRow["cmspayment_made_by"]);
    if($cmspayment_amount=="" || $cmspayment_date=="00/00/0000" || $cmspayment_date=="" || $cmspayment_made_by=="" ){
        $cmspayment_date="";
        $cmspayment_disabled="";
    } elseif($cmspayment_amount!="" && $cmspayment_date!="00/00/0000" && $cmspayment_made_by!="" && $user_type=="Admin") {
        $cmspayment_disabled="logdisplayshow";
    } else{
        $cmspayment_disabled="logdisplay";
    }
    $market_made_by       = stripslashes($getserviceRow["market_made_by"]);
    $market_date          = stripslashes($getserviceRow["market_date"]);
    list($year,$month, $day) =explode("-",$market_date);
    $market_date      =$day."/".$month."/".$year ;

    if($market_date=="00/00/0000" || $market_date=="" || $market_date=="//"){
        $market_date="";
    } 


    $support_made_by      = stripslashes($getserviceRow["support_made_by"]);
    $support_date         = stripslashes($getserviceRow["support_date"]);
    list($year,$month, $day) =explode("-",$support_date);
    $support_date     =$day."/".$month."/".$year ;
    if($support_date=="00/00/0000" || $support_date=="" || $support_date=="//"){
        $support_date="";
    }
?>