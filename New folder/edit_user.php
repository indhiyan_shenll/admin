<?php
include("../includes/configure.php");
include("../includes/session_check.php");
$dbdatetime=date('Y-m-d H:i:s',strtotime('now'));
$user_id=$_GET["user_id"];
if($user_id!=""){
	//$user_id
	$geUserQry="select * from  tbl_users where user_id=:user_id";
	$prepgetuserqry=$DBCONN->prepare($geUserQry);
	$prepgetuserqry->execute(array(":user_id"=>$user_id));
	//$geUserRes=mysql_query($geUserQry);
	$geUserRow=$prepgetuserqry->fetch();
	$username=stripslashes($geUserRow["username"]);
	$password=stripslashes($geUserRow["password"]);
	$email=stripslashes($geUserRow["email"]);
	$user_type=stripslashes($geUserRow["user_type"]);
	$trail_form=stripslashes($geUserRow["trail_form"]);
	$demo_page=stripslashes($geUserRow["demo_page"]);
	$master_list=stripslashes($geUserRow["master_list"]);
	$download_list=stripslashes($geUserRow["download_list"]);
	$payment_info=stripslashes($geUserRow["payment_info"]);
	$licensee_list=stripslashes($geUserRow["licensee_list"]);
	$alliance_list=stripslashes($geUserRow["alliance_list"]);
	$tbc=stripslashes($geUserRow["tbc"]);
	$mode="Edit";
	$value="Update";  
}
else{
	$mode="Add";
	$value="Create";
}
if($user_type==""){
	$user_type=0;
}

if(isset($_POST["username"])){  
	$username=addslashes(trim($_POST["username"]));
	$password=addslashes(trim($_POST["password"]));
	$user_email=addslashes(trim($_POST["user_email"]));
	$usertype=addslashes(trim($_POST["usertype"]));
	$trial_form=addslashes(trim($_POST["trial_form"]));
	$demo_page=addslashes(trim($_POST["demo_page"]));
	$master_list=addslashes(trim($_POST["master_list"]));
	$download_list=addslashes(trim($_POST["download_list"]));
	$payment_info=addslashes(trim($_POST["payment_info"]));
	$licensee_list=addslashes(trim($_POST["licensee_list"]));
	$alliance_list=addslashes(trim($_POST["alliance_info"]));
	$tbc=addslashes(trim($_POST["tbc"]));
	if($mode=="Edit"){
		$updateQry="update  tbl_users set username=:username,password=:password,email=:email,user_type=:user_type,trail_form=:trail_form,demo_page=:demo_page,master_list=:master_list,download_list=:download_list,payment_info=:payment_info,licensee_list=:licensee_list,alliance_list=:alliance_list,tbc=:tbc,modified_date=:modified_date where user_id=:user_id";
		$prepupdateQry=$DBCONN->prepare($updateQry);
		$updateRes=$prepupdateQry->execute(array(":username"=>$username,":password"=>$password,":email"=>$user_email,":user_type"=>$usertype,":trail_form"=>$trial_form,":demo_page"=>$demo_page,":master_list"=>$master_list,":download_list"=>$download_list,":payment_info"=>$payment_info,":licensee_list"=>$licensee_list,":alliance_list"=>$alliance_list,":tbc"=>$tbc,":modified_date"=>$dbdatetime,":user_id"=>$user_id));
		if($updateRes){
            header("Location:userslist.php");
			exit;
		}
	}
	else{
		$prepareinsqry=$DBCONN->prepare("insert into tbl_users(username,password,email,user_type,trail_form,demo_page,master_list,download_list,payment_info,added_date,modified_date,licensee_list,alliance_list,tbc) values(:username,:password,:email,:user_type,:trail_form,:demo_page,:master_list,:download_list,:payment_info,:added_date,:modified_date,:licensee_list,:alliance_list,:tbc)");
		$insertRes=$prepareinsqry->execute(array(":username"=>$username,":password"=>$password,":email"=>$user_email,":user_type"=>$usertype,":trail_form"=>$trial_form,":demo_page"=>$demo_page,":master_list"=>$master_list,":download_list"=>$download_list,":payment_info"=>$payment_info,":added_date"=>$dbdatetime,":modified_date"=>$dbdatetime,":licensee_list"=>$licensee_list,":alliance_list"=>$alliance_list,":tbc"=>$tbc));
		if($insertRes){
			$user_id=$DBCONN->lastInsertId();
			$logged_user_id=$_SESSION['user_id'];
			$joined_date=date('Y-m-d');
			if($usertype=="Licensee" || $usertype=="Affiliate" || $usertype=="Developer" || $usertype=="Admin"){
				/*== User level Licensee====*/

				 if($usertype=="Licensee"){

					     /*== Add the Record in tbl_licensee ====*/

						$insert_results=array(":joined_date"=>$joined_date,":full_name"=>$username,":email"=>$user_email, ":l_user_id"=>$user_id,":logged_user_id"=>$logged_user_id,":created_date"=>$dbdatetime,":modified_date"=>$dbdatetime);	
						$prep_insertqry=$DBCONN->prepare("insert into tbl_licensee(joined_date,full_name,email,l_user_id,logged_user_id,created_date,modified_date) values(:joined_date,:full_name,:email,:l_user_id,:logged_user_id,:created_date,:modified_date)");
						$insert_Res=$prep_insertqry->execute($insert_results);
						$last_insert_licence_id=$DBCONN->lastInsertId();

						  /*== Add the Record in tbl_affiliate ====*/

						$insert_results_affiliate=array(":joined_date"=>$joined_date,":full_name"=>$username,":email"=>$user_email,":logged_user_id"=>$logged_user_id,":l_id"=>$last_insert_licence_id,":l_user_id"=>$user_id,":licensee"=>$last_insert_licence_id,":created_date"=>$dbdatetime,":modified_date"=>$dbdatetime,":a_user_id"=>$user_id);	
						$prep_insert_affiliate_qry=$DBCONN->prepare("insert into tbl_affiliate(joined_date,full_name,email,logged_user_id,l_id,l_user_id,licensee,created_date,modified_date,a_user_id) values(:joined_date,:full_name,:email,:logged_user_id,:l_id,:l_user_id,:licensee,:created_date,:modified_date,:a_user_id)");
						$insert_Res1=$prep_insert_affiliate_qry->execute($insert_results_affiliate); 


                }

				/*== User level Affiliate Or Admin ====*/

				else if($usertype=="Affiliate" || $usertype=="Admin"){

					  /*== Add the Record in tbl_affiliate ====*/

					 $insert_results=array(":joined_date"=>$joined_date,":full_name"=>$username,":email"=>$user_email,":logged_user_id"=>$logged_user_id,":created_date"=>$dbdatetime,":modified_date"=>$dbdatetime,":a_user_id"=>$user_id);	
                     $prep_insertqry=$DBCONN->prepare("insert into tbl_affiliate(joined_date,full_name,email,logged_user_id,created_date,modified_date,a_user_id) values(:joined_date,:full_name,:email,:logged_user_id,:created_date,:modified_date,:a_user_id)");
		             $insert_Res=$prep_insertqry->execute($insert_results);

				 }
				 /*== User level Developer ====*/
				 else if($usertype=="Developer"){

					   /*== Add the Record in tbl_developers ====*/

						$insertQry="insert into tbl_developers(developer_name,developer_email,added_date,modified_date) values(:developer_name,:developer_email,:added_date,:modified_date)";
						$prepinsertQry=$DBCONN->prepare($insertQry);
						$insertRes=$prepinsertQry->execute(array(":developer_name"=>$username,":developer_email"=>$user_email,":added_date"=>$dbdatetime,
						":modified_date"=>$dbdatetime));

				 }
			}
			header("Location:userslist.php");
			exit;
		}
	}
}
include("includes/header.php");
?>
 
	<body>
		<div>
			<div style="margin-left:auto;margin-right:auto;">
				 
				<div class="content">
					<div class="list_content">
						<h1 style="font-size:25px;padding-top:15px;padding-bottom:15px;margin:0px;">New User</h1>
						<form name="user_form" id="user_form" method="post">
						<input type="hidden" name="hdn_userid" id="hdn_userid" value="<?php echo $user_id;?>">
						<table cellspacing="15" cellpadding="0" border="0" width="70%">
							<tr>
								<td style="width:138px;">
									Username<font color="red">*</font>: 
								</td>
								<td>
									<input type="text" name="username" id="username" class="inp_feild" value="<?php echo $username;?>">
								</td>
							</tr>
							<tr>
								<td style="width:138px;">
									Password<font color="red">*</font>: 
								</td>
								<td>
									<input type="text" name="password" id="password" class="inp_feild" value="<?php echo $password;?>">
								</td>
							</tr>
							<tr>
								<td style="width:138px;">
									Email<font color="red">*</font>: 
								</td>
								<td>
									<input type="text" name="user_email" id="user_email" class="inp_feild" value="<?php echo $email;?>">
								</td>
							</tr>
							<tr>
								<td style="width:138px;">
									User Type<font color="red">*</font>: 
								</td>
								<td>
									<select name="usertype" id="usertype" class="inp_feild">
										<option value="0">Select</option>
										<option value="User">User</option>
										<option value="Admin">Admin</option>
										<option value="Developer">Developer</option>
										<option value="Licensee">Licensee</option>
										<option value="Affiliate">Affiliate</option>
										<option value="Agent">Agent</option>
										<option value="DEM">Data-entry Manager</option>
										<option value="CSM">Customer Service Manager</option>
									</select>
									<script>
									$("#usertype").val("<?php echo $user_type;?>");
									</script>
								</td>
							</tr>
							<tr>
								<td style="width:138px;" valign="top">
									User Privileges: 
								</td>
								<td>
									<input type="checkbox" name="trial_form" id="trial_form" value="yes"<?php if($trail_form=="yes") echo " checked";?>>Prospect/Trial<br>
									<input type="checkbox" name="demo_page" id="demo_page" value="yes"<?php if($demo_page=="yes") echo " checked";?>>Demo Page<br>
									<input type="checkbox" name="master_list" id="master_list" value="yes"<?php if($master_list=="yes") echo " checked";?>>Master List<br>
									<input type="checkbox" name="download_list" id="download_list" value="yes"<?php if($download_list=="yes") echo " checked";?>>Download Lists<br>
									<input type="checkbox" name="payment_info" id="payment_info" value="yes"<?php if($payment_info=="yes") echo " checked";?>>Payment Info<br>
									<input type="checkbox" name="licensee_list" id="licensee_list" value="yes"<?php if($licensee_list=="yes") echo " checked";?>>Licensee List<br>
									<!-- <input type="checkbox" name="alliance_info" id="alliance_info" value="yes"<?php if($alliance_list=="yes") echo " checked";?>>Simula tor list<br> -->
									 <!-- <input type="checkbox" name="tbc" id="tbc" value="yes"<?php if($tbc=="yes") echo " checked";?>>TBC<br> -->
								</td> 
							</tr>
							<tr>
							     <td>
									<div class="form_actions" style="text-align:left;">
										<input type="button" value="Back To Users List" class="add_btn"  onclick="document.location='userslist.php'">
									
								</td>
								<td>
									  <div class="form_actions" style="text-align:right;">
										<input type="button" value="<?php echo $value;?> User" class="add_btn" id="add_user">
									</div>
								</td>
							</tr>
						</table>
						</form>
					</div>
					
				</div>
			</div>
		</div>
	<?php
	include("includes/footer.php");
	?>