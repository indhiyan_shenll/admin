<?php
include("../includes/configure.php");
include("../includes/session_check.php");
$versionid=$_GET["version_id"];
$sort=$_GET["sort"];
$field=$_GET["field"];
if($sort==""){
	$sort="asc";
}
if($field==""){
	$field="version_name";
}
if($field=="sno"){
	$fieldname="version_name";
	$order=" order by ".$fieldname." ".$sort;
	if($sort=="asc"){
		$dsort="desc";
		$dpath="images/up.png";
	}
	else{
		$dsort="asc";
		$dpath="images/down.png";
	}
}
if($field=="version_description"){
	$fieldname="version_description";
	$order=" order by ".$fieldname." ".$sort;
	if($sort=="asc"){
		$rsort="desc";
		$rpath="images/up.png";
	}
	else{
		$rsort="asc";
		$rpath="images/down.png";
	}
}
if($versionid!=""){
	$deleteQry="delete from  tbl_version where version_id='".$versionid."'";
	$deleteRes=mysql_query($deleteQry);
	if($deleteRes){
		header("Location:versionlist.php");
		exit;
	}
}
include("includes/header.php");
?>
 <body>
		<div>
			<div style="margin-left:auto;margin-right:auto;">
				 
				<div class="content">
					<div class="list_content">
						<div class="form_actions" style="padding-bottom:45px;">
							<input type="button" value="Back To Admin Features" class="add_btn" onclick="document.location='admin_features.php'" style="float:left;">
							<input type="button" value="Add App Version" class="add_btn" onclick="document.location='add_version.php'" style="float:right;">
						</div>
						<div class="header_div">
							<table cellspacing="0" cellpadding="0" width="100%" class="tbl_header">
								
								<tr>
									<th width="10%" >App Version</th>
									<th width="80%" onclick="document.location='versionlist.php?sort=<?php echo $rsort;?>&field=version_description'" style="cursor:pointer">
									Version Description&nbsp;&nbsp;<?php if($rpath!=""){?><img src="<?php echo $rpath;?>" style="width:10px;height:10px;"><?php }?></th>
									<th width="10%">Delete?</th>
								</tr>
							</table>
						</div>
						<div class="gap"></div>
						<table cellspacing="0" cellpadding="0" width="100%" class="tbl-body">
						<?php
							$getituneQry="select * from  tbl_version".$order;
							//exit;
							$getituneRes=mysql_query($getituneQry);
							$getituneCnt=mysql_num_rows($getituneRes);
							if($getituneCnt>0){
								$i=1;
								while($getituneRow=mysql_fetch_array($getituneRes)){
									
									if($i%2==1){
										$bgcolor="#a5a5a5";
									}
									else{
										$bgcolor="#d2d1d1";
									}
						?>
							<tr bgcolor="<?php echo $bgcolor;?>">
								<td width="10%"><?php echo stripslashes($getituneRow["version_name"]);?></td>
								<td width="80%"><?php echo stripslashes($getituneRow["version_description"]);?></td>
								<td width="10%"><a href="add_version.php?version_id=<?php echo $getituneRow["version_id"];?>">Edit</a>&nbsp;&nbsp;/&nbsp;&nbsp;<a href="versionlist.php?version_id=<?php echo $getituneRow["version_id"];?>" onclick="return confirm('Are you sure want to delete this app version?')">Delete</a></td>
							</tr>
							
						<?php
							$i++;
								}
								?>
							<tr><td height="10px"></td></tr>
							<tr>
								<td colspan="3">
								<div class="form_actions" style="text-align:left;position:relative;">
								<input type="button" value="Back To Admin Features" class="add_btn" onclick="document.location='admin_features.php'">
								</td>
							</tr>
								<?php
							}
							else{
								echo "<tr bgcolor='#a5a5a5'><td colspan=\"3\"><center>No App Version(s) found.</center></td></tr>";
							}
						?>
						</table>
					</div>
				</div>
			</div>
		</div>
	<?php
include("includes/footer.php");
?>