<?php
include("../includes/configure.php");
include("../includes/session_check.php");
$dbdatetime=date('Y-m-d H:i:s',strtotime('now'));
$callstatus_id=$_GET["callstatus_id"];
if($callstatus_id!=""){
	//$callstatus_id
	$getQry="select * from tbl_callstatus where callstatus_id=:callstatus_id";
	$prepgetQry=$DBCONN->prepare($getQry);
	$prepgetQry->execute(array(":callstatus_id"=>$callstatus_id));
	//$getRes=mysql_query($getQry);
	$getRow=$prepgetQry->fetch();
	$callstatus=stripslashes($getRow["callstatus"]);
	$Callstatus_color=stripslashes($getRow["status_color"]);
	$mode="Edit";
	$value="Update";
}
else{
	$mode="Add";
	$value="Create";
}
if(isset($_POST["callstatus"])){
	$callstatus=addslashes(trim($_POST["callstatus"]));
	$call_satuscolor=addslashes(trim($_POST["call_stauts_color"]));
	if($mode=="Edit"){
		//$callstatus   $call_satuscolor    $callstatus_id
		$updateQry="update tbl_callstatus set callstatus=:callstatus,status_color=:status_color,modified_date=:modified_date where callstatus_id=:callstatus_id";
		$prepupdateQry=$DBCONN->prepare($updateQry);
		$updateRes=$prepupdateQry->execute(array(":callstatus"=>$callstatus,":status_color"=>$call_satuscolor,":modified_date"=>$dbdatetime,":callstatus_id"=>$callstatus_id));

		//$updateRes=mysql_query($updateQry);
		if($updateRes){
			header("Location:callstatuslist.php");
			exit;
		}
	}
	else{
		/* $sqlqry="select max(display_order) as display_order from  tbl_callstatus";
		$sql=mysql_query($sqlqry);
		if(mysql_num_rows($sql)>0)
		{
			$fetch=mysql_fetch_array($sql);
			$display_order=$fetch["display_order"];
		}*/
		$get_order="select max(display_order) as display_order from  tbl_callstatus";
		$prepget_get_order=$DBCONN->prepare($get_order);
		$prepget_get_order->execute();
		$order_count =$prepget_get_order->rowCount();
		$get_display_Row=$prepget_get_order->fetch();
		$display_order=$get_display_Row['display_order'];
		$order_db_insert=$display_order+1;
		//$callstatus  $call_satuscolor $display_order
		$insertQry="insert into tbl_callstatus(callstatus,status_color,display_order,added_date,modified_date) values(:callstatus,:status_color,:display_order,:added_date,:modified_date)";
		$prepisnertQry=$DBCONN->prepare($insertQry);
		$insertRes=$prepisnertQry->execute(array(":callstatus"=>$callstatus,":status_color"=>$call_satuscolor,":display_order"=>$order_db_insert,":added_date"=>$dbdatetime,":modified_date"=>$dbdatetime));
		//$insertRes=mysql_query($insertQry);
		if($insertRes){
			header("Location:callstatuslist.php");
			exit;
		}
	}
}
include("includes/header.php");
?>
 <body>
		<div>
			<div style="margin-left:auto;margin-right:auto;">
				 
				<div class="content">
					<div class="list_content">
						<h1 style="font-size:25px;padding-top:15px;padding-bottom:15px;margin:0px;">New CallStatus</h1>
						<form name="callstatus_form" id="callstatus_form" method="post">
						<input type="hidden" name="callstatus_form_id" id="callstatus_form_id" value="<?php echo $callstatus_id;?>">
						<table cellspacing="15" cellpadding="0" border="0" width="70%">
							<tr>
								<td style="width:138px;">
									 Callstatus<font color="red">*</font>:
								</td>
								<td>
									<input type="text" name="callstatus" id="callstatus" class="inp_feild" value="<?php echo $callstatus;?>">
								</td>
							</tr>
							<tr>
								<td style="width:138px;">
									 Callstatus color<font color="red">*</font>: 
								</td>
								<td>
									<input type="text" name="call_stauts_color" id="call_stauts_color" class="inp_feild" value="<?php echo $Callstatus_color;?>">
									<SCRIPT LANGUAGE="JavaScript">
									$('#call_stauts_color').ColorPicker({
									eventName:'focus',
									 onSubmit: function(hsb, hex, rgb, el) {
									  $(el).val(hex);
									  $(el).ColorPickerHide();
									 },
									  onBeforeShow: function () {
									   $(this).ColorPickerSetColor(this.value);
									  }
									 })
									 .bind('keyup', function(){
									  $(this).ColorPickerSetColor(this.value);
									 });
									</SCRIPT>
								</td>
							</tr>
							<tr>
							     <td>
									<div class="form_actions" style="text-align:left;">
										<input type="button" value="Back to Call Status List" class="add_btn"  onclick="document.location='callstatuslist.php'">
									
								</td>
								<td>
									  <div class="form_actions" style="text-align:right;">
										<input type="button" value="<?php echo $value;?> Call Status" class="add_btn" id="add_callstatus">
									</div>
								</td>
							</tr>
						</table>
						</form>
					</div>
					
				</div>
			</div>
		</div>
	<?php
include("includes/footer.php");
?>