<?php
$file_name=basename($_SERVER["PHP_SELF"]);
function readmore_view($string,$length){
     $string = strip_tags($string);
	 if (strlen($string) > $length) {
		$stringCut = substr($string, 0, $length);
		 $string =$stringCut.'...';
	 }
   return $string;
}
$Current_PHP_File = basename($_SERVER['SCRIPT_NAME']);

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<title>Salon Emotion Consulting Tool</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="shortcut icon" href="../images/Fav.ico" type="image/ico">
	<link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/bootstrap-datepicker.min.css" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>    
    <script type="text/javascript" src="js/bootstrap-datepicker.min.js"></script>
    <link rel="stylesheet" type="text/css" href="css/loreal_style.css" />

	<meta name="Generator" content="EditPlus">
	<meta name="Author" content="">
	<meta name="Keywords" content="">
	<meta name="Description" content="">
</head>

<body>

    <div class="col-md-12 col-sm-12 col-xs-12 main_header">
        <div class="container">
            <!-- <div class="row"> -->
                <div class="col-md-4 col-sm-4 col-xs-4 col-lg-4">
            	    <h1 class="headerlogotxt">L'Oréal</h1>
            	</div>
            	<div class="col-md-8 col-sm-8 col-xs-8 col-lg-8">
				<?php
				if($Current_PHP_File!="login.php" || $Current_PHP_File!="forgotpassword.php"){
					?>
							<p class="session_name"><strong><?php echo readmore_view($_SESSION['user_name'],"20");?></strong></p>
							<p class="logout" ><a href="logout.php" class="logout" >Logout</a></p>
					<?php
				}
				?>
            	</div>
            <!-- </div> -->
        </div>
    </div>