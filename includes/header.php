<?php
# ========================================================================#
#  Author:    Shenll software solutions.
#  Version:	  1.0
#  Date:      19-Jan-15
# ========================================================================#
$file_name=basename($_SERVER["PHP_SELF"]);
function readmore_view($string,$length){
     $string = strip_tags($string);
	 if (strlen($string) > $length) {
		// truncate string
		$stringCut = substr($string, 0, $length);
		 $string =$stringCut.'...';
	   // make sure it ends in a word so assassinate doesn't become ass...
		//$string = substr($stringCut, 0, strrpos($stringCut, ' ')).'...'; 
	}
   return $string;
}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
 <html>
	<head>
	    <!-- =========================
		     TITLE OF SITE 
		 ============================== -->
        <title>MAB Sales CRM</title>
        <!-- =========================
		      FAV AND TOUCH ICONS 
		 ============================== -->
		<link rel="shortcut icon" href="images/Fav.ico" type="image/ico">
		<meta name="Generator" content="EditPlus">
		<meta name="Author" content="">
		<meta name="Keywords" content="">
		<meta name="Description" content="">
		<meta charset="UTF-8">
		 <!-- =========================
		STYLESHEETS 
		============================== -->
		<link rel="stylesheet" media="screen" type="text/css" href="css/colorpicker.css" />
		
		<link href="css/style_v1.css" rel="stylesheet"> 
		<link href="css/ui-lightness/jquery-ui-1.8.21.custom.css" rel="stylesheet">
		<!-- =========================
		JAVASCRIPTS
		============================== -->
		<script src="js/jquery-1.7.2.min.js"></script>
		<script src="js/jquery-ui-1.8.21.custom.min.js"></script>
		<script>var mode="<?php echo $mode;?>";</script>
		<link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css" rel="stylesheet" type="text/css"/>
		<link href="css/jquery.datetimepicker.css" rel="stylesheet" type="text/css"/> 
		<script type="text/javascript" src="js/jquery.datetimepicker.js"></script> 
		<script type="text/javascript" src="js/colorpicker.js"></script>
		<script type="text/javascript" src="js/custom_v1.js"></script>
		<script type="text/javascript" src="js/validate_v1.js"></script>
		<script type="text/javascript" src="js/jquery.fancybox.js"></script>
	    <link rel="stylesheet" type="text/css" href="css/jquery.fancybox.css" media="screen" />

	    <!-- Add fancyBox main JS and CSS files -->
	    
	 
		<?php
        $file_title="";
		if($file_name=="prospectinglist-v1.php" || $file_name=="prospectinglis.php"){
		  $file_title="Prospect List";
		}
		else if($file_name=="prospectdetails.php" || $file_name=="prospectdetails-v1.php"){
		  $file_title="Prospect Details";
		}
		else if($file_name=="newdemo.php" || $file_name=="newdemo-v1.php"){
		  $file_title="Master Details";
		}
		else if($file_name=="masterlist.php" || $file_name=="masterlist-v1.php"){
		  $file_title="Master List";
		}
		else if($file_name=="licensee_list.php" || $file_name=="Licensee_list.php"){
		  $file_title="Licensee List";
		}
		else if($file_name=="affiliate_list.php" || $file_name=="affiliate_list.php"){
		  $file_title="Affiliate List";
		}
		else if($file_name=="trials_list.php" || $file_name=="Trials_list.php"){
		  $file_title="FREE TRIALS LIST";
		}

		?>
	</head>
<div class="header" style="border-bottom: 1px solid white;">
<table cellspacing = "0" cellpadding = "0" border = "0" width='100%'>
<tr>
	<td>
		<img src="http://myappyrestaurant.com/images/applogo1.png">
	</td>	
	<td align="right" valign="top" width="200px" style="padding-right:50px;">
		<table  cellspacing = "0" cellpadding = "0" border = "0">			
			<tr><td >
				<div class="form_actions">
					<a href="https://asia.login.secureserver.net/index.php?'" style="border:0px;text-decoration: none;" target='_blank'><input type="button" value="Launch Webmail" class="add_btn"   ></a>
				</div>
			</td></tr>
			<?php
				if(($user_type=="Admin" || $_SESSION['user_type']=="Admin") && $file_name!="admin_features.php" ){
				?>
			<tr><td>
				<div class="headerbutton">
					<input type="button" value="Admin Features" class="add_btn"  onclick="document.location='admin_features.php'">
				</div>
			</td></tr>
			<?php } ?>
		</table>
	</td>
	<?php
	// desig for affiliate		
				
	if(($user_type=="Affiliate" || $user_type=="Licensee") && $user_type!='0'){
	?>
	<td width="275px">
		 <!-- <div style="float:right;margin-top:20px;width:20%;"><font style="font-family:arial;color:white;size:40px;" size="5px;"><?php echo $file_title;?></font></div> -->
		<div class="form_actions">

			 <?php
				$getQry_deatils="select * from tbl_affiliate where a_user_id='".$_SESSION['user_id']."'";
				$getQry_details_rs=$DBCONN->prepare($getQry_deatils);
				$getQry_details_rs->execute();
				$count=$getQry_details_rs->rowCount();
				if($count>0){
					$get_row_res=$getQry_details_rs->fetch();
					 
					?>
					<table width="100%" cellspacing="1" style="color:white;font-size:12px;layout:fixed;">
					<tr>
						<td align="left" style="width:70%"><?php echo readmore_view(stripslashes($get_row_res["package_des_1"]),'40');?></td>
						<td align="left" style="width:10%;"><?php echo readmore_view(stripslashes($get_row_res["promo_code_1"]),'10');?></td>
						<td align="left" style="width:20%;"><?php if($get_row_res["product_page1"]!="" && $get_row_res["product_page1"]!="0"){ echo readmore_view("(".stripslashes($get_row_res["product_page1"]).")","5"); }?></td>
					</tr>
					<tr>
						<td align="left"><?php echo  readmore_view(stripslashes($get_row_res["package_des_2"]),'40');?></td>
						<td align="left"><?php echo readmore_view(stripslashes($get_row_res["promo_code_2"]),'10');?></td>
						<td align="left"><?php if($get_row_res["product_page2"]!="" && $get_row_res["product_page2"]!="0"){ echo readmore_view("(".stripslashes($get_row_res["product_page2"]).")","5"); }?></td>
					</tr>
					<tr>
						<td align="left"><?php echo readmore_view(stripslashes($get_row_res["package_des_3"]),'40');?></td>
						<td align="left"><?php echo  readmore_view(stripslashes($get_row_res["promo_code_3"]),'10');?></td>
						<td align="left"><?php if($get_row_res["product_page3"]!="" && $get_row_res["product_page2"]!="0"){ echo readmore_view("(".stripslashes($get_row_res["product_page3"]).")","5"); }?></td>
					</tr>
					</table>
				<?php
				}
			 ?>
	</td>
	<?php
		}
	 ?>
	<td align="right" valign="top" width="175px" style="padding-right:10px;padding-top:18px;">
		<table  cellspacing = "0" cellpadding = "0" border = "0">
		<tr><td align="right" valign="top" style="color:#CF300A;font-size:23px;padding-top:10px;"><?php echo readmore_view($_SESSION['user_name'],"20");?></td></tr>
		<tr><td align="right" valign="top"><a href="logout.php" style="color:white;text-decoration:none;font-size: 12px;">Logout</a></td></tr>
		</table>
	</td>
</tr>
</table>		
</div> <!-- Header DIV ends here --> 