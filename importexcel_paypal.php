<?php
error_reporting(E_ALL);
include("../includes/configure.php");
error_reporting(E_ALL);
include("includes/PHPExcel.php");
$dbdatetime=date('Y-m-d H:i:s',strtotime('now'));
function fundateconversion($date,$purpose)
{
	if($purpose=="db")
	{
		list($month,$day,$year)=explode("/",$date);
		$returndate=$year."-".$month."-".$day;
	}
	else if($purpose=="display")
	{
		list($year,$month,$day)=explode("-",$date);
		$returndate=$month."-".$day."-".$year;
	}
	return $returndate;
}
function convertion($date)
{
	if(strpos($date,"AM") !== false || strpos($date,"PM") !== false){
		$time_stamp=$date;
	}
	else{
		$time_stamp=$date." AM";
	}
	$time_stamp=str_replace('/', '-', $time_stamp);
	$report_time=date('Y-m-d H:i:s',strtotime($time_stamp));  
	 
	return $report_time;
}

if($_POST["hdn_value"]){
	$sql_qry_drop="DROP TABLE `tbl_report_paypal`, `tbl_temp_paypal_report`";
    $prep_drop_Qry=$DBCONN->prepare($sql_qry_drop);
	$drop_res=$prep_drop_Qry->execute();
    $create_table_qry="CREATE TABLE IF NOT EXISTS `tbl_temp_paypal_report`(`report_id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY, transaction_id varchar(300) DEFAULT NULL, aba_routing_number varchar(300) DEFAULT NULL, account_number int DEFAULT NULL, currency_symbol varchar(300) DEFAULT NULL, amount int DEFAULT NULL, authcode varchar(300) DEFAULT NULL, batch_id varchar(300) DEFAULT NULL, billing_address varchar(300) DEFAULT NULL, billing_address_2 varchar(300) DEFAULT NULL, billing_city varchar(300) DEFAULT NULL, billing_company_name varchar(300) DEFAULT NULL, billing_country varchar(300) DEFAULT NULL, billing_email varchar(300) DEFAULT NULL, billing_first_name varchar(300) DEFAULT NULL, billing_last_name varchar(300) DEFAULT NULL, billing_state varchar(300) DEFAULT NULL, billing_zip varchar(300) DEFAULT NULL, csc_match varchar(300) DEFAULT NULL, comment1 varchar(300) DEFAULT NULL, comment2 varchar(300) DEFAULT NULL, customer_code varchar(300) DEFAULT NULL, duty_amount varchar(300) DEFAULT NULL, expires date DEFAULT NULL, freight_amount varchar(300) DEFAULT NULL, invoice_number varchar(300) DEFAULT NULL, original_transaction_id varchar(300) DEFAULT NULL, paypal_email_id varchar(300) DEFAULT NULL, paypal_fees varchar(300) DEFAULT NULL, paypal_transaction_id varchar(300) DEFAULT NULL, payment_advice_code varchar(300) DEFAULT NULL, pending_reason varchar(300) DEFAULT NULL, purchase_order varchar(300) DEFAULT NULL, response_msg varchar(300) DEFAULT NULL, result_code varchar(300) DEFAULT NULL, settled_date datetime DEFAULT NULL, shipping_address varchar(300) DEFAULT NULL, shipping_address_2 varchar(300) DEFAULT NULL, shipping_city varchar(300) DEFAULT NULL, shipping_country varchar(300) DEFAULT NULL, shipping_email varchar(300) DEFAULT NULL, shipping_first_name varchar(300) DEFAULT NULL, shipping_last_name varchar(300) DEFAULT NULL, shipping_state varchar(300) DEFAULT NULL, shipping_zip varchar(300) DEFAULT NULL, tax_amount varchar(300) DEFAULT NULL, tender_type varchar(300) DEFAULT NULL,time datetime DEFAULT NULL,transaction_state varchar(300) DEFAULT NULL,type varchar(300) DEFAULT NULL,user varchar(300) DEFAULT NULL,created_date datetime DEFAULT NULL,modified_date datetime DEFAULT NULL)";
	$prep_Qry=$DBCONN->prepare($create_table_qry);
	$Res=$prep_Qry->execute();
	/*create table */
	$create_table_qry1="CREATE TABLE IF NOT EXISTS `tbl_report_paypal`(`report_id` int(11) NOT NULL AUTO_INCREMENT, `transaction_id` varchar(250) DEFAULT NULL, `transaction_date` date DEFAULT NULL, `customer_id` varchar(500) DEFAULT NULL, `amount` int(11) DEFAULT NULL, `currency` varchar(250) DEFAULT NULL, `affiliate_id` int(11) DEFAULT NULL, `licensee_id` int(11) DEFAULT NULL, `payment_status` varchar(500) DEFAULT NULL, `created_date` datetime DEFAULT NULL, `modified_date` datetime DEFAULT NULL, PRIMARY KEY (`report_id`)) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1";
	$prep_Qry1=$DBCONN->prepare($create_table_qry1);
	$Res=$prep_Qry1->execute();
	$filename=pathinfo($_FILES["report_file"]["name"], PATHINFO_FILENAME);
	$extension=pathinfo($_FILES["report_file"]["name"], PATHINFO_EXTENSION);
	if($filename!=""){
			$renamed_file=time().".".$extension;
			$upload_file=$renamed_file;
			$upload_file_path="importdata/".$renamed_file;
			move_uploaded_file($_FILES["report_file"]["tmp_name"],$upload_file_path);
	}
	$contents = file($upload_file_path); 
    $renamed_csv_file="importdata/".time().".csv";
	$handle = fopen($renamed_csv_file,'w');
	foreach($contents as $line)
	{
		fputcsv($handle, explode("\t", $line));
	}
	fclose($handle);
	$handle = fopen("$renamed_csv_file", "r");
	$file_path=$renamed_csv_file;
	$objPHPExcel = PHPExcel_IOFactory::load($file_path);
	$sheetData = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);
	$rowscnt=count($sheetData);
	$i=1;
	$j=0;
	foreach ($sheetData as $row){

			   if($row['B']!="" && $row['B']!="Transaction ID"){
						   if($row['X']!=""){
							  $expiry_date=fundateconversion($row['X'],"db");
							}
							else{
							  $expiry_date="";
							}
							if($row["AJ"]!=""){
								 
								if (strpos($row["AJ"],"AM") !== false || strpos($row["AJ"],"PM") !== false  ) {
								   $time_stamp1=$row['AJ'];
								}
								else{
								    $time_stamp1=$row['AJ']." AM";
								 }
								 $time_stamp1=str_replace('/', '-', $time_stamp1);
								 $settled_date=date('Y-m-d H:i:s',strtotime($time_stamp1));  
									
							}
							else{
							  $settled_date="";
							}
							if($row['AV']!=""){
								$report_time=convertion($row['AV']);
							}
							else{
							   $report_time="";
							}
							 
						 
							 $insertQry="insert into tbl_temp_paypal_report(transaction_id,aba_routing_number,account_number,currency_symbol,amount,authcode,batch_id,billing_address,billing_address_2,billing_city,billing_company_name,billing_country,billing_email,billing_first_name,billing_last_name,billing_state,billing_zip,csc_match,comment1,comment2,customer_code,duty_amount,expires,freight_amount,invoice_number,original_transaction_id,paypal_email_id,paypal_fees,paypal_transaction_id,payment_advice_code,pending_reason,purchase_order,response_msg,result_code,settled_date,shipping_address,shipping_address_2,shipping_city,shipping_country,shipping_email,shipping_first_name,shipping_last_name,shipping_state,shipping_zip,tax_amount,tender_type,time,transaction_state,type,user,created_date,modified_date) values('".addslashes($row['B'])."','".addslashes($row['C'])."','".addslashes($row['D'])."','".addslashes($row['E'])."','".addslashes($row['F'])."','".addslashes($row['G'])."','".addslashes($row['H'])."','".addslashes($row['I'])."','".addslashes($row['J'])."','".addslashes($row['K'])."','".addslashes($row['L'])."','".addslashes($row['M'])."','".addslashes($row['N'])."','".addslashes($row['O'])."','".addslashes($row['P'])."','".addslashes($row['Q'])."','".addslashes($row['R'])."','".addslashes($row['S'])."','".addslashes($row['T'])."','".addslashes($row['U'])."','".addslashes($row['V'])."','".addslashes($row['W'])."',
							'".$expiry_date."','".addslashes($row['Y'])."','".addslashes($row['Z'])."','".addslashes($row['AA'])."','".addslashes($row['AB'])."','".addslashes($row['AC'])."','".addslashes($row['AD'])."','".addslashes($row['AE'])."','".addslashes($row['AF'])."','".addslashes($row['AG'])."','".addslashes($row['AH'])."','".addslashes($row['AI'])."','".$settled_date."','".addslashes($row['AK'])."','".addslashes($row['AL'])."','".addslashes($row['AM'])."','".addslashes($row['AN'])."','".addslashes($row['AO'])."','".addslashes($row['AP'])."','".addslashes($row['AQ'])."','".addslashes($row['AR'])."','".addslashes($row['AS'])."','".addslashes($row['AT'])."','".addslashes($row['AU'])."','".$report_time."','".addslashes($row['AW'])."','".addslashes($row['AX'])."','".addslashes($row['AY'])."','".$dbdatetime."','".$dbdatetime."')";
							$prepisnertQry=$DBCONN->prepare($insertQry);
							$insertRes=$prepisnertQry->execute();
				           if($insertRes){
								$j++;
							}
			}
			else{
			  $j++;
			}
	}
	$GetQry="select * from  tbl_temp_paypal_report";
	$prepgetQry=$DBCONN->prepare($GetQry);
	$prepgetQry->execute();
	$count = $prepgetQry->rowCount();
	if($count>0){
		while($GetRow=$prepgetQry->fetch()){
			$transaction_id=$GetRow["transaction_id"];
			if($GetRow["time"]!=""){
					$transaction_date=date('Y-m-d',strtotime($GetRow["time"]));
			}
			else{
				 $transaction_date="";
			}
			$currency_symbol=$GetRow["currency_symbol"];
			$amount=$GetRow["amount"];
			$payment_status=$GetRow["response_msg"];
			$comment2=$GetRow["comment2"];
			if(strpos($comment2,'Optional Trx') !== false) {
					$results=explode(' ',$comment2);  
					$rpref= $results[8];
					$get_demo="select * from tbl_paypal where rphref='".$rpref."'";

			}
			else{
					$results=explode(' ',$comment2);  
					$profile_id= $results[2];
					$get_demo="select * from tbl_paypal where profile_id='".$profile_id."'";
			}
			$prepgetdemoQry=$DBCONN->prepare($get_demo);
	        $prepgetdemoQry->execute();
			$getdemoRow=$prepgetdemoQry->fetch();
			$demo_id=$getdemoRow["demo_id"];
			$customer_id=$getdemoRow["profile_id"];
			$getDemoQry="select * from tbl_demo where demo_id=:demoid";
			$pepDemoQry=$DBCONN->prepare($getDemoQry);
			$pepDemoQry->execute(array(':demoid'=>$demo_id));
			$getDemo_Row=$pepDemoQry->fetch();
			$a_id=stripslashes($getDemo_Row["a_id"]);
			$l_id=stripslashes($getDemo_Row["l_id"]);
            $insert_qry_report="insert into tbl_report_paypal(transaction_id,transaction_date,customer_id,amount,currency,affiliate_id,licensee_id,	payment_status,created_date,modified_date) values('".$GetRow["transaction_id"]."','".$transaction_date."','".$customer_id."','".$GetRow["amount"]."','".$GetRow["currency_symbol"]."','".$getDemo_Row["a_id"]."','".$getDemo_Row["l_id"]."','".$GetRow["response_msg"]."','".$dbdatetime."','".$dbdatetime."')";
			$prepisnertQry1=$DBCONN->prepare($insert_qry_report);
		    $insertRes1=$prepisnertQry1->execute();

		}
	}
	header("Location:paypal_report.php");
	exit;
}
include("includes/header.php"); 
?>
<body>
		<div>
			<div style="margin-left:auto;margin-right:auto;">
				
				<div class="content">
					<div class="list_content">
						<h1 style="font-size:25px;padding-top:15px;padding-bottom:15px;margin:0px;">Import Paypal List</h1>
						<form name="report_form" id="report_form" method="post" enctype="multipart/form-data">
					   <input type="hidden" name="hdn_value"  id="hdn_value" value="test">
						<table cellspacing="15" cellpadding="0" border="0" width="70%">
							<tr>
								<td style="width:138px;">
									 Upload File<font color="red">*</font>:
								</td>
								<td>
									 <input type="file" name="report_file" id="report_file" class="inp_feild">
								</td>
							</tr>
							
							<tr>
							     <td>
									
									
								</td>
								<td>
									  <div class="form_actions">
										<input type="button" value="Import Paypal List" class="add_btn" id="get_list1">
									</div>
								</td>
							</tr>
						</table>
						</form>
					</div>
					
				</div>
			</div>
		</div>
	</body>
</html>
    <script type="text/javascript">
    $(document).ready(function(){
      $("#get_list1").click(function(){
		var filename=$("#report_file").val();
		var filenameArr=filename.split(".");
		var extension=filenameArr.pop();
		if($("#report_file").val()==""){
			alert("Please select a file to upload");
			$("#report_file").focus();
			return false;
		}
		else if(extension!="txt"){
			alert("Please upload valid file");
			$("#report_file").focus();
			return false;
		}
		else{
			$("#report_form").submit();
		}
	});
 });
  </script>


  













	