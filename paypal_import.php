<?php
set_time_limit(0);
include("../includes/configure.php");
include("includes/cmm_functions.php");
include("includes/PHPExcel.php");
$dbdatetime=date('Y-m-d H:i:s',strtotime('now'));
error_reporting(E_ALL);

if(isset($_POST["hdn_value"]))
{
	//Create table [If not exists] for temporarily storing the paypal data
    $sqlqry="CREATE TABLE IF NOT EXISTS `tbl_temp_paypal_report`(transaction_id varchar(300) DEFAULT NULL, aba_routing_number varchar(300) DEFAULT NULL, account_number int DEFAULT NULL, currency_symbol varchar(300) DEFAULT NULL, amount int DEFAULT NULL, authcode varchar(300) DEFAULT NULL, batch_id varchar(300) DEFAULT NULL, billing_address varchar(300) DEFAULT NULL, billing_address_2 varchar(300) DEFAULT NULL, billing_city varchar(300) DEFAULT NULL, billing_company_name varchar(300) DEFAULT NULL, billing_country varchar(300) DEFAULT NULL, billing_email varchar(300) DEFAULT NULL, billing_first_name varchar(300) DEFAULT NULL, billing_last_name varchar(300) DEFAULT NULL, billing_state varchar(300) DEFAULT NULL, billing_zip varchar(300) DEFAULT NULL, csc_match varchar(300) DEFAULT NULL, comment1 varchar(300) DEFAULT NULL, comment2 varchar(300) DEFAULT NULL, customer_code varchar(300) DEFAULT NULL, duty_amount varchar(300) DEFAULT NULL, expires date DEFAULT NULL, freight_amount varchar(300) DEFAULT NULL, invoice_number varchar(300) DEFAULT NULL, original_transaction_id varchar(300) DEFAULT NULL, paypal_email_id varchar(300) DEFAULT NULL, paypal_fees varchar(300) DEFAULT NULL, paypal_transaction_id varchar(300) DEFAULT NULL, payment_advice_code varchar(300) DEFAULT NULL, pending_reason varchar(300) DEFAULT NULL, purchase_order varchar(300) DEFAULT NULL, response_msg varchar(300) DEFAULT NULL, result_code varchar(300) DEFAULT NULL, settled_date datetime DEFAULT NULL, shipping_address varchar(300) DEFAULT NULL, shipping_address_2 varchar(300) DEFAULT NULL, shipping_city varchar(300) DEFAULT NULL, shipping_country varchar(300) DEFAULT NULL, shipping_email varchar(300) DEFAULT NULL, shipping_first_name varchar(300) DEFAULT NULL, shipping_last_name varchar(300) DEFAULT NULL, shipping_state varchar(300) DEFAULT NULL, shipping_zip varchar(300) DEFAULT NULL, tax_amount varchar(300) DEFAULT NULL, tender_type varchar(300) DEFAULT NULL,ptime datetime DEFAULT NULL,transaction_state varchar(300) DEFAULT NULL,type varchar(300) DEFAULT NULL,user varchar(300) DEFAULT NULL)";
	$prepqry=$DBCONN->prepare($sqlqry);
	$result=$prepqry->execute();

	//Create table [If not exists] for storing actual paypal data
	$sqlqry="CREATE TABLE IF NOT EXISTS `tbl_report_paypal`(`report_id` int(11) NOT NULL AUTO_INCREMENT, `transaction_id` varchar(250) DEFAULT NULL, `transaction_date` date DEFAULT NULL, `profile_id` varchar(500) DEFAULT NULL, `amount` double DEFAULT NULL, `currency` varchar(250) DEFAULT NULL, `demo_id` int(11) DEFAULT NULL, `a_id` int(11) DEFAULT NULL, `l_id` int(11) DEFAULT NULL, `payment_status` varchar(500) DEFAULT NULL, `last_upload_flag` ENUM('0','1') DEFAULT '0' NOT NULL, `created_date` datetime DEFAULT NULL, `modified_date` datetime DEFAULT NULL, PRIMARY KEY (`report_id`)) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1";
	$prepqry=$DBCONN->prepare($sqlqry);
	$result=$prepqry->execute();

	// Save the uploaded text file to the path  -- Start here
	$filename=pathinfo($_FILES["report_file"]["name"], PATHINFO_FILENAME);
	$extension=pathinfo($_FILES["report_file"]["name"], PATHINFO_EXTENSION);
	if($filename!="")
	{
		$renamed_file=time().".".$extension;
		$upload_file_path="importdata/".$renamed_file;
		move_uploaded_file($_FILES["report_file"]["tmp_name"],$upload_file_path);
	}
	// Save the uploaded text file to the path  -- End here

	// Loading text file to CSV -- Start here
	$contents = file($upload_file_path); 
    $file_path=$renamed_csv_file="importdata/".time().".csv";
	$handle = fopen($renamed_csv_file,'w');
	foreach($contents as $line)
	{
		fputcsv($handle, explode("\t", $line));
	}
	fclose($handle);
	// Loading text file to CSV -- End here

	// Reading CSV file using PHPExcel Reader to store data to temp table -- Start here
	$handle = fopen("$renamed_csv_file", "r");
	$objPHPExcel = PHPExcel_IOFactory::load($file_path);
	$sheetData = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);
	$rowscnt=count($sheetData);
	$i=1;$j=0;
	foreach ($sheetData as $row)
	{
		if($row['B']!="" && $row['B']!="Transaction ID")
		{
			$expiry_date=($row['X']!="")?fundateconversion($row['X'],"db"):"";
			
			$settled_date=($row["AJ"]!="")?funconversion($row['AJ']):"";
			
			$report_time=($row['AV']!="")?funconversion($row['AV']):"";
						 
			$insertqry="insert into tbl_temp_paypal_report(transaction_id,aba_routing_number,account_number,currency_symbol,amount,authcode,batch_id,billing_address,billing_address_2,billing_city,billing_company_name,billing_country,billing_email,billing_first_name,billing_last_name,billing_state,billing_zip,csc_match,comment1,comment2,customer_code,duty_amount,expires,freight_amount,invoice_number,original_transaction_id,paypal_email_id,paypal_fees,paypal_transaction_id,payment_advice_code,pending_reason,purchase_order,response_msg,result_code,settled_date,shipping_address,shipping_address_2,shipping_city,shipping_country,shipping_email,shipping_first_name,shipping_last_name,shipping_state,shipping_zip,tax_amount,tender_type,ptime,transaction_state,type,user) values('".addslashes($row['B'])."','".addslashes($row['C'])."','".addslashes($row['D'])."','".addslashes($row['E'])."','".addslashes($row['F'])."','".addslashes($row['G'])."','".addslashes($row['H'])."','".addslashes($row['I'])."','".addslashes($row['J'])."','".addslashes($row['K'])."','".addslashes($row['L'])."','".addslashes($row['M'])."','".addslashes($row['N'])."','".addslashes($row['O'])."','".addslashes($row['P'])."','".addslashes($row['Q'])."','".addslashes($row['R'])."','".addslashes($row['S'])."','".addslashes($row['T'])."','".addslashes($row['U'])."','".addslashes($row['V'])."','".addslashes($row['W'])."',
			'".$expiry_date."','".addslashes($row['Y'])."','".addslashes($row['Z'])."','".addslashes($row['AA'])."','".addslashes($row['AB'])."','".addslashes($row['AC'])."','".addslashes($row['AD'])."','".addslashes($row['AE'])."','".addslashes($row['AF'])."','".addslashes($row['AG'])."','".addslashes($row['AH'])."','".addslashes($row['AI'])."','".$settled_date."','".addslashes($row['AK'])."','".addslashes($row['AL'])."','".addslashes($row['AM'])."','".addslashes($row['AN'])."','".addslashes($row['AO'])."','".addslashes($row['AP'])."','".addslashes($row['AQ'])."','".addslashes($row['AR'])."','".addslashes($row['AS'])."','".addslashes($row['AT'])."','".addslashes($row['AU'])."','".$report_time."','".addslashes($row['AW'])."','".addslashes($row['AX'])."','".addslashes($row['AY'])."')";
			$prepinsertQry=$DBCONN->prepare($insertqry);
			$insertres=$prepinsertQry->execute();
			if($insertRes)
				$j++;
		}
		else
			$j++;
	}
	// Reading CSV file using PHPExcel Reader to store data to temp table -- End here

	// Move records from temp table to original table -- Start here
	$sqlqry="select * from tbl_temp_paypal_report";
	$prepqry=$DBCONN->prepare($sqlqry);
	$prepqry->execute();
	$count = $prepqry->rowCount();
	if($count>0)
	{

		//Update last_upload_flag to 0 to all records -- Start Here
		$sqlqry2	="update tbl_report_paypal set last_upload_flag='0'";
		$prepqry2	=$DBCONN->prepare($sqlqry2);
		$prepqry2->execute();
		//Update last_upload_flag to 0 to all records -- End Here

		$FetchRow=$prepqry->fetchAll();
		foreach($FetchRow as $rw => $GetRow)
		{
			$transaction_id		=$GetRow["transaction_id"];
			$transaction_date	=($GetRow["ptime"]!="")?date('Y-m-d',strtotime($GetRow["ptime"])):"";
			$currency_symbol	=$GetRow["currency_symbol"];
			$amount				=$GetRow["amount"];
			$payment_status		=$GetRow["response_msg"];
			$comment2			=$GetRow["comment2"];
			
			/*
			if(strpos($comment2,'Optional Trx') !== false) {
				$results=explode(' ',$comment2);  
				$rpref= $results[8];
				$get_demo="select * from tbl_paypal where rphref='".$rpref."'";
			}
			else{
				$results=explode(' ',$comment2);  
				$profile_id= $results[2];
				$get_demo="select * from tbl_paypal where profile_id='".$profile_id."'";
			}
			*/
			$lwrcomment2=strtolower($comment2);$profilecondn="";
			if(is_numeric(strpos($lwrcomment2,'recurring profile ')))
			{
				$recpos=strpos($lwrcomment2,'recurring profile ');
				$recprofileid=substr($lwrcomment2,$recpos+18,stripos(substr($lwrcomment2,$recpos+18,strlen($lwrcomment2)-$recpos+18)," "));
				$profilecondn=" AND SUBSTRING(profile_id,-".strlen($recprofileid).")='".$recprofileid."'";
			}
			else if(is_numeric(strpos($lwrcomment2,'recurring profile: ')))
			{
				$recpos=strpos($lwrcomment2,'recurring profile: ');
				$recprofileid=substr($comment2,$recpos+19,stripos(substr($comment2,$recpos+19,strlen($comment2)-$recpos+19)," "));
				$profilecondn=" AND LOWER(profile_id)='".strtolower($recprofileid)."'";
			}
			
			if(!empty($profilecondn))
			{
				// Get demo_id,affiliate and licensee for the profile -- Start here
				$sqlqry	="select * from tbl_paypal where profile_id<>'' $profilecondn";
				$prepqry=$DBCONN->prepare($sqlqry);
				$prepqry->execute();
				$getRow=$prepqry->fetch();
				$demo_id	=$getRow["demo_id"];
				$profile_id	=$getRow["profile_id"];

				$sqlqry="select * from tbl_demo where demo_id=:demoid";
				$prepqry=$DBCONN->prepare($sqlqry);
				$prepqry->execute(array(':demoid'=>$demo_id));
				$getRow=$prepqry->fetch();
				$a_id=stripslashes($getRow["a_id"]);
				$l_id=stripslashes($getRow["l_id"]);
				/*
				//Get demo_id, affiliate id, licensee id directly from tbl_demo table -- Start here
				$sqlqry="select demo_id,a_id,l_id from tbl_demo where profile_id<>'' $profilecondn";
				$prepqry=$DBCONN->prepare($sqlqry);
				$prepqry->execute();
				$getRow=$prepqry->fetch();
				$demo_id=stripslashes($getRow["demo_id"]);
				$a_id	=stripslashes($getRow["a_id"]);
				$l_id	=stripslashes($getRow["l_id"]);
				//Get demo_id, affiliate id, licensee id directly from tbl_demo table -- End here
				*/
				// Get demo_id,affiliate and licensee for the profile -- End here
				
				//Insert/Update paypal records using Paypal transaction id -- Start here
				$sqlqry="select * from tbl_report_paypal where transaction_id='".$transaction_id."'";
				$prepqry=$DBCONN->prepare($sqlqry);
				$prepqry->execute();
				if($prepqry->rowCount()>0)
					$sqlqry="update tbl_report_paypal set transaction_date='".$transaction_date."',profile_id='".$profile_id."',amount='".$amount."',currency='".$currency_symbol."',demo_id='".$demo_id."',a_id='".$a_id."',l_id='".$l_id."',	payment_status='".$payment_status."',last_upload_flag='1',modified_date='".$dbdatetime."' where transaction_id='".$transaction_id."'";
				else
					$sqlqry="insert into tbl_report_paypal(transaction_id,transaction_date,profile_id,amount,currency,demo_id,a_id,l_id,payment_status,last_upload_flag,created_date,modified_date) values('".$transaction_id."','".$transaction_date."','".$profile_id."','".$amount."','".$currency_symbol."','".$demo_id."','".$a_id."','".$l_id."','".$payment_status."','1','".$dbdatetime."','".$dbdatetime."')";
				//echo $sqlqry."<br>";
				$prepqry=$DBCONN->prepare($sqlqry);
				$result=$prepqry->execute();
				//Insert/Update paypal records using Paypal transaction id -- Start here
			}
		}
	}
	// Move records from temp table to original table -- End here
	//Truncate temp table -- Start here
	$sqlqry="truncate table tbl_temp_paypal_report";
	$prepqry=$DBCONN->prepare($sqlqry);
	$prepqry->execute();
	//Truncate temp table -- End here

	//Remove uploaded/created text and CSV files -- Start here
	unlink($upload_file_path);
	unlink($renamed_csv_file);
	//Remove uploaded/created text and CSV files -- End here

	header("Location:paypal_report.php");
	exit;
}
include("includes/header.php"); 
?>
<body>
		<div>
			<div style="margin-left:auto;margin-right:auto;">
				
				<div class="content">
					<div class="list_content">
						<h1 style="font-size:25px;padding-top:15px;padding-bottom:15px;margin:0px;">Import Paypal List</h1>
						<form name="report_form" id="report_form" method="post" enctype="multipart/form-data">
					   <input type="hidden" name="hdn_value"  id="hdn_value" value="test">
						<table cellspacing="15" cellpadding="0" border="0" width="70%">
							<tr>
								<td style="width:138px;">
									 Upload File<font color="red">*</font>:
								</td>
								<td>
									 <input type="file" name="report_file" id="report_file" class="inp_feild">
								</td>
							</tr>
							
							<tr>
							     <td>
									
									
								</td>
								<td>
									  <div class="form_actions">
										<input type="button" value="Import Paypal List" class="add_btn" id="get_list1">
									</div>
								</td>
							</tr>
						</table>
						</form>
					</div>
					
				</div>
			</div>
		</div>
	</body>
</html>
    <script type="text/javascript">
    $(document).ready(function(){
      $("#get_list1").click(function(){
		var filename=$("#report_file").val();
		var filenameArr=filename.split(".");
		var extension=filenameArr.pop();
		if($("#report_file").val()==""){
			alert("Please select a file to upload");
			$("#report_file").focus();
			return false;
		}
		else if(extension!="txt"){
			alert("Please upload valid file");
			$("#report_file").focus();
			return false;
		}
		else{
			$("#report_form").submit();
		}
	});
 });
  </script>