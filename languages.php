<?php
include("../includes/configure.php");
include("../includes/session_check.php");
$language_id=$_GET["language_id"];
$dbdatetime=date('Y-m-d H:i:s',strtotime('now'));

if($language_id!=""){
	//$language_id
	$getQry="select * from  tbl_language where language_id=:language_id";
	$prepgetQry=$DBCONN->prepare($getQry);
	$prepgetQry->execute(array(":language_id"=>$language_id));
	//$getRes=mysql_query($getQry);
	$getRow=$prepgetQry->fetch();
	$language=stripslashes($getRow["language"]);
	$mode="Edit";
	$value="Update";
}
else{
	$mode="Add";
	$value="Create";
}
if(isset($_POST["language"])){
	$Language=addslashes(trim($_POST["language"]));
    if($mode=="Edit"){
		//$language_id  $Language
		$updateQry="update   tbl_language set language=:language,modified_date=:modified_date where language_id=:language_id";
		$prepupdateQry=$DBCONN->prepare($updateQry);
		$updateRes=$prepupdateQry->execute(array(":language"=>$Language,":modified_date"=>$dbdatetime,":language_id"=>$language_id));
		//$updateRes=mysql_query($updateQry);
		if($updateRes){
			header("Location:language_list.php");
			exit;
		}
	}
	else{
		//$Language
		$insertQry="insert into  tbl_language(language,added_date,modified_date)values(:language,:added_date,:modified_date)";
		$prepinsQry=$DBCONN->prepare($insertQry);
		$insertRes=$prepinsQry->execute(array(":language"=>$Language,":added_date"=>$dbdatetime,":modified_date"=>$dbdatetime));
		//$insertRes=mysql_query($insertQry);
		if($insertRes){
			header("Location:language_list.php");
			exit;
		}
	}
}
 include("includes/header.php");
?>
 <body>
		<div>
			<div style="margin-left:auto;margin-right:auto;">
				 
				<div class="content">
					<div class="list_content">
						<h1 style="font-size:25px;padding-top:15px;padding-bottom:15px;margin:0px;">New 
						Language </h1>
						<form name="language_form" id="language_form" method="post" enctype="multipart/form-data">
						<input type="hidden" name="language_form_id" id="language_form_id" value="<?php echo $language_id;?>">
						<table cellspacing="15" cellpadding="0" border="0" width="70%">
							<tr>
								<td style="width:138px;">
									Language<font color="red">*</font>:

								</td>
								<td>
									<input type="text" name="language" id="language" class="inp_feild" value="<?php echo $language;?>">
								</td>
							</tr>
							
							<tr>
							     <td>
									<div class="form_actions" style="text-align:left;">
										<input type="button" value="Back To Languages List" class="add_btn"  onclick="document.location='language_list.php'">
									
								</td>
								<td>
									  <div class="form_actions" style="text-align:right;">
										<input type="button" value="<?php echo $value;?> Language" class="add_btn" id="add_language">
									</div>
								</td>
							</tr>
						</table>
						</form>
					</div>
					
				</div>
			</div>
		</div>
<?php
include("includes/footer.php");
?>