<?php
$parent_directory=basename(dirname($_SERVER["PHP_SELF"]));
$filename=basename($_SERVER["PHP_SELF"]);
include("../includes/configure.php");
include("includes/cmm_functions.php");
include("../includes/session_check.php");
$id=$_SESSION['user_id'];	
$feat="download_list";
$typeandfeature=checklogin($id,$feat);
$usrArr=explode("*",$typeandfeature);
$user_type=$usrArr[0];
$feature=$usrArr[1];
$trialformfeature=$usrArr[4];
$masterlistfeature=$usrArr[5];
include("includes/header.php");
?>
<body>
	<form name="demo_list" method="post">
		<input type="hidden" name="HiddenMode" id="HiddenMode" value="">
		<input type="hidden" name="HdnPage" id="HdnPage" value="">
			<div>
					<div style="margin-left:auto;margin-right:auto;">
							<div class="content">
									<div class="list_content">
										<h3 style="font-size: 1.8rem;line-height: 2rem;font-weight: 500;font-family: 'Raleway', sans-serif;text-align:center;color: #fff;margin-top:20px;">Admin Features</h3>
									<div>
									<table cellspacing="5" cellpadding="0" border="0" style="margin-top:50px;">
										<tr>
										   <!-- first -->
											<td valign="top"> 
												<table cellspacing="5" cellpadding="0" border="0">
													<tr>
														<td class="tdbtn">
																<div class="form_actions" style="padding:0px;">
																	<?php
																	if($masterlistfeature=="yes"||$user_type=="Admin"){
																	?>
																		<input type="button" value="Master List" class="add_btn" onclick="document.location='masterlist.php'" style="width:195px;">
																	<?php
																	}
																	?>
																</div>
														</td>
													</tr>
													<tr>
														<td class="tdbtn">
															<div class="form_actions" style="padding:0px;">
																<?php
																if($trialformfeature=="yes"||$user_type=="Admin" ||$user_type=="Affiliate"){
																?>
																		<input type="button" value="Prospect List" class="add_btn" onclick="document.location='prospectinglist.php'" style="width:195px;">
																<?php
																}
																?>
															</div>
														</td>  
													</tr>
													<tr>
														<td class="tdbtn">
															<div class="form_actions" style="padding:0px;">
																<?php
																$getQry="select * from tbl_users where user_id='".$id."'";
																$getRes=mysql_query($getQry);
																$getRow=mysql_fetch_array($getRes);
																$usertype=$getRow["user_type"];
																$licensee_page=$getRow["licensee_list"];
																$affiliate_page=$getRow["alliance_list"];
																if($licensee_page=="yes"||$user_type=="Admin"){
																?>
																	<input type="button" value="Licensee List" class="add_btn" onclick="document.location='licensee_list.php'" style="width:195px;">
																<?php
																}
																?>
															</div>
														</td>
													</tr>
													<tr>
														<td class="tdbtn">
															<div class="form_actions" style="padding:0px;">
																<?php
																$getQry="select * from tbl_users where user_id='".$id."'";
																$getRes=mysql_query($getQry);
																$getRow=mysql_fetch_array($getRes);
																$usertype=$getRow["user_type"];
																$licensee_page=$getRow["licensee_list"];
																$affiliate_page=$getRow["alliance_list"];
																if($affiliate_page=="yes" && $user_type!="Affiliate"||$user_type=="Admin"){
																?>
																	<input type="button" value="Affiliate List" class="add_btn" onclick="document.location='affiliate_list.php'" style="width:195px;">
																<?php
																}
																?>
															</div>
														</td>
													</tr>
												</table>
                                             </td>
											 <!-- second -->
											<td valign="top"> 
												<table cellspacing="5" cellpadding="0" border="0">
													<tr>
														<td class="tdbtn">
															<div class="form_actions" style="padding:0px;">
																<?php
																if($_SESSION['user_id']=="3" || $user_type=="Admin"){
																?>
																	<input type="button" value="Regional Defaults" class="add_btn" onclick="document.location='manage_simulator.php'" style="width:195px;"> 
																<?php
																}
																?>
															</div>
														</td>
													</tr>
													<tr>
														<td class="tdbtn">
															<div class="form_actions" style="padding:0px;">
																<?php
																if( $user_type=="Admin"){
																?>
																	<input type="button" value="Product Pages" class="add_btn" onclick="document.location='product_list.php'" style="width:195px;">
																<?php
																}
																?>
															</div>
														</td>
													</tr>
													<tr>
														<td class="tdbtn">
															<?php
															if($_SESSION['user_id']=="3" || $user_type=="Admin"){
															?>
																<div class="form_actions" style="padding:0px;">
																	<input type="button" value="Email List" class="add_btn" onclick="document.location='mail_list.php'" style="width:195px;"> 
																</div>
															<?php
															}
															?>
														</td>
													</tr>
													<tr>
														<td class="tdbtn">
															<?php
															if($_SESSION['user_id']=="3" || $user_type=="Admin"){
															?>
																<div class="form_actions" style="padding:0px;">
																	<input type="button" value="Free Trials List" class="add_btn" onclick="document.location='trials_list.php'" style="width:195px;"> 
																</div>
															<?php
															}
															?>
														</td>
													</tr>
													<tr>
														<td class="tdbtn">
																<div class="form_actions" style="padding:0px;">
																	<?php
																	if( $user_type=="Admin"){
																	?>
																		<input type="button" value="Terms of Service" class="add_btn" onclick="document.location='terms.php'" style="width:195px;">
																	<?php
																	}
																	?>
																</div>
														</td>
													</tr>
												</table>
											</td>
											 <!-- third -->
										<td valign="top"> 
												<table cellspacing="5" cellpadding="0" border="0">
												 
													<tr>
														<td class="tdbtn">
															<div class="form_actions" style="padding:0px;">
																<?php
																if( $user_type=="Admin"){
																?>
																	<input type="button" value="Developer List" class="add_btn" onclick="document.location='developerlist.php'" style="width:195px;">
																<?php
																}
																?>
															</div>
														</td>
													</tr>
													<tr>
														<td class="tdbtn">
															<div class="form_actions" style="padding:0px;">
																<?php
																if( $user_type=="Admin"){
																?>
																		<input type="button" value=" Business Type List" class="add_btn" onclick="document.location='business_list.php'" style="width:195px;">
																<?php
																}
																?>
															</div>
					
														</td>
													</tr>
													<tr>
														<td class="tdbtn">
																<div class="form_actions" style="padding:0px;">
																	<?php
																	if( $user_type=="Admin"){
																	?>
																		<input type="button" value="Status List" class="add_btn" onclick="document.location='statuslist.php'" style="width:195px;">
																	<?php
																	}
																	?>
															</div>
														</td>
													</tr>
													<tr>
														<td class="tdbtn">
															<div class="form_actions" style="padding:0px;">
																<?php
																if( $user_type=="Admin"){
																?>
																	<input type="button" value="Call Status List" class="add_btn" onclick="document.location='callstatuslist.php'" style="width:195px;">
																<?php
																}
																?>
															</div>
														</td>
													</tr>
													<tr>
														<td class="tdbtn">
															<div class="form_actions" style="padding:0px;">
																<?php
																if( $user_type=="Admin"){
																?>
																	<input type="button" value="Affiliate Status List" class="add_btn" onclick="document.location='affiliate_status_list.php'" style="width:195px;">
																<?php
																}
																?>
															</div>
														</td>
													</tr>
													<tr>
														<td class="tdbtn">
															<div class="form_actions" style="padding:0px;">
																<?php
																if( $user_type=="Admin"){
																?>
																	<input type="button" value="Country List" class="add_btn" onclick="document.location='country_list.php'" style="width:195px;">
																<?php
																}
																?>
															</div>
														</td>
													</tr>
													<tr>
														<td class="tdbtn">
															<div class="form_actions" style="padding:0px;">
																<?php
																if( $user_type=="Admin"){
																?>
																	<input type="button" value="Language List" class="add_btn" onclick="document.location='language_list.php'" style="width:195px;">
																<?php
																}
																?>
															</div>
														</td>
													</tr>
													<tr>
														<td class="tdbtn">
															<div class="form_actions" style="padding:0px;">
																<?php
																if($user_type=="Admin"){
																?>
																	<input type="button" value="App Category List" class="add_btn" onclick="document.location='Itunelist.php'" style="width:195px;">
																<?php
																}
																?>
															</div>	
														</td>
													</tr>
													<tr>
														<td class="tdbtn">
															<div class="form_actions" style="padding:0px;">
															  <?php
																if($user_type=="Admin"){
																?>	
													          <input type="button" value="App Version List" class="add_btn" onclick="document.location='versionlist.php'" style="width:195px;">
                                                              	<?php
																}
																?>
															</div>	
														</td>
													</tr>
												</table>
											</td>
											<!-- fourth -->
											<td valign="top"> 
												<table cellspacing="5" cellpadding="0" border="0">
													<tr>
														<td class="tdbtn">
															<div class="form_actions" style="padding:0px;">
																<?php
																if($_SESSION['user_id']=="3" || $_SESSION['user_id']=="173"){
																?>
																	<input type="button" value="Manage Users" class="add_btn" onclick="document.location='userslist.php'" style="width:195px;">
																<?php
																}
																?>
															</div>
														</td>
													</tr>
													<tr>
														<td class="tdbtn">
																<div class="form_actions" style="padding:0px;">
																	<?php
																	if($user_type=="Admin" || $_SESSION['user_id']=="173"){
																	?>
																		<input type="button" value="Email Messages" class="add_btn" onclick="document.location='templates.php'" style="width:195px;">
																	<?php
																	}
																	?>
																</div>
														</td>
													</tr>
													<tr>
														<td class="tdbtn">
																<div class="form_actions" style="padding:0px;">
																	<?php
																	if($_SESSION['user_id']=="3" || $_SESSION['user_id']=="173"){
																	?>
																			<!-- <input type="button" value="Import Prospect List" class="add_btn" onclick="document.location='importexcel.php'" style="width:195px;"> -->
																			 <input type="button" value="Import Prospect List" class="add_btn" onclick="alert('This feature is currently disabled.');"  style="width:195px;"> 
																	<?php
																	}
																	?>
																</div>
														</td>
													</tr>
													<tr>
														<td class="tdbtn">
																<div class="form_actions" style="padding:0px;">
																	<?php
																	if($_SESSION['user_id']=="3" || $_SESSION['user_id']=="173" ){
																	?>
																		<input type="button" value="Download DB Dump" class="add_btn" onclick='funcdbexport()' style="width:195px;">
																	<?php
																	}
																	?>
																</div>  	
														</td>
													</tr>
													<tr>
														<td class="tdbtn">
															<div class="form_actions" style="padding:0px;">
																<?php
																if($_SESSION['user_id']=="3" || $_SESSION['user_id']=="173"){
																?>
																	<input type="button" value="Download Master List" class="add_btn" onclick="funexport('masterlist');" style="width:195px;">
																<?php
																}
																?>
															</div>
														</td>
													</tr>
													<tr>
														<td class="tdbtn">
															<div class="form_actions" style="padding:0px;">
																<?php
																if($_SESSION['user_id']=="3" || $_SESSION['user_id']=="173"){
																?>
																	<!-- <input type="button" value="Download Prospect List" class="add_btn" onclick="funexport('prospectslist');" style="width:195px;"> -->
																	<input type="button" value="Download Prospect List" class="add_btn" onclick="alert('This feature is currently disabled.');" style="width:195px;">
																<?php
																}
																?>
															</div>
														</td>
													</tr>
													<tr>
														<td class="tdbtn">
																<div class="form_actions" style="padding:0px;">
																	<?php
																	if($_SESSION['user_id']=="3" || $_SESSION['user_id']=="173" ){
																	?>
																		<input type="button" value="Paypal Report" class="add_btn" onclick="document.location='paypal_report_list.php'" style="width:195px;">
																	<?php
																	}
																	?>
																</div> 
														</td>
													</tr>
													<tr>
														<td class="tdbtn">
														    <div class="form_actions" style="padding:0px;">
																	<?php
																	if($_SESSION['user_id']=="3" || $_SESSION['user_id']=="173" ){
																	?>
																		<input type="button" value="Survey Benchmarks" class="add_btn" onclick="document.location='addbenchmark.php'" style="width:195px;">
																	<?php
																	}
																	?>
															</div> 
														</td>
													</tr>
													<!-- <tr>
													    
														 <td class="tdbtn">
															<div class="form_actions" style="padding:0px;">
																	<?php
																	if($_SESSION['user_id']=="3" || $_SESSION['user_id']=="173" ){
																	?>
																		<input type="button" value="Loreal Surveys" class="add_btn" onclick="document.location='loreal_masterlist.php'" style="width:195px;">
																	<?php
																	}
																	?>
																</div> 
														 </td>
													</tr> -->
												</table>
											</td>
										</tr>
									</table>
							</div> 
					</div>
			</div>
		</div>
		</form>
<?php
include("includes/footer.php");
?>
<script>
function funexport(listitem)
{
	try
	{
		with(document.demo_list)
		{
			if(listitem=='masterlist'){
				action='demolist-export.php';
				submit();
				return true;
				action='';
			}
			if(listitem=='prospectslist'){
				action='prospectlist-export.php';
				submit();
				return true;
				action='';
			}
		}
	}
	catch(e)
	{
		alert(e)
	}
}

/****function for paging statrs*******/
function pagetransfer(pagenumber,formname)
{	
	with(document.forms[formname])
	{ 
		HdnPage.value=pagenumber;
		HiddenMode.value="paging";
		submit();
	}
}
/****function for paging ends*******/

function funcdbexport()
{
 try
 {
with(document.demo_list)
  {
	   action='mysqldump.php';
	   submit();
	    return true;
		action='';
  }
 }
 catch(e)
 {
  alert(e)
 }
}
</script>