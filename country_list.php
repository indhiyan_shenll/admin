<?php
include("../includes/configure.php");
include("../includes/session_check.php");
$country_id=$_GET["country_id"];
$sort=$_GET["sort"];
$field=$_GET["field"];
if($sort==""){
	$sort="asc";
}
if($field==""){
	$field="country_id";
}
if($field=="sno"){
	$fieldname="country_id";
	$order=" order by ".$fieldname." ".$sort;
	if($sort=="asc")
		$dsort="desc";
	else
		$dsort="asc";
}
if($field=="Country"){
	$fieldname="country";
	$order=" order by ".$fieldname." ".$sort;
	if($sort=="asc"){
		$rsort="desc";
		$rpath="images/up.png";
	}
	else{
		$rsort="asc";
		$rpath="images/down.png";
	}
}
if($country_id!=""){
	$deleteQry="delete from  tbl_country where country_id='".$country_id."'";
	$deleteRes=mysql_query($deleteQry);
	if($deleteRes){
		header("Location:country_list.php");
		exit;
	}
}
include("includes/header.php");
?>
 <body>
		<div>
			<div style="margin-left:auto;margin-right:auto;">
				<div class="content">
					<div class="list_content">
						<div class="form_actions" style="padding-bottom:45px;">
							<input type="button" value="Back To Admin Features" class="add_btn" onclick="document.location='admin_features.php'" style="float:left;">
							<input type="button" value="Add Country" class="add_btn" onclick="document.location='country.php'" style="float:right;">
						</div>
							<div class="header_div">
							<table cellspacing="0" cellpadding="0" width="100%" class="tbl_header">
								
								<tr>
									<th width="10%">No</th><!-- onclick="document.location='statuslist.php?sort=<?php echo $dsort;?>&field=sno'" -->
									<th width="80%" onclick="document.location='country_list.php?sort=<?php echo $rsort;?>&field=Country'" style="cursor:pointer">Country&nbsp;&nbsp;<?php if($rpath!=""){?><img src="<?php echo $rpath;?>" style="width:10px;height:10px;"><?php }?></th>
									<th width="10%">Delete?</th>
								</tr>
							</table>
						</div>
							<div class="gap" ></div> 
						<table cellspacing="0" cellpadding="0" width="100%" class="tbl-body">
						<?php
							$getQry="select * from   tbl_country".$order;
							//exit;
							$getRes=mysql_query($getQry);
							$getCnt=mysql_num_rows($getRes);
							if($getCnt>0){
								$i=1;
								while($getRow=mysql_fetch_array($getRes)){
									
									if($i%2==1){
										$bgcolor="#a5a5a5";
									}
									else{
										$bgcolor="#d2d1d1";
									}
						?>
							<tr bgcolor="<?php echo $bgcolor;?>">
								<td width="10%"><?php echo $i;?></td>
								<td width="80%"><?php echo stripslashes($getRow["country"]);?></td>
								<td width="10%"><a href="country.php?country_id=<?php echo $getRow["country_id"];?>">Edit</a>&nbsp;&nbsp;/&nbsp;&nbsp;<a href="country_list.php?country_id=<?php echo $getRow["country_id"];?>" onclick="return confirm('Are you sure want to delete this country?')">Delete</a></td>
							</tr>
							
						<?php
							$i++;
								}
								?>
							
							
								<?php
							}
							else{
								echo "<tr bgcolor='#a5a5a5'><td colspan=\"3\"><center>No countries found.</center></td></tr>";
							}
						?>
						<tr><td height="10px"></td></tr>
						<tr>
								<td colspan="3">
								<div class="form_actions" style="text-align:left;position:relative;" >
								<input type="button" value="Back To Admin Features" class="add_btn" onclick="document.location='admin_features.php'">
								</td>
							</tr>
						</table>
					</div>
				</div>
			</div>
		</div>
<?php
include("includes/footer.php");
?>