<?php
$parent_directory=basename(dirname($_SERVER["PHP_SELF"]));
$filename=basename($_SERVER["PHP_SELF"]);
include("../includes/configure.php");
include("includes/cmm_functions.php");
include("../includes/session_check.php");
$id=$_SESSION['user_id'];
$prospect_id=$_GET["demo_id"];
$dbdatetime=date('Y-m-d H:i:s',strtotime('now'));
$sort=$_GET["sort"];
$field=$_GET["field"];
$feat="trail_form";
$typeandfeature=checklogin($id,$feat);
$usrArr=explode("*",$typeandfeature);
$user_type=$usrArr[0];
$mfeature=$usrArr[1];
$trialfeature=$usrArr[4];
$masterfeature=$usrArr[5];
if(($trialfeature!="yes")&&$user_type!="Admin" && $user_type!="Affiliate" && $user_type!="Agent" && $user_type!="Licensee"){
	header("Location:noauthorised.php");
	exit;
}
else{

$AffArr=array();
$GeQry="select * from  tbl_affiliate";
$GetRes=mysql_query($GeQry);
$GetCnt=mysql_num_rows($GetRes);
if($GetCnt>0){
	while($GetRow=mysql_fetch_array($GetRes)){
		$AffId=$GetRow["a_user_id"];
		$AffArr[$AffId]=stripslashes($GetRow["full_name"]);
	}
}
if($sort==""){
	$sort="asc";
}
if($field==""){
	$field="import_date";
}
if($field=="import_date"){
	$fieldname="import_date";
	$order=" order by ".$fieldname." ".$sort;
	if($sort=="asc"){
		$dsort="desc";
		$dpath="images/up.png";
	}
	else{
		$dsort="asc";
		$dpath="images/down.png";
	}
}
if($field=="business_type"){
	$fieldname="business_type";
	$order=" order by ".$fieldname." ".$sort;
	if($sort=="asc"){
		$ddsort="desc";
		$ddpath="images/up.png";
	}
	else{
		$ddsort="asc";
		$ddpath="images/down.png";
	}
}
if($field=="bus_name"){
	$fieldname="business_name";
	$order=" order by ".$fieldname." ".$sort;
	if($sort=="asc"){
		$rsort="desc";
		$rpath="images/up.png";
	}
	else{
		$rsort="asc";
		$rpath="images/down.png";
	}
}
 
if($field=="phone"){
	$fieldname="phone";
	$order=" order by ".$fieldname." ".$sort;
	if($sort=="asc"){
		$usort="desc";
		$upath="images/up.png";
	}
	else{
		$usort="asc";
		$upath="images/down.png";
	}
}
if($field=="callbackdate"){
	$fieldname="callback_date";
	$order=" order by ".$fieldname." ".$sort;
	if($sort=="asc"){
		$cdsort="desc";
		$cdpath="images/up.png";
	}
	else{
		$cdsort="asc";
		$cdpath="images/down.png";
	}
}
if($prospect_id!=""){
	$deleteQry="delete from tbl_prospecting_list where special_one_id=:prospect_id";
	$prepdeleteQry=$DBCONN->prepare($deleteQry);
	$deleteRes=$prepdeleteQry->execute(array(":prospect_id"=>$prospect_id));
}
if(isset($_POST['HdnPage']) && $_POST['HdnPage']!="" && $_POST['HdnPage']!="0")
	$Page=$_POST['HdnPage'];
else if(!empty($_SESSION["page"]))
	$Page=$_SESSION["page"];
else
	$Page=1;
$_SESSION["page"]=$Page;
if($_GET["mode"]=="unset")
{ 
	$_SESSION["SES_START_DATE"]='';$_SESSION["SES_END_DATE"]='';unset($_SESSION["filter"]);
	 
}
if((isset($_POST['search_field'])&&$_POST['search_field']!="")||(isset($_POST['search_start_date'])&&$_POST['search_start_date']!="")||(isset($_POST['search_end_date'])&&$_POST['search_end_date']!="") || !empty($_SESSION["SES_START_DATE"]) || !empty($_SESSION["SES_END_DATE"])){
	$searchstr= mysql_real_escape_string($_POST['search_field']);
	$sear_start_date=clearIn($_POST['search_start_date']);
	$search_end_date=clearIn($_POST['search_end_date']);
	$sear_start_date=(!empty($sear_start_date))?$sear_start_date:$_SESSION["SES_START_DATE"];
    $sear_end_date=(!empty($search_end_date))?$search_end_date:$_SESSION["SES_END_DATE"];
	if(isset($_POST['search_start_date']))
		$_SESSION["SES_START_DATE"]=$sear_start_date;
	if(isset($_POST['search_end_date']))
		$_SESSION["SES_END_DATE"]=$search_end_date;
    if($searchstr!=""){
       $searchcondn.=" and business_name like '%".$searchstr."%'";
    }
	if($sear_start_date!=""&&$sear_end_date!=""){
      $searchcondn.=" and (callback_date>='".date('Y-m-d',strtotime($sear_start_date))."' and callback_date<='".date('Y-m-d',strtotime($sear_end_date))."')";
    }
	if($sear_start_date!=""&&$sear_end_date==""){
		$searchcondn.=" and callback_date='".date('Y-m-d',strtotime($sear_start_date))."'";
	}
	if($sear_start_date==""&&$sear_end_date!=""){
		$searchcondn.=" and callback_date='".date('Y-m-d',strtotime($sear_end_date))."'";
	}
  }
if(isset($_POST["dlete_mode"]) && !empty($_POST["delete_yes"]) && $_POST["delete_yes"]=='yes'){
	$checkIds=$_POST["delete_id"];
	$delStr="";
	for($del=0;$del<count($checkIds);$del++){
		if($delStr=="")
			$delStr=" special_one_id='".$checkIds[$del]."'";
		else
			$delStr.=" or special_one_id='".$checkIds[$del]."'";
	}
	$deleteQry="delete from tbl_prospecting_list where".$delStr;
	$deleteRes=mysql_query($deleteQry);
	
}
if(!empty($_POST["update_yes"]) && $_POST["update_yes"]=='yes'){
	$checkIds=$_POST["update_id"];
	$affiliate=$_POST["affiliate"];
	if($_POST["affiliate"]!="0" && $_POST["update_id"]!=""){
	     $prspct_id= explode(",",$prospect_id_values);
		 $result_response = implode(",",$checkIds);
		 $affiliate=$_POST["affiliate"];
		 $updateQry1="update tbl_prospecting_list set a_id=:a_id,assign_agents=:assign_agents,modified_date=:modified_date where find_in_set(cast(special_one_id as char), :result)";
		 $prepupdateQry1=$DBCONN->prepare($updateQry1);
		 $updateRes1=$prepupdateQry1->execute(array(":a_id"=>$affiliate,":assign_agents"=>$affiliate,":modified_date"=>$dbdatetime,":result"=>$result_response));
		 if($updateRes1){
		    header("Location:prospectinglist.php");
		    exit;
	     }
	
	} 
	 
}
if(isset($_POST["import_date1"]) || isset($_SESSION["filter"]["import_date1"])){
	$import_date1=(isset($_POST["import_date1"]))?addslashes(trim($_POST["import_date1"])):$_SESSION["filter"]["import_date1"];
	$import_date2=(isset($_POST["import_date2"]))?addslashes(trim($_POST["import_date2"])):$_SESSION["filter"]["import_date2"];
	$salesperson=(isset($_POST["salesperson"]))?$_POST["salesperson"]:$_SESSION["filter"]["salesperson"];
	$salesmanager=(isset($_POST["salesmanager"]))?$_POST["salesmanager"]:$_SESSION["filter"]["salesmanager"];
	$callstatus=(isset($_POST["callstatus"]))?$_POST["callstatus"]:$_SESSION["filter"]["callstatus"];
	$suburb=(isset($_POST["suburb"]))?$_POST["suburb"]:$_SESSION["filter"]["suburb"];
	$state=(isset($_POST["state"]))?$_POST["state"]:$_SESSION["filter"]["state"];
	$last_contact1=(isset($_POST["last_contact1"]))?addslashes(trim($_POST["last_contact1"])):$_SESSION["filter"]["last_contact1"];
	$last_contact2=(isset($_POST["last_contact2"]))?addslashes(trim($_POST["last_contact2"])):$_SESSION["filter"]["last_contact2"];
	
	if($import_date1!=""&&$import_date2!=""){
		$condition.=" and (import_date>='".date('Y-m-d',strtotime($import_date1))."' and import_date<='".date('Y-m-d',strtotime($import_date2))."')";
	}
	if($import_date1!=""&&$import_date2==""){
		$condition.=" and import_date='".date('Y-m-d',strtotime($import_date1))."'";
	}
	if($import_date1==""&&$import_date2!=""){
		$condition.=" and import_date='".date('Y-m-d',strtotime($import_date2))."'";
	}
	if(!empty($salesperson)){
		$condition.=" and ".setfieldarray("sales_person",$salesperson);
	}
	if(!empty($salesmanager)){
		$condition.=" and ".setfieldarray("sales_manager",$salesmanager);
	}
	if(!empty($callstatus)){
		$condition.=" and ".setfieldarray("callstatus",$callstatus);
		
	}
	if($suburb!="0"){
		$condition.=" and suburb='".$suburb."'";
	}
	if($state!="0"){
		$condition.=" and state='".$state."'";
	}
	if($last_contact1!=""&&$last_contact2!=""){
		$condition.=" and (last_contact>='".date('Y-m-d',strtotime($last_contact1))."' and last_contact<='".date('Y-m-d',strtotime($last_contact2))."')";
	}
	if($last_contact1!=""&&$$last_contact2==""){
		$condition.=" and last_contact='".date('Y-m-d',strtotime($last_contact1))."'";
	}
	if($last_contact1==""&& $last_contact2!=""){
		$condition.=" and last_contact='".date('Y-m-d',strtotime($last_contact2))."'";
	}
	$_SESSION["filter"]["import_date1"]=$import_date1;$_SESSION["filter"]["import_date2"]=$import_date2;$_SESSION["filter"]["salesperson"]=$salesperson;$_SESSION["filter"]["salesmanager"]=$salesmanager;$_SESSION["filter"]["callstatus"]=$callstatus;$_SESSION["filter"]["suburb"]=$suburb;$_SESSION["filter"]["state"]=$state;$_SESSION["filter"]["last_contact1"]=$last_contact1;$_SESSION["filter"]["last_contact2"]=$last_contact2;
}
/*==For Admin Users */
if($_GET["mode"]=="unset" && $_POST["my_records"]=="")
{ 
	$_SESSION['my_records']='';
}
 /*==For Admin Users */
if($user_type=="Admin"){
        if(isset($_POST["my_records"]) && !empty($_POST["my_records"]) && $_POST["my_records"]=='yes' ||  $_SESSION['my_records']=="yes"){
					 $usercondn=" and a_id='".$_SESSION['user_id']."'";
                     $_SESSION['my_records']="yes";
        }
		else{
					$usercondn="  and(a_id='".$_SESSION['user_id']."' OR assign_agents='' OR assign_agents='0')";
			}
  
  }
/*==For Affiliate */ 
if($user_type=="Affiliate"){
  $getDemoQry1="select * from tbl_affiliate where a_user_id='".$_SESSION['user_id']."'";
  $getDemoRes1=mysql_query($getDemoQry1);
  $getDemoRow1=mysql_fetch_array($getDemoRes1);
  //$usercondn=" and (a_id='".$getDemoRow1["affiliate_id"]."' OR assign_agents='".$getDemoRow1["a_user_id"]."')";
  if($getDemoRow1["affiliate_id"]!=""){
	$usercondn=" and (a_id='".$getDemoRow1["affiliate_id"]."' OR assign_agents='".$getDemoRow1["a_user_id"]."')";
   }
  else{
	  $usercondn=" and assign_agents='".$_SESSION['user_id']."'";
	}
  
}

 /*==For  Agent */
if($user_type=="Agent"){
		  $getDemoQry1="select * from tbl_affiliate where a_user_id='".$_SESSION['user_id']."'";
		  $getDemoRes1=mysql_query($getDemoQry1);
		  $getDemoRow1=mysql_fetch_array($getDemoRes1);
		  $usercondn=" and agent_id='".$_SESSION['user_id']."' OR user_id='".$_SESSION['user_id']."'";
}
/*==For Licence */ 
if($user_type=="Licensee"){
        $getDemoQry1="select * from tbl_licensee where l_user_id='".$_SESSION['user_id']."'";
		$getDemoRes1=mysql_query($getDemoQry1);
		$getDemoRow1=mysql_fetch_array($getDemoRes1);
		if($getDemoRow1["licensee_id"]!=""){
		  $usercondn=" and (l_id='".$getDemoRow1["licensee_id"]."' OR assign_agents='".$getDemoRow1["l_user_id"]."')";
		}
		else{
		  $usercondn=" and assign_agents='".$_SESSION['user_id']."'";
		}
		
}
echo $getDemoQry="select * from tbl_prospecting_list where special_one_id!='' ".$condition.$usercondn.$searchcondn.$order;
$getDemoRes=mysql_query($getDemoQry);
$getDemoCnt=mysql_num_rows($getDemoRes);
include("includes/header.php");
?>
<body <?php if($deleteRes){ ?>onload="redirect()"<?php }?>>
<form name="demo_list" id="demo_list" method="post">
<script type="text/javascript" src="js/jquery.clearsearch.js"></script>
<script>
	$(function() {
			$('#search_field').clearSearch();
			$("#search_start_date").datetimepicker({ format:'d-m-Y',formatDate:'d-m-Y',timepicker:false});
			$("#search_end_date").datetimepicker({ format:'d-m-Y',formatDate:'d-m-Y',timepicker:false});
	});
	function checkallboxes(chkcnt){
		var opt=document.getElementById("checkall").checked;
		var i;
		for(i=1;i<=chkcnt;i++){
			document.getElementById("delete_"+i).checked=opt;
		}
	}
	function chkind(totcnt){
		var chkdcnt=0;
		for(i=1;i<=totcnt;i++){
			if(document.getElementById("delete_"+i).checked)
				chkdcnt++;
		}
		if(chkdcnt==totcnt)
			document.getElementById("checkall").checked=true;
		else
			document.getElementById("checkall").checked=false;
	}
	function chkselcnt(cnt){
		var chkdcnt=0;
		for(i=1;i<=cnt;i++){
			if(document.getElementById("delete_"+i).checked)
				chkdcnt++;
		}
		return chkdcnt;
	}
	function searchoption(){
       var businessname=$.trim($("#search_field").val());
	   var startdate=$.trim($("#search_start_date").val());
	   var enddate=$.trim($("#search_end_date").val());
	   var startdateday=startdate.split("-")[0];
	   var startdatemonth=startdate.split("-")[1];
	   var startdateyear=startdate.split("-")[2];
	   var enddateday=enddate.split("-")[0];
	   var enddatemonth=enddate.split("-")[1];
	   var enddateyear=enddate.split("-")[2];
	   startdate=new Date(startdateyear, startdatemonth-1, startdateday);
	   enddate=new Date(enddateyear, enddatemonth-1, enddateday);
	   if($.trim($("#search_field").val())==""&&$.trim($("#search_start_date").val())==""&&$.trim($("#search_end_date").val())==""){

           alert('Please fill atleast one field to search');
			document.getElementById("search_field").focus();
			 return false;
		}
		else if(startdate.getTime()>enddate.getTime())  {
			   alert('Please select end date is greater then or equal to start date');
			 
				document.getElementById("search_start_date").focus();
					return false;
	    }
		else{
				$("#demo_list").submit();
		}
	    
	}
	function getmyrecords(){
		document.getElementById('my_records').value='yes';
        $("#demo_list").submit();
	}
	</script>
	 				 
				<div class="content">				
					<div class="list_content">
						<div class="form_actions" style="width:1234px;">
							<?php
							if($masterfeature=="yes" || $user_type=="Affiliate" || $user_type=="Agent" || $user_type=="Licensee"  || $user_type=="Admin" ){
							?>
								  <input type="button" style="width:180px;" value="Add a Prospect" class="add_btn" onclick="document.location='prospectdetails.php'" ><br>
							<?php
							}
							if($masterfeature=="yes"){

							?>
								  <input type="button" style="width:180px;margin-top: 10px;" value="Go to Master List" class="add_btn" onclick="document.location='masterlist.php'">
							<?php
							 }
							?>
							<div style="width:153px;float:right;text-align:right;">
							         <input type="button" value="Filter List" class="add_btn" onclick="document.location='filterprospectlist.php'" style="display:none;"><br>
								    <input type="button" value="Remove Filters" class="add_btn" onclick="document.location='prospectinglist.php?mode=unset'" style="margin-top: 10px;"><br><br>
								    <span style="font-size:12px;font-weight:bolder;color:white;" id="demo_count">Total Prospects:&nbsp;<?php echo $getDemoCnt;?></span>								
							</div>
						    <div style="width:470px;float:right;text-align:right;padding-right:25px;color:white;font-size:13px;font-weight:bold;">
								
							<table cellspacing="0" cellpadding="0" width="100%">
							    
                                      <?php
								if($user_type=="admin"||$user_type=="Admin"){
								?>
								 <tr>
									<td colspan="2" align="left">
									<div class="form_actions" style="float:left;text-align:right;margin-top:-50px;">
										<input type="button" value="My Records" class="add_btn"  onclick="getmyrecords()" >
									</div>
										</td>
										
								</tr>
								<?php
								}
								?>
								<tr height="22px;">
									<td colspan="2" align="left"><span style="text-decoration:underline;">Filter Prospect List</span>
									</td>
										
								</tr>
								<tr>
									<td width="90%">
										<table cellspacing="0" cellpadding="0" width="100%" border="0">
									<tr height="25px;">
									<td align="left">
										 Business Name:
									</td>
									<td colspan="2">
										<input type="text" name="search_field" id="search_field" class="inp_feild" value="<?php echo $searchstr;?>" style="width:100%;" autocomplete="off">
									</td>
									</tr>
									<tr height="25px;">
											<td align="left">
												Callback Date:
											</td>
											<td align="left">
												Start:<input type="text" name="search_start_date" id="search_start_date" class="inp_feild"  value="<?php echo $sear_start_date;?>" style="width: 100px;">

											</td>
										   
											<td>
												End:<input type="text" name="search_end_date" id="search_end_date" class="inp_feild"  value="<?php echo $sear_end_date;?>" style="width:100px;">
											</td>
											
										</tr>
									</table>
									</td>
									<td>
										 <input type="button" value="Go" class="add_btn" style="min-width:40px;padding:0px;float:right;height: 43px;" onclick="searchoption()">
									</td>
									</tr>
									</table>
							</div>						
						</div>
						 <div style="padding-bottom:12px;margin-top:45px;">
						    <input type="hidden" name="HiddenMode" id="HiddenMode" value="">
							<input type="hidden" name="HdnPage" id="HdnPage" value="">
							<input type="hidden" name="dlete_mode" id="dlete_mode">
							<input type="hidden" name="delete_yes" id="delete_yes" value="">
							<input type="hidden" name="update_yes" id="update_yes">
							<input type="hidden" name="hdn_mode" id="hdn_mode">
							<input type="hidden" name="my_records" id="my_records">
					  </div>
						<table cellspacing="0" cellpadding="0" width="130%" class="tbl-body" border="0">
						 <tr  style="height: 75px;">
						 <td align="right" colspan="11" style="">
						    <div style="width:100%;float:left;">
							  <div style="top:0px;left;display:none;" class="select_box">
                                 <table style="border:1px solid #2661a7;float:left;">
                                 <tr>
									<td> 
									  <select name="affiliate" id="affiliate" class="inp_feild" style="width:140px;">
										<option value="0">Select</option>
										<?php
											 
										     $get_Qry="select * from  tbl_users where user_type='Admin' OR user_type='Licensee' OR user_type='Affiliate'";
											$get_Res=mysql_query($get_Qry);
											
											while($get_value=mysql_fetch_array($get_Res)){
										   ?>
											<option value="<?php echo stripslashes($get_value["user_id"]);?>"<?php if($affiliate_id==$get_value["user_id"]) echo " selected";?>><?php echo stripslashes($get_value["username"]);?></option>
										<?php
											}
										?>
									 </select>
                                   </td>
									<td>
									     <div class="form_actions" style="float:left;border:1 px solid red;width:35%;text-align:right;margin-top: -13px;">
												   <input type="button" value="Assign to affiliate" class="add_btn"   onclick="setvalue()">
												</div>
									
									</td>
                                 </tr>
                                 </table>
							
							</div>
							 
							 </td> 
						</tr>
						<?php								
						$records_perpage=100;
						$TotalRecords	=	$getDemoCnt;
						if($TotalRecords <= (($Page * $records_perpage)-$records_perpage))
							$Page	=	$Page-1;
						$TotalPages		=	ceil($TotalRecords/$records_perpage);
						$Start			=	($Page-1)*$records_perpage;
						if($TotalPages>1)
						  {
						?>
						<tr style="height: 75px;">
							 
							<td align="center" colspan="11" style="">
								<?php
								$FormName="demo_list";
								include("../includes/paging.php");
								?>
							</td>
						</tr>
						<?php
						  }
						?>
					 
							<tr class="tbl_header" style="border-collapse: none;">
							       <?php
								    $style_design="cursor:pointer;border-top-left-radius: 10px;border-bottom-left-radius: 10px;";
									if($user_type=="Admin" && $_SESSION['user_id']=="3"){
									?>
									    <th style="<?php echo  $style_design;?>width:7%;"><center><span onclick="AsignRecordstoaffiliate('<?php echo $_SESSION['user_id'];?>')"> Status</span>&nbsp;<input type="checkbox" name="checkall_up" id="checkall_up"     onclick="checkallboxes1(document.getElementById('recordcnt').value)"></center></th>
                                        
									<?php
									$style_design="cursor:pointer;";
									}
									?>
									<th style="width:8%;<?php echo  $style_design;?>" onclick="document.location='prospectinglist.php?sort=<?php echo $dsort;?>&field=import_date'">Create Date&nbsp;<?php if($dpath!=""){?><img src="<?php echo $dpath;?>" style="width:10px;height:10px;"><?php }?></th>
									<th style="cursor:pointer;width:10%;" onclick="document.location='prospectinglist.php?sort=<?php echo $rsort;?>&field=bus_name'">Business Name&nbsp;&nbsp;<?php if($rpath!=""){?><img src="<?php echo $rpath;?>" style="width:10px;height:10px;"><?php }?></th>
									<th style="width:10%;" >Full Name&nbsp;&nbsp;</th>								 
									<th style="cursor:pointer;width:13%;" onclick="document.location='prospectinglist.php?sort=<?php echo $ddsort;?>&field=business_type'">Business Type&nbsp;&nbsp;<?php if($ddpath!=""){?><img src="<?php echo $ddpath;?>" style="width:10px;height:10px;"><?php }?></th>
									<th style="width:8%;">Promo Code &nbsp;&nbsp;</th>
									<th style="width:10%;cursor:pointer;" onclick="document.location='prospectinglist.php?sort=<?php echo $usort;?>&field=phone'">Phone&nbsp;&nbsp;<?php if($upath!=""){?><img src="<?php echo $upath;?>" style="width:10px;height:10px;"><?php }?></th>
									<th style="cursor:pointer;width:15%;" onclick="document.location='prospectinglist.php?sort=<?php echo $pdsort;?>&field=call'">Call Status&nbsp;&nbsp;<?php if($pdpath!=""){?><img src="<?php echo $pdpath;?>" style="width:10px;height:10px;"><?php }?></th>
                                    <th style="width:9%;">Affiliate&nbsp;&nbsp;</th> 
									<th style="width:4%;cursor:pointer;border-top-right-radius: 10px;border-bottom-right-radius: 10px;">
									<table width="100%">
										<tr>
											<td><span onclick="deleteRecords('<?php echo $user_type;?>')">Delete?</span></td>
											<td><input type="checkbox" name="checkall" id="checkall"onclick="checkallboxes(document.getElementById('recordcnt').value)"></td>
										</tr>
									</table>
								 
									
									
									</th>								 
								</tr>
								 
							<tr  class="gap">
								<td colspan="10" style=""></td>
							</tr>
						   <?php
							if($getDemoCnt>0){			
								$records_perpage=100;
								$TotalRecords	=	$getDemoCnt;
								if($TotalRecords <= (($Page * $records_perpage)-$records_perpage))
									$Page	=	$Page-1;
								$TotalPages		=	ceil($TotalRecords/$records_perpage);
								$Start			=	($Page-1)*$records_perpage;
								
								//code for paging ends
								$getDemoQry.=" limit $Start,$records_perpage";
								$getDemoRes = mysql_query($getDemoQry);
								$getDemoCnt = mysql_num_rows($getDemoRes);

								$i=1;
								$prospect_id_array=array();
								while($getDemoRow=mysql_fetch_array($getDemoRes)){
									$prospect_id=$getDemoRow["special_one_id"];
									$prospect_id_array[]=$getDemoRow["special_one_id"];
									$demo_id=$getDemoRow["demo_id"];
									if(!empty($demo_id)){
										$get_demourl_qry=mysql_query("select demo_page_url from tbl_demo where demo_id=$demo_id");
										$demo_pg_url=mysql_fetch_assoc($get_demourl_qry);
									}else
										$demo_pg_url['demo_page_url']='';;
									$import_date=stripslashes($getDemoRow["import_date"]);
									$callback_date=stripslashes($getDemoRow["callback_date"]);

									$Call_time=stripslashes($getDemoRow["callback_time"]);
									if($Call_time!="" && $Call_time!="00:00:00"){
									  $Call_time=date('g:i A',strtotime(stripslashes($getDemoRow["callback_time"])));
									}
									else{
									  $Call_time="";
									}
									$bdot='';
									if(strlen($getDemoRow["business_name"])>15)
							     	$bdot="...";
									//$business_name=stripslashes($getDemoRow["business_name"]);
									$suburb=stripslashes($getDemoRow["suburb"]);
									if($getDemoRow["business_type"]!="0"){
									  $business_type=stripslashes($getDemoRow["business_type"]);
									}
									else{
									 $business_type="";
									}
									
									$phone=stripslashes($getDemoRow["phone"]);
									$push_status=($getDemoRow["pushing_status"]);
								
									$arr2=explode("-",$getDemoRow["last_contact"]);
								  $contact=$arr2[2]."-".$arr2[1]."-".$arr2[0];
									if($contact==""||$contact=="00-00-0000")
									{
										$last_contact="";

									}
									else{
                                       $last_contact=$contact;
                                    }
									if($getDemoRow["callstatus"]!="0"){
										$callstatus=stripslashes($getDemoRow["callstatus"]);
										$getcolorQry="select * from  tbl_callstatus where callstatus='".$callstatus."'";
										$getcolorRes=mysql_query($getcolorQry);
										$getcolorRow=mysql_fetch_array($getcolorRes);
										$Status_color=stripslashes($getcolorRow["status_color"]);
										if($callstatus!=""&&$callstatus!="0"){
											$bgcolor='#'.$Status_color;

										}
										else{
												$bgcolor="white";
										}
									}
									else{
									  $callstatus="";
									
									}
									 
									$sales_person=stripslashes($getDemoRow["sales_person"]);
									
                                    $font_color='black';
									$stdot="";
									if(strlen($callstatus)>20)
										$stdot="...";
									if($bgcolor=="" || $bgcolor=="#"){
									  $bgcolor="white";
									}
							?>
							<tr style="background-color:<?php echo $bgcolor;?>">
							   <?php
						if($user_type=="Admin" && $_SESSION['user_id']=="3"){
						?>
							<td style="border-bottom:1px solid grey;color:<?php echo $font_color;?>"><center><input type="checkbox" name="update_id[]" value="<?php echo $getDemoRow["special_one_id"];?>" id="update_<?php echo $i;?>" onclick="chkind1('<?php echo $getDemoCnt;?>')" <?php if($demo_view_status=="1"){echo " checked";}?> ><center></td>
						<?php
						}
						?>
				<td style="border-bottom:1px solid grey;cursor:pointer;color:<?php echo $font_color;?>" onclick="edit_record('<?php echo $prospect_id;?> ');">
						  <?php if($import_date!=""){ echo   date("d-m-Y",strtotime($import_date));}?></td>
				<td style="border-bottom:1px solid grey;cursor:pointer;color:<?php echo $font_color;?>" onclick="edit_record('<?php echo $prospect_id;?> ');">
						  <?php echo readmore_view(stripslashes($getDemoRow["business_name"]),"15");?></td>
				<td style="border-bottom:1px solid grey;cursor:pointer;color:<?php echo $font_color;?>" onclick="edit_record('<?php echo $prospect_id;?> ');">
				         <?php echo   readmore_view(stripslashes($getDemoRow["first_name"]),"15");?></td>
				<td style="border-bottom:1px solid grey;cursor:pointer;color:<?php echo $font_color;?>" onclick="edit_record('<?php echo $prospect_id;?> ');">
				          <?php echo $business_type;?>
				</td>
				<td style="border-bottom:1px solid grey;cursor:pointer;color:<?php echo $font_color;?>" onclick="edit_record('<?php echo $prospect_id;?> ');">
				           <?php echo  readmore_view(stripslashes($getDemoRow["promo_code"]),"8");?>
				</td>
				<td style="border-bottom:1px solid grey;cursor:pointer;color:<?php echo $font_color;?>" onclick="edit_record('<?php echo $prospect_id;?> ');">
			        	<?php echo readmore_view($phone,"15");?> 
				</td>
				<td style="border-bottom:1px solid grey;cursor:pointer;color:<?php echo $font_color;?>" onclick="edit_record('<?php echo $prospect_id;?> ');">
				      <?php echo  readmore_view($callstatus,"30");?></td>
				<td style="border-bottom:1px solid grey;cursor:pointer;color:<?php echo $font_color;?>" onclick="edit_record('<?php echo $prospect_id;?> ');">
								  <?php
									$geQry_db="select * from  tbl_affiliate where a_user_id=:a_user_id";
									$prepgetuserqry_db=$DBCONN->prepare($geQry_db);
									$prepgetuserqry_db->execute(array(":a_user_id"=>$getDemoRow["a_id"]));
									$geUserRow_db=$prepgetuserqry_db->fetch();
									$affilat_name=stripslashes($geUserRow_db["full_name"]);
									if($affilat_name!=""){
									  $affilat_name=stripslashes($geUserRow_db["full_name"]);
									}
									else{
									 $affilat_name="";
									
									}
								   echo readmore_view($affilat_name,"20");
								   ?>
						   </td>
						<td style="border-bottom:1px solid grey;color:<?php echo $font_color;?>">
						<table width="100%">
						<tr>
							<!-- <td><a href="prospectdetails.php?prospect_id=<?php echo $prospect_id;?>" style="color:<?php echo $font_color;?>">Edit</a></td> -->
							<td align="center"><input type="checkbox" name="delete_id[]" value="<?php echo $prospect_id;?>" id="delete_<?php echo $i;?>" onclick="chkind('<?php echo $getDemoCnt;?>')"></td>
						</tr>
						</table>
						
						
						</td> 
					</tr>

						<?php
							$i++;
								}
								
								if($TotalPages>1)
								  {
								?>
								<tr>
									<td align="center" colspan="10" style="border-bottom:1px solid grey;">
										<?php
										$FormName="demo_list";
										include("../includes/paging.php");
										?>
									</td>
								</tr>
								<?php
								  }
								
							}
							else{
								echo "<tr bgcolor='#a5a5a5'><td colspan=\"10\" style=\"border-bottom:1px solid grey;text-align:center;\">No records to display</td></tr>";
							}
							echo "</table>";
							echo "<script>document.getElementById(\"demo_count\").innnerHTML=\"Demo Pages Count:&nbsp;$getDemoCnt\";</script>";
						?>
						
						<input type="hidden" name="recordcnt" id="recordcnt" value="<?php echo $getDemoCnt;?>">
						<input type="hidden" name="prospect_id_values" id="prospect_id_values" value="<?php echo implode(",",$prospect_id_array);?>">
						
					</div>
					<div style="height:50px;border:0px solid red;"></div>
				</div>
				
		</form>	
<?php
 include("includes/footer.php");
?>
	 
<script>
function edit_record(val){
  document.location.href="prospectdetails.php?prospect_id="+val;
}


function funexport()
{
	try
	{
		with(document.demo_list)
		{
			action='demolist-export.php';
			submit();
			return true;
			action='';
		}
	}
	catch(e)
	{
		alert(e)
	}
}

/****function for paging statrs*******/
function pagetransfer(pagenumber,formname)
{	
	with(document.forms[formname])
	{ 
		HdnPage.value=pagenumber;
		HiddenMode.value="paging";
		submit();
	}
}
/****function for paging ends*******/


function deleteRecords(usertype){
	try
	{
		with(document.demo_list)
		{
			if(usertype=="Admin"){
				var recordscnt=chkselcnt(document.getElementById("recordcnt").value);
				if(recordscnt==0){
					alert("Please select at least one prospect");
					return false;
				}
				else if(confirm('Are you sure want to delete these '+recordscnt+' prospects?'))
				{
					 document.getElementById('delete_yes').value='yes';
					submit();
				}else{
					document.getElementById("checkall").checked=false;
					var i;
					for(i=1;i<=recordscnt;i++){
						document.getElementById("delete_"+i).checked=false;
					}
				
				}


			}
			else{
				alert("You are not authorised to delete this item");
				return false;
			}
		}
	}
	catch(e)
	{
		alert(e)
	}
}

/****function for assign agents records  to affilite*******/
function AsignRecordstoaffiliate(userid){
	try
	{
		with(document.demo_list)
		{
			if(userid=="3"){
				var recordscnt=chkselcnt1(document.getElementById("recordcnt").value);
				 
				if(recordscnt==0){
					alert("Please select at least one record");
					return false;
				} 
				else{
				   $('.select_box').slideToggle("fast");
				
				}
			/*else if(confirm('Are you sure want to assign '+recordscnt+' records to  affiliate?'))
			{
					 
			}
			else{
					document.getElementById("checkall_up").checked=false;
					var i;
					for(i=1;i<=recordscnt;i++){
						document.getElementById("update_"+i).checked=false;
					}
				
				}*/
			}
			else{
				alert("You are not authorised to change this status");
				return false;
			}
		}
	}
	catch(e)
	{
		alert(e)
	}
}
function checkallboxes1(chkcnt){
		var opt=document.getElementById("checkall_up").checked;
		var i;
		for(i=1;i<=chkcnt;i++){
			document.getElementById("update_"+i).checked=opt;
		}
	}
    function chkind1(totcnt){
		var chkdcnt=0;
		for(i=1;i<=totcnt;i++){
			if(document.getElementById("update_"+i).checked)
				chkdcnt++;
		}
		if(chkdcnt==totcnt)
			document.getElementById("checkall_up").checked=true;
		else
			document.getElementById("checkall_up").checked=false;
	}
	function chkselcnt1(cnt){
		var chkdcnt=0;
		for(i=1;i<=cnt;i++){
			if(document.getElementById("update_"+i).checked)
				chkdcnt++;
		}
		return chkdcnt;
	}
  function setvalue()
  {
	   var val= document.getElementById('affiliate').value;
	   var recordscnt=chkselcnt1(document.getElementById("recordcnt").value);
	   
	   if(val=="0"){
	        alert("Please select affilite");
			$("#affilite").focus();
			return false; 
	   }
	   else if(confirm('Are you sure want to assign '+recordscnt+' records to  affiliate?'))
	   {
		      document.getElementById('update_yes').value='yes';
			  document.getElementById('hdn_mode').value=val;
              $("#demo_list").submit();			   
	  }
	  else{
		   $('.select_box').slideToggle("fast");		 
	  }
 }

</script>
<?php
	}
?>