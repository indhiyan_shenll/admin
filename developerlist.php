<?php
include("../includes/configure.php");
include("../includes/session_check.php");
$developer_id=$_GET["developer_id"];
$sort=$_GET["sort"];
$field=$_GET["field"];

if($field=="name"){
	$fieldname="developer_name";
	$order=" order by ".$fieldname." ".$sort;
	if($sort=="asc"){
		$nsort="desc";
		$npath="images/up.png";
	}
	else{
		$nsort="asc";
		$npath="images/down.png";
	}
}
if($field=="email"){
	$fieldname="developer_email";
	$order=" order by ".$fieldname." ".$sort;
	if($sort=="asc"){
		$rsort="desc";
		$rpath="images/up.png";
	}
	else{
		$rsort="asc";
		$rpath="images/down.png";
	}
}
if($developer_id!=""){
	$deleteQry="delete from tbl_developers  where developer_id='".$developer_id."'";
	$deleteRes=mysql_query($deleteQry);
	if($deleteRes){
		header("Location:developerlist.php");
		exit;
	}
}
include("includes/header.php");
?>
<body>
		<div>
			<div style="margin-left:auto;margin-right:auto;">
				 
				<div class="content">
					<div class="list_content">
						<div class="form_actions" style="padding-bottom:45px;">
							<input type="button" value="Back To Admin Features" class="add_btn" onclick="document.location='admin_features.php'" style="float:left;">
							<input type="button" value="Add Developer" class="add_btn" onclick="document.location='developer.php'" style="float:right;">
						</div>
						<div class="header_div" >
							<table cellspacing="0" cellpadding="0" width="100%" class="tbl_header"  style="border-radius:80px;">
								
								<tr>
									<th width="10%" style="border-top-left-radius:10px;border-bottom-left-radius:10px;" >No</th>
									<th width="30%" onclick="document.location='developerlist.php?sort=<?php echo $nsort;?>&field=name'" style="cursor:pointer"> Developer Name&nbsp;&nbsp;<?php if($npath!=""){?><img src="<?php echo $npath;?>" style="width:10px;height:10px;"><?php }?></th>
									<th width="50%" onclick="document.location='developerlist.php?sort=<?php echo $rsort;?>&field=email'" style="cursor:pointer"> Developer Email&nbsp;&nbsp;<?php if($rpath!=""){?><img src="<?php echo $rpath;?>" style="width:10px;height:10px;"><?php }?></th>
									<th width="10%" style="border-top-right-radius: 10px;border-bottom-right-radius: 10px;">Delete?</th>
								</tr>
							
							</table>
						</div>
						<div class="gap" ></div> 
						<table cellspacing="0" cellpadding="0" width="100%" class="tbl-body">
						<?php
							$getdeveloperQry="select * from tbl_developers".$order;
							$getdeveloperRes=mysql_query($getdeveloperQry);
							$getdeveloperCnt=mysql_num_rows($getdeveloperRes);
							if($getdeveloperCnt>0){
								$i=1;
								while($getdeveloperRow=mysql_fetch_array($getdeveloperRes)){
									
									if($i%2==1){
										$bgcolor="#a5a5a5";
									}
									else{
										$bgcolor="#d2d1d1";
									}
						?>
							<tr bgcolor="<?php echo $bgcolor;?>">
								<td width="10%" ><?php echo $i;?></td>
								<td width="30%"><?php echo stripslashes($getdeveloperRow["developer_name"]);?></td>
								<td width="50%"><?php echo stripslashes($getdeveloperRow["developer_email"]);?></td>
								<td width="10%"><a href="developer.php?developer_id=<?php echo $getdeveloperRow["developer_id"];?>">Edit</a>&nbsp;&nbsp;/&nbsp;&nbsp;<a href="developerlist.php?developer_id=<?php echo $getdeveloperRow["developer_id"];?>" onclick="return confirm('Are you sure want to delete this developer')">Delete</a></td>
							</tr>
							
						<?php
							$i++;
								}
								?>
							<tr><td height="10px"></td></tr>
							
								<?php
							}
							else{
								echo "<tr bgcolor='#a5a5a5'><td colspan=\"4\"><center>No developer(s) found.</center></td></tr>";
							}
						?>
						<tr>
								<td colspan="3">
								<div class="form_actions" style="text-align:left;position:relative;">
								<input type="button" value="Back To Admin Features" class="add_btn" onclick="document.location='admin_features.php'">
								</td>
							</tr>
						</table>
					</div>
				</div>
			</div>
		</div>
<?php
include("includes/footer.php");
?>