<?php
include("../includes/configure.php");
include("../includes/session_check.php");
$business_id=$_GET["id"];
if($business_id!=""){
	$deleteQry="delete from tbl_email where id=:id";
	$prepdeleteQry=$DBCONN->prepare($deleteQry);
	$deleteRes=$prepdeleteQry->execute(array(":id"=>$business_id));
	if($deleteRes){
		header("Location:mail_list.php");
		exit;
	}
}
if(isset($_POST['HdnPage']) && $_POST['HdnPage']!="" && $_POST['HdnPage']!="0")
	$Page=$_POST['HdnPage'];
else
	$Page=1;
include("includes/header.php"); 
?>
 
	<body>
		<div>
			<div style="margin-left:auto;margin-right:auto;">
				 
				<div class="content">
					<div class="list_content">
						<div class="form_actions" style="padding-bottom:45px;">
							<input type="button" value="Back To Admin Features" class="add_btn" onclick="document.location='admin_features.php'" style="float:left;">
							 
						</div>
						 
							<form name="demo_list" method="post">
							<input type="hidden" name="HiddenMode" id="HiddenMode" value="">
							<input type="hidden" name="HdnPage" id="HdnPage" value="">
							<div class="header_div">
							<table cellspacing="0" cellpadding="0" width="100%" class="tbl_header" border="0">
							  <tr>
									
									<th width="5%">No</th>
									<th width="30%"  style="cursor:pointer">Email&nbsp;&nbsp;</th>
									<th width="30%"  style="cursor:pointer">Subscribe Date&nbsp;&nbsp;</th>
									<th width="30%"  style="cursor:pointer">Country</th>
								   <th width="5%">Delete?</th>
								
							 </tr>
							</table>
						</div>
						<div class="gap" ></div> 
						<table cellspacing="0" cellpadding="0" width="100%" class="tbl-body" border="0">
                       <?php
					        $qryCondition="";
							$getQry="select * from  tbl_email  order by id desc";
							$prepgetQry=$DBCONN->prepare($getQry);
							$prepgetQry->execute();
							$count =$prepgetQry->rowCount();
							if($count>0){
								$records_perpage=25;
								$TotalRecords	=$count;
								if($TotalRecords <= (($Page * $records_perpage)-$records_perpage))
								$Page	=	$Page-1;
								$TotalPages		=	ceil($TotalRecords/$records_perpage);
								$Start			=	($Page-1)*$records_perpage;
								$getQry.=" limit $Start,$records_perpage";
								$prepgetQry=$DBCONN->prepare($getQry);
								$prepgetQry->execute();
								$count =$prepgetQry->rowCount();
								$sno=$Start+1;
                              if($count>0){
							  $rowno=1;
				               while($getRow=$prepgetQry->fetch()){
                                    if($rowno%2==1){
										$bgcolor="#a5a5a5";
									}
									else{
										$bgcolor="#d2d1d1";
									}
                                     
						?>
							<tr bgcolor="<?php echo $bgcolor;?>">
							   	   <td width="5%"><?php echo $sno;?></td>
									<td width="30%"><?php echo stripslashes($getRow["email_address"]);?></td>	
									<td width="30%"><?php echo date("d/m/Y", strtotime($getRow["created_date"]));?></td>	
									<td width="30%" style="cursor:pointer"><?php echo stripslashes($getRow["country"]);?></td> 
									<td width="5%">&nbsp;<a href="mail_list.php?id=<?php echo $getRow["id"];?>"  onclick="return confirm('Are you sure want to delete this record? ')">Delete</a></td>
							</tr>
							<?php
							$rowno++;
							$sno++;
								}
							if($TotalPages > 1){

									echo "<tr><td align='center' colspan='4' valign='middle' class='pagination'>";

									if($TotalPages>1){

											$FormName = "demo_list";
									       include("../includes/paging.php");
									 
									}

									echo "</td></tr>";

									  }
								}
								else{
									echo "<tr style=\"background-color:#f6f6f6;text-align:center;\"><td colspan=\"4\">No Record(s) found.</td></tr>";
								}

							
							}
							else{
								echo "<tr style=\"background-color:#f6f6f6;text-align:center;\"><td colspan=\"4\">No Record(s) found.</td></tr>";
							}
						?>
						<tr>
								<td colspan="4">
								<div class="form_actions" style="text-align:left;position:relative;">
								<input type="button" value="Back To Admin Features" class="add_btn" onclick="document.location='admin_features.php'">
								</td>
							</tr>
						</table>
						</form>
					</div>
				</div>
			</div>
		</div>
<?php
include("includes/footer.php");
?>
<script type="text/javascript">
/****function for paging statrs*******/
function pagetransfer(pagenumber,formname)
{	
	with(document.forms[formname])
	{ 
		HdnPage.value=pagenumber;
		HiddenMode.value="paging";
		submit();
	}
}
/****function for paging ends*******/
</script>
