<?php
//error_reporting(0);
include("includes/configure.php");
include("admin/includes/cmm_functions.php");
$demo_id=$_GET['id'];
$get_specials_qry="select * from tbl_demo where demo_id=:demo_id";
$prepget_special_qry=$DBCONN->prepare($get_specials_qry);
$prepget_special_qry->execute(array(":demo_id"=>$demo_id));
$getRow=$prepget_special_qry->fetch();
$old_restaurant_logo=$getRow['restaurant_logo'];
$business_name=$getRow['business_name'];
$email=$getRow['email'];
$comma_separated = explode(",", $getRow['app_name']);
$dbdatetime=date('Y-m-d H:i:s',strtotime('now'));
if(isset($_POST["app_name1"])){
	$demo_id=trim($_POST["hdn_id"]);                  
    $file_name=pathinfo($_FILES["restaurant_logo"]["name"], PATHINFO_FILENAME);
	$file_extension=pathinfo($_FILES["restaurant_logo"]["name"], PATHINFO_EXTENSION);
	if($file_name!=""){
		$renamed_filename=$file_name.time().".".$file_extension;
		$file_path=UPLOAD_PATH.$renamed_filename;
		$file_upload_path=UPLOAD_PATH.$renamed_filename;
		move_uploaded_file($_FILES["restaurant_logo"]["tmp_name"],$file_upload_path);
	}
	else{
		$file_path=$old_restaurant_logo;
	}
    $app_name[]=addslashes(trim($_POST["app_name1"]));
	$app_name[]=addslashes(trim($_POST["app_name2"]));
	$app_name[]=addslashes(trim($_POST["app_name3"]));
	$app_name_value=implode(", ",$app_name);
	$update_demo_Qry="update tbl_demo set restaurant_logo=:restaurant_logo,app_name=:app_name where demo_id=:demo_id";
	$prepupdate_demo_id_Qry=$DBCONN->prepare($update_demo_Qry);
	$updateRes=$prepupdate_demo_id_Qry->execute(array(":restaurant_logo"=>$file_path,":app_name"=>$app_name_value,":demo_id"=>$demo_id));
	if($updateRes){
			$gettemplateQry="select * from tbl_emailtemplates where title=:title";
			$prep_template_getQry=$DBCONN->prepare($gettemplateQry);
			$prep_template_getQry->execute(array(":title"=>"App information form"));
			$gettemplateRow=$prep_template_getQry->fetch();
	        //$from==stripslashes($gettemplateRow["to_mail"]); 
			$from=stripslashes($gettemplateRow["from_email"]);
	        $to=stripslashes($gettemplateRow["to_mail"]);
		    //$to="jayapal.shenll@gmail.com";
	        $subject=stripslashes($gettemplateRow["subject"]);;
		    $message=stripslashes($gettemplateRow["message"]);
			$message=str_replace('$businessname',$business_name,$message);
			 $message=str_replace('#mar_logo#',"<img src='http://www.myappyrestaurant.com/images/mail.png' title='MAB logo' alt='MAB logo'>",$message);
			sendEmail($from,$to,$subject,$message);

       header("Location:index.php");
	    exit;
    }
}
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>My Appy Restaurant</title>
	<link rel="shortcut icon" href="images/Fav.ico" type="image/ico">
	<link href="stylesheet.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="lib/jquery-1.10.1.min.js"></script>
	<!-- Add fancyBox main JS and CSS files -->
 
	<script type="text/javascript" src="jscript/validate.js"></script>
	<script type="text/javascript" src="jscript/image_loading.js"></script>
	<script type="text/javascript" src="source/jquery.fancybox.js?v=2.1.5"></script>
	<link rel="stylesheet" type="text/css" href="source/jquery.fancybox.css?v=2.1.5" media="screen" />
	<style>
	   .select_box{
	   display:none;
	
	}
       .card_class{
			padding-right:10px;
			cursor:pointer;
		}
		.copyright {
		   
			font-family: 'arial';
			color: grey;
			font-size: 11px;
			text-align: right;
			width: 790px;
			width:400px;
			margin-left:auto;
			
		}
		.special_image{
		 border:2px solid #ffc000;width:100px;height:100px;margin-left:10px;background-color:white;border-radius:10px;
		}
		.special_image_style1{
			width: 125px;
			float: right;
			border: 2px solid black;
			border-radius: 3px;
			height: 125px;
	   
	 }
	 
	 .submit_btn{
			background:#558ED5;
			font-family:'kristen_itcregular';
			border:1px solid #558ED5;
			border-radius:3px;
			padding:5px 20px 5px 20px;
			cursor:pointer;
			color:white;
		}

		body{
			margin:0px;
			padding:0px;
		}
		.phone_image{
		   float:left;
		   width:250px;
		}
		.phone_inline_image{
           height:435px;
		   margin-top:50px;
		}
        .right_div{
		float:right;
		margin-top:20px;
		border:1 px solid red;
		width:768px;
		
		}
        .right1{
		   width:100%;
		   float:left;
		   border:0px solid green;
		}
		.spl_heading{
          width:400px;
		}
         .spl_img{
		  width:100px;
		 }
		 .side_quote{
		     border: 7px solid #ffc000;
                     border-radius: 25px;			
                     text-align:justify;
		 }
		 .button{
		  width: 830px;
		  margin-left: 260px;
		  min-height: 25px;
		  padding-top: 20px;
		  padding-bottom: 10px;
		 
		 }
		 .input_field{
		font-family:'arial';
		border:1px solid #6193c9;
		background:#D9D9D9;
		border-radius:3px;
		width:400px;

	}
	.main_div{
	   width:1024px;
	   overflow:hidden;
	}
	#gettrila_btn1{
float:right;
	  margin-right:70px;	 
	
	}
		@media only screen 
   and (min-width :768px) 
   and (max-width :1024px) {
  .phone_image{
		   float:left;
		   width:23%;
		   
		}
		.phone_inline_image{
           height:395px;
		   margin-top:50px;
		}
		 .right_div{
		float:left;
		margin-top:20px;
		
		width:64%;
		
		}
		.right1{
		   width:100%;
		   float:left;
		   
		  
		  
		}
		.spl_heading{
          width:55%;
		  
		}
		.special_image{
		 border:2px solid #ffc000;width:90px;height:90px;margin-left:10px;background-color:white;border-radius:10px;
		}
		.special_image_style1{
			width: 90px;
			float: right;
			border: 2px solid black;
			border-radius: 3px;
			height: 90px;
	   
	 }
	 .input_field{
		font-family:'arial';
		border:1px solid #6193c9;
		background:#D9D9D9;
		border-radius:3px;
		width:230px;

	}
	   .button{
		width: 62.5%;
		margin-left: 237px;
		min-height: 25px;
		padding-top: 20px;
		padding-bottom: 10px;

		 
		 
		 }
		 
		 .main_div{
	   width:100%;
	   overflow:hidden;
	}
	#gettrila_btn1{
float:right;
margin-right:10px;	 
	
	}
}
</style>
<script type="text/javascript">

$(document).ready(function(){
	 $("input[type='text']:first", document.forms[0]).focus();
	 $('.fancybox').fancybox();
	 var emailPattern=/^[a-zA-Z0-9\-_]+(\.[a-zA-Z0-9\-_]+)*@[a-z0-9]+(\-[a-z0-9]+)*(\.[a-z0-9]+(\-[a-z0-9]+)*)*\.[a-z]{2,4}$/;
	var intPattern=/^-?\d\d*$/;
	var find=/^[\s()-]*([0-9][\s()-]*){6,20}$/;

	$("#validate").click(function(){
	var hidden=$("#hdn_logo").val();
	if(hidden==""){
		hidden=0;
	}
	var photoarr=$("#restaurant_logo").val().split(".");
	var photolen=photoarr.length;
	var extension=photoarr[photolen-1];

    if($.trim($("#restaurant_logo").val())==""&&hidden=='0'){
			alert("Please upload business logo");
			$("#restaurant_logo").focus();
			return false;
	}
	if($.trim($("#restaurant_logo").val())!=""&&extension!="jpg"&&extension!= "gif" && extension != "png" && extension !="bmp"
                    && extension != "jpeg" && extension!="jpg" && extension!="eps" && extension!="ai" && extension!="pdf"){
			alert("Please upload valid business logo");
			$("#restaurant_logo").focus();
			return false;
	}
	else if($.trim($("#app_name1").val())==""){
      alert("Please enter app name 1");
			$("#app_name1").focus();
			return false;



	}
	else if($.trim($("#app_name2").val())==""){
      alert("Please enter app name 2");
			$("#app_name2").focus();
			return false;

	}
	else if($.trim($("#app_name3").val())==""){
	    alert("Please enter app name 3");
	   $("#app_name3").focus();
	   return false;
   }
	else{

		setTimeout(function(){openFancyBox('Success_contact')},1000);
	    setTimeout(function(){$("#upload_logo_from").submit();},6000);
      
   }
   
  });
});
 </script>
</head>
<body style="background:url('images/Picture1.png') no-repeat;margin:0px;font-family:'kristen_itcregular';color:#8eb4e3;background-size:100% 87%;">
<div class="main_div"> 
<div class="phone_image">
	<img src="images/mobile_hand.png" class="phone_inline_image" >
</div>
<div  class="right_div">
		<div class="right1">
	
     <table  border="0" cellspacing="10" cellpadding="0" height="100%"  width="100%">
		
	   <tr>
			<td valign="middle"  height="25px" class="spl_heading" >
                               <div style="font-family: 'kristen_itcregular';color:#ffc000;margin-left:0px;font-size:60px;line-height: 57px;margin-top:22px;">
                                   upload logo
                               </div>
			 
		   </td>
			
	  </tr>
		<tr>
		    <td valign="top" colspan="2" style="height: 210px;">
			    <form name="upload_logo_from" id="upload_logo_from" method="post" enctype="multipart/form-data" style="color:white;font-size: 13px;font-family: 'calibri';">
				<input type="hidden" name="hdn_id" value="<?php echo $demo_id;?>">
				<input type="hidden" name="hdn_logo" id="hdn_logo" value="<?php echo $old_restaurant_logo;?>">
			
				<table cellspacing="5" cellpadding="0" border="0" style="font-size:13px;width:70%;">
			
			 <tr>
								<td valign="top" width="20%">
									Business Logo<font color="red">*</font>:
								</td>
								<td width="50%">
									<input type="file" name="restaurant_logo" id="restaurant_logo" class="inp_feild" value="<?php echo $restaurant_name;?>">
									<?php if(is_file($old_restaurant_logo)){ ?>
									<br>
										<img src="<?php echo $old_restaurant_logo?> " style="width:75px;height:75px;margin-left: 3px;">
									<?php }?>
								</td>
							</tr>
				<tr>
					<td class="td_class">App Name 1<font color="red">*</font>:</td>
					<td class="td_class"><input type="text" name="app_name1" id="app_name1" class="input_field"  tabindex="1" value="<?php echo $comma_separated[0];?>"></td>
                </tr>
				<tr>
					<td class="td_class">App Name 2<font color="red">*</font>:</td>
					<td class="td_class"><input type="text"  name="app_name2" id="app_name2" class="input_field"   tabindex="2" value="<?php echo $comma_separated[1];?>"></td>
               </tr>
				<tr>
					<td class="td_class">App Name 3<font color="red">*</font>:</td>
					<td class="td_class"><input type="text"   name="app_name3" id="app_name3" class="input_field"  tabindex="3" value="<?php echo $comma_separated[2];?>"></td>
              </tr>
			  <tr>
					<td class="td_class"></td>
					 <td class="td_class" >
					  <input type="button" value="< go back" class="submit_btn" id=""  onclick="document.location='index.php'" >
					  <input type="button" value="submit" class="submit_btn" id="validate" style="float:right;">
					
					</td>
              </tr>
              <tr>
				<td colspan="2" class="td_class" style="font-family:'arial';text-align:justify;">
				    <p>Regarding your logo: please provide your logo in the highest resolution possible. Preferred formats include EPS, AI, JPG, or PDF.</p>
				    <p> Regarding your App name: recommended that this is the same as your venue name. We cannot guarantee the availability of any App names on the App stores, so please provide 3 App name options (listed in order of preference). Also recommended that you provide at least one App name that is more specific (for example, instead of Joe's Italian Restaurant, perhaps try Joe's Italian Restaurant London, or Joe's Italian Restaurant Putney).</p>
				  </td>
              </tr>
          </table>
            



		    </form>

                
				</td>
			</tr>
		</table>
		 
	  </div>
	 
	</div>

	
 </div>
  <div id="Success_contact" style="display:none;width:500px;height:40px;">
		 <center>
		 <table cellpadding="0" cellspacing="0" border="0">
			<tr>
				<td>
					<div>
					<table>
					<tr>
					    <td style="color:#558ed5;font-size:22px;padding-left:10px;" valign="middle"><p>your details updated successfully.</p> </td></tr></table></div>
				    </td>
				</tr>
		</table>
	</center>
</div>
<div style="min-height:200px;background:url(images/footer_bg2.jpg);">
       
			<div style="width:400px;margin-left:auto;padding-bottom:10px;padding-top:95px;">
			   
				<img src="images/applogo.png">
			</div>
		<div class="copyright">
			<div style="width:283px;">
				Copyright &copy; My Appy Restaurant 2013 All rights reserved
			<div>
		</div>
	</div>
	
<div style="position:absolute; top:0px; right:0px;">


<div id="label" style="height: 32px;position: absolute;float: right;margin-left: 116px;margin-top: 2px;width: 75px;font-size: 27px;text-align: center;color: yellow;"></div>
<img src="images/sun.png"  id="showmenu" style="border:none;"></div>

</body>
</html>
	
	
	