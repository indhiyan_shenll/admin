<?php
include("../includes/configure.php");
include("includes/cmm_functions.php");
include("../includes/session_check.php");

//Page condition -- Start here
if(isset($_POST['HdnPage']) && $_POST['HdnPage']!="" && $_POST['HdnPage']!="0")
	$Page=$_POST['HdnPage'];
else
	$Page=1;
//Page condition -- End here

//Search condition -- Start here
$condition="";
if(isset($_POST["hdn_mode"]) && ($_POST["hdn_mode"]=="search" || $_POST["hdn_mode"]=="export"))
{
	$transaction_id	=trim(mysql_real_escape_string($_POST['transaction_id']));
	$affiliate		=$_POST['affiliate'];
	$licensee		=$_POST['licensee'];
	$status			=$_POST['status'];
	if(!empty($transaction_id))
       $condition.="and transaction_id='".$transaction_id."'";
	if(!empty($affiliate))
       $condition.="and a_id='".$affiliate."'";
	if(!empty($licensee))
       $condition.="and l_id='".$licensee."'";
	if(!empty($status))
       $condition.="and payment_status='".$status."'";
}
//Search condition -- End here

//Export paypal data -- Start here
if(isset($_POST["hdn_mode"]) && $_POST["hdn_mode"]=="export")
{
	$heading = "TransactionID\tTransaction Date\tCustomerID\tAmount\tCurrency\tAffiliate\tLicensee\tPayment Status\n";
	$sqlqry	="select transaction_id,DATE_FORMAT(transaction_date,'%d/%m/%Y') as transaction_date,profile_id,amount,currency,a_id,l_id,payment_status from tbl_report_paypal where demo_id<>'' and last_upload_flag='1' $condition order by demo_id desc,report_id desc";
	$sql	=mysql_query($sqlqry);
	if(mysql_num_rows($sql)>0)
	{
		while($fetch=mysql_fetch_array($sql))
		{
			$transaction_id		=stripslashes(trim($fetch["transaction_id"]));
			$transaction_date	=stripslashes(trim($fetch["transaction_date"]));
			$profile_id			=stripslashes(trim($fetch["profile_id"]));
			$amount				=stripslashes(trim($fetch["amount"]));
			$currency			=stripslashes(trim($fetch["currency"]));
			$a_id				=stripslashes(trim($fetch["a_id"]));
			$l_id				=stripslashes(trim($fetch["l_id"]));
			$payment_status		=stripslashes(trim($fetch["payment_status"]));
			$affiliate			=funreturnfullname("tbl_affiliate","full_name","where affiliate_id='".$a_id."'");
			$licensee			=funreturnfullname("tbl_licensee","full_name","where licensee_id='".$l_id."'");

			$data.=$transaction_id."\t".$transaction_date."\t".$profile_id."\t".$amount."\t".$currency."\t".$affiliate."\t".$licensee."\t".$payment_status."\n";
		}
	}
	
	$Resultdata=$heading.$data;
	$XLheading="Paypal_Report_";
	$strDirname = "importdata/";
	$strFileName =$XLheading.time().".xls";
	$strXlFileNm = $strDirname.$strFileName;
	$rhandle= @fopen($strXlFileNm,"r");
	$readed_contents = @fread($rhandle,filesize($strXlFileNm));
	@fclose($rhandle);

	$readed_contents=$Resultdata;
	$strXlFileNametoDwnld = $strDirname.$strFileName;
	$handle = fopen($strXlFileNametoDwnld,'w');
	fwrite($handle,$readed_contents);
	fclose($handle);

	if(is_file($strXlFileNametoDwnld))
	{
		$contents = '';
		$handle=@fopen($strXlFileNametoDwnld,"r");
		if($handle)
		{
			$contents = fread($handle, filesize($strXlFileNametoDwnld));
			fclose($handle);
		}
		header("Content-Type: application/x-octet-stream");
		header("Content-Disposition: attachment; filename=$strFileName");
		header("Content-Length:".filesize($strXlFileNametoDwnld));
		print $contents;
		unlink($strXlFileNametoDwnld);
	}
}
//Export paypal data -- End here

include("includes/header.php");
?>
 <body>
 <script>
 function searchoption()
 {
	if($.trim($("#transaction_id").val())==""&&$.trim($("#affiliate").val())==""&&$.trim($("#licensee").val())=="" &&$.trim($("#status").val())=="" ){
		alert('Please fill atleast one field to search');
		document.getElementById("transaction_id").focus();
		 return false;
	}
	else{
		
		$("#hdn_mode").val("search");
		$("#demo_list").submit();
	}
}
 </script>
<div>
	<div style="margin-left:auto;margin-right:auto;">
		<div class="content">
			<div class="list_content" style="width:1270px">
				<div class="form_actions" style="padding-bottom:50px;">
					<input type="button" value="Back To Admin Features" class="add_btn" onclick="document.location='admin_features.php'" style="float:left;">							
				</div>
				<div style="color:white;font-size:13px;font-weight:bold;">
					<form name="demo_list" id="demo_list" method="post">
					<input type="hidden" name="hdn_mode" id="hdn_mode" value="">
					<input type="hidden" name="HiddenMode" id="HiddenMode" value="">
					<input type="hidden" name="HdnPage" id="HdnPage" value="">
					<!-- Search Filter Display Start here -->
					<table cellspacing="0" cellpadding="0" width="100%" border="0" style="border:1px solid #D9D9D9;" >
						<tr height="30px;" BGCOLOR="#ff4215">
							<td colspan="8" align="left"><span style="">&nbsp;&nbsp;Filter Paypal Report</span></td>
						</tr>
						<tr height="50px;">
							<td colspan="8" align="center">    
								<table style="table-layout: fixed;width:1240px;margin:auto">
									<tr>
										<td></td>
										<td align="right">TransactionID:</td>
										<td align="left">
										      <input type="text" name="transaction_id" id="transaction_id" class="inp_feild" value="<?php echo stripslashes($transaction_id);?>"  >
										</td>
									    <td align="right">Affiliate:</td>
										<td align="left">
											<select name="affiliate" id="affiliate" class="inp_feild"  >
												<option value="">Select</option>
												<?php
												$sqlqry="select * from  tbl_affiliate order by affiliate_id asc";
												$prepqry=$DBCONN->prepare($sqlqry);
												$prepqry->execute();
												while($getRow=$prepqry->fetch()){
												?>
												<option value="<?php echo stripslashes($getRow["affiliate_id"]);?>"<?php if($affiliate==$getRow["affiliate_id"]) echo " selected";?>><?php echo stripslashes($getRow["full_name"]);?></option>
												<?php
												}
												?>
											</select>
										</td> 
										<td align="right">Licensee:</td>
									    <td align="left">
											<select name="licensee" id="licensee" class="inp_feild"  >
											<option value="">Select</option>					
											<?php
											$sqlqry="select * from  tbl_licensee order by licensee_id asc";
											$prepqry=$DBCONN->prepare($sqlqry);
											$prepqry->execute();
											while($getRow=$prepqry->fetch()){
											?>
											<option value="<?php echo $getRow["licensee_id"];?>" <?php if($licensee==$getRow["licensee_id"]) echo " selected";?>><?php echo $getRow["full_name"];?></option>
											<?php
											}
											?>
											</select>
									    </td>
										<td align="right">Payment Status:</td>	
										<td align="left">
											<select name="status" id="status" class="inp_feild" >
												<option value="">Select</option>					
												<?php
												$sqlqry="select distinct(payment_status) as payment_status from  tbl_report_paypal";
												$prepqry=$DBCONN->prepare($sqlqry);
												$prepqry->execute();
												while($getRow=$prepqry->fetch()){
												?>
												<option value="<?php echo $getRow["payment_status"];?>" <?php if($status==$getRow["payment_status"]) echo " selected";?>><?php echo $getRow["payment_status"];?></option>
												<?php
												}
												?>
											</select>
										</td>	
										<td></td>
									</tr>
								</table>
							</td>
						</tr>
						<tr height="40px;">
							<td colspan="8" align="center" class="form_actions">
								<input type="button" class="add_btn"  value="Search" onclick="searchoption()" style="width:90px;margin-right: 8px;">&nbsp;&nbsp;<input type="button" class="add_btn"  value="Export" onclick="funexport('demo_list')" style="width:90px;margin-right: 8px;"><input type="button" value="Cancel" class="add_btn" onclick="document.location='paypal_report.php'" style="width:90px;margin-left: 8px;">
							</td>	
						</tr>
					</table>
					<!-- Search Filter Display End here -->
				</div>
				<!-- Listing Records Start here -->
				<div class="gap1">
					<div style="height:50px;"></div>
					<div class="header_div">
						<table cellspacing="0" cellpadding="0" width="100%" class="tbl_header" style="">
							<tr>
							<th style="width:100px;">Transaction ID</th>
							<th style="width:100px;">Transaction Date&nbsp;&nbsp;</th>
							<th style="width:100px;">Customer ID&nbsp;&nbsp;</th>									
							<th style="width:100px;">Amount</th>
							<th style="width:100px;">Currency</th>
							<th style="width:100px;">Affiliate</th>
							<th style="width:100px;">Licensee</th>
							<th style="width:115px;">Payment Status</th>
							</tr>
						</table>
					</div>
				</div>
				<table cellspacing="0" cellpadding="0" width="100%" class="tbl-body" border="0">
				<?php
				$qryCondition="";
				$getQry="select * from tbl_report_paypal where demo_id<>'' AND last_upload_flag='1' $condition order by demo_id desc,report_id desc";
				$prepgetQry=$DBCONN->prepare($getQry);
				$prepgetQry->execute();
				$count =$prepgetQry->rowCount();
				if($count>0)
				{
					$records_perpage=100;
					$TotalRecords	=$count;
					if($TotalRecords <= (($Page * $records_perpage)-$records_perpage))
					$Page	=	$Page-1;
					$TotalPages		=	ceil($TotalRecords/$records_perpage);
					$Start			=	($Page-1)*$records_perpage;
					$getQry.=" limit $Start,$records_perpage";
					$prepgetQry=$DBCONN->prepare($getQry);
					$prepgetQry->execute();
					$count =$prepgetQry->rowCount();
					$sno=$Start+1;$rowno=1;
					if($count>0)
					{
						while($getRow=$prepgetQry->fetch())
						{
							$bgcolor=($rowno%2==1)?"#a5a5a5":"#d2d1d1";
				?>
					<tr bgcolor="<?php echo $bgcolor;?>">
						<td style="width:100px;"><?php echo stripslashes($getRow["transaction_id"]);?></td>
						<td style="width:100px;">
						<?php
							if($getRow["transaction_date"]!="") 
							{
								$date=fundateconversion($getRow["transaction_date"],"display");
								$date=($date!="01/01/1970")?$date:"";
							}
							echo $date;
						?>
						</td>	
						<td style="width:100px;"><?php echo stripslashes($getRow["profile_id"]);?></td>	
						<td style="width:100px;"><?php echo stripslashes($getRow["amount"]);?></td> 
						<td style="width:100px;"><?php echo stripslashes($getRow["currency"]);?></td>
						<td style="width:100px;"><?php echo funreturnfullname("tbl_affiliate","full_name","where affiliate_id='".$getRow["a_id"]."'"); ?></td>	
						<td style="width:100px;"><?php echo funreturnfullname("tbl_licensee","full_name","where licensee_id='".$getRow["l_id"]."'"); ?></td>	
						<td style="width:115px;"><?php echo stripslashes($getRow["payment_status"]);?></td> 
					</tr>
							<?php
							$rowno++;
							$sno++;
						}
						if($TotalPages > 1){
						echo "<tr><td align='center' colspan='8' valign='middle' class='pagination'>";
						if($TotalPages>1){
							$FormName = "demo_list";
						   include("../includes/paging.php");
						}
						echo "</td></tr>";
						}
					}
					else{
						echo "<tr style=\"background-color:#f6f6f6;text-align:center;\"><td colspan=\"8\">No Record(s) found.</td></tr>";
					}	
				}
				else{
					echo "<tr style=\"background-color:#f6f6f6;text-align:center;\"><td colspan=\"8\">No Record(s) found.</td></tr>";
				}
						?>
					<tr>
						<td colspan="8">
						<div class="form_actions" style="text-align:left;position:relative;">
						<input type="button" value="Back To Admin Features" class="add_btn" onclick="document.location='admin_features.php'">
						</td>
					</tr>
				</table>
				<!-- Listing Records End here -->
				</form>
			</div>
		</div>
	</div>
</div>
<?php
include("includes/footer.php");
?>
<script type="text/javascript">
/****function for paging statrs*******/
function pagetransfer(pagenumber,formname)
{	
	with(document.forms[formname])
	{ 
		HdnPage.value=pagenumber;
		HiddenMode.value="paging";
		submit();
	}
}
/****function for paging ends*******/

function funexport(formname)
{
	with(document.forms[formname])
	{
		$("#hdn_mode").val("export");
		$("#demo_list").submit();
		return true;
	}
}
</script>