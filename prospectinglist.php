
<?php

$parent_directory=basename(dirname($_SERVER["PHP_SELF"]));
$filename=basename($_SERVER["PHP_SELF"]);

include("../includes/configure.php");
include("includes/cmm_functions.php");
include("../includes/session_check.php");

/**************** Changes on 7-7-2015******************/
$set_search_array = array(
	"business_name"=>"Business Name",
	"first_name"=> "Full Name",
	"promo_code"=>"Promo Code",
	"business_type"=>"Business Type",
	"a_id"=>"Affiliate",
	"l_id"=>"Licensee",
	"country"=>"Country",
	"callstatus"=>"Call Status",
	"created_date"=>"Create Date"
);

/*****************************************************/

$id=$_SESSION['user_id'];
$dbdatetime=date('Y-m-d H:i:s',strtotime('now'));

$feat="trail_form";
$typeandfeature=checklogin($id,$feat);
$usrArr=explode("*",$typeandfeature);
$user_type=$usrArr[0];
$mfeature=$usrArr[1];
$trialfeature=$usrArr[4];
$masterfeature=$usrArr[5];

// Sorting column will be determined here -- Start here
if(isset($_GET["sort"]) && $_GET["sort"]!="")
	$sort=$_GET["sort"];
else 
	$sort="asc";

if(isset($_GET["field"]) && $_GET["field"]!="")
	$field = $_GET["field"];
else 
	$field = "import_date";

if($field=="import_date")
{
	$fieldname="import_date";
	$order=" order by ".$fieldname." ".$sort;
	if($sort=="asc"){
		$dsort="desc";
		$dpath="images/up.png";
	}
	else{
		$dsort="asc";
		$dpath="images/down.png";
	}
}
if($field=="business_type")
{
	$fieldname="business_type";
	$order=" order by ".$fieldname." ".$sort;
	if($sort=="asc"){
		$ddsort="desc";
		$ddpath="images/up.png";
	}
	else{
		$ddsort="asc";
		$ddpath="images/down.png";
	}
}
if($field=="bus_name")
{
	$fieldname="business_name";
	$order=" order by ".$fieldname." ".$sort;
	if($sort=="asc"){
		$rsort="desc";
		$rpath="images/up.png";
	}
	else{
		$rsort="asc";
		$rpath="images/down.png";
	}
}
 
if($field=="phone")
{
	$fieldname="phone";
	$order=" order by ".$fieldname." ".$sort;
	if($sort=="asc"){
		$usort="desc";
		$upath="images/up.png";
	}
	else{
		$usort="asc";
		$upath="images/down.png";
	}
}
if($field=="callbackdate")
{
	$fieldname="callback_date";
	$order=" order by ".$fieldname." ".$sort;
	if($sort=="asc"){
		$cdsort="desc";
		$cdpath="images/up.png";
	}
	else{
		$cdsort="asc";
		$cdpath="images/down.png";
	}
}
if($field=="calltime"){
	$fieldname="callback_time";
	$order=" order by ".$fieldname." ".$sort;
	if($sort=="asc"){
		$ssort="desc";
		$spath="images/up.png";
	}
	else{
		$ssort="asc";
		$spath="images/down.png";
	}
}
if(isset($_GET["mode"])){
	$unset=$_GET["mode"];
	if($unset=="unset"){
       unset($_SESSION["searchstrsession"]);
       unset($_SESSION["searchfieldstrsession"]);
       unset($_SESSION["sear_start_date_Session"]);
       unset($_SESSION["sear_end_date_Session"]);
	}
}
// Sorting column will be determined here -- Ends here

if(isset($_POST['HdnPage']) && $_POST['HdnPage']!="" && $_POST['HdnPage']!="0")
	$Page=$_POST['HdnPage'];
else if(!empty($_SESSION["page"]))
	$Page=$_SESSION["page"];
else
	$Page=1;

$_SESSION["page"]=$Page;


 if((isset($_POST['search_field']) && $_POST['search_field']!="" ) || (isset($_POST['search_start_date'])&&$_POST['search_start_date']!="") || (isset($_POST['search_end_date'])&&$_POST['search_end_date']!="") || (isset($_POST['srch_field_name'])&& $_POST['srch_field_name']!="")  ){
	unset($_SESSION["searchstrsession"]);
	unset($_SESSION["searchfieldstrsession"]);
	unset($_SESSION["sear_start_date_Session"]);
	unset($_SESSION["sear_end_date_Session"]);

	$searchfieldstrsession		=	mysql_real_escape_string($_POST['srch_field_name']);
	$searchfieldstr= $searchfieldstrsession;
    
    $searchstrsession  =	mysql_real_escape_string($_POST['search_field']);
	$searchstr = $searchstrsession;

	$sear_start_date_Session	=	clearIn($_POST['search_start_date']);
	$sear_start_date= $sear_start_date_Session;

	$sear_end_date_Session		=	clearIn($_POST['search_end_date']);
	$sear_end_date= $sear_end_date_Session;

	if($searchfieldstr == "a_id" || $searchfieldstr == "l_id"){
		
		if($searchfieldstr == "a_id"){
			$getALidQry = "select * from tbl_affiliate where full_name like '%".$searchstr."%'";
		}else{
			$getALidQry = "select * from tbl_licensee where business_name like '%".$searchstr."%'";
		}

		$getAidRes=mysql_query($getALidQry);
		$getAidCnt=mysql_num_rows($getAidRes);
		if($getAidCnt>0){

			$fet_alid_data	=	mysql_fetch_assoc($getAidRes);

			if(count($fet_alid_data)>0){

				if($searchfieldstr == "a_id"){
					$afli_id	=	$fet_alid_data['affiliate_id'];
					$searchcondn.= " and ".$searchfieldstr." = ".$afli_id;
				}else{
					$lic_id		=	$fet_alid_data['licensee_id'];
					$searchcondn.= " and ".$searchfieldstr." = ".$lic_id;
				}								
			}
			
		}
	}elseif($searchfieldstr == "created_date"){
		$searchstr	 = date("Y-m-d", strtotime($searchstr));
		$searchcondn.=" and ".$searchfieldstr." >='".$searchstr."'"; 
	}else{

		if($searchstr!="")	
		$searchcondn.=" and ".$searchfieldstr." like '%".$searchstr."%'";  
	}
	

	if($sear_start_date != "")	
		$searchcondn.=" and callback_date >= '".date('Y-m-d',strtotime($sear_start_date))."'";	

	if($sear_end_date != "")	
		$searchcondn.=" and callback_date <= '".date('Y-m-d',strtotime($sear_end_date))."'";

}

if($_SESSION["searchstrsession"]!="" || $_SESSION["searchfieldstrsession"]!="" || $_SESSION["sear_start_date_Session"]!="" || $_SESSION["sear_end_date_Session"]!=""){
    
	$searchfieldstr= !empty($_SESSION["searchfieldstrsession"]) ? $_SESSION["searchfieldstrsession"] : "" ;
	$searchstr = !empty($_SESSION["searchstrsession"]) ? $_SESSION["searchstrsession"] : "" ;
	$sear_start_date= !empty($_SESSION["sear_start_date_Session"]) ? $_SESSION["sear_start_date_Session"] : "" ;
	$sear_end_date= !empty($_SESSION["sear_end_date_Session"]) ? $_SESSION["sear_end_date_Session"] : "" ;

	if($searchfieldstr == "a_id" || $searchfieldstr == "l_id"){
		
		if($searchfieldstr == "a_id"){
			$getALidQry = "select * from tbl_affiliate where full_name like '%".$searchstr."%'";
		}else{
			$getALidQry = "select * from tbl_licensee where business_name like '%".$searchstr."%'";
		}

		$getAidRes=mysql_query($getALidQry);
		$getAidCnt=mysql_num_rows($getAidRes);
		if($getAidCnt>0){
			$fet_alid_data	=	mysql_fetch_assoc($getAidRes);

			if(count($fet_alid_data)>0){

				if($searchfieldstr == "a_id"){
					$afli_id	=	$fet_alid_data['affiliate_id'];
					$searchcondn.= " and ".$searchfieldstr." = ".$afli_id;
				}else{
					$lic_id		=	$fet_alid_data['licensee_id'];
					$searchcondn.= " and ".$searchfieldstr." = ".$lic_id;
				}								
			}
			
		}
	}elseif($searchfieldstr == "created_date"){
		$searchstr	 = date("Y-m-d", strtotime($searchstr));
		$searchcondn.=" and ".$searchfieldstr." >='".$searchstr."'"; 
	}else{
		if($searchstr!="")	
		$searchcondn.=" and ".$searchfieldstr." like '%".$searchstr."%'";  
	}
	

	if($sear_start_date != "")	
		$searchcondn.=" and callback_date >= '".date('Y-m-d',strtotime($sear_start_date))."'";	

	if($sear_end_date != "")	
		$searchcondn.=" and callback_date <= '".date('Y-m-d',strtotime($sear_end_date))."'";
 
}
// echo $searchcondn;

// Deleting from tbl_prospecting_list goes based on the seleceted checkbox
if(isset($_POST["dlete_mode"]) && !empty($_POST["delete_yes"]) && $_POST["delete_yes"]=='yes')
{
	$checkIds=$_POST["delete_id"];
	$delStr="";
	for($del=0;$del<count($checkIds);$del++)
	{
		if($delStr=="")
			$delStr = $checkIds[$del];
		else
			$delStr.= ','.$checkIds[$del];			
	}
	$deleteQry="delete from tbl_prospecting_list where special_one_id IN (".$delStr.")";
	$deleteRes=mysql_query($deleteQry);	
}


// Assigning multiple records to affiliates 
if(!empty($_POST["update_yes"]) && $_POST["update_yes"]=='yes')
{
	$checkIds = $_POST["update_id"]; // Selected Records from the list 
	$affiliate = $_POST["affiliate"]; // Selected Affiliate Id - stored in
	

	if($_POST["affiliate"] != "0" && $_POST["update_id"] != "")
	{
	     //$prspct_id = explode(",",$prospect_id_values); - Not Required
		$GetLicenseeIdForAffiliatQry = "select l_id from tbl_affiliate where affiliate_id='".$affiliate."'";
		$GetLicenseeIdForAffiliatRes = mysql_query($GetLicenseeIdForAffiliatQry);
		$GetLicenseeIdForAffiliatRow = mysql_fetch_array($GetLicenseeIdForAffiliatRes);
		$AffiliateLicenseeId = $GetLicenseeIdForAffiliatRow['l_id'];	

		
		 $SelectedProspects = implode(",", $checkIds);
		 $AssignProspectToAffiliateQry = "update tbl_prospecting_list set 
				a_id=:a_id, 
				l_id=:l_id,				
				modified_date=:modified_date 
				where find_in_set(cast(special_one_id as char), :result)";

		 $PrepareAssignProspectToAffiliate = $DBCONN->prepare($AssignProspectToAffiliateQry);
		 $AssignProspectToAffiliateResults = $PrepareAssignProspectToAffiliate -> execute( array(
					":a_id" => $affiliate,
					":l_id" => $AffiliateLicenseeId,
					":modified_date" => $dbdatetime,
					":result"=>$SelectedProspects)
					);
		 if($AssignProspectToAffiliateResults)
		 {
			header("Location:prospectinglist7_7.php");
			exit;
		 }

	}  

}

// Set the session to only show the logged in user my records
if(isset($_POST["my_records"]) && $_POST["my_records"] == 'yes')
{
	 $_SESSION['my_records'] = "yes";
}


if(isset($_GET["mode"]) && $_GET["mode"] == 'unset')
{
	$_SESSION['my_records'] = '';
}



if($user_type=="Agent") // If the logged in user type is Agent then change the condition here
{
	$usercondn = " and user_id = '".$_SESSION['user_id']."'";
}
else if($user_type=="Affiliate") // If the logged in user type is Affiliate then change the condition here
{
	$GetAffiliateIdQry = "select affiliate_id from tbl_affiliate where a_user_id='".$_SESSION['user_id']."'";
	$GetAffiliateIdRes = mysql_query($GetAffiliateIdQry);
	$GetAffiliateIdRow = mysql_fetch_array($GetAffiliateIdRes);
	$LoggedInAffiliateId = $GetAffiliateIdRow['affiliate_id'];	

	$usercondn = " and a_id = ".$LoggedInAffiliateId;
}
else if($user_type=="Licensee") // If the logged in user type is Licensee then  change the condition here
{
	$GetLicenceeId ="select licensee_id from  tbl_licensee where l_user_id = ".$_SESSION['user_id'];
	$PrepareGetLicenceeId=$DBCONN->prepare($GetLicenceeId);
	$PrepareGetLicenceeId->execute();
	$GetLicenceeIdRow=$PrepareGetLicenceeId->fetch();
	$LoggedInLicennseeId = $GetLicenceeIdRow['licensee_id'];
	
	$usercondn = " and l_id = ".$LoggedInLicennseeId;

	if(isset($_SESSION['my_records']) && $_SESSION['my_records'] == 'yes')
	{
		$GetAffiliateIdForLicenseeQry = "select affiliate_id from tbl_affiliate where a_user_id='".$_SESSION['user_id']."'";
		$GetAffiliateIdForLicenseeRes = mysql_query($GetAffiliateIdForLicenseeQry);
		$GetAffiliateIdForLicenseeRow = mysql_fetch_array($GetAffiliateIdForLicenseeRes);
		$AffiliateIdForLicensee = $GetAffiliateIdForLicenseeRow['affiliate_id'];	

		$usercondn .= " and a_id = '".$AffiliateIdForLicensee."'";
	}
}

else if($user_type=="Admin") // If the logged in user type is Admin then change the condition here
{
	//	if(isset($_SESSION['user_id']) && $_SESSION['user_id'] != "3"){
	//		//$usercondn = " and user_id != '".$_SESSION['user_id']."'";
	//		$usercondn = " and user_id = '".$_SESSION['user_id']."'";
	//	}

    if(isset($_SESSION['user_id']) && $_SESSION['user_id'] != "3" && $_SESSION['user_id'] != "173"){		
		// $usercondn = " and user_id != '".$_SESSION['user_id']."'";
		$usercondn = " and user_id = '".$_SESSION['user_id']."'";
	}

	if(isset($_SESSION['my_records']) && $_SESSION['my_records'] == 'yes')
	{
		$GetAffiliateIdForLicenseeQry = "select affiliate_id from tbl_affiliate where a_user_id='".$_SESSION['user_id']."'";
		$GetAffiliateIdForLicenseeRes = mysql_query($GetAffiliateIdForLicenseeQry);
		$GetAffiliateIdForLicenseeRow = mysql_fetch_array($GetAffiliateIdForLicenseeRes);
		$AffiliateIdForLicensee = $GetAffiliateIdForLicenseeRow['affiliate_id'];	

		$usercondn .= " and a_id = '".$AffiliateIdForLicensee."'";
	} 
}

$getDemoQry="select * from tbl_prospecting_list where special_one_id!='' ".$condition.$usercondn.$searchcondn.$order;

$_SESSION["searchfieldstrsession"]=!empty($searchfieldstrsession)?$searchfieldstrsession:$searchfieldstr;
$_SESSION["searchstrsession"]=!empty($searchstrsession)?$searchstrsession:$searchstr;
$_SESSION["sear_start_date_Session"]=!empty($sear_start_date_Session)?$sear_start_date_Session:$sear_start_date;
$_SESSION["sear_end_date_Session"]=!empty($sear_end_date_Session)?$sear_end_date_Session:$sear_end_date;
$getDemoRes=mysql_query($getDemoQry);
$getDemoCnt=mysql_num_rows($getDemoRes);

include("includes/header.php");
?>

<body <?php if($deleteRes){ ?>onload="redirect()"<?php }?>>
<form name="demo_list" id="demo_list" method="post" action="prospectinglist.php">
<script type="text/javascript" src="js/jquery.clearsearch.js"></script>
<script>
	$(function() {
			$('#search_field').clearSearch();
			$("#search_start_date").datetimepicker({ format:'d-m-Y',formatDate:'d-m-Y',timepicker:false});
			$("#search_end_date").datetimepicker({ format:'d-m-Y',formatDate:'d-m-Y',timepicker:false});
			
	});
	function checkallboxes(chkcnt){
		var opt=document.getElementById("checkall").checked;
		var i;
		for(i=1;i<=chkcnt;i++){
			document.getElementById("delete_"+i).checked=opt;
		}
	}
	function chkind(totcnt){
		var chkdcnt=0;
		for(i=1;i<=totcnt;i++){
			if(document.getElementById("delete_"+i).checked)
				chkdcnt++;
		}
		if(chkdcnt==totcnt)
			document.getElementById("checkall").checked=true;
		else
			document.getElementById("checkall").checked=false;
	}
	function chkselcnt(cnt){
		var chkdcnt=0;
		for(i=1;i<=cnt;i++){
			if(document.getElementById("delete_"+i).checked)
				chkdcnt++;
		}
		return chkdcnt;
	}
	function searchoption(){
       var businessname=$.trim($("#search_field").val());
	   var startdate=$.trim($("#search_start_date").val());
	   var enddate=$.trim($("#search_end_date").val());
	   var startdateday=startdate.split("-")[0];
	   var startdatemonth=startdate.split("-")[1];
	   var startdateyear=startdate.split("-")[2];
	   var enddateday=enddate.split("-")[0];
	   var enddatemonth=enddate.split("-")[1];
	   var enddateyear=enddate.split("-")[2];
	   startdate=new Date(startdateyear, startdatemonth-1, startdateday);
	   enddate=new Date(enddateyear, enddatemonth-1, enddateday);
	   if($.trim($("#search_field").val())==""&&$.trim($("#search_start_date").val())==""&&$.trim($("#search_end_date").val())==""){

           alert('Please fill atleast one field to search');
			document.getElementById("search_field").focus();
			 return false;
		}
		else if(startdate.getTime()>enddate.getTime())  {
			   alert('Please select end date is greater then or equal to start date');
			 
				document.getElementById("search_start_date").focus();
					return false;
	    }
		else{
				$("#demo_list").submit();
		}
	    
	}
	function getmyrecords(){
		document.getElementById('my_records').value='yes';
        $("#demo_list").submit();
	}
	</script>
	 				 
				<div class="content">				
					<div class="list_content">
						<div class="form_actions" style="width:1234px;">
							<?php
							if($masterfeature=="yes" || $user_type=="Affiliate" || $user_type=="Agent" || $user_type=="Licensee"  || $user_type=="Admin" ){
							?>
								  <input type="button" style="width:180px;" value="Add a Prospect" class="add_btn" onclick="document.location='prospectdetails.php'" ><br>
							<?php
							}
							if($masterfeature=="yes"){

							?>
								  <input type="button" style="width:180px;margin-top: 10px;" value="Go to Master List" class="add_btn" onclick="document.location='masterlist.php'">
							<?php
							 }
							?>
							<div style="width:153px;float:right;text-align:right;">
							         <input type="button" value="Filter List" class="add_btn" onclick="document.location='filterprospectlist.php'" style="display:none;"><br>
								    <input type="button" value="Remove Filters" class="add_btn" onclick="document.location='prospectinglist.php?mode=unset'" style="margin-top: 10px;"><br><br>
								    <span style="font-size:12px;font-weight:bolder;color:white;" id="demo_count">Total Prospects:&nbsp;<?php echo $getDemoCnt;?></span>								
							</div>
						    <div style="width:470px;float:right;text-align:right;padding-right:25px;color:white;font-size:13px;font-weight:bold;">
								
							<table cellspacing="0" cellpadding="0" width="100%">
							    
                                      <?php
								if($user_type=="admin"||$user_type=="Admin" || $user_type=="Licensee" || $id == "173"){
								?>
								 <tr>
									<td colspan="2" align="left">
									<div class="form_actions" style="float:left;text-align:right;margin-top:-50px;">
										<input type="button" value="My Records" class="add_btn"  onclick="getmyrecords()" >
									</div>
										</td>
										
								</tr>
								<?php
								}
								?>
								<tr height="22px;">
									<td colspan="2" align="left"><span style="text-decoration:underline;">Filter Prospect List</span>
									</td>
										
								</tr>
								<tr>
									<td width="90%">
										<table cellspacing="0" cellpadding="0" width="100%" border="0">
									<tr height="25px;">
									<td align="left">
										<select name="srch_field_name" id="srch_field_name" onchange="setSearchFilter(this.value)">
											<option value="" selected>Select</option>
											<?php 
											foreach($set_search_array as $src_key=>$src_value){

												$sel_sts = ($src_key == $searchfieldstr) ? "Selected" :"";
												
											?>
												 <option value="<?php echo $src_key;?>" <?php echo $sel_sts;?>><?php echo $src_value;?></option>
											<?php
											}
											?>
										</select>
									</td>
									<td colspan="2" id="search_string_result" style="text-align: left;">
										<input type="text" name="search_field" id="search_field" class="inp_feild" value="<?php echo $searchstr;?>" style="width:100%;" autocomplete="off">
									</td>
									</tr>
									<tr height="25px;">
											<td align="left">
												Callback Date:
											</td>
											<td align="left">
												Start:&nbsp;&nbsp;<input type="text" name="search_start_date" id="search_start_date" class="inp_feild"  value="<?php echo $sear_start_date;?>" style="width: 100px;">

											</td>
										   
											<td>
												End:&nbsp;&nbsp;<input type="text" name="search_end_date" id="search_end_date" class="inp_feild"  value="<?php echo $sear_end_date;?>" style="width:100px;">
											</td>
											
										</tr>
									</table>
									</td>
									<td>
										 <input type="button" value="Go" class="add_btn" style="min-width:40px;padding:0px;float:right;height: 43px;" onclick="searchoption()">
									</td>
									</tr>
									</table>
							</div>						
						</div>
						 <div style="padding-bottom:12px;margin-top:45px;">
						    <input type="hidden" name="HiddenMode" id="HiddenMode" value="">
							<input type="hidden" name="HdnPage" id="HdnPage" value="">
							<input type="hidden" name="dlete_mode" id="dlete_mode">
							<input type="hidden" name="delete_yes" id="delete_yes" value="">
							<input type="hidden" name="update_yes" id="update_yes">
							<input type="hidden" name="hdn_mode" id="hdn_mode">
							<input type="hidden" name="my_records" id="my_records">
					  </div>
						<table cellspacing="0" cellpadding="0" width="130%" class="tbl-body" border="0">
						<?php
						if($user_type=="Admin")
						{
						?>
						 <tr  style="height: 20px;">
						 <td align="right" colspan="11" style="">
						    <div style="width:100%;float:left;">
							  <div style="top:0px;left;display:one;" class="select_box">
                                 <table style="border:1px solid #2661a7;float:left;">
                                 <tr>
									<td> 
									  <select name="affiliate" id="affiliate" class="inp_feild" style="width:140px;">
										<option value="0">Select</option>
										<?php					

										$get_Qry="select * from tbl_affiliate where 1=1 ";
										
										$get_Res=mysql_query($get_Qry);
										while($get_value=mysql_fetch_array($get_Res))
										{											
											$TblAffiliateId = $get_value['affiliate_id'];
											?>
										<option value="<?php echo $TblAffiliateId;?>"><?php echo stripslashes($get_value["full_name"]);?></option>
										<?php
										}
										?>
									 </select>
                                   </td>
									<td>
									     <div class="form_actions" style="float:left;border:1 px solid red;width:35%;text-align:right;margin-top: -13px;">
												   <input type="button" value="Assign to affiliate" class="add_btn"   onclick="setvalue()">
												</div>
									
									</td>
                                 </tr>
                                 </table>
							
							</div>
							 
							 </td> 
						</tr>
						<?php	
						}
						$records_perpage=100;
						$TotalRecords	=	$getDemoCnt;
						if($TotalRecords <= (($Page * $records_perpage)-$records_perpage))
							$Page	=	$Page-1;
						$TotalPages		=	ceil($TotalRecords/$records_perpage);
						$Start			=	($Page-1)*$records_perpage;
						if($TotalPages>1)
						  {
						?>
						<tr style="height: 75px;">
							 
							<td align="center" colspan="11" style="">
								<?php
								$FormName="demo_list";
								include("../includes/paging.php");
								?>
							</td>
						</tr>
						<?php
						  }
						?>
					 
							<tr class="tbl_header" style="border-collapse: none;">
							       <?php
								    $style_design="cursor:pointer;border-top-left-radius: 10px;border-bottom-left-radius: 10px;";
									if($user_type=="Admin" && $_SESSION['user_id']=="3"){
									?>
									    <th style="<?php echo  $style_design;?>width:7%;"><center><span onclick="AsignRecordstoaffiliate('<?php echo $_SESSION['user_id'];?>')"> Status</span>&nbsp;<input type="checkbox" name="checkall_up" id="checkall_up"     onclick="checkallboxes1(document.getElementById('recordcnt').value)"></center></th>
                                        
									<?php
									$style_design="cursor:pointer;";
									}
									?>
									<th style="width:8%;<?php echo  $style_design;?>" onclick="document.location='prospectinglist7_7.php?sort=<?php echo $dsort;?>&field=import_date'">Create Date&nbsp;<?php if($dpath!=""){?><img src="<?php echo $dpath;?>" style="width:10px;height:10px;"><?php }?></th>
									<th style="cursor:pointer;width:9%;" onclick="document.location='prospectinglist7_7.php?sort=<?php echo $rsort;?>&field=bus_name'">Business Name&nbsp;&nbsp;<?php if($rpath!=""){?><img src="<?php echo $rpath;?>" style="width:10px;height:10px;"><?php }?></th>
									<th style="width:9%;" >Full Name&nbsp;&nbsp;</th>								 
									<th style="cursor:pointer;width:9%;" onclick="document.location='prospectinglist7_7.php?sort=<?php echo $ddsort;?>&field=business_type'">Business Type&nbsp;&nbsp;<?php if($ddpath!=""){?><img src="<?php echo $ddpath;?>" style="width:10px;height:10px;"><?php }?></th>
									<th style="width:8%;">Promo Code &nbsp;&nbsp;</th>
									<th style="width:9%;cursor:pointer;" onclick="document.location='prospectinglist7_7.php?sort=<?php echo $usort;?>&field=phone'">Phone&nbsp;&nbsp;<?php if($upath!=""){?><img src="<?php echo $upath;?>" style="width:10px;height:10px;"><?php }?></th>
									<th style="cursor:pointer;width:10%;" onclick="document.location='prospectinglist7_7.php?sort=<?php echo $pdsort;?>&field=call'">Call Status&nbsp;&nbsp;<?php if($pdpath!=""){?><img src="<?php echo $pdpath;?>" style="width:10px;height:10px;"><?php }?></th>
									<th style="cursor:pointer;width:10%" onclick="document.location='prospectinglist7_7.php?sort=<?php echo $ssort;?>&field=calltime'">Callback Time&nbsp;&nbsp;<?php if($spath!=""){?><img src="<?php echo $spath;?>" style="width:10px;height:10px;"><?php }?></th> 
                                    <th style="width:10%;">Affiliate&nbsp;&nbsp;</th> 
									<th style="width:4%;cursor:pointer;border-top-right-radius: 10px;border-bottom-right-radius: 10px;">
									<?php 
									if($user_type == "Admin")
									{
									?>
									<table width="100%">
										<tr>
											<td><span onclick="deleteRecords('<?php echo $user_type;?>')">Delete?</span></td>
											<td><input type="checkbox" name="checkall" id="checkall"onclick="checkallboxes(document.getElementById('recordcnt').value)"></td>
										</tr>
									</table>
									<?php } ?>
									</th>								 
								</tr>
								 
							<tr  class="gap">
								<td colspan="11" style=""></td>
							</tr>
						   <?php
							if($getDemoCnt>0){			
								$records_perpage=100;
								$TotalRecords	=	$getDemoCnt;
								if($TotalRecords <= (($Page * $records_perpage)-$records_perpage))
									$Page	=	$Page-1;
								$TotalPages		=	ceil($TotalRecords/$records_perpage);
								$Start			=	($Page-1)*$records_perpage;
								
								//code for paging ends
								$getDemoQry.=" limit $Start,$records_perpage";
								$getDemoRes = mysql_query($getDemoQry);
								$getDemoCnt = mysql_num_rows($getDemoRes);

								$i=1;
								$prospect_id_array=array();
								//$getDemoRow=mysql_fetch_array($getDemoRes);
								//print_r($getDemoRow);
								while($getDemoRow=mysql_fetch_array($getDemoRes)){
									$prospect_id=$getDemoRow["special_one_id"];
									$prospect_id_array[]=$getDemoRow["special_one_id"];
									$demo_id=$getDemoRow["demo_id"];
									if(!empty($demo_id)){
										$get_demourl_qry=mysql_query("select demo_page_url from tbl_demo where demo_id=$demo_id");
										$demo_pg_url=mysql_fetch_assoc($get_demourl_qry);
									}else
										$demo_pg_url['demo_page_url']='';;
									$import_date=stripslashes($getDemoRow["import_date"]);
									$callback_date=stripslashes($getDemoRow["callback_date"]);

									$Call_time=stripslashes($getDemoRow["callback_time"]);
									if($Call_time!="" && $Call_time!="00:00:00"){
									  $Call_time=date('g:i A',strtotime(stripslashes($getDemoRow["callback_time"])));
									  //$Call_time = stripslashes($getDemoRow["callback_time"]);
									}
									else{
									  $Call_time="";
									}
									$bdot='';
									if(strlen($getDemoRow["business_name"])>15)
							     	$bdot="...";
									//$business_name=stripslashes($getDemoRow["business_name"]);
									$suburb=stripslashes($getDemoRow["suburb"]);
									if($getDemoRow["business_type"]!="0"){
									  $business_type=stripslashes($getDemoRow["business_type"]);
									}
									else{
									 $business_type="";
									}
									
									$phone=stripslashes($getDemoRow["phone"]);
									$push_status=($getDemoRow["pushing_status"]);
								
									$arr2=explode("-",$getDemoRow["last_contact"]);
								  $contact=$arr2[2]."-".$arr2[1]."-".$arr2[0];
									if($contact==""||$contact=="00-00-0000")
									{
										$last_contact="";

									}
									else{
                                       $last_contact=$contact;
                                    }
									if($getDemoRow["callstatus"]!="0"){
										$callstatus=stripslashes($getDemoRow["callstatus"]);
										$getcolorQry="select * from  tbl_callstatus where callstatus='".$callstatus."'";
										$getcolorRes=mysql_query($getcolorQry);
										$getcolorRow=mysql_fetch_array($getcolorRes);
										$Status_color=stripslashes($getcolorRow["status_color"]);
										if($callstatus!=""&&$callstatus!="0"){
											$bgcolor='#'.$Status_color;

										}
										else{
												$bgcolor="white";
										}
									}
									else{
									  $callstatus="";
									
									}
									 
									$sales_person=stripslashes($getDemoRow["sales_person"]);
									
                                    $font_color='black';
									$stdot="";
									if(strlen($callstatus)>20)
										$stdot="...";
									if($bgcolor=="" || $bgcolor=="#"){
									  $bgcolor="white";
									}
							?>
							<tr style="background-color:<?php echo $bgcolor;?>">
							   <?php
						if($user_type=="Admin" && $_SESSION['user_id']=="3"){
						?>
							<td style="border-bottom:1px solid grey;color:<?php echo $font_color;?>"><center><input type="checkbox" name="update_id[]" value="<?php echo $getDemoRow["special_one_id"];?>" id="update_<?php echo $i;?>" onclick="chkind1('<?php echo $getDemoCnt;?>')" <?php if($demo_view_status=="1"){echo " checked";}?> ><center></td>
						<?php
						}
						?>
				<td style="border-bottom:1px solid grey;cursor:pointer;color:<?php echo $font_color;?>" onclick="edit_record('<?php echo $prospect_id;?> ');">
						  <?php if($import_date!=""){ echo   date("d-m-Y",strtotime($import_date));}?></td>
				<td style="border-bottom:1px solid grey;cursor:pointer;color:<?php echo $font_color;?>" onclick="edit_record('<?php echo $prospect_id;?> ');">
						  <?php echo readmore_view(stripslashes($getDemoRow["business_name"]),"15");?></td>
				<td style="border-bottom:1px solid grey;cursor:pointer;color:<?php echo $font_color;?>" onclick="edit_record('<?php echo $prospect_id;?> ');">
				         <?php echo   readmore_view(stripslashes($getDemoRow["first_name"]),"15");?></td>
				<td style="border-bottom:1px solid grey;cursor:pointer;color:<?php echo $font_color;?>" onclick="edit_record('<?php echo $prospect_id;?> ');">
				          <?php echo $business_type;?>
				</td>
				<td style="border-bottom:1px solid grey;cursor:pointer;color:<?php echo $font_color;?>" onclick="edit_record('<?php echo $prospect_id;?> ');">
				           <?php echo  readmore_view(stripslashes($getDemoRow["promo_code"]),"8");?>
				</td>
				<td style="border-bottom:1px solid grey;cursor:pointer;color:<?php echo $font_color;?>" onclick="edit_record('<?php echo $prospect_id;?> ');">
			        	<?php echo readmore_view($phone,"15");?> 
				</td>
				<td style="border-bottom:1px solid grey;cursor:pointer;color:<?php echo $font_color;?>" onclick="edit_record('<?php echo $prospect_id;?> ');">
				      <?php echo  readmore_view($callstatus,"18");?></td>
				<td align="center" style="border-bottom:1px solid grey;cursor:pointer;color:<?php echo $font_color;?>" onclick="edit_record('<?php echo $prospect_id;?> ');"><?php echo $Call_time;?></td>
				<td style="border-bottom:1px solid grey;cursor:pointer;color:<?php echo $font_color;?>" onclick="edit_record('<?php echo $prospect_id;?> ');">
								  <?php
									$geQry_db="select * from  tbl_affiliate where affiliate_id=:affiliate_id";
									$prepgetuserqry_db=$DBCONN->prepare($geQry_db);
									$prepgetuserqry_db->execute(array(":affiliate_id"=>$getDemoRow["a_id"]));
									$geUserRow_db=$prepgetuserqry_db->fetch();
									$affilat_name=stripslashes($geUserRow_db["full_name"]);
									if($affilat_name!=""){
									  $affilat_name=stripslashes($geUserRow_db["full_name"]);
									}
									else{
									 $affilat_name="";									
									}
								   echo readmore_view($affilat_name,"18");
								   ?>
						   </td>
						<td style="border-bottom:1px solid grey;color:<?php echo $font_color;?>">
						<table width="100%">
						<tr>
							<!-- <td><a href="prospectdetails.php?prospect_id=<?php echo $prospect_id;?>" style="color:<?php echo $font_color;?>">Edit</a></td> -->
							<td align="center">
							<?php 
							if($user_type == "Admin")
							{
							?>
							<input type="checkbox" name="delete_id[]" value="<?php echo $prospect_id;?>" id="delete_<?php echo $i;?>" onclick="chkind('<?php echo $getDemoCnt;?>')">
							<?php } ?>
							</td>
						</tr>
						</table>
						
						
						</td> 
					</tr>

						<?php
							$i++;
								}
								
								if($TotalPages>1)
								  {
								?>
								<tr>
									<td align="center" colspan="11" style="border-bottom:1px solid grey;">
										<?php
										$FormName="demo_list";
										include("../includes/paging.php");
										?>
									</td>
								</tr>
								<?php
								  }
								
							}
							else{
								echo "<tr bgcolor='#a5a5a5'><td colspan=\"11\" style=\"border-bottom:1px solid grey;text-align:center;\">No records to display</td></tr>";
							}
							echo "</table>";
							echo "<script>document.getElementById(\"demo_count\").innnerHTML=\"Demo Pages Count:&nbsp;$getDemoCnt\";</script>";
						?>
						
						<input type="hidden" name="recordcnt" id="recordcnt" value="<?php echo $getDemoCnt;?>">
						<input type="hidden" name="prospect_id_values" id="prospect_id_values" value="<?php echo implode(",",$prospect_id_array);?>">
						
					</div>
					<div style="height:50px;border:0px solid red;"></div>
				</div>
				
		</form>	
<?php
 include("includes/footer.php");
?>

<script>
function edit_record(val){
  document.location.href="prospectdetails7_7.php?prospect_id="+val;
}


function funexport()
{
	try
	{
		with(document.demo_list)
		{
			action='demolist-export.php';
			submit();
			return true;
			action='';
		}
	}
	catch(e)
	{
		alert(e)
	}
}

/****function for paging statrs*******/
function pagetransfer(pagenumber,formname)
{	
	with(document.forms[formname])
	{ 
		HdnPage.value=pagenumber;
		HiddenMode.value="paging";
		submit();
	}
}
/****function for paging ends*******/


function deleteRecords(usertype){
	try
	{
		with(document.demo_list)
		{
			if(usertype=="Admin"){
				var recordscnt=chkselcnt(document.getElementById("recordcnt").value);
				if(recordscnt==0){
					alert("Please select at least one prospect");
					return false;
				}
				else if(confirm('Are you sure want to delete these '+recordscnt+' prospects?'))
				{
					 document.getElementById('delete_yes').value='yes';
					submit();
				}else{
					document.getElementById("checkall").checked=false;
					var i;
					for(i=1;i<=recordscnt;i++){
						document.getElementById("delete_"+i).checked=false;
					}
				
				}


			}
			else{
				alert("You are not authorised to delete this item");
				return false;
			}
		}
	}
	catch(e)
	{
		alert(e)
	}
}

/****function for assign agents records  to affilite*******/
function AsignRecordstoaffiliate(userid){
	try
	{
		with(document.demo_list)
		{
			if(userid=="3"){
				var recordscnt=chkselcnt1(document.getElementById("recordcnt").value);
				 
				if(recordscnt==0){
					alert("Please select at least one record");
					return false;
				} 
				else{
				   $('.select_box').slideToggle("fast");
				
				}
			/*else if(confirm('Are you sure want to assign '+recordscnt+' records to  affiliate?'))
			{
					 
			}
			else{
					document.getElementById("checkall_up").checked=false;
					var i;
					for(i=1;i<=recordscnt;i++){
						document.getElementById("update_"+i).checked=false;
					}
				
				}*/
			}
			else{
				alert("You are not authorised to change this status");
				return false;
			}
		}
	}
	catch(e)
	{
		alert(e)
	}
}
function checkallboxes1(chkcnt){
		var opt=document.getElementById("checkall_up").checked;
		var i;
		for(i=1;i<=chkcnt;i++){
			document.getElementById("update_"+i).checked=opt;
		}
	}
    function chkind1(totcnt){
		var chkdcnt=0;
		for(i=1;i<=totcnt;i++){
			if(document.getElementById("update_"+i).checked)
				chkdcnt++;
		}
		if(chkdcnt==totcnt)
			document.getElementById("checkall_up").checked=true;
		else
			document.getElementById("checkall_up").checked=false;
	}
	function chkselcnt1(cnt){
		var chkdcnt=0;
		for(i=1;i<=cnt;i++){
			if(document.getElementById("update_"+i).checked)
				chkdcnt++;
		}
		return chkdcnt;
	}
  function setvalue()
  {
	   var val= document.getElementById('affiliate').value;
	   var recordscnt=chkselcnt1(document.getElementById("recordcnt").value);
	   
	  if(val=="0")
	  {
	        alert("Please select affilite");
			$("#affilite").focus();
			return false; 
	   }
	   else if (recordscnt <= 0)
	   {
			alert("Please select atleast 1 prospect to assign");
			//$("#affilite").focus();
			return false; 
	   }
	   else if(confirm('Are you sure want to assign '+recordscnt+' records to  affiliate?'))
	   {
		      document.getElementById('update_yes').value='yes';
			  document.getElementById('hdn_mode').value=val;
              $("#demo_list").submit();			   
	  }
	  /*else
	  {
		   $('.select_box').slideToggle("fast");		 
	  }*/
 }

function setSearchFilter(search_value){
	//search_value	=	$(this).val();
	if(search_value!=""){
		if(search_value == 'created_date'){
			$("#search_string_result").html('<input type="text" name="search_field" id="search_field" class="inp_feild" value="" style="width:100%;" autocomplete="off">');
			$("#search_field").datetimepicker({ format:'d-m-Y',formatDate:'d-m-Y',timepicker:false});
		}else{
			$.ajax({
				type : 'post',
				url : 'get_search_data.php', // in here you should put your query 
				async: false,
				data : 'srch_type='+ search_value, // here you pass your id via ajax .
				// in php you should use $_POST['post_id'] to get this value 
				success : function(response){
					if(response !='failure'){
						$("#search_string_result").html("");
						$("#search_string_result").html(response);
					}
					//console.log(response);
				}
		   });
		}
	}
}
</script>
<script>
	$(document).ready(function(){
		<?php if(!empty($searchfieldstr)){?>
			setSearchFilter("<?php echo $searchfieldstr; ?>");
			$("#search_field").val("<?php echo $searchstr; ?>");


		<?php }?>


	});

	
</script>
