<?php
include("../includes/configure.php");
include("includes/cmm_functions.php");
$msg=$_GET["msg"];
if(isset($_POST['Username'])){
	$Email=trim($_POST['Username']);
	//$Email
	$getQry="select * from tbl_users where email=:email";
	$prepgetQry=$DBCONN->prepare($getQry);
	$prepgetQry->execute(array(":email"=>$Email));
	//$result=mysql_query($getQry);		
	$count=$prepgetQry->rowCount();
	if ($count>0){
		$row=$prepgetQry->fetch();
		$username=stripslashes($row["username"]);
		$name=explode(" ",$username);
        $first_name=$name[0];
		$password=stripslashes($row["password"]);
		$templateqry="select * from   tbl_emailtemplates where title='Login Details'";
		$templateres=mysql_query($templateqry);
		$templaterow=mysql_fetch_array($templateres);
		$from=stripslashes($templaterow["from_email"]);
		$to=stripslashes($templaterow["to_mail"]);
		$subject=stripslashes($templaterow["subject"]);
		$message=stripslashes($templaterow["message"]);
        $loginpath=LOGIN_PATH;
		$message=str_replace('$firstname',$first_name,$message);
		$message=str_replace('$username',$username,$message);
		$message=str_replace('$password',$password,$message);
        $message=str_replace('$loginpath',$loginpath,$message);
		$message=str_replace('#mar_logo#',"<img src='http://www.myappyrestaurant.com/images/mail.png' title='MAB logo' alt='MAB logo'>",$message);
		sendEmail($from,$Email,$subject,$message);
		$_SESSION['emailmsg']='yes';
		header('location:login.php?msg=1');
		exit;
		
	}
	else{
		$ErrMsg = "Invalid email";
	}
}
 	
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<title>MAR Pipeline System</title>
		<link rel="shortcut icon" href="images/Fav.ico" type="image/ico">
		<link rel="stylesheet" type="text/css" href="css/style.css" />
		<meta name="Generator" content="EditPlus">
		<meta name="Author" content="">
		<meta name="Keywords" content="">
		<meta name="Description" content="">
		<style>
			body{
				margin:0;
				color:black;
				background:#2661a7;
				font-family:arial;
			}
			.header{
				height:80px;
				background:#1C242A;
			}
			.content{
				background:#455A68;
				min-height:600px;
			}
			
			.form_actions{
				padding-top:15px;
				padding-left:5px;
				padding-bottom:30px;
			}
			.form_actions .add_btn{
				cursor:pointer;
				border-radius:0px;
				background:#0D0D0D;
				color:#D9D9D9;
				border-color:#D9D9D9;
				padding:5px 15px 5px 15px;
				font-family:arial;
			}
			.list_content{
				/*width:950px;
				
				margin-left:40px;
				margin-left:auto;
				margin-right:auto;
				*/
				width:100%;
			}
			.tbl_header th{
				font-size:13px;
				border-bottom:1px solid #D9D9D9;
				text-align:left;
				font-family:arial;
				color:white;
			}
			.tbl-body{
				font-size:12px;
				line-height:25px;
				font-family:arial;
			}
			a{
				color:black;
			}
			
		</style>
	</head>
	<body>
		<div>
			<div >
				<div class="header" style="border-bottom: 1px solid white;">
					<img src="<?php echo HTTP_ROOT_FOLDER;?>images/applogo1.png" style="margin-top:20px;margin-left:40px;">
				</div>
				 <div class="list_content">
				 <?php
							if($msg!="")
								{
							?>
							<table style="margin:auto;">
									<tr style="background:white;height:40px;"id="error_message">
									<td style="color:black;font-size:20px;font-family:arial;margin-left:10px;" colspan="2">Your password has been send to registered email address</td>
								</tr>
							
							</table>
							<?php
								}
							?>
					<div class="container">
						<section id="content" style="margin:auto;">
						
							<form action="" method="post" name="forgotpassword_form" id="forgotpassword_form">
								<h1>Forgot Password</h1>
								<?php
								if($ErrMsg!=""){
									echo "<span style=\"color:red;\">".$ErrMsg."<span>";
								}
								?>
								<div>
									<input type="text" placeholder="Email" name="Username" required="" id="username"/>
								</div>
								<div>
									<!-- <input type="password" placeholder="Password" name="Password" required="" id="password" name="password"/> -->
								</div>
								<div>
									<input type="button" value="Get Password" style="width:150px;" onclick="forgotPassword()"/>
									<?php
									if($msg!="")
									{
										?>
									<input type="button" value="Login" style="width:150px;" onclick="document.location='login.php'"/>
									<?php
									}
									?>
								</div>
							</form><!-- form -->
							
						</section><!-- content -->
					</div><!-- container -->	
				</div>
			</div>
		</div>
	</body>
</html>
<script>
function forgotPassword(){
	var email=document.getElementById("username").value;
	if(email==""){
		alert("Please enter email");
		document.getElementById("username").focus();
		return false;
	}
	else{
		document.getElementById("forgotpassword_form").submit();
	}
}
</script>
<?php
if($msg!="")
{
	?>
	<script>setTimeout(function(){document.getElementById("error_message").style.display="none";},5000);</script>
	<?php
	
}
?>