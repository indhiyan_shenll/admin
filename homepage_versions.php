<?php
include("../includes/configure.php");
include("../includes/session_check.php");
$homepageversion_id=$_GET["homepageversion_id"];
$sort=$_GET["sort"];
$field=$_GET["field"];

if($field=="homepageversion"){
	$fieldname="homepageversion";
	$order=" order by ".$fieldname." ".$sort;
	if($sort=="asc"){
		$nsort="desc";
		$npath="images/up.png";
	}
	else{
		$nsort="asc";
		$npath="images/down.png";
	}
}

if($homepageversion_id!=""){
	$deleteQry="delete from tbl_homepageversions where homepageversion_id='".$homepageversion_id."'";
	$deleteRes=mysql_query($deleteQry);
	if($deleteRes){
		header("Location:homepage_versions.php");
		exit;
	}
}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<title>MAR Pipeline System</title>
		<link rel="shortcut icon" href="images/Fav.ico" type="image/ico">
		<meta name="Generator" content="EditPlus">
		<meta name="Author" content="">
		<meta name="Keywords" content="">
		<meta name="Description" content="">
		<style>
			body{
				margin:0;
				color:black;
				background:#455A68;
				font-family:arial;
			}
			.header{
				height:70px;
				background:#1C242A;
			}
			.content{
				background:#455A68;
				min-height:600px;
			}
			
			.form_actions{
				padding-top:15px;
				padding-left:5px;
				padding-bottom:30px;
			}
			.form_actions .add_btn{
				cursor:pointer;
				border-radius:0px;
				background:#0D0D0D;
				color:#D9D9D9;
				border-color:#D9D9D9;
				padding:5px 15px 5px 15px;
				font-family:arial;
			}
			.list_content{
				width:950px;
				margin-left:40px;
				/*
				margin-left:auto;
				margin-right:auto;
				*/
			}
			.tbl_header th{
				font-size:13px;
				border-bottom:1px solid #D9D9D9;
				text-align:left;
				font-family:arial;
				color:white;
			}
			.tbl-body{
				font-size:12px;
				line-height:25px;
				font-family:arial;
			}
			a{
				color:black;
			}
		</style>
	</head>
	<body>
		<div>
			<div style="margin-left:auto;margin-right:auto;">
				<div class="header">
					<span style="float:right;margin-right:20px;margin-top:5px;"><a href="logout.php" style="color:white;text-decoration:none;">Logout</a></span>
					<img src="images/myappyrestaurants.png" style="margin-top:10px;margin-left:40px;">
				</div>
				<div class="content">
					<div class="list_content">
						<div class="form_actions" style="padding-bottom:45px;">
							<input type="button" value="Back To Admin Features" class="add_btn" onclick="document.location='admin_features.php'" style="float:left;">
							<input type="button" value="Add Homepage Version" class="add_btn" onclick="document.location='edit_homepage_version.php'" style="float:right;">
						</div>
						<div style="padding-bottom:12px;">
							<table cellspacing="0" cellpadding="0" width="100%" class="tbl_header">
								
								<tr>
									<th width="10%">No</th>
									<th width="80%" onclick="document.location='homepage_versions.php?sort=<?php echo $nsort;?>&field=homepageversion'" style="cursor:pointer">Homepage Version&nbsp;&nbsp;<?php if($npath!=""){?><img src="<?php echo $npath;?>" style="width:10px;height:10px;"><?php }?></th>
									<th width="10%">Delete?</th>
								</tr>
							</table>
						</div>
						<table cellspacing="0" cellpadding="0" width="100%" class="tbl-body">
						<?php
							$gethomepageVersQry="select * from tbl_homepageversions".$order;
							$gethomepageVersRes=mysql_query($gethomepageVersQry);
							$gethomepageVersCnt=mysql_num_rows($gethomepageVersRes);
							if($gethomepageVersCnt>0){
								$i=1;
								while($gethomepageVersRow=mysql_fetch_array($gethomepageVersRes)){
									
									if($i%2==1){
										$bgcolor="#a5a5a5";
									}
									else{
										$bgcolor="#d2d1d1";
									}
						?>
							<tr bgcolor="<?php echo $bgcolor;?>">
								<td width="10%"><?php echo $i;?></td>
								<td width="80%"><?php echo stripslashes($gethomepageVersRow["homepageversion"]);?></td>
								<td width="10%"><a href="edit_homepage_version.php?homepageversion_id=<?php echo $gethomepageVersRow["homepageversion_id"];?>">Edit</a>&nbsp;&nbsp;/&nbsp;&nbsp;<a href="homepage_versions.php?homepageversion_id=<?php echo $gethomepageVersRow["homepageversion_id"];?>" onclick="return confirm('Are you sure want to delete this homepage version?')">Delete</a></td>
							</tr>
							
						<?php
							$i++;
								}
								?>
							<tr><td height="10px"></td></tr>
							
								<?php
							}
							else{
								echo "<tr bgcolor='#a5a5a5'><td colspan=\"3\"><center>No homepage version(s) found.</center></td></tr>";
							}
						?>
						<tr>
								<td colspan="3">
								<div class="form_actions" style="text-align:left;position:relative;">
								<input type="button" value="Back To Admin Features" class="add_btn" onclick="document.location='admin_features.php'">
								</td>
							</tr>
						</table>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>