<?php
include("../includes/configure.php");
include("../includes/session_check.php");
$id=$_GET["take_id"];
$dbdatetime=date('Y-m-d H:i:s',strtotime('now'));
if($id!=""){
	//$id
	$getQry="select * from  tbl_take_away where take_away_id=:id";
	$prepgetQry=$DBCONN->prepare($getQry);
	$prepgetQry->execute(array(":id"=>$id));
	//$getRes=mysql_query($getQry);
	$getRow=$prepgetQry->fetch();
	$take_away=stripslashes($getRow["take_away"]);
	$mode="Edit";
	$value="Update";
}
else{
	$mode="Add";
	$value="Create";
}
if(isset($_POST["take"])){
	$takeaway=addslashes(trim($_POST["take"]));
    if($mode=="Edit"){
		//$takeaway
		$updateQry="update tbl_take_away set take_away=:takeaway,modified_date=:modified_date where take_away_id=:id";
		$prepupdateQry=$DBCONN->prepare($updateQry);
		$updateRes=$prepupdateQry->execute(array(":takeaway"=>$takeaway,":modified_date"=>$dbdatetime,":id"=>$id));
		//$updateRes=mysql_query($updateQry);
		if($updateRes){
			header("Location:take_list.php");
			exit;
		}
	}
	else{
		$insertQry="insert into  tbl_take_away(take_away,added_date,modified_date) values(:takeaway,:added_date,:modified_date)";
		$prepinsertQry=$DBCONN->prepare($insertQry);
		$insertRes=$prepinsertQry->execute(array(":takeaway"=>$takeaway,":added_date"=>$dbdatetime,":modified_date"=>$dbdatetime));
		//$insertRes=mysql_query($insertQry);
		if($insertRes){
			header("Location:take_list.php");
			exit;
		}
	}
}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<title>MAR Pipeline System</title>
		<link rel="shortcut icon" href="images/Fav.ico" type="image/ico">
		<meta name="Generator" content="EditPlus">
		<meta name="Author" content="">
		<meta name="Keywords" content="">
		<meta name="Description" content="">
		<script type="text/javascript" src="js/jquery.js"></script>
		<script>
		var mode="<?php echo $mode;?>";
		</script>
		<script type="text/javascript" src="js/validate.js"></script>
		<style>
			body{
				margin:0;
				color:#D9D9D9;
				background:#455A68;
				font-family:arial;
			}
			.header{
				height:70px;
				background:#1C242A;
			}
			.content{
				background:#455A68;
				min-height:600px;
			}
			
			.form_actions{
				padding-top:15px;
				padding-left:5px;
				padding-bottom:30px;
			}
			.form_actions .add_btn{
				cursor:pointer;
				border-radius:0px;
				background:#0D0D0D;
				color:#D9D9D9;
				border-color:#D9D9D9;
				padding:5px 15px 5px 15px;
				font-family:arial;

			}
			.list_content{
				width:950px;
				margin-left:40px;
				/*margin-left:auto;
				margin-right:auto;*/
			}
			.tbl_header th{
				font-size:13px;
				border-bottom:1px solid #D9D9D9;
				text-align:left;
				font-family:arial;
			}
			.tbl-body{
				font-size:12px;
				font-family:arial;
			}
			a{
				color:black;
			}
			.inp_feild{
				border-radius:2px;
				border:none;
				width:100%;
			}
		</style>
	</head>
	<body>
		<div>
			<div style="margin-left:auto;margin-right:auto;">
				<div class="header">
					<span style="float:right;margin-right:20px;margin-top:5px;"><a href="logout.php" style="color:white;text-decoration:none;">Logout</a></span>
					<img src="images/myappyrestaurants.png" style="margin-top:10px;margin-left:40px;">
				</div>
				<div class="content">
					<div class="list_content">
						<h1 style="font-size:25px;padding-top:15px;padding-bottom:15px;margin:0px;">New 
						Take-away</h1>
						<form name="take_form" id="take_form" method="post" enctype="multipart/form-data">
						<input type="hidden" name="take_form_id" id="take_form_id" value="<?php echo $id;?>">
						<table cellspacing="15" cellpadding="0" border="0" width="70%">
							<tr>
								<td style="width:138px;">
									Take-away:
								</td>
								<td>
									<input type="text" name="take" id="take" class="inp_feild" value="<?php echo $take_away;?>">
								</td>
							</tr>
							
							<tr>
							     <td>
									<div class="form_actions" style="text-align:left;">
										<input type="button" value="Back To Take-away List" class="add_btn"  onclick="document.location='take_list.php'">
									
								</td>
								<td>
									  <div class="form_actions" style="text-align:right;">
										<input type="button" value="<?php echo $value;?> Take-away" class="add_btn" id="add_take">
									</div>
								</td>
							</tr>
						</table>
						</form>
					</div>
					
				</div>
			</div>
		</div>
	</body>
</html>