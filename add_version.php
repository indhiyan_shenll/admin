<?php
include("../includes/configure.php");
include("../includes/session_check.php");
$version_id=$_GET["version_id"];
$dbdatetime=date('Y-m-d H:i:s',strtotime('now'));
if($version_id!=""){
	
	$geItuneQry="select * from  tbl_version where version_id=:versionid";
	$prepgetItuneQry=$DBCONN->prepare($geItuneQry);
	$prepgetItuneQry->execute(array(":versionid"=>$version_id));
	$getItuneRow=$prepgetItuneQry->fetch();
	$version_name =stripslashes($getItuneRow["version_name"]);
	$version_des =stripslashes($getItuneRow["version_description"]);
	$mode="Edit";
	$value="Upadte";
}
else{
	$mode="Add";
	$value="Create";
}
if(isset($_POST["app_version"])){
	$app_version=addslashes(trim($_POST["app_version"]));
	$version_des=addslashes(trim($_POST["version_des"]));
    if($mode=="Edit"){
		//$Itune_name  $Ituneid
		$updateQry="update  tbl_version set version_name=:appversion,version_description=:appdes,modified_date=:modified_date where version_id=:version_id";
		$prepupdateQry=$DBCONN->prepare($updateQry);
		$updateRes=$prepupdateQry->execute(array(":appversion"=>$app_version,":appdes"=>$version_des,":modified_date"=>$dbdatetime,":version_id"=>$version_id));
		if($updateRes){
			header("Location:versionlist.php");
			exit;
		}
	}
	else{
		//$Itune_name
		$insertQry="insert into tbl_version(version_name,version_description,created_date,modified_date)values(:appversion,:appdes,:added_date,:modified_date)";
		$prepinsertQry=$DBCONN->prepare($insertQry);
		$insertRes=$prepinsertQry->execute(array(":appversion"=>$app_version,":appdes"=>$version_des,":added_date"=>$dbdatetime,":modified_date"=>$dbdatetime));
		if($insertRes){
			header("Location:versionlist.php");
			exit;
		}
	}
}
include("includes/header.php");
?>

	<body>
		<div>
			<div style="margin-left:auto;margin-right:auto;">
			  <div class="content">
					<div class="list_content">
						<h1 style="font-size:25px;padding-top:15px;padding-bottom:15px;margin:0px;">New 
						App Version</h1>
						<form name="version_form" id="version_form" method="post" enctype="multipart/form-data">
						<input type="hidden" name="hdn_version_id" id="hdn_version_id" value="<?php echo $version_id;?>">
						<table cellspacing="15" cellpadding="0" border="0" width="70%">
							<tr>
								<td style="width:138px;">
									App Version<font color="red">*</font>:
 
								</td>
								<td>
									<input type="text" name="app_version" id="app_version" class="inp_feild" value="<?php echo $version_name?>">
								</td>
							</tr>
							<tr>
								<td style="width:138px;">
								Version Description<font color="red">*</font>:
 
								</td>
								<td>
									<textarea name="version_des" id="version_des" class="inp_feild" cols="50" rows="5"><?php echo $version_des?></textarea>
								</td>
							</tr>
							<tr>
							     <td>
									<div class="form_actions" style="text-align:left;">
										<input type="button" value="Back To Version List" class="add_btn"  onclick="document.location='versionlist.php'">
									
								</td>
								<td>
									  <div class="form_actions" style="text-align:right;">
										<input type="button" value="<?php echo $value;?> App Version" class="add_btn" id="add_versionlist">
									</div>
								</td>
							</tr>
						</table>
						</form>
					</div>
					
				</div>
			</div>
		</div>
<?php
include("includes/footer.php");
?>