<?php
$dirname=basename(dirname($_SERVER["PHP_SELF"]));
include("../includes/configure.php");
$msg=$_GET["msg"];
$file=$_GET["file"];
$root=$_GET["root"];
if($root=="apprestaurant"){
	$path="../".$file;
}
else{
	$path=$file;
}
if($file==""&&$root==""){
	header("Location:masterlist.php");
	exit;
}
if(isset($_POST['Username'])){
	$UserName=trim($_POST['Username']);
	$Password=trim($_POST['Password']);
	$getQry="select * from tbl_users where username=:username and password=:password";
	$get_user=$DBCONN->prepare($getQry);
	$res=$get_user->execute(array(':username'=>$UserName,':password'=>$Password));
	$count=$get_user->rowCount();
	if ($count>0){
		$get_row=$get_user->fetchAll();
		$row=$get_row[0];
		$_SESSION['user_id']=$row['user_id'];
		$_SESSION['user_name']=stripslashes($row['username']);
		$userType=$row['user_type'];
		$aff_type=$row['affiliate_type'];
		$Arraffilatetype=array("1"=>"Non Loreal","2"=>"Loreal");
		$AffilateType=$Arraffilatetype[$aff_type];
		$_SESSION['user_type']=$row['user_type'];
		$_SESSION['affiliate_type']=$AffilateType;
		if($userType=='Admin' || $userType=='admin'){
				header('Location:admin_features.php');
				exit;
		}
		if($userType=='Affiliate' && $AffilateType=="Loreal"){
			header('Location:loreal_masterlist.php');
			exit;
		}
		if($userType=='Agent' || $userType=='Affiliate' || $userType=='agent' || $userType=='affiliate' || $userType=='Licensee' || $userType=='licensee'){
			    header('Location:prospectinglist.php');
				exit;
		}
		if($userType=='Developer' || $userType=='developer' || $userType=='CSM' || $userType=='DEM'|| $userType=='csm' || $userType=='dem'){
			header('Location:masterlist.php');
			exit;
		}
         



		/*if($userType=='user'|| $userType=='User'){
			if($row['demo_page']=="yes"){
				header('Location:prospectinglist.php');
				exit;
			}
		}
		else if($userType=='developer' || $userType=='Developer'){
			if($row['master_list']=="yes"){
				header('Location:masterlist.php');
				exit;
			}
		}
		else if($userType=='Admin'){
			header('Location:admin_features.php');
			exit;
		}
		if($row['trail_form']=="yes" || $userType=='Agent'){
           if($userType=='Agent'){
              	header('Location:prospectinglist.php');
				exit;
		   }
		   else{
		     header('Location:../trialform.php');
			exit;
		   
		   }

			
		}
		if($row['demo_page']=="yes"){
			header('Location:newdemo.php');
			exit;
		}
		if($row['master_list']=="yes"){
			header('Location:masterlist.php');
			exit;
		}
		if($row['download_list']=="yes"){
			header('Location:admin_features.php');
			exit;
		}
		if($row['payment_info']=="yes"){
			header('Location:newdemo.php');
			exit;
		}
		if($row['alliance_list']=="yes"){
			$_SESSION['user_type']="Affiliate";
			header('Location:admin_features.php');
			exit;
		}*/
		
	}
	else{
		$ErrMsg = "Invalid user name or password";
	}
}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<title>MAR Pipeline System</title>
		<link rel="shortcut icon" href="images/Fav.ico" type="image/ico">
		<link rel="stylesheet" type="text/css" href="css/style.css" />
		<meta name="Generator" content="EditPlus">
		<meta name="Author" content="">
		<meta name="Keywords" content="">
		<meta name="Description" content="">
		<style>
			body{
				margin:0;
				color:black;
				background:#2661a7;
				font-family:arial;
			}
			.header{
				height:80px;
				background:#1C242A;
			}
			.content{
				background:#455A68;
				min-height:600px;
			}
			
			.form_actions{
				padding-top:15px;
				padding-left:5px;
				padding-bottom:30px;
			}
			.form_actions .add_btn{
				cursor:pointer;
				border-radius:0px;
				background:#0D0D0D;
				color:#D9D9D9;
				border-color:#D9D9D9;
				padding:5px 15px 5px 15px;
				font-family:arial;
			}
			.list_content{
				/*width:950px;
				
				margin-left:40px;
				margin-left:auto;
				margin-right:auto;
				*/
				width:100%;
			}
			.tbl_header th{
				font-size:13px;
				border-bottom:1px solid #D9D9D9;
				text-align:left;
				font-family:arial;
				color:white;
			}
			.tbl-body{
				font-size:12px;
				line-height:25px;
				font-family:arial;
			}
			a{
				color:black;
			}
			
		</style>
	</head>
	<body>
		<div>
			<div >
				<div class="header" style="border-bottom: 1px solid white;">
					<img src="<?php echo HTTP_ROOT_FOLDER;?>images/applogo1.png" style="margin-top:20px;margin-left:40px;">
				</div>
				<div style="min-height: 120px;">
				<h3 style="font-size: 1.8rem;line-height: 2rem;font-weight: 500;font-family: 'Raleway', sans-serif;text-align:center;color: #fff;margin-top:20px;"> Welcome to the<br> My Appy Business<br> Sales CRM</h3>

				</div>
				 <div class="list_content">
				 <?php
				 $tblmsg="Login information sent successfully.";
				 if($_SESSION["emailmsg"]!=""){
					$tblmsg="Your password has been sent to your registered email address. Please check your inbox now.";
					}
							if($msg!="" || $_SESSION["emailmsg"]!="")
								{
									if(!empty($_SESSION["emailmsg"]))
										unset($_SESSION["emailmsg"]);
									
							?>
							<table style="margin:auto;">
									<tr style="background:white;height:28px;"id="error_message">
									<td valign="middle" style="color:black;font-size:20px;font-family:arial;margin-left:10px;" colspan="2"><?php echo $tblmsg;?></td>
								</tr>
							
							</table>
							<?php
								}
							?>
					<div class="container">
					    
						<section id="content" style="margin:auto;">
						 
							<form action="" method="post">
								<h1>Team Login</h1>
								<?php
								if($ErrMsg!=""){
									echo "<span style=\"color:red;\">".$ErrMsg."<span>";
								}
								?>
								<div>
									<input type="text" placeholder="Username" name="Username" required="" id="username" name="username" value="<?php echo $UserName; ?>"/>
								</div>
								<div>
									<input type="password" placeholder="Password" name="Password" required="" id="password" name="password"/>
								</div>
								<div>
									<input type="button" value="Forgot Password" style="width:150px;" onclick="forgotPassword()"/><input type="submit" value="Log in" />
									
								</div>
							</form><!-- form -->
							
						</section><!-- content -->
					</div><!-- container -->	
				</div>
			</div>
			 
		</div>
		
	</body>
</html>
<script>
function forgotPassword(){
	document.location='forgotpassword.php';
}
</script>
<?php
if($msg!="")
{
	?>
	<script>setTimeout(function(){document.getElementById("error_message").style.display="none";},5000);</script>
	<?php
	//alert("test");
}
?>