<?php
include("../includes/configure.php");
include("../includes/session_check.php");
$dbdatetime=date('Y-m-d H:i:s',strtotime('now'));
$developer_id=$_GET["developer_id"];
if($developer_id!=""){
	//$developer_id
	$getQry="select * from tbl_developers where developer_id=:developer_id";
	$prepgetQry=$DBCONN->prepare($getQry);
	//$getRes=mysql_query($getQry);
	$prepgetQry->execute(array(":developer_id"=>$developer_id));
	$getRow=$prepgetQry->fetch();
	$developer_name=stripslashes($getRow["developer_name"]);
	$developer_email=stripslashes($getRow["developer_email"]);
	$mode="Edit";
	$value="Update";
}
else{
	$mode="Add";
	$value="Create";
}
if(isset($_POST["developer_name"])){
	$developer_name=addslashes(trim($_POST["developer_name"]));
	$developer_email=addslashes(trim($_POST["developer_email"]));
    if($mode=="Edit"){
		//$developer_name     $developer_email     $developer_id
		$updateQry="update tbl_developers set developer_name=:developer_name,developer_email=:developer_email,modified_date=:modified_date where developer_id=:developer_id";
		$prepupdateQry=$DBCONN->prepare($updateQry);
		$updateRes=$prepupdateQry->execute(array(":developer_name"=>$developer_name,":developer_email"=>$developer_email,":modified_date"=>$dbdatetime,":developer_id"=>$developer_id));
		//$updateRes=mysql_query($updateQry);
		if($updateRes){
			header("Location:developerlist.php");
			exit;
		}
	}
	else{
		//$developer_name    $developer_email
		$insertQry="insert into tbl_developers(developer_name,developer_email,added_date,modified_date) values(:developer_name,:developer_email,:added_date,:modified_date)";
		$prepinsertQry=$DBCONN->prepare($insertQry);
		$insertRes=$prepinsertQry->execute(array(":developer_name"=>$developer_name,":developer_email"=>$developer_email,":added_date"=>$dbdatetime,":modified_date"=>$dbdatetime));
		//$insertRes=mysql_query($insertQry);
		if($insertRes){
			header("Location:developerlist.php");
			exit;
		}
	}
}
include("includes/header.php");
?>
 <body>
		<div>
			<div style="margin-left:auto;margin-right:auto;">
				 
				<div class="content">
					<div class="list_content">
						<h1 style="font-size:25px;padding-top:15px;padding-bottom:15px;margin:0px;">New Developer</h1>
						<form name="developer_form" id="developer_form" method="post">
						<input type="hidden" name="developer_form_id" id="developer_form_id" value="<?php echo $developer_id;?>">
						<table cellspacing="15" cellpadding="0" border="0" width="70%">
							<tr>
								<td style="width:138px;">
									 Developer Name<font color="red">*</font>:
								</td>
								<td>
									<input type="text" name="developer_name" id="developer_name" class="inp_feild" value="<?php echo $developer_name;?>">
								</td>
							</tr>
							<tr>
								<td style="width:138px;">
									 Developer Email<font color="red">*</font>:
								</td>
								<td>
									<input type="text" name="developer_email" id="developer_email" class="inp_feild" value="<?php echo $developer_email;?>">
								</td>
							</tr>
							<tr>
							     <td>
									<div class="form_actions" style="text-align:left;">
										<input type="button" value="Back To Developer List" class="add_btn"  onclick="document.location='developerlist.php'">
									
								</td>
								<td>
									  <div class="form_actions" style="text-align:right;">
										<input type="button" value="<?php echo $value;?> Developer" class="add_btn" id="add_developer">
									</div>
								</td>
							</tr>
						</table>
						</form>
					</div>
					
				</div>
			</div>
		</div>
	<?php
include("includes/footer.php");
?>