<?php
include("../includes/configure.php");
$themes_array =array('1'=>'Default Theme Round',
                     '2'=>'Dark Gray Theme Round',
					 '3'=>'Green Theme Round',
					 '4'=>'White Theme Round',
					 '5'=>'Blue Theme Round',
					 '6'=>'Default Theme Sqaure',
                     '7'=>'Dark Gray Theme Sqaure',
					 '8'=>'Green Theme Sqaure',
					 '9'=>'White Theme Sqaure',
					 '10'=>'Blue Theme Sqaure');
					 
include("../includes/session_check.php");
$trial_id=$_GET["id"];
$dbdatetime=date('Y-m-d H:i:s',strtotime('now'));
if($trial_id!=""){
        $get_special_qry="select * from tbl_free_trials where trial_id=:trial_id";
		$prepget_special_qry=$DBCONN->prepare($get_special_qry);
		$prepget_special_qry->execute(array(":trial_id"=>$trial_id));
        $get_special_Row=$prepget_special_qry->fetch();
		$business_name=$get_special_Row['business_name']; 
		$logo=$get_special_Row['logo'];
		$business_type=$get_special_Row['business_type'];
		$old_logo=$get_special_Row['logo'];	
		$email=$get_special_Row['email'];	
		$theme=$get_special_Row['theme'];	
		$mode="Edit";
		$value="Update";
}
else{
	$mode="Add";
	$value="Create";
	
}
if(isset($_POST["business_name"])){
		$business_name=addslashes(trim($_POST["business_name"]));
		$business_type=addslashes(trim($_POST["business_type"]));
		$email=addslashes(trim($_POST["email"]));
		$theme=addslashes(trim($_POST["theme"]));
        $upload_special_logo=pathinfo($_FILES["logo"]["name"], PATHINFO_FILENAME);
	    $upload_special_logo_extension=pathinfo($_FILES["logo"]["name"], PATHINFO_EXTENSION);
		if($upload_special_logo!=""){
			$renamed_special_logo=time().".".$upload_special_logo_extension;
			$uplaod_path='../business_paypal/uploaded_special_logo/'.$renamed_special_logo;
			move_uploaded_file($_FILES["logo"]["tmp_name"],$uplaod_path);
		}
		else
		{
		   $renamed_special_logo=$old_logo;
		}
		if($mode=="Edit"){
					$updateQry="update tbl_free_trials set  business_name=:business_name,logo=:logo,business_type=:business_type,email=:email,theme=:theme,
					modified_date=:modified_date where trial_id=:trial_id";
					$prepupdateQry=$DBCONN->prepare($updateQry);
					$updateRes=$prepupdateQry->execute(array(":business_name"=>$business_name,":logo"=>$renamed_special_logo,":business_type"=>$business_type,":email"=>$email,":theme"=>$theme,":modified_date"=>$dbdatetime,":trial_id"=>$trial_id));
					if($updateRes){
						header("Location:business_list.php");
						exit;
					}
		}
		else{
			$insertqry="insert into tbl_free_trials
			(business_name,logo,business_type,email,theme,created_date,modified_date)values(:business_name,:logo,:business_type,:email,:theme,:created_date,:modified_date)"; $prepinsertqry=$DBCONN->prepare($insertqry);
			$insertRes=$prepinsertqry->execute(array(":business_name"=>$business_name,":logo"=>$renamed_special_logo,":business_type"=>$business_type,":email"=>$email,":theme"=>$theme,":created_date"=>$dbdatetime,":modified_date"=>$dbdatetime));
			if($insertRes){
				header("Location:business_list.php");
				exit;
			}

	  }
}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<title>MAR Pipeline System</title>
		<link rel="shortcut icon" href="images/Fav.ico" type="image/ico">
		<meta name="Generator" content="EditPlus">
		<meta name="Author" content="">
		<meta name="Keywords" content="">
		<meta name="Description" content="">
		<script type="text/javascript" src="js/jquery.js"></script>
		<script>
		var mode="<?php echo $mode;?>";
		</script>
		 <style>
			body{
				margin:0;
				color:#D9D9D9;
				background:#455A68;
				font-family:arial;
			}
			.header{
				height:70px;
				background:#1C242A;
			}
			.content{
				background:#455A68;
				min-height:600px;
			}
			
			.form_actions{
				padding-top:15px;
				padding-left:5px;
				padding-bottom:30px;
			}
			.form_actions .add_btn{
				cursor:pointer;
				border-radius:0px;
				background:#0D0D0D;
				color:#D9D9D9;
				border-color:#D9D9D9;
				padding:5px 15px 5px 15px;
				font-family:arial;

			}
			.list_content{
				width:950px;
				margin-left:40px;
				/*margin-left:auto;
				margin-right:auto;*/

			}
			.tbl_header th{
				font-size:13px;
				border-bottom:1px solid #D9D9D9;
				text-align:left;
				font-family:arial;
			}
			.tbl-body{
				font-size:12px;
				font-family:arial;
			}
			a{
				color:black;
			}
			.inp_feild{
				border-radius:2px;
				border:none;
				width:100%;
			}
		</style>
	</head>
	<body>
		<div>
			<div style="margin-left:auto;margin-right:auto;">
				<div class="header">
					<span style="float:right;margin-right:20px;margin-top:5px;"><a href="logout.php" style="color:white;text-decoration:none;">Logout</a></span>
					<img src="images/myappyrestaurants.png" style="margin-top:10px;margin-left:40px;">
				</div>
				<div class="content">
					<div class="list_content">
						<h1 style="font-size:25px;padding-top:15px;padding-bottom:15px;margin:0px;">Business Details</h1>
						<form name="business_form" id="business_form" method="post" enctype="multipart/form-data">
						<input type="hidden" name="hdn_business_id" id="hdn_business_id" value="<?php echo $business_id;?>">    
						<table cellspacing="15" cellpadding="0" border="0" width="70%">
						    <tr>
								<td style="width:138px;" valign="top">
								Business Name<font color="red">*</font>:
								</td>
								<td>
									<input type="text" name="business_name" id="business_name" class="inp_feild" value="<?php echo $business_name;?>" style="width:100%">
									 
								</td>
							</tr>
							 <tr>
								<td style="width:138px;" valign="top">
									Business Type<font color="red">*</font>:
								</td>
								<td>
								 <select name="business_type"  id="business_type" class="inp_feild">
									<option value="0">Select</option>
										<?php
										$getbusinessQry="select * from  tbl_business_type order by display_order asc";
										$prepget_business_qry=$DBCONN->prepare($getbusinessQry);
										$prepget_business_qry->execute();
										while($getbusinessRow=$prepget_business_qry->fetch()){
										?>
										<option value="<?php echo $getbusinessRow["business_type"];?>" <?php if($business_type==$getbusinessRow["business_type"]){ echo "selected";}?>><?php echo $getbusinessRow["business_type"];?></option>
										<?php
										}
										?>
									</select>
									 
								</td>
							</tr>
							<tr>
								<td style="width:138px;" valign="top">
									Business Logo<font color="red">*</font>:
								</td>
								<td>
									<input type="file" name="logo" id="logo" class="inp_feild">
									<?php if($logo!=""){ ?>
									
										<img src="<?php echo "http://www.myappyrestaurant.com/business_paypal/uploaded_special_logo/".$logo;?>" style="width:75px;height:75px;">
									<?php }?>
								</td>
							</tr>
							 <tr>
								<td style="width:138px;" valign="top">
									Email Address<font color="red">*</font>:
								</td>
								<td>
									<input type="text" name="email" id="email" class="inp_feild" value="<?php echo $email;?>" style="width:100%">
									 
								</td>
							</tr>
							 
							<tr>
								<td style="width:138px;" valign="top">
									Homepage Theme:<font color="red">*</font>:
								</td>
								<td>
									<select name="theme" id="theme" class="inp_feild">
										<option value="0">Select</option>
                                        <?php
										foreach($themes_array as $val=>$name){ 
										  echo '<option value="'.$val.'">'.$name.'</option>'; 
										} 
										?>
										 
									</select>
									<?php 
									 if($theme!=""){
										?>
									<script type="text/javascript">
									
									  $("#theme").val('<?php echo $theme;?>')
									</script>
									<?php 
							          }
									?>
								</td>
							</tr>					 
							<tr>
							     <td>
									<div class="form_actions" style="text-align:left;">
										<input type="button" value="Back To Business List" class="add_btn"  onclick="document.location='business_list.php'">
									
								</td>
								<td>
									  <div class="form_actions" style="text-align:right;">
										<input type="button" value="<?php echo $value;?> Business" class="add_btn" id="add_business">
									</div>
								</td>
							</tr>
						</table>
						</form>
					</div>
					
				</div>
			</div>
		</div>
	</body>
</html>
<script type="text/javascript">
 $(document).ready(function(){
   $("#add_business").click(function(){
	   var emailPattern=/^[a-zA-Z0-9\-_]+(\.[a-zA-Z0-9\-_]+)*@[a-z0-9]+(\-[a-z0-9]+)*(\.[a-z0-9]+(\-[a-z0-9]+)*)*\.[a-z]{2,4}$/;
		var hdn_id=$("#hdn_business_id").val();
		if(hdn_id==""){
			hdn_id=0;
		}
		var photoarr=$("#logo").val().split(".");
		var photolen=photoarr.length;
		var extension=photoarr[photolen-1].toLowerCase();
		if($.trim($("#business_name").val())==""){
			alert("Please enter business name");
			$("#business_name").focus();
			return false;
		}
		else if($.trim($("#business_type").val())=="0"){
			 alert("Please select business type");
			 $("#business_type").focus();
			 return false;
	    }
		else if($.trim($("#logo").val())==""&&hdn_id=="0"){
			alert("Please enter business logo");
			$("#logo").focus();
			return false;
		}
		else if($.trim($("#logo").val())!=""&&extension!="jpg"&&extension!="jpeg"&&extension!="png"){
		   alert('Please upload valid business logo');
		   $("#logo").focus();
		   return false;
    
        }
		else if($.trim($("#email").val())==""){
			alert("Please enter email");
			$("#email").focus();
			return false;
		}
		else if(!emailPattern.test($.trim($("#email").val()))){
			alert("Please enter valid email");
			$("#email").focus();
			return false;
		}
        else if($.trim($("#theme").val())=="0"){
			 alert("Please select homepage theme");
			 $("#theme").focus();
			 return false;
	    }
		else{
			$("#business_form").submit();
		}
		 
				 
	});
});
</script>