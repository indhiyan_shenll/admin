<?php

$parent_directory=basename(dirname($_SERVER["PHP_SELF"]));
$filename=basename($_SERVER["PHP_SELF"]);

include("../includes/configure.php");
include("includes/cmm_functions.php");
include("../includes/session_check.php");

$dbdatetime=date('Y-m-d H:i:s',strtotime('now'));
$id			=	$_SESSION['user_id'];
$user_name	=	$_SESSION['user_name'];
//exit;
//print_r($_SESSION);
$simulator='1';
$style="";


$feat="trail_form";
$typeandfeature=checklogin($id,$feat);


$usrArr=explode("*",$typeandfeature);
$user_type=$usrArr[0];
$feature=$usrArr[1];
$dowfeature=$usrArr[2];
$paymentf=$usrArr[3];
$trialfeature=$usrArr[4];

function getUserNameFrom_tbl_users($DBCONN, $UserId)
{
	$geUserQry="select * from  tbl_users where user_id=:user_id";
	$prepgetuserqry=$DBCONN->prepare($geUserQry);
	$prepgetuserqry->execute(array(":user_id"=>$UserId));
	$geUserRow=$prepgetuserqry->fetch();
	$created_user=stripslashes($geUserRow["username"]);
	return $created_user;
}



/*
if($user_type=="Admin" || $user_type=="Affiliate" || $user_type=="Agent" || $trialfeature == 'yes' || $user_type == 'Licensee')
{
*/

$prospect_id=$_GET["prospect_id"];

if($prospect_id!="")
{
	$mode_value="Update";

	$getprospectQry="select * from tbl_prospecting_list where special_one_id='".$prospect_id."'";
	$getprospectRes=mysql_query($getprospectQry);
	$getprospectRow=mysql_fetch_array($getprospectRes);

	//data for simulator section.

	$business_name=stripslashes($getprospectRow["business_name"]);
	$business_type_db=$getprospectRow["business_type"];
	$email=stripslashes($getprospectRow["email"]);
	$phone=stripslashes($getprospectRow["phone"]);
	$website = stripslashes($getprospectRow["Website"]);
	$product_page_val=$getprospectRow['product_page'];
	$promo_code=$getprospectRow['promo_code'];
	$simulator=$getprospectRow['simulator_feature'];
	if($simulator=="1")
		 $style="";				
	else
		$style="display:none";
				
	$special_logo=$getprospectRow['logo'];
	$old_special_logo=$getprospectRow['logo'];
	$business_theme=$getprospectRow['theme'];
	$created_user_id=$getprospectRow['user_id'];
	if($created_user_id=="" || $created_user_id=="0"){
	  $created_user_id=$_SESSION['user_id'];
	}
	$created_user = getUserNameFrom_tbl_users($DBCONN, $created_user_id);

	$first_name=stripslashes($getprospectRow["first_name"]);				
	$street_address=stripslashes($getprospectRow["street_address"]);				
	$suburb=stripslashes($getprospectRow["suburb"]);
	$state=stripslashes($getprospectRow["state"]);
	$postcode=stripslashes($getprospectRow["postcode"]);
	$country=stripslashes($getprospectRow["country"]);			
	$mobile=stripslashes($getprospectRow["mobile"]);
	
	$callstatus=stripslashes($getprospectRow["callstatus"]);
	if($getprospectRow["last_contact"]!="" && $getprospectRow["last_contact"]!="0000-00-00")			
		$last_contact=date('d-m-Y',strtotime(stripslashes($getprospectRow["last_contact"])));				
	else
		$last_contact="";
	if($getprospectRow["callback_date"]!="" && $getprospectRow["callback_date"]!="0000-00-00")			
		$Call_date=date('d-m-Y',strtotime(stripslashes($getprospectRow["callback_date"])));				
	else
		$Call_date="";
	$Call_time=stripslashes($getprospectRow["callback_time"]);
	
				
	if($getprospectRow["import_date"]!="" && $getprospectRow["import_date"]!="0000-00-00")			
		$import_date=date('d-m-Y',strtotime(stripslashes($getprospectRow["import_date"])));				
	else
		$import_date="";	
	$affiliate_id=$getprospectRow['a_id'];
	$licence_id=$getprospectRow['l_id'];

	//$send_mail_date_db
	if($getprospectRow["mail_send_date"]!="" && $getprospectRow["mail_send_date"]!="NULL" && $getprospectRow["mail_send_date"]!="0000-00-00")
	{
		$send_mail_date_db = date('d-m-Y',strtotime(stripslashes($getprospectRow["mail_send_date"])));
		$send_mail_date_db_temp = $getprospectRow["mail_send_date"];
	
	}
	else
	{
		$send_mail_date_db = "";
		$send_mail_date_db_temp = "";	
	}

	$comments=stripslashes($getprospectRow["comments"]); 

	$mode="Edit";
	$value="Update";
}
else
{
	$mode="Add";
	$mode_value="Create";
	$value="Create";

	$created_user_id=$_SESSION['user_id'];
	$created_user = getUserNameFrom_tbl_users($DBCONN, $created_user_id); 
}


if(isset($_POST["hidden1"]))
{

	$business_name = addslashes(trim($_POST["business_name"]));
	$business_type = trim($_POST["business_type"]);
	$email = addslashes(trim($_POST["email"]));
	$phone=addslashes(trim($_POST["phone"]));
	$product_page=addslashes(trim($_POST["product_page"]));
	$promo_code=trim($_POST['promo_code']);

	if(empty($_POST["simulator"]))
	{
		 $simulator_value="0";
		 $theme="0";
	}
	else
	{
		$simulator_value="1";  
		$theme=$_POST["theme"];
	}

	$LogoAppendQry = "";
	$upload_special_logo=pathinfo($_FILES["special_logo"]["name"], PATHINFO_FILENAME);	
	if($upload_special_logo!="")
	{
		$upload_special_logo_extension=pathinfo($_FILES["special_logo"]["name"], PATHINFO_EXTENSION);
		$renamed_special_logo=time().".".$upload_special_logo_extension;
		$upload_logo_path=SPECIAL_LOG_PATH.$renamed_special_logo;		
		move_uploaded_file($_FILES["special_logo"]["tmp_name"],$upload_logo_path);
		$LogoAppendQry = "logo = :logo,";
	}

	$first_name=$_POST['first_name'];
	$street_address=addslashes(trim($_POST["street_address"]));
	$suburb=addslashes(trim($_POST["suburb"]));
	$state=addslashes(trim($_POST["state"]));
	$postcode=addslashes(trim($_POST["postcode"]));
	$country=addslashes(trim($_POST["country"]));
	$mobile=addslashes(trim($_POST["mobile"]));

	$callstatus=$_POST['callstatus'];
	$last_contact=(!empty($_POST["last_contact"]))?date('Y-m-d',strtotime(addslashes(trim($_POST["last_contact"])))):NULL;
	$CallBack_date=(!empty($_POST["callback_date"]))?date('Y-m-d',strtotime(addslashes(trim($_POST["callback_date"])))):NULL;
	$call_time=addslashes(trim($_POST["callback_time"]));

	$import_date=date('Y-m-d',strtotime(addslashes(trim($_POST["import_date"]))));
	$affiliate=addslashes(trim($_POST["affiliate"]));
	$licensee=addslashes(trim($_POST["licensee"]));
	

	//Send email 
	$AgentMailSendAppendQry = "";
	if($user_type=="Agent" && $_POST["agent_send_email"]=="yes")
	{
		$gettemplateQry="select * from tbl_emailtemplates where emailtemplate_id=:emailtemplate_id";
		$prep_template_getQry=$DBCONN->prepare($gettemplateQry);
		$prep_template_getQry->execute(array(":emailtemplate_id"=>"25"));
		$gettemplateRow=$prep_template_getQry->fetch();
		$from=stripslashes($gettemplateRow["from_email"]);
		$to=trim($_POST["email"]);
		$subject=stripslashes($gettemplateRow["subject"]);;
		$message=stripslashes($gettemplateRow["message"]);
		$message=str_replace('#promocode#',trim($_POST['promo_code']),$message);
		$message=str_replace('#mar_logo#',"<img src='http://www.myappyrestaurant.com/images/mail.png' title='MAB logo' alt='MAB logo'>",$message);
		sendEmail($from,$to,$subject,$message);
		$send_mail_date=date("Y-m-d");
		$AgentMailSendAppendQry = "mail_send_date = :mail_send_date,";
	}
					 
	if($mode=="Add")
	{ 					  
					    
			// PROSPECTING LIST INSERT QUERY

			$InsertSimulatorQuery = "insert into tbl_prospecting_list(
				business_name, business_type, email, phone, product_page, promo_code, simulator_feature, logo, theme, user_id,
				first_name, street_address, suburb, state, postcode, country, mobile, 
				callstatus, last_contact, callback_date, callback_time,
				comments, mail_send_date, 
				import_date, a_id, l_id, created_date, modified_date)				
				values(
				:business_name, :business_type, :email, :phone, :product_page, :promo_code, :simulator_feature, :logo, :theme, :user_id,
				:first_name, :street_address, :suburb, :state, :postcode, :country, :mobile, 
				:callstatus, :last_contact, :callback_date, :callback_time,
				:comments, :mail_send_date, 
				:import_date, :a_id, :l_id, :created_date, :modified_date)";				

			$PrepareInsertSimulatorQuery = $DBCONN->prepare($InsertSimulatorQuery);

			$SimulatorDateArray = array(":business_name" => $business_name,
										":business_type" => $business_type, 
										":email" => $email,
										":phone" => $phone, 
										":product_page" => $product_page, 
										":promo_code" => $promo_code, 
										":simulator_feature" => $simulator_value, 
										":logo" => $upload_logo_path, 
										":theme" => $theme, 
										":user_id" => $_SESSION['user_id'],
										":first_name" => $first_name, 
										":street_address" => $street_address, 
										":suburb" => $suburb, 
										":state" => $state, 
										":postcode" => $postcode, 
										":country" => $country, 
										":mobile" => $mobile, 
										":callstatus" => $callstatus, 
										":last_contact" => $last_contact, 
										":callback_date" => $CallBack_date, 
										":callback_time" => $call_time,
										":comments" => $comments, 
										":mail_send_date" => $send_mail_date, 
										":import_date" => $import_date, 
										":a_id" => $affiliate,
										":l_id" => $licensee, 
										":created_date" => $dbdatetime,
										":modified_date" => $dbdatetime
										);	
			
				if($PrepareInsertSimulatorQuery->execute($SimulatorDateArray))
				{
					$prospect_id_insert = $DBCONN->lastInsertId();

					// Adding Comments in prospects comments table					
					if(isset($_POST["prospect_comments_add"]) && $_POST["prospect_comments_add"]!="")
					{									  
						$ProspectsCommentsQuery = "insert into tbl_prospect_comments(comment,prospect_id,user_id,created_date,modified_date)values(:comment,:prospect_id,:user_id,:created_date,:modified_date)";
						$PrepareProspectsCommentsQuery=$DBCONN->prepare($ProspectsCommentsQuery);

						$ProspectsCommentDataArray = array(
												":comment" => addslashes($_POST["prospect_comments_add"]),
												":prospect_id"=>$prospect_id_insert,
												":user_id"=>$_SESSION['user_id'],
												":created_date"=>$dbdatetime,
												":modified_date"=>$dbdatetime);

						$PrepareProspectsCommentsQuery->execute($ProspectsCommentDataArray);

					}                                     
					header("Location:prospectinglist.php");
					exit;

				 }
					   
					   
		 }
		 else
		 {

			$UpdateProspectsQuery = "update tbl_prospecting_list set 
					business_name = :business_name, business_type = :business_type, email = :email, phone = :phone,
					product_page = :product_page, promo_code = :promo_code, simulator_feature = :simulator_feature,".$LogoAppendQry."  
					theme = :theme, first_name = :first_name, street_address = :street_address, suburb = :suburb, 
					state = :state, postcode = :postcode, country = :country, mobile = :mobile, 
					callstatus = :callstatus, last_contact = :last_contact, callback_date = :callback_date, callback_time = :callback_time,
					comments = :comments, ".$AgentMailSendAppendQry."  
					a_id = :a_id, l_id = :l_id, modified_date = :modified_date 
					where special_one_id=:special_one_id";

			
			$PrepareUpdateProspectsQuery = $DBCONN->prepare($UpdateProspectsQuery);
			$SimulatorUpdateDataArray = array(":business_name" => $business_name,
										":business_type" => $business_type, 
										":email" => $email,
										":phone" => $phone, 
										":product_page" => $product_page, 
										":promo_code" => $promo_code, 
										":simulator_feature" => $simulator_value, 										
										":theme" => $theme, 
										":first_name" => $first_name, 
										":street_address" => $street_address, 
										":suburb" => $suburb, 
										":state" => $state, 
										":postcode" => $postcode, 
										":country" => $country, 
										":mobile" => $mobile, 
										":callstatus" => $callstatus, 
										":last_contact" => $last_contact, 
										":callback_date" => $CallBack_date, 
										":callback_time" => $call_time,
										":comments" => $comments, 										
										":a_id" => $affiliate,
										":l_id" => $licensee, 
										":modified_date" => $dbdatetime,
										":special_one_id" => $prospect_id
										);
			
			if($LogoAppendQry!="")
				$SimulatorUpdateDataArray[':logo'] = $upload_logo_path;
			
			if($AgentMailSendAppendQry!="")			
				$SimulatorUpdateDataArray[':mail_send_date'] = $send_mail_date;
			
			$updateRes = $PrepareUpdateProspectsQuery->execute($SimulatorUpdateDataArray);

			if($updateRes)
			{
				header("Location:prospectinglist.php");
				exit;
			} 
			else
			{
				echo "Error Occured";
				exit;
			}
		  	 		 		 
	}
}
include("includes/header.php");
?>

<body>
     <div>
		 <div style="margin-left:auto;margin-right:auto;">
			<div class="content">
			  <div class="page_content" style = "border:0px solid red">
				<table cellspacing="15" cellpadding="0" border="0" width="100%">
					<tr>
						<td colspan="2">
								<table cellspacing="0" cellpadding="0" width="100%" border="0">
								<tr>
									<td>
										<div class="form_actions">
											<input type="button" value="Back to Prospect List" class="add_btn"   onclick="document.location='prospectinglist.php'">
										</div>
									</td>
									<td>
										<div class="form_actions" style="text-align:right;" id="upd_pros_btn" class="upd_pros_btn">
											  <input type="button" value="<?php echo $mode_value;?> Record" class="add_btn update_prospect"   >  
										</div>
									</td>
								</tr>
								</table>
						</td>
					</tr>
				</table>
                <form name="prospect_form" id="prospect_form" method="post" enctype="multipart/form-data">  
                <input type="hidden" name="hidden1" id="hidden1" value="">
				 <input type="hidden" name="agent_send_email" id="agent_send_email">
				<input type="hidden" name="hidden_special" id="hidden_special" value="<?php echo $special_id;?>">
				<input type="hidden" name="hidden_logo" id="hidden_logo" value="<?php echo $special_logo;?>">
				<input type="hidden" name="hdn_img" id="hdn_img" value="<?php echo $old_special_logo;?>">
				<input type="hidden" name="user_type" id="user_type" value="<?php echo $user_type;?>">
				<table cellspacing="15" cellpadding="0" border="0" width="100%">
					<tr>
						<td colspan="2">
							<h3><u>SIMULATOR INFO:</u></h3>
						</td>
					</tr>
					<?php
					if($msg!="")
					{
					?>
					<tr bgcolor="white" height="40px" id="error_message">
							<td style="color:black;font-size:20px;font-family:arial;margin-left:10px;" colspan="2">Client trial form added successfully.</td>
					</tr>
					<?php
					}
					?>
					<tr>
						<td style="width:168px;">
							Business Name<?php if($user_type=="Agent"){echo "<font color=\"red\">*</font>";}?>:
						</td>
						<td>
						  <?php
						  if($prospect_id==""){
						   ?>
							   <input type="text" name="business_name" id="business_name" class="inp_feild form_control" value="<?php echo $business_name;?>" tabindex="1"  onkeyup="setcode(this.value); validateForm();"  >
							<?php
				      	   }
						   else{
							   ?>
						        <input type="text" name="business_name" id="business_name" class="inp_feild form_control" value="<?php echo $business_name;?>" tabindex="1"  onkeyup="validateForm();"  >
						   <?php
						   }
							?>
						</td>
					</tr>
					<tr>
						<td>
							Business Type<?php if($user_type=="Agent"){echo "<font color=\"red\">*</font>";}?>:
						</td>
						<td>
						      <select name="business_type" id="business_type" class="input_field form_control"  tabindex="2" style="width:100%;" onchange="validateForm();"  >
								<option value="0">Select</option>					
								<?php
								$getbusinessQry="select * from  tbl_business_type order by display_order asc";
								$prepget_business_qry=$DBCONN->prepare($getbusinessQry);
								$prepget_business_qry->execute();
								while($getbusinessRow=$prepget_business_qry->fetch()){
								?>
								<option value="<?php echo $getbusinessRow["business_type"];?>" <?php if($business_type_db==$getbusinessRow["business_type"]) echo " selected";?>><?php echo $getbusinessRow["business_type"];?></option>
								<?php
								}
								?>
								</select>
						</td>
					</tr>
					<tr>
						<td>
							Email Address<?php if($user_type=="Agent"){echo "<font color=\"red\">*</font>";}?>:
						</td>
						<td>
						   <input type="text" name="email" id="email" class="inp_feild form_control" value="<?php echo $email;?>" tabindex="3" onkeyup="validateForm();" onchange="validateForm();">
						</td>
					</tr>
					<tr>
						<td>
							Website:
						</td>
						<td>
						   <input type="text" name="website" id="website" class="inp_feild form_control" value="<?php echo $website;?>" tabindex="3" readonly>
						</td>
					</tr>
					<tr>
						<td>
							Work Phone<?php if($user_type=="Agent"){echo "<font color=\"red\">*</font>";}?>:
						</td>
						<td>
							<input type="text" name="phone" id="phone" class="inp_feild" value="<?php echo $phone;?>" tabindex="4" style="width:65%" onkeyup="validateForm();" onchange="validateForm();"><!-- <span><a href="skype:+<?php echo $phone;?>?call"><img src="images/skype.png" style="position: absolute;"></a></span> --><span style="font-size:13px;"><?php echo $phone;?></span>
						</td>
					</tr>
					<tr>
						<td style="width:138px;" valign="top">
							Product Page<?php if($user_type=="Agent"){echo "<font color=\"red\">*</font>";}?>:
						</td>
						<td>
						   <select name="product_page" id="product_page" class="inp_feild form_control" style="width:50%;" tabindex="5" onchange="validateForm();" >
						   <option value="0">Select</option>
							<?php
							if($_SESSION['user_id']=="3" || $user_type=="Admin" || $user_type=="Agent"){
								$getQry_1="SELECT  DISTINCT `product_page` FROM `tbl_apps`  order by `product_page` asc";
								$prepgetQry1=$DBCONN->prepare($getQry_1);
								$prepgetQry1->execute();
								while($getRow1=$prepgetQry1->fetch()){
									$y=$getRow1["product_page"];
									echo "<option value='".$y."'>".$y."</option>";	
							    }
							}
							else{
								$getQry_1="SELECT * FROM  tbl_affiliate where a_user_id='".$_SESSION['user_id']."'";
								$prepgetQry1=$DBCONN->prepare($getQry_1);
								$prepgetQry1->execute();
								while($getRow1=$prepgetQry1->fetch()){
									$x=$getRow1["product_page1"];
									$y=$getRow1["product_page2"];
									$z=$getRow1["product_page3"];
									if(!empty($x)){
										echo "<option value='".$x."'>".$x."</option>";
									}
									if(!empty($y)){
										echo "<option value='".$y."'>".$y."</option>";
									}
									if(!empty($z)){
											echo "<option value='".$z."'>".$z."</option>";
									}
								}
							}
							?>
							</select>
							<?php
							if($product_page_val!=""){
							?>
								<script language="javascript">document.getElementById("product_page").value="<?php echo $product_page_val;?>"</script>
							<?php
							}
							?>

						</td>
					</tr>
					<tr>
						<td style="width:117px;">
							Promo Code<?php if($user_type=="Agent"){echo "<font color=\"red\">*</font>";}?>:
						</td>
						<td>
							<input type="text" name="promo_code" id="promo_code" class="inp_feild form_control" value="<?php echo $promo_code;?>" style="width:50%" tabindex="6" onkeyup="validateForm();"  <?php if($prospect_id!="" && $promo_code!=""){ echo "readonly";}
						   ?>>
						</td>
					</tr>
					<tr>
						<td style="width:138px;" valign="top">
						</td>
						<td>
							<input type="checkbox" name="simulator"   id="simulator"  value="1" <?php if($simulator=="1"){echo "checked";}?> onclick="show(this.id); " tabindex="7"> Add Simulator <br>
						</td>
					</tr>
			    	<tr class="theme_div" style="margin-top:5px;<?php echo $style;?>">
						<td style="width:138px;" valign="top">
							Business Logo<?php if($user_type=="Agent"){echo "<font color=\"red\">*</font>";}?>:
						</td>
						<td valign="top">
							<input type="file" name="special_logo" id="special_logo" class="inp_feild <?php if($special_logo==""){ echo "form_control";}?>" tabindex="8" onchange="validateForm();" >
							<?php if($special_logo!=""){ ?>
							<br><br>
							<img src="<?php echo $special_logo;?>" style="width:75px;height:75px;">
							<?php }?>
						</td>
					</tr>
					<tr class="theme_div_demo" style="display:none" >
						<td style="width:138px;" valign="top"> 
						</td>
						<td align="left" id="theme_div_design">
							 
						</td>
					</tr>
					<tr class="theme_div" style="margin-top:5px;<?php echo $style;?>" >
						<td style="width:138px;" valign="top"> 
							Design Style<?php if($user_type=="Agent"){echo "<font color=\"red\">*</font>";}?>:
						</td>
						<td>
							<select name="theme" id="theme" class="inp_feild form_control" tabindex="9" onchange="validateForm(); show_demo_image(this.value);" >
							<option value="0">Select</option>
							<?php
							foreach($themes_array as $val=>$name){ 
								echo '<option value="'.$val.'">'.$name.'</option>'; 
							} 
							?>
							</select>
							<?php 
							if($business_theme!="" && $business_theme>0){
							?>
								<script type="text/javascript">
								$("#theme").val('<?php echo $business_theme;?>')
								</script>
							<?php 
							}
							?>
						</td>
					</tr>
					<tr>
						<td style="width:138px;">
							Record Creator:
						</td>
						<td>
							<input type="text" name="user_id" id="user_id" class="inp_feild" tabindex="10" value="<?php echo stripslashes($created_user);?>"  readonly >
						</td>
					</tr>
					<?php
					if($user_type=="Agent"){
					?>
					<tr>
						<td style="width:138px;">
						</td>
						<td valign="top">
								<table >
									<tr>
										<td class="send_email_bg" style="border-radius: 5px;height: 40px;width: 110px;border: 1px solid black;
										cursor: pointer;text-align: center;color: #000;font-weight: bold;" onclick="send_mail()" >
											SEND EMAIL <br>NOW!
										</td>
									</tr>
									<tr>
											<td align="center" class="mail_date">
												<?php if($send_mail_date_db!="" && $send_mail_date_db!="00/00/0000"){ 
												    echo "(".$send_mail_date_db.")";
													
													}
												?> 
											</td>
								 </tr>
								</table>  
						</td>
					</tr>
					<?php
					}
					?>
					<tr>
						<td colspan="2">
							<h3><u>CONTACT INFO:</u></h3>
						</td>
						</tr>
						<tr>
							<td style="width:168px;">
								Full Name<?php if($user_type=="Agent"){echo "<font color=\"red\">*</font>";}?>:
							</td>
							<td>
								<input type="text" name="first_name" id="first_name" class="inp_feild" value="<?php echo stripslashes($first_name);?>" tabindex="11">
							</td>
					</tr>
					<tr>
						<td>
							Address<?php if($user_type=="Agent"){echo "<font color=\"red\">*</font>";}?>:
						</td>
						<td>
							<input type="text" name="street_address" id="street_address" class="inp_feild" value="<?php echo $street_address;?>" tabindex="12">
						</td>
					</tr>
					<tr>
						<td>
							Suburb:
						</td>
						<td>
							<input type="text" name="suburb" id="suburb" tabindex="13" class="inp_feild"   value="<?php echo stripslashes($suburb);?>">
						</td>
					</tr>
					<tr>
							<td  valign="top">State:</td>
							<td  valign="top">
							<table width="100%">
							<tr>
								<td><input type="text" name="state" id="state" class="inp_feild" value="<?php echo $state;?>" tabindex="14" style="margin-left: -2px;"></td>
								<td align="center">Postcode:</td>
								<td><input type="text"  name="postcode" id="postcode" class="inp_feild" value="<?php echo stripslashes($postcode);?>"   tabindex="15" ></td>
							</tr>
							</table>
							
							</td>
						</tr>
					 
					<tr>
					<td>
						Country<?php if($user_type=="Agent"){echo "<font color=\"red\">*</font>";}?>:
					</td>
					<td>
							<select name="country" id="country" class="inp_feild" tabindex="16">
							<option value="0"<?php if($res1_country=="0") echo " selected";?>>Select</option>
							<?php
							$getcountrQry="select * from  tbl_country";
							$getcountryRes=mysql_query($getcountrQry);
							while($getcountryRow=mysql_fetch_array($getcountryRes)){
							?>
							<option value="<?php echo stripslashes($getcountryRow["country"]);?>"<?php if($country==$getcountryRow["country"]) echo " selected";?>><?php echo stripslashes($getcountryRow["country"]);?></option>
							<?php
							}
							?>
							</select>
					</td>
					</tr>
					 
					<tr>
						<td>
							Work Phone:
						</td>
						<td>
							<input type="text" name="phone" id="phone1" class="inp_feild" value="<?php echo $phone;?>" tabindex="17" style="width:65%" onkeyup="validateForm();" readonly> <span style="font-size:13px;"><?php echo $phone;?></span>
						</td>
					</tr>
					<tr>
						<td>
							Email Address:
						</td>
						<td>
							<input type="text" name="email" id="email1" class="inp_feild " value="<?php echo $email;?>" tabindex="18" readonly onkeyup="validateForm();" style="width:65%">
						</td>
					</tr>
					<tr>
						<td>
							Mobile Phone:
						</td>
						<td>
							<input type="text" name="mobile" id="mobile" class="inp_feild" value="<?php echo $mobile;?>" tabindex="19"  style="width:65%"><span style="font-size:13px;"><?php echo $mobile;?></span>
						</td>
					</tr>
					<tr>
						<td colspan="2">
							<h3><u>SALES INFO:</u></h3>
						</td>
					</tr>
					<tr>
						<td>
							Call Status:
						</td>
						<td>
							<select name="callstatus" id="callstatus" class="inp_feild" style="width:50%;" tabindex="20"  >
							<option value="0">Select</option>
							<?php
							$getcallstatusQry="select * from  tbl_callstatus order by display_order asc";
							$getcallstatusRes=mysql_query($getcallstatusQry);
							$callStsArr="";
							while($getcallstatusRow=mysql_fetch_array($getcallstatusRes)){
								if($dbpush=='yes'){
									if($getcallstatusRow["callstatus"]=='Trial requested' || $getcallstatusRow["callstatus"]=='Activation email sent (follow-up)' ||     $getcallstatusRow["callstatus"]=='Payment processed' || $getcallstatusRow["callstatus"]=='Trial not sold' ||  $getcallstatusRow["callstatus"]=='Promo email sent (follow-up)'){
										$callStsArr[]=stripslashes($getcallstatusRow["callstatus"]);
									}
								}
								else{
									if($getcallstatusRow["callstatus"]!='Activation email sent (follow-up)' && $getcallstatusRow["callstatus"]!='Trial not sold'){
										$callStsArr[]=stripslashes($getcallstatusRow["callstatus"]);
									}
								}
							}
							foreach($callStsArr as $stsval){
							?>
							<option value="<?php echo $stsval;?>"<?php if($callstatus==$stsval) echo " selected";?>><?php echo $stsval;?></option>
							<?php
							}
							?>
							</select>
						</td>
					</tr>
					<tr>
						<td>
							Last Contact:
						</td>
						<td>
							<input type="text" name="last_contact" id="last_contact" class="inp_feild"  style="width:50%;" value="<?php echo $last_contact;?>" tabindex="21">
						</td>
						</tr>
						<tr>
							<td>Callback  date:</td>
							<td><input type="text" name="callback_date" id="callback_date" tabindex="22" class="inp_feild" style="width:25%;" value="<?php echo $Call_date;?>">&nbsp;&nbsp;&nbsp;Callback  time:&nbsp;&nbsp;&nbsp;<input type="text" name="callback_time" id="callback_time" class="inp_feild" style="width:25%;" tabindex="23" value="<?php echo $Call_time;?>"></td>
						</tr>
					<?php
					if($prospect_id!=""){
					?>
					<tr>
					<td valign="top">
					Comments:
					</td>
					<td>
						<table border="0" width="100%" cellspacing="0">
							<tr>
								<td width="80%">
									<textarea name="prospect_comments" id="prospect_comments" class="inp_feild " style="height:200px;font-family: arial;font-size:13px;width:100%;" tabindex="24" onkeyup="show_comment(this.value);" onkeypress="show_comment(this.value);"></textarea> 
								</td>
								<td  width="20%" valign="top">
									<div class="form_actions">
									<input type="button" value="Add Comment" class="add_btn add_coomment_box" onclick="addcomment('<?php echo addslashes($_SESSION['user_id']);?>','<?php echo $prospect_id;?>')" style="margin-top: -15px;margin-left:17px;background-color: gray;border-color:gray;" >
									</div>
								</td>
							</tr>
						</table>
					<script>
					$(document).ready(function(){
						$("#callstatus").trigger('change');
						$("#record_type").trigger('change');
						setwidth();

					});
					function setwidth()
					{
						if($(".myTable tr:first td").length>1){
							var table = $(".myTable tr:first");
							var firstColumnWidth = table.find("td:first").width();
							var secodColumnWidth = table.find("td:nth-child(2)").width();
							var thirdColumnWidth = table.find("td:nth-child(3)").width();
							var table_header = $(".mytable1 tr:first");
							$('#td1').width(firstColumnWidth);
							$('#td2').width(secodColumnWidth);
							$('#td3').width(thirdColumnWidth);
						}
						else{
						    $('#td1').width(150);
							$('#td2').width(395);
							$('#td3').width(150);
						}

					}
					function addcomment(userid,prospect_id){
						comment=$("#prospect_comments").val();
						if($.trim($("#prospect_comments").val())==""){
						alert("Please enter comments");
						$("#prospect_comments").focus();
						return false;

					}
					else{
						/*$.ajax({url:"ajax_comment.php?comment="+comment+"&userid="+userid+"&prospect_id="+prospect_id+"",success:function(result){
							$("#last_contact").val('<?php echo date("d-m-Y");?>');
							$('#prospect_comments').val("");
							$("#container").html(result);
						}});*/
								$.ajax({
										type : 'post',
										url : 'ajax_comment.php', // in here you should put your query 
										data :  'comment='+comment+'&userid='+userid+'&prospect_id='+prospect_id, // here you pass your id via ajax .
										// in php you should use $_POST['post_id'] to get this value 
										success : function(r)
										{       
												$("#last_contact").val('<?php echo date("d-m-Y");?>');
												$('#prospect_comments').val("");
												$("#container").html(r);
												if($(".myTable tr:first td").length>1){
												 setwidth();
												}
												else{
													 $('#td1').width(150);
							                         $('#td2').width(395);
							                         $('#td3').width(150);
												}
											 
										}
								}); 
					}
				}
				</script>
					</td>
					</tr>		
						<td colspan="2" valign="top" >
							
								<!-- <table  cellpadding="0" width="100%" cellspacing="0"   border="1"  style="border:1px solid #CAE1F9;font-size:13px;color:black;border-collapse: collapse;"> -->
								<table  cellpadding= "0 " width= "100% " cellspacing= "0 "  style= "border:1px solid #CAE1F9;font-size:13px;color:black;border-collapse: collapse; " border= "1 " class="mytable1">
									<tr style="color:white;min-height: 20px;">
										<td valign="top"  align="left"  id="td1" nowrap>Date</td>
										<td valign="top"  align="left"  id="td2" nowrap>Comment</td>
										<td valign="top"  align="left"  id="td3" colspan='2' nowrap>User Name</td>
										
									</tr>
								</table>
								<div id="container" style="width:100%;overflow: auto;height:100px;">
								<table  cellpadding= "0 " width= "100% " cellspacing= "0 "  style= "border:1px solid #CAE1F9;font-size:13px;color:black;border-collapse: collapse; " border= "1 " class="myTable">
							 
									<?php
									$getQry="select * from  tbl_prospect_comments where prospect_id=:prospect_id  order by comment_id desc";
									$prepgetQry=$DBCONN->prepare($getQry);
									$prepgetQry->execute(array(":prospect_id"=>$prospect_id));
									$count =$prepgetQry->rowCount();
									if($count>0){
									$i=1;
									while($getRow=$prepgetQry->fetch()){
									if($i%2==0)
									$display_color="#a5a5a5";
									else
									$display_color="#d2d1d1";
									$user_id=$getRow["user_id"];
									$get_Qry1="select * from  tbl_users where user_id=:user_id";
									$prepget1_Qry=$DBCONN->prepare($get_Qry1);
									$prepget1_Qry->execute(array(":user_id"=>$user_id));
									$get_Row1=$prepget1_Qry->fetch();
									$user_name=$get_Row1['username'];
									?>
									<tr style="background-color:<?php echo $display_color;?>;height: 20px;border-color:black">	
										<td valign="top"  align="left"  >
											<?php echo date("d-m-Y", strtotime($getRow["created_date"]));?>
										</td>
										<td valign="top"  align="left"   style="width:505px;">
											<?php echo stripslashes($getRow["comment"]);?>
										</td>
										<td valign="top"  align="left"   >
										    <?php echo $user_name;?></td>
										</tr>
								<?php
								$i++;
								}
								}
								else{
								echo "<tr style=\"background-color:#d2d1d1;text-align:center;\"><td colspan=\"3\">No Comment(s) found.</td></tr>";
								}
								?>
								</table>
						</div>
					</tr>
					<?php
					}
					else{
						?>
                     <tr>
						<td valign="top">
							Comments:
						</td>
						<td>
							<textarea name="prospect_comments_add" class="inp_feild " style="height:200px;font-family: arial;font-size:13px;width:100%;" tabindex="24" ></textarea> 
						</td>
					</tr>

					
					<?php
					}
					?>
					<tr>
						<td colspan="2">
							<h3><u>SYSTEM INFO:</u></h3>
						</td>
					</tr>
					<tr>
						<td>
							Create Date:
						</td>
						<td>
							<input type="text" name="import_date" id="import_date" class="inp_feild" style="width:50%;" value="<?php 
							 if($prospect_id==""){echo date("d-m-Y");} else { echo $import_date; }?>" readonly tabindex="25">
						</td>
					</tr>
					<tr>
						<td style="width:138px;" valign="top">
							Affiliate:							
						</td>
						<td>
							<?php
							$QryCondition = "";
							if($user_type=="Affiliate")
							{
								$AffDDQryCondition = " and a_user_id='".$_SESSION['user_id']."'";

								$GetLicensIdQry = "select l_id from tbl_affiliate where a_user_id='".$_SESSION['user_id']."'";
								$GetLicensIdQryRes = mysql_query($GetLicensIdQry);
								$GetLicensIdQryRow = mysql_fetch_array($GetLicensIdQryRes);
								$L_idFromAffiliateRow = $GetLicensIdQryRow['l_id'];

								$LicenseeQryCondition = " and licensee_id='".$L_idFromAffiliateRow."'";
								
							}

							if($user_type=="Licensee")
							{

								$LicenseeQryCondition = " and l_user_id='".$_SESSION['user_id']."'";
								

								$GetLicenceeId ="select licensee_id from  tbl_licensee where l_user_id = ".$_SESSION['user_id'];
								$PrepareGetLicenceeId=$DBCONN->prepare($GetLicenceeId);
								$PrepareGetLicenceeId->execute();
								$GetLicenceeIdRow=$PrepareGetLicenceeId->fetch();
								$LoggedInLicennseeId = $GetLicenceeIdRow['licensee_id'];

								$AffDDQryCondition = " and l_id='".$LoggedInLicennseeId."'";
								
							}
							
							?>
							<select name="affiliate" id="affiliate" class="inp_feild" onchange="get_Licensee_details(this.value)" style="width:50%;">
							<option value="0">Select</option>
							<?php
							

							$get_Qry="select * from tbl_affiliate where 1=1 ".$AffDDQryCondition;
							
							$get_Res=mysql_query($get_Qry);
							while($get_value=mysql_fetch_array($get_Res))
							{ 
								$SelectedString = "";
								$TblAffiliateId = $get_value['affiliate_id'];

								if($TblAffiliateId == $affiliate_id)
									$SelectedString = "selected";

								?>
							<option value="<?php echo $TblAffiliateId;?>" <?php echo $SelectedString;?>><?php echo stripslashes($get_value["full_name"]);?></option>
							<?php
							}
							?>
							</select>
						</td>
					</tr>
					<tr>
						<td valign="top">
							Licensee:
						</td>
						<td>
							<select name="licensee" id="licensee" class="input_field" style="width:50%;" tabindex="27">
							<option value="0">Select</option>					
							<?php							
							$getbusinessQry="select * from  tbl_licensee where 1 = 1 ".$LicenseeQryCondition;
							$prepget_business_qry=$DBCONN->prepare($getbusinessQry);
							$prepget_business_qry->execute();
							while($getbusinessRow=$prepget_business_qry->fetch())
							{
							?>
							<option value="<?php echo $getbusinessRow["licensee_id"];?>" <?php if($licence_id==$getbusinessRow["licensee_id"]) echo " selected";?>><?php echo $getbusinessRow["business_name"];?></option>
							<?php
							}
							?>
							</select>
						</td>
					</tr>
					<tr>
						<td colspan="2">
							<table cellspacing="0" cellpadding="0" width="100%" border="0">
								<tr>
									<td>
										<div class="form_actions">
											<input type="button" value="Back to Prospect List" class="add_btn"   onclick="document.location='prospectinglist.php'">
										</div>
									</td>
									<td>
										<div class="form_actions" style="text-align:right;" id="upd_pros_btn" class="upd_pros_btn">
											  <input type="button" value="<?php echo $mode_value;?> Record" class="add_btn update_prospect"   >  
										</div>
									</td>
									<!-- <td>
										<?php if($callstatus == "Affiliate Promo Code"){
											if($id == "3" || $id == "173" || $user_name=="Cedric Amoyal" || $user_name=="John Talbot"){
										?>
										<div class="form_actions" style="text-align:right;" id="upd_pros_btn">
											<input type="button" value="<?php echo $mode_value;?> Record" class="add_btn update_prospect">  
										</div>
										<?php
											}
										}?>
									</td> -->
								</tr>
							</table>
						</td>
					</tr>
				 </table>
				</form>
				</div>
				<!--  -->
				<!--  -->
			  </div>
			 </div>
			</div>
			<script>
			validateForm();
			</script>

			<?php
			 
			$getDemoQry1="select * from tbl_affiliate where a_user_id='".$_SESSION['user_id']."'";
			$getDemoRes1=mysql_query($getDemoQry1);
			$getDemoRow1=mysql_fetch_array($getDemoRes1);
			if($affiliate_id!=""){
			   $affiliate_user_id=$affiliate_id;
			}
			else{
				$affiliate_user_id=$getDemoRow1["affiliate_id"];
			}
			if($affiliate_user_id!=""){
			?>
			<script> 
				get_Licensee_details('<?php echo $affiliate_user_id;?>');
			</script>
			<?php
			}

	 
include("includes/footer.php");


?>