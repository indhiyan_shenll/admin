<?php
include("../includes/configure.php");
include("../includes/session_check.php");
$business_id=$_GET["business_id"];
if($business_id!=""){
	$deleteQry="delete from tbl_business_type where business_id=:business_id";
	$prepdeleteQry=$DBCONN->prepare($deleteQry);
	$deleteRes=$prepdeleteQry->execute(array(":business_id"=>$business_id));
	if($deleteRes){
		header("Location:business_list.php");
		exit;
	}
}
if(isset($_GET["dord"]) && $_GET["dord"]!="")
{
	$dorder=$_GET["dord"];
	$arr1=explode("*",$dorder);
	
	foreach($arr1 as $value)
	{
		$arr2=explode("^",$value);
		$id=$arr2[0];
		$order=$arr2[1];
		//mysql_query("update  tbl_special_one set display_order='".$order."' where special_one_id='".$id."'");
		$update_qry="update tbl_business_type set display_order=:dispaly_order where business_id=:business_id";
        $prepupdate_qry=$DBCONN->prepare($update_qry);
		$updateresults=$prepupdate_qry->execute(array(":dispaly_order"=>$order,":business_id"=>$id));
	}
	header("Location:business_list.php");
	exit;
}
include("includes/header.php");
?>
 
	<body>
		<div>
			<div style="margin-left:auto;margin-right:auto;">
				 
				<div class="content">
					<div class="list_content">
						<div class="form_actions" style="padding-bottom:45px;">
							<input type="button" value="Back To Admin Features" class="add_btn" onclick="document.location='admin_features.php'" style="float:left;">
							<input type="button" value="Add Business Type" class="add_btn" onclick="document.location='edit_business_type.php'" style="float:right;">
						</div>
						<div class="header_div">
							<table cellspacing="0" cellpadding="0" width="100%" class="tbl_header" >
								
								<tr>
									<th width="10%">No</th>
									<th width="60%"  style="cursor:pointer">Business Type&nbsp;&nbsp;</th>
									 <th width="20%" style="cursor:pointer">Sort Order&nbsp;&nbsp;</th>
								    <th width="10%">Delete?</th>
								</tr>
							</table>
						</div>
						<div class="gap" ></div> 
						<table cellspacing="0" cellpadding="0" width="100%" class="tbl-body" >
						<?php
						 $get_business_qry="select * from tbl_business_type order by display_order asc";
						 $prepget_business_qry=$DBCONN->prepare($get_business_qry);
						 $prepget_business_qry->execute();
                         $getCnt =$prepget_business_qry->rowCount();
						 if($getCnt>0){
							 $i=1;
							  while($getRow=$prepget_business_qry->fetch()){
                                   $id = $getRow['business_id'];
								   $display_order=stripslashes($getRow['display_order']);
									if($i%2==1){
									  $bgcolor="#a5a5a5";
									}
									else{
										$bgcolor="#d2d1d1";
									}
									 if($getCnt>2)
										{

										    $sqlqry	="select display_order,business_id from tbl_business_type where display_order>:display_order order by display_order asc limit 1";
                                            $prepget_qry1=$DBCONN->prepare($sqlqry);
						                    $prepget_qry1->execute(array(":display_order"=>$display_order));
                                            $dispaly_count =$prepget_qry1->rowCount();
											if($dispaly_count>0)
												{
														$fetch=$prepget_qry1->fetch();
														$nextorder=$fetch["display_order"];
														$nextid=$fetch["business_id"];
												}
											
										}
										$uparrow="<img src='images/arrow_up.png' border='0'>";
										$downarrow="<img src='images/arrow_down.png' border='0'>";
										$enduparrow="<img src='images/arrow_up.png' border='0' style=\"margin-left:10px;\">";
										$startdownarrow="<img src='images/arrow_down.png' border='0' style=\"margin-left:10px;\">";
										if($i==$getCnt && $getCnt==1)
										  $display="";
										else if($i==1)
										 $display="<a href='business_list.php?dord=$nextid^$display_order*$id^$nextorder'>".$startdownarrow."</a>";
										else if($i>1 && $i<>$getCnt)
										  $display="<a href='business_list.php?dord=$nextid^$display_order*$id^$nextorder'>".$downarrow."</a>&nbsp;&nbsp;<a href='business_list.php?dord=$previd^$display_order*$id^$prevorder'>".$uparrow."</a>";
										else if($i==$getCnt)
										  $display="<a href='business_list.php?dord=$previd^$display_order*$id^$prevorder'>".$enduparrow."</a>";
										$prevorder=$display_order;
										$previd=$id;
								?>
						     <tr bgcolor="<?php echo $bgcolor;?>">
								<td width="10%"><?php echo $i;?></td>
								<td width="60%"><?php echo stripslashes($getRow["business_type"]);?></td>
								  <td width="20%"><span style="margin-left:10px;"><?php echo $display;?></span></td>
							    <td width="10%"><a href="edit_business_type.php?business_id=<?php echo $getRow["business_id"];?>">Edit</a>&nbsp;&nbsp;/&nbsp;&nbsp;<a href="business_list.php?business_id=<?php echo $getRow["business_id"];?>" onclick="return confirm('Are you sure want to delete this business type?')">Delete</a></td>
						   </tr>
						   <?php
						  $i++;
						 }
						?>
						<tr><td height="10px"></td></tr>
						<tr>
							<td colspan="3">
							<div class="form_actions" style="text-align:left;position:relative;" >
							<input type="button" value="Back To Admin Features" class="add_btn" onclick="document.location='admin_features.php'">
							</td>
						</tr>
						<?php
						}
						else{
								echo "<tr bgcolor='#a5a5a5'><td colspan=\"3\"><center>No Business  type(s) found.</center></td></tr>";
						}
						?>
					 </table>
					</div>
				</div>
			</div>
		</div>
<?php
include("includes/footer.php");
?>