<?php
include("../includes/configure.php");
include("../includes/session_check.php");
$select_box_content="";
$getQry_1="SELECT  DISTINCT `product_page` FROM `tbl_apps`  order by `product_page` asc";
$prepgetQry1=$DBCONN->prepare($getQry_1);
$prepgetQry1->execute();
while($getRow1=$prepgetQry1->fetch()){
    $y=$getRow1["product_page"];
    $select_box_content.="<option value='".$y."'>".$y."</option>";	
}
$dbdatetime=date('Y-m-d H:i:s',strtotime('now'));
$codesArr	=array("GBP","EUR","USD","AUD","SEK");
$personalArr	= array();
$businessArr	= array();
$corporateArr	= array();
$promoArr	= array();
$productArr	= array();
$crazy_textArr= array();	
$crazy_popArr= array();	
$personal_packArr= array();	
for($j=0;$j<count($codesArr);$j++){
		$getSettingsQry	=	$DBCONN->prepare("select * from tbl_simulator_page where currency_code=:currency_code");
		$getSettingsQry->execute(array(":currency_code"=>$codesArr[$j]));
	    $getSettingsRow = $getSettingsQry->fetch(PDO::FETCH_ASSOC); 
		$personalArr[]	= $getSettingsRow["personal"];
		$businessArr[]	= $getSettingsRow["business"];
		$corporateArr[]	= $getSettingsRow["corporate"];
		$promoArr[]	= $getSettingsRow["promo_code"];
		$productArr[]	= $getSettingsRow["product_page"];
		$crazy_textArr[]	= $getSettingsRow["crazy_popup_text"];
		$crazy_popArr[]	= $getSettingsRow["crazy_popup"];
		$personal_packArr[]	= $getSettingsRow["personal_pack"];
		
}	
 
if(isset($_POST["hdn_value"])){
	$codesCnt=count($codesArr);
	$updateCnt	=   0;
	$crazy_value="0";
	$personal=$_POST["personal"];
	$business=$_POST["business"];
	$corporate=$_POST["corporate"];
    $promo=$_POST["promo"];
	$product_page=$_POST["product_page"];
	$crazy_text=$_POST["crazy_text"];
	$var = array_keys($_POST["crazy"]);
	$var1 = array_keys($_POST["personal_pack"]);
	for($i=0;$i<count($codesArr);$i++){
	   if(in_array($codesArr[$i], $var)){
		 $crazy_value="1";
	  }
	  else{
		$crazy_value="0";
	  }
	   if(in_array($codesArr[$i], $var1)){
		  $personal_pack_value="1";
	  }
	  else{
		 $personal_pack_value="0";
	  }
	  $updateSetQry	=	$DBCONN->prepare("update tbl_simulator_page set personal='".$personal[$i]."',business='".$business[$i]."',corporate='".$corporate[$i]."',promo_code='".$promo[$i]."',product_page='".$product_page[$i]."'
		,crazy_popup_text='".trim($crazy_text[$i])."',personal_pack='".trim($personal_pack_value)."',crazy_popup='".trim($crazy_value)."',modified_date='".$dbdatetime."' where currency_code='".$codesArr[$i]."'");
		$updateSetQry->execute();
	  
	}
    header("Location:manage_simulator.php?msg=success");
    exit;
   
}
include("includes/header.php");
?>
<style>
.tbl_header th{
	font-size:13px;
	border-bottom:1px solid #D9D9D9;
	text-align:left;
	font-family:arial;
	color:white;
}
.tbl-body{
	font-size:12px;
	line-height:25px;
	font-family:arial;
}
a{
	color:black;
}
table { table-layout: fixed; }
td { width: 20%; }
.inp_feild{
	width:100%;
	margin:auto;
}
 
</style>
 
	<body>
		<div>
			<div style="margin-left:auto;margin-right:auto;">
			 
				<div class="content">
					<div class="list_content">
						<div class="form_actions" style="padding-bottom:45px;">
							<input type="button" value="Back To Admin Features" class="add_btn" onclick="document.location='admin_features.php'" style="float:left;">
							 
						</div>
						<div style="padding-bottom:20px;">
							 
						</div>
						<form name="simulator_form" id="simulator_form" Action="" method="post">
						<table cellspacing="0" cellpadding="10"   class="tbl_header" border="0" style="text-align:center;color:white;width: 1280px;background: #2661a7;">
                        <tr>
							 <td align="center"  style="width:8%;"></td>
							 <td align="center">Trial</td>
							 <td align="center">Business</td>
							 <td align="center">Corporate</td>
							 <td align="center">Crazy Promo Code</td>
							  <td align="center" style="width: 21%;">Crazy Product Page</td>
							  <td align="center" style="width: 21%;">Crazy Popup text</td>
							   <td align="center" style="width: 15%;">Crazy Popup</td>
							    <td align="center" style="width: 17%;">Personal Pack</td>
						</tr>
						<tr>
							<td align="center">GBP</td>
							<td>
											<select name="personal[]" id="personal_uk" class="inp_feild">
												<option value="0">Select</option>
												<?php
                                                 echo $select_box_content;
												 ?>
											</select>
											<?php 
											if($personalArr[0]!=""){
											?>
											   <script language="javascript">document.getElementById("personal_uk").value="<?php echo $personalArr[0];?>"</script>
											<?php 
											}
											?>
							 </td>
							  <td>
											<select name="business[]" id="business_uk" class="inp_feild">
												<option value="0">Select</option>
												<?php
                                                 echo $select_box_content;
												 ?>
											</select>
											<?php 
											if($businessArr[0]!=""){
											?>
											   <script language="javascript">document.getElementById("business_uk").value="<?php echo $businessArr[0];?>"</script>
											<?php 
											}
											?>
							 </td>
							  <td>
											<select name="corporate[]" id="corporate_uk" class="inp_feild">
												<option value="0">Select</option>
												<?php
                                                 echo $select_box_content;
												 ?>
											</select>
											<?php 
											if($corporateArr[0]!=""){
											?>
											   <script language="javascript">document.getElementById("corporate_uk").value="<?php echo $corporateArr[0];?>"</script>
											<?php 
											}
											?>
							 </td>
							  <td>
							  
							  <input type="text" name="promo[]"   class="inp_feild" value="<?php echo  stripslashes($promoArr[0]);?>">
										 
							 </td>
							
							  <td>
											<select name="product_page[]" id="product_uk" class="inp_feild">
												<option value="0">Select</option>
												<?php
                                                 echo $select_box_content;
												 ?>
											</select>
											<?php 
											if($productArr[0]!=""){
											?>
											   <script language="javascript">document.getElementById("product_uk").value="<?php echo $productArr[0];?>"</script>
											<?php 
											}
											?>
							 </td>
							   <td>
							  
							  <input type="text" name="crazy_text[]"   class="inp_feild" value="<?php echo  stripslashes($crazy_textArr[0]);?>">
										 
							 </td>
							 <td><input type="checkbox" name="crazy[GBP][]" value="1" <?php if($crazy_popArr[0]=="1") echo " checked";?>></td>
							 <td><input type="checkbox" name="personal_pack[GBP][]" value="1" <?php if($personal_packArr[0]=="1") echo " checked";?>></td>
							 
						</tr>
						<tr>
						   <td align="center">EUR</td>
						   <td>
											<select name="personal[]" id="personal_eur" class="inp_feild">
												<option value="0">Select</option>
												<?php
                                                 echo $select_box_content;
												 ?>
											</select>
											<?php 
											if($personalArr[1]!=""){
											?>
											   <script language="javascript">document.getElementById("personal_eur").value="<?php echo $personalArr[1];?>"</script>
											<?php 
											}
											?>
							 </td>
							 <td>
											<select name="business[]" id="business_eur" class="inp_feild">
												<option value="0">Select</option>
												<?php
                                                 echo $select_box_content;
												 ?>
											</select>
											<?php 
											if($businessArr[1]!=""){
											?>
											   <script language="javascript">document.getElementById("business_eur").value="<?php echo $businessArr[1];?>"</script>
											<?php 
											}
											?>
							 </td>
							 	<td>
											<select name="corporate[]" id="corporate_eur" class="inp_feild">
												<option value="0">Select</option>
												<?php
                                                 echo $select_box_content;
												 ?>
											</select>
											<?php 
											if($corporateArr[1]!=""){
											?>
											   <script language="javascript">document.getElementById("corporate_eur").value="<?php echo $corporateArr[1];?>"</script>
											<?php 
											}
											?>
							 </td>
							  <td>
							  
							    <input type="text" name="promo[]"   class="inp_feild" value="<?php echo  stripslashes($promoArr[1]);?>">
										 
							 </td>
							  <td>
									<select name="product_page[]" id="product_eur" class="inp_feild">
												<option value="0">Select</option>
												<?php
                                                 echo $select_box_content;
												 ?>
											</select>
											<?php 
											if($productArr[1]!=""){
											?>
											   <script language="javascript">document.getElementById("product_eur").value="<?php echo $productArr[1];?>"</script>
											<?php 
											}
											?>
							 </td>
							   <td>
							  
							  <input type="text" name="crazy_text[]"   class="inp_feild" value="<?php echo  stripslashes($crazy_textArr[1]);?>">
										 
							 </td>
							 <td><input type="checkbox" name="crazy[EUR][]" value="1" <?php if($crazy_popArr[1]=="1") echo " checked";?>></td>
							 <td><input type="checkbox" name="personal_pack[EUR][]" value="1" <?php if($personal_packArr[1]=="1") echo " checked";?>></td>
							 
						</tr>
						<tr>
							<td align="center">USD</td>
								<td>
											<select name="personal[]" id="personal_usd" class="inp_feild">
												<option value="0">Select</option>
												<?php
                                                 echo $select_box_content;
												 ?>
											</select>
											<?php 
											if($personalArr[2]!=""){
											?>
											   <script language="javascript">document.getElementById("personal_usd").value="<?php echo $personalArr[2];?>"</script>
											<?php 
											}
											?>
							 </td>
							 <td>
											<select name="business[]" id="business_usd" class="inp_feild">
												<option value="0">Select</option>
												<?php
                                                 echo $select_box_content;
												 ?>
											</select>
											<?php 
											if($businessArr[2]!=""){
											?>
											   <script language="javascript">document.getElementById("business_usd").value="<?php echo $businessArr[2];?>"</script>
											<?php 
											}
											?>
							 </td>
							 <td>
											<select name="corporate[]" id="corporate_usd" class="inp_feild">
												<option value="0">Select</option>
												<?php
                                                 echo $select_box_content;
												 ?>
											</select>
											<?php 
											if($corporateArr[2]!=""){
											?>
											   <script language="javascript">document.getElementById("corporate_usd").value="<?php echo $corporateArr[2];?>"</script>
											<?php 
											}
											?>
							 </td>
							   <td>
							    <input type="text" name="promo[]"   class="inp_feild" value="<?php echo  stripslashes($promoArr[2]);?>">
							  
										 
							 </td>
							  <td>
									<select name="product_page[]" id="product_usd" class="inp_feild">
												<option value="0">Select</option>
												<?php
                                                 echo $select_box_content;
												 ?>
											</select>
												<?php 
											if($productArr[2]!=""){
											?>
											   <script language="javascript">document.getElementById("product_usd").value="<?php echo $productArr[2];?>"</script>
											<?php 
											}
											?>
							 </td>
							   <td>
							  
							  <input type="text" name="crazy_text[]"   class="inp_feild" value="<?php echo  stripslashes($crazy_textArr[2]);?>">
										 
							 </td>
							 <td><input type="checkbox" name="crazy[USD][]" value="1" <?php if($crazy_popArr[2]=="1") echo " checked";?>></td>
							  <td><input type="checkbox" name="personal_pack[USD][]" value="1" <?php if($personal_packArr[2]=="1") echo " checked";?>></td>
							 

							 
						</tr>
						<tr>
							 <td align="center">AUD</td>
							  <td>
											<select name="personal[]" id="personal_aud" class="inp_feild">
												<option value="0">Select</option>
												<?php
                                                 echo $select_box_content;
												 ?>
											</select>
											<?php 
											if($personalArr[3]!=""){
											?>
											   <script language="javascript">document.getElementById("personal_aud").value="<?php echo $personalArr[3];?>"</script>
											<?php 
											}
											?>
							  </td>
							   <td>
											<select name="business[]" id="business_aud" class="inp_feild">
												<option value="0">Select</option>
												<?php
                                                 echo $select_box_content;
												 ?>
											</select>
											<?php 
											if($businessArr[3]!=""){
											?>
											   <script language="javascript">document.getElementById("business_aud").value="<?php echo $businessArr[3];?>"</script>
											<?php 
											}
											?>
							  </td>
							   <td>
											<select name="corporate[]" id="corporate_aud" class="inp_feild">
												<option value="0">Select</option>
												<?php
                                                 echo $select_box_content;
												 ?>
											</select>
											<?php 
											if($corporateArr[3]!=""){
											?>
											   <script language="javascript">document.getElementById("corporate_aud").value="<?php echo $corporateArr[3];?>"</script>
											<?php 
											}
											?>
							  </td>
							    <td>
							  
							   <input type="text" name="promo[]"   class="inp_feild" value="<?php echo  stripslashes($promoArr[3]);?>">
										 
							 </td>
							 <td>
									<select name="product_page[]" id="product_aud" class="inp_feild">
												<option value="0">Select</option>
												<?php
                                                 echo $select_box_content;
												 ?>
											</select>
										   <?php 
											if($productArr[3]!=""){
											?>
											   <script language="javascript">document.getElementById("product_aud").value="<?php echo $productArr[3];?>"</script>
											<?php 
											}
											?>
							 </td>
							   <td>
							  
							  <input type="text" name="crazy_text[]"   class="inp_feild" value="<?php echo  stripslashes($crazy_textArr[3]);?>">
										 
							 </td>
							  <td><input type="checkbox" name="crazy[AUD][]" value="1" <?php if($crazy_popArr[3]=="1") echo " checked";?>></td>
							   <td><input type="checkbox" name="personal_pack[AUD][]" value="1" <?php if($personal_packArr[3]=="1") echo " checked";?>></td>
							 


						</tr>
								<tr>
							 <td align="center">SEK</td>
							  <td>
											<select name="personal[]" id="personal_sek" class="inp_feild">
												<option value="0">Select</option>
												<?php
                                                 echo $select_box_content;
												 ?>
											</select>
											<?php 
											if($personalArr[4]!=""){
											?>
											   <script language="javascript">document.getElementById("personal_sek").value="<?php echo $personalArr[4];?>"</script>
											<?php 
											}
											?>
							  </td>
							   <td>
											<select name="business[]" id="business_sek" class="inp_feild">
												<option value="0">Select</option>
												<?php
                                                 echo $select_box_content;
												 ?>
											</select>
											<?php 
											if($businessArr[4]!=""){
											?>
											   <script language="javascript">document.getElementById("business_sek").value="<?php echo $businessArr[4];?>"</script>
											<?php 
											}
											?>
							  </td>
							   <td>
											<select name="corporate[]" id="corporate_sek" class="inp_feild">
												<option value="0">Select</option>
												<?php
                                                 echo $select_box_content;
												 ?>
											</select>
											<?php 
											if($corporateArr[4]!=""){
											?>
											   <script language="javascript">document.getElementById("corporate_sek").value="<?php echo $corporateArr[4];?>"</script>
											<?php 
											}
											?>
							  </td>
							    <td>
							  
							   <input type="text" name="promo[]"   class="inp_feild" value="<?php echo  stripslashes($promoArr[4]);?>">
										 
							 </td>
							 <td>
									<select name="product_page[]" id="product_sek" class="inp_feild">
												<option value="0">Select</option>
												<?php
                                                 echo $select_box_content;
												 ?>
											</select>
										   <?php 
											if($productArr[4]!=""){
											?>
											   <script language="javascript">document.getElementById("product_sek").value="<?php echo $productArr[4];?>"</script>
											<?php 
											}
											?>
							 </td>
							   <td>
							  
							  <input type="text" name="crazy_text[]"   class="inp_feild" value="<?php echo stripslashes($crazy_textArr[4]);?>">
										 
							 </td>
							  <td><input type="checkbox" name="crazy[SEK][]" value="1" <?php if($crazy_popArr[4]=="1") echo " checked";?>></td>
							    <td><input type="checkbox" name="personal_pack[SEK][]" value="1" <?php if($personal_packArr[4]=="1") echo " checked";?>></td>


						</tr>
					      <tr>
							 <td  colspan="9">
									<div class="form_actions" style="text-align:left;padding-left:0px;width:50%;float:left">
										<input type="button" value="Back To Admin Features" class="add_btn" onclick="document.location='admin_features.php'" style="float:left;">
									</div>
									<div class="form_actions" style="text-align:right;width:40%;float:right">
									  <input type="button" value="Update Defaults" class="add_btn" id="update_simulator">
									</div>
								
							</td>
								 
							</tr>
						 
                       </table>
					   
					   <input type="hidden" name="hdn_value" id="hdn_value" value="submit">
					<form>
				</div>
			 </div>
			</div>
		</div>
   <?php
	include("includes/footer.php");
	?>
<script type="text/javascript">
$(document).ready(function(){
	 $("#update_simulator").click(function(){
		 $("#simulator_form").submit();
		 
	});
});
</script>
 