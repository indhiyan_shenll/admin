<?php
include("../includes/configure.php");
include("../includes/session_check.php");
$dbdatetime=date('Y-m-d H:i:s',strtotime('now'));
$saleperson_id=$_GET["sales_id"];
if($saleperson_id!=""){
	//$saleperson_id
	$getQry="select * from  tbl_sales_person where sales_person_id=:sales_person_id";
	$prepgetQry=$DBCONN->prepare($getQry);
	$prepgetQry->execute(array(":sales_person_id"=>$saleperson_id));
	//$getRes=mysql_query($getQry);
	$getRow=$prepgetQry->fetch();
	$Salesperson=stripslashes($getRow["salesperson"]);
	$email=stripslashes($getRow["email"]);
	$sales_manager=stripslashes($getRow["sales_manager_id"]);
	//print_r($getRow);
	$mode="Edit";
	$value="Update";
}
else{
	$mode="Add";
	$value="Create";
}
if(isset($_POST["person"])){
	$Person=addslashes(trim($_POST["person"]));
	$person_email=addslashes(trim($_POST["person_email"]));
	$sales_manager=addslashes(trim($_POST["sales_manager"]));
    if($mode=="Edit"){
		//$Person  $person_email $sales_manager  $saleperson_id
		$updateQry="update tbl_sales_person set salesperson=:person,email=:email,sales_manager_id=:sales_manager_id,modified_date=:modified_date where sales_person_id=:sales_person_id";
		$prepupdateQry=$DBCONN->prepare($updateQry);
		$updateRes=$prepupdateQry->execute(array(":person"=>$Person,":email"=>$person_email,":sales_manager_id"=>$sales_manager,":modified_date"=>$dbdatetime,":sales_person_id"=>$saleperson_id));
		//$updateRes=mysql_query($updateQry);
		if($updateRes){
			header("Location:person_list.php");
			exit;
		}
	}
	else{
		//$Person   $person_email  $sales_manager
		$insertQry="insert into  tbl_sales_person(salesperson,email,sales_manager_id,added_date,modified_date)values(:salesperson,:email,:sales_manager_id,:added_date,:modified_date)";
		$prepinsertQry=$DBCONN->prepare($insertQry);
		$insertRes=$prepinsertQry->execute(array(":salesperson"=>$Person,":email"=>$person_email,":sales_manager_id"=>$sales_manager,":added_date"=>$dbdatetime,":modified_date"=>$dbdatetime));
		//$insertRes=mysql_query($insertQry);
		if($insertRes){
			header("Location:person_list.php");
			exit;
		}
	}
}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<title>MAR Pipeline System</title>
		<link rel="shortcut icon" href="images/Fav.ico" type="image/ico">
		<meta name="Generator" content="EditPlus">
		<meta name="Author" content="">
		<meta name="Keywords" content="">
		<meta name="Description" content="">
		<script type="text/javascript" src="js/jquery.js"></script>
		<script>
		var mode="<?php echo $mode;?>";
		</script>
		<script type="text/javascript" src="js/validate.js"></script>
		<style>
			body{
				margin:0;
				color:#D9D9D9;
				background:#455A68;
				font-family:arial;
			}
			.header{
				height:70px;
				background:#1C242A;
			}
			.content{
				background:#455A68;
				min-height:600px;
			}
			
			.form_actions{
				padding-top:15px;
				padding-left:5px;
				padding-bottom:30px;
			}
			.form_actions .add_btn{
				cursor:pointer;
				border-radius:0px;
				background:#0D0D0D;
				color:#D9D9D9;
				border-color:#D9D9D9;
				padding:5px 15px 5px 15px;
				font-family:arial;

			}
			.list_content{
				width:950px;
				margin-left:40px;
				/*margin-left:auto;
				margin-right:auto;*/
			}
			.tbl_header th{
				font-size:13px;
				border-bottom:1px solid #D9D9D9;
				text-align:left;
				font-family:arial;
			}
			.tbl-body{
				font-size:12px;
				font-family:arial;
			}
			a{
				color:black;
			}
			.inp_feild{
				border-radius:2px;
				border:none;
				width:100%;
			}
		</style>
	</head>
	<body>
		<div>
			<div style="margin-left:auto;margin-right:auto;">
				<div class="header">
					<span style="float:right;margin-right:20px;margin-top:5px;"><a href="logout.php" style="color:white;text-decoration:none;">Logout</a></span>
					<img src="images/myappyrestaurants.png" style="margin-top:10px;margin-left:40px;">
				</div>
				<div class="content">
					<div class="list_content">
						<h1 style="font-size:25px;padding-top:15px;padding-bottom:15px;margin:0px;">New Salesperson </h1>
						<form name="person_form" id="person_form" method="post" enctype="multipart/form-data">
						<input type="hidden" name="person_form_id" id="person_form_id" value="<?php echo $saleperson_id;?>">
						<table cellspacing="15" cellpadding="0" border="0" width="70%">
							<tr>
								<td style="width:138px;">
									 Salesperson<font color="red">*</font>:
								</td>
								<td>
									<input type="text" name="person" id="person" class="inp_feild" value="<?php echo $Salesperson;?>">
								</td>
							</tr>
							<tr>
								<td style="width:138px;">
									 Salesperson Email Address<font color="red">*</font>:
								</td>
								<td>
									<input type="text" name="person_email" id="person_email" class="inp_feild" value="<?php echo $email;?>">
								</td>
							</tr>
							<tr>
								<td>
									Sales Manager<font color="red">*</font>:
								</td>
								<td>
									<select name="sales_manager" id="sales_manager" class="inp_feild">
										<option value="0">Select</option>
										<?php
											$getmanagerQry="select * from tbl_salesmanager order by sales_manager_id asc";
											$getmanagerRes=mysql_query($getmanagerQry);
											while($getmanagertRow=mysql_fetch_array($getmanagerRes)){
										?>
											<option value="<?php echo stripslashes($getmanagertRow["sales_manager"]);?>"<?php if($sales_manager==$getmanagertRow["sales_manager"]) echo " selected";?>><?php echo stripslashes($getmanagertRow["sales_manager"]);?></option>
										<?php
											}
										?>
										
									</select>

								</td>
								
							</tr>
							<tr>
							     <td>
									<div class="form_actions" style="text-align:left;">
										<input type="button" value="Back To Salespersons List" class="add_btn"  onclick="document.location='person_list.php'">
									
								</td>
								<td>
									  <div class="form_actions" style="text-align:right;">
										<input type="button" value="<?php echo $value;?> Salesperson" class="add_btn" id="add_person">
									</div>
								</td>
							</tr>
						</table>
						</form>
					</div>
					
				</div>
			</div>
		</div>
	</body>
</html>