<?php
include("../includes/configure.php");
include("../includes/session_check.php");
$promocode_id=$_GET["promocode_id"];
$sort=$_GET["sort"];
$field=$_GET["field"];
if($sort==""){
	$sort="asc";
}
if($field==""){
	$field="promocode_id";
}

if($field=="promocode"){
	$fieldname="promocode";
	$order=" order by ".$fieldname." ".$sort;
	if($sort=="asc"){
		$rsort="desc";
		$rpath="images/up.png";
	}
	else{
		$rsort="asc";
		$rpath="images/down.png";
	}
}

if($promocode_id!=""){
	$deleteQry="delete from tbl_promocodes where promocode_id='".$promocode_id."'";
	$deleteRes=mysql_query($deleteQry);
	if($deleteRes){
		header("Location:promocodelist.php");
		exit;
	}
}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<title>MAR Pipeline System</title>
		<link rel="shortcut icon" href="images/Fav.ico" type="image/ico">
		<meta name="Generator" content="EditPlus">
		<meta name="Author" content="">
		<meta name="Keywords" content="">
		<meta name="Description" content="">
		<style>
			body{
				margin:0;
				color:black;
				background:#455A68;
				font-family:arial;
			}
			.header{
				height:70px;
				background:#1C242A;
			}
			.content{
				background:#455A68;
				min-height:600px;
			}
			
			.form_actions{
				padding-top:15px;
				padding-left:5px;
				padding-bottom:30px;
			}
			.form_actions .add_btn{
				cursor:pointer;
				border-radius:0px;
				background:#0D0D0D;
				color:#D9D9D9;
				border-color:#D9D9D9;
				padding:5px 15px 5px 15px;
				font-family:arial;
			}
			.list_content{
				width:950px;
				margin-left:40px;
				/*margin-left:auto;
				margin-right:auto;*/
			}
			.tbl_header th{
				font-size:13px;
				border-bottom:1px solid #D9D9D9;
				text-align:left;
				font-family:arial;
				color:white;
			}
			.tbl-body{
				font-size:12px;
				line-height:25px;
				font-family:arial;
			}
			a{
				color:black;
			}
		</style>
	</head>
	<body>
		<div>
			<div style="margin-left:auto;margin-right:auto;">
				<div class="header">
					<span style="float:right;margin-right:20px;margin-top:5px;"><a href="logout.php" style="color:white;text-decoration:none;">Logout</a></span>
					<img src="images/myappyrestaurants.png" style="margin-top:10px;margin-left:40px;">
				</div>
				<div class="content">
					<div class="list_content">
						<div class="form_actions">
							<input type="button" value="Add Promo Code" class="add_btn" onclick="document.location='promocode.php'">
						</div>
						<div style="padding-bottom:12px;">
							<table cellspacing="0" cellpadding="0" width="100%" class="tbl_header">
								
								<tr>
									<th width="10%">No</th><!-- onclick="document.location='statuslist.php?sort=<?php echo $dsort;?>&field=sno'" -->
									<th width="80%" onclick="document.location='promocodelist.php?sort=<?php echo $rsort;?>&field=promocode'" style="cursor:pointer">Promo Code&nbsp;&nbsp;<?php if($rpath!=""){?><img src="<?php echo $rpath;?>" style="width:10px;height:10px;"><?php }?></th>
									<th width="10%">Delete?</th>
								</tr>
							</table>
						</div>
						<table cellspacing="0" cellpadding="0" width="100%" class="tbl-body">
						<?php
							$getQry="select * from  tbl_promocodes".$order;
							//exit;
							$getRes=mysql_query($getQry);
							$getCnt=mysql_num_rows($getRes);
							if($getCnt>0){
								$i=1;
								while($getRow=mysql_fetch_array($getRes)){
									
									if($i%2==1){
										$bgcolor="#a5a5a5";
									}
									else{
										$bgcolor="#d2d1d1";
									}
						?>
							<tr bgcolor="<?php echo $bgcolor;?>">
								<td width="10%"><?php echo $i;?></td>
								<td width="80%"><?php echo stripslashes($getRow["promocode"]);?></td>
								<td width="10%"><a href="promocode.php?promocode_id=<?php echo $getRow["promocode_id"];?>">Edit</a>&nbsp;&nbsp;/&nbsp;&nbsp;<a href="promocodelist.php?promocode_id=<?php echo $getRow["promocode_id"];?>" onclick="return confirm('Are you sure want to delete this promo code?')">Delete</a></td>
							</tr>
							
						<?php
							$i++;
								}
								?>
							<tr><td height="10px"></td></tr>
							
								<?php
							}
							else{
								echo "<tr bgcolor='#a5a5a5'><td colspan=\"3\"><center>No promo code(s) found.</center></td></tr>";
							}
						?>
						<tr>
								<td colspan="3">
								<div class="form_actions" style="text-align:left;position:relative;" >
								<input type="button" value="Back To Admin Features" class="add_btn" onclick="document.location='admin_features.php'">
								</td>
							</tr>
						</table>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>