<?php
include("../includes/configure.php");
include("../includes/session_check.php");
$dbdatetime=date('Y-m-d H:i:s',strtotime('now'));

if(isset($_POST["surveyquest"])){
    $questgreen  =$_POST["questgreen"];
    $questamber  =$_POST["questamber"];
	$s=1;
	for($CountRecord = 0; $CountRecord < count($_POST["questgreen"]); $CountRecord++){
		
	    $questgreen = str_replace('%','',$_POST["questgreen"][$CountRecord]);
		$questamber = str_replace('%','',$_POST["questamber"][$CountRecord]); 
	    
		$insertQry="update  tbl_survey_benchmark set green=:green,amber=:amber where loreal_type='Hair' and survey_id=".$s;
		$prepinsertQry=$DBCONN->prepare($insertQry);
		$updateRes=$prepinsertQry->execute(array(":green"=>$questgreen,":amber"=>$questamber));
	    $s++;
    }
	header("Location:admin_features.php");
    exit;
}
include("includes/header.php");
?>
<body>
		<div>
			<div style="margin-left:auto;margin-right:auto;">
				 
				<div class="content">
					<div class="list_content">
						<h1 style="font-size:25px;padding-top:15px;padding-bottom:15px;margin:0px;">SURVEY BENCHMARKS</h1>
						<form name="survey_form" id="survey_form" method="post" enctype="multipart/form-data">
						<input type="hidden" name="surveyquest" id="surveyquest" value="surveyquest">
						<input type="hidden" name="hdn_page" id="hdn_page" value="<?php echo $App_id;?>">
						<table cellspacing="6" cellpadding="0" border="0" width="80%">
						    <tr>
								<td>
									 
								</td>
								<td>
									<table cellspacing="0" cellpadding="0" width="50%" border="0">
										<tbody><tr>
											<td>
												<b>&nbsp;&nbsp;Green&nbsp;</b>

											</td>
											<td align="left">
												<b>&nbsp;&nbsp;&nbsp;&nbsp;Orange</b>
											</td>
											
										</tr>
									</tbody></table>
								</td>
							</tr>
                            <?php
							$getqry="select * from  tbl_survey_benchmark where loreal_type='Hair'";
							$prepget_getqry=$DBCONN->prepare($getqry);
							$prepget_getqry->execute();
							$getResCnt		=	$prepget_getqry->rowCount();
							$extra_loop		=	12 -$getResCnt;
							
							
							$getResRow=$prepget_getqry->fetchAll();
							$i=1;
							foreach($getResRow as $getRes){
								$survey_id  =  stripslashes($getRes["survey_id"]);
								$survey_question = stripslashes($getRes["quest_shortname"]);
								if($i=="4" || $i=="5" ||$i=="8" || $i=="10" || $i=="12"  ){
								  $survey_green=stripslashes($getRes["green"])."%";
								  $survey_amber=stripslashes($getRes["amber"])."%";
							    } else {
							    	$survey_green=stripslashes($getRes["green"]);
								    $survey_amber=stripslashes($getRes["amber"]);
							    }
							?>
							<tr>
								<td style="width:350px;">
									<?php echo $survey_question; ?><font color="red"></font>
								</td>
								<td>
									<table cellspacing="0" cellpadding="0" width="50%" border="0">
										<tbody>
											<tr>
												<td align="left">
													<input type="text" name="questgreen[]" id="quesgreen_<?php echo  $i; ?>" class=" green_<?php echo $i; ?>"   style="width:63%;height: 20px;text-align: center;"   value="<?php echo $survey_green;?>"> 
												</td>
												<td align="left">
													<input type="text" name="questamber[]" id="questamber_<?php echo $i;?>" class="amber_<?php echo $i; ?>" value="<?php echo $survey_amber;?>" style="width:63%;height: 20px;text-align: center;">
												</td>
												
											</tr>
									    </tbody>
									</table>
								</td>
							</tr>
							<?php 
							  $i++;
						    } 
							if($extra_loop){
							for ($x = 1; $x <=$extra_loop; $x++) {
							?>
							<tr>
								<td style="width:350px;">
									<?php echo $survey_question; ?><font color="red"></font>
								</td>
								<td>
									<table cellspacing="0" cellpadding="0" width="50%" border="0">
										<tbody>
											<tr>
												<td align="left">
													<input type="text" name="questgreen[]" id="quesgreen_<?php echo  $x; ?>" class="green_<?php echo $x; ?>"   style="width:63%;height: 20px;text-align: center;"   value="<?php echo $ques1green;?>"> 
												</td>
												<td align="left">
													<input type="text" name="questamber[]" id="questamber_<?php echo $x;?>" class="amber_<?php echo $x; ?>" value="<?php echo $ques1amber;?>" style="width:63%;height: 20px;text-align: center;">
												</td>
												
											</tr>
									    </tbody>
									</table>
								</td>
							</tr>
							<?php 
						        }
						    } 
							?>
							
							<tr>
							    <td>
									<div class="form_actions" style="text-align:left;">
									<input type="button" value="Back to Admin" class="add_btn" onclick="document.location='admin_features.php'">
									
								</td>
								<td><div class="form_actions" style="text-align:center;">
										<input type="button" value="Save" class="add_btn" id="survey_app">
									</div></td>
							
							</tr>
						</table>
					</form>
				</div>
			</div>
		</div>
	</div>
<?php
include("includes/footer.php");
?>
<script>

$(document).on('change', '*[class*="green_"], *[class*="amber_"]', function(){

	   var onChangeEle = $(this).attr('class');
	   var splitval = onChangeEle.split('_');
	   var changeEle = splitval[1];
       var greenEleclass="green_"+changeEle;
       var amberEleclass="amber_"+changeEle;


       if(greenEleclass=="green_1" || amberEleclass=="amber_1" || greenEleclass=="green_2" || amberEleclass=="amber_2" || greenEleclass=="green_3" || amberEleclass=="amber_3" || greenEleclass=="green_6" || amberEleclass=="amber_6" || greenEleclass=="green_7" || amberEleclass=="amber_7" ||  greenEleclass=="green_9" || amberEleclass=="amber_9" ||  greenEleclass=="green_11" || amberEleclass=="amber_11"){
       		var greenEleval = $(".green_"+changeEle).val();
	   		var amberEleval = $(".amber_"+changeEle).val();

	   		if (greenEleval > 5.0) {
            alert('Please enter green box equal to 5.0 or less than value');
		    }
		    else if (greenEleval < amberEleval || greenEleval == amberEleval){
	            alert('Should be greater than green box value');
		    }
			else if( amberEleval < 0.1){
	            alert('The box value enter minimum 0.1 or greater value');
		    } else if(greenEleval < 0.1){
	           alert('The box value enter minimum 0.1 or greater value');
		    } else {
				if(greenEleclass=="green_1" || amberEleclass=="amber_1"){
				   $("#quesgreen_1").val(parseFloat(greenEleval).toFixed(1));
	               $("#questamber_1").val(parseFloat(amberEleval).toFixed(1));
		    	}
		    	if(greenEleclass=="green_2" || amberEleclass=="amber_2"){
				   $("#quesgreen_2").val(parseFloat(greenEleval).toFixed(1));
	               $("#questamber_2").val(parseFloat(amberEleval).toFixed(1));
		    	}
		    	if(greenEleclass=="green_3" || amberEleclass=="amber_3"){
				   $("#quesgreen_3").val(parseFloat(greenEleval).toFixed(1));
	               $("#questamber_3").val(parseFloat(amberEleval).toFixed(1));
		    	}
		    	if(greenEleclass=="green_6" || amberEleclass=="amber_6"){
				   $("#quesgreen_6").val(parseFloat(greenEleval).toFixed(1));
	               $("#questamber_6").val(parseFloat(amberEleval).toFixed(1));
		    	}
		    	if(greenEleclass=="green_7" || amberEleclass=="amber_7"){
				   $("#quesgreen_7").val(parseFloat(greenEleval).toFixed(1));
	               $("#questamber_7").val(parseFloat(amberEleval).toFixed(1));
		    	}
		    	if(greenEleclass=="green_9" || amberEleclass=="amber_9"){
				   $("#quesgreen_9").val(parseFloat(greenEleval).toFixed(1));
	               $("#questamber_9").val(parseFloat(amberEleval).toFixed(1));
		    	}
		    	if(greenEleclass=="green_11" || amberEleclass=="amber_11"){
				   $("#quesgreen_11").val(parseFloat(greenEleval).toFixed(1));
	               $("#questamber_11").val(parseFloat(amberEleval).toFixed(1));
		    	}
		    	//$("#survey_form").submit();
		    }
	    } 
       

       //percentage
       if(greenEleclass=="green_4" || amberEleclass=="amber_4" || greenEleclass=="green_5" || amberEleclass=="amber_5" || greenEleclass=="green_8" || amberEleclass=="amber_8" || greenEleclass=="green_10" || amberEleclass=="amber_10" || greenEleclass=="green_12" || amberEleclass=="amber_12") {
			   var greenPerEleval = $(".green_"+changeEle).val();
			   var amberPerEleval = $(".amber_"+changeEle).val();
			   var greensplitEleval = greenPerEleval.split('%');
			   var greenElevalue=greensplitEleval[0];
			   var ambersplitEleval = amberPerEleval.split('%');
			   var amberElevalue=ambersplitEleval[0];
	           if(greenElevalue > 100){
	           alert('Please enter green box equal to 100 percentage or less than value');
			    }
			     else if (greenElevalue < amberElevalue || greenElevalue == amberElevalue){
		            alert('Should be greater than green box percentage value');
			    }
				else if(amberElevalue < 1){
		            alert('The box value enter minimum 1 percentage or greater value');
			    } else if(greenElevalue < 1){
		           alert('The box value enter minimum 1 percentage or greater value');
			    } else {
		            if(greenEleclass=="green_4" || amberEleclass=="amber_4"){
					   $("#quesgreen_4").val(Math.round(greenElevalue)+"%");
		               $("#questamber_4").val(Math.round(amberElevalue)+"%");
			    	}
			    	if(greenEleclass=="green_5" || amberEleclass=="amber_5"){
					   $("#quesgreen_5").val(Math.round(greenElevalue)+"%");
		               $("#questamber_5").val(Math.round(amberElevalue)+"%");
			    	}
			    	if(greenEleclass=="green_8" || amberEleclass=="amber_8"){
					   $("#quesgreen_8").val(Math.round(greenElevalue)+"%");
		               $("#questamber_8").val(Math.round(amberElevalue)+"%");
			    	}
			    	if(greenEleclass=="green_10" || amberEleclass=="amber_10"){
					   $("#quesgreen_10").val(Math.round(greenElevalue)+"%");
		               $("#questamber_10").val(Math.round(amberElevalue)+"%");
			    	}
			    	if(greenEleclass=="green_12" || amberEleclass=="amber_12"){
					   $("#quesgreen_12").val(Math.round(greenElevalue)+"%");
		               $("#questamber_12").val(Math.round(amberElevalue)+"%");
			    	}
			    	//$("#survey_form").submit();
		    	}
	    }
});
$(document).on("click",'#survey_app', function(){
	 $("#survey_form").submit();
});
</script>
