<?php
include("../includes/configure.php");
include("includes/cmm_functions.php");
include("../includes/session_check.php");

$search_type	=	$_REQUEST['srch_type'];
$return_string	=	"";
$set_search_array = array(
	"business_name"=>"Business Name",
	"first_name"=> "Full Name",
	"promo_code"=>"Promo Code",
	"business_type"=>"Business Type",
	"a_id"=>"Affiliate",
	"l_id"=>"Licensee",
	"country"=>"Country",
	"callstatus"=>"Call Status",
	"created_date"=>"Create Date"
);

if($search_type != "business_name" && $search_type != "first_name" && $search_type != "promo_code"){
	if($search_type != "a_id" && $search_type!="l_id" && $search_type != "created_date" && $search_type != "country"){
		$table_name	=	"tbl_prospecting_list";
	}elseif($search_type == "a_id"){
		$table_name	=	"tbl_affiliate";
		$search_type=	"full_name";
	}elseif($search_type == "l_id"){
		$table_name	=	"tbl_licensee";
		$search_type=	"business_name";
	}elseif($search_type == "country"){
		$table_name	=	"tbl_country";
		$search_type=	"country";
	}

	$geQry_db="SELECT DISTINCT ".$search_type." from ".$table_name." WHERE ".$search_type." != '' and ".$search_type." != '0'";

	$prepgetuserqry_db=$DBCONN->prepare($geQry_db);
	$prepgetuserqry_db->execute();
	$getRowCnt	=	$prepgetuserqry_db->rowCount();
	if($getRowCnt>0){
		$fetch_data	=	$prepgetuserqry_db->fetchAll(PDO::FETCH_ASSOC);
		$return_string.='<select name="search_field" id="search_field" style="width:95%;margin-left: 15px;">';
		$return_string.='<option value="" selected>Select</option>';
		foreach($fetch_data as $single_data){
			$return_string.='<option value="'.$single_data[$search_type].'">'.$single_data[$search_type].'</option>';
		}
		$return_string.='</select>';
		echo $return_string;
		exit;
	}else{
		echo "failure";
		exit;
	}
}else{
		$return_string.='<input type="text" name="search_field" id="search_field" class="inp_feild" value="" style="width:100%;" autocomplete="off">';
		echo $return_string;
		exit;
		
}
?>