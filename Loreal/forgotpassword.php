<?php
include("../../includes/configure.php");
$ErrMsg="";

if(isset($_POST['email'])){
	$Email=trim($_POST['email']);
	$getQry="select * from tbl_users where email=:email";
	$prepgetQry=$DBCONN->prepare($getQry);
	$prepgetQry->execute(array(":email"=>$Email));
	$count=$prepgetQry->rowCount();
	if ($count>0){
		$row=$prepgetQry->fetch();
		$username=stripslashes($row["username"]);
		$name=explode(" ",$username);
        $first_name=$name[0];
		$password=stripslashes($row["password"]);
		$loginpath="http://www.myappybusiness.com/admin/loreal/login.php";
		$LogoUrl= 'http://www.myappybusiness.com/images/email-logo.png';
		$selLang=$_POST["hnd_selectlang"];
        if($selLang=="English"){
            include("lang/eng_lang.php");
			$getLangMailContent  = funLangMailContent($selLang,$first_name,$username,$password,$loginpath,$LogoUrl); 
        } 
        if($selLang=="SEK"){
	        include("lang/swd_lang.php");
			$getLangMailContent  = funLangMailContent($selLang,$first_name,$username,$password,$loginpath,$LogoUrl);
        }
		$FromAddress    = $getLangMailContent["ForgotMail"]["fromaddress"];
	    $EmailSubject   = $getLangMailContent["ForgotMail"]["emailsubject"];
	    $EmailMessage   = $getLangMailContent["ForgotMail"]["message"];
		sendEmail($FromAddress,$Email,$EmailSubject,$EmailMessage);
		header('location:login.php?msg=1');
		exit;
		
	}
	else{
		$ErrMsg ="Invalid email";
	}
}
?>

<?php
	include_once("includes/lorealheader.php");
    $setlanguage=$_SESSION["language"];

?>
<style>
.main_header {
   margin-bottom: 20px;
}
.session_name, .logout{
	display:none;
}
</style>
<div class="container logincontainer">
	<div class="container login" style="min-height: ">
        <div class="col-md-12 col-sm-12 col-xs-12 col-lg-12 emotion"> 
            <img src="images/emotion_logo.png">
        </div>
		<div class="col-md-12 col-sm-12 col-xs-12 col-lg-12"> 
		   <div class="col-md-6 col-sm-8 col-xs-12 col-lg-4 loginbox">
			    <form name="frm_forgetlogin" id="frm_forgetlogin" method="post" action="">
			    <input type="hidden" id="hnd_selectlang" name="hnd_selectlang" value="<?php echo $setlanguage?>">
			    	<h1><?php  echo $getLangContent["Forgot"]["forgothead"];?></h1>
			    	<?php
						if($ErrMsg!=""){
					?>
							<div class="alert alert-danger">
							 <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
						     <?php echo $getLangContent["Forgot"]["invalidemail"]  ?>
						    </div>
					<?php		
						}
					?>
			    	<div class="col-md-12 col-sm-12 col-xs-12 col-lg-12">
			    		<input type="text" placeholder="<?php  echo $getLangContent["Forgot"]["emailplaceholder"];?>" name="email" required="" id="username" value="">
			    	</div>
			    	
			    	<div class="col-md-12 col-sm-12 col-xs-12 col-lg-12 forgot_submit">
			    	    <div class="col-md-6 col-sm-6 col-xs-5 col-lg-6 forgot1">
			    	   	   <a href="login.php" class="forgot"><i><?php  echo $getLangContent["Forgot"]["linkbacklogin"];?></i></a> 
			    	   </div>
			    	    <div class="col-md-6 col-sm-6 col-xs-7 col-lg-6 btnlog">
					   		<input type="submit" value="<?php  echo $getLangContent["Forgot"]["btnforgot"];?>" class="btnlogin btn btn-default">
					   </div>
					</div> 
			    	
					<div class="empty"></div>
			    </form> 
		   </div>
		</div>
    </div>
</div>
<div style="min-height:94px;">
</div>
<?php
    include_once("includes/lorealfooter.php");
?>
<script type="text/javascript" src="js/jquery.validate.js"></script>
<!-- <script type="text/javascript" src="js/custom.js"></script> -->
<script>
$(document).ready(function(){
	var emailvalid="<?php echo $getLangContent['Forgot']['emailvalid']; ?>";
	 var validemailaddress="<?php echo $getLangContent['Forgot']['validemailaddress']; ?>";
	$("#frm_forgetlogin").validate({
	    rules: {
			email: {
				required: true,
				email:true
			},
			
		},
		messages: {
			email: {
				required: emailvalid,
				email:validemailaddress

			},
			
		},
	});
});
</script>
<?php

function sendEmail($FromAddress, $ToAddress, $EmailSubject, $EmailMessage)
{
	
	$EmailHeaders   = array
    (
        'MIME-Version: 1.0',
        'Content-Type: text/html; charset="UTF-8";',
        'Content-Transfer-Encoding: 7bit',
        'Date: ' . date('r', $_SERVER['REQUEST_TIME']),
        'Message-ID: <' . $_SERVER['REQUEST_TIME'] . md5($_SERVER['REQUEST_TIME']) . '@' . $_SERVER['SERVER_NAME'] . '>',
		$FromAddress,
        'Reply-To: ' . $FromAddress,
        'Return-Path: ' . $FromAddress,
        'X-Mailer: PHP v' . phpversion(),
        'X-Originating-IP: ' . $_SERVER['SERVER_ADDR'],
    );
    mail($ToAddress, '=?UTF-8?B?' . base64_encode($EmailSubject) . '?=', $EmailMessage, implode("\n", $EmailHeaders));

}

?>

