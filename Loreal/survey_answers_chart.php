<?php
include_once("../../includes/configure.php");
include_once ("includes/loreal_sessioncheck.php");

$chartFlag = false;
$surveyQstn = $demo_id = $user_id = "";
if (isset($_GET['qstn'])) {

	$surveyQstn = trim($_GET['qstn']);
	$demo_id = trim($_SESSION['demo_id']);
	$user_id = $_SESSION['user_id'];

    $next = $surveyQstn + 1;
    $back = $surveyQstn - 1;

    $qstnQry = $DBCONN->prepare("SELECT * FROM tbl_survey_benchmark WHERE loreal_type=:lorealtype and survey_id=:survey_id");
    $qstnQryArr = array(":survey_id" => $surveyQstn,":lorealtype"=>"Hair");
    $qstnQry->execute($qstnQryArr);
    $cntQstn = $qstnQry->rowCount();

    if ($cntQstn > 0) {
        $fetchQstns = $qstnQry->fetch(PDO::FETCH_ASSOC);
        $question = $fetchQstns["question"];
        $green = $fetchQstns["green"];
        $amber = $fetchQstns["amber"];
        $questionType = $fetchQstns["question_type"];
    }

    if ($questionType == "scale") {
        $chartFlag = true;
        $chartStyle="visibility: visible;";
    } else {
        $chartFlag = false;
    }

    $start_date = $_SESSION["start_date"][$demo_id];
    $end_date = $_SESSION["end_date"][$demo_id];

    $betweenQry = "";
    if (!empty($start_date) && !empty($end_date)) {
        // $betweenQry = " AND (timestamp BETWEEN '$start_date' AND '$end_date')";
        $betweenQry = "AND DATE_FORMAT(timestamp, '%Y-%m-%d') >= '$start_date' AND  DATE_FORMAT(timestamp, '%Y-%m-%d') <= '$end_date'";
    } elseif (!empty($start_date) && empty($end_date)) {
        // $betweenQry = " AND timestamp >= '$start_date'";
        $betweenQry = "AND DATE_FORMAT(timestamp, '%Y-%m-%d') >= '$start_date'";
    } elseif (empty($start_date) && !empty($end_date)) {
        // $betweenQry = " AND timestamp <= '$end_date'";
        $betweenQry = "AND DATE_FORMAT(timestamp, '%Y-%m-%d') <= '$end_date'";
    } else {
        $betweenQry = "";
    }
    // echo "SELECT * FROM tbl_temp_survey_answers WHERE user_id='$user_id' AND demo_id='$demo_id' $betweenQry ORDER BY timestamp ASC";
	// $selectAns = $DBCONN->prepare("SELECT * FROM tbl_temp_survey_answers WHERE user_id=:user_id $betweenQry ORDER BY timestamp ASC");
	// $selectAnsArr = array(":user_id" => $user_id);
    $selectAns = $DBCONN->prepare("SELECT * FROM tbl_temp_survey_answers WHERE user_id=:user_id AND demo_id=:demo_id $betweenQry ORDER BY timestamp ASC");
    $selectAnsArr = array(":user_id" => $user_id, ":demo_id" => $demo_id);
	$selectAns->execute($selectAnsArr);
	$cntRes = $selectAns->rowCount();

	if ($cntRes > 0) {
		$fetchSurveyAns = $selectAns->fetchAll(PDO::FETCH_ASSOC);       

		$AnsArr = "";
        $YearMonthAvg = "";

        $len = 0;
		foreach ($fetchSurveyAns as $surveyAns) {

            $timestamp = "";
            if (!empty($surveyAns['timestamp'])) {
                $dbtimeStamp = str_replace('/', '-', $surveyAns['timestamp']);
                $dateTime = explode(" ", $dbtimeStamp);            
                $date = explode("-", $dateTime[0]);
                $dateFormat = $date[2]."-".$date[1]."-".$date[0];
                // $timestamp = $dateFormat." ".$dateTime[1];
                $timestamp = date("Y-m-d H:i:s", strtotime($dateFormat." ".$dateTime[1]));
            }
            // $Year = (int) date("Y", strtotime($surveyAns['timestamp']));
            $monthNum = date("Y-m", strtotime($timestamp));
            // $Year = date("Y");

            $notRelevant = strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', $surveyAns["qstn".$surveyQstn."_ans"]));
            if ($chartFlag == true) {
                if ($surveyQstn == 6 ) {
                   
                    if ($notRelevant != 'notrelevant') {
                        $explodeAns = explode(" ", $surveyAns["qstn".$surveyQstn."_ans"]);
                        $AnsArr[$monthNum][] += (int) $explodeAns[0];
                    }
                    
                } else {
                    
                    // $AnsArr[$monthNum][] += (int) $surveyAns["qstn".$surveyQstn."_ans"];
                    $AnsArr[$monthNum][] += (int) $surveyAns["qstn".$surveyQstn."_ans"];

                }

                $j = 1;
                foreach ($AnsArr as $AnsMonth => $AnsMonthqs) {
                    $YearMonthAvg[$j] = round(array_sum($AnsMonthqs) / count($AnsMonthqs), 1);
                    $j++;
                }
            } else {

                $AnsArr[$monthNum][] = $surveyAns["qstn".$surveyQstn."_ans"];
                $YearMonthAvgNew[$monthNum] = array_count_values($AnsArr[$monthNum]);
                $j = 1;
                foreach ($YearMonthAvgNew as $AnsMonth => $AnsMonthqs) {

                    $YearMonthAvgBar[$j]['Yes'] = round($AnsMonthqs['Yes'] / array_sum($AnsMonthqs), 2);
                    $YearMonthAvgBar[$j]['No'] = round($AnsMonthqs['No'] / array_sum($AnsMonthqs), 2);
                    $YearMonthAvgBar[$j]['Maybe'] = round($AnsMonthqs['Maybe'] / array_sum($AnsMonthqs), 2);
                    $barChartData['Yes'][$j] = ($YearMonthAvgBar[$j]['Yes']) * 100;
                    $barChartData['No'][$j] = ($YearMonthAvgBar[$j]['No']) * 100;
                    $barChartData['Maybe'][$j] = ($YearMonthAvgBar[$j]['Maybe']) * 100;

                    $j++;
                }
            }

           $len++;     
		}

	}

    // echo $currentYear = date("Y");
    if ($chartFlag == true) { 
        $yaxisValues = json_encode(array_values($YearMonthAvg));
        $xaxisValues = json_encode(array_keys($YearMonthAvg));
    } else {
        

        // $xaxisValueYes = json_encode(array_values($barChartData['Yes']));
        // $xaxisValueNo = json_encode(array_values($barChartData['No']));
        // $xaxisValueMaybe = json_encode(array_values($barChartData['Maybe']));
        // $yaxisValues = json_encode(array_keys($barChartData['Yes']));


        $yaxisValueYes = json_encode(array_values($barChartData['Yes']));
        $yaxisValueNo = json_encode(array_values($barChartData['No']));
        $yaxisValueMaybe = json_encode(array_values($barChartData['Maybe']));
        $xaxisValuesYes = json_encode(array_keys($barChartData['Yes']));

    }

}
include_once("includes/lorealheader.php");
$sellangquestion="quest".$surveyQstn;
$getquestion=$getLangContent["Surveychart"][$sellangquestion];
?>
<div class="container">
    <div class="col-md-12 col-sm-12 col-xs-12 remove-padding-right-left">
        <a href="loreal_masterlist.php?demo_id=<?php echo $demo_id; ?>" ><input type="button" id="backtolist" class="btn chart_btn commonbtn" value="<?php  echo $getLangContent["Surveychart"]["backtolist"];?>"></a>
        <a href="survey_question_dashboard.php?demo_id=<?php echo $demo_id; ?>" ><input type="button" id="backtodashboard" class="btn chart_btn commonbtn" value="<?php  echo $getLangContent["Surveychart"]["backtodashboard"];?>"></a>
    </div>
    <div class="col-md-12 col-sm-12 col-xs-12 questions remove-padding-right-left">
        <div class="col-md-10 col-sm-10 col-xs-10 ">
           <!--  <p class="chart_title_content"><?php echo $question; ?></p> --> 
           <p class="chart_title_content"><?php echo $getquestion; ?></p>
           
        </div>
        <div class="col-md-2 col-sm-2 col-xs-2">
            <p class="chart_title_content_res"><?php  echo $getLangContent["Surveychart"]["responsecount"];?> <?php echo $cntRes; ?></p>
        </div>
    </div>
    <div class="col-md-12 col-sm-12 col-xs-12 remove-padding-right-left">
        <div id="chart" style="min-width: 400px; height: 350px; margin: 0 auto"></div>
        <?php //if ($chartFlag == true) { ?>
        
        <div class="col-md-12 col-sm-12 col-xs-12" style="<?php echo $chartStyle=!empty($chartStyle)?$chartStyle:"visibility: hidden;";?>">
           <p><spam class="greenbox"></spam> <spam class="goodtxt"> = <?php  echo $getLangContent["Surveychart"]["goodbenchmark"];?></spam> </p>
           <p><span class="orangebox"></span> <spam class="goodtxt"> = <?php  echo $getLangContent["Surveychart"]["satisfactorybenchmark"];?></spam></p>
        </div>
        <?php //} ?>
    </div>
    <div class="col-md-12 col-sm-12 col-xs-12 remove-padding-right-left" style="padding-bottom: 10px;">
        <div class="col-md-6 col-sm-6 col-xs-6 remove-padding-right-left back-section">
            <!-- <a href="survey_question_dashboard.php?demo_id=<?php echo $_SESSION["demo_id"]; ?>"><input type="button" name="back" class="btn chart_btn commonbtn" value="Back"></a> -->
            <?php if ($surveyQstn != 1) {?>
            <a href="survey_answers_chart.php?qstn=<?php echo $back; ?>" question-no="<?php echo $surveyQstn; ?>" id="backbtn"><input type="button" name="back" class="btn chart_btn commonbtn" value="<?php  echo $getLangContent["Surveychart"]["previousquestion"];?>"></a>
            <?php } ?>
        </div>
        <div class="col-md-6 col-sm-6 col-xs-6 text-right remove-padding-right-left next-section">
            <?php if ($surveyQstn != 12) {?>
            <a href="survey_answers_chart.php?qstn=<?php echo $next; ?>" question-no="<?php echo $surveyQstn; ?>" id="nextbtn"><input type="button" name="next" class="btn chart_btn commonbtn" value="<?php  echo $getLangContent["Surveychart"]["nextquestion"];?>"></a> 
            <?php } ?>
        </div>
    </div>
</div>

<div id="footer">
    <?php include("includes/lorealfooter.php"); ?> 
</div>
<?php
if ($questionType == "scale") {
?> 
    <script type="text/javascript">
        $(function() {
            var green = <?php echo $green; ?>;
            var amber = <?php echo $amber; ?>;
            $('#chart').highcharts({

                chart: {
                    zoomType: 'xy',
                    height: 350,
                    plotBorderColor: '#000',
                    plotBorderWidth: 2,
                },
                title: {
                    text: '',
                    x: -20 //center
                },

                credits: {
                    enabled: false,
                },
                colors: ['#000000'],
                xAxis: [{
                    categories: <?php echo $xaxisValues; ?>,
                    crosshair: true,
                }],
                
                yAxis: [{ // Primary yAxis
                    min: 0,  
                    max: 5,          
                    tickInterval: 1,
                    labels: {
                        format:'{value}',
                    },
                    plotLines: [{               
                        value: green,                
                        color: '#6dcf95',                
                        width: 2,                
                        label: {                    
                            text: ''                
                        }            
                    }, {                
                        value: amber,                
                        color: '#ffcf44',               
                         width: 2,                
                         label: {                    
                            text: ''                
                        }           
                     }],
                    title: {
                        text: '',
                        style: {
                            color: Highcharts.getOptions().colors[1],
                        }
                    },
                },{ // Secondary yAxis
                    title: {
                        text: '',
                        style: {
                            color: Highcharts.getOptions().colors[1],
                        }
                    },
                }],
                plotOptions: {
                    line: {
                        // marker: {
                        //     enabled: true
                        // }
                        marker: {
                            enabled: true,
                            symbol: 'circle',
                            radius: 2,
                            states: {
                                hover: {
                                    enabled: true
                                }
                            }
                        }
                    }
                },
                series: [{
                    name: 'Avg',
                    type: '',
                    data: <?php echo $yaxisValues; ?>,

                },],

            });
        });
    </script>
<?php } else { ?>
    <script type="text/javascript">

        $(function() {
            Highcharts.chart('chart', {
                chart: {
                    type: 'column',
                    plotBorderColor: '#000',
                    plotBorderWidth: 2,
                },
                title: {
                    text: '',
                    x: -20 //center
                },
                credits: {
                    enabled: false
                },
                xAxis: {
                    // categories: ['1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20','21','22','23','24','25','26','27','28','29','30']
                    categories: <?php echo $xaxisValuesYes; ?>
                },
                yAxis: {
                    min: 0,
                    max: 110,
                    labels:{enabled: false},
                    tickInterval: 1,
                    title: {
                        text: ''
                    },
                    stackLabels: {
                        enabled: true,
                        style: {
                            fontWeight: 'bold',
                            color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                        },
                        formatter: function () {
                            return (this.axis.series[0].yData[this.x]) + '%';
                        }
                    }
                },
                tooltip: {
                    pointFormat: '{series.name}: {point.y}<br/>Total: {point.stackTotal}'
                },
                plotOptions: {
                    column: {
                        stacking: 'normal',
                    }
                },
                colors: [
                    '#00b050',
                    '#ffc000',
                    '#ff0000'
                ],
                series: [{
                    name: '<?php echo $getLangContent["Surveychart"]["yes"] ?>',
                    data: <?php echo $yaxisValueYes; ?>
                    // data: [2, 2]
                }, {
                    name: '<?php echo $getLangContent["Surveychart"]["no"] ?>',
                    data: <?php echo $yaxisValueNo; ?>
                    // data: [2, 2]
                }, {
                    name: '<?php echo $getLangContent["Surveychart"]["maybe"] ?>',
                    data: <?php echo $yaxisValueMaybe; ?>
                    // data: [2, 2]
                }],
             
            });
            
        });
    </script>
<?php } ?>
<script type="text/javascript">

    $(document).on("click","#backbtn, #nextbtn",function() {

        question = $(this).attr("question-no");
        if (question >= 12) {
            $("#nextbtn").removeAttr("href");
            // $(".next-section").css("opacity", '0.5');
        } 
        if (question <= 1) {
            $("#backbtn").removeAttr("href");
            // $(".back-section").css("opacity", '0.5');
        }
    });
</script>

