<?php
//============================================================+

// Include the main TCPDF library (search for installation path).
require_once('tcpdf/tcpdf.php');
require_once('tcpdf/fpdi.php');

// create new PDF document
//$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);


class MYPDF extends TCPDF {

    //Page header
    public function Header() {

        // Set font
        $this->SetFont('helvetica', 'B', 20);
        $this->Rect(0, 0, 1000, 15,'F',array(255,255,255),array(0,0,0));
        $this->SetTextColor(255, 255, 255);
        $this->setCellMargins(0, 2, 0, 0);
        // Title
        $this->Cell(0, 15, "L'ORÉAL", 0, false, 'L', 0, '', 0, false, 'M', 'M');

    }

    // Page footer
    public function Footer() {
        // Position at 15 mm from bottom
        // $this->SetY(-15);
        // Set font
        //$this->SetFont('helvetica', 'I', 8);
        $this->Rect(0, 193, 1000, 10,'F',array(),array(0, 0, 0));
    }

}
// create new PDF document
// $pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// $pdf = new MYPDF("L", "mm", PDF_PAGE_FORMAT, true, 'UTF-8', false);
// $pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
$pdf = new MYPDF("L", "mm", array('340','203'), true, 'UTF-8', false);

// set document information
//$pdf->SetCreator(PDF_CREATOR);
//$pdf->SetAuthor('Gnanavel');
// $pdf->SetTitle('Chart');
//$pdf->SetSubject('TCPDF Tutorial');

$PDF_HEADER_TEXT  = '';
// $Test  = 'L&#39;Oréal';

$pdf->SetHeaderData('','' , PDF_HEADER_TITLE.' 001', PDF_HEADER_STRING, array(0,64,255), array(0,64,128));

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
// if (@file_exists($lorealPath.'/tcpdf/lang/eng.php')) {
// 	require_once($lorealPath.'/tcpdf/lang/eng.php');
// 	$pdf->setLanguageArray($l);
// } 
if (@file_exists('/tcpdf/lang/eng.php')) {
    require_once('/tcpdf/lang/eng.php');
    $pdf->setLanguageArray($l);
}   

//echo $sessionlang;exit;


require_once('tcpdf/tcpdf_import.php'); 
$dateOfReport = date("dmy");
$chartFlag = false;

$pdf->AddPage();
if($sessionlang=="English"){
   include("lang/eng_lang.php");
        $getLangContent  = funLangContent($sessionlang); 
        $analysis="ANALYSIS REPORT";
}
if($sessionlang=="SEK"){
        include("lang/swd_lang.php");
        $getLangContent  = funLangContent($sessionlang); 
        $analysis="ANALYS RAPPORT";
} 


$emotionImage = '<br><br><br><br><table style="width:100%;"><tr><td></td></tr><tr><td style="text-align:center;"><img src="images/emotion_logo.png"  width="200" ></td></tr><tr><td style="font-size:18px;text-align:center;text-transform: uppercase;">'.$analysis.'</td></tr></table>';
$pdf->writeHTML($emotionImage, true, 0, true, 'C');
$pdf->setCellMargins(14, 0, 0, 0);

$demoQry = $DBCONN->prepare("SELECT * FROM tbl_demo WHERE demo_id=:demo_id");
$demoQryArr = array(":demo_id" =>$demo_id );
$demoQry->execute($demoQryArr);
$getRowCnt = $demoQry->rowCount();
if ($getRowCnt > 0) {
    $fetchMasterinfo = $demoQry->fetch(PDO::FETCH_ASSOC);
    $businessName = $fetchMasterinfo["business_name"];
}

$salonname = '<br><br><table style="width:89%;"><tr><td></td></tr><tr><td style="font-size:18px;text-align:center;">'.strtoupper($businessName).'</td></tr><tr><td style="font-size:18px;text-align:center;text-transform: uppercase;">'.date("d/m/Y").'</td></tr></table>';
$pdf->writeHTML($salonname, true, 0, true, 'C');
$pdf->setCellMargins(10, 0, 0, 0);

$salonname ='<br><br><br><br><br><br><br><br><br><br><br><br>';
$salonname .= '<table style="width:100%;"><tr><td></td></tr><tr><td style="font-size:18px;text-align:right;">'.ucfirst($_SESSION['user_name']).'</td></tr></table>';
$pdf->writeHTML($salonname, true, 0, true, 'C');
$pdf->setCellMargins(10, 0, 0, 0);

$summaryDasboard = '<img src="salon_eMotion_report/summary_dashboard.png"  style="margin: auto;text-align:center;" width="1000" height="500">';
$pdf->writeHTML($summaryDasboard, true, false, false, false, 'C');

for ($Inc=1;$Inc<=12;$Inc++) {

	$qstnQry = $DBCONN->prepare("SELECT * FROM tbl_survey_benchmark WHERE loreal_type=:lorealtype and survey_id=:survey_id");
    $qstnQryArr = array(":survey_id" => $Inc,":lorealtype"=>"Hair");
    $qstnQry->execute($qstnQryArr);
    $cntQstn = $qstnQry->rowCount();

    if ($cntQstn > 0) {
        $fetchQstns = $qstnQry->fetch(PDO::FETCH_ASSOC);
        //$question = $fetchQstns["question"];
        $sellangquestion="quest".$Inc;
        $question=$getLangContent["Surveychart"][$sellangquestion];
        $green = $fetchQstns["green"];
        $amber = $fetchQstns["amber"];
        $questionType = $fetchQstns["question_type"];
    }

    if ($questionType == "scale") {
        $chartFlag = true;
    } else {
        $chartFlag = false;
    }

    $start_date = $_SESSION["start_date"][$demo_id];
    $end_date = $_SESSION["end_date"][$demo_id];

    $betweenQry = "";
    if (!empty($start_date) && !empty($end_date)) {
        // $betweenQry = " AND (timestamp BETWEEN '$start_date' AND '$end_date')";
        $betweenQry = "AND DATE_FORMAT(timestamp, '%Y-%m-%d') >= '$start_date' AND  DATE_FORMAT(timestamp, '%Y-%m-%d') <= '$end_date'";
    } elseif (!empty($start_date) && empty($end_date)) {
        // $betweenQry = " AND timestamp >= '$start_date'";
        $betweenQry = "AND DATE_FORMAT(timestamp, '%Y-%m-%d') >= '$start_date'";
    } elseif (empty($start_date) && !empty($end_date)) {
        // $betweenQry = " AND timestamp <= '$end_date'";
        $betweenQry = "AND DATE_FORMAT(timestamp, '%Y-%m-%d') <= '$end_date'";
    } else {
        $betweenQry = "";
    }

    
    $selectAns = $DBCONN->prepare("SELECT * FROM tbl_temp_survey_answers WHERE user_id=:user_id AND demo_id=:demo_id $betweenQry ORDER BY timestamp ASC");
    $selectAnsArr = array(":user_id" => $user_id, ":demo_id" => $demo_id);
	$selectAns->execute($selectAnsArr);
	$cntRes = $selectAns->rowCount();

	$pdf->AddPage();
	$toolcopy = '<br>';
	if ($chartFlag == true) {
		$toolcopy .= '<table style="width:73.5%;background-color:#000;"><tr style="color:#FFFFFF;font-size:13px;"><td style="width:75%;">'.strtoupper($question).'</td><td style="width:25%;font-size:13px;">'.strtoupper($getLangContent["Rawresponsedata"]["rawcount"]).' '.$cntRes.'</td></tr></table>';
		$pdf->setCellMargins(21.5, 0, 0, 0);
        $pdf->setCellPaddings(2, 2, 0, 2);
	} else {

		$toolcopy .= '<table style="width:79.5%;"><tr style="background-color:#000;color:#FFFFFF;font-size:13px;"><td style="width:75%;">'.strtoupper($question).'</td><td style="width:25%;font-size:13px;">'.strtoupper($getLangContent["Rawresponsedata"]["rawcount"]).' '.$cntRes.'</td></tr></table>';
		$pdf->setCellMargins(15, 0, 0, 0);
        $pdf->setCellPaddings(2, 2, 0, 2);
	}
    $pdf->writeHTML($toolcopy, true, false, false, false, 'L');

    $chartHtml = "";
	$chartHtml .= '<br><br><img src="'."salon_eMotion_report/chartimg_".$chartImgName."_".$Inc.'.png"  width="1000" height="350">';
	$pdf->writeHTML($chartHtml, true, false, false, false, 'C');
    

	// $saloneMotionReport = $lorealPath."/salon_eMotion_report/chartimg_".$chartImgName."_".$Inc.".png";
	// $saloneMotionSummaryReport = $lorealPath."/salon_eMotion_report/summary_dashboard.png";
    $saloneMotionReport = "salon_eMotion_report/chartimg_".$chartImgName."_".$Inc.".png";
    $saloneMotionSummaryReport = "salon_eMotion_report/summary_dashboard.png";
	if (file_exists($saloneMotionReport)) {
		unlink($saloneMotionReport);		
	}
	if (file_exists($saloneMotionSummaryReport)) {
		unlink($saloneMotionSummaryReport);
	}
}

//Close and output PDF document
//LIVE
   $pdf->Output($lorealPath.'/salon_eMotion_report/Salon_eMotions_Report_'.$chartImgName."_".$dateOfReport.'.pdf', 'F', 'F');
//Local
// $pdf->Output(dirname(__FILE__ ). '/salon_eMotion_report/Salon_eMotions_Report_'.$chartImgName."_".$dateOfReport.'.pdf', 'F', 'F');
// $pdf->Output('salon_eMotion_report/Salon_eMotions_Report_'.$chartImgName."_".$dateOfReport.'.pdf', 'F', 'F');


