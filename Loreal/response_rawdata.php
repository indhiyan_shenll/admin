<?php
include_once("../../includes/configure.php");
include_once("includes/loreal_sessioncheck.php");

$id=$_SESSION['user_id'];
$demo_id=$_GET["demo_id"];
// $demo_id="1128";
$sort=$_GET["sort"];
$field=$_GET["field"];

if($sort==""){
	$sort="asc";
}
 if($field==""){
 	$field="timestamp";
 }
if($field=="timestamp"){
	$fieldname="timestamp";
	$order=" order by ".$fieldname." ".$sort;
	if($sort=="asc"){
		$rsort="desc";
		$rpath="images/up.png";
	}
	else{
		$rsort="asc";
		$rpath="images/down.png";
	}
}
if($field=="quest1"){
	$fieldname="qstn1_ans";
	$order=" order by ".$fieldname." ".$sort;
	if($sort=="asc"){
		$q1sort="desc";
		$q1path="images/up.png";
	}
	else{
		$q1sort="asc";
		$q1path="images/down.png";
	}
}

if($field=="quest2"){
	$fieldname="qstn2_ans";
	$order=" order by ".$fieldname." ".$sort;
	if($sort=="asc"){
		$q2sort="desc";
		$q2path="images/up.png";
	}
	else{
		$q2sort="asc";
		$q2path="images/down.png";
	}
}
if($field=="quest3"){
	$fieldname="qstn3_ans";
	$order=" order by ".$fieldname." ".$sort;
	if($sort=="asc"){
		$q3sort="desc";
		$q3path="images/up.png";
	}
	else{
		$q3sort="asc";
		$q3path="images/down.png";
	}
}
if($field=="quest4"){
	$fieldname="qstn4_ans";
	$order=" order by ".$fieldname." ".$sort;
	if($sort=="asc"){
		$q4sort="desc";
		$q4path="images/up.png";
	}
	else{
		$q4sort="asc";
		$q4path="images/down.png";
	}
}
if($field=="quest5"){
	$fieldname="qstn5_ans";
	$order=" order by ".$fieldname." ".$sort;
	if($sort=="asc"){
		$q5sort="desc";
		$q5path="images/up.png";
	}
	else{
		$q5sort="asc";
		$q5path="images/down.png";
	}
}
if($field=="quest6"){
	$fieldname="qstn6_ans";
	$order=" order by ".$fieldname." ".$sort;
	if($sort=="asc"){
		$q6sort="desc";
		$q6path="images/up.png";
	}
	else{
		$q6sort="asc";
		$q6path="images/down.png";
	}
}
if($field=="quest7"){
	$fieldname="qstn7_ans";
	$order=" order by ".$fieldname." ".$sort;
	if($sort=="asc"){
		$q7sort="desc";
		$q7path="images/up.png";
	}
	else{
		$q7sort="asc";
		$q7path="images/down.png";
	}
}
if($field=="quest8"){
	$fieldname="qstn8_ans";
	$order=" order by ".$fieldname." ".$sort;
	if($sort=="asc"){
		$q8sort="desc";
		$q8path="images/up.png";
	}
	else{
		$q8sort="asc";
		$q8path="images/down.png";
	}
}
if($field=="quest9"){
	$fieldname="qstn9_ans";
	$order=" order by ".$fieldname." ".$sort;
	if($sort=="asc"){
		$q9sort="desc";
		$q9path="images/up.png";
	}
	else{
		$q9sort="asc";
		$q9path="images/down.png";
	}
}
if($field=="quest10"){
	$fieldname="qstn10_ans";
	$order=" order by ".$fieldname." ".$sort;
	if($sort=="asc"){
		$q10sort="desc";
		$q10path="images/up.png";
	}
	else{
		$q10sort="asc";
		$q10path="images/down.png";
	}
}
if($field=="quest11"){
	$fieldname="qstn11_ans";
	$order=" order by ".$fieldname." ".$sort;
	if($sort=="asc"){
		$q11sort="desc";
		$q11path="images/up.png";
	}
	else{
		$q11sort="asc";
		$q11path="images/down.png";
	}
}
if($field=="quest12"){
	$fieldname="qstn12_ans";
	$order=" order by ".$fieldname." ".$sort;
	if($sort=="asc"){
		$q12sort="desc";
		$q12path="images/up.png";
	}
	else{
		$q12sort="asc";
		$q12path="images/down.png";
	}
}


if(isset($_POST['HdnPage']) && $_POST['HdnPage']!="" && $_POST['HdnPage']!="0")
	$Page=$_POST['HdnPage'];
else
	$Page=1;

$user_type=$_SESSION['user_type'];
$affiliate_type=$_SESSION['affiliate_type'];

// if (!empty($_SESSION['start_date'])) {
// 	$fromDate = explode("/", $_SESSION['start_date']);
// 	$start_date = $fromDate[2]."-".$fromDate[1]."-".$fromDate[0];
// }
// if (!empty($_SESSION['end_date'])) {
// 	$toDate = explode("/", $_SESSION['end_date']);
// 	$end_date = $toDate[2]."-".$toDate[1]."-".$toDate[0];
// }
$start_date = $_SESSION['start_date'][$demo_id];
$end_date = $_SESSION['end_date'][$demo_id];

$betweenQry = "";
if (!empty($start_date) && !empty($end_date)) {
        // $betweenQry = " AND (timestamp BETWEEN '$start_date' AND '$end_date')";
        $betweenQry = "AND DATE_FORMAT(timestamp, '%Y-%m-%d') >= '$start_date' AND  DATE_FORMAT(timestamp, '%Y-%m-%d') <= '$end_date'";
    } elseif (!empty($start_date) && empty($end_date)) {
        // $betweenQry = " AND timestamp >= '$start_date'";
        $betweenQry = "AND DATE_FORMAT(timestamp, '%Y-%m-%d') >= '$start_date'";
    } elseif (empty($start_date) && !empty($end_date)) {
        // $betweenQry = " AND timestamp <= '$end_date'";
        $betweenQry = "AND DATE_FORMAT(timestamp, '%Y-%m-%d') <= '$end_date'";
    } else {
        $betweenQry = "";
    }

// if($id!="" && $user_type=="Affiliate" && $affiliate_type=="Loreal"){
	
	// $get_Qry="select * from tbl_affiliate where a_user_id=".$id;
	// $get_Res=mysql_query($get_Qry);
	// while($get_value=mysql_fetch_array($get_Res))
	// { 
	//    $TblAffiliateId = $get_value['affiliate_id'];
	// }
    // $QueryCondition="select * from tbl_temp_survey_answers where user_id=".$id." and demo_id='$demo_id' $betweenQry";
// }

// if($id!="" && $user_type=="Admin"){
	$QueryCondition="select * from tbl_temp_survey_answers where user_id=".$id." and demo_id='$demo_id' $betweenQry";
// }
// echo $QueryCondition;

//get results
$getDemoQry=$QueryCondition.$order;
$getDemoRes		=	$DBCONN->prepare($getDemoQry);
$getDemoRes->execute();
$getDemoCnt		=	$getDemoRes->rowCount();

$records_perpage=100;
if($records_perpage>$getDemoCnt){
  $records=$records_perpage;
}
else{
   $records=$getDemoCnt;
}
include_once("includes/lorealheader.php");
?>
<div class="container">
    <div class="col-md-12 col-sm-12 col-xs-12 questions remove-padding-right-left rawdatatop">
        <div class="col-md-7 col-sm-7 col-xs-7 ">
            <p class="chart_title_content"><?php  echo $getLangContent["Rawresponsedata"]["rawhead"];?></p>
        </div>
        <div class="col-md-5 col-sm-5 col-xs-5">
            <p class="chart_title_content_res"><?php  echo $getLangContent["Rawresponsedata"]["rawcount"];?> <?php echo $getDemoCnt; ?></p>
        </div>
    </div>
</div>
<div class="container responsetblContainer" style="min-height: 300px;">
    <form name="frm_responsedata" method="post">
		<input type="hidden" name="HiddenMode" id="HiddenMode" value="">
		<input type="hidden" name="HdnPage" id="HdnPage" value="">
		<div class="container">
		<div class="col-md-12 col-sm-12 col-xs-12 page_nation"> 
	            <?php								
							
					$TotalRecords	=	$getDemoCnt;
					if($TotalRecords <= (($Page * $records_perpage)-$records_perpage))
						$Page	=	$Page-1;
					$TotalPages		=	ceil($TotalRecords/$records_perpage);
					$Start			=	($Page-1)*$records_perpage;
					if($TotalPages>1)
					  {
					?>
					<table class="pagetbl">
					<tr>
						<td align="center" colspan="11" style="padding-bottom: 0px;">
							<?php
							  $FormName="frm_responsedata";
							  include("paging.php");
							?>
						</td>
					</tr>
					</table>
					<?php
					  }
					?>
			</div>
		</div>
        <div class="table-responsive" style="margin-top:0px;min-height:370px;">          
		    <table class="table">
			    <thead>
			        <tr>
				        <th nowrap class="lorealth" style="width: 180px;" onclick="document.location='response_rawdata.php?demo_id=<?php echo $demo_id ?>&sort=<?php echo $rsort;?>&field=timestamp'"><?php  echo $getLangContent["Rawresponsedata"]["timestamp"];?> &nbsp;&nbsp;<?php if($rpath!=""){?><img src="<?php echo $rpath;?>" ><?php }?></th>

				        <th  nowrap class="lorealth"  onclick="document.location='response_rawdata.php?demo_id=<?php echo $demo_id ?>&sort=<?php echo $q1sort;?>&field=quest1'"><?php  echo $getLangContent["Rawresponsedata"]["pleasent"];?> &nbsp;&nbsp;<?php if($q1path!=""){?><img src="<?php echo $q1path;?>"><?php }?></th>

				        <th  nowrap class="lorealth" onclick="document.location='response_rawdata.php?demo_id=<?php echo $demo_id ?>&sort=<?php echo $q2sort;?>&field=quest2'"><?php  echo $getLangContent["Rawresponsedata"]["window"];?> &nbsp;&nbsp;<?php if($q2path!=""){?><img src="<?php echo $q2path;?>" ><?php }?></th>

				       <th nowrap class="lorealth"  onclick="document.location='response_rawdata.php?demo_id=<?php echo $demo_id ?>&sort=<?php echo $q3sort;?>&field=quest3'"><?php  echo $getLangContent["Rawresponsedata"]["greeting"];?> &nbsp;&nbsp;<?php if($q3path!=""){?><img src="<?php echo $q3path;?>" ><?php }?></th>

				       <th nowrap class="lorealth"  onclick="document.location='response_rawdata.php?demo_id=<?php echo $demo_id ?>&sort=<?php echo $q4sort;?>&field=quest4'"><?php  echo $getLangContent["Rawresponsedata"]["consulting"];?> &nbsp;&nbsp;<?php if($q4path!=""){?><img src="<?php echo $q4path;?>" ><?php }?></th>

				       <th nowrap class="lorealth"  onclick="document.location='response_rawdata.php?demo_id=<?php echo $demo_id ?>&sort=<?php echo $q5sort;?>&field=quest5'"><?php  echo $getLangContent["Rawresponsedata"]["listened"];?> &nbsp;&nbsp;<?php if($q5path!=""){?><img src="<?php echo $q5path;?>" ><?php }?></th>

				       <th nowrap class="lorealth"  onclick="document.location='response_rawdata.php?demo_id=<?php echo $demo_id ?>&sort=<?php echo $q6sort;?>&field=quest6'"><?php  echo $getLangContent["Rawresponsedata"]["color"];?> &nbsp;&nbsp;<?php if($q6path!=""){?><img src="<?php echo $q6path;?>" ><?php }?></th>

				       <th nowrap class="lorealth"  onclick="document.location='response_rawdata.php?demo_id=<?php echo $demo_id ?>&sort=<?php echo $q7sort;?>&field=quest7'"><?php  echo $getLangContent["Rawresponsedata"]["treatement"];?> &nbsp;&nbsp;<?php if($q7path!=""){?><img src="<?php echo $q7path;?>" ><?php }?></th>

				       <th nowrap class="lorealth"  onclick="document.location='response_rawdata.php?demo_id=<?php echo $demo_id ?>&sort=<?php echo $q8sort;?>&field=quest8'"><?php  echo $getLangContent["Rawresponsedata"]["backwash"];?> &nbsp;&nbsp;<?php if($q8path!=""){?><img src="<?php echo $q8path;?>" ><?php }?></th>

				       <th nowrap class="lorealth"  onclick="document.location='response_rawdata.php?demo_id=<?php echo $demo_id ?>&sort=<?php echo $q9sort;?>&field=quest9'"><?php  echo $getLangContent["Rawresponsedata"]["satisfied"];?> &nbsp;&nbsp;<?php if($q9path!=""){?><img src="<?php echo $q9path;?>" ><?php }?></th>

				       <th nowrap class="lorealth"  onclick="document.location='response_rawdata.php?demo_id=<?php echo $demo_id ?>&sort=<?php echo $q10sort;?>&field=quest10'"><?php  echo $getLangContent["Rawresponsedata"]["home"];?> &nbsp;&nbsp;<?php if($q10path!=""){?><img src="<?php echo $q10path;?>" ><?php }?></th>

				       <th nowrap class="lorealth"  onclick="document.location='response_rawdata.php?demo_id=<?php echo $demo_id ?>&sort=<?php echo $q11sort;?>&field=quest11'"><?php  echo $getLangContent["Rawresponsedata"]["result"];?> &nbsp;&nbsp;<?php if($q11path!=""){?><img src="<?php echo $q11path;?>" ><?php }?></th>

				       <th nowrap class="lorealth"  onclick="document.location='response_rawdata.php?demo_id=<?php echo $demo_id ?>&sort=<?php echo $q12sort;?>&field=quest12'"><?php  echo $getLangContent["Rawresponsedata"]["comeback"];?> &nbsp;&nbsp;<?php if($q12path!=""){?><img src="<?php echo $q12path;?>" ><?php }?></th>
			        </tr>
			    </thead>
			    <tbody>
			        <tr style="border: 1px solid white;">
					  <td colspan="13" style="height:10px;"></td>
	                <tr>
			        <?php
						if($getDemoCnt>0){
							$records_perpage=100;
							$TotalRecords	=	$getDemoCnt;
							if($TotalRecords <= (($Page * $records_perpage)-$records_perpage))
								$Page	=	$Page-1;
							$TotalPages		=	ceil($TotalRecords/$records_perpage);
							$Start			=	($Page-1)*$records_perpage;
							
							//code for paging ends
							$getDemoQry.=" limit $Start,$records_perpage";
							
							$getDemoRes		=	$DBCONN->prepare($getDemoQry);
							$getDemoRes->execute();
							$getDemoCnt		=	$getDemoRes->rowCount();
							$getDemoRes		=	$getDemoRes->fetchAll();

							$i=1;
							$j=0;
							$demo_id_array=array();
							foreach($getDemoRes as $getDemoRow){
								$timestamp=date("d/m/Y h:i:s",strtotime($getDemoRow["timestamp"]));
								$qstn1_ans=$getDemoRow["qstn1_ans"];
								$qstn2_ans=$getDemoRow["qstn2_ans"];
								$qstn3_ans=$getDemoRow["qstn3_ans"];
								$qstn4_ans=$getDemoRow["qstn4_ans"];
								$qstn5_ans=$getDemoRow["qstn5_ans"];
								$qstn6_ans=$getDemoRow["qstn6_ans"];
								$qstn7_ans=$getDemoRow["qstn7_ans"];
								$qstn8_ans=$getDemoRow["qstn8_ans"];
								$qstn9_ans=$getDemoRow["qstn9_ans"];
								$qstn10_ans=$getDemoRow["qstn10_ans"];
								$qstn11_ans=$getDemoRow["qstn11_ans"];
								$qstn12_ans=$getDemoRow["qstn12_ans"];
								if($qstn12_ans=="")
									$qstn12_ans=0;
								$bgcolor	=($i%2==0)?'#e2e1e1':'white';
								
						    ?>
							
					<tr bgcolor="<?php echo $bgcolor;?>">
						<td style="cursor:pointer;"><?php echo  readmore_view(stripslashes($timestamp),"60");?></td>
				        <td style="cursor:pointer;" ><?php echo  readmore_view(stripslashes($qstn1_ans),"60");?></td>
						<td style="cursor:pointer;"><?php echo $qstn2_ans ?></td>
						<td style="cursor:pointer;"><?php echo $qstn3_ans ?></td>
						<td nowrap><?php echo $qstn4_ans ?></td>
				        <td nowrap><?php echo $qstn5_ans ?></td>
				        <td nowrap><?php echo $qstn6_ans ?></td>
				        <td  ><?php echo $qstn7_ans ?></td>
				        <td  ><?php echo $qstn8_ans ?></td>
				        <td  ><?php echo $qstn9_ans ?></td>
				        <td  ><?php echo $qstn10_ans ?></td>
				        <td  ><?php echo $qstn11_ans ?></td>
				        <td  ><?php echo $qstn12_ans ?></td>
					</tr>
					<?php
					  $i++;
						}
						?>
			    </tbody>
			</table>
		<div class="container" style="padding:0px">
		<div class="col-md-12 col-xs-12 col-sm-12 page_nation" style="padding:0px;margin-left: 40px;">
		
		<?php
			if($TotalPages>1)
			{
			?>
			<table align="right">
				<tr>
					<td align="center">
						<?php
						$FormName="frm_responsedata";
						include("paging.php");
						?>
					</td>
				</tr>
				
			<?php
				}
			}
			else{
				echo "<tr bgcolor='#a5a5a5'><td colspan=\"13\" style=\"border-bottom:1px solid grey;\"><center>".$getLangContent["Rawresponsedata"]["nofound"]."</center></td></tr>";
			}

			?>
			</table>
		</div>
	 </div>
		</form>
   </div>
</div>
<div class="container">
	<div class="col-md-12 col-sm-12 col-xs-12 remove-padding-right-left" style="margin-top:10px;padding-bottom: 20px;">
	        <div class="col-md-6 col-sm-6 col-xs-6 remove-padding-right-left">
	            <a href="survey_question_dashboard.php?demo_id=<?php echo $_SESSION["demo_id"]; ?>"><input type="button" name="back" class="btn chart_btn commonbtn" value="<?php  echo $getLangContent["Rawresponsedata"]["back"];?>"></a>
	        </div>
	        <!-- <div class="col-md-6 col-sm-6 col-xs-6 text-right remove-padding-right-left">
	            <input type="button" name="next" class="btn chart_btn commonbtn" value="Next">
	        </div> -->
	    </div>
	</div>
</div>

<?php
if ($getDemoCnt <= 15 ) { ?>
<div id="footer">
	<?php include("includes/lorealfooter.php"); ?> 
</div>
<?php
} else { include("includes/lorealfooter.php"); ?> 
<?php } 
?> 


<script>
function edit_record(val){
  //document.location.href="newdemo.php?demo_id="+val;
}
/****function for paging statrs*******/
function pagetransfer(pagenumber,formname)
{	
	with(document.forms[formname])
	{ 
		HdnPage.value=pagenumber;
		HiddenMode.value="paging";
		submit();
	}
}
/****function for paging ends*******/
 
</script>
