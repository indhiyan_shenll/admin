<?php
error_reporting(0);


function getLocationInfoByIp(){
	$client  = @$_SERVER['HTTP_CLIENT_IP'];
	$forward = @$_SERVER['HTTP_X_FORWARDED_FOR'];
	$remote  = @$_SERVER['REMOTE_ADDR'];
	$result  = array('country'=>'', 'city'=>'');
	if(filter_var($client, FILTER_VALIDATE_IP)){
		$ip = $client;
	} elseif (filter_var($forward, FILTER_VALIDATE_IP)){
		$ip = $forward;
	} else {
		$ip = $remote;
	}
	$ip_data = @json_decode(file_get_contents("http://www.geoplugin.net/json.gp?ip=".$ip));
	if($ip_data && $ip_data->geoplugin_countryName != null){
		$result['country'] = $ip_data->geoplugin_countryName;
		$result['city'] = $ip_data->geoplugin_city;
	}

	return $result['country'];
}

$geo_country_name=getLocationInfoByIp();
$geo_country_name = strtolower($geo_country_name);

$Euro_array = array("austria","belgium","cyprus","estonia","finland","france","germany","greece","ireland","italy","latvia","luxembourg"," malta","the netherlands","portugal","slovakia","slovenia","spain");
$Aud_array = array("australia", "new zealand");
$Gbp_array = array("england", "scotland");
$Sek_array = array("sweden"); 

if(in_array($geo_country_name,$Euro_array) && $currency_flag=="") {
    $currency_flag="EUR";
	$currency_symbol="€";
}
if(in_array($geo_country_name,$Aud_array) && $currency_flag=="") {
    $currency_flag="AUD";
	$currency_symbol="$";
}
if(in_array($geo_country_name,$Gbp_array) && $currency_flag=="") {
    $currency_flag="GBP";
	$currency_symbol="£";
}
if(in_array($geo_country_name,$Sek_array) && $currency_flag=="") {
    $currency_flag="SEK";
	$currency_symbol="kr";
}
if($currency_flag=="" || empty($currency_flag)){
   $currency_flag="USD";
   $currency_symbol="$";
}


?>
