<style>
.Pagetext
{
	font-family:arial;
	font-size:11px;
	color:white;
	font-weight:bold;
	background-color: black !important;
}

.Pagetext1
{
   	font-family:arial;
	font-size:10px;
	padding-left:5px;
   	padding-right:5px;
	font-weight:bold;
	color:black;
	/*padding: 10px 10px 10px 10px;*/
}

a.PageLink:link {color:black;font-size:11px;font-weight:bold;font-family:arial;text-decoration:none;}
a.PageLink:visited {color:black;font-size:11px;font-weight:bold;font-family:arial;text-decoration:none;}
a.PageLink:active {color:white;font-size:11px;font-weight:bold;font-family:arial;text-decoration:none;}
a.PageLink:hover {color: white;font-size:11px;font-weight:bold;font-family:arial;text-decoration:none;}
</style>
<table border="0" cellpadding="0" cellspacing="0">
	<tr><td height="10px"></td></tr>
	<tr >
	<!-- Paging code starts here -->
	<?php	
	$CurrentPage=$Page;
	if ($TotalPages > 1)
	{	
	?>
		<td class="Pagetext1" align="center">PAGE  <B>[ <?php echo $CurrentPage?> of <?php echo $TotalPages?> ]</B></td>
		<td style='width:10px'></td>
	<?php							
		if($CurrentPage==1)
		{
			echo "<td  class='Pagetext' align='center'><img src='".HTTP_ROOT_FOLDER."images/paging_images/first_d.gif' border='0' height='30px' width='30px';></td> ";
		}
		else
		{
			echo "<td class='Pagetext' align='center' ><a href=\"javascript: pagetransfer(1,'$FormName')\" class='PageLink' onmouseover='roll(\"first\", \"".HTTP_ROOT_FOLDER."images/paging_images/first_over.png\")' onmouseout='roll(\"first\", \"".HTTP_ROOT_FOLDER."images/paging_images/first.gif\")'><img src='".HTTP_ROOT_FOLDER."images/paging_images/first.gif' border='0' title='First Page' name='first'height='30px' width='30px';></a></td>";
		}
		echo "<td style='width:10px'></td>";
		$cp=$CurrentPage;
		if ($cp<=1)
			echo "<td class='Pagetext' align='center'><img src='".HTTP_ROOT_FOLDER."images/paging_images/prev_d.gif' border='0' height='30px' width='30px';></td> ";
		else
		{
			$cp--;
			echo "<td class='Pagetext' align='center'><a href=\"javascript: pagetransfer($cp,'$FormName')\" class='PageLink' onmouseover='roll(\"prev\", \"".HTTP_ROOT_FOLDER."images/paging_images/prev_over.png\")' onmouseout='roll(\"prev\", \"".HTTP_ROOT_FOLDER."images/paging_images/prev.gif\")'><img src='".HTTP_ROOT_FOLDER."images/paging_images/prev.gif' border='0' title='Previous Page' name='prev' height='30px' width='30px';></a></td>";
		}
		echo "<td style='width:10px'></td>";
		for($i=1;$i<=10;$i++)
		{	
			$disp_i=$i+(10*floor(($CurrentPage)/10));
			if($CurrentPage>=10)
			{
				$disp_i=$disp_i-5;
				if($CurrentPage>=15 && $CurrentPage%10>=5)
				{
					$disp_i=$disp_i+5;
				}
			}
			if($disp_i<=$TotalPages)
			{
				if($disp_i==$CurrentPage)
				{
					//background:url(\"".HTTP_ROOT_FOLDER."images/paging_images/currentpage.gif\");background-repeat:no-repeat;
				?>
					<b><?php echo "<td class='Pagetext' align='center' width='30px' style='background-color:#f26822;vertical-align:middle;' valign='bottom'>".$disp_i."</td>" ?></b>
				<?php
				}
				else
				{
					//;background:url(\"".HTTP_ROOT_FOLDER."images/paging_images/pagenumber.gif\");background-repeat:no-repeat;
					echo "<td style='cursor:pointer;border:1px solid black;color:black;background-color:#FFFFFF;vertical-align:middle;' id='numdisptd".$i."' align='center' onmouseover='return fun_change_tbackground(\"numdisptd$i\",\"onmousedisplay\");' onmouseout='return fun_change_tbackground(\"numdisptd$i\",\"bgdisplay\");' width='30px' height='25px' onclick='pagetransfer(\"$disp_i\",\"$FormName\")' valign='bottom'><a href=\"javascript: pagetransfer($disp_i,'$FormName')\"  class='PageLink' align='center' >$disp_i</a></td> ";
				}
				echo "<td style='width:10px'></td>";
			}
		}
		$forward=$CurrentPage;
		if ($forward>=$TotalPages)
		{
			echo "<td  class='Pagetext' align='center'><img src='".HTTP_ROOT_FOLDER."images/paging_images/next_d.gif' border='0' height='30px' width='30px';></td> ";
		}
		else
		{
			$forward_page=$CurrentPage+1;
			echo "<td class='Pagetext' align='center'><a href=\"javascript: pagetransfer($forward_page,'$FormName')\" class='PageLink' onmouseover='roll(\"next\", \"".HTTP_ROOT_FOLDER."images/paging_images/next_over.png\")' onmouseout='roll(\"next\", \"".HTTP_ROOT_FOLDER."images/paging_images/next.gif\")'><img src='".HTTP_ROOT_FOLDER."images/paging_images/next.gif' border='0' title='Next Page' name='next' height='30px' width='30px';></a></td>";
		}
		echo "<td style='width:10px'></td>";
		if($CurrentPage==$TotalPages)
		{
			echo "<td  class='Pagetext' align='center'><img src='".HTTP_ROOT_FOLDER."images/paging_images/last_d.gif' border='0' height='30px' width='30px';></td>";
		}
		  else
		{
			echo "<td  class='Pagetext' align='center' ><a href=\"javascript: pagetransfer($TotalPages,'$FormName')\" class='PageLink' onmouseover='roll(\"last\", \"".HTTP_ROOT_FOLDER."images/paging_images/last_over.png\")' onmouseout='roll(\"last\", \"".HTTP_ROOT_FOLDER."images/paging_images/last.gif\")'><img src='".HTTP_ROOT_FOLDER."images/paging_images/last.gif' border='0' title='Last Page' name='last' height='30px' width='30px';></a></td> ";
		}
	}
	?>
	<!-- paging code ends here --> 
	</tr>
</table>
<script>
function fun_change_tbackground(tdid,varclassname)
{
	/*
	if(varclassname == 'onmousedisplay')
		document.getElementById(tdid).style.backgroundImage="url(<?php echo HTTP_ROOT_FOLDER; ?>images/paging_images/currentpage.gif)";
	else if(varclassname == 'bgdisplay')
		document.getElementById(tdid).style.backgroundImage="url(<?php echo HTTP_ROOT_FOLDER; ?>images/paging_images/pagenumber.gif)";
	*/
	if(varclassname == 'onmousedisplay')
		document.getElementById(tdid).style.backgroundColor="#000000";
	else if(varclassname == 'bgdisplay')
		document.getElementById(tdid).style.backgroundColor="#FFFFFF";
	//document.getElementById(tdid).className=varclassname;
}
function roll(img_name,img_src)
{
	document[img_name].src = img_src;
}
</script>