<?php
include_once ("../../includes/configure.php");
include_once ("includes/loreal_sessioncheck.php");
include_once ("survey_answers.php");

if (isset($_GET["demo_id"])) {

    $demo_id = trim($_GET["demo_id"]);
    $user_id = trim($_SESSION["user_id"]);
    $_SESSION["demo_id"] = $demo_id;

    $demoQry = $DBCONN->prepare("SELECT * FROM tbl_demo WHERE demo_id=:demo_id");
    $demoQryArr = array(":demo_id" =>$demo_id );
    $demoQry->execute($demoQryArr);
    $getRowCnt = $demoQry->rowCount();
    if ($getRowCnt > 0) {
        $fetchMasterinfo = $demoQry->fetch(PDO::FETCH_ASSOC);
    }
     
    $DemoFirstName = $fetchMasterinfo["first_name"];
    $googleTabname = $fetchMasterinfo["google_tab_name"];
    $businessName = $fetchMasterinfo["business_name"];

    //Mail send affilate name and phone
    $affilateid = $fetchMasterinfo["a_id"];
    $affilateQry = $DBCONN->prepare("SELECT * FROM tbl_affiliate WHERE affiliate_id=:affiliateid");
    $affilateQryArr = array(":affiliateid" =>$affilateid);
    $affilateQry->execute($affilateQryArr);
    $getRowCnts = $affilateQry->rowCount();
    if ($getRowCnts > 0) {
        $fetchaffilatedata = $affilateQry->fetch(PDO::FETCH_ASSOC);
        $full_name = $fetchaffilatedata["full_name"];
        $firstname =html_entity_decode($full_name[0]);
        $mobile_phone=$fetchaffilatedata["mobile_phone"];
        $emailAddress=$fetchaffilatedata["email"];
    }

    $chartURL = "";
    if (!empty($googleTabname) && in_array($googleTabname, $Arrsheets)) {
        $chartURL = "survey_answers_chart.php";
    }
    

    $user_type = $_SESSION['user_type'];
    $affiliate_type = $_SESSION['affiliate_type'];

    $surveyArrvalues = "";
    foreach ($questionRes as $surveyKeys => $surveyValues) {
        $surveyArrvalues[] = array_values($surveyValues);
    }

    if (!empty($surveyArrvalues)) {

        $deleteUseransQry = $DBCONN->prepare("DELETE FROM tbl_temp_survey_answers WHERE demo_id=:demo_id AND user_id=:user_id ORDER BY timestamp ASC");
        $deleteUseransQryArr = array(":demo_id" =>$demo_id, ":user_id" =>$user_id );
        $deleteUseransQry->execute($deleteUseransQryArr);

        foreach ($surveyArrvalues as $surveyAns) {
            $timestamp = "";

            if (!empty($surveyAns[0])) {
                $dbtimeStamp = str_replace('/', '-', $surveyAns[0]);
                $dateTime = explode(" ", $dbtimeStamp);            
                $date = explode("-", $dateTime[0]);
                $dateFormat = $date[2]."-".$date[1]."-".$date[0];
                $timestamp = date("Y-m-d H:i:s", strtotime($dateFormat." ".$dateTime[1]));

                // $time = !empty($dateTime[1]) ? $dateTime[1] : "00:00:00";
                // $timestamp = $dateFormat." ".$time;
                // $timestamp = date("Y-m-d H:i:s", strtotime($dateFormat." ".$dateTime[1]));
                // $timestamp = $dateFormat." ".$dateTime[1];
            }
            $qstn1_ans = ($surveyAns[1]) ? $surveyAns[1] : "";
            $qstn2_ans = ($surveyAns[2]) ? $surveyAns[2] : "";
            $qstn3_ans = ($surveyAns[3]) ? $surveyAns[3] : "";
            $qstn4_ans = ($surveyAns[4]) ? $surveyAns[4] : "";
            $qstn5_ans = ($surveyAns[5]) ? $surveyAns[5] : "";
            $qstn6_ans = ($surveyAns[6]) ? $surveyAns[6] : "";
            $qstn7_ans = ($surveyAns[7]) ? $surveyAns[7] : "";
            $qstn8_ans = ($surveyAns[8]) ? $surveyAns[8] : "";
            $qstn9_ans = ($surveyAns[9]) ? $surveyAns[9] : "";
            $qstn10_ans = ($surveyAns[10]) ? $surveyAns[10] : "";
            $qstn11_ans = ($surveyAns[11]) ? $surveyAns[11] : "";
            $qstn12_ans = ($surveyAns[12]) ? $surveyAns[12] : "";
            $createDate = date("Y-m-d H:i:s");

            $businessAnsInsert = $DBCONN->prepare("INSERT INTO tbl_temp_survey_answers (user_id, business_name, demo_id, timestamp, qstn1_ans, qstn2_ans, qstn3_ans, qstn4_ans, qstn5_ans, qstn6_ans, qstn7_ans, qstn8_ans, qstn9_ans, qstn10_ans, qstn11_ans, qstn12_ans, created_date) VALUES (:user_id, :business_name, :demo_id, :timestamp, :qstn1_ans, :qstn2_ans, :qstn3_ans, :qstn4_ans, :qstn5_ans, :qstn6_ans, :qstn7_ans, :qstn8_ans, :qstn9_ans, :qstn10_ans, :qstn11_ans, :qstn12_ans, :created_date)");
            $answers = array (  ":user_id" => $_SESSION['user_id'],
                             ":business_name" => $businessName,
                             ":demo_id" => $demo_id,
                             ":timestamp" => $timestamp,
                             ":qstn1_ans" => $qstn1_ans,
                             ":qstn2_ans" => $qstn2_ans,
                             ":qstn3_ans" => $qstn3_ans,
                             ":qstn4_ans" => $qstn4_ans,
                             ":qstn5_ans" => $qstn5_ans,
                             ":qstn6_ans" => $qstn6_ans,
                             ":qstn7_ans" => $qstn7_ans,
                             ":qstn8_ans" => $qstn8_ans,
                             ":qstn9_ans" => $qstn9_ans,
                             ":qstn10_ans" => $qstn10_ans,
                             ":qstn11_ans" => $qstn11_ans,
                             ":qstn12_ans" => $qstn12_ans,
                             ":created_date" => $createDate
                         );
            $businessAnsInsert->execute($answers);
        }

        $start_date = $_SESSION['start_date'][$demo_id];
        $end_date = $_SESSION['end_date'][$demo_id];

        $betweenQry = "";
        if (!empty($start_date) && !empty($end_date)) {
            $betweenQry = " AND (timestamp BETWEEN '$start_date' AND '$end_date')";
        } else if (!empty($start_date) && empty($end_date)) {
            $betweenQry = " AND timestamp >= '$start_date'";
        } else if (empty($start_date) && !empty($end_date)) {
            $betweenQry = " AND timestamp <= '$end_date'";
        } else {
            $betweenQry = "";
        }

        
        //Get count of responses for the user
        $countRresQry = $DBCONN->prepare("SELECT count(user_id) as user_id FROM tbl_temp_survey_answers where user_id=:user_id and demo_id=:demo_id $betweenQry ORDER BY timestamp ASC");
        $countRresQryArr = array(":demo_id" =>$demo_id, ":user_id" =>$user_id );
        $countRresQry->execute($countRresQryArr);
        $getResCnt = $countRresQry->rowCount();
        $surveyResCount = 0;
        if ($getResCnt > 0) {
            $surveyRes = $countRresQry->fetch(PDO::FETCH_ASSOC);
            $surveyResCount = $surveyRes["user_id"];
            
        }

    }

    $previousMonth = date("m/Y", strtotime('-1 month'));
    $surveyAnsQry = $DBCONN->prepare("SELECT *, DATE_FORMAT(timestamp, '%m/%Y') as newtime  FROM tbl_temp_survey_answers  where user_id=:user_id and demo_id=:demo_id AND DATE_FORMAT(timestamp, '%m/%Y')='$previousMonth' ORDER BY timestamp ASC");
    // $surveyAnsQry = $DBCONN->prepare("SELECT * FROM tbl_temp_survey_answers where user_id=:user_id and demo_id=:demo_id ORDER BY timestamp ASC");
    $surveyAnsQryArr = array(":demo_id" =>$demo_id, ":user_id" =>$user_id );
    $surveyAnsQry->execute($surveyAnsQryArr);
    $getResCnt = $surveyAnsQry->rowCount();
    
    if ($getResCnt > 0) {
        $masterSurveyRes = $surveyAnsQry->fetchAll(PDO::FETCH_ASSOC);
        
        $qstnArr = "";
        $q = 0;
        foreach ($masterSurveyRes as $masterSurveyval) {
            $qstnArr["qstn1_ans"][] = $masterSurveyval["qstn1_ans"];
            $qstnArr["qstn2_ans"][] = $masterSurveyval["qstn2_ans"];
            $qstnArr["qstn3_ans"][] = $masterSurveyval["qstn3_ans"];
            $qstnArr["qstn4_ans"][] = $masterSurveyval["qstn4_ans"];
            $qstnArr["qstn5_ans"][] = $masterSurveyval["qstn5_ans"];
            $qstnArr["qstn6_ans"][] = $masterSurveyval["qstn6_ans"];
            $qstnArr["qstn7_ans"][] = $masterSurveyval["qstn7_ans"];
            $qstnArr["qstn8_ans"][] = $masterSurveyval["qstn8_ans"];
            $qstnArr["qstn9_ans"][] = $masterSurveyval["qstn9_ans"];
            $qstnArr["qstn10_ans"][] = $masterSurveyval["qstn10_ans"];
            $qstnArr["qstn11_ans"][] = $masterSurveyval["qstn11_ans"];
            $qstnArr["qstn12_ans"][] = $masterSurveyval["qstn12_ans"];
            $q++;

        }
        $qstnAnsAvg = "";
        $yesnoqstn = "";
        foreach ($qstnArr as $qstnNo => $qstnvals) {
            if ($qstnNo == "qstn4_ans" || $qstnNo == "qstn5_ans" || $qstnNo == "qstn8_ans" || $qstnNo == "qstn10_ans" || $qstnNo == "qstn12_ans") {
                $yesnoqstn = array_count_values($qstnArr[$qstnNo]);

                foreach ($yesnoqstn as $ansKey => $ansVal) {

                    $qstnAnsAvg[$qstnNo][$ansKey] = round(($yesnoqstn[$ansKey] / array_sum($yesnoqstn)) * 100, 1)."%";
                   
                }                    
            } else if ($qstnNo == "qstn6_ans") {
                $a = 1;
                foreach ($qstnvals as $ansKey => $ansVal) { 
                    if (strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', $ansVal)) != "notrelevant") {
                        $cntans = $a;
                        $qstn6ans = explode(" ", $ansVal);
                        $qstn6arr[$qstnNo][] = $qstn6ans[0];
                        $qstnAnsAvg[$qstnNo] = round(array_sum($qstn6arr[$qstnNo]) / count($qstn6arr[$qstnNo]), 1);
                        $a++;
                    }

                }
            } else {
                $qstnAnsAvg[$qstnNo] = round(array_sum($qstnvals) / count($qstnvals), 1);
            }            
        }

        $backgroundColor = "";
        for ($k = 1; $k <=count($qstnAnsAvg); $k++) {

            $qstncolorQry = $DBCONN->prepare("SELECT green, amber FROM tbl_survey_benchmark WHERE survey_id=:id");
            $qstncolorQryArr = array(":id" => $k);
            $qstncolorQry->execute($qstncolorQryArr);
            $getQstCnt = $qstncolorQry->rowCount();
            

            if ($getQstCnt > 0) {
                $QstnResData = $qstncolorQry->fetch(PDO::FETCH_ASSOC);

                if ($qstnAnsAvg["qstn".$k."_ans"] >= $QstnResData['green']) {            
                    $backgroundColor["qstn".$k."_ans"] = "background-color: #B4DE86";
                } elseif ($qstnAnsAvg["qstn".$k."_ans"] <= $QstnResData['green'] && $qstnAnsAvg["qstn".$k."_ans"] >= $QstnResData['amber']) {

                    $backgroundColor["qstn".$k."_ans"] = "background-color: #FFDE75";
                } else {

                    $backgroundColor["qstn".$k."_ans"] = "background-color: #F0A48C";
                }
            }
        }
    }
}
include_once("includes/lorealheader.php");
?>
<link rel="stylesheet" type="text/css" href="css/survey_question_dashboard.css" />
<input type="hidden" id="demo_id" value="<?php echo $demo_id; ?>" >
<style>
@media screen and (min-width: 0px) and (max-width: 1356px) {
    #footer {
        position: relative;
    }
    
}
</style>
<div class="container chartcontainer">

    <div class="col-md-12 col-sm-12 col-xs-12 salong_abc remove-padding-right-left">
        <div class="col-md-3 col-sm-4 col-xs-12 remove-padding-right-left">
            <form name="survey-filter" id="survey-filter" >
                <div class="chart_title">
                    <p class="chart_title_content"><?php echo stripslashes($businessName); ?></p>
                    
                </div>
                <div class="reporting-period">
                    <p>REPORTING PERIOD:</p>
                </div>
                <?php 
                $allDateschecked = "checked";
                $dateRangechecked = "";

                if (!empty($_SESSION["start_date"][$demo_id]) || !empty($_SESSION["end_date"][$demo_id])) {                        
                    $surveyCount = $_SESSION["survey_res_count"][$demo_id];
                } else {
                    $surveyCount = $surveyResCount;
                }

                if (isset($_SESSION["reporting_period"][$demo_id])) {
                    $allDateschecked = ($_SESSION["reporting_period"][$demo_id] == "all dates") ? "checked" : "" ;
                    $dateRangechecked = ($_SESSION["reporting_period"][$demo_id] == "date range") ? "checked" : "" ; 

                    $startDate = !empty($_SESSION["start_date"][$demo_id]) ? date("d/m/Y", strtotime($_SESSION["start_date"][$demo_id])) : "" ;
                    $endDate = !empty($_SESSION["end_date"][$demo_id]) ? date("d/m/Y", strtotime($_SESSION["end_date"][$demo_id])) : "" ;

                       
                }
               ?>
                <div class="radio radio-selection">
                  <label><input type="radio" name="reporting_period" id="all_dates" value="all dates" <?php echo $allDateschecked; ?> >All dates</label>
                </div>
                <div class="radio radio-selection">
                  <label><input type="radio" name="reporting_period" id="date_range" value="date range" <?php echo $dateRangechecked; ?> >Date range</label>
                </div>
                <div class="col-md-12 col-sm-12 col-xs-12 start-date">
                    <div class='col-md-2 col-sm-2 col-xs-12 remove-padding-right-left start-input'>Start</div>
                    <div class='col-md-8 col-sm-8 col-xs-12 remove-padding-right-left'>
                        <div class="form-group">
                            <div class='input-group date' id='datetimepicker6'>
                                <input type='text' name="start_date" class="form-control datepicker" value="<?php echo $startDate; ?>" />
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                            </div>
                        </div>
                    </div>
                </div> 
                <div class="col-md-12 col-sm-12 col-xs-12 end-date">
                    <div class='col-md-2 col-sm-2 col-xs-12 remove-padding-right-left end-input'>End</div>
                    <div class='col-md-8 col-sm-8 col-xs-12 remove-padding-right-left'>
                        <div class="form-group">
                            <div class='input-group date' id='datetimepicker7'>
                                <input type='text' name="end_date" class="form-control datepicker" value="<?php echo $endDate; ?>" />
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                            </div>
                        </div>
                    </div>
                </div> 
                <div class="col-md-6 col-sm-6 col-xs-6 survey_resp remove-padding-right-left back-apply-btn">
                    <a href="loreal_masterlist.php"><input type="button" name="back" class="chart_btn" value="BACK"></a>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-6 text-right remove-padding-right-left back-apply-btn">
                    <input type="button" name="next" id="applybtn" class="chart_btn" value="APPLY">
                </div>    
            </form>
            <div class="col-md-12 col-sm-12 col-xs-12 survey_resp remove-padding-right-left survey-resp-selected">
                <p >Survey responses selected: 
                    <img src="images/survey-loading.gif" alt="Survey Loading" id="survey-loading">
                    <span id="survey-response-count">                    
                    <!-- <?php echo !empty($surveyCount) ? $surveyCount : $surveyResCount; ?> -->
                    <!-- <?php echo isset($_SESSION["survey_res_count"]) ? $surveyCount : $surveyResCount; ?> --> 
                    <?php echo $surveyCount; ?>
                    </span>
                </p>
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12 remove-padding-right-left raw-email-btn">
                <div class="alert alert-success fade in alert-dismissable" id="successalert">
                    <a class="close"  title="close" id="close">x</a>
                    <strong>Report has been emailed successfully</strong>
                </div>
                <div class="col-md-5 col-sm-6 col-xs-6 survey_resp rawbtn">
                    <a href="response_rawdata.php?demo_id=<?php echo $demo_id;?>"><p>Raw Response Data</p></a>
                </div>
                <div class="col-md-2 col-sm-2 col-xs-2"></div>
                <div class="col-md-5 col-sm-6 col-xs-6 text-right rawbtn" id="emailanalysis">
                    <img src="images/survey-loading.gif" alt="Email Loading" id="email-loading">
                    <a id="emailbtn"><p>Email analysis report</p></a>
                </div>
                <!-- <a href="response_rawdata.php?demo_id=<?php echo $demo_id;?>"><input type="button" name="next" class="chart_btn" value="RAW RESPONSNE DATA"></a> -->
            </div> 
        </div>
        <div class="col-md-1 col-sm-1 col-xs-12 remove-padding-right-left">
        </div>
        <img src="images/benchmark-key.png" alt="Benchmark Key" class="img-responsive benchmark-image">
        <div class="col-md-8 col-sm-8 col-xs-12 over">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="col-md-3 col-sm-3 col-xs-3 cheout">
                    <hr class="line_diagonal_checkout_new">
                    <p class="checkout">CHECKOUT</p>
                    <a <?php echo !empty($chartURL) ? "href=".$chartURL."?qstn=1" : "" ; ?> ><div class="pleasant_visit pv_margin chartbox" style="<?php echo $backgroundColor['qstn1_ans']; ?>">Pleasant <br>visit?</div></a>                
                    <a <?php echo !empty($chartURL) ? "href=".$chartURL."?qstn=12" : "" ; ?> ><div class="come_back" style="<?php echo $backgroundColor['qstn12_ans']; ?>">Come <br> back?</div></a>
                    <hr class="line_diagonal_checkout">
                </div>
                <div class="col-md-6 col-sm-6 col-xs-6">
                    <hr class="line_diagonal_skyltfonster_new">
                    <p class="skyltfonster">SKYLTFONSTER</p>
                    <a <?php echo !empty($chartURL) ? "href=".$chartURL."?qstn=2" : "" ; ?> ><div class="come_back_center chartboxcenter" style="<?php echo $backgroundColor['qstn2_ans']; ?>">Window <br>effectiveness?</div></a>
                    <hr class="line_diagonal_skyltfonster">
                </div>

                <div class="col-md-3 col-sm-3 col-xs-3 recep">
                    <hr class="line_diagonal_reception_new">
                    <p class="reception">RECEPTION</p>
                    <a <?php echo !empty($chartURL) ? "href=".$chartURL."?qstn=3" : "" ; ?> ><div class="pleasant_visit chartbox" style="<?php echo $backgroundColor['qstn3_ans']; ?>">Nice <br>greeting?</div></a>
                    <hr class="line_diagonal_reception">
                </div>
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="col-md-3 col-sm-3 col-xs-3 orsal">
                    <p class="reception">FORSALJNINGSYTA</p>
                    <hr class="line_diagonal_forsal_new">
                    <a <?php echo !empty($chartURL) ? "href=".$chartURL."?qstn=11" : "" ; ?> ><div class="come_back chartbox" style="<?php echo $backgroundColor['qstn11_ans']; ?>">Recommend <br>home products?</div></a>
                    <hr class="line_diagonal_forsaljningsyta">
                </div>
               <!--  <div class="col-md-6 col-sm-6 col-xs-12" style='background-image: url("../images/questions.png");min-height: 220px;background-repeat: no-repeat;background-position: 50% 50%;'> -->
                <div class="col-md-6 col-sm-6 col-xs-6 questionsimg" >
                    <img src="images/questions.png" class="img-responsive" style="margin:auto;">
                </div>

                <div class="col-md-3 col-sm-3 col-xs-3">

                </div>
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="col-md-3 col-sm-3 col-xs-3 section-five">
                    <p class="reception">KLIPP / STYLING</p>
                    <hr class="line_diagonal_klipp_new">
                    <a <?php echo !empty($chartURL) ? "href=".$chartURL."?qstn=9" : "" ; ?> ><div class="pleasant_visit pv_margin chartbox" style="<?php echo $backgroundColor['qstn9_ans']; ?>">Mention <br>product names?</div></a>
                    <a <?php echo !empty($chartURL) ? "href=".$chartURL."?qstn=10" : "" ; ?> ><div class="hair_styling" style="<?php echo $backgroundColor['qstn10_ans']; ?>">Hair <br>styling?</div></a>
                    <hr class="line_diagonal_styling">
                </div>
                <div class="col-md-6 col-sm-6 col-xs-6 shamponering section-four-heading">
                    <div class="col-md-2 col-sm-2 col-xs-2"></div>
                    <div class="col-md-10 col-sm-10 col-xs-10 remove-padding-right-left">
                        <div class="col-md-8 col-sm-8 col-xs-8 backwash remove-padding-right-left">
                            <hr class="line_diagonal_teknisk_new">
                            <p class="reception">TEKNISK APPLICERING<br>/ SHAMPONERING</p>
                        </div>
                        <div class="col-md-11 col-sm-12 col-xs-12 backwash section-four pull-right remove-padding-right-left">
                            <a <?php echo !empty($chartURL) ? "href=".$chartURL."?qstn=6" : "" ; ?> ><div class="col-md-10 col-sm-6 col-xs-12 come_back_Application pv_margin chartboxdown" style="<?php echo $backgroundColor['qstn6_ans']; ?>">Application <br>of color?</div></a>
                        </div>
                        <div class="col-md-11 col-sm-12 col-xs-12 backwash section-four pull-right remove-padding-right-left">
                            <a <?php echo !empty($chartURL) ? "href=".$chartURL."?qstn=7" : "" ; ?> ><div class="col-md-5 col-sm-6 col-xs-12 come_back_Application pv_margin chartboxdown" style="<?php echo $backgroundColor['qstn7_ans']; ?>">Backwash <br>expenience?</div></a>
                            <a <?php echo !empty($chartURL) ? "href=".$chartURL."?qstn=8" : "" ; ?> ><div class="col-md-5 col-sm-5 col-xs-12 hair_styling_recommend recommend chartboxdown" style="<?php echo $backgroundColor['qstn8_ans']; ?>">Recommend <br>treatment?</div></a>
                        </div>
                        
                    </div>
                    <hr class="come_shamponering">
                </div>
                <div class="col-md-3 col-sm-3 col-xs-3">
                    <hr class="line_diagonal_konsultation_new">
                    <p class="reception">KONSULTATION / DIAGNOS</p>
                    <a <?php echo !empty($chartURL) ? "href=".$chartURL."?qstn=4" : "" ; ?> ><div class="come_backdiagnos pv_margin chartbox" style="<?php echo $backgroundColor['qstn4_ans']; ?>">Hair <br>consultation?</div></a>
                    <a <?php echo !empty($chartURL) ? "href=".$chartURL."?qstn=5" : "" ; ?> ><div class="come_backdiagnos chartbox" style="<?php echo $backgroundColor['qstn5_ans']; ?>">Listen <br>and respect?</div></a>
                    <hr class="line_diagonal_diagnos">
                </div>
            </div>            
        </div>
    </div>

    <div id="chartcontainer" style="width:1100px;display:none;"><!--  -->
    <?php
        for ($Inc = 1; $Inc <= 12 ; $Inc++) {
            echo '<div id="ajaxchartsvg_'.$Inc.'" style="width: 1000px; height: 350px; margin: 0 auto"></div>';
            ?>
            <canvas id="canvas_<?php echo $Inc;?>" style="width: 1000px; height: 350px; margin: 0 auto">
            </canvas>    
        <?php
        }
    ?>
    </div>

</div>

<div id="footer">
<?php
    include_once("includes/lorealfooter.php");
?>
</div>

<div id="summaryDashboardcanvas" style="background-color:#FFFFFF;margin-top: 50px;" >
    <div class="container chartcontainer" id="summaryDashboard">
        <div class="col-md-12 col-sm-12 col-xs-12 salong_abc remove-padding-right-left">
            <div class="col-md-3 col-sm-4 col-xs-12 remove-padding-right-left">
                <form name="survey-filter" id="survey-filter" >
                    <div class="chart_title">
                        <p class="chart_title_content"><?php echo stripslashes($businessName); ?></p>
                        
                    </div>
                    <?php 
                    $allDateschecked = "checked";
                    $dateRangechecked = "";

                    if (!empty($_SESSION["start_date"][$demo_id]) || !empty($_SESSION["end_date"][$demo_id])) {                        
                        $surveyCount = $_SESSION["survey_res_count"][$demo_id];
                    } else {
                        $surveyCount = $surveyResCount;
                    }
                    
                   ?>
                    <div class="col-md-12 col-sm-12 col-xs-12 survey_resp remove-padding-right-left survey-resp-selected">
                        <p >Survey responses selected:                            
                            <span id="survey-response-count-hidden">                    
                            <?php echo $surveyCount; ?>
                            </span>
                        </p>
                    </div>
                    
            </div>
            <div class="col-md-1 col-sm-1 col-xs-12 remove-padding-right-left">
            </div>
            <img src="images/benchmark-key.png" alt="Benchmark Key" class="img-responsive benchmark-image">
            <div class="col-md-8 col-sm-8 col-xs-12 over">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="col-md-3 col-sm-3 col-xs-3 cheout">
                        <hr class="line_diagonal_checkout_new">
                        <p class="checkout">CHECKOUT</p>
                        <a <?php echo !empty($chartURL) ? "href=".$chartURL."?qstn=1" : "" ; ?> ><div class="pleasant_visit pv_margin chartbox" style="<?php echo $backgroundColor['qstn1_ans']; ?>">Pleasant <br>visit?</div></a>                
                        <a <?php echo !empty($chartURL) ? "href=".$chartURL."?qstn=12" : "" ; ?> ><div class="come_back" style="<?php echo $backgroundColor['qstn12_ans']; ?>">Come <br> back?</div></a>
                        <hr class="line_diagonal_checkout">
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-6">
                        <hr class="line_diagonal_skyltfonster_new">
                        <p class="skyltfonster">SKYLTFONSTER</p>
                        <a <?php echo !empty($chartURL) ? "href=".$chartURL."?qstn=2" : "" ; ?> ><div class="come_back_center chartboxcenter" style="<?php echo $backgroundColor['qstn2_ans']; ?>">Window <br>effectiveness?</div></a>
                        <hr class="line_diagonal_skyltfonster">
                    </div>

                    <div class="col-md-3 col-sm-3 col-xs-3 recep">
                        <hr class="line_diagonal_reception_new">
                        <p class="reception">RECEPTION</p>
                        <a <?php echo !empty($chartURL) ? "href=".$chartURL."?qstn=3" : "" ; ?> ><div class="pleasant_visit chartbox" style="<?php echo $backgroundColor['qstn3_ans']; ?>">Nice <br>greeting?</div></a>
                        <hr class="line_diagonal_reception">
                    </div>
                </div>
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="col-md-3 col-sm-3 col-xs-3 orsal">
                        <p class="reception">FORSALJNINGSYTA</p>
                        <hr class="line_diagonal_forsal_new">
                        <a <?php echo !empty($chartURL) ? "href=".$chartURL."?qstn=11" : "" ; ?> ><div class="come_back chartbox" style="<?php echo $backgroundColor['qstn11_ans']; ?>">Recommend <br>home products?</div></a>
                        <hr class="line_diagonal_forsaljningsyta">
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-6 questionsimg" >
                        <img src="images/questions.png" class="img-responsive" style="margin:auto;">
                    </div>

                    <div class="col-md-3 col-sm-3 col-xs-3">

                    </div>
                </div>
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="col-md-3 col-sm-3 col-xs-3 section-five">
                        <p class="reception">KLIPP / STYLING</p>
                        <hr class="line_diagonal_klipp_new">
                        <a <?php echo !empty($chartURL) ? "href=".$chartURL."?qstn=9" : "" ; ?> ><div class="pleasant_visit pv_margin chartbox" style="<?php echo $backgroundColor['qstn9_ans']; ?>">Mention <br>product names?</div></a>
                        <a <?php echo !empty($chartURL) ? "href=".$chartURL."?qstn=10" : "" ; ?> ><div class="hair_styling" style="<?php echo $backgroundColor['qstn10_ans']; ?>">Hair <br>styling?</div></a>
                        <hr class="line_diagonal_styling">
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-6 shamponering section-four-heading">
                        <div class="col-md-2 col-sm-2 col-xs-2"></div>
                        <div class="col-md-10 col-sm-10 col-xs-10 remove-padding-right-left">
                            <div class="col-md-8 col-sm-8 col-xs-8 backwash remove-padding-right-left">
                                <hr class="line_diagonal_teknisk_new">
                                <p class="reception">TEKNISK APPLICERING<br>/ SHAMPONERING</p>
                            </div>
                            <div class="col-md-11 col-sm-12 col-xs-12 backwash section-four pull-right remove-padding-right-left">
                                <a <?php echo !empty($chartURL) ? "href=".$chartURL."?qstn=6" : "" ; ?> ><div class="col-md-10 col-sm-6 col-xs-12 come_back_Application pv_margin chartboxdown" style="<?php echo $backgroundColor['qstn6_ans']; ?>">Application <br>of color?</div></a>
                            </div>
                            <div class="col-md-11 col-sm-12 col-xs-12 backwash section-four pull-right remove-padding-right-left">
                                <a <?php echo !empty($chartURL) ? "href=".$chartURL."?qstn=7" : "" ; ?> ><div class="col-md-5 col-sm-6 col-xs-12 come_back_Application pv_margin chartboxdown" style="<?php echo $backgroundColor['qstn7_ans']; ?>">Backwash <br>expenience?</div></a>
                                <a <?php echo !empty($chartURL) ? "href=".$chartURL."?qstn=8" : "" ; ?> ><div class="col-md-5 col-sm-5 col-xs-12 hair_styling_recommend recommend chartboxdown" style="<?php echo $backgroundColor['qstn8_ans']; ?>">Recommend <br>treatment?</div></a>
                            </div>
                            
                        </div>
                        <hr class="come_shamponering">
                    </div>
                    <div class="col-md-3 col-sm-3 col-xs-3">
                        <hr class="line_diagonal_konsultation_new">
                        <p class="reception">KONSULTATION / DIAGNOS</p>
                        <a <?php echo !empty($chartURL) ? "href=".$chartURL."?qstn=4" : "" ; ?> ><div class="come_backdiagnos pv_margin chartbox" style="<?php echo $backgroundColor['qstn4_ans']; ?>">Hair <br>consultation?</div></a>
                        <a <?php echo !empty($chartURL) ? "href=".$chartURL."?qstn=5" : "" ; ?> ><div class="come_backdiagnos chartbox" style="<?php echo $backgroundColor['qstn5_ans']; ?>">Listen <br>and respect?</div></a>
                        <hr class="line_diagonal_diagnos">
                    </div>
                </div>            
            </div>
        </div>
    </div>    
</div>

<script type="text/javascript" src="js/rgbcolor.js"></script>
<script type="text/javascript" src="js/StackBlur.js"></script>
<script type="text/javascript" src="js/canvg.js"></script>
<script type="text/javascript" src="js/survey_question_dashboard.js"></script>
<!-- <script src="http://html2canvas.hertzen.com/build/html2canvas.js"></script> -->
<script language="javascript"  src="js/html2canvas.js"></script>
<script language="javascript"  src="js/canvas2image.js"></script>

<script type="text/javascript">

    $(document).on("click","#emailanalysis",function() {

        $("#emailanalysis").addClass("emailcustomloading");
        $("#emailbtn").css("display", "none");
        $("#email-loading").css("display", "inline");

        $("body").css("overflow", "hidden");
        $("#summaryDashboardcanvas").css("display", "block");
        html2canvas($("#summaryDashboard"), {
            allowTaint: true,
            onrendered: function(canvas) {
                dataURL = canvas.toDataURL("image/png");                
                theCanvas = canvas;
                $("#summaryDashboardcanvas").append(canvas);
                $("#summaryDashboardcanvas canvas").attr('width', 1000);
                $("#summaryDashboardcanvas canvas").attr('height', 500);

                $.ajax({
                    type: "POST",
                    dataType: "json",
                    url: "savecharts.php", //second file
                    data: {htmltoimage: dataURL},
                    success: function(summaryStatus) {
                       
                        if (summaryStatus.status) { 
                            $("body").css("overflow", "auto");      
                            $("#summaryDashboardcanvas").css("display", "none");                     
                        }
                    }
                });
            }
        });        

        $.ajax({
            method: "POST",
            url: "survey_answers_chart_email.php",
            data: {post_type: 'sendmail', demo_id: $("#demo_id").val()},
            error: function(){
            
            },
            success: function(response) {

                var data = response;               
                var obj = $.parseJSON( data);
                var imgArr = [];
                var imgnameArr = [];
                
                
                for(var i=1;i<=12;i++) {  

                    var ChartType= obj[i].charttype;    
                     var ImageName  =  obj[i].image_name;

                    if(ChartType == 1){
                        
                        var xaxisValues  =  JSON.parse(obj[i].xaxisValues);
                        var yaxisValues  =  JSON.parse(obj[i].yaxisValues);
                        var green = JSON.parse(obj[i].green);
                        var amber = JSON.parse(obj[i].amber);

                        var chart = customlineChart (i, xaxisValues, yaxisValues, green, amber);
                        var elementID = 'canvas_' +i;
                        var canvas = document.getElementById(elementID);
                        var svgWider = chart.getSVG();

                    } else if (ChartType==2) {   
                       
                        var xaxisValuesYes  =  obj[i].xaxisValuesYes;
                        var yaxisValueYes  =  JSON.parse(obj[i].yaxisValueYes);
                        var yaxisValueNo  =  JSON.parse(obj[i].yaxisValueNo);
                        var yaxisValueMaybe  =  JSON.parse(obj[i].yaxisValueMaybe);

                        var chart = custombarChart (i, xaxisValues, yaxisValueYes, yaxisValueNo, yaxisValueMaybe);
                        var svgWider = chart.getSVG();
                    }
                   
                    canvg(canvas, svgWider);

                    var img = canvas.toDataURL('image/png');
                    img = img.replace('data:image/png;base64,', '');

                    imgArr.push(img);
                    imgnameArr.push(ImageName);

                }

                var bin_data = JSON.stringify(imgArr);
                var imagenameJsonString = JSON.stringify(imgnameArr);
                var data = 'chart_number=' + i+'&bin_data=' + bin_data+'&image_name='+imagenameJsonString+'&demo_id='+'<?php echo trim($_GET["demo_id"]);?>';

                $.ajax({
                    type: 'POST',
                    url: 'savecharts.php',
                    data: data,
                    success: function(data){
                        var saveChartres = data;

                        var DemoFirstName="<?php echo $DemoFirstName ?>";
                        var full_name="<?php echo $full_name ?>";
                        var mobile_phone="<?php echo $mobile_phone ?>";

                        $.ajax({
                            method: "POST",
                            url: "ajax_survey.php",

                            data: {post_type: 'sendmail',salonownername:DemoFirstName, affiliatename:full_name, mobilenumber:mobile_phone, email:'<?php echo $emailAddress; ?>', demo_id: '<?php echo trim($_GET["demo_id"]);?>'},

                            error: function(){
                            
                            },
                            success: function(response) {
                               
                                if (response == "success") {

                                    $("#successalert").css("visibility", "visible");
                                    $("#emailanalysis").removeClass("emailcustomloading");
                                    $("#email-loading").css("display", "none");
                                    $("#emailbtn").css("display", "inline");

                                    setTimeout(function() { 
                                        $("#successalert").css("visibility", "hidden");
                                    }, 5000);
                                }

                            },      
                        })
                        
                    }
                });                

            }      
        })

    }); 
    
</script>
</body>
</html>
