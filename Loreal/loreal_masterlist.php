<?php
include("../../includes/configure.php");
include("includes/loreal_sessioncheck.php");
$id=$_SESSION['user_id'];
$user_name=$_SESSION['user_name'];

$demo_id=$_GET["demo_id"];
$sort=$_GET["sort"];
$field=$_GET["field"];

if($sort==""){
	$sort="asc";
}
if($field==""){
	$field="bus_name";
}

if($field=="bus_name"){
	$fieldname="business_name";
	$order=" order by ".$fieldname." ".$sort;
	if($sort=="asc"){
		$rsort="desc";
		$rpath="images/up.png";
	}
	else{
		$rsort="asc";
		$rpath="images/down.png";
	}
}

if($field=="first_name"){
	$fieldname="first_name";
	$order=" order by ".$fieldname." ".$sort;
	if($sort=="asc"){
		$fsort="desc";
		$fpath="images/up.png";
	}
	else{
		$fsort="asc";
		$fpath="images/down.png";
	}
}

if($field=="phone_number"){
	$fieldname="res1_workphone";
	$order=" order by ".$fieldname." ".$sort;
	if($sort=="asc"){
		$spsort="desc";
		$sppath="images/up.png";
	}
	else{
		$spsort="asc";
		$sppath="images/down.png";
	}
}

$SelectMemberQry="Select * from tbl_demo order by demo_id $sort";	
$getMemberRes=	$DBCONN->prepare($SelectMemberQry);
$getMemberRes->execute();
$getMembercount	=	$getMemberRes->rowCount();
if ($getMembercount>0) {
    $getResRowsdata		=	$getMemberRes->fetchAll();
	foreach($getResRowsdata as $getmeberRow) {
		$memberidArray[]=$getmeberRow["demo_id"];
	}
}

$memberCount=funmembercount($memberidArray,$sort);
$mcount=json_decode($memberCount,true);

if($field=="app_members"){
	if($mcount!=""){
       $order=" and demo_id in (".implode(',',array_keys($mcount)).") ORDER BY FIELD(demo_id,".implode(',',array_keys($mcount))."),demo_id $sort";
	} else {
		$order=" ORDER BY demo_id $sort";
	}
	if($sort=="asc"){
		$asort="desc";
		$apath="images/up.png";
	}
	else{
		$asort="asc";
		$apath="images/down.png";
	}
}
//print_r($memberCount);


if(isset($_POST['HdnPage']) && $_POST['HdnPage']!="" && $_POST['HdnPage']!="0")
	$Page=$_POST['HdnPage'];
else
	$Page=1;

if (isset($_GET["getsearch"])) {
    $SearchboxVal=urldecode(trim($_GET["getsearch"]));
}

if(isset($_POST["search"])){
	  $SearchboxVal=trim($_POST["search"]);
}

$user_type=$_SESSION['user_type'];
$affiliate_type=$_SESSION['affiliate_type'];

if($id!="" && $user_type=="Affiliate" && $affiliate_type=="Loreal Hair") {

	if ($SearchboxVal!="") {
    	$QueryCondition="select * from tbl_demo where (business_name LIKE '%$SearchboxVal%' or first_name LIKE '%$SearchboxVal%') and a_id=".$id."";
     
    } else {
       $QueryCondition="select * from tbl_demo where a_id=".$id;
    }
}
if($id!="" && $user_type=="Admin"){
	
	if($SearchboxVal!=""){
		$QueryCondition="select * from tbl_demo where (business_name LIKE '%$SearchboxVal%' or first_name LIKE '%$SearchboxVal%')";
     
    } else {
       $QueryCondition="select * from tbl_demo where 1=1 ";
    }
}

//get results
$getDemoQry=$QueryCondition.$order;
$getDemoRes		=	$DBCONN->prepare($getDemoQry);
$getDemoRes->execute();
$getDemoCnt		=	$getDemoRes->rowCount();

$records_perpage=100;
if($records_perpage>$getDemoCnt){
  $records=$records_perpage;
}
else{
   $records=$getDemoCnt;
}
include("includes/lorealheader.php");
?>
<div class="container" style="min-height: 390px;">
	<div class="col-md-12 col-sm-12 col-xs-12" style="margin-top: 20px;padding-left: 0px;padding-right: 0px;">
	    <div class="col-md-4 col-sm-12 col-xs-12 searchboxtxt"> 
            <form id="frm_search" id="frm_search" method="post">
	          	<div class="form-group">
	          	    <label><?php  echo $getLangContent["Masterlist"]["searchlabel"];?></label>
				   	<div id="input_container">
				   	  <input type="text" name="search" id="search" class="search form-control" style="height:28px;" value="<?php echo  $SearchboxVal ?>">
				   	  <img src="images/inputclose.png" class="input_img " data_id="reset">
				   	</div>
			    </div>
			</form>
	    </div>
	    <div class="col-md-3 col-sm-12 col-xs-12 emotionimg" > 
            <img src="images/emotion_logo.png" class="img-responsive">
        </div>
        <div class="col-md-5 col-sm-12 col-xs-12 page_nation"> 
            <form name="frm_demolist" method="post">
			<input type="hidden" name="HiddenMode" id="HiddenMode" value="">
			<input type="hidden" name="HdnPage" id="HdnPage" value="">
            <?php								
						
				$TotalRecords	=	$getDemoCnt;
				if($TotalRecords <= (($Page * $records_perpage)-$records_perpage))
					$Page	=	$Page-1;
				$TotalPages		=	ceil($TotalRecords/$records_perpage);
				$Start			=	($Page-1)*$records_perpage;
				if($TotalPages>1)
				  {
				?>
				<table class="pagetbl">
				<tr>
					<td align="center" colspan="11" style="padding-bottom: 0px;">
						<?php
						  $FormName="frm_demolist";
						  include("paging.php");
						?>
					</td>
				</tr>
				</table>
				<?php
				  }
				?>
	    </div>
    </div>
    <div class="col-md-12 col-sm-12 col-xs-12 mastertable" >
    <div class="table-responsive tblline" style="margin-top:7px;min-height:390px;">          
		<table class="table" style="width: 99%;">
			<thead>
			    <tr>
			        <th nowrap class="lorealth"  style="text-align:left;" onclick="document.location='loreal_masterlist.php?sort=<?php echo $rsort;?>&getsearch=<?php echo urlencode($SearchboxVal);?> &field=bus_name'"><?php  echo $getLangContent["Masterlist"]["businesshead"];?> &nbsp;&nbsp;<?php if($rpath!=""){?><img src="<?php echo $rpath;?>" ><?php }?></th>
			        <th  nowrap class="lorealth"  style="text-align:left;" onclick="document.location='loreal_masterlist.php?sort=<?php echo $fsort;?>&getsearch=<?php echo urlencode($SearchboxVal);?>&field=first_name'"><?php  echo $getLangContent["Masterlist"]["contacthead"];?> &nbsp;&nbsp;<?php if($fpath!=""){?><img src="<?php echo $fpath;?>"><?php }?></th>
			        <th  nowrap class="lorealth"  style="text-align:left;" onclick="document.location='loreal_masterlist.php?sort=<?php echo $spsort;?>&getsearch=<?php echo urlencode($SearchboxVal);?>&field=phone_number'"><?php  echo $getLangContent["Masterlist"]["phonehead"];?> &nbsp;&nbsp;<?php if($sppath!=""){?><img src="<?php echo $sppath;?>" ><?php }?></th>
			        <th nowrap class="lorealth"  style="text-align:left;" onclick="document.location='loreal_masterlist.php?sort=<?php echo $asort;?>&getsearch=<?php echo urlencode($SearchboxVal);?>&field=app_members'"><?php  echo $getLangContent["Masterlist"]["appmemberhead"];?> &nbsp;&nbsp;<?php if($apath!=""){?><img src="<?php echo $apath;?>" ><?php }?></th>
				</tr>
			</thead>
			<tbody>
		        <tr style="border: 1px solid white;">
				  <td colspan="4" style="height:10px;"></td>
                <tr>
	        <?php
				if($getDemoCnt>0){
					$records_perpage=100;
					$TotalRecords	=	$getDemoCnt;
					if($TotalRecords <= (($Page * $records_perpage)-$records_perpage))
						$Page	=	$Page-1;
					$TotalPages		=	ceil($TotalRecords/$records_perpage);
					$Start			=	($Page-1)*$records_perpage;
					
					//code for paging ends
					$getDemoQry.=" limit $Start,$records_perpage";
					//echo$getDemoQry;
					$getDemoRes		=	$DBCONN->prepare($getDemoQry);
					$getDemoRes->execute();
					$getDemoCnt		=	$getDemoRes->rowCount();
					

					$i=1;
					$j=0;
					$demo_id_array=array();
					$value="";
					foreach($getDemoRes as $getDemoRow){
						$demoid=$getDemoRow["demo_id"];
						$added_date=explode(" ",$getDemoRow["added_date"]);
						$satusLog=$getDemoRow["status"];
						$phone=$getDemoRow["res1_workphone"];
                        
						//$mcount=json_decode($memberCount,true);
						
						$member_count=$mcount[$demoid];
						//print_r($member_count);
						$bgcolor	=($i%2==0)?'#e2e1e1':'white';
					
					?>
					<tr bgcolor="<?php echo $bgcolor;?>">
						<td style="cursor:pointer;" onclick="edit_record('<?php echo $getDemoRow["demo_id"];?>');">
						<?php echo  readmore_view(stripslashes($getDemoRow["business_name"]),"60");?></td>
				       
						<td style="cursor:pointer;" onclick="edit_record('<?php echo $getDemoRow["demo_id"];?>');">
						<?php echo  readmore_view(stripslashes($getDemoRow["first_name"]),"60");?></td>

						<td style="cursor:pointer;" onclick="edit_record('<?php echo $getDemoRow["demo_id"];?>');"><?php echo $phone ?></td>
						
						<td style="cursor:pointer;" onclick="edit_record('<?php echo $getDemoRow["demo_id"];?>');"><?php echo  $member_count ?></td> 

						<!-- <td style="cursor:pointer;" onclick="edit_record('<?php echo $getDemoRow["demo_id"];?>');"><?php echo ($mcount[$demoid]) ? $mcount[$demoid] :0; ?></td> -->
						<!-- <td style="cursor:pointer;" onclick="edit_record('<?php echo $getDemoRow["demo_id"];?>');"><?php echo number_format(rand(400,2000)); ?></td> -->
						
					</tr>
					<?php
					  $i++;
					}
				?>
			    </tbody>
			</table>
			<div class="bottompage" >
			
			</div>
			<div class="bottompage1" id="bottompage2">
			<?php
				if($TotalPages>1)
				{
				?>
				<table style="width:382px;">
					<tr>
						<td align="center">
							<?php
							$FormName="frm_demolist";
							include("paging.php");
							?>
						</td>
					</tr>
					
				<?php
					}
				}
				else{
	
				   echo "<tr bgcolor='#a5a5a5'><td colspan=\"5\" style=\"border-bottom:1px solid grey;\"><center> ".$getLangContent["Masterlist"]["searchemptymsg"]."</center></td></tr>";
				}

				?>
				</table>
			</div>
			</form>
	   </div>
	</div>
</div>
<div style="min-height:10px;">
</div>

<?php
if ($getDemoCnt <= 15 ) { ?>
<div id="footer">
	<?php include("includes/lorealfooter.php"); ?> 
</div>
<?php
} else { include("includes/lorealfooter.php"); ?> 
<?php } 
function funmembercount($masteridArray,$sortorder){

	//print_r($masteridArray);

	$url = 'http://www.myappybusiness.com/member_count.php';
    // $url = 'http://localhost/myappybusiness/member_count.php';
 	$params = array(
 		'connection_type' => 'myappysalon',
        'master_id' => $masteridArray,
        'sortorder'=> $sortorder
 	);

    //print_r($params);

 	$parm = http_build_query($params);
    
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_POST,1);
    curl_setopt($ch, CURLOPT_POSTFIELDS,$parm);
    curl_setopt($ch, CURLOPT_URL,$url);
    //curl_setopt($ch, CURLOPT_USERAGENT, $user_agent);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
	
	if(curl_exec($ch) === false) {
		$Response = curl_exec($ch);
	} else {
		$Response = curl_exec($ch);
	}
	return $Response;
}
?>

<script>
function edit_record(val){
  document.location.href="survey_question_dashboard.php?demo_id="+val;
}
/****function for paging statrs*******/
function pagetransfer(pagenumber,formname)
{	
	with(document.forms[formname])
	{ 
		HdnPage.value=pagenumber;
		HiddenMode.value="paging";
		submit();
	}
}
/****function for paging ends*******/
 
</script>
<script>
$(".search").change(function() {
	var txtbox_value=$(".search").val();
	if(txtbox_value!=""){
       $("#frm_search").submit();
    } else {
    	$("#frm_search").submit();
    }
});

$(".input_img").click(function() {
    var reset=$(this).attr("data_id");
    var sort="<?php echo $sort ?>";
    var field="<?php echo $field ?>";
    
    if(reset!="") {
        $("#search").val("");
    	document.location.href="loreal_masterlist.php?sort="+sort+"&field="+field;
		//$("#frm_search").submit();
	}
});
</script>
