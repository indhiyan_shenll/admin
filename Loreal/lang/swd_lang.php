<?php
function funLangContent($postedcuntrylang)
{
	//Main Array start Here
	$retrunCountryLang=array(
	    //Swedish Array start Here
	    "SEK"  => array(
		    //Login Array start Here
		    "Login" =>array(
		    	"loginhead" =>"Team Logga in" , 
		    	"userplaceholder" =>"Användarnamn", 
		    	"passplaceholder" =>"Lösenord" ,
		    	"uservalid" =>"Ange användarnamn" , 
		    	"passvalid" =>"Ange lösenord tack" , 
		    	"invaliderror" =>"Ogiltigt användarnamn eller lösenord" , 
		    	"mailsendmsg" =>"Ditt lösenord har skickats till din e-postadress. Kontrollera din inkorg nu." , 
		    	"linkforgotpass" =>"Glömt ditt lösenord?" , 
		    	"btnlogin" =>"Logga in" 
		    ),
            //Login Array end Here
             
			//Forgot Password Array Start Here
		    "Forgot" =>array(
		    	"forgothead"=>"Glömt ditt lösenord",
		    	"emailplaceholder"=>"E-postadress",
		    	"emailvalid" =>"Ange e-postadressen" ,
		    	"validemailaddress" =>"Ange giltig e-postadress" , 
		    	"invalidemail" =>"Ogiltig email" , 
		    	"linkbacklogin" =>"Tillbaka till login" ,  
				"btnforgot" =>"Glömt ditt lösenord" 
		    ),
			//Forgot Password Array end Here

			//Masterlist Array Start Here
		    "Masterlist"=>array(
		     	"searchlabel"=>"Sök:",	
		     	"businesshead"=>"FÖRETAGSNAMN",	
		     	"contacthead"=>"KONTAKT PERSON",	
		     	"phonehead"=>"TELEFON",	
		     	"appmemberhead"=>"APP MEDLEMMAR",	
		     	"searchemptymsg"=>"Tyvärr finns ingen matchar din sökning."
		    ),
			//Masterlist Array end Here

			//Dashboard Array Start Here
		    "Dashboard"=>array(
		    	"reportingperiod"=>"Rapporterings perioden:", 
		    	"alldates"=>"alla datum",
		    	"daterange"=>"Datumintervall",
		    	"start"=>"Start",
		    	"end"=>"Slutet",
		    	"back"=>"TILLBAKA", 
		    	"apply"=>"TILLÄMPA", 
		    	"responseselect"=>"Enkätsvar VALD", 
		    	"btnrawresponse"=>"Raw Response Data",
		    	"btnemailanalysis"=>"E-post analys rapport",
		    	"emailsentmsg"=>"Rapport har skickats framgångsrikt",
		    	"emailcompilemsg"=>"Vänta ... din rapporten sammanställer ...",
		    	"checkout"=>"CHECKAUT", 
		    	"checkoutpleasant"=>"Trevliga",
		    	"checkoutvisit"=>"besök?",
		    	"checkoutcome"=>"Kom",
		    	"checkoutback"=>"tillbaka?",
		    	"skyltfonster"=>"SKYLTFONSTER",
		    	"skyltfonsterwindow"=>"Fönster",
		    	"skyltfonstereffectiveness"=>"effektivitet?",
		    	"reception"=>"RECEPTION",
		    	"receptionnice"=>"Trevlig",
		    	"receptiongreeting"=>"hälsning?", 
		    	"konsultation"=>"KONSULTATION / DIAGNOS", 
		    	"konsultationhair"=>"Hår", 
		    	"konsultationconsultation"=>"samråd?",
		    	"konsultationlisten"=>"Lyssna",
		    	"konsultationrespect"=>"och respektera?", 
		    	"teknisk"=>"TEKNISK APPLICERING / SCHAMPONERING",
		    	"tekniskapplication"=>"Tillämpning",
		    	"tekniskcolor"=>"av färg?",
		    	"tekniskbackwash"=>"Backwash", 
		    	"tekniskexpenience"=>"erfarenhet?",
		    	"tekniskrecommend"=>"Rekommenderar",
		    	"teknisktreatment"=>"behandling?",
		    	"klipp"=>"KLIPP / STYLING",
		    	"klippmention"=>"Nämn", 
		    	"klippproduct"=>"produktnamn?", 
		    	"klipphair"=>"Hår", 
		    	"klippstyling"=>"styling?", 
		    	"forsaljningsyta"=>"FORSALJNINGSYTA", 
		    	"forsaljningsytarecommend"=>"Rekommenderar", 
		    	"forsaljningsytahome"=>"hem produkter?" 
			),   
			//Dashboard Array end Here

            //Surveychart Array Start Here
		    "Surveychart"=>array(
		     	"backtolist"=>"Tillbaka till listan", 
		     	"backtodashboard"=>"Tillbaka till instrumentpanelen", 
		     	"responsecount"=>"Antal svar:", 
		     	"nextquestion"=>"Nästa fråga", 
		     	"goodbenchmark"=>"bra riktmärke", 
		     	"satisfactorybenchmark"=>"tillfredsställande riktmärke", 
		     	"previousquestion"=>"föregående fråga", 
		     	"quest1"=>"Hur trevligt VAR ditt besök på denna salong? (1-5 skala):", 
		     	"quest2"=>"Hur mycket Salon FÖNSTER gör du vill komma i salongen? (1-5 skala):", 
		     	"quest3"=>"Hur skulle du gradera hur ni möttes? (1-5 skala):", 
		     	"quest4"=>"GJORDE frisören UTFÖR samråd med håret och diskutera dina HÅR tillstånd och behov? (JA / Kanske / Nej):",
		     	"quest5"=>"Känner du att frisören lyssnade på och respekterade håret ambitioner? (JA / Kanske / Nej):", 
		     	"quest6"=>"Hur skulle du gradera erfarenheterna av tillämpningen hårfärg på ditt hår? (1-5 skala):", 
		     	"quest7"=>"Hur skulle du bedöma erfarenheterna i backspolnings ? (1-5 skala):",
		     	"quest8"=>"GJORDE frisören REKOMMENDERAR En särskild behandling för håret behov? (JA / Kanske / Nej):", 
		     	"quest9"=>"Hur väl frisören nämna namnet eller funktion av alla produkter HE / hon använde? (JA / Kanske / Nej):", 
		     	"quest10"=>"GJORDE frisören rekommenderar minst en produkt för hemmabruk ? (JA / Kanske / Nej):",
		     	"quest11"=>"Hur nöjd är du med håret RESULTAT? (1-5 skala):", 
		     	"quest12"=>"Utifrån detta besök, skulle du komma tillbaka till denna salong? (JA / Kanske / Nej):", 
		     	"yes"=>"Ja",
		     	"no"=>"Nej",
		     	"maybe"=>"Kanske"
		    ),
			//Surveychart Array end Here

			//Rawresponsedata Array Start Here
		    "Rawresponsedata"=>array(
		    	"rawhead"=>"RAW RESPONSE DATA", 
		     	"rawcount"=>"Antal svar:", 
		     	"timestamp"=>"TIDSSTÄMPEL", 
		     	"pleasent"=>"BEHAGLIG", 
		     	"window"=>"FÖNSTER", 
		     	"greeting"=>"HÄLSNING",
		     	"consulting"=>"KONSULTERANDE",
		     	"listened"=>"LYSSNADE",
		     	"color"=>"FÄRG",
		     	"treatement"=>"BEHANDLING",
		     	"backwash"=>"SVALLVÅG",
		     	"satisfied"=>"NÖJD",
		     	"home"=>"HEM",
		     	"result"=>"RESULTAT",
		     	"comeback"=>"KOMTILLBAKA",
		     	"back"=>"TILLBAKA",
		     	"nofound"=>"Inget svar (ar)"
		    ),
			//Rawresponsedata Array end Here
		), 
        //Swedish Array start Here
    );
    return $retrunCountryLang[$postedcuntrylang];
}
function funLangMailContent($selLang,$first_name,$username,$password,$loginpath,$LogoUrl){
	$retrunMailCountryLang=array(
	    //SEK Array start Here
	    "SEK"  => array(
	       //ForgotMail Array Start Here
		    "ForgotMail"=>array(
		    	"fromaddress"=>"From: Min Appy Business <server@myappybusiness.com>", 
		     	"emailsubject"=>"Din salong KÄNSLOR inloggningsuppgifter", 
		     	"message"=>'<body style="margin: 0; padding: 0;"><table border="0" align="left" cellpadding="0" cellspacing="0" width="600px" style="font-size:12px;font-family:arial;">
				    <tr>
				        <td>Hej '.$first_name.',</td>
				    </tr>
				    <tr style="height:10px"><td></td></tr>
				    <tr>
				        <td>Du verkar ha glömt dina inloggningsuppgifter för L\'Oréal "Salon känslor" Consulting Tool.</td>
				    </tr>
				    <tr style="height:10px"><td></td></tr>
				    <tr style="height:10px"><td></td></tr>
				    
				    <tr style="height:10px"><td>Din inloggningsinformation finns nedan:</td></tr>
				    <tr style="height:20px"><td></td></tr>
				    <tr style="height:10px"><td>Användarnamn: '.$username.'</td></tr>
				    <tr style="height:10px"><td>Lösenord: '.$password.'</td></tr>
				    <tr style="height:10px"><td>Logga in här:</td></tr>
				    <tr style="height:10px"><td><a href='.$loginpath.'>'.$loginpath.'</a></td></tr>
				    <tr style="height:20px"><td></td></tr>
				    <tr style="height:20px"><td></td></tr>
					<tr><td>Hälsningar,</tr>
					<tr style="height:10px"><td></td></tr>
					<tr><td><img src="'.$LogoUrl.'"></td></tr>
					<tr style="height:15px"><td></td></tr>
					<tr>
						<td style="font-size:9px;font-family:arial;">Den här e-post och bilagor är konfidentiella och kan innehålla juridiskt privilegierade och / eller upphovsrättsinformation Min Appy Business eller tredje part. Om du inte är en auktoriserad mottagare av detta e-post, kontakta avsändaren omedelbart och inte ut, återutsända eller lagra e-post eller eventuella bilagor. Någon förlust / skada som uppstått genom att använda detta material är inte avsändaren\'s ansvar. Ingen garanti ges att denna e-post eller eventuella bilagor är helt fria från datavirus eller andra defekter.</td>
					</tr>
				    </table>
				    </body>'
		    ),
		),
	);
	return $retrunMailCountryLang[$selLang];
	//ForgotMail Array end Here
}
function funpdfMailCoutry($sessionlang,$salonOwnerFirstname,$affiliatename,$mobilenumber){
	$retrunMailPdfLang=array(
	    //English Array start Here
	    // <tr style="height:10px"><td>'.$mobilenumber.'</td></tr>
	    "SEK"  => array(
	       //ForgotMail Array Start Here
		"PdfMailContent"=>array(
			     	"emailsubject"=>"Salon eMotions Rapportera", 
			     	"message"=>'
			    <body style="margin: 0; padding: 0;">
			    <table border="0" align="left" cellpadding="0" cellspacing="0" width="600px" style="font-size:12px;font-family:arial;">
			    <tr>
			        <td>Hej '.htmlentities($salonOwnerFirstname[0]).',</td>
			    </tr>
			    <tr style="height:10px"><td></td></tr>
			    <tr>
			        <td>Som diskuterats bifogas din senaste "Salon eMotions" Rapportera.</td>
			    </tr>
			    <tr>
			        <td>Hör av dig om du behöver något annat.</td>
			    </tr>
			    <tr style="height:10px"><td></td></tr>
			    <tr><td>Vänliga hälsningar,</td></tr>
			    <tr style="height:10px"><td></td></tr>
			    <tr style="height:10px"><td></td></tr>
			    <tr style="height:10px"><td></td></tr>
			    <tr style="height:10px"><td>'.htmlentities($affiliatename).'</td></tr>
			    <tr style="height:10px"><td>L\'Oréal Sverige AB</td></tr>
			    <tr style="height:15px"><td></td></tr>
			    <tr>
			        <td style="font-size:9px;font-family:arial;">Detta meddelande och eventuella bilagor är konfidentiell och enbart avsedda för mottagarna. Om du får detta meddelande av misstag, vänligen ta bort det och omedelbart underrätta avsändaren. Om läsaren av detta meddelande inte är den avsedda mottagaren, är du härmed att all obehörig användning, kopiering eller spridning är förbjuden. E-post är känsliga för ändringar. Varken Loreal eller något av dess dotterbolag eller närstående ska vara ansvarig för meddelandet om ändrats, eller förfalskade.
			        </td>
			    </tr>
			    </table>
			    </body>'
	        ),
	    ),
	);
  return $retrunMailPdfLang[$sessionlang];
}
?>