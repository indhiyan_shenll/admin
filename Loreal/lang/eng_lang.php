<?php
function funLangContent($postedcuntrylang)
{
	//Main Array start Here
	$retrunCountryLang=array(
	  //English Array start Here
	    "English"  => array(
		    //Login Array start Here
		    "Login" =>array(
		    	"loginhead" =>"Team Login" , 
		    	"userplaceholder" =>"Username", 
		    	"passplaceholder" =>"Password" ,
		    	"uservalid" =>"Please enter user name" , 
		    	"passvalid" =>"Please enter password" , 
		    	"invaliderror" =>"Invalid user name or password " , 
		    	"mailsendmsg" =>"Your password has been sent to your registered email address. Please check your inbox now." , 
		    	"linkforgotpass" =>"Forgot Password?" , 
		    	"btnlogin" =>"Login" 
		    ),
            //Login Array end Here
             
			//Forgot Password Array Start Here
		    "Forgot" =>array(
		    	"forgothead"=>"Forgot Password",
		    	"emailplaceholder"=>"Email Address",
		    	"emailvalid" =>"Please enter email address" ,
		    	"validemailaddress" =>"Please enter valid email addresss" , 
		    	"invalidemail" =>"Invalid email" , 
		    	"linkbacklogin" =>"Back to Login" ,  
				"btnforgot" =>"Forgot Password" 
		    ),
			//Forgot Password Array end Here

			//Masterlist Array Start Here
		    "Masterlist"=>array(
		     	"searchlabel"=>"Search:",	
		     	"businesshead"=>"BUSINESS NAME",	
		     	"contacthead"=>"CONTACT PERSON",	
		     	"phonehead"=>"PHONE",	
		     	"appmemberhead"=>"APP MEMBERS",	
		     	"searchemptymsg"=>"Sorry no matches for your search."
		    ),
			//Masterlist Array end Here

			//Dashboard Array Start Here
		    "Dashboard"=>array(
		    	"reportingperiod"=>"REPORTING PERIOD:", 
		    	"alldates"=>"All dates",
		    	"daterange"=>"Date range",
		    	"start"=>"Start",
		    	"end"=>"End",
		    	"back"=>"BACK", 
		    	"apply"=>"APPLY", 
		    	"responseselect"=>"SURVEY RESPONSES SELECTED", 
		    	"btnrawresponse"=>"Raw Response Data",
		    	"btnemailanalysis"=>"Email analysis report",
		    	"emailsentmsg"=>"Report has been emailed successfully",
		    	"emailcompilemsg"=>"Please wait...your Analysis Report is compiling...", 
		    	"checkout"=>"CHECKOUT", 
		    	"checkoutpleasant"=>"Pleasant",
		    	"checkoutvisit"=>"visit?",
		    	"checkoutcome"=>"Come",
		    	"checkoutback"=>"back?",
		    	"skyltfonster"=>"SKYLTFONSTER",
		    	"skyltfonsterwindow"=>"Window",
		    	"skyltfonstereffectiveness"=>"effectiveness?",
		    	"reception"=>"RECEPTION",
		    	"receptionnice"=>"Nice",
		    	"receptiongreeting"=>"greeting?", 
		    	"konsultation"=>"KONSULTATION / DIAGNOS", 
		    	"konsultationhair"=>"Hair", 
		    	"konsultationconsultation"=>"consultation?",
		    	"konsultationlisten"=>"Listen",
		    	"konsultationrespect"=>"and respect?", 
		    	"teknisk"=>"TEKNISK APPLICERING / SHAMPONERING",
		    	"tekniskapplication"=>"Application",
		    	"tekniskcolor"=>"of color?",
		    	"tekniskbackwash"=>"Backwash", 
		    	"tekniskexpenience"=>"expenience?",
		    	"tekniskrecommend"=>"Recommend",
		    	"teknisktreatment"=>"treatment?",
		    	"klipp"=>"KLIPP / STYLING",
		    	"klippmention"=>"Mention", 
		    	"klippproduct"=>"product names?", 
		    	"klipphair"=>"Hair", 
		    	"klippstyling"=>"styling?", 
		    	"forsaljningsyta"=>"FORSALJNINGSYTA", 
		    	"forsaljningsytarecommend"=>"Recommend", 
		    	"forsaljningsytahome"=>"home products?" 
			),   
			//Dashboard Array end Here

            //Surveychart Array Start Here
		     "Surveychart"=>array(
		     	"backtolist"=>"Back to List", 
		     	"backtodashboard"=>"Back to Dashboard", 
		     	"responsecount"=>"Number of responses:", 
		     	"nextquestion"=>"Next question", 
		     	"goodbenchmark"=>"Good benchmark", 
		     	"satisfactorybenchmark"=>"Satisfactory benchmark", 
		     	"previousquestion"=>"Previous question", 
		     	"quest1"=>"HOW PLEASANT WAS YOUR VISIT TO THIS SALON? (1-5 SCALE):", 
		     	"quest2"=>"HOW MUCH DID THE SALON WINDOW MAKE YOU WANT TO COME INTO THE SALON? (1-5 SCALE):", 
		     	"quest3"=>"HOW WOULD YOU RATE THE WAY IN WHICH YOU WERE GREETED? (1-5 SCALE):", 
		     	"quest4"=>"DID THE HAIRDRESSER PERFORM A CONSULTATION OF YOUR HAIR AND DISCUSS YOUR HAIR CONDITION AND NEEDS? (YES/MAYBE/NO):",
		     	"quest5"=>"DO YOU FEEL THAT THE HAIRDRESSER LISTENED TO AND RESPECTED YOUR HAIR ASPIRATIONS? (YES/MAYBE/NO):", 
		     	"quest6"=>"HOW WOULD YOU RATE THE EXPERIENCE OF THE APPLICATION OF HAIR COLOR ONTO YOUR HAIR? (1-5 SCALE):", 
		     	"quest7"=>"HOW WOULD YOU RATE THE EXPERIENCE IN THE BACKWASH? (1-5 SCALE):",
		     	"quest8"=>"DID THE HAIRDRESSER RECOMMEND A SPECIFIC TREATMENT FOR YOUR HAIR NEEDS? (YES/MAYBE/NO):", 
		     	"quest9"=>"HOW WELL DID THE HAIRDRESSER MENTION THE NAME OR THE FUNCTION OF ALL THE PRODUCTS HE/SHE USED? (YES/MAYBE/NO):", 
		     	"quest10"=>"DID THE HAIRDRESSER RECOMMEND AT LEAST ONE PRODUCT FOR HOME USE? (YES/MAYBE/NO):",
		     	"quest11"=>"HOW SATISFIED ARE YOU WITH THE HAIR RESULT? (1-5 SCALE):", 
		     	"quest12"=>"BASED ON THIS VISIT, WOULD YOU COME BACK TO THIS SALON? (YES/MAYBE/NO):", 
		     	"yes"=>"Yes",
		     	"no"=>"No",
		     	"maybe"=>"Maybe"
		    ),
			//Surveychart Array end Here

			 //Rawresponsedata Array Start Here
		    "Rawresponsedata"=>array(
		    	"rawhead"=>"RAW RESPONSE DATA", 
		     	"rawcount"=>"Number of responses:", 
		     	"timestamp"=>"TIMESTAMP", 
		     	"pleasent"=>"PLEASENT", 
		     	"window"=>"WINDOW", 
		     	"greeting"=>"GREETING",
		     	"consulting"=>"CONSULTING",
		     	"listened"=>"LISTENED",
		     	"color"=>"COLOR",
		     	"treatement"=>"TREATEMENT",
		     	"backwash"=>"BACKWASH",
		     	"satisfied"=>"SATISFIED",
		     	"home"=>"HOME",
		     	"result"=>"RESULT",
		     	"comeback"=>"COMEBACK",
		     	"back"=>"Back",
		     	"nofound"=>"No Response(s) found."
		    ),
			//Rawresponsedata Array end Here

			
		),
    );
   return $retrunCountryLang[$postedcuntrylang];
}
function funLangMailContent($selLang,$first_name,$username,$password,$loginpath,$LogoUrl){
	$retrunMailCountryLang=array(
	    //English Array start Here
	    "English"  => array(
	       //ForgotMail Array Start Here
		    "ForgotMail"=>array(
		    	"fromaddress"=>"From: My Appy Business <server@myappybusiness.com>", 
		     	"emailsubject"=>"Your Salon eMotions login details", 
		     	"message"=>'<body style="margin: 0; padding: 0;"><table border="0" align="left" cellpadding="0" cellspacing="0" width="600px" style="font-size:12px;font-family:arial;">
				    <tr>
				        <td>Hi '.$first_name.',</td>
				    </tr>
				    <tr style="height:10px"><td></td></tr>
				    <tr>
				        <td>You seem to have forgotten your login information for L\'Oréal “Salon eMotions” Consulting Tool.</td>
				    </tr>
				    <tr style="height:10px"><td></td></tr>
				    <tr style="height:10px"><td></td></tr>
				    
				    <tr style="height:10px"><td>Your login information is provided below:</td></tr>
				    <tr style="height:20px"><td></td></tr>
				    <tr style="height:10px"><td>Username: '.$username.'</td></tr>
				    <tr style="height:10px"><td>Password: '.$password.'</td></tr>
				    <tr style="height:10px"><td>Login here:</td></tr>
				    <tr style="height:10px"><td><a href='.$loginpath.'>'.$loginpath.'</a></td></tr>
				    <tr style="height:20px"><td></td></tr>
				    <tr style="height:20px"><td></td></tr>
					<tr><td>With Regards,</tr>
					<tr style="height:10px"><td></td></tr>
					<tr><td><img src="'.$LogoUrl.'"></td></tr>
					<tr style="height:15px"><td></td></tr>
					<tr>
						<td style="font-size:9px;font-family:arial;">This e-mail and any attachments are confidential and may contain legally privileged and/or copyright information of My Appy Business or third parties. If you are not an authorised recipient of this e-mail, please contact the sender immediately and do not print, re-transmit or store this email or any attachments. Any loss/damage incurred by using this material is not the sender\'s responsibility. No warranty is given that this e-mail or any attachments are totally free from computer viruses or other defects.</td>
					</tr>
				    </table>
				    </body>'
		    ),

		),
	);
	return $retrunMailCountryLang[$selLang];
	//ForgotMail Array end Here
}

function funpdfMailCoutry($sessionlang,$salonOwnerFirstname,$affiliatename,$mobilenumber){
	$retrunMailPdfLang=array(
	    //English Array start Here 
	    // <tr style="height:10px"><td>'.$mobilenumber.'</td></tr>
	    "English"  => array(
	       //ForgotMail Array Start Here
		"PdfMailContent"=>array(
			     	"emailsubject"=>"Salon eMotions Report", 
			     	"message"=>'
			    <body style="margin: 0; padding: 0;">
			    <table border="0" align="left" cellpadding="0" cellspacing="0" width="600px" style="font-size:12px;font-family:arial;">
			    <tr>
			        <td>Hi '.htmlentities($salonOwnerFirstname[0]).',</td>
			    </tr>
			    <tr style="height:10px"><td></td></tr>
			    <tr>
			        <td>As discussed, please find attached your latest "Salon eMotions" Report.</td>
			    </tr>
			    <tr>
			        <td>Please let me know if you need anything else.</td>
			    </tr>
			    <tr style="height:10px"><td></td></tr>
			    <tr><td>Best Regards,</td></tr> 
			    <tr style="height:10px"><td></td></tr>
			    <tr style="height:10px"><td></td></tr>
			    <tr style="height:10px"><td></td></tr>
			    <tr style="height:10px"><td>'.htmlentities($affiliatename).'</td></tr>
			    <tr style="height:10px"><td>L\'Oréal Sverige AB</td></tr>
			    <tr style="height:15px"><td></td></tr>
			    <tr>
			        <td style="font-size:9px;font-family:arial;">This message and any attachments are confidential and intended solely for the addressees. If you receive this message in error, please delete it and immediately notify the sender. If the reader of this message is not the intended recipient, you are hereby notified that any unauthorized use, copying or dissemination is prohibited. E-mails are susceptible to alteration. Neither LOREAL nor any of its subsidiaries or affiliates shall be liable for the message if altered, changed or falsified.
			        </td>
			    </tr>
			    </table>
			    </body>'
	        ),
	    ),
	);
  return $retrunMailPdfLang[$sessionlang];
}
?>

