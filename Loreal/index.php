<?php

	$redirectUrl = "";
    if(!isset($_SESSION['user_id']) || trim($_SESSION['user_id'])=="")
	{	
	 	$redirectUrl = ($_SERVER['SERVER_NAME'] == "localhost") ? "Location:http://localhost/myappyrestaurant/admin/Loreal/loreal_masterlist.php" : "Location:http://myappybusiness.com/admin/loreal/loreal_masterlist.php" ;
	 	header($redirectUrl);
	 	exit;
	} else {
		$redirectUrl = ($_SERVER['SERVER_NAME'] == "localhost") ? "Location:http://localhost/myappyrestaurant/admin/Loreal/login.php" : "Location:http://myappybusiness.com/admin/loreal/login.php" ;
		header($redirectUrl);
	  	exit;
	}
?>

