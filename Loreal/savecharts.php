<?php

include_once ("../../includes/configure.php");
include_once ("../includes/loreal_sessioncheck.php"); 
error_reporting(E_ALL);

if (isset($_POST["bin_data"])) {

	$binDataArr = json_decode($_POST['bin_data'], true);
	$imagenameArr = json_decode($_POST['image_name'], true);
	$demo_id = $_SESSION['demo_id'];
	$user_id = $_SESSION['user_id'];
	$sessionlang = $_POST['sessionlang'];
	//$_SESSION["selectlang"]=$selectlang;
	//print_r($_SESSION["selectlang"]); exit;

	

	for ($i= 0; $i<count($binDataArr);$i++) {
		
		// $data = str_replace(' ', '+', $_POST['bin_data'][$i]);
		// $imagename = stripcslashes($_POST['image_name'][$i]);
		$data = str_replace(' ', '+', $binDataArr[$i]);
		$imagename = stripcslashes($imagenameArr[$i]);

		// $chart_number = $_POST['chart_number'];

		$data = base64_decode($data);
		//$fileName = date('ymdhis').'.png';
		$ChartImageName  = $imagename.'.png';
		$im = imagecreatefromstring($data);
		
		if ($im !== false) {

		    // Save image in the specified location
		    // imagepng($im, $lorealPath."/salon_eMotion_report/".$ChartImageName);
		    imagepng($im, "salon_eMotion_report/".$ChartImageName);
		    imagedestroy($im);
		    
		    $chartImgName  = $demo_id.'_'.$user_id;
		    if ($i == 11) {		    	
		    	include('createchartpdf.php');
		    }		    
		    $response  = "success";
		    
		} else {
		 	$response  = "failed";
		}
	}
	echo $response;
	exit;
}

if (isset($_POST["htmltoimage"])) {

	$data = $_REQUEST['htmltoimage'];
	$response = "";
	if (!empty($data)) {
		$rawImage = $data;
		$removeheaders =substr($rawImage,strpos($rawImage,",")+1);
		$decode = base64_decode($removeheaders);
		// $fopen = fopen($lorealPath.'/salon_eMotion_report/summary_dashboard.png','wb');
		$fopen = fopen('salon_eMotion_report/summary_dashboard.png','wb');
		$test = fwrite($fopen, $decode);
		$response["status"] = "success";
	}
	echo json_encode($response);
	exit;

}


?>