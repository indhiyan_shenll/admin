$( document ).ready(function() {
    $('#datetimepicker6').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true
    })
    $('#datetimepicker7').datepicker({
        format: 'dd/mm/yyyy',
        // endDate: '+0d',
        autoclose: true
        // useCurrent: false //Important! See issue #1075
    });

});



var formDate = $("input[name='start_date']").val();
var toDate = $("input[name='end_date']").val();
$(document).on("click","#all_dates",function() {
    
    formDate = $("input[name='start_date']").val();
    toDate = $("input[name='end_date']").val();

    if ($(this).is(':checked')) {
        $("#datetimepicker6 input[name=start_date]").val("");
        $("#datetimepicker7 input[name=end_date]").val("");
    } 
    
});

$(document).on("click","#date_range,#all_dates",function() {
    //if ($(this).is(':checked')) {
       $("#datetimepicker6").datepicker("setDate",'today');
       $("#datetimepicker7").datepicker("setDate",'today');
       $("#datetimepicker6 input[name=start_date]").val("");
       $("#datetimepicker7 input[name=end_date]").val("");
       $("#datetimepicker6 input[name=start_date]").attr("value", '');
       $("#datetimepicker7 input[name=end_date]").attr("value", '');
       
    //} 
});

$(document).on("click","#datetimepicker6, #datetimepicker7",function() {

    var reportingPeriod = $("input[name=reporting_period]:checked").val();
    if (reportingPeriod == "all dates") {
        $("#date_range").prop("checked", true);
        $("#all_dates").prop("checked", false);
    } 
});

$(document).on("click","#applybtn",function() {

    var formValues = $( "#survey-filter" ).serialize();
    var startDate = $("input[name='start_date']").val();
    var endDate = $("input[name='end_date']").val();
    var reportingPeriod = $("input[name=reporting_period]:checked").val();        

    $("#survey-response-count").css("display", "none");
    $("#survey-loading").css("display", "inline");
    
    $.ajax({
        method: "POST",
        url: "ajax_survey.php",
        data: {post_type: 'surveyrescount', formValues: formValues, demo_id: $("#demo_id").val()},
        error: function(){
        
        },
        success: function(response) {
            var surveyRes = jQuery.parseJSON( response );
            
            $("#survey-loading").css("display", "none");
            $("#survey-response-count").css("display", "inline");
            $("#survey-response-count").text(surveyRes['survey_count']);
            $("#survey-response-count-hidden").text(surveyRes['survey_count']);

            // $("#qstn_1").css("background-color", surveyRes.background_color['qstn1_ans']);
            if (surveyRes.background_color != '') {
                for ($k = 1; $k <= 12; $k++) {
                    $("#qstn_"+$k).attr("style", surveyRes.background_color['qstn'+$k+"_ans"]);
                }
            } else {
                $("div[id^='qstn_']").attr("style", "background-color: #F0A48C;");
            }
            

        },      
    })
});

$(document).on("click","#close",function() {
    $("#successalert").css("visibility", "hidden");
});


function customlineChart (i, xaxisValues, yaxisValues, green, amber) {
   
    var chart = Highcharts.chart('ajaxchartsvg_'+i, {
        chart: {
            zoomType: 'xy',
            height: 400,
            plotBorderColor: '#000',
            plotBorderWidth: 2,
        },
        title: {
            text: '',
            x: -20 //center
        },

        credits: {
            enabled: false,
        },
        colors: ['#000000'],
        xAxis: [{
            categories: xaxisValues,
            crosshair: true,
        }],
        
        yAxis: [{ // Primary yAxis
            min: 0,  
            max: 5,            
            tickInterval: 1,
            labels: {
                format:'{value}',
            },
            plotLines: [{               
                value: green,                
                color: '#6dcf95',                
                width: 2,                
                label: {                    
                    text: ''                
                }            
            }, {                
                value: amber,                
                color: '#ffcf44',               
                 width: 2,                
                 label: {                    
                    text: ''                
                }           
             }],
            title: {
                text: '',
                style: {
                    color: Highcharts.getOptions().colors[1],
                }
            },
        },{ // Secondary yAxis
            title: {
                text: '',
                style: {
                    color: Highcharts.getOptions().colors[1],
                }
            },
        }],
        plotOptions: {
            line: {
                // marker: {
                //     enabled: true
                // }
                marker: {
                    enabled: true,
                    symbol: 'circle',
                    radius: 2,
                    states: {
                        hover: {
                            enabled: true
                        }
                    }
                }
            }
        },
        series: [{
            name: 'Avg',
            type: '',
            data: yaxisValues,
        },],

    });
    return chart;
}

function custombarChart (i, xaxisValuesYes, yaxisValueYes, yaxisValueNo, yaxisValueMaybe,yes,no,maybe) {
    
    if (xaxisValuesYes == "[1]")
        xaxisValuesYes = ['1'];

    var chart = Highcharts.chart('ajaxchartsvg_'+i, {
        chart: {
            type: 'column',
            plotBorderColor: '#000',
            plotBorderWidth: 2,
        },
        title: {
            text: '',
            x: -20 //center
        },
        credits: {
            enabled: false
        },
        xAxis: {                                
            categories: xaxisValuesYes
        },
        yAxis: {
            min: 0,
            max: 110,
            labels:{enabled: false},
            tickInterval: 1,
            title: {
                text: ''
            },
            stackLabels: {
                enabled: true,
                style: {
                    fontWeight: 'bold',
                    color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                },
                formatter: function () {
                    return (this.axis.series[0].yData[this.x]) + '%';
                }
            }
        },
        tooltip: {
            pointFormat: '{series.name}: {point.y}<br/>Total: {point.stackTotal}'
        },
        plotOptions: {
            column: {
                stacking: 'normal',
            }
        },
        colors: [
            '#00b050',
            '#ffc000',
            '#ff0000'
        ],
        series: [{
            name: yes,
            data: yaxisValueYes
        }, {
            name: no,
            data: yaxisValueNo
        }, {
            name: maybe,
            data: yaxisValueMaybe
        }],
    });
    return chart;
}