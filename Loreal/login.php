<?php
include("../../includes/configure.php");
include("includes/cmm_functions.php");
if(isset($_GET["msg"])=="1"){
	$tblmsg="Your password has been sent to your registered email address. Please check your inbox now.";
} else{
	$tblmsg="";
}


$ErrMsg="";
if(isset($_POST['Username'])){
	$UserName=trim($_POST['Username']);
	$Password=trim($_POST['Password']);
	$getQry="select * from tbl_users where username=:username and password=:password";
	$get_user=$DBCONN->prepare($getQry);
	$res=$get_user->execute(array(':username'=>$UserName,':password'=>$Password));
	$count=$get_user->rowCount();
	if ($count>0){
		$get_row=$get_user->fetchAll();
		$row=$get_row[0];
		// $_SESSION['user_id']=$row['user_id'];
		$user_id = $row['user_id'];
		$_SESSION['user_name']= stripslashes($row['username']);
		$userType=$row['user_type'];
		$aff_type=$row['affiliate_type'];
		$Arraffilatetype=array("2"=>"Non Loreal","1"=>"Loreal Hair","3"=>"Loreal Beauty");
		$AffilateType=$Arraffilatetype[$aff_type];
		$_SESSION['user_type']=$row['user_type'];
		$_SESSION['affiliate_type']=$AffilateType;

		if ($user_id !="" && $userType=="Affiliate" && $AffilateType=="Loreal Hair") {
        
        	// echo "select * from tbl_affiliate where a_user_id='$user_id'";
	        $get_Qry = $DBCONN->prepare("select * from tbl_affiliate where a_user_id='$user_id'");
	        $get_Qry->execute();
	        $get_Qry_Cnt = $get_Qry->rowCount();
	        if ($get_Qry_Cnt > 0) {
	            $get_value = $get_Qry->fetch();
	            $TblAffiliateId = $get_value['affiliate_id'];
	            $user_id = $TblAffiliateId;
	        }
	        
	    }
	     // print_r($_SESSION);
	    // exit;

	    $_SESSION['user_id']= $user_id;


	   
		if($userType=='Admin' || $userType=='admin'){
			header('Location:loreal_masterlist.php');
			exit;
		}
		if($userType=='Affiliate' && $AffilateType=="Loreal Hair"){
			header('Location:loreal_masterlist.php');
			exit;
		}
	}
	else{

		$ErrMsg = "Invalid user name or password";
	}
}

?>
<?php
	include_once("includes/lorealheader.php");

?>

<style>
.main_header {
   margin-bottom: 20px;
}
.session_name, .logout{
	display:none;
}
</style>
<div class="container logincontainer">
	<div class="container login">
        <div class="col-md-12 col-sm-12 col-xs-12 emotion"> 
            <img src="images/emotion_logo.png">
        </div>
         <?php

			if($tblmsg!=""){
		?>
        <div class="col-md-8 col-sm-12 col-xs-12 " style="margin:auto;float:none;">
           <div class="alert alert-success">
			 <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		     <?php echo $getLangContent["Login"]["mailsendmsg"]; ?>
		    </div>
        </div>
        <?php
        }
        ?>
		<div class="col-md-12 col-sm-12 col-xs-12 col-lg-12"> 
		   	<div class="col-md-6 col-sm-8 col-xs-12 col-lg-4 loginbox">
			    <form name="frm_lorellogin" id="frm_lorellogin" method="post" action="">
			    	<h1><?php  echo $getLangContent["Login"]["loginhead"];?></h1> 
			    	<?php
						if($ErrMsg!=""){
					?>
							<div class="alert alert-danger">
							 <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
						       <?php echo $getLangContent["Login"]["invaliderror"]; ?>
						    </div>
					<?php		
						}
					?>
			    	<div class="col-md-12 col-sm-12 col-xs-12 ">
			    		<input type="text" placeholder="<?php  echo $getLangContent["Login"]["userplaceholder"];?>" name="Username" required="" id="username" value="">
			    	</div>
			    	<div class="col-md-12 col-sm-12 col-xs-12">
			    		<input type="password" placeholder="<?php  echo $getLangContent["Login"]["passplaceholder"];?>" name="Password" required="" id="password">
			    	</div>
			    	<div class="col-md-12 col-sm-12 col-xs-12 forgot_submit">
			    	   <div class="col-md-6 col-sm-6 col-xs-5 col-lg-6 forgot1">
			    	   		<a href="forgotpassword.php" class="forgot"><i><?php  echo $getLangContent["Login"]["linkforgotpass"];?></i></a>
			    	   </div>
			    	    <div class="col-md-6 col-sm-6 col-xs-7 btnlog">
					   		<input type="submit" value="<?php  echo $getLangContent["Login"]["btnlogin"];?>" class="btnlogin btn btn-default">
					   </div>
					</div> 
			    	<!-- <div class="col-md-12 col-sm-12 col-xs-12 col-lg-12 forgot_submit">
			    	   <a href="forgotpassword.php" class="forgot"><i>Forgot Password?</i></a>
					   <input type="submit" value="LOGIN" class="btn_login btn btn-default">
					</div> -->
					<div class="empty"></div>
			    </form> 
		   </div>
		</div>
    </div>
</div>
<!-- <div style="min-height:128px;">
</div> -->
<div id="footer">
<?php
    include_once("includes/lorealfooter.php");
?>
</div>
<script type="text/javascript" src="js/jquery.validate.js"></script>
<!-- <script type="text/javascript" src="js/custom.js"></script> -->
<script >

$(document).ready(function(){
	 var uservalid="<?php echo $getLangContent['Login']['uservalid']; ?>";
	 var passvalid="<?php echo $getLangContent['Login']['passvalid']; ?>";
	$("#frm_lorellogin").validate({
	    rules: {
			Username: {
				required: true
			},
			Password:"required",
		},
		messages: {
			Username: {
				required: uservalid
			},
			Password: passvalid,
		},
	});	
});


</script>


