<?php
require_once 'google-api-php-client-master/src/Google/autoload.php';
require_once 'vendor/asimlqt/php-google-spreadsheet-client/src/Google/Spreadsheet/ServiceRequestInterface.php';
require_once 'vendor/asimlqt/php-google-spreadsheet-client/src/Google/Spreadsheet/DefaultServiceRequest.php';
require_once 'vendor/asimlqt/php-google-spreadsheet-client/src/Google/Spreadsheet/Exception.php';
require_once 'vendor/asimlqt/php-google-spreadsheet-client/src/Google/Spreadsheet/UnauthorizedException.php';
require_once 'vendor/asimlqt/php-google-spreadsheet-client/src/Google/Spreadsheet/ServiceRequestFactory.php';
require_once 'vendor/asimlqt/php-google-spreadsheet-client/src/Google/Spreadsheet/SpreadsheetService.php';
require_once 'vendor/asimlqt/php-google-spreadsheet-client/src/Google/Spreadsheet/SpreadsheetFeed.php';
require_once 'vendor/asimlqt/php-google-spreadsheet-client/src/Google/Spreadsheet/Spreadsheet.php';
require_once 'vendor/asimlqt/php-google-spreadsheet-client/src/Google/Spreadsheet/WorksheetFeed.php';
require_once 'vendor/asimlqt/php-google-spreadsheet-client/src/Google/Spreadsheet/Worksheet.php';
require_once 'vendor/asimlqt/php-google-spreadsheet-client/src/Google/Spreadsheet/ListFeed.php';
require_once 'vendor/asimlqt/php-google-spreadsheet-client/src/Google/Spreadsheet/ListEntry.php';
require_once 'vendor/asimlqt/php-google-spreadsheet-client/src/Google/Spreadsheet/Batch/BatchRequest.php';
require_once 'vendor/asimlqt/php-google-spreadsheet-client/src/Google/Spreadsheet/Batch/BatchResponse.php';
require_once 'vendor/asimlqt/php-google-spreadsheet-client/src/Google/Spreadsheet/CellFeed.php';
require_once 'vendor/asimlqt/php-google-spreadsheet-client/src/Google/Spreadsheet/CellEntry.php';
require_once 'vendor/asimlqt/php-google-spreadsheet-client/src/Google/Spreadsheet/Util.php';


//class Google
//{
//	var $data;
//
//	function getToken()
//	{
//		$client = new Google_Client();
//		$client->setApplicationName("My Application");
//		$client->setClientId("650025949541-3jgp1lcs3g0j43k05pm8oj4oh3q2vfv6.apps.googleusercontent.com");
//
//		$key = file_get_contents("shenllapi-4eb459ac604d.p12");
//		$cred = new Google_Auth_AssertionCredentials(
//			"650025949541-ldo88lvhe0guq3ou6tbi4re5dvv0gp20@developer.gserviceaccount.com",
//			array("https://spreadsheets.google.com/feeds","https://docs.google.com/feeds"),
//			$key
//		);
//
//		$client->setAssertionCredentials($cred);
//
//		if($client->getAuth()->isAccessTokenExpired()) {
//			$client->getAuth()->refreshTokenWithAssertion($cred);
//		}
//
//		$service_token = json_decode($client->getAccessToken());
//		return $service_token->access_token;
//	}
//
//	function getSpreadsheetsList()
//	{
//		$accessToken        = $this->getToken();
//
//		$serviceRequest     = new DefaultServiceRequest($accessToken);
//		ServiceRequestFactory::setInstance($serviceRequest);
//
//		$spreadsheetService = new SpreadsheetService();
//		$spreadsheetFeed    = $spreadsheetService->getSpreadsheets();
//
//		print_r($spreadsheetFeed);
//
//		foreach($spreadsheetFeed as $item) {
//			$spreadsheets[basename($item->getId())] = $item->getTitle();
//		}
//
//		$this->data->spreadsheets = isset($spreadsheets) ? $spreadsheets : false;
//	}
//}
//
//$obj = new Google();
//$obj->getSpreadsheetsList();

/**
 * AUTHENTICATE
 *
 */
// These settings are found on google developer console
// const CLIENT_APP_NAME = '805 Gsheets';
// const CLIENT_ID       = '370ff9d8a0028b7cb6a3e545e3cb69cfe2b8f0d7';
// const CLIENT_EMAIL    = 'id-05-gsheets@handy-position-134915.iam.gserviceaccount.com';
// const CLIENT_KEY_PATH = '805 Gsheets-370ff9d8a002.p12'; // PATH_TO_KEY = where you keep your key file
// const CLIENT_KEY_PW   = 'notasecret';

// const CLIENT_APP_NAME = 'myappybusinessnew';
// const CLIENT_ID       = '1004426043209-m4h3pf1grmphk37bjtdmqj2qjhak07o2.apps.googleusercontent.com';
// // const CLIENT_ID       = '7252c2736d9ab937406fdb1a83f26b386dd62115';
// const CLIENT_EMAIL    = 'myappybusinessnew@myappybusinessnew.iam.gserviceaccount.com';
// const CLIENT_KEY_PATH = 'myappybusinessnew-0941d72948c0.p12'; // PATH_TO_KEY = where you keep your key file
// const CLIENT_KEY_PW   = 'notasecret';

const CLIENT_APP_NAME = 'myappybusiness';
const CLIENT_ID       = '1017388056215-fvmjvr3kp1h0q3his0qjkuee2p6r4pqo.apps.googleusercontent.com';
// const CLIENT_ID       = '7252c2736d9ab937406fdb1a83f26b386dd62115';
const CLIENT_EMAIL    = 'myappybusiness-service-act@myappybusiness-160015.iam.gserviceaccount.com';
const CLIENT_KEY_PATH = 'myappybusiness-7252c2736d9a.p12'; // PATH_TO_KEY = where you keep your key file
const CLIENT_KEY_PW   = 'notasecret';
 
$objClientAuth  = new Google_Client ();
$objClientAuth -> setApplicationName (CLIENT_APP_NAME);
$objClientAuth -> setClientId (CLIENT_ID);
$objClientAuth -> setAssertionCredentials (new Google_Auth_AssertionCredentials (
    CLIENT_EMAIL, 
    array('https://spreadsheets.google.com/feeds','https://docs.google.com/feeds'), 
    file_get_contents (CLIENT_KEY_PATH), 
    CLIENT_KEY_PW
));
$objClientAuth->getAuth()->refreshTokenWithAssertion();
$objToken  = json_decode($objClientAuth->getAccessToken());
$accessToken = $objToken->access_token;
 
 
/**
 * Initialize the service request factory
 */ 
use Google\Spreadsheet\DefaultServiceRequest;
use Google\Spreadsheet\ServiceRequestFactory;
 
$serviceRequest = new DefaultServiceRequest($accessToken);
ServiceRequestFactory::setInstance($serviceRequest);
 

// $worksheetTitle = "2443";
$sheetflag = true;
/**
 * Get spreadsheet by title
 */
// $spreadsheetTitle = '805 Score';
// $spreadsheetTitle = 'GS-myappybusiness';
$spreadsheetTitle = 'Venkat Survey (Responses)';

$spreadsheetService = new Google\Spreadsheet\SpreadsheetService();
$spreadsheetFeed = $spreadsheetService->getSpreadsheets();
$spreadsheet = $spreadsheetFeed->getByTitle($spreadsheetTitle);
$worksheetFeed = $spreadsheet->getWorksheets();
$worksheet = $worksheetFeed->getByTitle('Salong Noir');
$listFeed = $worksheet->getListFeed();

$questionRes = "";
foreach ($listFeed->getEntries() as $entry) {
	$values = $entry->getValues();
	$questionRes[] = $values;
}
print_r($worksheetFeed["entry"]);
print_r($worksheetFeed);
// print_r($questionRes);
