<?php
/*
 * Copyright 2014 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

//$__autoload = array(
//  "myClass" => "myClass.class.php"
//);
//
//function myAutoload($class) {
//
//  global $__autoload;
//
//  if(isset($__autoload[$class])) {
//    require $__autoload[$class];
//  }
// Autoloader for Classes
//spl_autoload_register(null, false); // Nullify any existing autoloads
//spl_autoload_extensions('.php, .class.php'); // Specify extensions that may be loaded
//
//    function class_loader($className) {
//      $classPath = explode('_', $className);
//      if ($classPath[0] != 'Google') {
//        return;
//      }
//       Drop 'Google', and maximum class file path depth in this project is 3.
//      $classPath = array_slice($classPath, 1, 2);
//
//      $filePath = dirname(__FILE__) . '/' . implode('/', $classPath) . '.php';
//      if (file_exists($filePath)) {
//        require_once($filePath);
//      }
//    }

//    function class_loader($className) {
//      $classPath = explode('_', $className);
//      if ($classPath[0] != 'Google') {
//        return;
//      }
//       Drop 'Google', and maximum class file path depth in this project is 3.
//      $classPath = array_slice($classPath, 1, 2);
//
//      $filePath = dirname(__FILE__) . '/' . implode('/', $classPath) . '.php';
//      if (file_exists($filePath)) {
//        require_once($filePath);
//      }
//    }
function autoload($className) {
      $classPath = explode('_', $className);
      if ($classPath[0] != 'Google') {
        return;
      }
      // Drop 'Google', and maximum class file path depth in this project is 3.
      $classPath = array_slice($classPath, 1, 2);

      $filePath = dirname(__FILE__) . '/' . implode('/', $classPath) . '.php';
      if (file_exists($filePath)) {
        require_once($filePath);
      }
    }
spl_autoload_register('autoload');