<?php
require_once 'google-api-php-client-master/src/Google/autoload.php';
require_once 'vendor/asimlqt/php-google-spreadsheet-client/src/Google/Spreadsheet/ServiceRequestInterface.php';
require_once 'vendor/asimlqt/php-google-spreadsheet-client/src/Google/Spreadsheet/DefaultServiceRequest.php';
require_once 'vendor/asimlqt/php-google-spreadsheet-client/src/Google/Spreadsheet/Exception.php';
require_once 'vendor/asimlqt/php-google-spreadsheet-client/src/Google/Spreadsheet/UnauthorizedException.php';
require_once 'vendor/asimlqt/php-google-spreadsheet-client/src/Google/Spreadsheet/ServiceRequestFactory.php';
require_once 'vendor/asimlqt/php-google-spreadsheet-client/src/Google/Spreadsheet/SpreadsheetService.php';
require_once 'vendor/asimlqt/php-google-spreadsheet-client/src/Google/Spreadsheet/SpreadsheetFeed.php';
require_once 'vendor/asimlqt/php-google-spreadsheet-client/src/Google/Spreadsheet/Spreadsheet.php';
require_once 'vendor/asimlqt/php-google-spreadsheet-client/src/Google/Spreadsheet/WorksheetFeed.php';
require_once 'vendor/asimlqt/php-google-spreadsheet-client/src/Google/Spreadsheet/Worksheet.php';
require_once 'vendor/asimlqt/php-google-spreadsheet-client/src/Google/Spreadsheet/ListFeed.php';
require_once 'vendor/asimlqt/php-google-spreadsheet-client/src/Google/Spreadsheet/ListEntry.php';
require_once 'vendor/asimlqt/php-google-spreadsheet-client/src/Google/Spreadsheet/Batch/BatchRequest.php';
require_once 'vendor/asimlqt/php-google-spreadsheet-client/src/Google/Spreadsheet/Batch/BatchResponse.php';
require_once 'vendor/asimlqt/php-google-spreadsheet-client/src/Google/Spreadsheet/CellFeed.php';
require_once 'vendor/asimlqt/php-google-spreadsheet-client/src/Google/Spreadsheet/CellEntry.php';
require_once 'vendor/asimlqt/php-google-spreadsheet-client/src/Google/Spreadsheet/Util.php';


/**
 * AUTHENTICATE
 *
 */
// These settings are found on google developer console
const CLIENT_APP_NAME = '805 Gsheets';
const CLIENT_ID       = '370ff9d8a0028b7cb6a3e545e3cb69cfe2b8f0d7';
const CLIENT_EMAIL    = 'id-05-gsheets@handy-position-134915.iam.gserviceaccount.com';
const CLIENT_KEY_PATH = '805 Gsheets-370ff9d8a002.p12'; // PATH_TO_KEY = where you keep your key file
const CLIENT_KEY_PW   = 'notasecret';
 
$objClientAuth  = new Google_Client ();
$objClientAuth -> setApplicationName (CLIENT_APP_NAME);
$objClientAuth -> setClientId (CLIENT_ID);
$objClientAuth -> setAssertionCredentials (new Google_Auth_AssertionCredentials (
    CLIENT_EMAIL, 
    array('https://spreadsheets.google.com/feeds','https://docs.google.com/feeds'), 
    file_get_contents (CLIENT_KEY_PATH), 
    CLIENT_KEY_PW
));
$objClientAuth->getAuth()->refreshTokenWithAssertion();
$objToken  = json_decode($objClientAuth->getAccessToken());
$accessToken = $objToken->access_token;
 
 
/**
 * Initialize the service request factory
 */ 
use Google\Spreadsheet\DefaultServiceRequest;
use Google\Spreadsheet\ServiceRequestFactory;
//$Arrsheets = array("2444","2445","2446");
//foreach ($Arrsheets as $worksheetTitle) { 
$serviceRequest = new DefaultServiceRequest($accessToken);
ServiceRequestFactory::setInstance($serviceRequest);
 

$worksheetTitle = "2443";
$sheetflag = true;
/**
 * Get spreadsheet by title
 */

$spreadsheetTitle = '805 Score';

$spreadsheetService = new Google\Spreadsheet\SpreadsheetService();
$spreadsheetFeed = $spreadsheetService->getSpreadsheets();
$spreadsheet = $spreadsheetFeed->getByTitle($spreadsheetTitle);
$worksheetFeed =  $spreadsheet->getWorksheets();




/**
 * Add new worksheet to the spreadsheet
 */
if(!$worksheetFeed->getByTitle($worksheetTitle)) {
	$sheetflag = false;
//	$worksheetTitle = '2444';
	$spreadsheet->addWorksheet($worksheetTitle, 50, 20); // 50 rows & 20 columns
}

//$worksheetTitle = '2443'; // it's generally named 'Sheet1' 
$worksheetFeed = $spreadsheet->getWorksheets();
$worksheet = $worksheetFeed->getByTitle($worksheetTitle);

/** 
 * Add/update headers of worksheet
 */
//visteam,hometeam,visscore,homescore,inning,b,s,o,first,second,third,pitcher
$cellFeed = $worksheet->getCellFeed();
$batchRequest = new Google\Spreadsheet\Batch\BatchRequest();
//Add heading when sheet newly created
if($sheetflag == false) {

//	$cellFeed->editCell(1,1, "PROGRAM"); // 1st row, 1st column
//	$cellFeed->editCell(1,2, "Gamenumber"); // 1st row, 2nd column
//	$cellFeed->editCell(1,3, "visteam"); // 1st row, 1st column
//	$cellFeed->editCell(1,4, "hometeam"); // 1st row, 2nd column
//	$cellFeed->editCell(1,5, "visscore"); // 1st row, 1st column
//	$cellFeed->editCell(1,6, "homescore"); // 1st row, 2nd column
//	$cellFeed->editCell(1,7, "inning"); // 1st row, 1st column
//	$cellFeed->editCell(1,8, "b"); // 1st row, 2nd column
//	$cellFeed->editCell(1,9, "s"); // 1st row, 1st column
//	$cellFeed->editCell(1,10, "o"); // 1st row, 2nd column
//	$cellFeed->editCell(1,11, "first"); // 1st row, 1st column
//	$cellFeed->editCell(1,12, "second"); // 1st row, 2nd column
//	$cellFeed->editCell(1,13, "third"); // 1st row, 1st column
//	$cellFeed->editCell(1,14, "pitcher"); // 1st row, 2nd column

	$batchRequest->addEntry($cellFeed->createInsertionCell(1,1, "PROGRAM"));
	$batchRequest->addEntry($cellFeed->createInsertionCell(1,2, "Gamenumber"));
	$batchRequest->addEntry($cellFeed->createInsertionCell(1,3, "visteam"));
	$batchRequest->addEntry($cellFeed->createInsertionCell(1,4, "hometeam"));
	$batchRequest->addEntry($cellFeed->createInsertionCell(1,5, "visscore"));
	$batchRequest->addEntry($cellFeed->createInsertionCell(1,6, "homescore"));
	$batchRequest->addEntry($cellFeed->createInsertionCell(1,7, "inning"));
	$batchRequest->addEntry($cellFeed->createInsertionCell(1,8, "b"));
	$batchRequest->addEntry($cellFeed->createInsertionCell(1,9, "s"));
	$batchRequest->addEntry($cellFeed->createInsertionCell(1,10, "o"));
	$batchRequest->addEntry($cellFeed->createInsertionCell(1,11, "first"));
	$batchRequest->addEntry($cellFeed->createInsertionCell(1,12, "second"));
	$batchRequest->addEntry($cellFeed->createInsertionCell(1,13, "third"));
	$batchRequest->addEntry($cellFeed->createInsertionCell(1,14, "pitcher"));
}

//$cellFeed->editCell(2,1, "ON"); // 1st row, 1st column
//$cellFeed->editCell(2,2, "2345"); // 1st row, 2nd column
//$cellFeed->editCell(2,3, "VISTEAM"); // 1st row, 1st column
//$cellFeed->editCell(2,4, "HOMETEAM"); // 1st row, 2nd column
//$cellFeed->editCell(2,5, "VISSCORE"); // 1st row, 1st column
//$cellFeed->editCell(2,6, "homescore"); // 1st row, 2nd column
//$cellFeed->editCell(2,7, "inning"); // 1st row, 1st column
//$cellFeed->editCell(2,8, "b"); // 1st row, 2nd column
//$cellFeed->editCell(2,9, "s"); // 1st row, 1st column
//$cellFeed->editCell(2,10, "o"); // 1st row, 2nd column
//$cellFeed->editCell(2,11, "first"); // 1st row, 1st column
//$cellFeed->editCell(2,12, "SECOND"); // 1st row, 2nd column
//$cellFeed->editCell(2,13, "third"); // 1st row, 1st column
//$cellFeed->editCell(2,14, "PITCHER"); // 1st row, 2nd column

$batchRequest->addEntry($cellFeed->createInsertionCell(2,1, "ON"));
$batchRequest->addEntry($cellFeed->createInsertionCell(2,2, "2345"));
$batchRequest->addEntry($cellFeed->createInsertionCell(2,3, "VISTEAM"));
$batchRequest->addEntry($cellFeed->createInsertionCell(2,4, "HOMETEAM"));
$batchRequest->addEntry($cellFeed->createInsertionCell(2,5, "VISSCORE"));
$batchRequest->addEntry($cellFeed->createInsertionCell(2,6, "homescore"));
$batchRequest->addEntry($cellFeed->createInsertionCell(2,7, "inning"));
$batchRequest->addEntry($cellFeed->createInsertionCell(2,8, "b"));
$batchRequest->addEntry($cellFeed->createInsertionCell(2,9, "s"));
$batchRequest->addEntry($cellFeed->createInsertionCell(2,10, "o"));
$batchRequest->addEntry($cellFeed->createInsertionCell(2,11, "first"));
$batchRequest->addEntry($cellFeed->createInsertionCell(2,12, "SECOND"));
$batchRequest->addEntry($cellFeed->createInsertionCell(2,13, "third"));
$batchRequest->addEntry($cellFeed->createInsertionCell(2,14, "PITCHER"));
$batchRequest->addEntry($cellFeed->createInsertionCell(2,14, "green"));


$batchResponse = $cellFeed->insertBatch($batchRequest);
//}
?>