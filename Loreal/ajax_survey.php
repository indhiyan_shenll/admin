<?php
include_once("../../includes/configure.php");
include_once ("includes/loreal_sessioncheck.php");
error_reporting(E_ALL);

if (isset($_POST["formValues"]) && $_POST["post_type"]=="surveyrescount") {

    $formValues = $_POST["formValues"];    
    $formValuesArray = array();
    parse_str($formValues, $formValuesArray);
    $reporting_period = $formValuesArray["reporting_period"];

    if (!empty($formValuesArray['start_date'])) {
        $fromDate = explode("/", $formValuesArray['start_date']);
        $start_date = $fromDate[2]."-".$fromDate[1]."-".$fromDate[0];
    }
    if (!empty($formValuesArray['end_date'])) {
        $toDate = explode("/", $formValuesArray['end_date']);
        $end_date = $toDate[2]."-".$toDate[1]."-".$toDate[0];
    }

    $user_id = $_SESSION['user_id'];
    $demo_id = $_SESSION['demo_id'];
    $_SESSION["reporting_period"][$demo_id] = $reporting_period;

    //Set Startdate and Todate into session
    if (!empty($start_date)) {
        $_SESSION["start_date"][$demo_id] = $start_date;
    } else {
        $_SESSION["start_date"][$demo_id] = "";
    }

    if (!empty($end_date)) {
        $_SESSION["end_date"][$demo_id] = $end_date;
    } else {
        $_SESSION["end_date"][$demo_id] = "";
    }
    

    $betweenQry = "";
    if ($reporting_period != "all dates") {
        if (!empty($start_date) && !empty($end_date)) {
            // $betweenQry = " AND (timestamp BETWEEN '$start_date' AND '$end_date')";
            $betweenQry = "AND DATE_FORMAT(timestamp, '%Y-%m-%d') >= '$start_date' AND DATE_FORMAT(timestamp, '%Y-%m-%d') <= '$end_date'";
        } elseif (!empty($start_date) && empty($end_date)) {
            // $betweenQry = " AND timestamp >= '$start_date'";
            $betweenQry = "AND DATE_FORMAT(timestamp, '%Y-%m-%d') >= '$start_date'";
        } elseif (empty($start_date) && !empty($end_date)) {
            // $betweenQry = " AND timestamp <= '$end_date'";
            $betweenQry = "AND DATE_FORMAT(timestamp, '%Y-%m-%d') <= '$end_date'";
        } else {
            $betweenQry = "";
        }
    } 

    $datestring = date("Y-m-d", strtotime($end_date." last day of last month"));
    $dt = date_create($datestring);
    $previousMonth = $dt->format('Y-m-d');

    $surveyAnsQry = $DBCONN->prepare("SELECT *  FROM tbl_temp_survey_answers  WHERE user_id=:user_id AND demo_id=:demo_id AND DATE_FORMAT(timestamp, '%Y-%m-%d') > '$start_date' AND  DATE_FORMAT(timestamp, '%Y-%m-%d') <= '$previousMonth' ORDER BY timestamp ASC");
    $surveyAnsQryArr = array(":demo_id" =>$demo_id, ":user_id" =>$user_id );
    $surveyAnsQry->execute($surveyAnsQryArr);
    $getsurveyCnt = $surveyAnsQry->rowCount();

    if ($getsurveyCnt == 0) {
        
        $endMonth = date("Y-m-d", strtotime($end_date));        
        $surveyAnsQry = $DBCONN->prepare("SELECT *  FROM tbl_temp_survey_answers  WHERE user_id=:user_id AND demo_id=:demo_id AND DATE_FORMAT(timestamp, '%Y-%m-%d') >= '$start_date' AND  DATE_FORMAT(timestamp, '%Y-%m-%d') <= '$endMonth' ORDER BY timestamp ASC");
        $surveyAnsQryArr = array(":demo_id" =>$demo_id, ":user_id" =>$user_id );
        $surveyAnsQry->execute($surveyAnsQryArr);
        $getsurveyCnt = $surveyAnsQry->rowCount();

    }
    
    $responseArr = "";
    $backgroundColor = "";
    if ($getsurveyCnt > 0) {
        $masterSurveyRes = $surveyAnsQry->fetchAll(PDO::FETCH_ASSOC); 

        $qstnArr = "";
        $q = 0;
        foreach ($masterSurveyRes as $masterSurveyval) {
            $qstnArr["qstn1_ans"][] = $masterSurveyval["qstn1_ans"];
            $qstnArr["qstn2_ans"][] = $masterSurveyval["qstn2_ans"];
            $qstnArr["qstn3_ans"][] = $masterSurveyval["qstn3_ans"];
            $qstnArr["qstn4_ans"][] = $masterSurveyval["qstn4_ans"];
            $qstnArr["qstn5_ans"][] = $masterSurveyval["qstn5_ans"];
            $qstnArr["qstn6_ans"][] = $masterSurveyval["qstn6_ans"];
            $qstnArr["qstn7_ans"][] = $masterSurveyval["qstn7_ans"];
            $qstnArr["qstn8_ans"][] = $masterSurveyval["qstn8_ans"];
            $qstnArr["qstn9_ans"][] = $masterSurveyval["qstn9_ans"];
            $qstnArr["qstn10_ans"][] = $masterSurveyval["qstn10_ans"];
            $qstnArr["qstn11_ans"][] = $masterSurveyval["qstn11_ans"];
            $qstnArr["qstn12_ans"][] = $masterSurveyval["qstn12_ans"];
            $q++;

        }
        $qstnAnsAvg = "";
        $yesnoqstn = "";
        foreach ($qstnArr as $qstnNo => $qstnvals) {
            if ($qstnNo == "qstn4_ans" || $qstnNo == "qstn5_ans" || $qstnNo == "qstn8_ans" || $qstnNo == "qstn10_ans" || $qstnNo == "qstn12_ans") {
                $yesnoqstn = array_count_values($qstnArr[$qstnNo]);

                foreach ($yesnoqstn as $ansKey => $ansVal) {

                    $qstnAnsAvg[$qstnNo][$ansKey] = round(($yesnoqstn[$ansKey] / array_sum($yesnoqstn)) * 100, 1)."%";
                   
                }                    
            } elseif ($qstnNo == "qstn6_ans") {
                $a = 1;
                foreach ($qstnvals as $ansKey => $ansVal) { 
                    if (strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', $ansVal)) != "notrelevant") {
                        $cntans = $a;
                        $qstn6ans = explode(" ", $ansVal);
                        $qstn6arr[$qstnNo][] = $qstn6ans[0];
                        $qstnAnsAvg[$qstnNo] = round(array_sum($qstn6arr[$qstnNo]) / count($qstn6arr[$qstnNo]), 1);
                        $a++;
                    }

                }
            } else {
                $qstnAnsAvg[$qstnNo] = round(array_sum($qstnvals) / count($qstnvals), 1);
            }            
        }
        
        for ($k = 1; $k <=count($qstnAnsAvg); $k++) {

            $qstncolorQry = $DBCONN->prepare("SELECT green, amber FROM tbl_survey_benchmark WHERE survey_id=:id");
            $qstncolorQryArr = array(":id" => $k);
            $qstncolorQry->execute($qstncolorQryArr);
            $getQstCnt = $qstncolorQry->rowCount();
            

            if ($getQstCnt > 0) {
                $QstnResData = $qstncolorQry->fetch(PDO::FETCH_ASSOC);

                if ($qstnAnsAvg["qstn".$k."_ans"] >= $QstnResData['green']) {            
                    $backgroundColor["qstn".$k."_ans"] = "background-color: #B4DE86";
                } elseif ($qstnAnsAvg["qstn".$k."_ans"] <= $QstnResData['green'] && $qstnAnsAvg["qstn".$k."_ans"] >= $QstnResData['amber']) {

                    $backgroundColor["qstn".$k."_ans"] = "background-color: #FFDE75";
                } else {

                    $backgroundColor["qstn".$k."_ans"] = "background-color: #F0A48C";
                }
            }
        }        

    }

    // echo "SELECT count(user_id) as user_id FROM tbl_temp_survey_answers WHERE user_id=:user_id AND demo_id=:demo_id $betweenQry";
    $countRresQry = $DBCONN->prepare("SELECT count(user_id) as user_id FROM tbl_temp_survey_answers WHERE user_id=:user_id AND demo_id=:demo_id $betweenQry");
    $countRresQryArr = array(":demo_id" =>$demo_id, ":user_id" =>$user_id );
    $countRresQry->execute($countRresQryArr);
    $getResCnt = $countRresQry->rowCount();

    $surveyResCount = 0;
    if ($getResCnt > 0) {
        $surveyRes = $countRresQry->fetch();
        $surveyResCount = (int) $surveyRes["user_id"];
    }
    $responseArr['survey_count'] = $surveyResCount;
    $responseArr['background_color'] = $backgroundColor;

    $_SESSION["survey_res_count"][$demo_id] = $surveyResCount;
    echo json_encode($responseArr);
    exit;    
}

//Sending Email with Attachment
if(isset($_POST["post_type"]) && $_POST["post_type"] == "sendmail") {

    $salonownername=$_POST["salonownername"];
    $salonOwnerFirstname = explode(" ", $salonownername);
    $affiliatename=$_POST["affiliatename"];
    $affFirstname = explode(" ", $affiliatename);
    $mobilenumber=$_POST["mobilenumber"];    
    $DemoemailAddress=$_POST["email"];
    $emailAddress=$_POST["frommail"];
     
    $sessionlang=$_POST["sessionlang"];
    $demo_id = $_POST["demo_id"];
    $user_id = $_SESSION["user_id"];
 
    $FromAddress = $affiliatename;
    if($sessionlang=="English"){
        include("lang/eng_lang.php");
        $getLangMailContent  = funpdfMailCoutry($sessionlang,$salonOwnerFirstname,$affiliatename,$mobilenumber); 
    } 
    if($sessionlang=="SEK"){
        include("lang/swd_lang.php");
        $getLangMailContent  = funpdfMailCoutry($sessionlang,$salonOwnerFirstname,$affiliatename,$mobilenumber);
    }
    //print_r($getLangMailContent); exit;
    $EmailSubject   = $getLangMailContent["PdfMailContent"]["emailsubject"];
    $saloneMotionReport = "salon_eMotion_report/Salon_eMotions_Report_".$demo_id."_".$user_id."_".date("dmy").".pdf";

    global $EmailMessage;
    $EmailMessage   = $getLangMailContent["PdfMailContent"]["message"];
    
    header("Content-Type: text/html; charset=UTF-8", true);
    require 'mailer/PHPMailerAutoload.php';
    $mail = new PHPMailer;
    $mail->charSet = "UTF-8";
    $mail->Host = 'shenll.net;shenll.net'; // Specify main and backup server
    $mail->From = $emailAddress;
    $mail->CharSet = 'UTF-8'; 
    $mail->FromName = $FromAddress;
    $mail->addAddress($DemoemailAddress);
    // $mail->addAddress('indhiyan.shenll@gmail.com');
    // $mail->addAddress('karuna.shenll@gmail.com');
    // $mail->addAddress('karunakaran.c@shenll.com');
    // $mail->addAddress('prahathese@shenll.com'); 
    $mail->addReplyTo($FromAddress);         //,'Support'
    $mail->WordWrap = 100;                                 // Set word wrap to 50 characters
    $mail->isHTML(true);     
    $mail->CharSet = 'UTF-8';                             // Set email format to HTML
    $mail->Subject =$EmailSubject;
    $mail->CharSet = 'UTF-8';
    $mail->Body    =  $EmailMessage;
    $mail->addAttachment($saloneMotionReport);
    $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';    
    if(!$mail->send()) {
        echo "failed";
    } else {
        if (file_exists($saloneMotionReport)) {
            unlink($saloneMotionReport);        
        }
        echo "success";        
    }
    exit; 
}