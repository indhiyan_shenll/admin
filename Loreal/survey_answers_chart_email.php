<?php
include_once("../../includes/configure.php");
include_once ("../includes/loreal_sessioncheck.php");
include_once ("survey_answers.php");

$chartFlag = false;
$surveyQstn = $demo_id = $user_id = "";
// print_r($_SESSION);

// $_POST['demo_id'] = 630;
// $_POST['user_id'] = 3;

if (isset($_POST['demo_id'])) {

    $surveyQstn  = '';
    $demo_id = trim($_SESSION['demo_id']);
    $user_id = $_SESSION['user_id'];
    $responseArr  = array();


    for ($Inc=1;$Inc<=12;$Inc++) {
        $surveyQstn= $Inc;
        $Imagename  = 'chartimg_'.$demo_id.'_'.$user_id.'_'.$Inc;

        $qstnQry = $DBCONN->prepare("SELECT * FROM tbl_survey_benchmark WHERE survey_id=:survey_id");
        $qstnQryArr = array(":survey_id" => $surveyQstn);
        $qstnQry->execute($qstnQryArr);
        $cntQstn = $qstnQry->rowCount();

        if ($cntQstn > 0) {
            $fetchQstns = $qstnQry->fetch(PDO::FETCH_ASSOC);
            $question = $fetchQstns["question"];
            $green = $fetchQstns["green"];
            $amber = $fetchQstns["amber"];
            $questionType = $fetchQstns["question_type"];
        }

        if ($questionType == "scale") {
            $chartFlag = true;
        } else {
            $chartFlag = false;
        }

        // if ($surveyQstn == 1 || $surveyQstn == 2 || $surveyQstn == 3 || $surveyQstn == 6 || $surveyQstn == 7 || $surveyQstn == 9 || $surveyQstn == 11) {
        //     $chartFlag = true;
        // } else {
        //     $chartFlag = false;
        // }

        // $qstnQry = $DBCONN->prepare("SELECT * FROM tbl_survey_benchmark WHERE survey_id=:survey_id");
        // $qstnQryArr = array(":survey_id" => $surveyQstn);
        // $qstnQry->execute($qstnQryArr);
        // $cntQstn = $qstnQry->rowCount();

        // if ($cntQstn > 0) {
        //     $fetchQstns = $qstnQry->fetch(PDO::FETCH_ASSOC);
        //     $question = $fetchQstns["question"];
        // }

        // $start_date = '';
        // $end_date = '';
        $start_date = $_SESSION["start_date"][$demo_id];
        $end_date = $_SESSION["end_date"][$demo_id];

        $betweenQry = "";
        if (!empty($start_date) && !empty($end_date)) {
            // $betweenQry = " AND (timestamp BETWEEN '$start_date' AND '$end_date')";
            $betweenQry = "AND DATE_FORMAT(timestamp, '%Y-%m-%d') >= '$start_date' AND  DATE_FORMAT(timestamp, '%Y-%m-%d') <= '$end_date'";
        } elseif (!empty($start_date) && empty($end_date)) {
            // $betweenQry = " AND timestamp >= '$start_date'";
            $betweenQry = "AND DATE_FORMAT(timestamp, '%Y-%m-%d') >= '$start_date'";
        } elseif (empty($start_date) && !empty($end_date)) {
            // $betweenQry = " AND timestamp <= '$end_date'";
            $betweenQry = "AND DATE_FORMAT(timestamp, '%Y-%m-%d') <= '$end_date'";
        } else {
            $betweenQry = "";
        }

        $selectAns = $DBCONN->prepare("SELECT * FROM tbl_temp_survey_answers WHERE user_id=:user_id AND demo_id=:demo_id $betweenQry ORDER BY timestamp ASC");
        $selectAnsArr = array(":user_id" => $user_id, ":demo_id" => $demo_id);
        $selectAns->execute($selectAnsArr);
        $cntRes = $selectAns->rowCount();

        if ($cntRes > 0) {
            $fetchSurveyAns = $selectAns->fetchAll(PDO::FETCH_ASSOC);       

            $AnsArr = "";
            $YearMonthAvg = "";

            $len = 0;
            foreach ($fetchSurveyAns as $surveyAns) {

                $timestamp = "";
                if (!empty($surveyAns['timestamp'])) {
                    $dbtimeStamp = str_replace('/', '-', $surveyAns['timestamp']);
                    $dateTime = explode(" ", $dbtimeStamp);            
                    $date = explode("-", $dateTime[0]);
                    $dateFormat = $date[2]."-".$date[1]."-".$date[0];
                    // $timestamp = $dateFormat." ".$dateTime[1];
                    $timestamp = date("Y-m-d H:i:s", strtotime($dateFormat." ".$dateTime[1]));
                }
                // $Year = (int) date("Y", strtotime($surveyAns['timestamp']));
                $monthNum = date("Y-m", strtotime($timestamp));
                // $Year = date("Y");

                $notRelevant = strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', $surveyAns["qstn".$surveyQstn."_ans"]));
                if ($chartFlag == true) {
                    if ($surveyQstn == 6 ) {
                       
                        if ($notRelevant != 'notrelevant') {
                            $explodeAns = explode(" ", $surveyAns["qstn".$surveyQstn."_ans"]);
                            $AnsArr[$monthNum][] += (int) $explodeAns[0];
                        }
                        
                    } else {
                        
                        // $AnsArr[$monthNum][] += (int) $surveyAns["qstn".$surveyQstn."_ans"];
                        $AnsArr[$monthNum][] += (int) $surveyAns["qstn".$surveyQstn."_ans"];

                    }

                    $j = 1;
                    foreach ($AnsArr as $AnsMonth => $AnsMonthqs) {
                        $YearMonthAvg[$j] = round(array_sum($AnsMonthqs) / count($AnsMonthqs), 1);
                        $j++;
                    }
                } else {

                    $AnsArr[$monthNum][] = $surveyAns["qstn".$surveyQstn."_ans"];
                    $YearMonthAvgNew[$monthNum] = array_count_values($AnsArr[$monthNum]);
                    $j = 1;
                    foreach ($YearMonthAvgNew as $AnsMonth => $AnsMonthqs) {

                        $YearMonthAvgBar[$j]['Yes'] = round($AnsMonthqs['Yes'] / array_sum($AnsMonthqs), 2);
                        $YearMonthAvgBar[$j]['No'] = round($AnsMonthqs['No'] / array_sum($AnsMonthqs), 2);
                        $YearMonthAvgBar[$j]['Maybe'] = round($AnsMonthqs['Maybe'] / array_sum($AnsMonthqs), 2);
                        $barChartData['Yes'][$j] = ($YearMonthAvgBar[$j]['Yes']) * 100;
                        $barChartData['No'][$j] = ($YearMonthAvgBar[$j]['No']) * 100;
                        $barChartData['Maybe'][$j] = ($YearMonthAvgBar[$j]['Maybe']) * 100;

                        $j++;
                    }
                }

               $len++;     
            }

        }

        if ($chartFlag == true) { 

            $responseArr[$surveyQstn]['charttype']  = 1;
            $yaxisValues = json_encode(array_values($YearMonthAvg));
            $xaxisValues = json_encode(array_keys($YearMonthAvg));

            $responseArr[$surveyQstn]['xaxisValues']  = $xaxisValues;
            $responseArr[$surveyQstn]['yaxisValues']  = $yaxisValues;

        } else {

            $responseArr[$surveyQstn]['charttype']  = 2;
            $yaxisValueYes = json_encode(array_values($barChartData['Yes']));
            $yaxisValueNo = json_encode(array_values($barChartData['No']));
            $yaxisValueMaybe = json_encode(array_values($barChartData['Maybe']));
            $xaxisValues = json_encode(array_keys($barChartData['Yes']));

            // $responseArr[$surveyQstn]['yaxisValues']  = $yaxisValues;

            $responseArr[$surveyQstn]['yaxisValueYes']  = $yaxisValueYes;
            $responseArr[$surveyQstn]['yaxisValueNo']  = $yaxisValueNo;
            $responseArr[$surveyQstn]['yaxisValueMaybe']  = $yaxisValueMaybe;
            $responseArr[$surveyQstn]['xaxisValuesYes']  = $xaxisValues;
        }

        $responseArr[$surveyQstn]['image_name']  = $Imagename;
        $responseArr[$surveyQstn]['green']  = $green;
        $responseArr[$surveyQstn]['amber']  = $amber;

    }    
    echo json_encode($responseArr);
    exit;
}