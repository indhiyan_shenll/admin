/*
 *custom.js
 *
 * Demo JavaScript used on Validation-pages.
 */
//Loreal Form validations start here
$(document).ready(function(){
	$("#frm_lorellogin").validate({
	    rules: {
			Username: {
				required: true
			},
			Password:"required",
		},
		messages: {
			Username: {
				required: "Please enter user name"
			},
			Password: "Please enter password",
		},
	});

	$("#frm_forgetlogin").validate({
	    rules: {
			email: {
				required: true,
				email:true
			},
			
		},
		messages: {
			email: {
				required: "Please enter email address",
				email:"Please enter valid email addresss"

			},
			
		},
	});
});

//Loreal end validations start here
