/**
 * jQuery fixedHeader plugin.
 * 
 * @author   Dave Widmer
 * @version  0.0.1
 */(function(a){var b=function(){var b=function(){return this.each(function(){c(this)})},c=function(b){var c=a(b).offset().top,f=a(b).clone();id=a(b).attr("id")||"fixedTable",f.attr("id",id+"-cloned"),f.find("tbody").remove(),f.css("position","fixed").css("top","0px"),e(a(b),f),f.hide(),a(b).after(f),a(window).bind("scroll",function(){d(f,c,a(this).scrollTop())}),a(window).resize(function(){e(a(b),f)})},d=function(a,b,c){c>b?a.show():a.hide()},e=function(a,b){b.width(a.width()),a.find("th").each(function(c){var d="thead th:eq("+c+")";b.find(d).css("width",a.find(d).css("width"))})};return{init:b}}();a.fn.fixedHeader=b.init})(jQuery);