/*
 * custom.js
 *
 * Place your code here that you need on all your pages.
 */
$(document).ready(function(){
	$("#callback_date").datetimepicker({ format:'d-m-Y',formatDate:'d-m-Y',timepicker:false});
	$("#import_date").datetimepicker({ format:'d-m-Y',formatDate:'d-m-Y',timepicker:false});
	$("#last_contact").datetimepicker({ format:'d-m-Y',formatDate:'d-m-Y',timepicker:false});
	$('#callback_time1').datetimepicker({
		datepicker:false,
		formatTime:'g:i A',
		format:'g:i A',
		step:5

	});	
	  $('#demo_time').datetimepicker({
		datepicker:false,
		formatTime:'g:i A',
		format:'g:i A',
		step:5
		
		});
		 
		$("#demo_date").datetimepicker({ format:'d-m-Y',formatDate:'d-m-Y',timepicker:false});
});
	       
			function setcode(val) {
			res = val.replace(/([~!@#$%^&*()_+=`{}\[\]\|\\:;'<>,.\/? ])+/g, '').replace(/^(-)+|(-)+$/g,'');
			var res = res.substring(0,4);
			var res = res.toUpperCase();
			var chars = "0123456789";
			var string_length = 3;
			var randomstring = '';
			for (var i=0; i<string_length; i++) {
				var rnum = Math.floor(Math.random() * chars.length);
				randomstring += chars.substring(rnum,rnum+1);
				
			}
			 var extra="0";
		     var promocode=res+randomstring+extra;
			 var promocode_id=$("#hidden_special").val();
		     if(promocode_id==""){
			    promocode_id=0;
		     }
			 $.ajax({url:"check_promo.php?name="+promocode+"&id="+promocode_id,success:function(result){
			 if(parseInt(result)>0){
               var business_val=$("#business_name").val();
				  setcode(business_val) ;    
			}
			else{
				
				$('#promo_code').val(promocode);
			  }
			  }});
			}
			function show(id){
				if($("#"+id).is(':checked'))
					$(".theme_div").show();
				else
					$(".theme_div").hide();

            }
		function send_mail(){
				var emailPattern=/^[a-zA-Z0-9\-_]+(\.[a-zA-Z0-9\-_]+)*@[a-z0-9]+(\-[a-z0-9]+)*(\.[a-z0-9]+(\-[a-z0-9]+)*)*\.[a-z]{2,4}$/;
				var result=validateForm();
				if($.trim($("#email").val())==""){
					alert("Please enter email address");
					$("#email").focus();
					return false;
				}
				else if(!emailPattern.test($.trim($("#email").val()))){
					alert("Please enter valid email address");
					$("#email").focus();
					return false;
				}
				else if($.trim($("#promo_code").val())==""){
					alert("Please enter promo code");
					$("#promo_code").focus();
					return false;
				}
				else if(result){
					$("#agent_send_email").val("yes");
					$("#prospect_form").submit();
					/* var promo_code=$("#promo_code").val();
					 var email=$("#email").val();
							$.ajax({url:"send_mail_prospect.php?email="+email+"&promocode="+promo_code,success:function(result){
							 $(".mail_date").show();
							  alert('Your email has been sent.');
							  

							}});*/
				}
				 

		   
		}
		function validateForm() {
		   var isValid = true;
		     $("#email1").val($("#email").val());
		     $("#phone1").val($("#phone").val());
			$('.form_control:visible').each(function() {
			if ($(this).val() ==='' || $(this).val() ==='0') 
               isValid = false;
			});
             if(isValid){
			   $('.send_email_bg').css('background-color', '#FFC000');
			 }
			 else{
			   $('.send_email_bg').css('background-color', 'gray');
			 
			 }
			 return isValid;
			 
			 
		}
		function get_Licensee(affiliate_id){
			 
			$.ajax({url:"get_licensee.php?affiliate_id="+affiliate_id,success:function(result){
				 
				$("#licensee").val(result);
				$("#affiliate").val(affiliate_id);
		    }});
		}
		function get_Licensee_details(affiliate_id){
			 
			 $("#affiliate").val(affiliate_id);
			$.ajax({url:"get_licensee_details.php?affiliate_id="+affiliate_id,success:function(result){
				 if(result!=""){
                   $("#licensee").val(result);
				 }
				
				$("#affiliate").val(affiliate_id);
		    }});
		}
		function getmanger(salespersonid){
				//alert(salespersonid);
				$.ajax({url:"getmanger.php?salespersonid="+salespersonid,success:function(result){
					$("#sales_manager").val(result);
				}});
		 }
		function show_demo_image(val){
			if(val>0){
				$(".theme_div_demo").show();
				$("#theme_div_design").html('<img src="themes/'+val+'.png" style="margin:auto;height:150px;"> ');
			}
			else{
				$(".theme_div_demo").hide();
			}
		}
		function show_comment(val){
			var val=$.trim(val);
			 if(val==""){
			      $('.add_coomment_box').css('background-color', 'gray');
			      $('.add_coomment_box').css('border-color', 'gray'); 
			 }else{
			    $('.add_coomment_box').css('background-color', '#ff4215');
			    $('.add_coomment_box').css('border-color', '#ff4215');
			 
			 }	 
		  		   
		
		}
		  




 