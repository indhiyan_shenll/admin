<?php
include("../includes/configure.php");
include("includes/cmm_functions.php");
include("../includes/session_check.php");
include("email_function.php");

if(isset($_REQUEST['post_type']) && $_REQUEST['post_type'] !=''){
  
	if($_REQUEST['post_type']!="reset"){
	    
	    $list_id=$_REQUEST["list_id"];
	    $user_name =$_SESSION['user_name'];
	    $user_id=$_SESSION['user_id'];
		$servicebydate=date("Y-m-d");
        $post_type =$_REQUEST['post_type'];
        $amount    =$_REQUEST['amount'];
        $pay_date  =$_REQUEST['pay_date'];
        list($day,$month,$year) =explode("-",$pay_date);
		$pay_date=$year."-".$month."-".$day;
        $comment_txt  =$_REQUEST['comments'];

        // Get demotable get detail and user table
            $getDemoQry="select * from tbl_demo where demo_id=:demoid";
			$pepDemoQry=$DBCONN->prepare($getDemoQry);
			$pepDemoQry->execute(array(":demoid"=>$list_id));
			$getDemoCount=$pepDemoQry->rowcount();
		    $getDemoRow=$pepDemoQry->fetch();
			$userid      = stripslashes($getDemoRow["user_id"]);
			$csmusertype = stripslashes($getDemoRow["csm_usertype"]);
			$demusertype = stripslashes($getDemoRow["dem_usertype"]);
            $useremailArrid[]=$userid.",".$csmusertype.",".$demusertype;
			
			$dbQry1="select * from  tbl_users where user_id in(".implode(',',$useremailArrid).")";
			$getResQry		=	$DBCONN->prepare($dbQry1);
			$getResQry      ->execute();
			$getResCnt		=	$getResQry->rowCount();
			if($getResCnt>0){
		        $si=1;
		        $getuserRow=$getResQry->fetchAll();
				foreach($getuserRow as $getuserdata) {
					$usertype=$getuserdata["user_type"];
					if($usertype=="Admin")
					$Adminemail=$getuserdata["email"];
				    if($usertype=="CSM")
					$csmmail=$getuserdata["email"];
				    $CSMemail =isset($csmmail)? ",".$csmmail : " ";
				    if($usertype=="DEM")
					$dememail=$getuserdata["email"];
				    $DEMemail =isset($dememail)? ",".$dememail : " ";
				  $si++;
                }
			}
			//echo $Adminemail."-".$CSMemail."--".$DEMemail;exit;

			if($post_type=="ios built" || $post_type=="andorid built" || $post_type=="app_iostested" || $post_type=="app_andoridtested" || $post_type=="app_iossumbmit" || $post_type=="app_andoridsumbmit" || $post_type=="app_ioslive" || $post_type=="app_andoridlive" ||  $post_type=="app_desiged" || $post_type=="app_populated" || $post_type=="app_welcomecall" || $post_type=="app_training"){

            //check the listed id or not
			$getServicesQry="select * from tbl_prospectinglist_services where list_id=:demoid and reset=:resetid";
			$pepServiceQry=$DBCONN->prepare($getServicesQry);
			$pepServiceQry->execute(array(":demoid"=>$list_id,":resetid"=>"0"));
			$getserviceCount=$pepServiceQry->rowcount();
		    $getserviceRow=$pepServiceQry->fetch();
			$service_id = stripslashes($getserviceRow["services_id"]);
			
			if($getserviceCount==0){

				if($post_type=="ios built"){
				
	              $insertIosBuiilt="INSERT INTO tbl_prospectinglist_services(list_id,ios_built_by,ios_built_date)values(:list_id,:servicebyname,:servicebydate)";
				  $pepgetqryQry=$DBCONN->prepare($insertIosBuiilt);
			    }

			    if($post_type=="andorid built"){
                	$insertIosBuiilt="INSERT INTO tbl_prospectinglist_services(list_id,android_built_by,android_built_date)values(:list_id,:servicebyname,:servicebydate)";
			    	$pepgetqryQry=$DBCONN->prepare($insertIosBuiilt);
			    }

			    if($post_type=="app_iostested"){

			    	$insertIosBuiilt="INSERT INTO tbl_prospectinglist_services(list_id,ios_tested_by,ios_tested_date)values(:list_id,:servicebyname,:servicebydate)";
			    	$pepgetqryQry=$DBCONN->prepare($insertIosBuiilt);
                }

                if($post_type=="app_andoridtested"){

			    	$insertIosBuiilt="INSERT INTO tbl_prospectinglist_services(list_id,android_tested_by,android_tested_date)values(:list_id,:servicebyname,:servicebydate)";
			    	$pepgetqryQry=$DBCONN->prepare($insertIosBuiilt);
                }
                if($post_type=="app_iossumbmit"){
                	$insertIosBuiilt="INSERT INTO tbl_prospectinglist_services(list_id,ios_submitted_by,ios_submitted_date)values(:list_id,:servicebyname,:servicebydate)";
			    	$pepgetqryQry=$DBCONN->prepare($insertIosBuiilt);
                }

                if($post_type=="app_andoridsumbmit"){
                	$insertIosBuiilt="INSERT INTO tbl_prospectinglist_services(list_id,android_submitted_by,android_submitted_date)values(:list_id,:servicebyname,:servicebydate)";
			    	$pepgetqryQry=$DBCONN->prepare($insertIosBuiilt);
                }

                if($post_type=="app_ioslive"){
                	$insertIosBuiilt="INSERT INTO tbl_prospectinglist_services(list_id,ios_live_by,ios_live_date)values(:list_id,:servicebyname,:servicebydate)";
			    	$pepgetqryQry=$DBCONN->prepare($insertIosBuiilt);
                }
                if($post_type=="app_andoridlive"){
                	$insertIosBuiilt="INSERT INTO tbl_prospectinglist_services(list_id,android_live_by,android_live_date)values(:list_id,:servicebyname,:servicebydate)";
			    	$pepgetqryQry=$DBCONN->prepare($insertIosBuiilt);
                }

                if($post_type=="app_desiged"){
                	$insertIosBuiilt="INSERT INTO tbl_prospectinglist_services(list_id,app_design_log,app_design_date	)values(:list_id,:servicebyname,:servicebydate)";
			    	$pepgetqryQry=$DBCONN->prepare($insertIosBuiilt);

                }

                if($post_type=="app_populated"){
                	$insertIosBuiilt="INSERT INTO tbl_prospectinglist_services(list_id,app_populated_by,app_populated_date)values(:list_id,:servicebyname,:servicebydate)";
			    	$pepgetqryQry=$DBCONN->prepare($insertIosBuiilt);
                }

                if($post_type=="app_welcomecall"){
                	$insertIosBuiilt="INSERT INTO tbl_prospectinglist_services(list_id,appwelcome_call,appwelcome_date)values(:list_id,:servicebyname,:servicebydate)";
			    	$pepgetqryQry=$DBCONN->prepare($insertIosBuiilt);
				}
				if($post_type=="app_training"){
					$insertIosBuiilt="INSERT INTO tbl_prospectinglist_services(list_id,apptraining,apptraining_date)values(:list_id,:servicebyname,:servicebydate)";
			    	$pepgetqryQry=$DBCONN->prepare($insertIosBuiilt);
				}

				$pepgetqryQry->execute(array(':list_id'=>$list_id,':servicebyname'=>$user_name,':servicebydate'=>$servicebydate));

		    } else {

                if($post_type=="ios built"){
		          $UpdateIosbuilt="update tbl_prospectinglist_services set list_id=:list_id,  ios_built_by=:servicebyname, ios_built_date=:servicebydate where services_id=:sid";
		          $pepupdateqryQry=$DBCONN->prepare($UpdateIosbuilt);
		        }

		        if($post_type=="andorid built"){
		        	$UpdateIosbuilt="update tbl_prospectinglist_services set list_id=:list_id,  android_built_by=:servicebyname, android_built_date=:servicebydate where services_id=:sid";
                    $pepupdateqryQry=$DBCONN->prepare($UpdateIosbuilt);
		        }

		        if($post_type=="app_iostested"){

		        	$UpdateIosbuilt="update tbl_prospectinglist_services set list_id=:list_id,  ios_tested_by=:servicebyname, ios_tested_date=:servicebydate where services_id=:sid";
                    $pepupdateqryQry=$DBCONN->prepare($UpdateIosbuilt);
		        }
		        if($post_type=="app_andoridtested"){
					$UpdateIosbuilt="update tbl_prospectinglist_services set list_id=:list_id,  android_tested_by=:servicebyname, android_tested_date=:servicebydate where services_id=:sid";
                    $pepupdateqryQry=$DBCONN->prepare($UpdateIosbuilt);
		        }

		        if($post_type=="app_iossumbmit"){

		        	$UpdateIosbuilt="update tbl_prospectinglist_services set list_id=:list_id,  ios_submitted_by=:servicebyname, ios_submitted_date=:servicebydate where services_id=:sid";
                    $pepupdateqryQry=$DBCONN->prepare($UpdateIosbuilt);
		        }
		        if($post_type=="app_andoridsumbmit"){

		        	$UpdateIosbuilt="update tbl_prospectinglist_services set list_id=:list_id,  android_submitted_by=:servicebyname, android_submitted_date=:servicebydate where services_id=:sid";
                    $pepupdateqryQry=$DBCONN->prepare($UpdateIosbuilt);
		        }
		        if($post_type=="app_ioslive"){
		        	$UpdateIosbuilt="update tbl_prospectinglist_services set list_id=:list_id,  ios_live_by=:servicebyname, ios_live_date=:servicebydate where services_id=:sid";
                    $pepupdateqryQry=$DBCONN->prepare($UpdateIosbuilt);
		        }
		        if($post_type=="app_andoridlive"){

		        	$UpdateIosbuilt="update tbl_prospectinglist_services set list_id=:list_id,  android_live_by=:servicebyname, android_live_date=:servicebydate where services_id=:sid";
                    $pepupdateqryQry=$DBCONN->prepare($UpdateIosbuilt);
		        }

		        if($post_type=="app_desiged"){
		        	$UpdateIosbuilt="update tbl_prospectinglist_services set list_id=:list_id,  app_design_log=:servicebyname, app_design_date=:servicebydate where services_id=:sid";
                    $pepupdateqryQry=$DBCONN->prepare($UpdateIosbuilt);
		        }
		        if($post_type=="app_populated"){

		        	$UpdateIosbuilt="update tbl_prospectinglist_services set list_id=:list_id,  app_populated_by=:servicebyname, app_populated_date=:servicebydate where services_id=:sid";
                    $pepupdateqryQry=$DBCONN->prepare($UpdateIosbuilt);
		        }
		        if($post_type=="app_welcomecall"){
		        	$UpdateIosbuilt="update tbl_prospectinglist_services set list_id=:list_id,  appwelcome_call=:servicebyname, appwelcome_date=:servicebydate where services_id=:sid";
                    $pepupdateqryQry=$DBCONN->prepare($UpdateIosbuilt);
		        }
		        if($post_type=="app_training"){
		        	$UpdateIosbuilt="update tbl_prospectinglist_services set list_id=:list_id,  apptraining=:servicebyname, apptraining_date=:servicebydate where services_id=:sid";
                    $pepupdateqryQry=$DBCONN->prepare($UpdateIosbuilt);
		        }


			  $pepupdateqryQry->execute(array(':list_id'=>$list_id,':servicebyname'=>$user_name,':servicebydate'=>$servicebydate,":sid"=>$service_id));
		    }
          
		    if($post_type=="ios built"){
		    	$comments="IOS built";
		    	$returnLogstatus=FunAppStatus("AppBuilt");
		    	$built_by_key = "ios_built_by";
		    	$built_date   ="ios_built_date";
		    	$LogEmailId=Logemails("Adminonly");
		    	$ToAddress=$LogEmailId["logemailid"].$DEMemail;
		    	$LogoUrl = 'http://www.myappybusiness.com/images/email-logo.png';
		    	sendlogemail($ToAddress,'Ios App Built','Your Ios app built succeeded',$LogoUrl);
			}
		    if($post_type=="andorid built"){
		    	$comments="Android built";
		    	$returnLogstatus=FunAppStatus("AppBuilt");
		    	$built_by_key = "android_built_by";
		    	$built_date   ="android_built_date";
		    	$LogEmailId=Logemails("Adminonly");
		    	$ToAddress=$LogEmailId["logemailid"].$DEMemail;
		    	$LogoUrl = 'http://www.myappybusiness.com/images/email-logo.png';
		    	sendlogemail($ToAddress,'Android App Built','Your Android app built succeeded',$LogoUrl);
		    }
		    if($post_type=="app_iostested"){
		    	$comments="IOS Tested";
		    	$returnLogstatus=FunAppStatus("AppTest");
		    	$built_by_key = "ios_tested_by";
		    	$built_date   ="ios_tested_date";
		    	$LogEmailId=Logemails("Adminonly");
		    	$ToAddress=$LogEmailId["logemailid"];
		    	$LogoUrl = 'http://www.myappybusiness.com/images/email-logo.png';
		    	sendlogemail($ToAddress,'Ios App Tested','Your Ios app to be tested',$LogoUrl);
		    }
		    if($post_type=="app_andoridtested"){  
		    	$comments="Android Tested";
		    	$returnLogstatus=FunAppStatus("AppTest");
		    	$built_by_key = "android_tested_by";
		    	$built_date   ="android_tested_date";
		    	$LogEmailId=Logemails("Adminonly");
		    	$ToAddress=$LogEmailId["logemailid"];
		    	$LogoUrl = 'http://www.myappybusiness.com/images/email-logo.png';
		    	sendlogemail($ToAddress,'Android App Tested','Your Android app to be tested',$LogoUrl);
		    }
		    if($post_type=="app_iossumbmit"){  
		    	$comments="IOS App Submitted";
		    	$returnLogstatus=FunAppStatus("AppSubmitted");
		    	$built_by_key = "ios_submitted_by";
		    	$built_date   ="ios_submitted_date";
		    	$LogEmailId=Logemails("Adminonly");
		    	$ToAddress=$LogEmailId["logemailid"];
		    	$LogoUrl = 'http://www.myappybusiness.com/images/email-logo.png';
		    	sendlogemail($ToAddress,'Ios App Submitted','Your app submitted to the iTunes Store',$LogoUrl);
		    }

		    if($post_type=="app_andoridsumbmit"){
		    	$comments="Android App Submitted";
		    	$returnLogstatus=FunAppStatus("AppSubmitted");
		    	$built_by_key = "android_submitted_by";
		    	$built_date   ="android_submitted_date";
		    	$LogEmailId=Logemails("Adminonly");
		    	$ToAddress=$LogEmailId["logemailid"];
		    	$LogoUrl = 'http://www.myappybusiness.com/images/email-logo.png';
		    	sendlogemail($ToAddress,'Android App Submitted','Your app submitted to the Google Play Store',$LogoUrl);
		    }
		    if($post_type=="app_ioslive"){  
				$comments="IOS App live";
				$returnLogstatus=FunAppStatus("AppLive");
		    	$built_by_key ="ios_live_by";
		    	$built_date   ="ios_live_date";
		    	$LogEmailId=Logemails("Adminonly");
		    	$ToAddress=$LogEmailId["logemailid"].$CSMemail;
		    	$LogoUrl = 'http://www.myappybusiness.com/images/email-logo.png';
		    	sendlogemail($ToAddress,'App Live','Your app is live on the iTunes Store',$LogoUrl);
		    }
		    if($post_type=="app_andoridlive"){
		    	$comments="Android App live";
		    	$returnLogstatus=FunAppStatus("AppLive");
		    	$built_by_key ="android_live_by";
		    	$built_date   ="android_live_date";
		    	$LogEmailId=Logemails("Adminonly");
		    	$ToAddress=$LogEmailId["logemailid"].$CSMemail;
		    	$LogoUrl = 'http://www.myappybusiness.com/images/email-logo.png';
		    	sendlogemail($ToAddress,'App Live','Your app is live on the Google Play Store',$LogoUrl);
		    }

		    if($post_type=="app_desiged"){
				$comments="App Desiged";
		    	$built_by_key ="app_design_log";
		    	$built_date   ="app_design_date";
		    	$LogEmailId=Logemails("Developer");
		    	$ToAddress=$LogEmailId["logemailid"];
		    	$LogoUrl = 'http://www.myappybusiness.com/images/email-logo.png';
				sendlogemail($ToAddress,'App Design','Your design stage is completed for this app',$LogoUrl);
            }
		    if($post_type=="app_populated"){
		    	$comments="App Populated";
		    	$built_by_key ="app_populated_by";
		    	$built_date   ="app_populated_date";
		    	$LogEmailId=Logemails("Adminonly");
		    	$ToAddress=$LogEmailId["logemailid"];
		    	$LogoUrl = 'http://www.myappybusiness.com/images/email-logo.png';
		    	sendlogemail($ToAddress,'App Populated','Your app to be populated',$LogoUrl);
		    }
		    if($post_type=="app_welcomecall"){
		    	$comments="Welcome Call";
		    	$built_by_key ="appwelcome_call";
		    	$built_date   ="appwelcome_date";
		    	$LogEmailId=Logemails("Adminonly");
		    	$ToAddress=$LogEmailId["logemailid"];
		    	$LogoUrl = 'http://www.myappybusiness.com/images/email-logo.png';
		    	sendlogemail($ToAddress,'Welcome Call','Your customer Welcome call has been completed',$LogoUrl);
		    }
		    if($post_type=="app_training"){
		    	$comments="App Training";
		    	$built_by_key ="apptraining";
		    	$built_date   ="apptraining_date";
		    	$LogEmailId=Logemails("Adminonly");
		    	$ToAddress=$LogEmailId["logemailid"];
		    	$LogoUrl = 'http://www.myappybusiness.com/images/email-logo.png';
		    	sendlogemail($ToAddress,'App Training','Your app training has been completed',$LogoUrl);
		    }

			
			$insertIosBuiilt="INSERT INTO tbl_master_comments(master_id,comment,user_id,created_date,modified_date)values(:list_id,:comment,:user_id,:ios_created_date,:ios_modified_date)";
				$pepgetqryQry=$DBCONN->prepare($insertIosBuiilt);
				$pepgetqryQry->execute(array(':list_id'=>$list_id,':comment'=>$comments,':user_id'=>$user_id,':ios_created_date'=>$servicebydate,':ios_modified_date'=>$servicebydate));

		    //check the listed id or not
			$getServicesQry="select * from tbl_prospectinglist_services where list_id=:demoid and reset=:resetid";
			$pepServiceQry=$DBCONN->prepare($getServicesQry);
			$pepServiceQry->execute(array(':demoid'=>$list_id,":resetid"=>"0"));
			$getserviceCount=$pepServiceQry->rowcount();
		    $getserviceRow=$pepServiceQry->fetch();
			$service_by= stripslashes($getserviceRow[$built_by_key]);
			$servicebydate = $getserviceRow[$built_date];
			list($year,$month, $day) =explode("-",$servicebydate);
			$servicebydate=$day."/".$month."/".$year ;
            
            if($post_type=="ios built" || $post_type=="andorid built") {
                $appLogIosbyname = stripslashes($getserviceRow["ios_built_by"]);
				$appLogAndroidbyname = stripslashes($getserviceRow["android_built_by"]);
			    if($appLogIosbyname!="" && $appLogAndroidbyname=="") {
                	$Log_status=$returnLogstatus["iosbuilt"];
                } elseif($appLogIosbyname=="" && $appLogAndroidbyname!="") {
                   $Log_status=$returnLogstatus["androidbuilt"];
                } elseif($appLogIosbyname!="" && $appLogAndroidbyname!="") {
                	$Log_status	= $returnLogstatus["bothbuilt"];
                } else {
                	$Log_status="App Built";
                }
			}
                 
			if($post_type=="app_iostested" || $post_type=="app_andoridtested") {
                $appLogIosbyname = stripslashes($getserviceRow["ios_tested_by"]);
				$appLogAndroidbyname = stripslashes($getserviceRow["android_tested_by"]);
			    if($appLogIosbyname!="" && $appLogAndroidbyname=="") {
                	$Log_status=$returnLogstatus["iostest"];
                } elseif($appLogIosbyname=="" && $appLogAndroidbyname!="") {
                   $Log_status=$returnLogstatus["androidtest"];
                } elseif($appLogIosbyname!="" && $appLogAndroidbyname!="") {
                	$Log_status	= $returnLogstatus["bothtest"];
                } else {
                	$Log_status="Both to be built";
                }
			}

			if($post_type=="app_iossumbmit" || $post_type=="app_andoridsumbmit") {
                $appLogIosbyname = stripslashes($getserviceRow["ios_submitted_by"]);
				$appLogAndroidbyname = stripslashes($getserviceRow["android_submitted_by"]);
			    if($appLogIosbyname!="" && $appLogAndroidbyname=="") {
                	$Log_status=$returnLogstatus["iossubmit"];
                } elseif($appLogIosbyname=="" && $appLogAndroidbyname!="") {
                   $Log_status=$returnLogstatus["androidsubmit"];
                } elseif($appLogIosbyname!="" && $appLogAndroidbyname!="") {
                	$Log_status	= $returnLogstatus["bothsubmit"];
                } else {
                	$Log_status="Both to be tested";
                }
			}

			if($post_type=="app_ioslive" || $post_type=="app_andoridlive") {
                $appLogIosbyname = stripslashes($getserviceRow["ios_live_by"]);
				$appLogAndroidbyname = stripslashes($getserviceRow["android_live_by"]);
			    if($appLogIosbyname!="" && $appLogAndroidbyname=="") {
                	$Log_status=$returnLogstatus["ioslive"];
                } elseif($appLogIosbyname=="" && $appLogAndroidbyname!="") {
                   $Log_status=$returnLogstatus["androidlive"];
                } elseif($appLogIosbyname!="" && $appLogAndroidbyname!="") {
                	$Log_status	= $returnLogstatus["bothlive"];
                } else {
                	$Log_status="Both to be submitted";
                }
			}

            // update status demo start here table 
			$Updatedemotable="update tbl_demo set status=:logstatus where demo_id=:demoid";
            $pepupdatedemoqryQry=$DBCONN->prepare($Updatedemotable);
            $pepupdatedemoqryQry->execute(array(':demoid'=>$list_id,':logstatus'=>$Log_status));
			//update status demo End here table 

			$getQry="SELECT * FROM tbl_master_comments WHERE master_id =:master_id ORDER BY comment_id DESC";
			$prepgetQry=$DBCONN->prepare($getQry);
			$prepgetQry->execute(array(":master_id"=>$list_id));
			$count =$prepgetQry->rowCount();
			if($count>0){
			$i=1;

			while($getRow=$prepgetQry->fetch()){
			if($i%2==0)
			$display_color="#a5a5a5";
			else
			$display_color="#d2d1d1";
			$user_id=$getRow["user_id"];
			$get_Qry1="select * from  tbl_users where user_id=:user_id";
			$prepget1_Qry=$DBCONN->prepare($get_Qry1);
			$prepget1_Qry->execute(array(":user_id"=>$user_id));
			$get_Row1=$prepget1_Qry->fetch();
			$user_name=$get_Row1['username'];
			$user_date=date("d-m-Y", strtotime($getRow["created_date"]));
			$tble_comment.="<tr style='background-color:".$display_color.";height: 20px;'>";
			$tble_comment.="<td valign='top' align='left'>".$user_date."</td>";
			$tble_comment.="<td valign='top' align='left' style='width:505px;'>".stripslashes($getRow["comment"])."</td>";
			$tble_comment.="<td valign='top' align='left'>".$user_name."</td></tr>";
			$i++;
		    }
		  }
	        if($service_by!="" && $servicebydate!="" && $tble_comment!=""){
		   	 echo "Success##^^##". $service_by."##^^##".$servicebydate."##^^##".$tble_comment;
		   	 exit;
		   	}
		   	else {
			    echo "failed";
			    exit;
		    }
	    }

	    if($post_type=="dev_payment" || $post_type=="dem_payment" || $post_type=="cms_payment"){
 
            //check the listed id or not
			$getServicesQry="select * from tbl_prospectinglist_services where list_id=:demoid and reset=:resetid";
			$pepServiceQry=$DBCONN->prepare($getServicesQry);
			$pepServiceQry->execute(array(':demoid'=>$list_id,":resetid"=>"0"));
			$getserviceCount=$pepServiceQry->rowcount();
		    $getserviceRow=$pepServiceQry->fetch();
			$service_id = stripslashes($getserviceRow["services_id"]);
			
			if($getserviceCount==0){

				if($post_type=="dev_payment"){
				
	              $insertIosBuiilt="INSERT INTO tbl_prospectinglist_services(list_id,devpayment_made_by,devpayment_date,devpayment_amount)values(:list_id,:servicebyname,:servicebydate,:servicebyamount)";
				  $pepgetqryQry=$DBCONN->prepare($insertIosBuiilt);
			    }

			    if($post_type=="dem_payment"){
				
	              $insertIosBuiilt="INSERT INTO tbl_prospectinglist_services(list_id,dempayment_made_by	,dempayment_date,dempayment_amount)values(:list_id,:servicebyname,:servicebydate,:servicebyamount)";
				  $pepgetqryQry=$DBCONN->prepare($insertIosBuiilt);
			    }
			    if($post_type=="cms_payment"){
				
	              $insertIosBuiilt="INSERT INTO tbl_prospectinglist_services(list_id,cmspayment_made_by,cmspayment_date,cmspayment_amount)values(:list_id,:servicebyname,:servicebydate,:servicebyamount)";
				  $pepgetqryQry=$DBCONN->prepare($insertIosBuiilt);
			    }


			   
				$pepgetqryQry->execute(array(':list_id'=>$list_id,':servicebyname'=>$user_name,':servicebydate'=>$pay_date,':servicebyamount'=>$amount));

		    }else{

                if($post_type=="dev_payment"){
		          $UpdateIosbuilt="update tbl_prospectinglist_services set list_id=:list_id,  devpayment_made_by=:servicebyname, devpayment_date=:servicebydate, devpayment_amount=:servicebyamount where services_id=:sid";
		          $pepupdateqryQry=$DBCONN->prepare($UpdateIosbuilt);
		        }
                
                if($post_type=="dem_payment"){
		          $UpdateIosbuilt="update tbl_prospectinglist_services set list_id=:list_id,  dempayment_made_by=:servicebyname, dempayment_date=:servicebydate, dempayment_amount=:servicebyamount where services_id=:sid";
		          $pepupdateqryQry=$DBCONN->prepare($UpdateIosbuilt);
		        }

		        if($post_type=="cms_payment"){
		          $UpdateIosbuilt="update tbl_prospectinglist_services set list_id=:list_id,  cmspayment_made_by=:servicebyname, cmspayment_date=:servicebydate, cmspayment_amount=:servicebyamount where services_id=:sid";
		          $pepupdateqryQry=$DBCONN->prepare($UpdateIosbuilt);
		        }

		          $pepupdateqryQry->execute(array(':list_id'=>$list_id,':servicebyname'=>$user_name,':servicebydate'=>$pay_date,':servicebyamount'=>$amount,":sid"=>$service_id));
            }

		    if($post_type=="dev_payment"){
		    	$comments="Dev'T Payment";
		    	$built_by_key  ="devpayment_made_by";
		    	$built_date    ="devpayment_date";
		    	$built_amount  ="devpayment_amount";
		    	// $LogEmailId=Logemails("Adminonly");
		    	// $LogoUrl = 'http://www.myappybusiness.com/images/email-logo.png';
		    	// sendlogemail($LogEmailId,"Dev'T Payment",'Your Dev payment succeeded',$LogoUrl);
		    }

		    if($post_type=="dem_payment"){
		    	$comments="Dem Payment";
		    	$built_by_key  ="dempayment_made_by";
		    	$built_date    ="dempayment_date";
		    	$built_amount  ="dempayment_amount";
		    	// $LogEmailId=Logemails("Adminonly");
		    	// $LogoUrl = 'http://www.myappybusiness.com/images/email-logo.png';
		    	// sendlogemail($LogEmailId,"Dem Payment",'Your Dem Payment succeeded',$LogoUrl);
		    }
		     if($post_type=="cms_payment"){
		    	$comments="Cms Payment";
		    	$built_by_key  ="cmspayment_made_by";
		    	$built_date    ="cmspayment_date";
		    	$built_amount  ="cmspayment_amount";
		    	// $LogEmailId=Logemails("Adminonly");
		    	// $LogoUrl = 'http://www.myappybusiness.com/images/email-logo.png';
		    	// sendlogemail($LogEmailId,"Csm Payment",'Your Csm Payment succeeded',$LogoUrl);
		    }

		    $insertIosBuiilt="INSERT INTO tbl_master_comments(master_id,comment,user_id,created_date,modified_date)values(:list_id,:comment,:user_id,:ios_created_date,:ios_modified_date)";
				$pepgetqryQry=$DBCONN->prepare($insertIosBuiilt);
				$pepgetqryQry->execute(array(':list_id'=>$list_id,':comment'=>$comments,':user_id'=>$user_id,':ios_created_date'=>$servicebydate,':ios_modified_date'=>$servicebydate));

		    //check the listed id or not
			$getServicesQry="select * from tbl_prospectinglist_services where list_id=:demoid and reset=:resetid";
			$pepServiceQry=$DBCONN->prepare($getServicesQry);
			$pepServiceQry->execute(array(':demoid'=>$list_id,":resetid"=>"0"));
			$getserviceCount=$pepServiceQry->rowcount();
		    $getserviceRow=$pepServiceQry->fetch();
			$service_by= stripslashes($getserviceRow[$built_by_key]);
			$servicebydate = $getserviceRow[$built_date];
			
			list($year,$month, $day) =explode("-",$servicebydate);
			$servicebydate=$day."/".$month."/".$year ;
			$servicebyamount ="$".$getserviceRow[$built_amount];
			$getQry="SELECT * FROM tbl_master_comments WHERE master_id =:master_id ORDER BY comment_id DESC";
			$prepgetQry=$DBCONN->prepare($getQry);
			$prepgetQry->execute(array(":master_id"=>$list_id));
			$count =$prepgetQry->rowCount();
			if($count>0){
			$i=1;

			while($getRow=$prepgetQry->fetch()){
			if($i%2==0)
			$display_color="#a5a5a5";
			else
			$display_color="#d2d1d1";
			$user_id=$getRow["user_id"];
			$get_Qry1="select * from  tbl_users where user_id=:user_id";
			$prepget1_Qry=$DBCONN->prepare($get_Qry1);
			$prepget1_Qry->execute(array(":user_id"=>$user_id));
			$get_Row1=$prepget1_Qry->fetch();
			$user_name=$get_Row1['username'];
			$user_date=date("d-m-Y", strtotime($getRow["created_date"]));
			$tble_comment.="<tr style='background-color:".$display_color.";height: 20px;'>";
			$tble_comment.="<td valign='top' align='left'>".$user_date."</td>";
			$tble_comment.="<td valign='top' align='left' style='width:505px;'>".stripslashes($getRow["comment"])."</td>";
			$tble_comment.="<td valign='top' align='left'>".$user_name."</td></tr>";
			$i++;
		    }
		  }

	        if($service_by!="" && $servicebydate!="" && $servicebyamount!=""&& $tble_comment!==""){
		   	 echo "Success##^^##". $service_by."##^^##".$servicebydate."##^^##".$servicebyamount."##^^##".$tble_comment;
		   	 exit;
		   	}
		   	else {
			    echo "failed";
			    exit;
		    }
	    }


	    if($post_type=="market_service" || $post_type=="support_call"){
 
            //check the listed id or not
			$getServicesQry="select * from tbl_prospectinglist_services where list_id=:demoid and reset='0'";
			$pepServiceQry=$DBCONN->prepare($getServicesQry);
			$pepServiceQry->execute(array(':demoid'=>$list_id));
			$getserviceCount=$pepServiceQry->rowcount();
		    $getserviceRow=$pepServiceQry->fetch();
			$service_id = stripslashes($getserviceRow["services_id"]);
			
			if($getserviceCount==0){

				if($post_type=="market_service"){
				
	              $insertIosBuiilt="INSERT INTO tbl_prospectinglist_services(list_id,market_made_by,market_date)values(:list_id,:servicebyname,:servicebydate)";
				  $pepgetqryQry=$DBCONN->prepare($insertIosBuiilt);
			    }

			    if($post_type=="support_call"){
				
	              $insertIosBuiilt="INSERT INTO tbl_prospectinglist_services(list_id,support_made_by,support_date)values(:list_id,:servicebyname,:servicebydate)";
				  $pepgetqryQry=$DBCONN->prepare($insertIosBuiilt);
			    }
			   $pepgetqryQry->execute(array(':list_id'=>$list_id,':servicebyname'=>$user_name,':servicebydate'=>$servicebydate));

		    }else{

                if($post_type=="market_service"){
		          $UpdateIosbuilt="update tbl_prospectinglist_services set list_id=:list_id,  market_made_by=:servicebyname, market_date=:servicebydate where services_id=:sid";
		          $pepupdateqryQry=$DBCONN->prepare($UpdateIosbuilt);
		        }
                
                if($post_type=="support_call"){
		          $UpdateIosbuilt="update tbl_prospectinglist_services set list_id=:list_id,  support_made_by=:servicebyname, support_date=:servicebydate where services_id=:sid";
		          $pepupdateqryQry=$DBCONN->prepare($UpdateIosbuilt);
		        }

		         $pepupdateqryQry->execute(array(':list_id'=>$list_id,':servicebyname'=>$user_name,':servicebydate'=>$servicebydate,":sid"=>$service_id));
            }

		    if($post_type=="market_service"){
		    	$comments= $comment_txt;
		    	$built_by_key  ="market_made_by";
		    	$built_date    ="market_date";
		    	$LogEmailId=Logemails("Adminonly");
		    	$ToAddress=$LogEmailId["logemailid"];
		    	$LogoUrl = 'http://www.myappybusiness.com/images/email-logo.png';
		    	sendlogemail($ToAddress,"Marketing Service",'Your app Marketing Service is completed',$LogoUrl);
		    }

		    if($post_type=="support_call"){
		    	$comments=$comment_txt;
		    	$built_by_key  ="support_made_by";
		    	$built_date    ="support_date";
		    	$LogEmailId=Logemails("Adminonly");
		    	$ToAddress=$LogEmailId["logemailid"];
		    	$LogoUrl = 'http://www.myappybusiness.com/images/email-logo.png';
		    	sendlogemail($ToAddress,"Support Call",'Your app Support call is completed',$LogoUrl);
		    }

		    $insertIosBuiilt="INSERT INTO tbl_master_comments(master_id,comment,user_id,created_date,modified_date)values(:list_id,:comment,:user_id,:ios_created_date,:ios_modified_date)";
				$pepgetqryQry=$DBCONN->prepare($insertIosBuiilt);
				$pepgetqryQry->execute(array(':list_id'=>$list_id,':comment'=>$comments,':user_id'=>$user_id,':ios_created_date'=>$servicebydate,':ios_modified_date'=>$servicebydate));

		    //check the listed id or not
			$getServicesQry="select * from tbl_prospectinglist_services where list_id=:demoid and reset='0'";
			$pepServiceQry=$DBCONN->prepare($getServicesQry);
			$pepServiceQry->execute(array(':demoid'=>$list_id));
			$getserviceCount=$pepServiceQry->rowcount();
		    $getserviceRow=$pepServiceQry->fetch();
			$service_by= stripslashes($getserviceRow[$built_by_key]);
			$servicebydate = $getserviceRow[$built_date];
			
			list($year,$month, $day) =explode("-",$servicebydate);
			$servicebydate=$day."/".$month."/".$year ;
			$getQry="SELECT * FROM tbl_master_comments WHERE master_id =:master_id ORDER BY comment_id DESC";
			$prepgetQry=$DBCONN->prepare($getQry);
			$prepgetQry->execute(array(":master_id"=>$list_id));
			$count =$prepgetQry->rowCount();
			if($count>0){
			$i=1;

			while($getRow=$prepgetQry->fetch()){
			if($i%2==0)
			$display_color="#a5a5a5";
			else
			$display_color="#d2d1d1";
			$user_id=$getRow["user_id"];
			$get_Qry1="select * from  tbl_users where user_id=:user_id";
			$prepget1_Qry=$DBCONN->prepare($get_Qry1);
			$prepget1_Qry->execute(array(":user_id"=>$user_id));
			$get_Row1=$prepget1_Qry->fetch();
			$user_name=$get_Row1['username'];
			$user_date=date("d-m-Y", strtotime($getRow["created_date"]));
			$tble_comment.="<tr style='background-color:".$display_color.";height: 20px;'>";
			$tble_comment.="<td valign='top' align='left'>".$user_date."</td>";
			$tble_comment.="<td valign='top' align='left' style='width:505px;'>".stripslashes($getRow["comment"])."</td>";
			$tble_comment.="<td valign='top' align='left'>".$user_name."</td></tr>";
			$i++;
		    }
		  }
          if($service_by!="" && $servicebydate!="" && $tble_comment!=""){
		   	 echo "Success##^^##".$service_by."##^^##".$servicebydate."##^^##".$tble_comment;
		   	 exit;
		   	}
		   	else {
			    echo "failed";
			    exit;
		    }
	    }
	} else {

		$list_id=$_REQUEST["list_id"];
	    $user_name =$_SESSION['user_name'];
	    $user_id=$_SESSION['user_id'];
	    $servicebydate=date("Y-m-d");
	    if($list_id!=""){
	    	$getServicesQry="select * from tbl_prospectinglist_services where list_id=:demoid and reset=:resetid ORDER BY services_id DESC LIMIT 0,1";
			$pepServiceQry=$DBCONN->prepare($getServicesQry);
			$pepServiceQry->execute(array(':demoid'=>$list_id,":resetid"=>"0"));
			$getserviceCount=$pepServiceQry->rowcount();
		    $getserviceRow=$pepServiceQry->fetch();
		    $services_id=$getserviceRow["services_id"];

            if($services_id!=""){
				$UpdateIosbuilt="update tbl_prospectinglist_services set list_id=:list_id, reset=:resetid where services_id=:sid";
		          $pepupdateqryQry=$DBCONN->prepare($UpdateIosbuilt);
		        $pepupdateqryQry->execute(array(':list_id'=>$list_id,":resetid"=>"1",":sid"=>$services_id));

		        $insertIosBuiilt="INSERT INTO tbl_master_comments(master_id,comment,user_id,service_id,created_date,modified_date)values(:list_id,:comment,:user_id,:serviceid,:ios_created_date,:ios_modified_date)";
				$pepgetqryQry=$DBCONN->prepare($insertIosBuiilt);
		        $pepgetqryQry->execute(array(':list_id'=>$list_id,':comment'=>"Progress Table Reseted", ':user_id'=>$user_id, ':serviceid'=>$services_id, ':ios_created_date'=>$servicebydate, ':ios_modified_date'=> $servicebydate));

				$getQry="SELECT * FROM tbl_master_comments WHERE master_id =:master_id ORDER BY comment_id DESC";
				$prepgetQry=$DBCONN->prepare($getQry);
				$prepgetQry->execute(array(":master_id"=>$list_id));
				$count =$prepgetQry->rowCount();
				if($count>0){
					$i=1;
					while($getRow=$prepgetQry->fetch()) {
						if($i%2==0)
						$display_color="#a5a5a5";
						else
						$display_color="#d2d1d1";
						$user_id=$getRow["user_id"];
						$get_Qry1="select * from  tbl_users where user_id=:user_id";
						$prepget1_Qry=$DBCONN->prepare($get_Qry1);
						$prepget1_Qry->execute(array(":user_id"=>$user_id));
						$get_Row1=$prepget1_Qry->fetch();
						$user_name=$get_Row1['username'];
						$user_date=date("d-m-Y", strtotime($getRow["created_date"]));
						$comments=stripslashes(trim($getRow["comment"]));
						if($comments=="Progress Table Reseted"){
						 $comment=$comments."<a class='viewimg' service_id='".$services_id."'>View</a>";	
						} else {
						 $comment=$comments;
						}

						$tble_comment.="<tr style='background-color:".$display_color.";height: 20px;'>";
						$tble_comment.="<td valign='top' align='left'>".$user_date."</td>";
			            $tble_comment.="<td valign='top' align='left' style='width:505px;'>".$comment."</td>";
						$tble_comment.="<td valign='top' align='left'>".$user_name."</td></tr>";
						$i++;
			    	}
			    }
		    }
        }
        if($user_name!="" && $servicebydate!="" && $tble_comment!=""){
		   	echo "Success##^^##".$user_name."##^^##".$servicebydate."##^^##".$tble_comment;
		   	exit;
		   	}
		   	else {
			echo "failed";
			exit;
		}
    } 
}
?>