<?php
include("../includes/configure.php");
//include("../includes/session_check.php");
include("includes/cmm_functions.php");
$demoId=$_GET["demo_id"];
$appType=$_REQUEST["app_type"];
$msg=$_REQUEST["msg"];
$getDemoQry="select * from tbl_demo where demo_id='".$demoId."'";
$getDemoRes=mysql_query($getDemoQry);
$getDemoRow=mysql_fetch_array($getDemoRes);
$restaurant_name=stripslashes($getDemoRow["res1_name"]);
if($appType=="custom"){
	$appArr=getPrice(1);
	$intial=stripslashes($appArr["initial_price"]);
	$monthly=stripslashes($appArr["monthly_price"]);
}
else if($appType=="double"){
	$appArr=getPrice(2);
	$intial=stripslashes($appArr["initial_price"]);
	$monthly=stripslashes($appArr["monthly_price"]);
}
else if($appType=="triple"){
	$appArr=getPrice(3);
	$intial=stripslashes($appArr["initial_price"]);
	$monthly=stripslashes($appArr["monthly_price"]);
}
else if($appType=="staff"){
	$appArr=getPrice(4);
	$intial=stripslashes($appArr["initial_price"]);
	$monthly=stripslashes($appArr["monthly_price"]);
}
$pro=urldecode($_REQUEST["pro"]);
$prom=$_REQUEST["pro"];
$getproQry="select * from tbl_apps where app_title='".$pro."'";
$getproRes=mysql_query($getproQry);
$getproRow=mysql_fetch_array($getproRes);

if($msg=='success'){
	$message="<b>Congratulations!</b><br> your payment has processed successfully";
}
if($msg=='cancel'){
	$message="Your payment has been cancelled";
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>My Appy Restaurant</title>
	<link rel="shortcut icon" href="images/Fav.ico" type="image/ico">
	<link rel="stylesheet" href="stylesheet.css"/>
	<style>
		.app{
			border:3px solid #ffc000;
			float:left;
			width:675px;
			border-radius:30px;
			margin-right:25px;
			
		}
		.submit_btn{
			background:#558ED5;
			font-family:'kristen_itcregular';
			border:1px solid #558ED5;
			border-radius:3px;
			padding:5px 20px 5px 20px;
			cursor:pointer;
			color:white;
		}
		.app_heading{
			padding:5px;
			text-align:center;
			color:#ffc000;
			font-size:10px;
		}
		.app_content{
			padding:15px;
			font-size:10px;
		}
		.td_data{
			font-size:13px;
			font-family:'calibri';
		}
		.copyright {
			font-family: 'arial';
			color: grey;
			font-size: 11px;
			text-align: right;
			width: 790px;
			width:400px;
			margin-left:auto;
			
		}
	</style>
	<script type="text/javascript" src="js/jquery.js"></script>
	<script type="text/javascript" src="../source/jquery.fancybox.js?v=2.1.5"></script>
	<link rel="stylesheet" type="text/css" href="../source/jquery.fancybox.css?v=2.1.5" media="screen" />
	<script>
		function recurringPaypal(pay,demoid,intial,monthly,app_type,pro){
			//alert(pay+'-->'+demoid+'-->'+intial+'-->'+monthly+'-->'+app_type);
			if(document.getElementById("terms").checked==false)
			{
				alert("You must accept the terms of service");
				document.getElementById("terms").focus();
				return false;
			}
			else
				document.location='paypal/payflowprocess.php?acc='+pay+'&demo_id='+demoid+'&intial='+intial+'&monthly='+monthly+'&app_type='+app_type+'&pro='+pro;
		}
		function showMessage(){
			 $.fancybox({
			 'autoScale': true,
			 'transitionIn': 'elastic',
			 'transitionOut': 'elastic',
			 'speedIn': 500,
			 'speedOut': 300,
			 'autoDimensions': true,
			 'centerOnScroll': true,
			 'href' : '#message'
		  });
		}
	</script>
</head>

<body style="background:url('images/Picture1.png') no-repeat;margin:0px;font-family:'kristen_itcregular';color:#8eb4e3;background-size:100% 83%;"<?php if($message!="") echo " onload=\"showMessage()\""; ?>>
<form name="place_order_form" id="place_order_form" method="post">
	
	<div style="width:1024px;margin-left:auto;margin-right:auto;">
			<div style="font-size:32px;transform:rotate(-3deg);-ms-transform:rotate(-3deg);-webkit-transform:rotate(-3deg);">so <?php echo strtolower(stripslashes($getDemoRow["first_name"]));?>... <br>are all the details <span style="color:#ffc000;">correct</span>?</div>
			<div style="padding-top:30px;min-height:450px;">
				<div class="app">
					<div >
						<div style="width:45%;float:left;padding-left:20px;padding-top:15px;padding-bottom:20px;">
							<span style="font-size:18px;">your order:</span>
							<table cellspacing="0" cellpadding="5" border="0" width="100%">
								<tr>
									<td class="td_data">First Name:</td>
									<td class="td_data"><?php echo stripslashes($getDemoRow["first_name"]);?></td>
								</tr>
								<tr>
									<td class="td_data">Surname:</td>
									<td class="td_data"><?php echo stripslashes($getDemoRow["sur_name"]);?></td>
								</tr>
								<tr>
									<td class="td_data">Address 1:</td>
									<td class="td_data"><?php echo stripslashes($getDemoRow["res1_address"]);?></td>
								</tr>
								<tr>
									<td class="td_data">Address 2:</td>
									<td class="td_data"><?php echo stripslashes($getDemoRow["res2_address"]);?></td>
								</tr>
								<tr>
									<td class="td_data">City:</td>
									<td class="td_data"><?php echo stripslashes($getDemoRow["res1_suburb"]);?></td>
								</tr>
								<tr>
									<td class="td_data">State:</td>
									<td class="td_data"><?php echo stripslashes($getDemoRow["res1_state"]);?></td>
								</tr>
								<tr>
									<td class="td_data">Country:</td>
									<td class="td_data"><?php echo stripslashes($getDemoRow["res1_country"]);?></td>
								</tr>
								
							</table>
								<?php
								if($msg!='success'){
								?>
                         <div style="font-family:'calibri';font-size:13px;width: 140%;padding-top:7px;padding-left:6px;"><input type="checkbox" name="terms" id="terms" value="yes" style="margin:0px;">&nbsp;&nbsp;I have read and agree to the my appy restaurant <a href="http://myappyrestaurant.com/terms/"  target="_blank" style="color:#38659c;">terms of service</a></div>
								<?php
							}
								?>
						</div>
						<div style="width:45%;float:right;font-family:'calibri';padding-top:18px;">
							<p style="font-size:20px;color:#ffc000;margin:0px;font-weight:bolder;">Product: <?php echo $pro;?></p>
							<p style="font-size:13px;width:230px;margin:0px;padding-top:6px;"><?php echo stripslashes($getproRow["app_content"]);?></p>
							
						</div>
						
					</div>
					
				</div>
				
			</div>
			
					
	</div>

	<div style="min-height:200px;background:url(images/footer_bg2.jpg);">
				<div style="width:1024px;margin-left:auto;margin-right:auto;">
				<div style="width:680px;padding-top:40px;">
					
					<?php
						if($msg!='success'){
					?>
					<input type="button" value="< edit order" class="submit_btn" onclick="document.location='secondpay.php?demo_id=<?php echo $demoId;?>'" style="float:left;">
					<input type="button" value="place order >" class="submit_btn" onclick="recurringPaypal('paypal','<?php echo $demoId;?>','<?php echo $intial;?>','<?php echo $monthly;?>','<?php echo $appType;?>','<?php echo $prom;?>')" style="float:right;"> 
					<?php
					}
					else{
					?>
					<div style="margin-left:auto;margin-right:auto;width:265px;">
						<input type="button" value="go to my appy restaurant website >" class="submit_btn" onclick="document.location='../index.php'">
					</div>
					<?php
					}
					?>
					
				</div>
			</div>
				<div style="width:400px;margin-left:auto;padding-top:55px;;padding-bottom:10px;">
					<img src="images/applogo.png">
				</div>
				<div class="copyright">
					<div style="width:283px;">
						Copyright &copy; My Appy Restaurant 2013 All rights reserved
					<div>
				</div>
			</div> 	
	
	
	<div style="position:absolute; top:0px; right:0px;"><img src="images/sun.png" /></div>
	</form>
	<?php if($message!=""){?>
	<div style="width:400px;float:right;display:none;" id="message">
	<table>
		<tr style="background:white;height:40px;"id="error_message">
			<td style="color:rgb(142, 180, 227);font-size:25px;text-align:center;" colspan="2"><?php echo $message;?></td>
		</tr>
	</table>
	</div>
	<?php } ?>
</body>
</html>