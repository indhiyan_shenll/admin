<?php
include("../includes/configure.php");
include("paypal/Payflow-paypal-pro2.php");
include("includes/cmm_functions.php");
$demo_id=urldecode(base64_decode($_GET["demo_id"]));
$getPayQry="select * from tbl_paypal where demo_id='".$demo_id."' order by paypal_id desc limit 0,1";
$getPayRes=mysql_query($getPayQry);
$getPayRow=mysql_fetch_array($getPayRes);
$profile_id=$getPayRow["profile_id"];
$ack=$getPayRow["ack"];
$call_status=$getPayRow["call_status"];
if($ack!='Approved'||$call_status!='paid'){
	header("Location:".HTTP_ROOT_FOLDER);
	exit;
}
$getDemoQry="select * from tbl_demo where demo_id='".$demo_id."'";
$getDemoRes=mysql_query($getDemoQry);
$getDemoRow=mysql_fetch_array($getDemoRes);
$business_name=stripslashes($getDemoRow["business_name"]);
$first_name=stripslashes($getDemoRow["first_name"]);
$user_email=stripslashes($getDemoRow["email"]);
if(isset($_POST["hdn_post"])){
	try { 
			$txn = new PayflowTransaction();
			//these are provided by your payflow reseller
			$txn->PARTNER 	= PARTNER;
			$txn->USER 		= PAYPAL_USER;
			$txn->PWD		= PAYPAL_PWD;
			$txn->VENDOR 	= $txn->USER; //or your vendor name
			
			$txn->TRXTYPE 	= 'R'; //txn type: R for Recurring, S for Sale
			$txn->TENDER 	= 'C'; //sets to a cc transaction
			//$this->environment = 'live';
			$txn->debug = true; //uncomment to see debugging information
			$txn->ACTION 			= 'C';  // Specifies Add, Modify, Cancel, Reactivate, Inquiry, or Payment.
			$txn->ORIGPROFILEID		= $profile_id;
			$txn->process();
			extract($txn->response_arr);
			$insertCancelQry="insert into tbl_paypal(ack,profile_id,rphref,demo_id,call_status) values('$RESPMSG','$PROFILEID','$RPREF','$demo_id','cancelled')";
			$insertCancelRes=mysql_query($insertCancelQry);
			if($insertCancelRes){
				$getTempQry="select * from tbl_emailtemplates where title='Service cancelled'";
				$getTempRes=mysql_query($getTempQry);
				$getTempRow=mysql_fetch_array($getTempRes);
				$from_email=stripslashes($getTempRow["from_email"]).' <accounts@myappyrestaurant.com>';
				//$to_mail=stripslashes($getTempRow["to_mail"]).','.$user_email;
				$to_mail=stripslashes($getTempRow["to_mail"]).',cedric@myappyrestaurant.com';
				$subject=stripslashes($getTempRow["subject"]);
				$message=stripslashes($getTempRow["message"]);
				$message=str_replace('$firstname',$first_name,$message);
				sendEmail($from_email,$to_mail,$subject,$message);
				$msg="success";
			}
			/*echo '<pre>';
			echo "success: " . $txn->txn_successful;
			echo "response was: " . print_r( $txn->response_arr, true );*/
	}
	catch( TransactionDataException $tde ) {
		$message='bad transaction data ' . $tde->getMessage();
	}
	catch( InvalidCredentialsException $e ) {
		$message='Invalid credentials';
	}
	catch( InvalidResponseCodeException $irc ) {
		$message='bad response code: ' . $irc->getMessage();
	}
	catch( AVSException $avse ) {
		$message='AVS error: ' . $avse->getMessage();
	}
	catch( CVV2Exception $cvve ) {
		$message='CVV2 error: ' . $cvve->getMessage();
	}
	catch( FraudProtectionException $fpe ) {
		$message='Fraud Protection error: ' . $fpe->getMessage();
	}
	catch( Exception $e ) {
		$message=$e->getMessage();
	}
		
}
if($msg=="success"){
	$message="<b>Congratulations!</b><br> your recurring payment has cancelled successfully";
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>My Appy Restaurant</title>
	<link rel="shortcut icon" href="images/Fav.ico" type="image/ico">
	<link href="stylesheet.css" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" type="text/css" href="../source/jquery.fancybox.css?v=2.1.5" media="screen" />
	
	<style>
		.fancybox-skin {
			background: none repeat scroll 0 0 white ! important;
		}
		.fancybox-overlay {
			position: absolute;
			top: 0;
			left: 0;
			overflow: hidden;
			display: none;
			z-index: 8010;
			background: url("images/fancybox_overlay.png") repeat scroll 0 0 transparent;
		}
		.copyright {
			font-family: 'arial';
			color: grey;
			font-size: 11px;
			text-align: right;
			width: 790px;
			width:400px;
			margin-left:auto;
			
		}
		.cancel_btn{
			font-family:'kristen_itcregular';
			background:#ffc000;
			color:#568fd6;
			padding:5px 15px 5px 15px;
			border:1px solid #ffc000;
			border-radius:5px;
			cursor:pointer;
			font-size:18px;
		}
		
	</style>
	<script type="text/javascript" src="js/jquery.js"></script>
	<script type="text/javascript" src="../source/jquery.fancybox.js?v=2.1.5"></script>
	<script type="text/javascript">
		
		function showMessage(msg){
			 $.fancybox({
			 'autoScale': true,
			 'transitionIn': 'elastic',
			 'transitionOut': 'elastic',
			 'speedIn': 500,
			 'speedOut': 300,
			 'autoDimensions': true,
			 'centerOnScroll': true,
			'overlayShow':true,
			 'href' : '#message',
					 
		  });
		  
		}
		function CancelPayment(no){
			
				$.fancybox({
					'autoScale': true,
					'transitionIn': 'elastic',
					 'transitionOut': 'elastic',
					 'speedIn': 500,
					 'speedOut': 300,
					 'autoDimensions': true,
					 'centerOnScroll': true,
					'overlayShow':true,
					 'href' : '#loader',
					'closeBtn' : false,
				});
			
			document.cancel_payment_from.submit();
		}
	</script>
</head>

<body style="background:url('images/Picture1.png') no-repeat;margin:0px;font-family:'kristen_itcregular';color:#8eb4e3;background-size:100% 87%;"<?php if($message!="") echo " onload=\"showMessage('".$msg."')\""; ?>>
	<form name="cancel_payment_from" id="cancel_payment_from" method="post">
	<input type="hidden" name="hdn_post">
	<div style="min-height:130px;">
		<div style="width:54.5%;float:left;">
			<div style="width:500px;float:right;">
				<p style="font-size:32px;width:345px;margin-right:200px;transform:rotate(-3deg);-ms-transform:rotate(-3deg);-webkit-transform:rotate(-3deg);margin:0px;margin-top:10px;line-height:40px;">hi <?php echo strtolower($first_name);?>! <br>are you sure want to <span style="color:#ffc000;">cancel</span> access to your "Marketing Portal"...?</p>
			</div>
		</div>
		<?php if($message!=""){?>
			<div style="width:400px;float:right;display:none;" id="message">
			<table>
				<tr style="background:white;height:40px;">
					<td style="color:rgb(142, 180, 227);font-size:25px;text-align:center;" colspan="2"><?php echo $message;?></td>
				</tr>
			</table>
			</div>
		<?php } ?>
		<div style="width:400px;float:right;display:none;" id="loader">
			<table>
				<tr style="background:white;height:40px;">
					<td align="right">
						&nbsp;&nbsp;<img src="images/loading.gif">&nbsp;</td>
						<td style="color:rgb(142, 180, 227);font-size:25px;text-align:center;">cancelling your payment</td>
					
				</tr>
				<tr style="background:white;height:40px;">
					<td style="color:rgb(142, 180, 227);font-size:25px;text-align:center;" colspan="2">please wait...</td>
				</tr>
			</table>
		</div>
		<div style="width:45%;float:right;font-family:'Calibri';padding-top:40px;">
			
			<table cellspacing="0" cellpadding="0" style="border:3px solid #ffc000;border-radius:25px;width:420px;padding-left:15px;padding-top:10px;padding-bottom:10px;font-size:15px;">
				<tr>
					<td>Owner Name:</td>
					<td><?php echo $first_name." ".stripslashes($getDemoRow["sur_name"]);?></td>
				</tr>
				<tr>
					<td>Restaurant Name:</td>
					<td><?php echo $business_name;?></td>
				</tr>
				<tr>
					<td>App Name:</td>
					<td><?php echo stripslashes($getDemoRow["app_name"]);?></td>
				</tr>
				<tr>
					<td>iTunes Category:</td>
					<td><?php if($getDemoRow["itunes_category"]!='0'&& $getDemoRow["itunes_category"]!="") { echo stripslashes($getDemoRow["itunes_category"]); }?></td>
				</tr>
				<tr>
					<td>Status:</td>
					<td>Awaiting activation...</td>
				</tr>
			</table>
		</div>
	</div>
	<div style="min-height:500px;">
		<div style="width:54.5%;float:left;padding-top:10px;">
			<div style="width:200px;float:right;margin-top:290px;">
				
			</div>
			<div style="width:316px;float:right;">
				
			</div>
			
		</div>
		<div style="width:45%;float:right;padding-top:70px;">
			<div style="width:337px;font-size:18px;color:#ffc000;text-align:center;">
				<p style="width:337px;margin-left:auto;margin-right:auto;padding-bottom:10px;">if you are sure you wish to cancel access to your "marketing portal",<br>then click the button below</p>
				<div style="margin-left:auto;margin-right:auto;padding-bottom:70px;">
					<?php
						if($msg!='success'){
					?>
					<input type="button" value="cancel my access" class="cancel_btn" onclick="CancelPayment()">
					<?php
						}
						else{
					?>
					<input type="button" value="go to my appy restaurant website >" class="cancel_btn" onclick="document.location='../index.php'">
					<?php
						}
					?>
				</div>
			</div>
			<div>
				
				<div style="width:180px;height:45px;margin-left:auto;margin-right:auto;">
					<span style="float:right;padding-top:5px;"><img src="images/paypal_small.png"></span>
					<p style="text-align:right;width:100px;color:#ffc000;font-family:calibri;float:right">
						Managed by &nbsp;&nbsp;
					</p>
				</div>
			</div>
		</div>
	</div>
	<div style="min-height:200px;background:url(images/footer_bg2.jpg);">
		<div style="width:400px;margin-left:auto;padding-top:95px;padding-bottom:10px;">
			<img src="images/applogo.png">
		</div>
		<div class="copyright">
			<div style="width:283px;">
				Copyright &copy; My Appy Restaurant 2013 All rights reserved
			<div>
		</div>
	</div>
	<div style="position:absolute; top:0px; right:0px;"><img src="images/sun.png" /></div>
	
	<!-- <div style="position:absolute; bottom:30px;right:120px;"><img src="images/myappyrestaurants.png" style="width:280px;height:50px;"/></div> -->
</body>
</form>
</html>