<?php
include("../includes/configure.php");
include("../includes/session_check.php");
include("includes/resize-class.php");
include("includes/cmm_functions.php");
$testimonial_id=$_GET["tesimonial_id"];
$dbdatetime=date('Y-m-d H:i:s',strtotime('now'));
if($testimonial_id!=""){
	//$testimonial_id
	$getQry="select * from tbl_testimonial where testimonial_id=:testimonial_id";
	$prepgetQry=$DBCONN->prepare($getQry);
	$prepgetQry->execute(array(":testimonial_id"=>$testimonial_id));
	//$getRes=mysql_query($getQry);
	$getRow=$prepgetQry->fetch();
	$Logo=stripslashes($getRow["logo"]);
	$old_logo=stripslashes($getRow["logo"]);
	$Client_name=stripslashes($getRow["client_name"]);
	$Testimonial=stripslashes($getRow["testimonial"]);
	$res_name=stripslashes($getRow["res_name"]);

	$mode="Edit";
	$value="Update";
}
else{
	$mode="Add";
	$value="Create";
}
if(isset($_POST["client_name"])){
	$name=addslashes(trim($_POST["client_name"]));
    $Res_name=addslashes(trim($_POST["res_name"]));
	$testimonial=addslashes(trim($_POST["testimonial"]));
	if(!file_exists(LOGO_PATH)){
				mkdir(LOGO_PATH,0777);
		}
	$file_name=pathinfo($_FILES["logo"]["name"], PATHINFO_FILENAME);
	$file_extension=pathinfo($_FILES["logo"]["name"], PATHINFO_EXTENSION);
	$renamed_filename=$file_name.time().".".$file_extension;

		$file_path=LOGO_PATH.$renamed_filename;
		if($mode=="Edit"&&$file_name!=""){
			if(file_exists($old_logo)){
				unlink($old_logo);
			}
			move_uploaded_file($_FILES["logo"]["tmp_name"],$file_path);
			 //resizeImage($file_path,$file_path,141,110);
			//$file_path=UPLOAD_PATH.$renamed_filename;
		}
		else if($mode=="Add"){
			move_uploaded_file($_FILES["logo"]["tmp_name"],$file_path);
			 //resizeImage($file_path,$file_path,141,110);
			//$file_path=UPLOAD_PATH.$renamed_filename;
		}
		else{
			$file_path=$old_logo;
		}
	
    if($mode=="Edit"){
		//$testimonial     $name    $file_path   $Res_name   $testimonial_id
		$updateQry="update tbl_testimonial set testimonial=:testimonial,client_name=:client_name,logo=:logo,res_name=:res_name,modified_date=:modified_date where testimonial_id=:testimonial_id";
		$prepupdateRes=$DBCONN->prepare($updateQry);
		$updateRes=$prepupdateRes->execute(array(":testimonial"=>$testimonial,":client_name"=>$name,":logo"=>$file_path,":res_name"=>$Res_name,":modified_date"=>$dbdatetime,":testimonial_id"=>$testimonial_id));
		//$updateRes=mysql_query($updateQry);
		if($updateRes){
			header("Location:testimonial_list.php");
			exit;
		}
	}
	else{
		//$testimonial  $name   $file_path   $Res_name
		$insertQry="insert into tbl_testimonial(testimonial,client_name,logo,res_name,added_date,modified_date)values(:testimonial,:client_name,:logo,:res_name,:added_date,:modified_date)";
		$prepinsertQry=$DBCONN->prepare($insertQry);
		$insertRes=$prepinsertQry->execute(array(":testimonial"=>$testimonial,":client_name"=>$name,":logo"=>$file_path,":res_name"=>$Res_name,":added_date"=>$dbdatetime,":modified_date"=>$dbdatetime));
		//$insertRes=mysql_query($insertQry);
		if($insertRes){
			header("Location:testimonial_list.php");
			exit;
		}
	}
}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<title>MAR Pipeline System</title>
		<link rel="shortcut icon" href="images/Fav.ico" type="image/ico">
		<meta name="Generator" content="EditPlus">
		<meta name="Author" content="">
		<meta name="Keywords" content="">
		<meta name="Description" content="">
		<script type="text/javascript" src="js/jquery.js"></script>
		<script>
		var mode="<?php echo $mode;?>";
		</script>
		<script type="text/javascript" src="js/validate.js"></script>
		<style>
			body{
				margin:0;
				color:#D9D9D9;
				background:#455A68;
				font-family:arial;
			}
			.header{
				height:70px;
				background:#1C242A;
			}
			.content{
				background:#455A68;
				min-height:600px;
			}
			
			.form_actions{
				padding-top:15px;
				padding-left:5px;
				padding-bottom:30px;
			}
			.form_actions .add_btn{
				cursor:pointer;
				border-radius:0px;
				background:#0D0D0D;
				color:#D9D9D9;
				border-color:#D9D9D9;
				padding:5px 15px 5px 15px;
				font-family:arial;

			}
			.list_content{
				width:950px;
				margin-left:40px;
				
			}
			.tbl_header th{
				font-size:13px;
				border-bottom:1px solid #D9D9D9;
				text-align:left;
				font-family:arial;
			}
			.tbl-body{
				font-size:12px;
				font-family:arial;
			}
			a{
				color:black;
			}
			.inp_feild{
				border-radius:2px;
				border:none;
				width:77%;
			}
		</style>
	</head>
	<body>
		<div>
			<div style="margin-left:auto;margin-right:auto;">
				<div class="header">
					<span style="float:right;margin-right:20px;margin-top:5px;"><a href="logout.php" style="color:white;text-decoration:none;">Logout</a></span>
					<img src="images/myappyrestaurants.png" style="margin-top:10px;margin-left:40px;">
				</div>
				<div class="content">
					<div class="list_content">
						<h1 style="font-size:25px;padding-top:15px;padding-bottom:15px;margin:0px;">New Testimonial</h1>
						<form name="testimonial_form" id="testimonial_form" method="post" enctype="multipart/form-data">
						<input type="hidden" name="testimonial_form_id" id="testimonial_form_id" value="<?php echo $testimonial_id;?>">
						<table cellspacing="15" cellpadding="0" border="0" width="80%">
							<tr>
									<td valign="top"  style="width:90px;">
										 Logo<font color="red">*</font>:<br>
										 <span style="font-size:10px;">Size as 141 x 110</span>
									</td>
									<td>
										<input type="file" name="logo" id="logo" class="inp_feild" value="<?php echo $restaurant_name;?>">
										<?php if($old_logo!=""){ ?>
										<br>
											<img src="<?php echo $old_logo?>" style="width:75px;height:75px;">
										<?php }?>
									</td>
								</tr>
								<tr>
								<td  style="width:90px;">
									Name<font color="red">*</font>:
								</td>
								<td>
									<input type="text" name="client_name" id="client_name" class="inp_feild" value="<?php echo $Client_name;?>">
								</td>
							</tr>
							<tr>
								<td  style="width:90px;">
									Restaurant Name<font color="red">*</font>:
								</td>
								<td>
									<input type="text" name="res_name" id="res_name" class="inp_feild" value="<?php echo $res_name;?>">
								</td>
							</tr>

								<tr>
								<td valign="top" style="width:90px;">
									Testimonial<font color="red">*</font>:
								</td>
								<td>
									<textarea style="width:400px;height:158px;" id="testimonial" name="testimonial"><?php echo $Testimonial;?></textarea>
								</td>
							</tr>
							
							<tr>
							     <td>
									<div class="form_actions" style="text-align:left;">
										<input type="button" value="Back To Testimonial List" class="add_btn"  onclick="document.location='testimonial_list.php'">
									
								</td>
								<td>
									  <div class="form_actions" style="text-align:right;">
										<input type="button" value="<?php echo $value;?> Testimonial" class="add_btn" id="add_testimonial">
									</div>
								</td>
							</tr>
						</table>
						</form>
					</div>
					
				</div>
			</div>
		</div>
	</body>
</html>