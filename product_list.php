<?php
error_reporting(E_ALL);
include("../includes/configure.php");
include("../includes/session_check.php");
$del_id=$_GET["del_id"];
$App_id=$_GET["app_id"];
$sort=$_GET["sort"];
$field=$_GET["field"];
if($sort==""){
	$sort="asc";
}
if($field==""){
	$field="app_id";
}
if($field=="title"){
	$fieldname="app_title";
	$order=" order by ".$fieldname." ".$sort;
	if($sort=="asc"){
		$dsort="desc";
		$dpath="images/up.png";
	}
	else{
		$dsort="asc";
		$dpath="images/down.png";
	}
}

if($field=="page"){
	$fieldname="product_page";
	$order=" order by ".$fieldname." ".$sort;
	if($sort=="asc"){
		$rsort="desc";
		$rpath="images/up.png";
	}
	else{
		$rsort="asc";
		$rpath="images/down.png";
	}
	
}
if($del_id!=""){
	$deleteQry="delete from  tbl_apps where app_id='".$del_id."'";
	$deleteRes=mysql_query($deleteQry);
	if($deleteRes){
		header("Location:product_list.php");
		exit;
	}
}
include("includes/header.php");
?>
 <body>
		<div>
			<div style="margin-left:auto;margin-right:auto;">
				 <div class="content">
					<div class="list_content">
						<div class="form_actions" style="padding-bottom:50px;">
							<input type="button" value="Back To Admin Features" class="add_btn" onclick="document.location='admin_features.php'" style="float:left;">
							<input type="button" value="Add Product Page" class="add_btn" onclick="document.location='product.php'" style="float:right;">
						</div>
							<div class="header_div">
							<table cellspacing="0" cellpadding="0" width="100%" class="tbl_header">
								
								<tr>
									 <!-- onclick="document.location='statuslist.php?sort=<?php echo $dsort;?>&field=sno'" -->
                                    <th width="12%" onclick="document.location='product_list.php?sort=<?php echo $rsort;?>&field=page'" style="cursor:pointer">Product Page&nbsp;&nbsp;<?php if($rpath!=""){?><img src="<?php echo $rpath;?>" style="width:10px;height:10px;"><?php }?></th>
									<th width="46%">Description&nbsp;&nbsp;</th>
									<th width="12%">Country</th>
									<th width="12%">Currency</th>
									<th width="8%">Delete?</th>
								</tr>
							</table>
						</div>
						<div class="gap" ></div> 
						<table cellspacing="0" cellpadding="0" width="100%" class="tbl-body">
						<?php
						 $getQry="select * from  tbl_apps".$order;
							//exit;
							$getRes=mysql_query($getQry);
							$getCnt=mysql_num_rows($getRes);
							if($getCnt>0){
								$i=1;
								while($getRow=mysql_fetch_array($getRes)){
									
									if($i%2==1){
										$bgcolor="#a5a5a5";
									}
									else{
										$bgcolor="#d2d1d1";
									}
								 
									 
						?>
							<tr bgcolor="<?php echo $bgcolor;?>">
								<td width="12%"><?php echo stripslashes($getRow["product_page"]);?></td>
								<td width="46%"><?php echo readmore_view(stripslashes($getRow['app_content']),'75');?></td>
								<td width="12%"><?php echo stripslashes($getRow["country"]);?></td>
								<td width="12%"><?php echo array_search(stripslashes($getRow["currency"]), $currencyCodes);?></td>
								<td width="8%"><a href="product.php?app_id=<?php echo $getRow["app_id"];?>">Edit</a>&nbsp;&nbsp;/&nbsp;&nbsp;<a href="product_list.php?del_id=<?php echo $getRow["app_id"];?>" onclick="return confirm('Are you sure want to delete this product?');">Delete</a></td>
							</tr>
							
						<?php
							$i++;
								}
								?>
							
								<?php
							}
							else{
								echo "<tr bgcolor='#a5a5a5'><td colspan=\"3\"><center>No App(s) found.</center></td></tr>";
							}
						?>
						<tr><td height="10px"></td></tr>
							<tr>
								<td colspan="3">
								<div class="form_actions" style="text-align:left;position:relative;" >
								<input type="button" value="Back To Admin Features" class="add_btn" onclick="document.location='admin_features.php'">
								</td>
							</tr>
						</table>
					</div>
				</div>
			</div>
		</div>
<?php
include("includes/footer.php");
?>