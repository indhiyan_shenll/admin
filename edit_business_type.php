<?php
include("../includes/configure.php");
include("../includes/session_check.php");
$business_id=$_GET["business_id"];
$dbdatetime=date('Y-m-d H:i:s',strtotime('now'));
if($business_id!=""){
        $get_business_qry="select * from tbl_business_type where business_id=:business_id";
		$prepget_business_qry=$DBCONN->prepare($get_business_qry);
		$prepget_business_qry->execute(array(":business_id"=>$business_id));
        $get_business_Row=$prepget_business_qry->fetch();
        $business_type_db=$get_business_Row['business_type'];
		$mode="Edit";
		$value="Update";
}
else{
	$mode="Add";
	$value="Create";
}
if(isset($_POST["business_type"])){
	$business_type=addslashes(trim($_POST["business_type"]));
    if($mode=="Edit"){
		$updateQry="update tbl_business_type set business_type=:business_type,modified_date=:modified_date where business_id=:business_id";
		$prepupdateQry=$DBCONN->prepare($updateQry);
		$updateRes=$prepupdateQry->execute(array(":business_type"=>$business_type,":modified_date"=>$dbdatetime,":business_id"=>$business_id));
		if($updateRes){
	        header("Location:business_list.php");
			exit;
		}
	}
	else{
		$get_order="select max(display_order) as display_order from  tbl_business_type";
		$prepget_get_order=$DBCONN->prepare($get_order);
		$prepget_get_order->execute();
		$order_count =$prepget_get_order->rowCount();
		$get_display_Row=$prepget_get_order->fetch();
		$display_order=$get_display_Row['display_order'];
		$order_db_insert=$display_order+1;
		$insertqry="insert into tbl_business_type(business_type,display_order,added_date,modified_date)values(:business_type,:display_order,:added_date,:modified_date)";
	    $prepinsertqry=$DBCONN->prepare($insertqry);
	    $insertRes=$prepinsertqry->execute(array(":business_type"=>$business_type,":display_order"=>$order_db_insert,":added_date"=>$dbdatetime,":modified_date"=>$dbdatetime));
	    if($insertRes){
			header("Location:business_list.php");
			exit;
		}
	}
}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<title>MAR Pipeline System</title>
		<link rel="shortcut icon" href="images/Fav.ico" type="image/ico">
		<meta name="Generator" content="EditPlus">
		<meta name="Author" content="">
		<meta name="Keywords" content="">
		<meta name="Description" content="">
		<script type="text/javascript" src="js/jquery.js"></script>
		<script>
		var mode="<?php echo $mode;?>";
		</script>
		<script type="text/javascript" src="js/validate.js"></script>
		<link href="css/new.css" rel="stylesheet" type="text/css"/> 
	</head>
	<body>
		<div>
			<div style="margin-left:auto;margin-right:auto;">
				<div class="header">
					<span style="float:right;margin-right:20px;margin-top:5px;"><a href="logout.php" style="color:white;text-decoration:none;">Logout</a></span>
					<img src="images/myappyrestaurants.png" style="margin-top:10px;margin-left:40px;">
				</div>
				<div class="content">
					<div class="list_content">
						<h1 style="font-size:25px;padding-top:15px;padding-bottom:15px;margin:0px;">New Business Type</h1>
						<form name="business_form" id="business_form" method="post">
						<input type="hidden" name="hdn_business_id" id="hdn_business_id" value="<?php echo $business_id;?>">
						<table cellspacing="15" cellpadding="0" border="0" width="70%">
							<tr>
								<td style="width:138px;">
									Business Type<font color="red">*</font>: 
								</td>
								<td>
									<input type="text" name="business_type" id="business_type" class="inp_feild" value="<?php echo $business_type_db;?>">
								</td>
							</tr>
							
							<tr>
							     <td>
									<div class="form_actions" style="text-align:left;">
										<input type="button" value="Back To Business Type List" class="add_btn"  onclick="document.location='business_list.php'">
									
								</td>
								<td>
									  <div class="form_actions" style="text-align:right;">
										<input type="button" value="<?php echo $value;?> Business Type" class="add_btn" id="add_business_type">
									</div>
								</td>
							</tr>
						</table>
						</form>
					</div>
					
				</div>
			</div>
		</div>
	</body>
</html>