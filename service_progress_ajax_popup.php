<?php
include("../includes/configure.php");
include("includes/cmm_functions.php");
include("../includes/session_check.php");
include("email_function.php");

if(isset($_REQUEST['post_type']) && $_REQUEST['post_type'] !=''){
	if($_REQUEST['post_type']=="service_popup"){
		$serviceid =$_REQUEST['serviceid'];
		$getServicesQry="select * from tbl_prospectinglist_services where services_id=:serviceid";
			$pepServiceQry=$DBCONN->prepare($getServicesQry);
			$pepServiceQry->execute(array(":serviceid"=>$serviceid));
			$getserviceCount=$pepServiceQry->rowcount();
		    $getserviceRow=$pepServiceQry->fetch();
		
		$service_id          = stripslashes($getserviceRow["services_id"]);
	    $app_design_log      = stripslashes($getserviceRow["app_design_log"]);
	    $app_design_date     = stripslashes($getserviceRow["app_design_date"]);
	    list($year,$month, $day) =explode("-",$app_design_date);
	    $app_design_date     =$day."/".$month."/".$year ;
	    if(($app_design_log=="" || $app_design_date=="00/00/0000" || $app_design_date=="" )){
	        $app_design_date="";
	    }
		   
        $ios_built_by        = stripslashes($getserviceRow["ios_built_by"]);
	    $ios_built_date      = stripslashes($getserviceRow["ios_built_date"]);
	    list($year,$month, $day) =explode("-",$ios_built_date);
	    $ios_built_date     =$day."/".$month."/".$year ;

	    if($ios_built_by=="" || $ios_built_date=="00/00/0000" || $ios_built_date=="" ){
	        $ios_built_date="";
	    }
	    $android_built_by    = stripslashes($getserviceRow["android_built_by"]);
	    $android_built_date  = stripslashes($getserviceRow["android_built_date"]);
	    list($year,$month, $day) =explode("-",$android_built_date);
	    $android_built_date     =$day."/".$month."/".$year ;

	    if($android_built_by=="" || $android_built_date=="00/00/0000" || $android_built_date=="" ){
	        $android_built_date="";
	    }


	    $devpayment_amount   = stripslashes($getserviceRow["devpayment_amount"]);

	    if($devpayment_amount!="")
	    	$devpayment_amount1="$".$devpayment_amount;

	    $devpayment_date     = stripslashes($getserviceRow["devpayment_date"]);
	    list($year,$month, $day) =explode("-",$devpayment_date);
	    $devpayment_date     =$day."/".$month."/".$year ;
	    $devpayment_made_by  = stripslashes($getserviceRow["devpayment_made_by"]);
	   
	    if($devpayment_amount=="" || $devpayment_date=="00/00/0000" || $devpayment_made_by==""){
	        $devpayment_date1="";
		} 

	    $ios_tested_by       = stripslashes($getserviceRow["ios_tested_by"]);
	    $ios_tested_date     = stripslashes($getserviceRow["ios_tested_date"]);
	    list($year,$month, $day) =explode("-",$ios_tested_date);
	    $ios_tested_date     =$day."/".$month."/".$year ;

	    if($ios_tested_by=="" || $ios_tested_date=="00/00/0000" || $ios_tested_date==""){
	        $ios_tested_date="";
	    } 
		

	    $android_tested_by   = stripslashes($getserviceRow["android_tested_by"]);
	    $android_tested_date = stripslashes($getserviceRow["android_tested_date"]);
	    list($year,$month, $day) =explode("-",$android_tested_date);
	    $android_tested_date     =$day."/".$month."/".$year ;
	    if($android_tested_by=="" || $android_tested_date=="00/00/0000" || $android_tested_date==""){
	        $android_tested_date="";
	    } 

	    $app_populated_by    = stripslashes($getserviceRow["app_populated_by"]);
	    $app_populated_date  = stripslashes($getserviceRow["app_populated_date"]);
	    list($year,$month, $day) =explode("-",$app_populated_date);
	    $app_populated_date     =$day."/".$month."/".$year ;
	    
	    if($app_populated_by=="" || $app_populated_date=="00/00/0000" || $app_populated_date==""){
	        $app_populated_date="";
	        $app_populated_disabled="";
	    } 

	    $ios_submitted_by    = stripslashes($getserviceRow["ios_submitted_by"]);
	    $ios_submitted_date  = stripslashes($getserviceRow["ios_submitted_date"]);
	    list($year,$month, $day) =explode("-",$ios_submitted_date);
	    $ios_submitted_date     =$day."/".$month."/".$year ;
	    
	    if($ios_submitted_by=="" || $ios_submitted_date=="00/00/0000" || $ios_submitted_date==""){
	        $ios_submitted_date="";
	    } 

	    $android_submitted_by = stripslashes($getserviceRow["android_submitted_by"]);
	    $android_submitted_date  = stripslashes($getserviceRow["android_submitted_date"]);
	    list($year,$month, $day) =explode("-",$android_submitted_date);
	    $android_submitted_date     =$day."/".$month."/".$year ;
	    

	    if($android_submitted_by=="" || $android_submitted_date=="00/00/0000" || $android_submitted_date==""){
	        $android_submitted_date="";
	    } 

	    $dempayment_amount    = stripslashes($getserviceRow["dempayment_amount"]);
	    $dempayment_date      = stripslashes($getserviceRow["dempayment_date"]);
	    list($year,$month, $day) =explode("-",$dempayment_date);
	    $dempayment_date     =$day."/".$month."/".$year ;
	    $dempayment_made_by   = stripslashes($getserviceRow["dempayment_made_by"]);
        
        if($dempayment_amount!="")
        	$dempayment_amount1="$".$dempayment_amount;

	    if($dempayment_amount=="" || $dempayment_date=="00/00/0000" || $dempayment_date==""){
	    	$dempayment_amount1="";
	        $dempayment_date="";
	        $dempayment_disabled="";
	    } 

	    $ios_live_by          = stripslashes($getserviceRow["ios_live_by"]);
	    $ios_live_date        = stripslashes($getserviceRow["ios_live_date"]);
	    list($year,$month, $day) =explode("-",$ios_live_date);
	    $ios_live_date     =$day."/".$month."/".$year ;
	     
	    if($ios_live_by=="" || $ios_live_date=="00/00/0000" || $ios_live_date==""){
	        $ios_live_date="";
	    }

	    $android_live_by      = stripslashes($getserviceRow["android_live_by"]);
	    $android_live_date    = stripslashes($getserviceRow["android_live_date"]);
	    list($year,$month, $day) =explode("-",$android_live_date);
	    $android_live_date     =$day."/".$month."/".$year ;

	    if($android_live_by=="" || $android_live_date=="00/00/0000" || $android_live_date==""){
	        $android_live_date="";
	    } 

	    $appwelcome_call      = stripslashes($getserviceRow["appwelcome_call"]);
	    $appwelcome_date      = stripslashes($getserviceRow["appwelcome_date"]);
	    list($year,$month, $day) =explode("-",$appwelcome_date);
	    $appwelcome_date     =$day."/".$month."/".$year ;
	    
	    if($appwelcome_call=="" || $appwelcome_date=="00/00/0000" || $appwelcome_date==""){
	        $appwelcome_date="";
	        $welcome_btn_disabled="";
	    } 
	    $apptraining          = stripslashes($getserviceRow["apptraining"]);
	    $apptraining_date     = stripslashes($getserviceRow["apptraining_date"]);
	    list($year,$month, $day) =explode("-",$apptraining_date);
	    $apptraining_date     =$day."/".$month."/".$year ;
	    
	    if($apptraining=="" || $apptraining_date=="00/00/0000" || $apptraining_date==""){
	        $apptraining_date="";
	        $training_disabled="";
	    }

	    $cmspayment_amount    = stripslashes($getserviceRow["cmspayment_amount"]);
	    $cmspayment_date      = stripslashes($getserviceRow["cmspayment_date"]);
	    list($year,$month, $day) =explode("-",$cmspayment_date);
	    $cmspayment_date     =$day."/".$month."/".$year ;
	    $cmspayment_made_by   = stripslashes($getserviceRow["cmspayment_made_by"]);
	    if($cmspayment_amount!=""){
	    	$cmspayment_amount1="$".$cmspayment_amount;
	    }
	    if($cmspayment_amount=="" || $cmspayment_date=="00/00/0000" || $cmspayment_date=="" || $cmspayment_made_by=="" ){
	        $cmspayment_date="";
	        $cmspayment_disabled="";
	        $cmspayment_amount1="";
	    } 

	    $market_made_by       = stripslashes($getserviceRow["market_made_by"]);
	    $market_date          = stripslashes($getserviceRow["market_date"]);
	    list($year,$month, $day) =explode("-",$market_date);
	    $market_date      =$day."/".$month."/".$year ;

	    if($market_date=="00/00/0000" || $market_date=="" || $market_date=="//"){
	        $market_date="";
	    } 


	    $support_made_by      = stripslashes($getserviceRow["support_made_by"]);
	    $support_date         = stripslashes($getserviceRow["support_date"]);
	    list($year,$month, $day) =explode("-",$support_date);
	    $support_date     =$day."/".$month."/".$year ;
	    if($support_date=="00/00/0000" || $support_date=="" || $support_date=="//"){
	        $support_date="";
	    }
		$displaypopup='<table>
						    <tr> 
                               <td colspan="2" nowrap>
			                        <h3><u>SERVICE DELIVERY PROGRESS</u></h3>
						        </td>
						    </tr>
						     <tr>
						        <td style="max-width:600px !important;">
						            <table class="content_table" style="border-collapse: collapse;border: 1px solid white;" cellpadding="3">
						                <tr>
					                        <td style="min-width: 188px;"></td>
					                        <td style="min-width: 150px;">IPHONE APP</td>
					                        <td style="min-width: 95px;"></td>
					                        <td style="min-width: 85px;"></td>
					                        <td style="min-width: 20px;"></td>
					                        <td class="border_right"></td>
					                        <td style="min-width: 150px; padding-left: 15px;">ANDROID APP</td>
					                        <td style="min-width: 95px;"></td>
					                        <td style="min-width: 85px;"></td>
					                        <td style="min-width: 20px;"></td>
						                </tr>
						                <tr>
						                    <td>APP DESIGNED:</td>
						                    <td class="" id=""></td>
						                    <td></td>
						                    <td></td>
						                    <td class="padding_left"></td>
						                    <td></td>
						                    <td class="app_designname padding" id="app_designname">'.$app_design_log.'</td>
						                    <td></td>
						                    
						                    <td class="app_designdate" id="app_designdate">'.$app_design_date.'</td>
						                    <td style="float: right;">
						                       
						                    </td>
							            </tr>
						                <tr>
					                        <td>APP BUILT:</td>
					                        <td class="iosbuiltname" id="iosbuiltname">'.$ios_built_by.'</td>
					                        <td></td>
					                        <td class="iosbuiltdate" id="iosbuiltdate">'.$ios_built_date.'</td>
					                        <td class="padding_left">
					            
					                        </td>
					                        <td class="border_right"></td>
					                        <td class="andoridbuiltname padding" id="andoridbuiltname">'.$android_built_by.'</td>
					                        <td></td>
					                        <td class="andoridbuiltdate" id="andoridbuiltdate">'.$android_built_date.'</td>
					                        <td style="float: right;">
					                           
					                        </td>
						                </tr>
						                <tr>
						                    <td>DEV T PAYMENT:</td>
						                    <td></td>
						                    <td></td>
						                    <td></td>
						                    <td class="padding_left"></td>
						                    <td></td>
						                    <td class="app_devpaymentname padding" id="app_devpaymentname">'.$devpayment_made_by.'</td>
						                   
						                    <td class="app_devpaymentamount" id="app_devpaymentamount">'.$devpayment_amount1.'</td>
						                    <td class="app_devpaymentdate" id="app_devpaymentdate">'.$devpayment_date.'</td>
						                    <td style="float: right;">
						                       
						                      
						                    </td> 
						                </tr>
						                <tr>
					                        <td>APP TESTED:</td>
					                        <td class="iostestbyname" id="iostestbyname">'.$ios_tested_by.'</td>
					                        <td></td>
					                        <td class="iostestbydate" id="iostestbydate">'.$ios_tested_date.'</td>
					                        <td class="padding_left">
					                           
					                        </td>
					                        <td class="border_right"></td>
					                        <td class="andoridtestbyname padding" id="andoridtestbyname">'.$android_tested_by.'</td>
					                        <td ></td>
					                        <td class="andoridtestbydate" id="andoridtestbydate">'.$android_tested_date.'</td>
					                        <td style="float: right;">
					                          
					                        </td>
						                </tr>
						                <tr>
						                    <td>APP POPULATED:</td>
						                    <td></td>
						                    <td></td>
						                    <td></td>
						                    <td class="padding_left">
						                    </td>
						                    <td></td>
						                    <td class="app_populatedname padding" id="app_populatedname">'.$app_populated_by.'</td>
						                    <td></td>
						                    <td class="app_populateddate" id="app_populateddate">'.$app_populated_date.'</td>
						                    <td style="float: right;">
						                       
						                    </td>
						                </tr> 
						                <tr>
						                    <td>DEM PAYMENT:</td>
						                    <td></td>
						                    <td></td>
						                    <td></td>
						                    <td class="padding_left">
						                        
						                    </td>
						                    <td ></td>
						                    <td class="app_dempaymentname padding" id="app_dempaymentname">'.$dempayment_made_by.'</td>
						   
						                    <td class="app_dempaymentamount" id="app_dempaymentamount">'.$dempayment_amount1.'</td>
						                    <td class="app_dempaymentdate" id="app_dempaymentdate">'.$dempayment_date.'</td>
						                    <td style="float: right;">
						                       
						                    </td>
						                </tr> 
						                <tr>
						                    <td>APP SUBMITTED:</td>
						                    <td class="iossubmitbyname" id="iossubmitbyname" >'.$ios_submitted_by.'</td>
						                    <td></td>
						                    <td class="iossubmitbydate" id="iossubmitbydate" >'.$ios_submitted_date.'</td>
						                    <td class="padding_left">
						                     
						                    </td>
						                    <td class="border_right"></td>
						                    <td class="andoridsubmitbyname padding" id="andoridsubmitbyname">'.$android_submitted_by.'</td>
						                    <td ></td>
						                    <td class="andoridsubmitbydate" id="andoridsubmitbydate">'.$android_submitted_date.'</td>
						                    <td style="float: right;">
						                      
						                    </td>
						                </tr>
						                <tr>
						                    <td>APP LIVE:</td>
						                    <td class="ioslivebyname" id="ioslivebyname" >'.$ios_live_by.'</td>
						                    <td></td>
						                    <td class="ioslivebydate" id="ioslivebydate">'.$ios_live_date.'</td>
						                    <td class="padding_left">
						                       
						                    </td>
						                    <td class="border_right"></td>
						                    <td class="andoridlivebyname padding" id="andoridlivebyname">'.$android_live_by.'</td>
						                    <td ></td>
						                    <td class="andoridlivebydate" id="andoridlivebydate">'.$android_live_date.'</td>
						                    <td style="float: right;">
						                      
						                    </td>
						                </tr> 
						                <tr> 
						                    <td>APP WELCOME CALL:</td>
						                    <td></td>
						                    <td></td>
						                    <td></td>
						                    <td class="padding_left">
						                    </td>
						                    <td ></td>
						                    <td class="app_welcallname padding" id="app_welcallname">'.$appwelcome_call.'</td>
						                    <td></td>
						                    <td class="app_welcalldate" id="app_welcalldate">'.$appwelcome_date.'</td>
						                    <td style="float: right;">
						                        
						                    </td>
						                </tr>
						                <tr>
						                    <td>APP TRAINING:</td>
						                    <td></td>
						                    <td></td>
						                    <td></td>
						                    <td class="padding_left">
						                    </td>
						                    <td ></td>
						                    <td class="app_trainingname padding" id="app_trainingname">'.$apptraining.'</td>
						                    <td></td>
						                    <td calss="apptrainingdate" id="apptrainingdate">'.$apptraining_date.'</td>
						                    <td style="float: right;">
						                      
						                    </td>
						                </tr>
						                <tr>
						                    <td>CSM PAYMENT:</td>
						                    <td></td>
						                    <td></td>
						                    <td></td>
						                    <td class="padding_left">
						                        
						                    </td>
						                    <td ></td>
						                    <td class="app_cmspaymentname padding" id="app_cmspaymentname">'.$cmspayment_made_by.'</td>
						                    
						                    <td class="app_cmspaymentamount" id="app_cmspaymentamount">'.$cmspayment_amount1.'</td>
						                    <td class="app_cmspaymentdate" id="app_cmspaymentdate">'.$cmspayment_date.'</td>
						                    <td style="float: right;">
						                       
						                    </td>
						                </tr>
						                <tr> 
						                    <td>MARKETING SERVICES:</td>
						                    <td></td>
						                    <td></td>
						                    <td></td>
						                    <td class="padding_left">
						                      
						                    </td>
						                    <td ></td>
						                    <td class="app_marketingname padding" id="app_marketingname">'.$market_made_by.'</td>
						                    <td ></td>
						                    <td class="app_marketingdate" id="app_marketingdate">'.$market_date.'</td> 
						                    <td style="float: right;">
						                       
						                    </td>
						                </tr>
						                <tr>
						                    <td>SUPPORT CALLS:</td>
						                    <td></td>
						                    <td></td>
						                    <td></td>
						                    <td class="padding_left">
						                        
						                    </td>   
						                    <td ></td>
						                    <td class="app_supportname padding" id="app_supportname">'.$support_made_by.'</td> 
						                    <td ></td>
						                    <td class="app_supportdate" id="app_supportdate">'.$support_date.'</td>
						                    <td style="float: right;">
						                       
						                    </td>
						                </tr>
						            </table>
						        </td>
						    </tr>
						</table>';
		if($displaypopup!=""){
		   	echo "Success##^^##".$displaypopup;
		   	exit;
		   	}
		   	else {
			echo "failed";
			exit;
		}
    } else {
    	echo "failed";
		exit;
    }
}
?>