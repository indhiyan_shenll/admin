<?php
$simulator='1';
$style="";
$parent_directory=basename(dirname($_SERVER["PHP_SELF"]));
$filename=basename($_SERVER["PHP_SELF"]);

include("../includes/configure.php");
include("includes/cmm_functions.php");
include("../includes/session_check.php");
include("email_function.php");

$id=$_SESSION['user_id'];
$dbdatetime=date('Y-m-d H:i:s',strtotime('now'));
$feat="demo_page";
$typeandfeature=checklogin($id,$feat);
$usrArr=explode("*",$typeandfeature);
$user_type=$usrArr[0];
$feature=$usrArr[1];
$dowfeature=$usrArr[2];
$trialfeature=$usrArr[4];
$paymentf=$usrArr[3];
$demofeature=$usrArr[6];
// print_r($_POST);exit;
if ($demofeature!="yes") {
	header("Location:noauthorised.php");
	exit;
} else {
    //get user access service button start here
    if ($user_type=="Admin") {
        $btnmenu=array('iosbuilt','andoridbuilt','iossubmit','andoridsubmit','ioslive','andoridlive','iostested','andoridtested','appdesigned','apppopulated','welcomecall','apptraining','marketservice','supportcall','devpayment','cmspayment','dempayment');
    } elseif ($user_type=="Affiliate") {
        $btnmenu=array('appdesigned','iostested','andoridtested','apppopulated','welcomecall','apptraining','marketservice','supportcall');
    } elseif ($user_type=="Developer") {
        $btnmenu=array('iosbuilt','andoridbuilt','iossubmit','andoridsubmit','ioslive','andoridlive');
    }
    elseif ($user_type=="CSM" || $user_type=="csm" ) {
        $btnmenu=array('welcomecall','apptraining','marketservice','supportcall');
    }
    elseif ($user_type=="DEM" || $user_type=="dem") {
        $btnmenu=array('appdesigned','iostested','andoridtested','apppopulated');
    }
    //get user access service button end here

    //get user access prospectinglist_services start here
    $demoId=$_GET["demo_id"];
    include("service_delivery.php");
    //get user access prospectinglist_services end here 

    if($demoId!="") {
        $getDemoQry="select * from tbl_demo where demo_id=:demoid";
        $pepDemoQry=$DBCONN->prepare($getDemoQry);
        $pepDemoQry->execute(array(':demoid'=>$demoId));
        $getDemoRow=$pepDemoQry->fetch();

        // Stimulator Info Section
        $business_name=stripslashes($getDemoRow["business_name"]);
        $business_type=stripslashes($getDemoRow['business_type']);
        $email=stripslashes($getDemoRow["email"]);
        $product_page_val=stripslashes($getDemoRow["product_page"]);
        $promo_code =stripslashes($getDemoRow["promo_code"]);

        $simulator=$getDemoRow["simulator_feature"];
        if ($simulator=="1")
        	$style="";	
        else
        	$style="display:none";

        $restaurant_logo = stripslashes($getDemoRow["restaurant_logo"]);
        $theme=stripslashes($getDemoRow["theme"]);
        $created_user_id=$getDemoRow['user_id'];
	
        if ($created_user_id=="" || $created_user_id=="0")	
            $created_user_id=$_SESSION['user_id'];	

            $geUserQry="select * from tbl_users where user_id=:user_id";
            $prepgetuserqry=$DBCONN->prepare($geUserQry);
            $prepgetuserqry->execute(array(":user_id"=>$created_user_id));
            $geUserRow=$prepgetuserqry->fetch();
            $created_user=stripslashes($geUserRow["username"]);

            // CONTACT INFO
            $first_name=stripslashes($getDemoRow["first_name"]);
            $res1_address=stripslashes($getDemoRow["res1_address"]);
            $res1_suburb=stripslashes($getDemoRow["res1_suburb"]);
            $res1_state=stripslashes($getDemoRow["res1_state"]);
            $res1_postcode=stripslashes($getDemoRow["res1_postcode"]);
            $res1_country=stripslashes($getDemoRow["res1_country"]);
            $res1_workphone=stripslashes($getDemoRow["res1_workphone"]);
            $mobile_phone=stripslashes($getDemoRow["mobile_phone"]);

            // SALES INFO
            $callstatus=stripslashes($getDemoRow["call_status"]);

        $demo_date="";
        if ($getDemoRow["demo_date"]!="" && $getDemoRow["demo_date"]!="0000-00-00")
	       $demo_date=date('d-m-Y',strtotime(stripslashes($getDemoRow["demo_date"])));

            $demo_time="";
        if ($getDemoRow["demo_time"]!="" && $getDemoRow["demo_time"]!="00:00:00")
	       $demo_time=date('g:i A',strtotime(stripslashes($getDemoRow["demo_time"])));	

        // SYSTEM INFO
        $CreatedDate = date('d-m-Y',strtotime($getDemoRow["added_date"]));

        // DEVELOPMENT INFO
        $status=stripslashes($getDemoRow["status"]);
        $android_status=stripslashes($getDemoRow["android_status"]);

        $iphonedate="";   
        if ($getDemoRow["iphone_date"]!="" && $getDemoRow["iphone_date"]!="0000-00-00")
	       $iphonedate=date('d-m-Y',strtotime(stripslashes($getDemoRow["iphone_date"])));
           $androiddate="";
        if ($getDemoRow["android_date"]!="" && $getDemoRow["android_date"]!="0000-00-00")
	       $androiddate=date('d-m-Y',strtotime(stripslashes($getDemoRow["android_date"])));

        $developer_id=stripslashes($getDemoRow["developer_id"]);
        $business_logo = stripslashes($getDemoRow["business_logo"]);
        $app_name=stripslashes($getDemoRow["app_name"]);
        $default_langauage=stripslashes($getDemoRow["default_langauage"]);
        $itunes_category=stripslashes($getDemoRow["itunes_category"]);
        $app_modifications =stripslashes($getDemoRow["app_modifications"]);
        $background_image =stripslashes($getDemoRow["background_image"]);
        $background_image2 =stripslashes($getDemoRow["background_image2"]);
        $background_image3 =stripslashes($getDemoRow["background_image3"]);
        $background_image4 =stripslashes($getDemoRow["background_image4"]);
        
        //APP BULID
        $ios_ipa_apk     =stripslashes($getDemoRow["ios_ipa_apk"]);
        $andorid_ipa_apk =stripslashes($getDemoRow["andorid_ipa_apk"]);

        // PAYMENT INFORMATIONS
        $paymentdate="";
        if($getDemoRow["payment_date"]!="" && $getDemoRow["payment_date"]!="0000-00-00")
	       $paymentdate=date('d-m-Y',strtotime(stripslashes($getDemoRow["payment_date"])));

        $commisiondate="";
        if($getDemoRow["commission_date"]!="" && $getDemoRow["commission_date"]!="0000-00-00")
	       $commisiondate=date('d-m-Y',strtotime(stripslashes($getDemoRow["commission_date"])));

        $Dev_date = "";
        if($getDemoRow["dev_qa_date"]!="" && $getDemoRow["dev_qa_date"]!="0000-00-00")
	       $Dev_date=date('d-m-Y',strtotime(stripslashes($getDemoRow["dev_qa_date"])));

        $affiliate_id=$getDemoRow['a_id'];
        $Licensee=stripslashes($getDemoRow["l_id"]);
        $Background_RGB=stripslashes($getDemoRow["Background_RGB"]);
        //AD-HOC PAYMENT INFORMATION
        $item_description    =stripslashes($getDemoRow["item_description"]);
        $one_offprice        =stripslashes($getDemoRow["one_offprice"]);
        $recrring_price      =stripslashes($getDemoRow["recrring_price"]);
        $currency_code       =stripslashes($getDemoRow["currency"]);
        $payment_taxcode     =stripslashes($getDemoRow["payment_taxcode"]);
        $payment_taxrate     =stripslashes($getDemoRow["payment_taxrate"]);
        $generate_payment_url=stripslashes($getDemoRow["generate_payment_url"]);
        $include_tax         =stripslashes($getDemoRow["include_tax"]);
        $csmusertype         =stripslashes($getDemoRow["csm_usertype"]);
    	$dataentryuser       =stripslashes($getDemoRow["dem_usertype"]);
    	$appversion          =stripslashes($getDemoRow["app_version"]);
    	//tabname
    	$tab_name            =stripslashes($getDemoRow["google_tab_name"]);
        $google_workbook     =stripslashes($getDemoRow["google_workbook"]);

        
    	
        $mode="Edit";
        $value="Update";
    } else {
	
        $created_user_id = $_SESSION['user_id'];
        $geUserQry="select * from  tbl_users where user_id=:user_id";
        $prepgetuserqry=$DBCONN->prepare($geUserQry);
        $prepgetuserqry->execute(array(":user_id"=>$created_user_id));
        $geUserRow=$prepgetuserqry->fetch();
        $created_user=stripslashes($geUserRow["username"]);

        $mode="Add";
        $value="Create";
    }

    if (isset($_POST["first_name"])) {
	
        $DemoTableArray = array();
    	
    	// Stimulator Info Section
    	$business_name=addslashes(trim($_POST["business_name"]));
    	$business_type=addslashes(trim($_POST["business_type"]));
    	$email=addslashes(trim($_POST["email"]));
    	$product_page=addslashes(trim($_POST["product_page"]));
    	$promo_code=addslashes(trim($_POST["promo_code"]));

    	if(empty($_POST["simulator"]))
    		$simulator_value="0";
    	else
    		$simulator_value="1";  		

    	$ResLogoQryCondition = "";
    	$file_name=pathinfo($_FILES["restaurant_logo"]["name"], PATHINFO_FILENAME);

    	if ($file_name != "") {
    		$file_extension=pathinfo($_FILES["restaurant_logo"]["name"], PATHINFO_EXTENSION);
    		$file_path = UPLOAD_PATH.$file_name.time().".".$file_extension;
    		
    		move_uploaded_file($_FILES["restaurant_logo"]["tmp_name"], "../".$file_path);
    		$ResLogoQryCondition = "restaurant_logo=:restaurant_logo, ";
    	}

	    $theme=addslashes(trim($_POST["theme"]));

    	// Contact Info Section
    	$first_name=addslashes(trim($_POST["first_name"]));
    	$street1_address=addslashes(trim($_POST["street1_address"]));
    	$suburb1=addslashes(trim($_POST["suburb1"]));
    	$state1=addslashes(trim($_POST["state1"]));
    	$country1=addslashes(trim($_POST["country1"]));
    	$postcode1=addslashes(trim($_POST["postcode1"]));
    	$res1_workphone=addslashes(trim($_POST["res1_workphone"]));
    	$mobile_phone=addslashes(trim($_POST["mobile_phone"]));

    	// Sales Info Section
    	$callstatus=addslashes(trim($_POST["callstatus"]));
    	$demo_date=(!empty($_POST["demo_date"]))?date('Y-m-d',strtotime(addslashes(trim($_POST["demo_date"])))):NULL;
    	if (!empty($_POST["demo_time"])) {
    		$demo_timeArr=explode(" ",addslashes(trim($_POST["demo_time"])));
    		if($demo_timeArr[1]=="PM")
    			$demo_time=date("H:i:s",strtotime($demo_timeArr[0]."pm"));
    		else
    			$demo_time=date("H:i:s",strtotime($demo_timeArr[0]."am"));
    	} 
        else
    		$demo_time=NULL;

    	// Development Info Section
    	if ($_POST["development_info_status"]!="")
    	 $status = addslashes(trim($_POST["development_info_status"])); // This field will represent status in development section
    	else
    	  $status=NULL;
    	
    	if ($_POST["development_info_android_status"]!="")
    	   $android_status = addslashes(trim($_POST["development_info_android_status"]));
    	else
    	  $android_status=NULL;
    	
    	if ($_POST["iphone_date"]!="")
    		$iphone_date=date('Y-m-d',strtotime(addslashes(trim($_POST["iphone_date"]))));
    	else
    		$iphone_date=NULL;
     
    	if ($_POST["android_date"]!="")
    		$android_date=date('Y-m-d',strtotime(addslashes(trim($_POST["android_date"]))));
    	else
    		$android_date=NULL;

    	$hdnstatus = addslashes(trim($_POST["hdn_development_info_status"]));
    	$developer = addslashes(trim($_POST["developer"]));
        
        if ($developer=="") {
           $developer="0"; 
        }

	    $BusinessLogoQryCondition = "";
	    $business_logo_file_name = pathinfo($_FILES["business_logo"]["name"], PATHINFO_FILENAME);
	    if ($business_logo_file_name != "") {
		    $business_logo_fe = pathinfo($_FILES["business_logo"]["name"], PATHINFO_EXTENSION);
		    $business_logo_file_path = UPLOAD_PATH.$business_logo_file_name.time().".".$business_logo_fe;
		
		    move_uploaded_file($_FILES["business_logo"]["tmp_name"], "../".$business_logo_file_path);
		    $BusinessLogoQryCondition = "business_logo=:business_logo, ";
	    }
    	$app_name = addslashes(trim($_POST["app_name"]));
    	$def_language = addslashes(trim($_POST["def_language"]));
    	$itunes_category = addslashes(trim($_POST["itunes_category"]));
    	$app_modifications = addslashes(trim($_POST["app_modifications"]));
    	$Background_RGB = addslashes(trim($_POST["Background_RGB"])); 

    	$BackgroundImageQryCondition = "";
    	$background_image_file_name = pathinfo($_FILES["background_image"]["name"], PATHINFO_FILENAME);
    	if ($background_image_file_name != "") {
    		$background_image_fe = pathinfo($_FILES["background_image"]["name"], PATHINFO_EXTENSION);
    		$background_image_file_path = UPLOAD_PATH.$background_image_file_name.time().".".$background_image_fe;
    		
    		move_uploaded_file($_FILES["background_image"]["tmp_name"], "../".$background_image_file_path);
    		$BackgroundImageQryCondition = "background_image=:background_image, ";
    	}
	
    	$BackgroundImage2QryCondition = "";
    	$background_image_file_name2 = pathinfo($_FILES["background_image2"]["name"], PATHINFO_FILENAME);
    	if ($background_image_file_name2 != "") {
    		$background_image_fe2 = pathinfo($_FILES["background_image2"]["name"], PATHINFO_EXTENSION);
    		$background_image_file_path2 = UPLOAD_PATH.$background_image_file_name2.time().".".$background_image_fe2;
    		
    		move_uploaded_file($_FILES["background_image2"]["tmp_name"], "../".$background_image_file_path2);
    		$BackgroundImage2QryCondition = "background_image2=:background_image2, ";
    	}

    	$BackgroundImage3QryCondition = "";
    	$background_image_file_name3 = pathinfo($_FILES["background_image3"]["name"], PATHINFO_FILENAME);
    	if ($background_image_file_name3 != "") {
    		$background_image_fe3 = pathinfo($_FILES["background_image3"]["name"], PATHINFO_EXTENSION);
    		$background_image_file_path3 = UPLOAD_PATH.$background_image_file_name3.time().".".$background_image_fe3;
    		
    		move_uploaded_file($_FILES["background_image3"]["tmp_name"], "../".$background_image_file_path3);
    		$BackgroundImage3QryCondition = "background_image3=:background_image3, ";
    	}	

    	$BackgroundImage4QryCondition = "";
    	$background_image_file_name4 = pathinfo($_FILES["background_image4"]["name"], PATHINFO_FILENAME);
    	if($background_image_file_name4 != "") {
    		$background_image_fe4 = pathinfo($_FILES["background_image4"]["name"], PATHINFO_EXTENSION);
    		$background_image_file_path4 = UPLOAD_PATH.$background_image_file_name4.time().".".$background_image_fe4;
    		
    		move_uploaded_file($_FILES["background_image4"]["tmp_name"], "../".$background_image_file_path4);
    		$BackgroundImage4QryCondition = "background_image4=:background_image4, ";
    	}

        //APP BUILD SECTION
        $BuildIosQryCondition = "";
    	$built_ios_file_name = pathinfo($_FILES["ios_app_ipa"]["name"], PATHINFO_FILENAME);
    	
    	if ($built_ios_file_name != "") {
    		$built_file_fe = pathinfo($_FILES["ios_app_ipa"]["name"], PATHINFO_EXTENSION);
    		$builtios_file_path = UPLOAD_PATH_APK.$built_ios_file_name."_".time().".".$built_file_fe;
    		
    		move_uploaded_file($_FILES["ios_app_ipa"]["tmp_name"], "../".$builtios_file_path);
    		$BuildIosQryCondition = "ios_ipa_apk=:iosipafile, ";
    	}

    	$BuildAndoridQryCondition = "";
    	$built_andorid_file_name = pathinfo($_FILES["andorid_app_ipa"]["name"], PATHINFO_FILENAME);
    	if ($built_andorid_file_name != "") {
    		$built_andorid_fe = pathinfo($_FILES["andorid_app_ipa"]["name"], PATHINFO_EXTENSION);
    		$builtandorid_file_path = UPLOAD_PATH_APK.$built_andorid_file_name."_".time().".".$built_andorid_fe;
    		
    		move_uploaded_file($_FILES["andorid_app_ipa"]["tmp_name"], "../".$builtandorid_file_path);
    		$BuildAndoridQryCondition = "andorid_ipa_apk=:andoridipafile, ";
    	}
      
    	// Payment Info Section
    	if ($_POST["payment_date"]!="")
    		$paymentdate=date('Y-m-d',strtotime(addslashes(trim($_POST["payment_date"]))));
    	else
    		$paymentdate=NULL;

    	if ($_POST["commision_date"]!="")
    		$commision_date=date('Y-m-d',strtotime(addslashes(trim($_POST["commision_date"]))));
    	else
    		$commision_date=NULL;

    	if ($_POST["dev_date"]!="")
    		$dev_date=date('Y-m-d',strtotime(addslashes(trim($_POST["dev_date"]))));
    	else
    		$dev_date=NULL;

    	$affiliate=$_POST["affiliate"];

    	//AD-HOC PAYMENT INFORMATION
    	$item_description   =$_POST["item_description"];
    	$one_offprice       =$_POST["one_offprice"];
    	$recrring_price     =$_POST["recrring_price"];
    	$currency           =$_POST["currency"];   
    	$tax_code           =$_POST["tax_code"];
    	$tax_rate           =$_POST["tax_rate"];
        $include_tax        = isset($_POST["includetax"]) ? $_POST["includetax"] : 0 ;

        // user_type
        $csm_entryuser   =isset($_POST["csm_entryuser"])? $_POST["csm_entryuser"] : 0 ;
    	$data_entryuser  =isset($_POST["data_entryuser"])? $_POST["data_entryuser"] : 0 ;
        //appversion
        $app_version   =isset($_POST["app_version"])? $_POST["app_version"] : 0 ;
        // google tabname
		$tabname=isset($_POST["tab_name"])? $_POST["tab_name"] :"";
        //google_workbook
        $googleworkbook=isset($_POST["google_workbook"])? $_POST["google_workbook"] :"";

    	$CurrentDate = date("Y-m-d H:i:s");
    	$DemoTableArray = array(":business_name" => $business_name
    							, ":business_type" => $business_type
    							, ":email" => $email							
    							, ":simulator_feature" => $simulator_value
    							, ":theme" => $theme
    							, ":first_name" => $first_name
    							, ":res1_address" => $street1_address
    							, ":res1_suburb" => $suburb1
    							, ":res1_state" => $state1
    							, ":res1_postcode" => $postcode1
    							, ":res1_country" => $country1
    							, ":res1_workphone" => $res1_workphone
    							, ":mobile_phone" => $mobile_phone							
    							, ":demo_date" => $demo_date
    							, ":demo_time" => $demo_time
    							, ":status" => $status
    		                    , ":android_status" => $android_status
    							, ":iphone_date" => $iphone_date 
    							, ":android_date" => $android_date
    							, ":developer_id" => $developer
    							, ":app_name" => $app_name
    							, ":default_langauage" => $def_language
    							, ":itunes_category" => $itunes_category
    							, ":app_modifications" => $app_modifications
    							, ":payment_date" => $paymentdate
    							, ":commission_date" => $commision_date
    							, ":dev_qa_date" => $dev_date
    							, ":a_id" => $affiliate
    							, ":l_id" => $licence_id
    							, ":modified_date" => $CurrentDate
    							, ":Background_RGB" => $Background_RGB
    		                    , ":item_description" => $item_description
    							, ":one_offprice" => $one_offprice
    							, ":recrring_price" => $recrring_price
    							, ":currency" => $currency
    							, ":tax_code" => $tax_code
    							, ":tax_rate" => $tax_rate
    						    , ":include_tax"=>$include_tax
    						    , ":csmtype"=>$csm_entryuser
    						    , ":demtype"=>$data_entryuser
    						    , ":appversion"=>$app_version
    						    , ":tabname"=>$tabname
                                , ":google_workbook"=>$googleworkbook
    							);

    	// Add restraurant logo value to array if field uploaded
    	$ResLogoQryCondAdd1 = "";
    	$ResLogoQryCondAdd2 = "";
    	if ($ResLogoQryCondition != "") {
    		$ResLogoQryCondAdd1 = "restaurant_logo, ";
    		$ResLogoQryCondAdd2 = ":restaurant_logo, ";
    		$DemoTableArray[':restaurant_logo'] = $file_path;
    	}

    	// Add Business logo value to array if field uploaded
    	$BiznsLogoQryCondAdd1 = "";
    	$BiznsLogoQryCondAdd2 = "";
    	if ($BusinessLogoQryCondition != "") {
    		$BiznsLogoQryCondAdd1 = "business_logo, ";
    		$BiznsLogoQryCondAdd2 = ":business_logo, ";
    		$DemoTableArray[':business_logo'] = $business_logo_file_path;
    	}

    	// Add BackgroundImage value to array if field uploaded
    	$BackgroundImageQryCondAdd1 = "";
    	$BackgroundImageQryCondAdd2 = "";
    	if ($BackgroundImageQryCondition != "") {
    		$BackgroundImageQryCondAdd1 = "background_image, ";
    		$BackgroundImageQryCondAdd2 = ":background_image, ";
    		$DemoTableArray[':background_image'] = $background_image_file_path;
    	}

    	// Add BackgroundImage 2  value to array if field uploaded
    	$BackgroundImage2QryCondAdd1 = "";
    	$BackgroundImage2QryCondAdd2 = "";
    	if ($BackgroundImage2QryCondition != "") {
    		$BackgroundImage2QryCondAdd1 = "background_image2, ";
    		$BackgroundImage2QryCondAdd2 = ":background_image2, ";
    		$DemoTableArray[':background_image2'] = $background_image_file_path2;
    	}

    	// Add BackgroundImage 3  value to array if field uploaded
    	$BackgroundImage3QryCondAdd1 = "";
    	$BackgroundImage3QryCondAdd2 = "";
    	if ($BackgroundImage3QryCondition != "") {
    		$BackgroundImage3QryCondAdd1 = "background_image3, ";
    		$BackgroundImage3QryCondAdd2 = ":background_image3, ";
    		$DemoTableArray[':background_image3'] = $background_image_file_path3;
    	}

    	// Add BackgroundImage 4  value to array if field uploaded
    	$BackgroundImage4QryCondAdd1 = "";
    	$BackgroundImage4QryCondAdd2 = "";
    	if($BackgroundImage4QryCondition != "") {
    		$BackgroundImage4QryCondAdd1 = "background_image4, ";
    		$BackgroundImage4QryCondAdd2 = ":background_image4, ";
    		$DemoTableArray[':background_image4'] = $background_image_file_path4;
    	}

    	// Add Buildios  value to array if field uploaded
    	$IosbuiltQryCondAdd1 = "";
    	$IosbuiltQryCondAdd2 = "";
    	if ($BuildIosQryCondition != "") {
    		$IosbuiltQryCondAdd1 = "ios_ipa_apk, ";
    		$IosbuiltQryCondAdd2 = ":iosipafile, ";
    		$DemoTableArray[':iosipafile'] = $builtios_file_path;
    	}
        
         
       // Add BuildAndorid  value to array if field uploaded
    	$AndoridQryCondAdd1 = "";
    	$AndoridQryCondAdd2 = "";
    	if ($BuildAndoridQryCondition != "") {
    		$AndoridQryCondAdd1 = "andorid_ipa_apk, ";
    		$AndoridQryCondAdd2 = ":andoridipafile, ";
    		$DemoTableArray[':andoridipafile'] = $builtandorid_file_path;
    	}
        

        $Mode = $_POST["hdn_mode"];
    	if ($Mode == 'Edit')  {
    		$DemoTableArray[':demo_id'] = $demoId;

    		$DemoQry = "update tbl_demo set 
    		business_name=:business_name,  business_type=:business_type, email=:email, simulator_feature=:simulator_feature, ".$ResLogoQryCondition."theme=:theme,  
    		first_name=:first_name, res1_address=:res1_address, res1_suburb=:res1_suburb, res1_state=:res1_state, res1_postcode=:res1_postcode,res1_country=:res1_country,res1_workphone=:res1_workphone, mobile_phone=:mobile_phone,
    		demo_date=:demo_date, demo_time=:demo_time,
    		status=:status, android_status=:android_status,iphone_date=:iphone_date, android_date=:android_date, developer_id=:developer_id, ".$BusinessLogoQryCondition."app_name=:app_name, default_langauage=:default_langauage, itunes_category=:itunes_category, app_modifications=:app_modifications, 
    		payment_date=:payment_date, commission_date=:commission_date, dev_qa_date=:dev_qa_date,
    		a_id=:a_id, l_id=:l_id, modified_date=:modified_date, ".$BackgroundImageQryCondition.$BackgroundImage2QryCondition.$BackgroundImage3QryCondition.$BackgroundImage4QryCondition.$BuildIosQryCondition.$BuildAndoridQryCondition."Background_RGB=:Background_RGB , item_description=:item_description, one_offprice=:one_offprice, recrring_price=:recrring_price, currency=:currency,payment_taxcode=:tax_code,payment_taxrate=:tax_rate, include_tax=:include_tax, csm_usertype=:csmtype, dem_usertype=:demtype, app_version=:appversion,google_tab_name=:tabname, google_workbook=:google_workbook   
    		where 
    		demo_id=:demo_id";	
        
        } else {
		
    		$DemoTableArray[':product_page'] = $product_page;
    		$DemoTableArray[':call_status'] = $callstatus;
    		$DemoTableArray[':promo_code'] = $promo_code;
    		$DemoTableArray[':user_id'] = $_SESSION['user_id'];
    		$DemoTableArray[':added_date'] = $CurrentDate;
		
		    $DemoQry = "INSERT INTO tbl_demo 
				(business_name, business_type, email, product_page, promo_code, simulator_feature, ".$ResLogoQryCondAdd1." theme, user_id,
					first_name, res1_address, res1_suburb, res1_state, res1_postcode, res1_country, res1_workphone, mobile_phone,
					call_status, demo_date, demo_time, 
					status, android_status, iphone_date, android_date, developer_id, ".$BiznsLogoQryCondAdd1." app_name, default_langauage, itunes_category, app_modifications, 
					payment_date, commission_date, dev_qa_date, a_id, l_id,
					added_date, modified_date, ".$BackgroundImageQryCondAdd1.$BackgroundImage2QryCondAdd1.$BackgroundImage3QryCondAdd1.$BackgroundImage4QryCondAdd1.$IosbuiltQryCondAdd1.$AndoridQryCondAdd1." Background_RGB ,item_description ,	one_offprice, recrring_price, currency, payment_taxcode, payment_taxrate, include_tax, csm_usertype, dem_usertype, app_version, google_tab_name,google_workbook) 
				VALUES 
					(:business_name, :business_type, :email, :product_page, :promo_code, :simulator_feature, ".$ResLogoQryCondAdd2." :theme, :user_id,
					:first_name, :res1_address, :res1_suburb, :res1_state, :res1_postcode, :res1_country, :res1_workphone, :mobile_phone,
					:call_status, :demo_date, :demo_time, 
					:status, :android_status, :iphone_date, :android_date, :developer_id, ".$BiznsLogoQryCondAdd2." :app_name, :default_langauage, :itunes_category, :app_modifications,
					:payment_date, :commission_date, :dev_qa_date, :a_id, :l_id,
					:added_date, :modified_date, ".$BackgroundImageQryCondAdd2.$BackgroundImage2QryCondAdd2.$BackgroundImage3QryCondAdd2.$BackgroundImage4QryCondAdd2.$IosbuiltQryCondAdd2.$AndoridQryCondAdd2." :Background_RGB,:item_description, :one_offprice, :recrring_price,:currency,:tax_code,:tax_rate, :include_tax,:csmtype,:demtype, :appversion, :tabname, :google_workbook)";
	    }

		$PrepareDemoQuery =$DBCONN->prepare($DemoQry);	
        $PrepareDemoQuery->execute($DemoTableArray);
		if ($PrepareDemoQuery->errorCode()> 0) {
    		$arrerrors = $PrepareDemoQuery->errorInfo();
    		print_r($arrerrors);
    		exit;
	   }

	header("Location:masterlist.php");
	exit;
}

include("includes/header.php");
?>
<style>
    a:link, a:visited { 
        color: white;
        text-decoration: underline;
        cursor: auto;
    	font-size:12px;
    }
    .logdisplay{
        display: none;
    }
    #comment_popup::-webkit-input-placeholder {
    color: black;
  }
  .viewimg{
  	float: right;
  	margin-right:15px;
  	cursor: pointer;
    border-radius: 6px;
    background: #ff4215;
    color: #D9D9D9;
    border-color: #ff4215;
    padding: 5px 5px 5px 5px;
    outline: none;
    font-weight: bold;
    margin-bottom: 5px;
    margin-top: 5px;
    font-size: 12px;
  }
</style>
<script>
    $(function() {
    	<?php
    	if($paymentf=="yes"){
    	?>
    		$("#payment_date").datetimepicker({ format:'d-m-Y',formatDate:'d-m-Y',timepicker:false});
    		$("#commision_date").datetimepicker({ format:'d-m-Y',formatDate:'d-m-Y',timepicker:false});
    		$("#dev_date").datetimepicker({ format:'d-m-Y',formatDate:'d-m-Y',timepicker:false});
    	<?php
    	}
    	?>
        	$("#iphone_date").datetimepicker({ format:'d-m-Y',formatDate:'d-m-Y',timepicker:false});
        	$("#android_date").datetimepicker({ format:'d-m-Y',formatDate:'d-m-Y',timepicker:false});
        	$("#dev_date1").datetimepicker({ format:'d-m-Y',formatDate:'d-m-Y',timepicker:false});
        	$("#dem_date").datetimepicker({ format:'d-m-Y',formatDate:'d-m-Y',timepicker:false});
        	$("#cms_date").datetimepicker({ format:'d-m-Y',formatDate:'d-m-Y',timepicker:false});
    });
</script>
<body>
    <div>
		<div style="margin-left:auto;margin-right:auto;">
			<div class="content">
				<div class="list_content">
					<table cellspacing="15" cellpadding="0" border="0" width="75%">
						<tr>
						   <td colspan="2">
								<table cellspacing="0" cellpadding="0" width="100%" border="0">
									<tr>
										<td>
										   <div class="form_actions">
										      <input type="button" value="Back To Master List" class="add_btn"   onclick="document.location='masterlist.php'">
										    </div>
										</td>										 
									 	<td>
										    <div class="form_actions" style="text-align:right;">
											   <input type="button" value="<?php echo ucfirst($value);?> Record" class="add_btn add_demo" id="add_demo">  
										    </div>
										</td>
									</tr>
								</table>
							</td>
						</tr>
					</table>				 
					<form name="demo_form" id="demo_form" method="post" enctype="multipart/form-data">
						<input type="hidden" name="hdn_mode" id="hdn_mode" value="<?php echo $mode;?>">
						<input type="hidden" name="hdn_demo_id" id="hdn_demo_id" value="<?php echo $demoId;?>">
						<input type="hidden" name="hdn_img" id="hdn_img" value="<?php echo $old_special_logo;?>">
						<table cellspacing="15" cellpadding="0" border="0" width="100%">
    						<?php
    						   if ($msg!="") {
    						?>  
                                <tr bgcolor="white" height="40px" id="error_message">
    								<td style="color:black;font-size:20px;font-family:arial;margin-left:10px;" colspan="2">Client trial form added successfully.</td>
    							</tr>
    						<?php
    							}
    						?>
                            <tr>
								<td colspan="2" nowrap>
									<h3><u>CONTACT INFO:</u></h3>
								</td>
							</tr>
                           <tr>
                                <td style="width:190px;" nowrap> Business Name <font color="red">*</font>:</td>
                                <td>
                                    <input type="text" name="business_name" id="business_name" class="inp_feild" value="<?php echo $business_name;?>" tabindex="1">
                                </td>
                                <td></td>
                                <td rowspan="4">
                                   <table border="0" cellspacing="5" cellpadding="0" style="padding-right:10px;padding-left:10px;border:3px solid white;width:50%;margin-right: -60px;">
                                    <tr>
                                        <td  colspan="2" nowrap>
                                            <h4 style="margin-top: 7px;"><u>SURVEY LINK INFO:</u></h4>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td nowrap> Tab Name <font color="red">*</font>:</td>
                                        <td>
                                            <input type="text" name="tab_name" id="tab_name" class="inp_feild" value="<?php echo $tab_name;?>" style="width:200px" tabindex="2">
                                        </td>
                                    </tr>
                                    <tr><td></td></tr>
                                    <tr>
                                        <td  nowrap>Google Workbook:</td>
                                        <td>
                                            <input type="text" name="google_workbook" id="google_workbook" class="inp_feild" value="<?php echo $google_workbook;?>" tabindex="3">
                                        </td>
                                    </tr>
                                    <tr><td></td></tr>
                                    </table>
                                </td>
                            </tr> 
                            
							<tr>
							    <td style="width:168px;" nowrap>Full Name<?php if($prospect_id==""){echo "<font color=\"red\">*</font>";} ?>:
								</td>
								<td>
									<input type="text" name="first_name" id="first_name" class="inp_feild" value="<?php echo $first_name;?>" tabindex="4">
								</td>
                                <td></td>
								<td></td>
                                <td></td>
							</tr>
							<tr>
								<td nowrap>Address<?php if($prospect_id==""){echo "<font color=\"red\">*</font>";} ?>:</td>
								<td>
									<input type="text" name="street1_address" id="street1_address" class="inp_feild" value="<?php echo $res1_address;?>" tabindex="5">
								</td>
                                <td></td>
								<td></td>
                                <td></td>
							</tr>
							<tr>
								<td nowrap>Suburb:</td>
								<td>
									<input type="text" name="suburb1" id="suburb1" class="inp_feild" value="<?php echo $res1_suburb;?>" tabindex="6">
								</td>
							</tr>
							<tr>
								<td  valign="top" nowrap>State:</td>
								<td  valign="top">
									<table width="100%" style="margin-left: -3px;">
										<tr>
											<td><input type="text" name="state1" id="state1" class="inp_feild" value="<?php echo $res1_state;?>" tabindex="7"></td>
											<td align="center">Postcode:</td>
											<td><input type="text"  name="postcode1" id="postcode1" class="inp_feild"  maxlength="20" size="20" value="<?php echo stripslashes($res1_postcode);?>"   tabindex="8" ></td>
										</tr>
									</table>
                                </td>
                                <td></td>
                               <td></td>
                                <td></td>
							</tr>
							<tr>
								<td nowrap>Country<font color="red">*</font>:</td>
								<td>
									<select name="country1" id="country1" class="inp_feild"  tabindex="9">
										<option value="0"<?php if($res1_country=="0") echo " selected";?>>Select</option>
										<?php
											$getcountrQry="select * from  tbl_country";
											$getcountryRes=mysql_query($getcountrQry);
											
											while($getcountryRow=mysql_fetch_array($getcountryRes)){
										   ?>
											<option value="<?php echo stripslashes($getcountryRow["country"]);?>"<?php if($res1_country==$getcountryRow["country"]) echo " selected";?>><?php echo stripslashes($getcountryRow["country"]);?></option>
										<?php
											}
										?>
									</select>
                                </td>
                                <td></td>
                                <td></td>
                                <td></td>
							</tr>
                            <tr>
                                <td nowrap>Email Address<font color="red">*</font>:</td>
                                <td>
                                    <input type="text" name="email" id="email" class="inp_feild" value="<?php echo $email;?>" tabindex="10">
                                </td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
								<td nowrap>Work Phone<font color="red">*</font>:</td>
								<td>
									<input type="text" name="res1_workphone" id="res1_workphone" class="inp_feild" value="<?php echo $res1_workphone;?>" tabindex="11">
								</td>
                                <td></td>
								<td></td>
                                <td></td>
							</tr>
							<tr>
								<td nowrap>Mobile Phone:</td>
								<td>
								   <input type="text" name="mobile_phone" id="mobile_phone" class="inp_feild" value="<?php echo $mobile_phone;?>" tabindex="12">
								</td>
                                <td></td>
								<td></td>
                                <td></td>
							</tr>
						</table>
						
						<!--********************Service Delivery Process******************-->
                        <table>
						    <tr> 
                               <td colspan="2" nowrap>
			                        <h3><u>SERVICE DELIVERY PROGRESS</u></h3>
						        </td>
						    </tr>
						     <tr> 
                                <td colspan="2" nowrap style="padding-left:10px;"">App Version<font color="red"></font>:
							
									<select name="app_version" id="app_version" class="inp_feild"  tabindex="13" style="margin-left:125px;width:56%;">
										<option value="0">Select</option>
                                        <?php
                                        $get_Qry="select * from tbl_version order by version_name asc";
                                        
                                        $get_Res=mysql_query($get_Qry);
                                        while($get_value=mysql_fetch_array($get_Res))
                                        { 
                                            $SelectedString = "";
                                            $TblversionId = $get_value['version_id'];

                                            if($TblversionId == $appversion)
                                                $SelectedString = "selected";

                                            ?>
                                        <option value="<?php echo $TblversionId;?>" <?php echo $SelectedString;?>><?php echo stripslashes($get_value["version_name"]);?></option>
                                        <?php
                                        }
                                        ?> 
                                    </select> 
                                </td>
						    </tr>
						    <tr>
						    <td style="height:10px;"></td>
						    </tr>
						    
						    <tr>
						        <td style="max-width:600px !important;">
						            <table class="content_table" style="border-collapse: collapse;border: 1px solid white;" cellpadding="3">
						                <tr>
					                        <td style="min-width: 188px;"></td>
					                        <td style="min-width: 150px;">IPHONE APP</td>
					                        <td style="min-width: 95px;"></td>
					                        <td style="min-width: 85px;"></td>
					                        <td style="min-width: 50px;"></td>
					                        <td class="border_right"></td>
					                        <td style="min-width: 150px; padding-left: 15px;">ANDROID APP</td>
					                        <td style="min-width: 95px;"></td>
					                        <td style="min-width: 85px;"></td>
					                        <td style="min-width: 50px;"></td>
						                </tr>
						                <tr>
						                    <td>APP DESIGNED:</td>
						                    <td class="" id=""></td>
						                    <td></td>
						                    <td></td>
						                    <td class="padding_left"></td>
						                    <td></td>
						                    <td class="app_designname padding" id="app_designname"><?php echo $app_design_log;?></td>
						                    <td></td>
						                    
						                    <td class="app_designdate" id="app_designdate"><?php echo $app_design_date;?></td>
						                    <td style="float: right;">
						                       <input type="button" class="service_color app_designed_log <?php echo in_array('appdesigned',$btnmenu)?"":"logdisplay";?> <?php echo $app_design_btn_disabled ?>"  value="LOG" data-id="app_desiged" list-id="<?php echo $list_id;?>"  user-type="<?php echo $user_type;?>">
						                    </td>
							            </tr>
						                <tr>
					                        <td>APP BUILT:</td>
					                        <td class="iosbuiltname" id="iosbuiltname"><?php echo $ios_built_by;?></td>
					                        <td></td>
					                        <td class="iosbuiltdate" id="iosbuiltdate"><?php echo $ios_built_date;?></td>
					                        <td class="padding_left">
					                           <input type="button" value="LOG" class="service_color ios_built_log <?php echo in_array('iosbuilt',$btnmenu)?"":"logdisplay";?> <?php echo $ios_built_btn_disabled ?>" data-id="ios built" list-id="<?php echo $list_id;?>"  user-type="<?php echo $user_type;?>">
					                        </td>
					                        <td class="border_right"></td>
					                        <td class="andoridbuiltname padding" id="andoridbuiltname"><?php echo $android_built_by;?></td>
					                        <td></td>
					                        <td class="andoridbuiltdate" id="andoridbuiltdate"><?php echo $android_built_date;?></td>
					                        <td style="float: right;">
					                            <input type="button" value="LOG" class="service_color andorid_built_log <?php echo in_array('andoridbuilt',$btnmenu)?"":"logdisplay";?> <?php echo $android_built_btn_disabled ?>" data-id="andorid built" list-id="<?php echo $list_id;?>"   user-type="<?php echo $user_type;?>">
					                        </td>
						                </tr>
						                <tr>
						                    <td>DEV'T PAYMENT:</td>
						                    <td></td>
						                    <td></td>
						                    <td></td>
						                    <td class="padding_left"></td>
						                    <td></td>
						                    <td class="app_devpaymentname padding" id="app_devpaymentname"><?php echo $devpayment_made_by;?></td>
						                    <?php if($devpayment_amount!=""){
                                               $devpayment_amount1="$".$devpayment_amount;

                                             } ?>
						                    <td class="app_devpaymentamount" id="app_devpaymentamount"><?php echo $devpayment_amount1;?></td>
						                    <td class="app_devpaymentdate" id="app_devpaymentdate"><?php echo $devpayment_date; ?></td>
						                    <td style="float: right;">
						                        <a class="fancybox devpayment_log dev_payment_log <?php echo in_array('devpayment',$btnmenu)?"":"logdisplay";?> <?php echo $devpayment_disabled ?>" data-id="dev_payment" list-id="<?php echo $list_id;?>" href="#inline2" style="text-decoration:none"  user-type="<?php echo $user_type;?>"><input type="button" class="service_color dev_payment_log <?php echo in_array('devpayment',$btnmenu)?"":"logdisplay";?> <?php echo $devpayment_disabled ?>" value="LOG" data-id="dev_payment" list-id="<?php echo $list_id;?>"   user-type="<?php echo $user_type;?>"></a>
						                      
						                    </td> 
						                </tr>
						                <tr>
					                        <td>APP TESTED:</td>
					                        <td class="iostestbyname" id="iostestbyname"><?php echo $ios_tested_by;?></td>
					                        <td></td>
					                        <td class="iostestbydate" id="iostestbydate"><?php echo $ios_tested_date;?></td>
					                        <td class="padding_left">
					                           <input type="button" value="LOG" class="service_color ios_test_log <?php echo in_array('iostested',$btnmenu)?"":"logdisplay";?> <?php echo $ios_tested_btn_disabled ?>" data-id="app_iostested" list-id="<?php echo $list_id;?>"  user-type="<?php echo $user_type;?>" >
					                        </td>
					                        <td class="border_right"></td>
					                        <td class="andoridtestbyname padding" id="andoridtestbyname"><?php echo $android_tested_by;?></td>
					                        <td ></td>
					                        <td class="andoridtestbydate" id="andoridtestbydate"><?php echo $android_tested_date;?></td>
					                        <td style="float: right;">
					                           <input type="button" value="LOG" class="service_color andorid_test_log <?php echo in_array('andoridtested',$btnmenu)?"":"logdisplay";?> <?php echo $android_tested_btn_disabled ?>" data-id="app_andoridtested" list-id="<?php echo $list_id;?>" user-type="<?php echo $user_type;?>" >
					                        </td>
						                </tr>
						                <tr>
						                    <td>APP POPULATED:</td>
						                    <td></td>
						                    <td></td>
						                    <td></td>
						                    <td class="padding_left">
						                    </td>
						                    <td></td>
						                    <td class="app_populatedname padding" id="app_populatedname"><?php echo $app_populated_by;?></td>
						                    <td></td>
						                    <td class="app_populateddate" id="app_populateddate"><?php echo $app_populated_date;?></td>
						                    <td style="float: right;">
						                        <input type="button" class="service_color app_populated_log <?php echo in_array('apppopulated',$btnmenu)?"":"logdisplay";?> <?php echo $app_populated_disabled ?>" value="LOG" data-id="app_populated" list-id="<?php echo $list_id;?>"   user-type="<?php echo $user_type;?>" >
						                    </td>
						                </tr> 
						                <tr>
						                    <td>DEM PAYMENT:</td>
						                    <td></td>
						                    <td></td>
						                    <td></td>
						                    <td class="padding_left">
						                        
						                    </td>
						                    <td ></td>
						                    <td class="app_dempaymentname padding" id="app_dempaymentname"><?php echo $dempayment_made_by;?></td>
						                     <?php if($dempayment_amount!=""){
                                               $dempayment_amount1="$".$dempayment_amount;

                                             } ?>
						                    <td class="app_dempaymentamount" id="app_dempaymentamount"><?php echo $dempayment_amount1;?></td>
						                    <td class="app_dempaymentdate" id="app_dempaymentdate"><?php echo $dempayment_date;?></td>
						                    <td style="float: right;">
						                        <a class="fancybox devpayment_log dem_payment_log <?php echo in_array('dempayment',$btnmenu)?"":"logdisplay";?> <?php echo $dempayment_disabled ?> " data-id="dem_payment" list-id="<?php echo $list_id;?>" href="#inline2"  user-type="<?php echo $user_type;?>"><input type="button" class="service_color dem_payment_log <?php echo in_array('dempayment',$btnmenu)?"":"logdisplay";?> <?php echo $dempayment_disabled ?>" value="LOG"  data-id="dem_payment" list-id="<?php echo $list_id;?>"  user-type="<?php echo $user_type;?>"></a>
						                    </td>
						                </tr> 
						                <tr>
						                    <td>APP SUBMITTED:</td>
						                    <td class="iossubmitbyname" id="iossubmitbyname" ><?php echo $ios_submitted_by;?></td>
						                    <td></td>
						                    <td class="iossubmitbydate" id="iossubmitbydate" ><?php echo $ios_submitted_date;?></td>
						                    <td class="padding_left">
						                      <input type="button" value="LOG" class="service_color ios_submitted_log <?php echo in_array('iossubmit',$btnmenu)?"":"logdisplay";?> <?php echo $ios_submitted_btn_disabled ?>" data-id="app_iossumbmit" list-id="<?php echo $list_id;?>" user-type="<?php echo $user_type;?>">
						                    </td>
						                    <td class="border_right"></td>
						                    <td class="andoridsubmitbyname padding" id="andoridsubmitbyname"><?php echo $android_submitted_by;?></td>
						                    <td ></td>
						                    <td class="andoridsubmitbydate" id="andoridsubmitbydate"><?php echo $android_submitted_date;?></td>
						                    <td style="float: right;">
						                       <input type="button" value="LOG" class="service_color andorid_submitted_log <?php echo in_array('andoridsubmit',$btnmenu)?"":"logdisplay";?> <?php echo $android_submitted_btn_disabled ?>"  data-id="app_andoridsumbmit" list-id="<?php echo $list_id;?>" user-type="<?php echo $user_type;?>">
						                    </td>
						                </tr>
						                <tr>
						                    <td>APP LIVE:</td>
						                    <td class="ioslivebyname" id="ioslivebyname" ><?php echo $ios_live_by;?></td>
						                    <td></td>
						                    <td class="ioslivebydate" id="ioslivebydate"><?php echo $ios_live_date;?></td>
						                    <td class="padding_left">
						                       <input type="button" value="LOG" class="service_color ios_live_log <?php echo in_array('ioslive',$btnmenu)?"":"logdisplay";?> <?php echo $ios_live_disabled ?>" data-id="app_ioslive" list-id="<?php echo $list_id;?>"   user-type="<?php echo $user_type;?>">
						                    </td>
						                    <td class="border_right"></td>
						                    <td class="andoridlivebyname padding" id="andoridlivebyname"><?php echo $android_live_by;?></td>
						                    <td ></td>
						                    <td class="andoridlivebydate" id="andoridlivebydate"><?php echo $android_live_date;?></td>
						                    <td style="float: right;">
						                        <input type="button" class="service_color andorid_live_log <?php echo in_array('andoridlive',$btnmenu)?"":"logdisplay";?> <?php echo $android_live_disabled ?>" value="LOG" data-id="app_andoridlive" list-id="<?php echo $list_id;?>"   user-type="<?php echo $user_type;?>">
						                    </td>
						                </tr> 
						                <tr> 
						                    <td>APP WELCOME CALL:</td>
						                    <td></td>
						                    <td></td>
						                    <td></td>
						                    <td class="padding_left">
						                    </td>
						                    <td ></td>
						                    <td class="app_welcallname padding" id="app_welcallname"><?php echo $appwelcome_call;?></td>
						                    <td></td>
						                    <td class="app_welcalldate" id="app_welcalldate"><?php echo $appwelcome_date;?></td>
						                    <td style="float: right;">
						                        <input type="button" class="service_color welcome_call_log  <?php echo in_array('welcomecall',$btnmenu)?"":"logdisplay";?> <?php echo $welcome_btn_disabled ?>" value="LOG" data-id="app_welcomecall" list-id="<?php echo $list_id;?>"  user-type="<?php echo $user_type;?>">
						                    </td>
						                </tr>
						                <tr>
						                    <td>APP TRAINING:</td>
						                    <td></td>
						                    <td></td>
						                    <td></td>
						                    <td class="padding_left">
						                    </td>
						                    <td ></td>
						                    <td class="app_trainingname padding" id="app_trainingname"><?php echo $apptraining;?></td>
						                    <td></td>
						                    <td calss="apptrainingdate" id="apptrainingdate"><?php echo $apptraining_date;?></td>
						                    <td style="float: right;">
						                       <input type="button" class="service_color app_training_log <?php echo in_array('apptraining',$btnmenu)?"":"logdisplay";?> <?php echo $training_disabled ?>" value="LOG" data-id="app_training" list-id="<?php echo $list_id;?>"  user-type="<?php echo $user_type;?>">
						                    </td>
						                </tr>
						                <tr>
						                    <td>CSM PAYMENT:</td>
						                    <td></td>
						                    <td></td>
						                    <td></td>
						                    <td class="padding_left">
						                        
						                    </td>
						                    <td ></td>
						                    <td class="app_cmspaymentname padding" id="app_cmspaymentname"><?php echo $cmspayment_made_by;?></td>
						                     <?php if($cmspayment_amount!=""){
                                               $cmspayment_amount1="$".$cmspayment_amount;

                                             } ?>
						                    <td class="app_cmspaymentamount" id="app_cmspaymentamount"><?php echo $cmspayment_amount1;?></td>
						                    <td class="app_cmspaymentdate" id="app_cmspaymentdate"><?php echo $cmspayment_date;?></td>
						                    <td style="float: right;">
						                        <a class="fancybox devpayment_log cms_payment_log <?php echo in_array('cmspayment',$btnmenu)?"":"logdisplay";?> <?php echo $cmspayment_disabled ?>" data-id="cms_payment" list-id="<?php echo $list_id;?>" href="#inline2" style="text-decoration:none" user-type="<?php echo $user_type;?>"><input type="button" class="service_color cms_payment_log <?php echo in_array('cmspayment',$btnmenu)?"":"logdisplay";?> <?php echo $cmspayment_disabled ?>" value="LOG"  data-id="cms_payment" list-id="<?php echo $list_id;?>"   user-type="<?php echo $user_type;?>"></a>
						                    </td>
						                </tr>
						                <tr> 
						                    <td>MARKETING SERVICES:</td>
						                    <td></td>
						                    <td></td>
						                    <td></td>
						                    <td class="padding_left">
						                      
						                    </td>
						                    <td ></td>
						                    <td class="app_marketingname padding" id="app_marketingname"><?php echo $market_made_by;?></td>
						                    <td ></td>
						                    <td class="app_marketingdate" id="app_marketingdate"><?php echo $market_date;?></td> 
						                    <td style="float: right;">
						                        <a style="text-decoration:none" class="fancybox market_service_log <?php echo in_array('marketservice',$btnmenu)?"":"logdisplay";?>" data-id="market_service" list-id="<?php echo $list_id;?>" href="#inline2" ><input type="button" class="service_color app_marketing_log <?php echo in_array('marketservice',$btnmenu)?"":"logdisplay";?> " value="LOG" data-id="market_service"  list-id="<?php echo $list_id;?>" user-type="<?php echo $user_type;?>"></a>
						                    </td>
						                </tr>
						                <tr>
						                    <td>SUPPORT CALLS:</td>
						                    <td></td>
						                    <td></td>
						                    <td></td>
						                    <td class="padding_left">
						                        
						                    </td>   
						                    <td ></td>
						                    <td class="app_supportname padding" id="app_supportname"><?php echo $support_made_by;?></td> 
						                    <td ></td>
						                    <td class="app_supportdate" id="app_supportdate"><?php echo $support_date;?></td>
						                    <td style="float: right;">
						                       <a style="text-decoration:none" class="fancybox support_call_log <?php echo in_array('supportcall',$btnmenu)?"":"logdisplay";?>" data-id="support_call" list-id="<?php echo $list_id;?>" href="#inline2" ><input type="button" class="service_color <?php echo in_array('supportcall',$btnmenu)?"":"logdisplay";?>" value="LOG" data-id="support_call" list-id="<?php echo $list_id;?>"  user-type="<?php echo $user_type;?>">
						                    </td>
						                </tr>
						            </table>
						        </td>
						    </tr>
						    <?php if($_SESSION['user_id']=="3" || $_SESSION['user_id']=="173"){?>
						    <tr>
						        <td colspan="3"></td>
								<td style="padding-left: 270px;">
							    	<div class="form_actions" style="text-align:right;" id="upd_pros_btn">
	                              	<input type="button" value="Reset Progress Table" class="uplaod_url btn_reset" id="btn_reset" data-id="reset" list-id="<?php echo $list_id;?>" user-type="resetuser">  
	                         	   </div>
						    	</td> 
						    </tr>
						    <?php }?>
						</table>    
						<!--********************Service End Here Delivery Process******************-->
						<table cellspacing="15" cellpadding="0" border="0" width="90%">
                             <tr>
    							<td colspan="2" nowrap>
    								<h3><u></u></h3>
    							</td>
    					    </tr>
							<tr> 
								<td nowrap style="width:200px;">
									Data-entry Manager (DEM)<font color="red"></font>:
								</td>
								<td>
									<select name="data_entryuser" id="data_entryuser" class="inp_feild" style="width:53%;" tabindex="14">
										<option value="0">Select</option>
                                        <?php
                                        $get_Qry="select * from tbl_users where user_type='DEM' order by username asc";
										$get_Res=mysql_query($get_Qry);
                                        while($get_value=mysql_fetch_array($get_Res))
                                        { 
                                            $SelectedString = "";
                                            $TbldemId = $get_value['user_id'];

                                            if($TbldemId == $dataentryuser)
                                                $SelectedString = "selected";

                                            ?>
                                        <option value="<?php echo $TbldemId;?>" <?php echo $SelectedString;?>><?php echo stripslashes($get_value["username"]);?></option>
                                        <?php
                                        }
                                        ?>  
									</select>
                                </td>
                            </tr>
                            <tr>
								<td nowrap style="width:205px;">
									Customer Service Manager (CSM)<font color="red"></font>:
								</td>
								<td>
									<select name="csm_entryuser" id="csm_entryuser" class="inp_feild" style="width:53%;" tabindex="15">
										<option value="0">Select</option>
                                        <?php
                                        $get_Qry="select * from tbl_users where user_type='CSM' order by username asc";
                                        
                                        $get_Res=mysql_query($get_Qry);
                                        while($get_value=mysql_fetch_array($get_Res))
                                        { 
                                            $SelectedString = "";
                                            $TblcsmId = $get_value['user_id'];

                                            if($TblcsmId == $csmusertype)
                                                $SelectedString = "selected";

                                            ?>
                                        <option value="<?php echo $TblcsmId;?>" <?php echo $SelectedString;?>><?php echo stripslashes($get_value["username"]);?></option>
                                        <?php
                                        }
                                        ?>  
									</select>
                                </td>
                            </tr>
							
                        </table>
                        <?php
                            if($demoId!="")
                            {
                              $tbl_width="88%";
                            } else{
                              $tbl_width="75%";
                            }
                        ?>
                        <table cellspacing="15" cellpadding="0" border="0" width="<?php echo $tbl_width;?> ">
                            <?php
                            if($demoId!="")
                            {
                            ?>
                            <tr>
                                <td colspan="2" nowrap>
                                    <h3><u>HISTORY:</u></h3>
                                </td>
                            </tr>
                            <tr>
                                <td valign="top" nowrap>
                                    Comments:
                                </td>
                                <td>
                                    <table border="0" width="100%" cellspacing="0">
                                        <tr>
                                            <td width="65%">
                                               <textarea name="prospect_comments" id="prospect_comments" class="inp_feild " style="height:200px;font-family: arial;font-size:13px;width:95%;" tabindex="16" onkeyup="show_comment(this.value);" onkeypress="show_comment(this.value);"></textarea> 
                                            </td>
                                            <td  width="20%" valign="top">
                                                 <div class="form_actions">
                                                   <input type="button" value="Add Comment" class="add_btn add_coomment_box" onclick="addcomment('<?php echo addslashes($_SESSION['user_id']);?>','<?php echo $demoId;?>')" style="margin-top: -15px;margin-left: 17px;background-color: gray;border-color:gray;" tabindex="17">
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                    <script>
                                        function addcomment(userid,prospect_id) {
                                            comment=$("#prospect_comments").val();
                                            if ($.trim($("#prospect_comments").val())=="") {
                                                alert("Please enter comments");
                                                $("#prospect_comments").focus();
                                                return false;
                                            } else {
                                                $.ajax({
                                                    type: "POST",
                                                    url: "ajax_comment1.php",
                                                    data: { comment: comment, userid: userid, prospect_id: prospect_id }
                                                }).done(function(r) {
                                                    $('#prospect_comments').val("");
                                                    $("#container").html(r);
                                                    if ($(".myTable tr:first td").length>1) {
                                                        setwidth();
                                                    } else {
                                                        $('#td1').width(150);
                                                        $('#td2').width(395);
                                                        $('#td3').width(150);
                                                    }
                                                });
                                            }
                                        }
                                    </script>
                                    <script>
                                        $(document).ready(function() {
                                            $("#callstatus").trigger('change');
                                            $("#record_type").trigger('change');
                                            setwidth();
                                        });
                                        function setwidth()
                                        {
                                            if ($(".myTable tr:first td").length>1) {
                                                var table = $(".myTable tr:first");
                                                var firstColumnWidth = table.find("td:first").width();
                                                var secodColumnWidth = table.find("td:nth-child(2)").width();
                                                var thirdColumnWidth = table.find("td:nth-child(3)").width();
                                                var table_header = $(".mytable1 tr:first");
                                                $('#td1').width(firstColumnWidth);
                                                $('#td2').width(secodColumnWidth);
                                                $('#td3').width(thirdColumnWidth);
                                            } else {
                                                $('#td1').width(150);
                                                $('#td2').width(395);
                                                $('#td3').width(150);
                                            }
                                        }
                                    </script>
                                </td>
                            </tr>
                            <tr> 
                               <td></td>      
                                <table  cellpadding= "0 " width= "100% " cellspacing= "0 "  style= "border:1px solid #CAE1F9;font-size:13px;color:black;border-collapse: collapse; " border= "1 " class="mytable1">
                                    <tr style="color:white;min-height: 20px;">
                                        <th valign="top"  align="left"  id="td1" nowrap>Date</th>
                                        <th valign="top"  align="left"  id="td2" nowrap>Comment</th>
                                        <th valign="top"  align="left"  id="td3" colspan='2' nowrap>User Name</th>
                                    </tr>
                                </table>
                                <div id="container" style="width:100%;overflow: auto;height:350px;"> 
                                    <table  cellpadding= "0 " width= "100% " cellspacing= "0 "  style= "border:1px solid black;font-size:13px;color:black;border-collapse: collapse; " border= "1 " class="myTable" id="comment_ajax_table">
                                    <?php
                                        $getQry="select * from  tbl_master_comments where master_id=:master_id  order by comment_id DESC";
                                        $prepgetQry=$DBCONN->prepare($getQry);
                                        $prepgetQry->execute(array(":master_id"=>$demoId));
                                        $count =$prepgetQry->rowCount();
                                        if ($count>0) {
                                            $i=1;
                                            while($getRow=$prepgetQry->fetch()) {
                                                if($i%2==0)
                                                    $display_color="#a5a5a5";
                                                else
                                                    $display_color="#d2d1d1";
                                                    $user_id=$getRow["user_id"];
                                                    $get_Qry1="select * from  tbl_users where user_id=:user_id";
                                                    $prepget1_Qry=$DBCONN->prepare($get_Qry1);
                                                    $prepget1_Qry->execute(array(":user_id"=>$user_id));
                                                    $get_Row1=$prepget1_Qry->fetch();
                                                    $user_name=$get_Row1['username'];
                                        ?>
                                        <tr style="background-color:<?php echo $display_color;?>;height: 20px;">    
                                            <td valign="top"  align="left"  >
                                            <?php echo date("d-m-Y", strtotime($getRow["created_date"]));?>
                                            </td>
                                            <td valign="top"  align="left"   style="width:505px;">
                                            <?php 
                                              $service_id=stripslashes(trim($getRow["service_id"]));
                                              $comments=stripslashes(trim($getRow["comment"]));
                                             if($comments=="Progress Table Reseted"){
												$comment=$comments."<a class='viewimg' service_id='".$service_id."'>View</a>";	
											  } else {
												$comment=$comments;
											 } 
                                             ?>
                                             <?php echo $comment; ?>
                                            
                                         
                                            </td>
                                            <td valign="top"  align="left"   >
                                            <?php echo $user_name;?></td>
                                            </tr>
                                        <?php
                                            $i++;
                                            }
                                        } else {
                                          echo "<tr style=\"background-color:#d2d1d1;text-align:center;\"><td colspan=\"3\">No Comment(s) found.</td></tr>";
                                        }
                                    ?>
                                    </table>
                                </div> 
                            </tr>
                            <?php
                            }
                            ?>
                        </table>
                        <table cellspacing="15" cellpadding="0" border="0" width="80%">
                            <tr>
                                <td colspan="2" nowrap>
                                    <h3><u>APP DESIGN:</u></h3>
                                </td>
                            </tr>
                            <tr>
                                <td nowrap>Business Type<font color="red">*</font>:</td>
                                <td>
                                    <select name="business_type"  id="business_type" class="inp_feild" tabindex="18">
                                        <option value="0">Select</option>
                                        <?php
                                        $getbusinessQry="select * from  tbl_business_type order by display_order asc";
                                        $prepget_business_qry=$DBCONN->prepare($getbusinessQry);
                                        $prepget_business_qry->execute();
                                        while($getbusinessRow=$prepget_business_qry->fetch()){
                                        ?>
                                        <option value="<?php echo $getbusinessRow["business_type"];?>" <?php if($business_type==$getbusinessRow["business_type"]){ echo "selected";}?>><?php echo $getbusinessRow["business_type"];?></option>
                                        <?php
                                        }
                                        ?>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td style="width:178px" nowrap>App Name:</td>
                                <td>
                                    <input type="text" name="app_name" id="app_name" class="inp_feild" value="<?php echo $app_name;?>" tabindex="19">
                                </td>
                            </tr>
                            <?php 
                             if ($theme!="" && $theme>0) {
                            ?>
                            <tr class="theme_div_demo">
                                <td style="width:138px;" valign="top"> 
                                </td>
                                <td align="left" id="dev_info_design">
                                <img src="themes/<?php echo $theme?>.png" style="margin:auto;height:150px;"> 
                                 </td>
                            </tr>
                            <?php 
                            }
                            ?>
                            <tr>
                                <td nowrap>Default Langauage<font color="red">*</font>:</td>
                                <td>
                                    <select name="def_language" id="def_language" class="inp_feild" tabindex="20">
                                        <option value="0"<?php if($default_langauage=="0") echo " selected";?>>Select</option>
                                        <?php
                                            $getBookQry="select * from tbl_language";
                                            $getBookRes=mysql_query($getBookQry);
                                            
                                            while($getBookRow=mysql_fetch_array($getBookRes)){
                                        ?>
                                            <option value="<?php echo stripslashes($getBookRow["language"]);?>"<?php if($default_langauage==$getBookRow["language"]) echo " selected";?>><?php echo stripslashes($getBookRow["language"]);?></option>
                                        <?php
                                            }
                                        ?>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td nowrap> Store Category: </td>
                                <td>
                                    <select name="itunes_category" id="itunes_category" class="inp_feild" tabindex="21">
                                        <option value="0">Select</option>
                                        <?php
                                            $getituneQry="select * from  tbl_itune_category order by category_id asc";
                                            $getituneRes=mysql_query($getituneQry);
                                            while($getituneRow=mysql_fetch_array($getituneRes)){
                                            ?>
                                            <option value="<?php echo $getituneRow["category"]?>"<?php if($itunes_category==$getituneRow["category"]) echo " selected";?>><?php echo $getituneRow["category"]?></option>
                                        <?php
                                            }
                                        ?>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td valign="top" nowrap> App Modifications: </td>
                                <td width="100%">
                                   <textarea name="app_modifications" id="app_modifications" class="inp_feild" style="height:210px;font-family: arial;font-size:13px;width:100%;" tabindex="22" ><?php echo $app_modifications;?></textarea> 
                                </td>                               
                            </tr>
                            <tr>
                                <td style="width:178px" valign='top' nowrap> Business Logo: </td>
                                <td>
                                    <input type="file" name="business_logo" id="business_logo" class="inp_feild" tabindex="23">
                                    <?php 
                                    if(is_file("../".$business_logo))
                                    {
                                        $ExplodeBLogo = explode(".", $business_logo);
                                        $BLFileExtenstion = strtolower($ExplodeBLogo[1]);

                                        if($BLFileExtenstion == 'jpg' || $BLFileExtenstion == 'jpeg' || $BLFileExtenstion == 'gif' || $BLFileExtenstion == 'png')
                                        {
                                            ?>
                                            <br><br>
                                            <div id="deleteimagelogo">
                                            <img src="<?php echo "../".$business_logo?>" style="width:75px;height:75px;"> <img src="images/cross.png" class="deleteimage" data-img='<?php echo $business_logo; ?>' data-type="business_logo" data-deid="<?php echo $demoId ?>" style="cursor: pointer;margin-left: 10px;border: 2px solid white;border-radius: 12px;padding: 3px 3px;vertical-align: top;" title="Delete">
                                            </div>
                                            
                                            <?php
                                        }
                                        else                                            
                                        {
                                            ?>
                                             <br><br>
                                             <a href="<?php echo  HTTP_ROOT_FOLDER.$business_logo;?>" target="_blank" >Download Business Logo</a>
                                            <?php
                                        }
                                            
                                     }                                              
                                    ?>                                                              
                                </td>
                            </tr>
                            <tr>
                                <td style="width:178px" valign='top' nowrap> Background Image1: </td>
                                <td>
                                    <input type="file" name="background_image" id="background_image" class="inp_feild" tabindex="24">
                                    <?php 
                                    if(is_file("../".$background_image))
                                    {
                                        $ExplodeBGImage = explode(".", $background_image);
                                        $BGFileExtenstion = strtolower($ExplodeBGImage[1]);

                                        if($BGFileExtenstion == 'jpg' || $BGFileExtenstion == 'jpeg' || $BGFileExtenstion == 'gif' || $BGFileExtenstion == 'png')
                                        {
                                            ?>
                                            <br><br><div id="deleteimage1"><img src="<?php echo "../".$background_image?>" style="width:75px;height:75px;">
                                              <img src="images/cross.png" class="deleteimage" data-img='<?php echo $background_image; ?>' data-type="background_image" data-deid="<?php echo $demoId ?>" style="cursor: pointer;margin-left: 10px;border: 2px solid white;border-radius: 12px;padding: 3px 3px;vertical-align: top;" title="Delete">
                                              </div>
                                            <?php
                                        }                                   
                                            
                                     }                                              
                                    ?>                                                              
                                </td>
                            </tr>
                            <tr>
                                <td style="width:178px" valign='top' nowrap> Background Image 2:
                                </td>
                                <td>
                                    <input type="file" name="background_image2" id="background_image2" class="inp_feild" tabindex="25">
                                    <?php 
                                    if(is_file("../".$background_image2))
                                    {
                                        $ExplodeBGImage2 = explode(".", $background_image2);
                                        $BGFileExtenstion2 = strtolower($ExplodeBGImage2[1]);

                                        if($BGFileExtenstion2 == 'jpg' || $BGFileExtenstion2 == 'jpeg' || $BGFileExtenstion2 == 'gif' || $BGFileExtenstion2 == 'png')
                                        {
                                            ?>
                                            <br><br><div id="deleteimage2"><img src="<?php echo "../".$background_image2?>" style="width:75px;height:75px;"><img src="images/cross.png" class="deleteimage" data-img='<?php echo $background_image2; ?>' data-type="background_image2" data-deid="<?php echo $demoId ?>" style="cursor: pointer;margin-left: 10px;border: 2px solid white;border-radius: 12px;padding: 3px 3px;vertical-align: top;" title="Delete"></div>
                                            <?php
                                        }                                   
                                            
                                     }                                              
                                    ?>                                                              
                                </td>
                            </tr>
                            <tr>
                                <td style="width:178px" valign='top' nowrap>
                                    Background Image 3:
                                </td>
                                <td>
                                    <input type="file" name="background_image3" id="background_image3" class="inp_feild" tabindex="26">
                                    <?php 
                                    if(is_file("../".$background_image3))
                                    {
                                        $ExplodeBGImage3 = explode(".", $background_image3);
                                        $BGFileExtenstion3 = strtolower($ExplodeBGImage3[1]);

                                        if($BGFileExtenstion3 == 'jpg' || $BGFileExtenstion3 == 'jpeg' || $BGFileExtenstion3 == 'gif' || $BGFileExtenstion3 == 'png')
                                        {
                                            ?>
                                            <br><br><div id="deleteimage3"><img src="<?php echo "../".$background_image3?>" style="width:75px;height:75px;"><img src="images/cross.png" class="deleteimage" data-img='<?php echo $background_image3; ?>' data-type="background_image3" data-deid="<?php echo $demoId ?>" style="cursor: pointer;margin-left: 10px;border: 2px solid white;border-radius: 12px;padding: 3px 3px;vertical-align: top;" title="Delete"></div>
                                            <?php
                                        }                                   
                                            
                                     }                                              
                                    ?>                                                              
                                </td>
                            </tr>
                            <tr>
                                <td style="width:178px" valign='top' nowrap>
                                    Background Image 4:
                                </td>
                                <td>
                                    <input type="file" name="background_image4" id="background_image4" class="inp_feild" tabindex="27">
                                    <?php 
                                    if(is_file("../".$background_image4))
                                    {
                                        $ExplodeBGImage4 = explode(".", $background_image4);
                                        $BGFileExtenstion4 = strtolower($ExplodeBGImage4[1]);

                                        if($BGFileExtenstion4 == 'jpg' || $BGFileExtenstion4 == 'jpeg' || $BGFileExtenstion4 == 'gif' || $BGFileExtenstion4 == 'png')
                                        {
                                            ?>
                                            <br><br><div id="deleteimage4"><img src="<?php echo "../".$background_image4?>" style="width:75px;height:75px;"><img src="images/cross.png" class="deleteimage" data-img='<?php echo $background_image4; ?>' data-type="background_image4" data-deid="<?php echo $demoId ?>" style="cursor: pointer;margin-left: 10px;border: 2px solid white;border-radius: 12px;padding: 3px 3px;vertical-align: top;" title="Delete"></div>
                                            <?php
                                        }                                   
                                            
                                     }                                              
                                    ?>                                                              
                                </td>
                            </tr>
                            <tr>
                                <td valign="top" nowrap>
                                    Background RGB:
                                </td>
                                <td>
                                    <input type="text" name="Background_RGB" id="Background_RGB" class="inp_feild" value="<?php echo $Background_RGB;?>" tabindex="28">
                                </td>                           
                            </tr> 
                            <tr>
                                <td colspan="2" nowrap>
                                    <h3><u>APP BUILDS</u></h3>
                                </td>
                            </tr>
                            <tr>
                                <td style="width:178px" valign='top' nowrap>
                                    IOS App (IPA):
                                </td>
                                <td>
                                    <input type="file" name="ios_app_ipa" id="ios_app_ipa" class="inp_feild" tabindex="29"> 
                                    <?php if($ios_ipa_apk!="") { ?>
                                    <br><br><div id="ios_ipa_apk">
                                      <a href="<?php echo  HTTP_ROOT_FOLDER.$ios_ipa_apk;?>" title="Download IPA File"><img src="images/ipa.png" style="width:75px;height:75px;"></a><img src="images/cross.png" class="deleteimage" data-img='<?php echo $ios_ipa_apk; ?>' data-type="ios_ipa_apk" data-deid="<?php echo $demoId ?>" style="cursor: pointer;margin-left: 10px;border: 2px solid white;border-radius: 12px;padding: 3px 3px;vertical-align: top;" title="Delete">
                                    </div>
                                    <?php }?>                                                   
                                </td>
                            </tr>
                            <tr>
                                <td style="width:178px" valign='top' nowrap>
                                    Android App  (APK): 
                                </td>
                                <td>
                                    <input type="file" name="andorid_app_ipa" id="andorid_app_ipa" class="inp_feild" tabindex="30">
                                        <?php if($andorid_ipa_apk!="") { ?>
                                        <br><br><div id="andorid_ipa_apk">
                                            <a href="<?php echo  HTTP_ROOT_FOLDER.$andorid_ipa_apk;?>" title="Download APK File"><img src="images/apk.png" style="width:75px;height:75px;"></a><img src="images/cross.png" class="deleteimage" data-img='<?php echo $andorid_ipa_apk; ?>' data-type="andorid_ipa_apk" data-deid="<?php echo $demoId ?>" style="cursor: pointer;margin-left: 10px;border: 2px solid white;border-radius: 12px;padding: 3px 3px;vertical-align: top;" title="Delete">
                                        </div>
                                    <?php }?>                   
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" nowrap>
                                    <h3><u>SYSTEM INFO:</u></h3>
                                </td>
                            </tr>
                            <tr>
                                <td style="width:138px;" valign="top" nowrap>
                                    Product Page<font color="red">*</font>:
                                </td>
                                <td>
                                    <select name="product_page" id="product_page" class="inp_feild" style="width:50%;" tabindex="31" <?php if($mode == 'Edit'){echo 'disabled';} ?>>
                                        <option value="0">Select</option>
                                        <?php
                                        if($_SESSION['user_id']=="3" || $user_type=="Admin")
                                        {
                                                $getQry_1="SELECT  DISTINCT `product_page` FROM `tbl_apps`  order by `product_page` asc";
                                                $prepgetQry1=$DBCONN->prepare($getQry_1);
                                                $prepgetQry1->execute();
                                                while($getRow1=$prepgetQry1->fetch()){
                                                   $y=$getRow1["product_page"];
                                                   echo "<option value='".$y."'>".$y."</option>";   
                                                }

                                        }
                                        else
                                        {
                                                $getQry_1="SELECT * FROM  tbl_affiliate where a_user_id='".$_SESSION['user_id']."'";
                                                $prepgetQry1=$DBCONN->prepare($getQry_1);
                                                $prepgetQry1->execute();
                                                while($getRow1=$prepgetQry1->fetch()){
                                                   $x=$getRow1["product_page1"];
                                                   $y=$getRow1["product_page2"];
                                                   $z=$getRow1["product_page3"];
                                                   if(!empty($x)){
                                                      echo "<option value='".$x."'>".$x."</option>";
                                                   }
                                                    if(!empty($y)){
                                                      echo "<option value='".$y."'>".$y."</option>";
                                                   }
                                                     if(!empty($z)){
                                                      echo "<option value='".$z."'>".$z."</option>";
                                                   }


                                                }
                                        }
                                        
                                        ?>
                                        
                                    </select>
                                    <?php
                                    if($product_page_val!=""){
                                    ?>
                                    <script language="javascript">document.getElementById("product_page").value="<?php echo $product_page_val;?>"</script>
                                    <?php
                                    }
                                    ?>
                                </td>
                            </tr>
                            <tr>
                                <td style="width:138px;" nowrap>
                                     Promo Code<font color="red">*</font>:
                                </td>
                                <td>
                                    <input type="text" name="promo_code" id="promo_code" class="inp_feild" value="<?php echo $promo_code;?>"  tabindex="32" <?php if($mode == 'Edit'){echo 'readonly';} ?> style="width:50%">
                                </td>
                            </tr>
                            <tr>
                                <td style="width:138px;" valign="top" nowrap></td>
                                <td>
                                    <input type="checkbox" name="simulator"   id="simulator"  value="1" <?php if($simulator=="1"){echo "checked";}?> onclick="show(this.id)" tabindex="33"> Add Simulator <br>
                                </td>
                            </tr>
                            <tr class="theme_div" style="margin-top:5px;<?php echo $style;?>">
                                <td valign="top" nowrap>
                                    Simulator Logo:
                                </td>
                                <td>
                                    <input type="file" name="restaurant_logo" id="restaurant_logo" class="inp_feild" value="<?php echo $restaurant_name;?>" tabindex="34">
                                    
                                    <?php 
                                    if(is_file($restaurant_logo))
                                    {
                                        $ExplodeLogo = explode(".", $restaurant_logo);
                                        $FileExtenstion = strtolower($ExplodeLogo[1]);

                                        if($FileExtenstion == 'jpg' || $FileExtenstion == 'jpeg' || $FileExtenstion == 'gif' || $FileExtenstion == 'png')
                                        {
                                            ?>

                                            <br><br><div id="restaurantlogo"><img src="<?php echo "../".$restaurant_logo?>" style="width:75px;height:75px;"><img src="images/cross.png" class="deleteimage" data-img='<?php echo $restaurant_logo; ?>' data-type="restaurant_logo" data-deid="<?php echo $demoId ?>" style="cursor: pointer;margin-left: 10px;border: 2px solid white;border-radius: 12px;padding: 3px 3px;vertical-align: top;" title="Delete"></div>

                                            <?php
                                        }
                                        else                                            
                                        {
                                            ?>
                                             <br><br>
                                             <a href="<?php echo  HTTP_ROOT_FOLDER.$old_restaurant_logo;?>" target="_blank" >Download Stimulator Logo</a>
                                            <?php
                                        }
                                            
                                     }                                              
                                    ?>
                                </td>
                            </tr>
                            <tr class="theme_div_demo" style="display:none" >
                                <td style="width:138px;" valign="top"> </td>
                                <td align="left" id="theme_div_design"></td>
                            </tr>
                            <tr class="theme_div" style="margin-top:5px;<?php echo $style;?>" >
                                <td style="width:138px;" valign="top" nowrap> 
                                    Design Style<font color="red">*</font>: 
                                </td>
                                <td>
                                    <select name="theme" id="theme" class="inp_feild" tabindex="35" onchange="show_demo_image(this.value);">
                                        <option value="0">Select</option>
                                        <?php
                                        foreach($themes_array as $val=>$name){ 
                                          echo '<option value="'.$val.'">'.$name.'</option>'; 
                                        } 
                                        ?>
                                    </select>
                                    <?php 
                                        if ($theme!="" && $theme>0) {
                                        ?>
                                        <script type="text/javascript">
                                           $("#theme").val('<?php echo $theme;?>')
                                        </script>
                                        <?php 
                                        }
                                    ?>
                                </td>
                            </tr>
                            <tr>
                                <td style="width:138px;" nowrap>
                                    Simulator Creator<font color="red">*</font>:
                                </td>
                                <td>
                                    <input type="text" name="user_id" id="user_id" class="inp_feild" tabindex="36" value="<?php echo stripslashes($created_user);?>"  readonly >
                                </td>
                            </tr>
                            <tr>
                                <td style="width:138px;" valign="top" nowrap>
                                    Affiliate:
                                </td>
                                <td>
                                    <select name="affiliate" id="affiliate" class="inp_feild"  onchange="get_Licensee_details_v1(this.value)"   style="width:50%;" tabindex="37">
                                        <option value="0">Select</option>
                                        <?php
                                        $get_Qry="select * from tbl_affiliate where 1=1 ";
                                        
                                        $get_Res=mysql_query($get_Qry);
                                        while($get_value=mysql_fetch_array($get_Res))
                                        { 
                                            $SelectedString = "";
                                            $TblAffiliateId = $get_value['affiliate_id'];

                                            if($TblAffiliateId == $affiliate_id)
                                                $SelectedString = "selected";

                                            ?>
                                        <option value="<?php echo $TblAffiliateId;?>" <?php echo $SelectedString;?>><?php echo stripslashes($get_value["full_name"]);?></option>
                                        <?php
                                        }
                                        ?>  
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td valign="top" nowrap>
                                    Licensee:
                                </td>
                                <td>
                                    <select name="licensee" id="licensee" class="input_field" style="width:50%;" tabindex="38" disabled>
                                        <option value="0">Select</option>                   
                                        <?php
                                        $getbusinessQry="select * from  tbl_licensee order by licensee_id asc";
                                        $prepget_business_qry=$DBCONN->prepare($getbusinessQry);
                                        $prepget_business_qry->execute();
                                        while($getbusinessRow=$prepget_business_qry->fetch()){
                                        ?>

                                        <option value="<?php echo $getbusinessRow["licensee_id"];?>" <?php if($Licensee==$getbusinessRow["licensee_id"]) echo " selected";?>><?php echo $getbusinessRow["business_name"];?></option>
                                        <?php
                                        }
                                        ?>
                                    </select>
                                        <input type="hidden" id="hdn_licensee" name="hdn_licensee" value="<?php echo $Licensee;?>">
                                </td>
                            </tr>
                            <tr>
                                <td nowrap>
                                    Upload Logo Link:
                                </td>
                                <td id="upload_logo_link">
                                    <a href="<?php echo "http://www.myappyrestaurant.com/upload_logo.php?id=".$demoId?>" target="_blank">http://www.myappyrestaurant.com/upload_logo.php?id=<?php echo $demoId;?></a>&nbsp;&nbsp;&nbsp;
                                </td>
                                <td>
                                <!--<div class="form_actions" style="text-align:right;" id="upd_pros_btn">
                                    <input type="button" value="SEND WELCOME EMAIL" class="uplaod_url" id="send_mail"  onclick="upload_logo_url();">  
                                    </div> -->
                        
                                </td>
                            </tr>
                        </table>
                        <table cellspacing="15" cellpadding="0" border="0" width="70%">
                            <tr>
    							<td colspan="2" nowrap>
    								<h3><u>SALES INFO:</u></h3>
    							</td>
    					    </tr>
							<tr>
								<td nowrap>
									Call Status<font color="red">*</font>:
								</td>
								<td>
									<select name="callstatus" id="callstatus" class="inp_feild" style="width:53%;margin-left:50px;" tabindex="39"  <?php if($mode == 'Edit'){echo 'disabled';} ?>>
										<option value="0">Select</option>
										<?php
											$getcallstatusQry="select * from  tbl_callstatus order by display_order asc";
											$getcallstatusRes=mysql_query($getcallstatusQry);
											$callStsArr="";
											while($getcallstatusRow=mysql_fetch_array($getcallstatusRes)){
												if($dbpush=='yes'){
													if($getcallstatusRow["callstatus"]=='Trial requested' || $getcallstatusRow["callstatus"]=='Activation email sent (follow-up)' || $getcallstatusRow["callstatus"]=='Payment processed' || $getcallstatusRow["callstatus"]=='Trial not sold' ||  $getcallstatusRow["callstatus"]=='Promo email sent (follow-up)'){
														$callStsArr[]=stripslashes($getcallstatusRow["callstatus"]);
													}
												} else {
													if($getcallstatusRow["callstatus"]!='Activation email sent (follow-up)' && $getcallstatusRow["callstatus"]!='Trial not sold'){
														$callStsArr[]=stripslashes($getcallstatusRow["callstatus"]);
													}
												}
											}
										foreach ($callStsArr as $stsval) {
										?>
											<option value="<?php echo $stsval;?>"<?php if($callstatus==$stsval) echo " selected";?>><?php echo $stsval;?></option>
										<?php
										}
										?>
									</select>
                                </td>
                            </tr>
							<tr>
							    <td nowrap>Callback Date:&nbsp;&nbsp;&nbsp;</td>
								<td>
                                    <input type="text" name="demo_date" id="demo_date" class="inp_feild" style="width:25%;margin-left:50px;" value="<?php echo $demo_date;?>" tabindex="40">&nbsp;&nbsp;&nbsp;Time:&nbsp;&nbsp;&nbsp;<input type="text" name="demo_time" id="demo_time" class="inp_feild" style="width:25%;" value="<?php echo $demo_time?>" tabindex="41">
                                </td>
							 </tr>
                        </table>
                        <table cellspacing="15" cellpadding="0" border="0" width="65%">
                            <tr>
								<td colspan="2" nowrap>
									<h3><u>AD-HOC PAYMENT INFORMATION</u></h3>
								</td>
							</tr>
							<tr>
								<td valign="top" nowrap>
									Item Description:
								</td>
								<td width="100%">
									<textarea name="item_description" id="item_description" class="inp_feild" style="height:50px;font-family: arial;font-size:13px;width:90%;margin-left:32px;" tabindex="42" ><?php echo $item_description;?></textarea> 
								</td>								
							</tr>
							<tr>
								<td nowrap>
									One-off Price:
								</td>
								<td>
									<input type="text" name="one_offprice" id="one_offprice" class="inp_feild" style="width:50%;margin-left:32px;" value="<?php echo $one_offprice;?>" tabindex="43"> 
								</td>
							</tr>
							<tr>
								<td nowrap>
									Recurring Price:
								</td>
								<td>
									<input type="text" name="recrring_price" id="recrring_price" class="inp_feild" style="width:50%;margin-left:32px;" value="<?php echo $recrring_price;?>" tabindex="44"> (per month)
								</td>
							</tr>
							<tr>
			                    <td nowrap>
									Currency:
								</td>
								<td>
									<select name="currency" id="currency" class="input_field" style="width:50%;margin-left:32px;" tabindex="45">
										<option value="0">Select</option>
										<?php
											foreach($currencyCodes as $currency => $code)
											{
										   ?>
											<option value="<?php echo stripslashes($code);?>"<?php if($code==$pro_currency) echo " selected";?>><?php echo stripslashes($currency);?></option>
										<?php
											}
										?>
									</select>
										<script>$('#currency').val("<?php echo $currency_code?>")</script>
								</td>
							</tr> 
							<tr>
							    <td nowrap>
									Tax Code / Rate(%):
								</td>
								<td>
									<input type="text" name="tax_code" id="tax_code" class="inp_feild" style="width:20%;margin-left:32px;" value="<?php echo $payment_taxcode;?>" tabindex="46">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
									<input type="text" name="tax_rate" id="tax_rate" class="inp_feild" style="width:20%;" value="<?php echo $payment_taxrate;?>" tabindex="47"> %
								</td>
							</tr>
                            <tr>
                                <td nowrap>
                                    Including Tax:
                                </td>
                                <td>
                                    <input type="checkbox" id="includetax" name="includetax"  value="1" style="margin-left:32px;" tabindex="48" <?php if($include_tax=="1"){echo "checked";}?> ><br>
                                   
                                </td>
                            </tr>
							<tr>
                                <td nowrap colspan='2'>
                                	<input type="button" name="btn_generate_payment_link" id="btn_generate_payment_link" value="GENERATE PAYMENT URL" onclick="generate_payment_url();">
                                </td>
                            </tr>
							<tr>
								<td nowrap>
									Payment Page URL:
								</td>
								<td id="payment_page_url"></td>
								
							</tr>
							<tr>
								<td colspan="2">
									<table cellspacing="0" cellpadding="0" width="100%" border="0">
										<tr>
											<td>
											    <div class="form_actions">
											      <input type="button" value="Back To Master List" class="add_btn"   onclick="document.location='masterlist.php'">
											    </div>
											</td>
											<td>
											    <div class="form_actions" style="text-align:right;" id="upd_pros_btn">
												    <input type="button" value="<?php echo ucfirst($value);?> Record" class="add_btn add_demo" id="add_demo">  
												</div>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
						<?php
						if($affiliate_id!=""){
						?>
						<script> 
						get_Licensee_details('<?php echo $affiliate_id;?>');
						</script>
						<?php
						}
						?>
                    </form> 
				</div>
			</div>
		</div>
    </div>
	<style>
	 	.border_right {
	 		max-width: 0px;
	 		padding:0px !important;
	 		border-right: 1px solid white;
	 	}
		.uplaod_url{
			cursor: pointer;border-radius: 80px;background: #ff4215;
			color: #D9D9D9;
			border-color: #ff4215;
			padding: 5px 5px 5px 5px;
			outline: none;
			font-weight: bold;
		}
		.fancybox-custom .fancybox-skin {
			box-shadow: 0 0 50px #222;
		}
		.fancybox-close {
			position: absolute;
			top: -1px;
			right: -1px;
			width: 36px;
			height: 36px;
			cursor: pointer;
			z-index: 8040;
        }
        .service_color{
    	   color:black;
    	   font-size: 12px;
    	   font-weight: 800;
    	   background-color: #FFc000;
    	   border: 1px solid #FFc000;
    	   outline: none;
    	}
	    .content_table tr {
	       border-bottom: 1px solid white;
        }
	    .padding{
	        padding-left: 15px;
	    }
	    .padding_left{
	        padding-left: 14px;
	    }
	    .popup_btn{
            cursor: pointer;
            border-radius: 80px;
            background: #ff4215;
            color: #D9D9D9;
            border-color: #ff4215;
            padding: 4px 4px 4px 4px;
            outline: none;
            font-weight: bold;margin-left:30px;border-radius:4px;height:30px;"
        }
        .group {
            position: relative;
        }

        .group textarea, .group .Prefix {
            font-size: 14px;
            font-family: Arial, serif;
            color:black;
        }

        .Prefix {
            position: absolute;
            top: 22px;
            left: 3px;
            z-index: 2;
        }

        .group textarea {
            width:390px;
            height: 270px;
        }
	</style>
	<div id="inline2" style="width:400px;display: none;">
        <h3 class="payment_head" style="display: none;">DEV'T Payment Log</h3>
        <h3 class="payment_head1" style="display: none;">DEM Payment Log</h3>
        <h3 class="payment_head2" style="display: none;">CMS Payment Log</h3>
        <h3 class="support_head" style="display: none;">Support Call</h3>
        <h3 class="market_head" style="display: none;">Marketing Service</h3>
        <form id="frm_payment" name="frm_payment" method="post" action="" style="display:none;">
            <input type="hidden" name="hnd_type_dev" id="hnd_type_dev">
            <input type="hidden" name="hnd_listid_dev" id="hnd_listid_dev">
            <div>
              <label style="padding-top:10px;">Payment amount :</label>
              <input type="text" name="dev_amount" id="dev_amount" style="border-radius:4px;height:30px;width: 383px;background: #B8B8B8;">
              <label id="error-devamount" style="text-align:center;padding-left: 0px;display:none;color:red">Please enter amount</label>
            </div>
            <div style="padding-top:10px;">
              <label>Payment Date :</label>
              <input type="text" name="dev_date1" id="dev_date1" calss="dev_date" style="width: 383px;margin-left:0px;border-radius:4px;height:30px;background: #B8B8B8;" value="">
              <label id="error-devdate" style="text-align:center;padding-left: 0px;display:none;color:red">Please select date</label>
            </div>
            <div style="text-align:center;padding-top:10px;">
              <input type="button" value="submit" class="popup_btn dev_payment">
            </div>
        </form>
        <form id="frm_cmspayment" name="frm_cmspayment" method="post" action="" style="display:none;">
            <input type="hidden" name="hnd_type_cms" id="hnd_type_cms">
            <input type="hidden" name="hnd_listid_cms" id="hnd_listid_cms">
            <div>
               <label style="padding-top:10px;">Payment amount :</label>
               <input type="text" name="cms_amount" id="cms_amount" style="border-radius:4px;height:30px;width: 390px;background: #B8B8B8;">
               <label id="error-cmsamount" style="text-align:center;padding-left: 0px;display:none;color:red">Please enter amount</label>
            </div>
            <div style="padding-top:10px;">
               <label>Payment Date :</label>
               <input type="text" name="cms_date" id="cms_date" style="width:393px;margin-left:0px;border-radius:4px;height:30px;background: #B8B8B8;">
               <label id="error-cmsdate" style="text-align:center;padding-left: 0px;display:none;color:red">Please select date</label>
            </div>
            <div style="text-align:center;padding-top:10px;">
                <input type="button" value="submit" class="popup_btn cms_payment">
            </div>
        </form>
        <form id="frm_dempayment" name="frm_dempayment" method="post" action="" style="display:none;">
            <input type="hidden" name="hnd_type_dem" id="hnd_type_dem">
            <input type="hidden" name="hnd_listid_dem" id="hnd_listid_dem">
            <div>
                <label style="padding-top:10px;">Payment amount :</label>
                <input type="text" name="dem_amount" id="dem_amount" style="border-radius:4px;height:30px;    width: 383px;background: #B8B8B8;">
                <label id="error-demamount" style="text-align:center;padding-left: 0px;display:none;color:red">Please enter amount</label>
            </div>
            <div style="padding-top:10px;">
                <label>Payment Date :</label>
                <input type="text" name="dem_date" id="dem_date" style="width: 383px;margin-left:0px;border-radius:4px;height:30px;background: #B8B8B8;">
                <label id="error-demdate" style="text-align:center;padding-left: 0px;display:none;color:red">Please select date</label>
            </div>
            <div style="text-align:center;padding-top:10px;">
                <input type="button" value="submit" class="popup_btn dem_payment">
            </div>
        </form>

        <form id="frm_comment" name="frm_comment" method="post" action="" style="display:none;">
            <input type="hidden" name="hnd_type_comment" id="hnd_type_comment">
            <input type="hidden" name="hnd_listid_comment" id="hnd_listid_comment">
            <div class="group">
               <label style="padding-top:10px;">Comments :</label>
               <label for="field" class="Prefix">Prefix </label>
               <textarea type="text" name="comment_popup" id="comment_popup" style="border-radius:4px;background: #B8B8B8;"></textarea>
               <p class="comment_error" style="padding-left: 0px;display:none;color:red;">Please add comments above</p>
            </div>
          
            <div style="text-align:center;padding-top:10px;">
               <input type="button" value="submit" class="popup_btn comments_sup_market">
            </div>
        </form>
    </div>
    <p class="fancybox" href="#inline1"></p>
    <div id="inline1" style="width:1000px;display: none;height:480px;background: #2661a7;color:white;">
       <div id="serviceidpopup" style="margin-top:10px;"> </div>
		
	</div>
	<!-- <div id="inline1" style="width:720px;display: none;height: 62px;">
		<p style="color:rgb(142, 180, 227);font-size:15px;text-align:center;padding: 15px;">
			Sorry, cannot send welcome email until you have completed both status fields and both submit date fields
		</p>
	</div>
	 -->

<?php
include("includes/footer.php");
}
?>
<script>
    $(document).ready(function() {
            var generate_payment_url="<?php echo $generate_payment_url?>";
    	    if (generate_payment_url!="") {
    		    $("#payment_page_url").html('<a href="'+generate_payment_url+'" target="_blank">'+generate_payment_url+'</a>');
            }
    		/*
    		 *  Simple image gallery. Uses default settings
    		 */
            $('.fancybox').fancybox();
        });
        function generate_payment_url()
        {
        	var hdn_demo_id	= $("#hdn_demo_id").val();
        	var item_description = $("#item_description").val();
        	var oneoffprice	= $("#one_offprice").val();
        	var recrringprice = $("#recrring_price").val();
        	var currency = $("#currency").val();
        	var tax_code = $("#tax_code").val();
        	var taxrate	= $("#tax_rate").val();
            var includetax = 0;
            if ($('#includetax').is(":checked")) {
                includetax = $("#includetax").val();
            }
    	    $.ajax({
                type: "POST",
                url: "update_adhoc_payments.php",
                data: { demo_id : hdn_demo_id, item_description : item_description, oneoffprice: oneoffprice, recrringprice: recrringprice, currency: currency, tax_code: tax_code, taxrate: taxrate,includetax:includetax },
                cache: false,
                success: function(result){
                    if (result == 'error') {
            			$("#payment_page_url").html('Error in genenerating payment URL'); 
            		} else {
            			var payment_pageurl = 'http://www.myappybusiness.com/adhoc_payment.php?id='+result;
            			$("#payment_page_url").html('<a href="'+payment_pageurl+'" target="_blank">'+payment_pageurl+'</a>');
            		}
                }
    	    });
        }

        function upload_logo_url()
        {
        	var hdn_demo_id			            = $("#hdn_demo_id").val();
        	//var development_info_status			= $("#development_info_status").val();
        	//var development_info_android_status	= $("#development_info_android_status").val();
        	//var iphone_date			            = $("#iphone_date").val();
        	//var android_date			        = $("#android_date").val();
        	//if( hdn_demo_id!="" && development_info_status!="" && iphone_date!="" && android_date!=""){
    	    if(hdn_demo_id!=""){
        	    $.ajax({
                    type: "POST",
                    url: "welcome_email.php",
                    data: { demo_id : hdn_demo_id},
                    cache: false,
                    success: function(result){
                        if(result == 'error')
                		{
                			//$("#payment_page_url").html('Error in genenerating payment URL'); 
                		}
                		else
                		{
                			//var payment_pageurl = 'http://www.myappybusiness.com/adhoc_payment.php?id='+result;
                			//$("#payment_page_url").html('<a href="'+payment_pageurl+'" target="_blank">'+payment_pageurl+'</a>');
                		}
                    }
        	    });
    	    } else {
    		  $('.fancybox').trigger('click');	
    	}
    }
    function get_Licensee_details_v1(affiliate_id) {
    	$("#affiliate").val(affiliate_id);
    		$.ajax({url:"get_licensee_details.php?affiliate_id="+affiliate_id,success:function(result)
    		{
    			if(result!=""){
                   $("#licensee").val(result);
    			   $("#hdn_licensee").val(result);				   
    			}
    			$("#affiliate").val(affiliate_id);
    		}
        });
    }
    $(document).on("blur","#one_offprice,#recrring_price",function(){
    	var oneoffprice=$("#one_offprice").val();
    	var recrringprice=$("#recrring_price").val();
    	var taxrate=$("#tax_rate").val();
    	if(oneoffprice!=""){
    		$("#one_offprice").val(parseFloat(oneoffprice).toFixed(2));
    	}
    	if(recrringprice!=""){
    		$("#recrring_price").val(parseFloat(recrringprice).toFixed(2));
        }
    	/*if(taxrate!=""){
    		$("#tax_rate").val(parseFloat(taxrate).toFixed(2));
        }*/
    });
</script>

<script>
//reset resetuser
    $(document).on("click",".ios_built_log,.andorid_built_log,.ios_test_log,.andorid_test_log,.ios_submitted_log,.andorid_submitted_log,.ios_live_log,.andorid_live_log, .app_designed_log, .app_populated_log,.welcome_call_log,.app_training_log,.btn_reset",function(){
        var type=$(this).attr("data-id");
        var list_id=$(this).attr("list-id");
        var usertype=$(this).attr("user-type"); 
        if(type!=""){
            	
            if(type=="ios built"){
        		msg="Are you sure the iPhone app has been built?"
        	}
        	if(type=="andorid built"){
        		msg="Are you sure the Android app has been built?"
        	}
        	if( type=="app_iostested"){
        		msg="Are you sure the iPhone app has been tested?"
        	} 
            if(type=="app_andoridtested"){
                msg="Are you sure the Android app has been tested?"
            }

        	if(type=="app_iossumbmit" ){
        		msg="Are you sure this app has been submitted to the iTunes Store?"
        	}
            if(type=="app_andoridsumbmit"){
                msg="Are you sure this app has been submitted to the Google Play Store?" 
            }
        	if(type=="app_ioslive" ){
        		msg="Are you sure this app is live on the iTunes Store?"
        	} 
            if(type=="app_andoridlive"){
                msg="Are you sure this app is live on the Google Play Store?"
            }
        	if(type=="app_desiged"){
        		msg="Are you sure the design stage is completed for this app?"
        	}
        	if(type=="app_populated"){
        		msg="Are you sure this app has been populated?"
        	}
            if(type=="app_welcomecall"){
        		msg='Are you sure the customer "Welcome" call has been completed?'
        	}
        	if(type=="app_training"){
        		msg="Are you sure the app training has been completed?"
        	}
        	if(type=="reset"){ 
        		msg="Are you sure want to this reset progress table?"
        	}
            var check = confirm(msg);
    	   if (check == true) {
                $.ajax({
                    url:"service_progress_ajax.php",  
                    method:'GET',
                    data:"post_type="+type+"&list_id="+list_id,
                    success:function(data) { 
                    	//alert(data);
                        var responseArr=data.split("##^^##");
                        if(responseArr[0]=="Success"){
                        	var servicename=responseArr[1];
                        	var servicedate=responseArr[2];
                        	if(type=="reset" && usertype=="resetuser"){
                        		$(".iosbuiltname").html("");
                        		$(".iosbuiltdate").html("");
                        		$(".andoridbuiltname").html("");
                        		$(".andoridbuiltdate").html("");
                        		$(".iostestbyname").html("");
                        		$(".iostestbydate").html("");
                        		$(".andoridtestbyname").html("");
                        		$(".andoridtestbydate").html("");
                        		$(".iossubmitbyname").html("");
                        		$(".iossubmitbydate").html("");
                        		$(".andoridsubmitbyname").html("");
                        		$(".andoridsubmitbydate").html("");
                        		$(".ioslivebyname").html("");
                        		$(".ioslivebydate").html("");
                        		$(".andoridlivebyname").html("");
                        		$(".andoridlivebydate").html("");
                        		$(".app_designname").html("");
                                $(".app_designdate").html("");
                                $(".app_populatedname").html("");
                        		$(".app_populateddate").html("");
                        		$(".app_welcallname").html("");
                                $(".app_welcalldate").html("");
                                $(".app_trainingname").html("");
                                $("#apptrainingdate").html("");
                                $(".app_marketingname").html("");
								$(".app_marketingdate").html("");
								$(".app_supportname").html("");
								$(".app_supportdate").html("");
								$(".app_devpaymentname").html("");
								$(".app_devpaymentdate").html("");
								$(".app_devpaymentamount").html("");
								$(".app_cmspaymentname").html("");
								$(".app_cmspaymentdate").html("");
								$(".app_cmspaymentamount").html("");
								$(".app_dempaymentname").html("");
								$(".app_dempaymentdate").html("");
								$(".app_dempaymentamount").html("");
                        	}
                        	//iosbuilt
                        	if(type=="ios built" && usertype=="Admin"){
                        		$(".iosbuiltname").empty().append(servicename);
                        		$(".iosbuiltdate").empty().append(servicedate);
                        		$('.ios_built_log').css('display','block');
                                $('.ios_test_log').css('display','block');
                        	}
                            else if(type=="ios built" && usertype=="Affiliate"){
                                $(".iosbuiltname").empty().append(servicename);
                                $(".iosbuiltdate").empty().append(servicedate);
                                $('.ios_built_log').css('display','none');
                                $('.ios_test_log').css('display','none');
                            }
                            else if(type=="ios built" && usertype=="Developer"){
                                $(".iosbuiltname").empty().append(servicename);
                                $(".iosbuiltdate").empty().append(servicedate);
                                $('.ios_built_log').css('display','none');
                                $('.ios_submitted_log').css('display','none');
                            }
                            
                            //andorid built
                        	if(type=="andorid built" && usertype=="Admin"){
                        		$(".andoridbuiltname").empty().append(servicename);
                        		$(".andoridbuiltdate").empty().append(servicedate);
                        		$('.andorid_built_log').css('display','block');
                                $('.andorid_test_log ').css('display','block');
                        	}
                            else if(type=="andorid built" && usertype=="Affiliate"){
                                $(".andoridbuiltname").empty().append(servicename);
                                $(".andoridbuiltdate").empty().append(servicedate);
                                $('.andorid_built_log').css('display','none');
                                $('.andorid_test_log ').css('display','none');
                            }
                            else if(type=="andorid built" && usertype=="Developer"){
                                $(".andoridbuiltname").empty().append(servicename);
                                $(".andoridbuiltdate").empty().append(servicedate);
                                $('.andorid_built_log').css('display','none');
                                $('.andorid_submitted_log').css('display','none');
                            }

                            //IOSTested
                        	if(type=="app_iostested" && usertype=="Admin"){
                        		$(".iostestbyname").empty().append(servicename);
                        		$(".iostestbydate").empty().append(servicedate);
                        		$('.ios_test_log').css('display','block');
                                $('.ios_submitted_log').css('display','block');
                        	}
                            else if(type=="app_iostested" && usertype=="Affiliate"){
                                $(".iostestbyname").empty().append(servicename);
                                $(".iostestbydate").empty().append(servicedate);
                                $('.ios_test_log').css('display','none');
                            }
                            else if(type=="app_iostested" && usertype=="DEM"){
                                $(".iostestbyname").empty().append(servicename);
                                $(".iostestbydate").empty().append(servicedate);
                                $('.ios_test_log').css('display','none');
                            }
                        	if(type=="app_andoridtested" && usertype=="Admin"){
                        		$(".andoridtestbyname").empty().append(servicename);
                        		$(".andoridtestbydate").empty().append(servicedate);
                        		$('.andorid_test_log ').css('display','block');
                                $('.andorid_submitted_log').css('display','block');
                        	}
                            else if(type=="app_andoridtested" && usertype=="Affiliate"){
                                $(".andoridtestbyname").empty().append(servicename);
                                $(".andoridtestbydate").empty().append(servicedate);
                                $('.andorid_test_log ').css('display','none');
                            }
                            else if(type=="app_andoridtested" && usertype=="DEM"){
                                $(".andoridtestbyname").empty().append(servicename);
                                $(".andoridtestbydate").empty().append(servicedate);
                                $('.andorid_test_log ').css('display','none');
                            }

                        	if(type=="app_iossumbmit" && usertype=="Admin"){
                        		$(".iossubmitbyname").empty().append(servicename);
                        		$(".iossubmitbydate").empty().append(servicedate);
                        		$('.ios_submitted_log').css('display','block');
                                $('.ios_live_log').css('display','block');
                        	}
                            else if (type=="app_iossumbmit" && usertype=="Developer"){
                                $(".iossubmitbyname").empty().append(servicename);
                                $(".iossubmitbydate").empty().append(servicedate);
                                $('.ios_submitted_log').css('display','none');
                                $('.ios_live_log').css('display','block');
                            }


                        	if(type=="app_andoridsumbmit" && usertype=="Admin"){
                        		$(".andoridsubmitbyname").empty().append(servicename);
                        		$(".andoridsubmitbydate").empty().append(servicedate);
                        		$('.andorid_submitted_log').css('display','block');
                                $('.andorid_live_log ').css('display','block');
                        	}
                            else if(type=="app_andoridsumbmit" && usertype=="Developer"){
                                $(".andoridsubmitbyname").empty().append(servicename);
                                $(".andoridsubmitbydate").empty().append(servicedate);
                                $('.andorid_submitted_log').css('display','none');
                                $('.andorid_live_log ').css('display','block');
                            }

                        	if(type=="app_ioslive" && usertype=="Admin"){
                        		$(".ioslivebyname").empty().append(servicename);
                        		$(".ioslivebydate").empty().append(servicedate);
                        		$('.ios_live_log').css('display','block');
                        	}
                            else if(type=="app_ioslive" && usertype=="Developer"){
                                $(".ioslivebyname").empty().append(servicename);
                                $(".ioslivebydate").empty().append(servicedate);
                                $('.ios_live_log').css('display','none');
                            }
                        	if(type=="app_andoridlive" && usertype=="Admin"){
                        		$(".andoridlivebyname").empty().append(servicename);
                        		$(".andoridlivebydate").empty().append(servicedate);
                        		$('.andorid_live_log ').css('display','block');
                        	}
                            else if(type=="app_andoridlive" && usertype=="Developer"){
                                $(".andoridlivebyname").empty().append(servicename);
                                $(".andoridlivebydate").empty().append(servicedate);
                                $('.andorid_live_log ').css('display','none');
                            }
                        	if(type=="app_desiged" && usertype=="Admin"){
    			        		$(".app_designname").empty().append(servicename);
                        		$(".app_designdate").empty().append(servicedate);
                        		$('.app_designed_log').css('display','block');
                                $('.ios_built_log').css('display','block');
                                $('.andorid_built_log').css('display','block');
    			        	}
                            else if(type=="app_desiged" && usertype=="Affiliate"){
                                $(".app_designname").empty().append(servicename);
                                $(".app_designdate").empty().append(servicedate);
                                $('.app_designed_log').css('display','none');
                                $('.ios_built_log').css('display','none');
                                $('.andorid_built_log').css('display','none');
                            }
                            else if(type=="app_desiged" && usertype=="DEM"){
                                $(".app_designname").empty().append(servicename);
                                $(".app_designdate").empty().append(servicedate);
                                $('.app_designed_log').css('display','none');
                                $('.ios_built_log').css('display','none');
                                $('.andorid_built_log').css('display','none');
                            }
    			        	if(type=="app_populated" && usertype=="Admin"){
    			        		$(".app_populatedname").empty().append(servicename);
                        		$(".app_populateddate").empty().append(servicedate);
                        		$('.app_populated_log').css('display','block');
    			        	}
                            else if(type=="app_populated" && usertype=="Affiliate"){
                                $(".app_populatedname").empty().append(servicename);
                                $(".app_populateddate").empty().append(servicedate);
                                $('.app_populated_log').css('display','none');
                            }
                            else if(type=="app_populated" && usertype=="DEM"){
                                $(".app_populatedname").empty().append(servicename);
                                $(".app_populateddate").empty().append(servicedate);
                                $('.app_populated_log').css('display','none');
                            }
    			        	if(type=="app_welcomecall" && usertype=="Admin"){
    			        		$(".app_welcallname").empty().append(servicename);
                        		$(".app_welcalldate").empty().append(servicedate);
                        		$('.welcome_call_log').css('display','block');
    			        	} else if(type=="app_welcomecall" && usertype=="Affiliate"){
                                $(".app_welcallname").empty().append(servicename);
                                $(".app_welcalldate").empty().append(servicedate);
                                $('.welcome_call_log').css('display','none');
                            }
                            else if(type=="app_welcomecall" && usertype=="CSM"){
                                $(".app_welcallname").empty().append(servicename);
                                $(".app_welcalldate").empty().append(servicedate);
                                $('.welcome_call_log').css('display','none');
                            }
    			        	if(type=="app_training" && usertype=="Admin"){
    			        	    $(".app_trainingname").empty().append(servicename);
                        		$("#apptrainingdate").empty().append(servicedate);
                        		$('.app_training_log').css('display','block');
    			        	}
                            if(type=="app_training" && usertype=="Affiliate"){
                                $(".app_trainingname").empty().append(servicename);
                                $("#apptrainingdate").empty().append(servicedate);
                                $('.app_training_log').css('display','none');
                            }
                            if(type=="app_training" && usertype=="CSM"){
                                $(".app_trainingname").empty().append(servicename);
                                $("#apptrainingdate").empty().append(servicedate);
                                $('.app_training_log').css('display','none');
                            }
    			        	$("#comment_ajax_table").empty().append(responseArr[3]);
            	        }  
    				}
                }); 
    	    } else {
    	        return false;
    	    }
	    }
    });
    
    $(document).on("keyup","#comment_popup",function(){
       
        var comments =$("#comment_popup").val();
        if(comments=="") {
            $(".comment_error").css("display","block");
            return false;
        } else {
            $(".comment_error").css("display","none");  
        }
    });
    $(document).on("click",".comments_sup_market",function(){
        var type=$("#hnd_type_comment").val();
        var list_id=$("#hnd_listid_comment").val();
        var commentstype  =$("#hnd_type_comment").val();
        var comments =$("#comment_popup").val();
        if(comments=="") {
            $(".comment_error").css("display","block");
            return false;
        } else {
            $(".comment_error").css("display","none");  
        }
        /*var defaultStr;
        var getAreavalue;
        var comments;

        if(commentstype=="market_service"){
            defaultStr="Marketing Service:";
            getAreavalue=comment_popup.search(defaultStr);
            if (getAreavalue<0) {
               comments=defaultStr+" "+comment_popup;
               $("#comment_popup").empty().val(comments);
            } else if(getAreavalue>0) {
               comments=defaultStr+" "+comment_popup;
               $("#comment_popup").empty().val(comments);
            } else {
              comments=comment_popup;
            }
        }
        if (commentstype=="support_call") {
            defaultStr="Support Call:";
            getAreavalue=comment_popup.search(defaultStr);
            if(getAreavalue<0) {
                comments=defaultStr+" "+comment_popup;
                $("#comment_popup").empty().val(comments);
            }
            else if (getAreavalue>0) {
                comments=defaultStr+" "+comment_popup;
                $("#comment_popup").empty().val(comments);
            } else {
              comments=comment_popup;
            }
        }*/
        if(comments!=""){
            $.ajax({
                url:"service_progress_ajax.php",  
                method:'GET',
                data:"post_type="+type+"&list_id="+list_id+"&comments="+comments,
                success:function(data) { 
                	//alert(data);
                    var responseArr=data.split("##^^##");
                    if(responseArr[0]=="Success"){
                        var servicename=responseArr[1];
                        var servicedate=responseArr[2];
                        if (type=="market_service") {
                            $(".app_marketingname").empty().append(servicename);
                            $(".app_marketingdate").empty().append(servicedate);
                            setTimeout("parent.$.fancybox.close()", 100);
                        }    
                        if(type=="support_call"){
                            $(".app_supportname").empty().append(servicename);
                            $(".app_supportdate").empty().append(servicedate);
                            setTimeout("parent.$.fancybox.close()", 100);
                        } 
                            $("#comment_ajax_table").empty().append(responseArr[3]);
                    }   
                }
            });
        } else{
            $(".comment_error").css("display","block");
            return false;
        }
    });

    $(document).on("click",".dev_payment",function(){
        var type=$("#hnd_type_dev").val();
        var list_id=$("#hnd_listid_dev").val();
        var amount=$("#dev_amount").val();
        var dev_date=$("#dev_date1").val();
          
        if (amount=="") {
            $("#error-devamount").show();
            return false;
        } else if(dev_date=="") {
            $("#error-devdate").show();
            return false;
        } else {
            $("#error-devamount").hide();
            $("#error-devdate").hide();
        }
        $.ajax({
            url:"service_progress_ajax.php",  
            method:'GET',
            data:"post_type="+type+"&list_id="+list_id+"&amount="+amount+"&pay_date="+dev_date,
            success:function(data) { 
                var responseArr=data.split("##^^##");
                if(responseArr[0]=="Success"){
                    var servicename=responseArr[1];
                    var servicedate=responseArr[2];
                    var serviceamount=responseArr[3];
                    if(type=="dev_payment"){
                        $(".app_devpaymentname").empty().append(servicename);
                        $(".app_devpaymentdate").empty().append(servicedate);
                        $(".app_devpaymentamount").empty().append(serviceamount);
                        $('.devpayment_log').css('display','block');
                        setTimeout("parent.$.fancybox.close()", 100);
                    }
                    $("#comment_ajax_table").empty().append(responseArr[4]);
                }  
            }
        });
    });
    $(document).on("click",".cms_payment",function(){
        var type=$("#hnd_type_cms").val();
        var list_id=$("#hnd_listid_cms").val();
        var amount=$("#cms_amount").val();
        var dev_date=$("#cms_date").val();
        if (amount=="") {
            $("#error-cmsamount").show();
            return false;
        } else if(dev_date=="") {
            $("#error-cmsdate").show();
            return false;
        } else {
            $("#error-cmsamount").hide();
            $("#error-cmsdate").hide();
        }
        $.ajax({
            url:"service_progress_ajax.php",  
            method:'GET',
            data:"post_type="+type+"&list_id="+list_id+"&amount="+amount+"&pay_date="+dev_date,
            success:function(data) { 
                var responseArr=data.split("##^^##");
                if(responseArr[0]=="Success"){
                    var servicename=responseArr[1];
                    var servicedate=responseArr[2];
                    var serviceamount=responseArr[3];
                    if(type=="cms_payment"){
                        $(".app_cmspaymentname").empty().append(servicename);
                        $(".app_cmspaymentdate").empty().append(servicedate);
                        $(".app_cmspaymentamount").empty().append(serviceamount);
                        $('.cms_payment_log').css('display','block');
                        setTimeout("parent.$.fancybox.close()", 100);
                    }
                $("#comment_ajax_table").empty().append(responseArr[4]);
               }  
            }
        });
    });
    $(document).on("click",".dem_payment",function(){
        var type=$("#hnd_type_dem").val();
        var list_id=$("#hnd_listid_dem").val();
        var amount=$("#dem_amount").val();
        var dev_date=$("#dem_date").val();
        if (amount=="") {
            $("#error-demamount").show();
            return false;
        } else if(dev_date=="") {
            $("#error-demdate").show();
            return false;
        } else {
            $("#error-devamount").hide();
            $("#error-devdate").hide();
        }
        $.ajax({
            url:"service_progress_ajax.php",  
            method:'GET',
            data:"post_type="+type+"&list_id="+list_id+"&amount="+amount+"&pay_date="+dev_date,
            success:function(data) { 
                var responseArr=data.split("##^^##");
                if (responseArr[0]=="Success") {
                    var servicename=responseArr[1];
                    var servicedate=responseArr[2];
                    var serviceamount=responseArr[3];
                    if (type=="dem_payment") {
                        $(".app_dempaymentname").empty().append(servicename);
                        $(".app_dempaymentdate").empty().append(servicedate);
                        $(".app_dempaymentamount").empty().append(serviceamount);
                        $('.dem_payment_log').css('display','block');
                        setTimeout("parent.$.fancybox.close()", 100);
                    }
                    $("#comment_ajax_table").empty().append(responseArr[4]);
                }  
            }
        });
    });
    $(document).on("change ,keyup","#dev_amount",function(){
        var amount=$("#dev_amount").val();
        if (amount=="") {
            $("#error-devamount").show();
            return false;
        } else {
            $("#error-devamount").hide();
        } 
    });
    $(document).on("change ,keyup","#comment_popup",function(){
        var comment_popup=$("#comment_popup").val();
        if (comment_popup=="") {
            $(".comment_error").css("display","block");
            return false;
        } else {
          $(".comment_error").css("display","none");
        } 
    });

    $(document).on("change ,keyup","#dev_date1",function(){
        var dev_date=$("#dev_date1").val();
        if (dev_date=="") {
            $("#error-devdate").show();
            return false;
        } else {
         $("#error-devdate").hide();
        } 
    });
    $(document).on("change ,keyup","#dem_amount",function(){
        var dev_date=$("#dem_amount").val();
        if(dev_date==""){
            $("#error-demamount").show();
            return false;
        }else{
           $("#error-demamount").hide();
        } 
    });
    $(document).on("change ,keyup","#dem_date",function(){
        var dev_date=$("#dem_date").val();
        if (dev_date=="") {
            $("#error-demdate").show();
            return false;
        } else {
            $("#error-demdate").hide();
        } 
    });
    $(document).on("change ,keyup","#cms_date",function(){
        var dev_date=$("#cms_date").val();
        if (dev_date=="") {
            $("#error-cmsdate").show();
            return false;
        } else {
            $("#error-cmsdate").hide();
        } 
    });
    $(document).on("change ,keyup","#cms_amount",function(){
        var dev_date=$("#cms_amount").val();
        if(dev_date=="") {
            $("#error-cmsamount").show();
            return false;
        } else {
            $("#error-cmsamount").hide();
        } 
    });

    $(document).on("click",".dem_payment_log,.devpayment_log,.cms_payment_log,.support_call_log,.market_service_log",function(){
        var type=$(this).attr("data-id");
        var list_id=$(this).attr("list-id");
        if (type=="dev_payment")  {
            $(".payment_head, #frm_payment").show();
            $(".payment_head1, .payment_head2, .support_head, .market_head, #frm_cmspayment, #frm_dempayment, #frm_comment").hide();
            $("#hnd_type_dev").val(type);
            $("#hnd_listid_dev").val(list_id);
        }
        if (type=="dem_payment") {
            $(".payment_head, .payment_head2, .support_head, .market_head, #frm_payment, #frm_cmspayment, #frm_comment").hide();
            $("#frm_dempayment, .payment_head1").show();
            $("#hnd_type_dem").val(type);
            $("#hnd_listid_dem").val(list_id);
        }
        if (type=="cms_payment") {
            $(".payment_head, .payment_head1, .support_head, .market_head, #frm_payment, #frm_comment, #frm_dempayment").hide();
        	$(".payment_head2, #frm_cmspayment").show();
            $("#hnd_type_cms").val(type);
            $("#hnd_listid_cms").val(list_id);
        }
        if(type=="support_call"){  

            $(".Prefix").empty().append("Support Call");
            $("#comment_popup").css("text-indent","85px");
           	$("#comment_popup").empty().val("");
            $(".payment_head, .payment_head1, .payment_head2, .market_head, #frm_payment, #frm_cmspayment, #frm_dempayment,.comment_error").hide();
            $(".support_head, #frm_comment,").show();
            $("#hnd_type_comment").val(type);
            $("#hnd_listid_comment").val(list_id);
        }
        if(type=="market_service"){
        	$(".Prefix").empty().append("Marketing Service");
            $("#comment_popup").css("text-indent","123px");
            $("#comment_popup").empty().val("");
            $("#frm_payment, #frm_cmspayment, .payment_head, .payment_head1, .payment_head2, .support_head, #frm_dempayment,.comment_error").hide();
            $(".market_head, #frm_comment").show();
            $("#hnd_type_comment").val(type);
            $("#hnd_listid_comment").val(list_id);
        }
    });
</script>
<script>
    $(document).on("click",".deleteimage",function(){
        var imagepath=$(this).attr("data-img");
        var imagetype=$(this).attr("data-type");
        var imagedeleteId=$(this).attr("data-deid");
        if(imagetype=="business_logo" || imagetype=="background_image" || imagetype=="background_image2" || imagetype=="background_image3" || imagetype=="background_image4" || imagetype=="restaurant_logo" ){
            msg="Are you sure want to delete this image?";
        }
        if(imagetype=="andorid_ipa_apk"){
            msg="Are you sure want to delete this Android build?";
        }
        if(imagetype=="ios_ipa_apk"){
            msg="Are you sure want to delete this Ios build?";
        }
        var check = confirm(msg);
        if (check == true) {
            $.ajax({
                type: 'POST',
                url: 'deleteimg.php',
                data: {"image": imagepath,"id":imagedeleteId,"imgtype":imagetype},
                complete: function(httpResponse, status) {
                    var response = httpResponse.responseText;
                    var result = $.parseJSON(response);
                    if(result.status=="Sucess"){
                        if(imagetype=="business_logo"){
                           $("#deleteimagelogo").css({"display":"none"});
                        }
                        if(imagetype=="background_image"){
                           $("#deleteimage1").css({"display":"none"});
                        }
                        if(imagetype=="background_image2"){
                            $("#deleteimage2").css({"display":"none"});
                        }
                        if(imagetype=="background_image3"){
                            $("#deleteimage3").css({"display":"none"});
                        }
                        if(imagetype=="background_image4"){
                           $("#deleteimage4").css({"display":"none"});
                        }
                        if(imagetype=="ios_ipa_apk"){
                           $("#ios_ipa_apk").css({"display":"none"});
                        }
                        if(imagetype=="andorid_ipa_apk"){
                           $("#andorid_ipa_apk").css({"display":"none"});
                        }
                        if(imagetype=="restaurant_logo"){
                           $("#restaurantlogo").css({"display":"none"});
                        }
                    } else {
                        alert("Somthing went to wrong");
                    }
                }
            });
        } else{
            return false;
        }
    });
    $(function () {
        $('#comment_popup,#prospect_comments').keyup(function (e){
            if(e.keyCode == 13){
               var curr = getCaret(this);
               var val = $(this).val();
               var end = val.length;
               $(this).val( val.substr(0, curr) + '' + val.substr(curr, end));
            }
        })
    });

    function getCaret(el) { 
        if (el.selectionStart) { 
            return el.selectionStart; 
        } else if (document.selection) { 
            el.focus(); 
            var r = document.selection.createRange(); 
            if (r == null) { 
                return 0; 
            } 
            var re = el.createTextRange(), 
            rc = re.duplicate(); 
            re.moveToBookmark(r.getBookmark()); 
            rc.setEndPoint('EndToStart', re); 
            return rc.text.length; 
           }  
      return 0; 
    }
    $(document).on("click",".viewimg", function(){
    	var serviceid=$(this).attr("service_id");
    	type="service_popup";
    	if(serviceid!=""){
	    	$.ajax({
                url:"service_progress_ajax_popup.php",  
                method:'GET',
                data:"post_type="+type+"&serviceid="+serviceid,
                success:function(data) { 
                    var responseArr=data.split("##^^##");
                    if (responseArr[0]=="Success") {
                    var servicehtml=responseArr[1];
                    $("#serviceidpopup").empty().html(servicehtml);
                     $('.fancybox').trigger('click');
                    }   
                }
	         });
    	
        }
    });
</script>