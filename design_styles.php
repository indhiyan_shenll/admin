<?php
include("../includes/configure.php");
include("../includes/session_check.php");
$country_id=$_GET["country_id"];
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<title>MAR Pipeline System</title>
		<link rel="shortcut icon" href="images/Fav.ico" type="image/ico">
		<meta name="Generator" content="EditPlus">
		<meta name="Author" content="">
		<meta name="Keywords" content="">
		<meta name="Description" content="">
		<style>
			body{
				margin:0;
				color:black;
				background:#455A68;
				font-family:arial;
			}
			.header{
				height:70px;
				background:#1C242A;
			}
			.content{
				background:#455A68;
				min-height:600px;
			}
			
			.form_actions{
				padding-top:15px;
				padding-left:5px;
				padding-bottom:30px;
			}
			.form_actions .add_btn{
				cursor:pointer;
				border-radius:0px;
				background:#0D0D0D;
				color:#D9D9D9;
				border-color:#D9D9D9;
				padding:5px 15px 5px 15px;
				font-family:arial;
			}
			.list_content{
				width:950px;
				margin-left:40px;
				/*margin-left:auto;
				margin-right:auto;*/
			}
			.tbl_header th{
				font-size:13px;
				border-bottom:1px solid #D9D9D9;
				text-align:left;
				font-family:arial;
				color:white;
			}
			.tbl-body{
				font-size:12px;
				line-height:25px;
				font-family:arial;
			}
			a{
				color:black;
			}
		</style>
	</head>
	<body>
		<div>
			<div style="margin-left:auto;margin-right:auto;">
				<div class="header">
					<span style="float:right;margin-right:20px;margin-top:5px;"><a href="logout.php" style="color:white;text-decoration:none;">Logout</a></span>
					<img src="images/myappyrestaurants.png" style="margin-top:10px;margin-left:40px;">
				</div>
				<div class="content">
					<div class="list_content">
						<div class="form_actions" style="padding-bottom:45px;">
							<input type="button" value="Back To Demo Page" class="add_btn" onclick="document.location='newdemo.php'" style="float:left;">
							 
						</div>
						<div style="padding-bottom:12px;">
						<table cellspacing="0" cellpadding="8" width="100%" border="1" style="border:1px solid #D9D9D9;" >
							     <tr height="30px" BGCOLOR="gray" style="padding:0px;">
										<td colspan="" align="left" width="10px"><span style="color: white;">&nbsp;&nbsp;Sno</span></td>
										<td colspan="" align="left" width="170px"><span style="color: white;">&nbsp;&nbsp;Title</span></td>
										<td colspan="" align="center"><span style="color: white;">&nbsp;&nbsp;Design style</span></td>
									 										
								</tr>
								<?php
								$themes_array1 =array(
								'1'=>'Verve Square',
							    '2'=>'Metro Square',
								'3'=>'Solstice Square',
								'10'=>'Modern Square',
								'8'=>'Eclipse Square');
								$i=1;
								$j=1;
								foreach($themes_array1 as $val=>$name){ 
								 
									if($j%2==1){
										$bgcolor="#a5a5a5";
									}
									else{
										$bgcolor="#d2d1d1";
									}
								?>
                                <tr  bgcolor="<?php echo $bgcolor;?>" style="font-size:14px;">
										<td colspan="" align="left"><?php echo $j;?></td>
										<td colspan="" align="left"><?php echo $name;?></td>
										<td colspan="" align="left"><center><img src="themes/<?php echo $j;?>.png" style="width: 184px;height: 220px;"></center></td>
									 										
								</tr>


								<?php
								$j++;

							 
								$i++;
								 
								} 
								?>
                                 
                             
								 
								 
								</table>	 
						</div>
						<table cellspacing="0" cellpadding="0" width="100%" class="tbl-body">
							 
						 
						<tr>
								<td colspan="3">
								<div class="form_actions" style="text-align:left;position:relative;" >
									<input type="button" value="Back To Demo Page" class="add_btn" onclick="document.location='newdemo.php'" style="float:left;">
								</td>
							</tr>
						</table>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>