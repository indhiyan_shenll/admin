<?php
include("../includes/configure.php");
include("../includes/session_check.php");
$dbdatetime=date('Y-m-d H:i:s',strtotime('now'));
$sale_manager_id=$_GET["sales_id"];
if($sale_manager_id!=""){
	//$sale_manager_id
	$getQry="select * from tbl_salesmanager where sales_manager_id=:sales_manager_id";
	$prepgetQry=$DBCONN->prepare($getQry);
	$prepgetQry->execute(array(":sales_manager_id"=>$sale_manager_id));
	//$getRes=mysql_query($getQry);
	$getRow=$prepgetQry->fetch();
	$Salesmanager=stripslashes($getRow["sales_manager"]);
	$email=stripslashes($getRow["email"]);
	$mode="Edit";
	$value="Update";
}
else{
	$mode="Add";
	$value="Create";
}
if(isset($_POST["manager"])){
	$Person=addslashes(trim($_POST["manager"]));
	$manager_email=addslashes(trim($_POST["manager_email"]));
    if($mode=="Edit"){
		//$Person   $manager_email      $sale_manager_id
		$updateQry="update tbl_salesmanager set sales_manager=:sales_manager,email=:email,modified_date=:modified_date where sales_manager_id=:sales_manager_id";
		$prepupdateQry=$DBCONN->prepare($updateQry);
		$updateRes=$prepupdateQry->execute(array(":sales_manager"=>$Person,":email"=>$manager_email,":modified_date"=>$dbdatetime,":sales_manager_id"=>$sale_manager_id));
		//$updateRes=mysql_query($updateQry);
		if($updateRes){
			header("Location:manager_list.php");
			exit;
		}
	}
	else{
		//$Person   $manager_email
		$insertQry="insert into   tbl_salesmanager(sales_manager,email,added_date,modified_date) values(:sales_manager,:email,:added_date,:modified_date)";
		$prepinsertQry=$DBCONN->prepare($insertQry);
		$insertRes=$prepinsertQry->execute(array(":sales_manager"=>$Person,":email"=>$manager_email,":added_date"=>$dbdatetime,":modified_date"=>$dbdatetime));
		//$insertRes=mysql_query($insertQry);
		if($insertRes){
			header("Location:manager_list.php");
			exit;
		}
	}
}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<title>MAR Pipeline System</title>
		<link rel="shortcut icon" href="images/Fav.ico" type="image/ico">
		<meta name="Generator" content="EditPlus">
		<meta name="Author" content="">
		<meta name="Keywords" content="">
		<meta name="Description" content="">
		<script type="text/javascript" src="js/jquery.js"></script>
		<script>
		var mode="<?php echo $mode;?>";
		</script>
		<script type="text/javascript" src="js/validate.js"></script>
		<style>
			body{
				margin:0;
				color:#D9D9D9;
				background:#455A68;
				font-family:arial;
			}
			.header{
				height:70px;
				background:#1C242A;
			}
			.content{
				background:#455A68;
				min-height:600px;
			}
			
			.form_actions{
				padding-top:15px;
				padding-left:5px;
				padding-bottom:30px;
			}
			.form_actions .add_btn{
				cursor:pointer;
				border-radius:0px;
				background:#0D0D0D;
				color:#D9D9D9;
				border-color:#D9D9D9;
				padding:5px 15px 5px 15px;
				font-family:arial;

			}
			.list_content{
				width:950px;
				margin-left:40px;
				/*margin-left:auto;
				margin-right:auto;*/
			}
			.tbl_header th{
				font-size:13px;
				border-bottom:1px solid #D9D9D9;
				text-align:left;
				font-family:arial;
			}
			.tbl-body{
				font-size:12px;
				font-family:arial;
			}
			a{
				color:black;
			}
			.inp_feild{
				border-radius:2px;
				border:none;
				width:100%;
			}
		</style>
	</head>
	<body>
		<div>
			<div style="margin-left:auto;margin-right:auto;">
				<div class="header">
					<span style="float:right;margin-right:20px;margin-top:5px;"><a href="logout.php" style="color:white;text-decoration:none;">Logout</a></span>
					<img src="images/myappyrestaurants.png" style="margin-top:10px;margin-left:40px;">
				</div>
				<div class="content">
					<div class="list_content">
						<h1 style="font-size:25px;padding-top:15px;padding-bottom:15px;margin:0px;">New Sales Manager </h1>
						<form name="manager_form" id="manager_form" method="post" enctype="multipart/form-data">
						<input type="hidden" name="manager_form_id" id="manager_form_id" value="<?php echo $sale_manager_id;?>">
						<table cellspacing="15" cellpadding="0" border="0" width="80%">
							<tr>
								<td style="width:225px;">
									 Sales Manager<font color="red">*</font>:
								</td>
								<td>
									<input type="text" name="manager" id="manager" class="inp_feild" value="<?php echo $Salesmanager;?>">
								</td>
							</tr>
							<tr>
								<td>
									 Sales Manager Email Address<font color="red">*</font>:
								</td>
								<td>
									<input type="text" name="manager_email" id="manager_email" class="inp_feild" value="<?php echo $email;?>">
								</td>
							</tr>
							<tr>
							     <td>
									<div class="form_actions" style="text-align:left;">
										<input type="button" value="Back To Sales Manager List" class="add_btn"  onclick="document.location='manager_list.php'">
									
								</td>
								<td>
									  <div class="form_actions" style="text-align:right;">
										<input type="button" value="<?php echo $value;?> Sales Manager" class="add_btn" id="add_manager">
									</div>
								</td>
							</tr>
						</table>
						</form>
					</div>
					
				</div>
			</div>
		</div>
	</body>
</html>