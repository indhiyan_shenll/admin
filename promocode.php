<?php
include("../includes/configure.php");
$promocode_id=$_GET["promocode_id"];
if($promocode_id!=""){
	$getQry="select * from  tbl_promocodes where promocode_id='".$promocode_id."'";
	$getRes=mysql_query($getQry);
	$getRow=mysql_fetch_array($getRes);
	$promocode=stripslashes($getRow["promocode"]);
	$mode="Edit";
	$value="Update";
}
else{
	$mode="Add";
	$value="Create";
}
if(isset($_POST["promocode"])){
	$promocode=addslashes(trim($_POST["promocode"]));
	if($mode=="Edit"){
		$updateQry="update tbl_promocodes set promocode='".$promocode."',modified_date=now() where promocode_id='".$promocode_id."'";
		$updateRes=mysql_query($updateQry);
		if($updateRes){
			header("Location:promocodelist.php");
			exit;
		}
	}
	else{
		$insertQry="insert into tbl_promocodes(promocode,added_date,modified_date)values('".$promocode."',now(),now())";
		$insertRes=mysql_query($insertQry);
		if($insertRes){
			header("Location:promocodelist.php");
			exit;
		}
	}
}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<title>MAR Pipeline System</title>
		<link rel="shortcut icon" href="images/Fav.ico" type="image/ico">
		<meta name="Generator" content="EditPlus">
		<meta name="Author" content="">
		<meta name="Keywords" content="">
		<meta name="Description" content="">
		<script type="text/javascript" src="js/jquery.js"></script>
		<script>
		var mode="<?php echo $mode;?>";
		</script>
		<script type="text/javascript" src="js/validate.js"></script>
		<style>
			body{
				margin:0;
				color:#D9D9D9;
				background:#455A68;
				font-family:arial;
			}
			.header{
				height:70px;
				background:#1C242A;
			}
			.content{
				background:#455A68;
				min-height:600px;
			}
			
			.form_actions{
				padding-top:15px;
				padding-left:5px;
				padding-bottom:30px;
			}
			.form_actions .add_btn{
				cursor:pointer;
				border-radius:0px;
				background:#0D0D0D;
				color:#D9D9D9;
				border-color:#D9D9D9;
				padding:5px 15px 5px 15px;
				font-family:arial;

			}
			.list_content{
				width:950px;
				margin-left:40px;
				/*margin-left:auto;
				margin-right:auto;*/
			}
			.tbl_header th{
				font-size:13px;
				border-bottom:1px solid #D9D9D9;
				text-align:left;
				font-family:arial;
			}
			.tbl-body{
				font-size:12px;
				font-family:arial;
			}
			a{
				color:black;
			}
			.inp_feild{
				border-radius:2px;
				border:none;
				width:100%;
			}
		</style>
	</head>
	<body>
		<div>
			<div style="margin-left:auto;margin-right:auto;">
				<div class="header">
					<span style="float:right;margin-right:20px;margin-top:5px;"><a href="logout.php" style="color:white;text-decoration:none;">Logout</a></span>
					<img src="images/myappyrestaurants.png" style="margin-top:10px;margin-left:40px;">
				</div>
				<div class="content">
					<div class="list_content">
						<h1 style="font-size:25px;padding-top:15px;padding-bottom:15px;margin:0px;">New Promo Code</h1>
						<form name="promocode_form" id="promocode_form" method="post">
						<input type="hidden" name="promocode_id" id="promocode_id" value="<?php echo $promocode_id;?>">
						<table cellspacing="15" cellpadding="0" border="0" width="70%">
							<tr>
								<td style="width:138px;">
									 Promo Code<font color="red">*</font>:
								</td>
								<td>
									<input type="text" name="promocode" id="promocode" class="inp_feild" value="<?php echo $promocode;?>">
								</td>
							</tr>
													
							<tr>
							     <td>
									<div class="form_actions" style="text-align:left;">
										<input type="button" value="Back to Promo Code List" class="add_btn"  onclick="document.location='promocodelist.php'">
									
								</td>
								<td>
									  <div class="form_actions" style="text-align:right;">
										<input type="button" value="<?php echo $value;?> Promo Code" class="add_btn" id="add_promocode">
									</div>
								</td>
							</tr>
						</table>
						</form>
					</div>
					
				</div>
			</div>
		</div>
	</body>
</html>