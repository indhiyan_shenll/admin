<?php

// Paypal Payment Received 
function PaypalPaymentRecived($ToAddress, $BusinessName, $ContactName, $PromoCode, $AffiliateName, $LogoUrl)
{

$FromAddress = 'From: My Appy Business <server@myappybusiness.com>';
$EmailSubject	= "Paypal Payment Received ($BusinessName)";
$EmailMessage	= '
<body style="margin: 0; padding: 0;">
<table border="0" align="left" cellpadding="0" cellspacing="0" width="600px" style="font-size:10px;font-family:arial;">
	<tr><td>PAYMENT RECEIVED!</td></tr>
	<tr style="height:5px"><td></td></tr>
	<tr><td >This is to confirm that the following payment was successfully processed through Paypal:</td></tr>
	<tr style="height:20px"><td></td></tr>
	<tr>
		<td>PAYMENT DETAILS:</td>
	</tr>
	<tr style="height:5px"><td></td></tr>
	<tr>
		<td>Contact Name: '.$ContactName.'</td>
	</tr>
	<tr style="height:5px"><td></td></tr>
	<tr>
		<td>Business Name: '.$BusinessName.'</td>
	</tr>
	<tr style="height:5px"><td></td></tr>
	<tr>
		<td>Promo code: '.$PromoCode.'</td>
	</tr>
	<tr style="height:5px"><td></td></tr>
	<tr>
		<td>Affiliate: '.$AffiliateName.'</td>
	</tr>
	<tr style="height:20px"><td></td></tr>
	<tr>
		<td>Please ensure you recieve the official confirmation email from Paypal before you register this payment into the system.</td>
	</tr>	
	<tr style="height:20px"><td></td></tr>
	<tr><td>With Regards,</tr>
	<tr style="height:10px"><td></td></tr>
	<tr><td><img src="'.$LogoUrl.'"></td></tr>
	<tr style="height:15px"><td></td></tr>
	<tr>
		<td style="font-size:9px;font-family:arial;">This e-mail and any attachments are confidential and may contain legally privileged and/or copyright information of My Appy Business or third parties. If you are not an authorised recipient of this e-mail, please contact the sender immediately and do not print, re-transmit or store this email or any attachments. Any loss/damage incurred by using this material is not the sender\'s responsibility. No warranty is given that this e-mail or any attachments are totally free from computer viruses or other defects.</td>
	</tr>
</table>
</body>';

SendEmailCommonfn($FromAddress, $ToAddress, $EmailSubject, $EmailMessage);

}


//Customer payment success
function CustomerPaymentSuccess($FirstName, $CustomerEmail, $DemoId, $LogoUrl)
{


$UploadLogoURL		= "http://www.myappyrestaurant.com/upload_logo.php?id=".$DemoId;
$CancelURL			= "http://www.myappyrestaurant.com/live/cancel_payment.php?app_id=".$DemoId;

$FromAddress = 'From: My Appy Business <payments@myappybusiness.com>';
$EmailSubject	= "Payment approved!";
$EmailMessage	= '
<body style="margin: 0; padding: 0;">
<table border="0" align="left" cellpadding="0" cellspacing="0" width="600px" style="font-size:10px;font-family:arial;">
	<tr>
		<td>Hi '.$FirstName.',</td>
	</tr>
	<tr style="height:10px"><td></td></tr>
	<tr>
		<td>Congratulations! Your payment to My Appy Business was successful!</td>
	</tr>
	<tr style="height:10px"><td></td></tr>
	<tr>
		<td>Our team will be commencing development of your customized App very soon, but first we need you to please confirm a couple of details about your App. Please <a href="'.$UploadLogoURL.'">click here</a> to upload your business logo and nominate your preferred App name.</td>
	</tr>
	<tr style="height:10px"><td></td></tr>
	<tr>
		<td>Once you have confirmed the details above, you will receive an email with your “Marketing Portal” login details within a few days.</td>
	</tr>
	<tr style="height:10px"><td></td></tr>
	<tr>
		<td>With those login details, you can access your “Marketing Portal” anytime at: <a href="http://www.myappybusiness.com/">www.myappybusiness.com</a></td>
	</tr>
	<tr style="height:5px"><td></td></tr>
	<tr>
		<td>If you have any questions relating to your service, please contact us at support@myappybusiness.com</td>
	</tr>
	<tr style="height:5px"><td></td></tr>
	<tr>
		<td>If you have any questions relating to your account, please contact us at accounts@myappybusiness.com</td>
	</tr>
	<tr style="height:5px"><td></td></tr>
	<tr>
		<td>If for any reason you would like to cancel your “Marketing Portal” service, then <a href="'.$CancelURL.'">click here</a>.</td>
	</tr>	
	<tr style="height:20px"><td></td></tr>
	<tr><td>With Regards,</tr>
	<tr style="height:10px"><td></td></tr>
	<tr><td><img src="'.$LogoUrl.'"></td></tr>
	<tr style="height:15px"><td></td></tr>
	<tr>
		<td style="font-size:9px;font-family:arial;">This e-mail and any attachments are confidential and may contain legally privileged and/or copyright information of My Appy Business or third parties. If you are not an authorised recipient of this e-mail, please contact the sender immediately and do not print, re-transmit or store this email or any attachments. Any loss/damage incurred by using this material is not the sender\'s responsibility. No warranty is given that this e-mail or any attachments are totally free from computer viruses or other defects.</td>
	</tr>
</table>
</body>';

SendEmailCommonfn($FromAddress, $CustomerEmail, $EmailSubject, $EmailMessage);

}


function AppsAwaitingAppStoreApprovalEmail($ToAddress, $LogoUrl)
{

$FromAddress = 'From: My Appy Business <server@myappybusiness.com>';
$EmailSubject	= "Apps Awaiting App Store Approval";
$EmailMessage	= '
<body style="margin: 0; padding: 0;">
<table border="0" align="left" cellpadding="0" cellspacing="0" width="600px" style="font-size:10px;font-family:arial;">
	<tr><td>APPS CREATED AND SUBMITTED IN STORE!!</td></tr>
	<tr style="height:5px"><td></td></tr>
	<tr><td >This is to confirm that your app was completed and submitted to apple and play stores for approval.</td></tr>
	<tr style="height:20px"><td></td></tr>
	<tr>
		<td>Waiting for content from cedric. Waiting for content from cedric. Waiting for content from cedric. Waiting for content from cedric. Waiting for content from cedric. Waiting for content from cedric. Waiting for content from cedric.</td>
	</tr>	
	<tr style="height:20px"><td></td></tr>
	<tr><td>With Regards,</tr>
	<tr style="height:10px"><td></td></tr>
	<tr><td><img src="'.$LogoUrl.'"></td></tr>
	<tr style="height:15px"><td></td></tr>
	<tr>
		<td style="font-size:9px;font-family:arial;">This e-mail and any attachments are confidential and may contain legally privileged and/or copyright information of My Appy Business or third parties. If you are not an authorised recipient of this e-mail, please contact the sender immediately and do not print, re-transmit or store this email or any attachments. Any loss/damage incurred by using this material is not the sender\'s responsibility. No warranty is given that this e-mail or any attachments are totally free from computer viruses or other defects.</td>
	</tr>
</table>
</body>';

SendEmailCommonfn($FromAddress, $ToAddress, $EmailSubject, $EmailMessage);

}

function AppsAreLiveEmail($ToAddress, $LogoUrl)
{

$FromAddress = 'From: My Appy Business <server@myappybusiness.com>';
$EmailSubject	= "Apps Live";
$EmailMessage	= '
<body style="margin: 0; padding: 0;">
<table border="0" align="left" cellpadding="0" cellspacing="0" width="600px" style="font-size:10px;font-family:arial;">
	<tr><td>APPS LIVE!</td></tr>
	<tr style="height:5px"><td></td></tr>
	<tr><td >This is to confirm that your app was published in both apple and android stores.</td></tr>
	<tr style="height:20px"><td></td></tr>
	<tr>
		<td>Waiting for content from cedric. Waiting for content from cedric. Waiting for content from cedric. Waiting for content from cedric. Waiting for content from cedric. Waiting for content from cedric. Waiting for content from cedric.</td>
	</tr>	
	<tr style="height:20px"><td></td></tr>
	<tr><td>With Regards,</tr>
	<tr style="height:10px"><td></td></tr>
	<tr><td><img src="'.$LogoUrl.'"></td></tr>
	<tr style="height:15px"><td></td></tr>
	<tr>
		<td style="font-size:9px;font-family:arial;">This e-mail and any attachments are confidential and may contain legally privileged and/or copyright information of My Appy Business or third parties. If you are not an authorised recipient of this e-mail, please contact the sender immediately and do not print, re-transmit or store this email or any attachments. Any loss/damage incurred by using this material is not the sender\'s responsibility. No warranty is given that this e-mail or any attachments are totally free from computer viruses or other defects.</td>
	</tr>
</table>
</body>';

SendEmailCommonfn($FromAddress, $ToAddress, $EmailSubject, $EmailMessage);

}

function sendWelcomeMail($FirstName,$BusinessName,$ToAddress,$LogoUrl,$DemoId){

	$UploadLogoURL		= "http://www.myappyrestaurant.com/upload_logo.php?id=".$DemoId;
	$FromAddress = 'From: My Appy Business <server@myappybusiness.com>';
	$EmailSubject	= "Welcome to My Appy Business";
	$EmailMessage	= '
	<body style="margin: 0; padding: 0;">
	<table border="0" align="left" cellpadding="0" cellspacing="0" width="600px" style="font-size:10px;font-family:arial;">
		<tr>
		<td>Hi '.$FirstName.',</td>
		</tr>
			<tr style="height:10px"><td></td></tr>
		<tr>
		 <td>Welcome to My Appy Business - the fastest growing Mobile App solution for small business!</td>
		</tr>
		   <tr style="height:10px"><td></td></tr>
		<tr>
				<td>Our development team will start building your customized App as soon as you have provided us with your business logo and preferred names for your App. You can do that by simply clicking on this link and filling in the required information: <a href="'.$UploadLogoURL.'">click here</a> </td>
		</tr>
		  <tr style="height:10px"><td></td></tr>
		 <tr>
				<td>Once you have done this, you can expect your customized App to be live within 7-10 working days.We will send you a courtesy email confirming when it is live!</td>
		</tr>
		  <tr style="height:10px"><td></td></tr>
		  <tr>
				<td>If you have any questions relating to your service, please contact us at support@myappybusiness.com</td>
		</tr>
		  <tr style="height:10px"><td></td></tr>
		<tr>
				<td>In the meantime, please let me know if you need anything else.<br>Welcome to the App Revolution!</td>
		</tr>
		 
		  <tr style="height:5px"><td></td></tr>
		<tr><td>With Regards,</tr>
		<tr style="height:10px"><td></td></tr>
		<tr><td><img src="'.$LogoUrl.'"></td></tr>
		<tr style="height:15px"><td></td></tr>
		<tr>
			<td style="font-size:9px;font-family:arial;">This e-mail and any attachments are confidential and may contain legally privileged and/or copyright information of My Appy Business or third parties. If you are not an authorised recipient of this e-mail, please contact the sender immediately and do not print, re-transmit or store this email or any attachments. Any loss/damage incurred by using this material is not the sender\'s responsibility. No warranty is given that this e-mail or any attachments are totally free from computer viruses or other defects.</td>
		</tr>
	</table>
	</body>';
	SendEmailCommonfn($FromAddress, $ToAddress, $EmailSubject, $EmailMessage);
}
function SendEmailCommonfn($FromAddress, $ToAddress, $EmailSubject, $EmailMessage)
{
	
	$EmailHeaders   = array
    (
        'MIME-Version: 1.0',
        'Content-Type: text/html; charset="UTF-8";',
        'Content-Transfer-Encoding: 7bit',
        'Date: ' . date('r', $_SERVER['REQUEST_TIME']),
        'Message-ID: <' . $_SERVER['REQUEST_TIME'] . md5($_SERVER['REQUEST_TIME']) . '@' . $_SERVER['SERVER_NAME'] . '>',
		$FromAddress,
        'Reply-To: ' . $FromAddress,
        'Return-Path: ' . $FromAddress,
        'X-Mailer: PHP v' . phpversion(),
        'X-Originating-IP: ' . $_SERVER['SERVER_ADDR'],
    );
    mail($ToAddress, '=?UTF-8?B?' . base64_encode($EmailSubject) . '?=', $EmailMessage, implode("\n", $EmailHeaders));

}

function sendlogemail($ToAddress,$Subject,$comments,$LogoUrl){
	$FromAddress = 'From: My Appy Business <server@myappybusiness.com>';
	$EmailSubject	= $Subject;
	$EmailMessage	= '
	<body style="margin: 0; padding: 0;">
	<table border="0" align="left" cellpadding="0" cellspacing="0" width="600px" style="font-size:10px;font-family:arial;">
		<tr><td>'.strtoupper($Subject).'</td></tr>
		<tr style="height:5px"><td></td></tr>
		<tr><td >'.$comments.'</td></tr>
		<tr style="height:20px"><td></td></tr>
		<tr><td>With Regards,</tr>
		<tr style="height:10px"><td></td></tr>
		<tr><td><img src="'.$LogoUrl.'"></td></tr>
		<tr style="height:15px"><td></td></tr>
		<tr>
			<td style="font-size:9px;font-family:arial;">This e-mail and any attachments are confidential and may contain legally privileged and/or copyright information of My Appy Business or third parties. If you are not an authorised recipient of this e-mail, please contact the sender immediately and do not print, re-transmit or store this email or any attachments. Any loss/damage incurred by using this material is not the sender\'s responsibility. No warranty is given that this e-mail or any attachments are totally free from computer viruses or other defects.</td>
		</tr>
	</table>
	</body>';
  SendEmailCommonfn($FromAddress, $ToAddress, $EmailSubject, $EmailMessage);
}

?>