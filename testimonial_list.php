<?php
include("../includes/configure.php");
include("../includes/session_check.php");
$testimonial_id=$_GET["tesimonial_id"];
$sort=$_GET["sort"];
$field=$_GET["field"];
if($sort==""){
	$sort="asc";
}
if($field==""){
	$field="testimonial_id";
}
if($field=="name"){
	$fieldname="res_name";
	$order=" order by ".$fieldname." ".$sort;
	if($sort=="asc"){
		$dsort="desc";
		$dpath="images/up.png";
	}
	else{
		$dsort="asc";
		$dpath="images/down.png";
	}
}

if($field=="testimonial"){
	$fieldname="testimonial";
	$order=" order by ".$fieldname." ".$sort;
	if($sort=="asc"){
		$rsort="desc";
		$rpath="images/up.png";
	}
	else{
		$rsort="asc";
		$rpath="images/down.png";
	}
	
}
if($testimonial_id!=""){
    $getQry="select * from tbl_testimonial where testimonial_id='".$testimonial_id."'";
	$getRes=mysql_query($getQry);
	$getRow=mysql_fetch_array($getRes);
	$Logo=stripslashes($getRow["logo"]);
		if(file_exists($Logo)){
				unlink($Logo);
			}
	$deleteQry="delete from  tbl_testimonial  where testimonial_id='".$testimonial_id."'";
	$deleteRes=mysql_query($deleteQry);
	if($deleteRes){
		header("Location:testimonial_list.php");
		exit;
	}
}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<title>MAR Pipeline System</title>
		<link rel="shortcut icon" href="images/Fav.ico" type="image/ico">
		<meta name="Generator" content="EditPlus">
		<meta name="Author" content="">
		<meta name="Keywords" content="">
		<meta name="Description" content="">
		<style>
			body{
				margin:0;
				color:black;
				background:#455A68;
				font-family:arial;
			}
			.header{
				height:70px;
				background:#1C242A;
			}
			.content{
				background:#455A68;
				min-height:600px;
			}
			
			.form_actions{
				padding-top:15px;
				padding-left:5px;
				padding-bottom:30px;
			}
			.form_actions .add_btn{
				cursor:pointer;
				border-radius:0px;
				background:#0D0D0D;
				color:#D9D9D9;
				border-color:#D9D9D9;
				padding:5px 15px 5px 15px;
				font-family:arial;
			}
			.list_content{
				width:950px;
				margin-left:40px;
				/*margin-left:auto;
				margin-right:auto;*/
			}
			.tbl_header th{
				font-size:13px;
				border-bottom:1px solid #D9D9D9;
				text-align:left;
				font-family:arial;
				color:white;
			}
			.tbl-body{
				font-size:12px;
				line-height:25px;
				font-family:arial;
			}
			a{
				color:black;
			}
		</style>
	</head>
	<body>
		<div>
			<div style="margin-left:auto;margin-right:auto;">
				<div class="header">
					<span style="float:right;margin-right:20px;margin-top:5px;"><a href="logout.php" style="color:white;text-decoration:none;">Logout</a></span>
					<img src="images/myappyrestaurants.png" style="margin-top:10px;margin-left:40px;">
				</div>
				<div class="content">
					<div class="list_content">
						<div class="form_actions" style="padding-bottom:50px;">
							<input type="button" value="Back To Admin Features" class="add_btn" onclick="document.location='admin_features.php'" style="float:left;">
							<input type="button" value="Add Testimonial" class="add_btn" onclick="document.location='testimonial.php'" style="float:right;">
						</div>
						<div style="padding-bottom:12px;">
							<table cellspacing="0" cellpadding="0" width="100%" class="tbl_header">
								
								<tr>
									<th width="10%">No</th><!-- onclick="document.location='statuslist.php?sort=<?php echo $dsort;?>&field=sno'" -->
									<th width="20%" onclick="document.location='testimonial_list.php?sort=<?php echo $dsort;?>&field=name'" style="cursor:pointer">Restaurant Name&nbsp;&nbsp;<?php if($dpath!=""){?><img src="<?php echo $dpath;?>" style="width:10px;height:10px;"><?php }?></th>
									<!-- <th width="15%" onclick="document.location='testimonial_list.php?sort=<?php echo $rsort;?>&field=logo'" style="cursor:pointer">Logo&nbsp;&nbsp;<?php if($rpath!=""){?><img src="<?php echo $rpath;?>" style="width:10px;height:10px;"><?php }?></th> -->
									<th width="60%" onclick="document.location='testimonial_list.php?sort=<?php echo $rsort;?>&field=testimonial'" style="cursor:pointer">Testimonial&nbsp;&nbsp;<?php if($rpath!=""){?><img src="<?php echo $rpath;?>" style="width:10px;height:10px;"><?php }?></th>
									<th width="10%">Delete?</th>
								</tr>
							</table>
						</div>
						<table cellspacing="0" cellpadding="0" width="100%" class="tbl-body">
						<?php
							$getQry="select * from  tbl_testimonial".$order;
							//exit;
							$getRes=mysql_query($getQry);
							$getCnt=mysql_num_rows($getRes);
							if($getCnt>0){
								$i=1;
								while($getRow=mysql_fetch_array($getRes)){
									
									if($i%2==1){
										$bgcolor="#a5a5a5";
									}
									else{
										$bgcolor="#d2d1d1";
									}
									$bdot='';
									$testimonial=stripslashes($getRow["testimonial"]);
								if(strlen($testimonial)>70)
								$bdot="...";
						?>
							<tr bgcolor="<?php echo $bgcolor;?>">
								<td width="10%"><?php echo $i;?></td>
								<td width="20%"><?php echo stripslashes($getRow["res_name"]);?></td>
								<!-- <td width="15%"><?php echo"<img src=\"$path\" style=\"width:10px;height:10px;\">";?></td> -->
								<td width="60%"><?php echo substr(stripslashes($getRow["testimonial"]),0,70).$bdot;?></td>
								<td width="10%"><a href="testimonial.php?tesimonial_id=<?php echo $getRow["testimonial_id"];?>">Edit</a>&nbsp;&nbsp;/&nbsp;&nbsp;<a href="testimonial_list.php?tesimonial_id=<?php echo $getRow["testimonial_id"];?>" onclick="return confirm('Are you sure want to delete this testimonial?')">Delete</a></td>
							</tr>
							
						<?php
							$i++;
								}
								?>
							<tr><td height="10px"></td></tr>
							
								<?php
							}
							else{
								echo "<tr bgcolor='#a5a5a5'><td colspan=\"5\"><center>No testimonial(s) found.</center></td></tr>";
							}
						?>
						     <tr>
								<td colspan="3">
								<div class="form_actions" style="text-align:left;position:relative;" >
								<input type="button" value="Back To Admin Features" class="add_btn" onclick="document.location='admin_features.php'">
								</td>
							</tr>
						</table>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>