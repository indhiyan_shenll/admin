<?php
include("../includes/configure.php");
include("includes/cmm_functions.php");
$id=$_SESSION['user_id'];	
$feat="download_list";
$typeandfeature=checklogin($id,$feat);
$usrArr=explode("*",$typeandfeature);
$user_type=$usrArr[0];
$feature=$usrArr[1];
$trialformfeature=$usrArr[4];
$masterlistfeature=$usrArr[5];
//error_reporting(E_ALL);
include("../includes/session_check.php");
$special_id=$_GET["special_id"];
$product_page_val="0";
$style="display:none";
$created_user=$_SESSION['user_name'];
$created_user_id=$_SESSION['user_id'];
$dbdatetime=date('Y-m-d H:i:s',strtotime('now'));
if($special_id!=""){
        $get_special_qry="select * from tbl_special_one where special_one_id=:special_one_id";
		$prepget_special_qry=$DBCONN->prepare($get_special_qry);
		$prepget_special_qry->execute(array(":special_one_id"=>$special_id));
	    $get_special_Row=$prepget_special_qry->fetch();
		$special_id_unique=$get_special_Row['special_id'];
		$special_type_db=$get_special_Row['special_type'];
		$product_page_val=$get_special_Row['product_page'];
		$territory_val=$get_special_Row['territory'];
		$master_status=$get_special_Row['master_status'];
		$special_heading=$get_special_Row['special_heading'];
		$special_content=$get_special_Row['special_content'];
		$special_button1=$get_special_Row['special_button1'];
		$special_button2=$get_special_Row['special_button2'];
		$special_logo=$get_special_Row['logo'];
		$old_special_logo=$get_special_Row['logo'];
		$confirmation_messsage=$get_special_Row['confirmation_messsage'];
		$bottom_quote=$get_special_Row['bottom_quote'];
		$bottom_quote_person=$get_special_Row['bottom_quote_person'];
		$side_quote=stripslashes(trim($get_special_Row['side_quote']));
		$side_quote_person=stripslashes(trim($get_special_Row['side_quote_person']));
		$send_mail_val=$get_special_Row['send_mail'];
		$send_sms_val=$get_special_Row['send_sms'];
		$email_scheduled_time_val=$get_special_Row['email_scheduled_time'];
		$sms_scheduled_time=$get_special_Row['sms_scheduled_time'];
		$affiliate_id=$get_special_Row['a_id'];
		$simulator=$get_special_Row['simulator_feature'];
		$business_name=$get_special_Row['business_name'];
		$promo_code=$get_special_Row['promo_code'];
		$created_user_id=$get_special_Row['user_id'];
		$geUserQry="select * from  tbl_users where user_id=:user_id";
		$prepgetuserqry=$DBCONN->prepare($geUserQry);
		$prepgetuserqry->execute(array(":user_id"=>$created_user_id));
		//$geUserRes=mysql_query($geUserQry);
		$geUserRow=$prepgetuserqry->fetch();
		$created_user=stripslashes($geUserRow["username"]);
	    if($simulator=="1"){
		  $style="";
		}
		$business_theme=$get_special_Row['theme'];
		$business_type=$get_special_Row['business_type'];
		$mode="Edit";
		$value="Update";
}
else{
	$mode="Add";
	$value="Create";
	
}
if(isset($_POST["business_name"])){
	$heading=addslashes(trim($_POST["heading"]));
	$product_page=addslashes(trim($_POST["product_page"]));
    $business_type=addslashes(trim($_POST["business_type"]));
	$special_type=addslashes(trim($_POST["reg"]));
	$territory=addslashes(trim($_POST["territory"]));
	$Check_box_value=addslashes(trim($_POST["Add_mastrlist"]));
    if($Check_box_value==""){
		$Check_box_value="0";
    }
    $content=htmlspecialchars($_POST["content"]);
	$spl_bt1=addslashes(trim($_POST["spl_bt1"]));
	$spl_bt2=addslashes(trim($_POST["spl_bt2"]));
    if(!file_exists(SPECIAL_LOG_PATH)){
		mkdir(SPECIAL_LOG_PATH,0777);
	}
	$confirm_msg=htmlspecialchars($_POST["confirm_msg"]);
	$bottom_quote=$_POST["bottom_quote"];
	$bottom_quote_person=htmlspecialchars($_POST["bottom_quote_person"]);
	$side_quote=$_POST["side_quote"];
	$side_quote_person=htmlspecialchars($_POST["side_quote_person"]);
	$send_mail=addslashes(trim($_POST["send_mail"]));
    if($send_mail==""){
		$send_mail="0";
    }
	$send_sms=addslashes(trim($_POST["send_sms"]));
    if($send_sms==""){
		$send_sms="0";
    }
    $email_scheduled_time=$_POST["scheduled_time"];
	$sms_scheduled_time=$_POST["sms_scheduled_time"];
	$affiliate=$_POST["affiliate"];
	if(empty($_POST["simulator"])){
		 $simulator_value="0";
		 $theme="";
         $upload_logo_path="";


    }
	else{
	    $simulator_value=addslashes(trim($_POST["simulator"]));  
	    $theme=$_POST["theme"];
		$upload_special_logo=pathinfo($_FILES["special_logo"]["name"], PATHINFO_FILENAME);
			$upload_special_logo_extension=pathinfo($_FILES["special_logo"]["name"], PATHINFO_EXTENSION);
			if($upload_special_logo!=""){
			if($upload_special_logo!="$old_special_logo"){
				if(file_exists($old_special_logo)){
						unlink($old_special_logo);
				  }

			}
			$renamed_special_logo=time().".".$upload_special_logo_extension;
			$upload_logo_path=SPECIAL_LOG_PATH.$renamed_special_logo;
            move_uploaded_file($_FILES["special_logo"]["tmp_name"],$upload_logo_path);
			}
			else
			{
			   $upload_logo_path=$old_special_logo;
	        }
   	}
	 $promo_code=trim($_POST['promo_code']);
	 $business_name=trim($_POST["business_name"]);
	 $special_id_value = str_replace(' ', '-', $business_name); 
	 $special_id_value = preg_replace('/[^A-Za-z0-9\-]/', '', $special_id_value); 
	 $special_id_value = preg_replace('/-+/', '-', $special_id_value);
	 $special_id_value=strtolower($special_id_value);
	 
    if($mode=="Edit"){
		$updateQry="update tbl_special_one set special_heading=:heading,special_content=:special_content,special_button1=:special_button1,special_button2=:special_button2,logo=:logo,modified_date=:modified_date,confirmation_messsage=:confirmation_messsage,bottom_quote=:bottom_quote,bottom_quote_person=:bottom_quote_person,side_quote=:side_quote,side_quote_person=:side_quote_person,special_id=:caterer,master_status=:master_status,product_page=:product_page,territory=:territory,send_mail=:send_mail,send_sms=:send_sms,email_scheduled_time=:scheduled_time,sms_scheduled_time=:sms_scheduled_time,special_type=:special_type,a_id=:affiliate_id,simulator_feature=:simulator_feature,theme=:theme,business_name=:business_name,business_type=:business_type,promo_code=:promo_code where special_one_id=:special_one_id";
		$prepupdateQry=$DBCONN->prepare($updateQry);
		$updateRes=$prepupdateQry->execute(array(":heading"=>$heading,":special_content"=>$content,":special_button1"=>$spl_bt1,":special_button2"=>$spl_bt2,":logo"=>$upload_logo_path,":modified_date"=>$dbdatetime,":confirmation_messsage"=>$confirm_msg,":bottom_quote"=>$bottom_quote,":bottom_quote_person"=>$bottom_quote_person,":side_quote"=>$side_quote,":side_quote_person"=>$side_quote_person,":caterer"=>$special_id_value,":special_one_id"=>$special_id,":product_page"=>$product_page,":master_status"=>$Check_box_value,":territory"=>$territory,":send_mail"=>$send_mail,":send_sms"=>$send_sms,":scheduled_time"=>$email_scheduled_time,":sms_scheduled_time"=>$sms_scheduled_time,":special_type"=>$special_type,":affiliate_id"=>$affiliate,":simulator_feature"=>$simulator_value,":theme"=>$theme,":business_name"=>$business_name,":business_type"=>$business_type,":promo_code"=>$promo_code));
		if($updateRes){
	        header("Location:special_one_list.php");
			exit;
		}
	}
	else{
		 
		$get_order="select max(display_order) as display_order from  tbl_special_one";
		$prepget_get_order=$DBCONN->prepare($get_order);
		$prepget_get_order->execute();
		$order_count =$prepget_get_order->rowCount();
		$get_display_Row=$prepget_get_order->fetch();
		$display_order=$get_display_Row['display_order'];
		$order_db_insert=$display_order+1;
		$insertqry="insert into tbl_special_one(special_heading,special_content,special_button1,special_button2,logo,display_order,created_date,modified_date,confirmation_messsage,bottom_quote,bottom_quote_person,side_quote,side_quote_person,special_id,	master_status,product_page,territory,send_mail,send_sms,email_scheduled_time,sms_scheduled_time,special_type,a_id,simulator_feature,theme,business_name,promo_code,user_id,business_type)values(:heading,:special_content,:special_button1,:special_button2,:logo,:dispaly_order,:created_date,:modified_date,:confirmation_messsage,:bottom_quote,:bottom_quote_person,:side_quote,:side_quote_person,:caterer,:master_status,:product_page,:territory,:send_mail,:send_sms,:scheduled_time,:sms_scheduled_time,:special_type,:affiliate_id,:simulator_feature,:theme,:business_name,:promo_code,:user_id,:business_type)";
	    $prepinsertqry=$DBCONN->prepare($insertqry);
	    $insertRes=$prepinsertqry->execute(array(":heading"=>$heading,":special_content"=>$content,":special_button1"=>$spl_bt1,":special_button2"=>$spl_bt2,":logo"=>$upload_logo_path,":dispaly_order"=>$order_db_insert,":created_date"=>$dbdatetime,":modified_date"=>$dbdatetime,":confirmation_messsage"=>$confirm_msg,":bottom_quote"=>$bottom_quote,":bottom_quote_person"=>$bottom_quote_person,":side_quote"=>$side_quote,":side_quote_person"=>$side_quote_person,":caterer"=>$special_id_value,":master_status"=>$Check_box_value,":product_page"=>$product_page,":territory"=>$territory,":send_mail"=>$send_mail,":send_sms"=>$send_sms,":scheduled_time"=>$email_scheduled_time,":sms_scheduled_time"=>$sms_scheduled_time,":special_type"=>$special_type,":affiliate_id"=>$affiliate,":simulator_feature"=>$simulator_value,":theme"=>$theme,":business_name"=>$business_name,":promo_code"=>$promo_code,":user_id"=>$_SESSION['user_id'],":business_type"=>$business_type));
	    if($insertRes){
			header("Location:special_one_list.php");
			exit;
		}

  }

}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<title>MAR Pipeline System</title>
		<link rel="shortcut icon" href="images/Fav.ico" type="image/ico">
		<meta name="Generator" content="EditPlus">
		<meta name="Author" content="">
		<meta name="Keywords" content="">
		<meta name="Description" content="">
		<script type="text/javascript" src="js/jquery.js"></script>
		<script>
		var mode="<?php echo $mode;?>";
		</script>
		<script type="text/javascript" src="js/validate.js"></script>
		<script type="text/javascript" src="js/tiny_mce.js"></script>
		<script type="text/javascript">
		function setcode(val) {
			res = val.replace(/([~!@#$%^&*()_+=`{}\[\]\|\\:;'<>,.\/? ])+/g, '').replace(/^(-)+|(-)+$/g,'');
			var res = res.substring(0,4);
			var res = res.toUpperCase();
			var chars = "0123456789";
			var string_length = 3;
			var randomstring = '';
			for (var i=0; i<string_length; i++) {
				var rnum = Math.floor(Math.random() * chars.length);
				randomstring += chars.substring(rnum,rnum+1);
				
			}
			 var extra="0";
		     var promocode=res+randomstring+extra;
			 var promocode_id=$("#hdn_special_id").val();
		     if(promocode_id==""){
			    promocode_id=0;
		     }
			 $.ajax({url:"check_promo.php?name="+promocode+"&id="+promocode_id,success:function(result){
			 if(parseInt(result)>0){
               var business_val=$("#business_name").val();
				  setcode(business_val) ;    
			}
			else{
				
				$('#promo_code').val(promocode);
			  }
			  }});
			}
		tinyMCE.init({
					// General options
					mode : "textareas",
					theme : "advanced",
						 
					plugins : "autolink,lists,spellchecker,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,tabfocus",

					// Theme options
					theme_advanced_buttons1 : "save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,styleselect,formatselect,fontselect,fontsizeselect",
					theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,|,insertdate,inserttime,preview,|,forecolor,backcolor",
					theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr,|,print,|,ltr,rtl,|,fullscreen",
					theme_advanced_buttons4 : "insertlayer,moveforward,movebackward,absolute,|,styleprops,spellchecker,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,blockquote,pagebreak,|,insertfile,insertimage",
					theme_advanced_toolbar_location : "top",
					theme_advanced_toolbar_align : "left",
					theme_advanced_statusbar_location : "bottom",
					theme_advanced_resizing : true,

					// Skin options
					skin : "o2k7",
					skin_variant : "silver",

					// Example content CSS (should be your site CSS)
					content_css : "css/example.css",

					// Drop lists for link/image/media/template dialogs
					template_external_list_url : "js/template_list.js",
					external_link_list_url : "js/link_list.js",
					external_image_list_url : "js/image_list.js",
					media_external_list_url : "js/media_list.js",

					// Replace values for the template plugin
					template_replace_values : {
							username : "Some User",
							staffid : "991234"
					}
					 
			});
		</script>
		<style>
			body{
				margin:0;
				color:#D9D9D9;
				background:#455A68;
				font-family:arial;
			}
			.header{
				height:70px;
				background:#1C242A;
			}
			.content{
				background:#455A68;
				min-height:600px;
			}
			
			.form_actions{
				padding-top:15px;
				padding-left:5px;
				padding-bottom:30px;
			}
			.form_actions .add_btn{
				cursor:pointer;
				border-radius:0px;
				background:#0D0D0D;
				color:#D9D9D9;
				border-color:#D9D9D9;
				padding:5px 15px 5px 15px;
				font-family:arial;

			}
			.list_content{
				width:950px;
				margin-left:40px;
				/*margin-left:auto;
				margin-right:auto;*/

			}
			.tbl_header th{
				font-size:13px;
				border-bottom:1px solid #D9D9D9;
				text-align:left;
				font-family:arial;
			}
			.tbl-body{
				font-size:12px;
				font-family:arial;
			}
			a{
				color:black;
			}
			.inp_feild{
				border-radius:2px;
				border:none;
				width:50%;
			}
		</style>
	</head>
	<body>
		<div>
			<div style="margin-left:auto;margin-right:auto;">
				<div class="header">
					<span style="float:right;margin-right:20px;margin-top:5px;"><a href="logout.php" style="color:white;text-decoration:none;">Logout</a></span>
					<img src="images/myappyrestaurants.png" style="margin-top:10px;margin-left:40px;">
				</div>
				<div class="content">
					<div class="list_content">
						<h1 style="font-size:25px;padding-top:15px;padding-bottom:15px;margin:0px;">Create Simulator</h1>
						<form name="special_form" id="special_form" method="post" enctype="multipart/form-data">
						<input type="hidden" name="hdn_special_id" id="hdn_special_id" value="<?php echo $special_id;?>">
						<input type="hidden" name="hdn_user_id" id="hdn_user_id" value="<?php echo $special_id;?>">
						<input type="hidden" name="hdn_img" id="hdn_img" value="<?php echo $old_special_logo;?>">
						<table cellspacing="15" cellpadding="0" border="0" width="70%">
						    <tr style="display:none;">
								<td style="width:138px;" valign="top">
									Special Id<font color="red">*</font>:
								</td>
								<td>
									<input type="text" name="special_id_value" id="special_id_value" class="inp_feild" value="<?php echo $special_id_unique;?>" style="width:100%">
									<span style="display:none;"><input type="checkbox" name="Add_mastrlist" value="1" <?php if($master_status=="1"){echo "checked";}?>>Automatically add records to master list</span>
								</td>
							</tr>
							  <tr style="display:none;">
								<td style="width:138px;" valign="top">
									Rego/Activation:
								</td>
								<td>
									<select name="reg" id="reg" class="inp_feild">
										<option value="">Select</option>
										<option value="registration" <?php if($special_type_db=='registration') echo " selected";?>>Registration only</option>
										<option value="activation" <?php if($special_type_db=='activation') echo " selected";?>>Registration & Activation</option>
										
									</select>
								</td>
							</tr>
							<tr style="display:none;">
								<td style="width:138px;" valign="top">
									Territory:<font color="red">*</font>:
								</td>
								<td>
									<select name="territory" id="territory" class="inp_feild">
										<option value="0">Select</option>
										<?php
											$getcountrQry="select * from  tbl_country";
											$getcountryRes=mysql_query($getcountrQry);
											
											while($getcountryRow=mysql_fetch_array($getcountryRes)){
										   ?>
											<option value="<?php echo stripslashes($getcountryRow["country"]);?>"<?php if($territory_val==$getcountryRow["country"]) echo " selected";?>><?php echo stripslashes($getcountryRow["country"]);?></option>
										<?php
											}
										?>
									</select>
								</td>
							</tr>
							

							<tr style="display:none;">
								<td style="width:138px;">
									Special Heading<font color="red">*</font>:
								</td>
								<td>
									<input type="text" name="heading" id="heading" class="inp_feild" value="<?php echo $special_heading;?>">
								</td>
							</tr>
							<tr style="display:none;">
								<td style="width:138px;" valign="top">
									Special Content<font color="red">*</font>:
								</td>
								<td>
									<textarea name="content" id="content" class="inp_feild" style="height:100px;font-family: arial;font-size:13px;"><?php echo $special_content; ?></textarea>
								</td>
							</tr>
							<tr style="display:none;">
								<td style="width:138px;">
									Special Button 1<font color="red">*</font>:
								</td>
								<td>
									<input type="text" name="spl_bt1" id="spl_bt1" class="inp_feild" value="<?php echo $special_button1;?>">
								</td>
							</tr>
							<tr style="display:none;">
								<td style="width:138px;">
									Special Button 2<font color="red">*</font>:
								</td>
								<td>
									<input type="text" name="spl_bt2" id="spl_bt2" class="inp_feild" value="<?php echo $special_button2;?>">
								</td>
							</tr>
							<tr>
								<td style="width:138px;">
									Business Name<font color="red">*</font>:
								</td>
								<td>
									<input type="text" name="business_name" id="business_name" class="inp_feild" value="<?php echo stripslashes($business_name);?>" onkeyup="setcode(this.value)"  >
								</td>
							</tr>
							<tr>
								<td style="width:138px;" valign="top"> 
									Business Type<font color="red">*</font>:
								</td>
								<td>
									   <select name="business_type"  id="business_type" class="inp_feild">
									    <option value=" ">Select</option>
										<?php
										$getbusinessQry="select * from  tbl_business_type order by display_order asc";
										$prepget_business_qry=$DBCONN->prepare($getbusinessQry);
										$prepget_business_qry->execute();
										while($getbusinessRow=$prepget_business_qry->fetch()){
										?>
										<option value="<?php echo $getbusinessRow["business_type"];?>" <?php if($business_type==$getbusinessRow["business_type"]){ echo "selected";}?>><?php echo $getbusinessRow["business_type"];?></option>
										<?php
										}
										?>
									</select>
								 						 
								</td>
							</tr>
							
							<tr>
								<td style="width:138px;" valign="top">
									Product Page<font color="red">*</font>:
								</td>
								<td>
									<select name="product_page" id="product_page" class="inp_feild">
									   
										<?php
										if($_SESSION['user_id']=="3" || $user_type=="Admin"){
												$getQry_1="SELECT  DISTINCT `product_page` FROM `tbl_apps`  order by `product_page` asc";
												$prepgetQry1=$DBCONN->prepare($getQry_1);
												$prepgetQry1->execute();
												while($getRow1=$prepgetQry1->fetch()){
												   $y=$getRow1["product_page"];
												   echo "<option value='".$y."'>".$y."</option>";	
												}

								        }
										else{
												$getQry_1="SELECT * FROM  tbl_affiliate where a_user_id='".$_SESSION['user_id']."'";
												$prepgetQry1=$DBCONN->prepare($getQry_1);
												$prepgetQry1->execute();
												while($getRow1=$prepgetQry1->fetch()){
												   $x=$getRow1["product_page1"];
												   $y=$getRow1["product_page2"];
												   $z=$getRow1["product_page3"];
												   if(!empty($x) && $x!="0"){
												      echo "<option value='".$x."'>".$x."</option>";
												   }
												    if(!empty($y)&& $y!="0"){
												      echo "<option value='".$y."'>".$y."</option>";
												   }
												     if(!empty($z) && $z!="0"){
												      echo "<option value='".$z."'>".$z."</option>";
												   }


												}
										}
										
										?>
										
									</select>
									<?php
									if($product_page_val!="" &&$product_page_val!="0" ){
									?>
									<script language="javascript">document.getElementById("product_page").value="<?php echo $product_page_val;?>"</script>
									<?php
									}
									?>
										
								</td>
							</tr>
							<!-- <tr>
								<td style="width:138px;" valign="top">
									Product Page<font color="red">*</font>:
								</td>
								<td>
									<select name="product_page" id="product_page" class="inp_feild">
									    <option value="0">Select</option>
										<?php
										
										for($y=1;$y<=9999;$y++)
										{
										  echo "<option value='".$y."'>".$y."</option>";	
										}
										?>
										
									</select>
									<script language="javascript">document.getElementById("product_page").value="<?php echo $product_page_val;?>"</script>
										
								</td>
							</tr> -->
							<tr>
								<td style="width:117px;">
									 Promo Code<font color="red">*</font>:
								</td>
								<td>
									<input type="text" name="promo_code" id="promo_code" class="inp_feild" value="<?php echo $promo_code;?>" style="width:50%">
								</td>
							</tr>
							<tr>
								<td style="width:138px;">
									Simulator Creator:
								</td>
								<td>
									 <input type="text" name="user_id" id="user_id" class="inp_feild" value="<?php echo stripslashes($created_user);?>"  readonly>
								</td>
							</tr>
							<tr style="display:none;">
								<td style="width:138px;" valign="top">
									Confirmation Message<font color="red">*</font>:
								</td>
								<td>
									<textarea name="confirm_msg" id="confirm_msg" class="inp_feild" style="height:100px;font-family: arial;font-size:13px;"><?php echo $confirmation_messsage; ?></textarea>
								</td>
							</tr>
							<tr style="display:none;">
								<td style="width:138px;" valign="top">
									Bottom Quote<font color="red">*</font>:
								</td>
								<td>
									<textarea name="bottom_quote" id="bottom_quote" class="inp_feild " style="height:100px;font-family: arial;font-size:13px;"><?php echo $bottom_quote; ?></textarea>
								</td>
							</tr>
							
							<tr style="display:none;">
								<td style="width:138px;" valign="top">
									Side Quote<font color="red">*</font>:
								</td>
								<td>
									<textarea name="side_quote" id="side_quote" class="inp_feild " style="height:100px;font-family: arial;font-size:13px;"><?php echo $side_quote; ?></textarea>
								</td>
							</tr>
							<tr style="display:none;">
								<td style="width:138px;" valign="top">
									
								</td>
								<td>
									<input type="checkbox" name="send_mail" value="1" <?php if($send_mail_val=="1"){echo "checked";}?>> Auto-send email <br>
									<input type="checkbox" name="send_sms" value="1" <?php if($send_sms_val=="1"){echo "checked";}?>>Auto-send sms
								</td>
							</tr>
							<tr style="display:none;">
								<td style="width:138px;" valign="top">
									Email Scheduled Time:
								</td>
								<td>
									<select name="scheduled_time" id="scheduled_time" class="inp_feild">
									    
										<option value="0">0 Minutes </option>
										<option value="5">5 Minutes </option>
										<option value="15">15 Minutes </option>
										<option value="30">30 Minutes </option>
										<option value="45">45 Minutes </option>
										<option value="60">1 Hour </option>
										<?php
										for($y=02;$y<=24;$y++)
										{
											$z=$y*60;
										  echo "<option value='".$z."'>".$y." Hours</option>";	
										}
										?>										
									</select>
									<?php 
										 if($email_scheduled_time_val!=""){
											?>
									<script language="javascript">document.getElementById("scheduled_time").value="<?php echo $email_scheduled_time_val;?>"</script>
										<?php 
										  }
										?>
									
								</td>
							</tr>
							<tr style="display:none;">
								<td style="width:138px;" valign="top">
									SMS Scheduled Time:
								</td>
								<td>
									<select name="sms_scheduled_time" id="sms_scheduled_time" class="inp_feild">
									   
										<option value="0">0 Minutes </option>
										<option value="5">5 Minutes </option>
										<option value="15">15 Minutes </option>
										<option value="30">30 Minutes </option>
										<option value="45">45 Minutes </option>
										<option value="60">1 Hour </option>
										<?php
										for($y=02;$y<=24;$y++)
										{
											$z=$y*60;
										  echo "<option value='".$z."'>".$y." Hours</option>";	
										}
										?>										
									</select>
									<?php 
										 if($sms_scheduled_time!=""){
											?>
									<script language="javascript">document.getElementById("sms_scheduled_time").value="<?php echo $sms_scheduled_time;?>"</script>
										<?php 
										  }
										?>
									
								</td>
							</tr>
							<tr style="display:none;">
								<td style="width:138px;" valign="top">
									Affiliate:
								</td>
								<td>
									<select name="affiliate" id="affiliate" class="inp_feild">
										<option value="0">Select</option>
										<?php
											$get_Qry="select * from  tbl_affiliate";
											$get_Res=mysql_query($get_Qry);
											
											while($get_value=mysql_fetch_array($get_Res)){
										   ?>
											<option value="<?php echo stripslashes($get_value["affiliate_id"]);?>"<?php if($affiliate_id==$get_value["affiliate_id"]) echo " selected";?>><?php echo stripslashes($get_value["full_name"]);?></option>
										<?php
											}
										?>
									</select>
								</td>
							</tr>
							<tr>
							<td style="width:138px;" valign="top">
									 
								</td>
								<td>
									<input type="checkbox" name="simulator"   id="simulator"  value="1" <?php if($simulator=="1"){echo "checked";}?> onclick="show(this.id)"> Add Simulator <br>
								</td>
							</tr>

							
							<tr class="theme_div" style="margin-top:5px;<?php echo $style;?>">
								 
								<td style="width:138px;" valign="top">
									Business Logo<font color="red">*</font>:
								</td>
								<td>
									<input type="file" name="special_logo" id="special_logo" class="inp_feild">
									<?php if($special_logo!=""){ ?>
									
										<br><br><img src="<?php echo $special_logo;?>" style="width:75px;height:75px;">
									<?php }?>
								</td>
							</tr>
							<tr class="theme_div" style="margin-top:5px;<?php echo $style;?>">
								<td style="width:138px;" valign="top"> 
									Design Style<font color="red">*</font>: 
								</td>
								<td>
									
									 
										<select name="theme" id="theme" class="inp_feild">
											<option value=" ">Select</option>
											<?php
											foreach($themes_array as $val=>$name){ 
											  echo '<option value="'.$val.'">'.$name.'</option>'; 
											} 
											?>
											 
										</select>
										<?php 
										 if($business_theme!="" && $business_theme>0){
											?>
										<script type="text/javascript">
										   $("#theme").val('<?php echo $business_theme;?>')
										</script>
										<?php 
										  }
										?>
								 						 
								</td>
							</tr>
							
							<tr >
							     <td>
									<div class="form_actions" style="text-align:left;">
										<input type="button" value="Back to Simulator List" class="add_btn"  onclick="document.location='special_one_list.php'">
									
								</td>
								<td>
									  <div class="form_actions" style="text-align:right;">
										<input type="button" value="<?php echo $value;?> Simulator" class="add_btn" id="add_special_one">
									</div>
								</td>
							</tr>
						</table>
						</form>
					</div>
					
				</div>
			</div>
		</div>
	</body>
</html>
<script type="text/javascript">
 function show(id){
      if($("#"+id).is(':checked'))
           $(".theme_div").toggle();
        else
           $(".theme_div").toggle();
    

}
 
 
</script>