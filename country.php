<?php
include("../includes/configure.php");
include("../includes/session_check.php");
$country_id=$_GET["country_id"];
$dbdatetime=date('Y-m-d H:i:s',strtotime('now'));

if($country_id!=""){
	//$country_id
	$getQry="select * from   tbl_country where country_id=:country_id";
	$prepgetQry=$DBCONN->prepare($getQry);
	$prepgetQry->execute(array(":country_id"=>$country_id));
	$getRow=$prepgetQry->fetch();
	//$getRes=mysql_query($getQry);
	//$getRow=mysql_fetch_array($getRes);
	$country=stripslashes($getRow["country"]);
	$mode="Edit";
	$value="Update";
}
else{
	$mode="Add";
	$value="Create";
}
if(isset($_POST["country"])){
	$Country=addslashes(trim($_POST["country"]));
    if($mode=="Edit"){
		//$Country    $country_id
		$updateQry="update  tbl_country set country=:country,modified_date=:modified_date where country_id=:countryid";
		$prepupdateQry=$DBCONN->prepare($updateQry);
		$updateRes=$prepupdateQry->execute(array(":country"=>$Country,":modified_date"=>$dbdatetime,":countryid"=>$country_id));
		//$updateRes=mysql_query($updateQry);
		if($updateRes){
			header("Location:country_list.php");
			exit;
		}
	}
	else{
		//$Country 
		$insertQry="insert into tbl_country(country,added_date,modified_date) values(:country,:added_date,:modified_date)";
		//$insertRes=mysql_query($insertQry);
		$prepinsqry=$DBCONN->prepare($insertQry);
		$insertRes=$prepinsqry->execute(array(":country"=>$Country,":added_date"=>$dbdatetime,":modified_date"=>$dbdatetime));
		if($insertRes){
			header("Location:country_list.php");
			exit;
		}
	}
}
include("includes/header.php");
?>
<body>
		<div>
			<div style="margin-left:auto;margin-right:auto;">
				<div class="content">
					<div class="list_content">
						<h1 style="font-size:25px;padding-top:15px;padding-bottom:15px;margin:0px;">New 
						Country </h1>
						<form name="country_form" id="country_form" method="post" enctype="multipart/form-data">
						<input type="hidden" name="country_form_id" id="country_form_id" value="<?php echo $country_id;?>">
						<table cellspacing="15" cellpadding="0" border="0" width="70%">
							<tr>
								<td style="width:138px;">
									Country<font color="red">*</font>:

								</td>
								<td>
									<input type="text" name="country" id="country" class="inp_feild" value="<?php echo $country;?>">
								</td>
							</tr>
							
							<tr>
							     <td>
									<div class="form_actions" style="text-align:left;">
										<input type="button" value="Back To Country List" class="add_btn"  onclick="document.location='country_list.php'">
									
								</td>
								<td>
									  <div class="form_actions" style="text-align:right;">
										<input type="button" value="<?php echo $value;?> Country" class="add_btn" id="add_country">
									</div>
								</td>
							</tr>
						</table>
						</form>
					</div>
					
				</div>
			</div>
		</div>
<?php
include("includes/footer.php");
?>