<?php
include("../includes/configure.php");
include("../includes/session_check.php");
$dbdatetime=date('Y-m-d H:i:s',strtotime('now'));
$status_id=$_GET["status_id"];
if($status_id!=""){
	$getQry="select * from tbl_affiliate_status where affiliate_status_id=:affiliate_status_id";
	$prepgetQry=$DBCONN->prepare($getQry);
	$prepgetQry->execute(array(":affiliate_status_id"=>$status_id));
	$getRow=$prepgetQry->fetch();
	$callstatus=stripslashes($getRow["callstatus"]);
	$Callstatus_color=stripslashes($getRow["status_color"]);
	$mode="Edit";
	$value="Update";
}
else{
	$mode="Add";
	$value="Create";
}
if(isset($_POST["callstatus"])){
	$callstatus=addslashes(trim($_POST["callstatus"]));
	$call_satuscolor=addslashes(trim($_POST["call_stauts_color"]));
	if($mode=="Edit"){
		$updateQry="update tbl_affiliate_status set callstatus=:callstatus,status_color=:status_color,modified_date=:modified_date where affiliate_status_id=:affiliate_status_id";
		$prepupdateQry=$DBCONN->prepare($updateQry);
		$updateRes=$prepupdateQry->execute(array(":callstatus"=>$callstatus,":status_color"=>$call_satuscolor,":modified_date"=>$dbdatetime,":affiliate_status_id"=>
			$status_id));
		if($updateRes){
			header("Location:affiliate_status_list.php");
			exit;
		}
	}
	else{
		 $get_order="select max(display_order) as display_order from  tbl_affiliate_status";
		$prepget_get_order=$DBCONN->prepare($get_order);
		$prepget_get_order->execute();
		$order_count =$prepget_get_order->rowCount();
		$get_display_Row=$prepget_get_order->fetch();
		$display_order=$get_display_Row['display_order'];
		$order_db_insert=$display_order+1;
		$insertQry="insert into tbl_affiliate_status(callstatus,status_color,display_order,added_date,modified_date) values(:callstatus,:status_color,:display_order,:added_date,:modified_date)";
		$prepisnertQry=$DBCONN->prepare($insertQry);
		$insertRes=$prepisnertQry->execute(array(":callstatus"=>$callstatus,":status_color"=>$call_satuscolor,":display_order"=>$order_db_insert,":added_date"=>$dbdatetime,":modified_date"=>$dbdatetime));
		 if($insertRes){
			header("Location:affiliate_status_list.php");
			exit;
		}
	}
}
include("includes/header.php");
?>
 <body>
		<div>
			<div style="margin-left:auto;margin-right:auto;">
				 
				<div class="content">
					<div class="list_content">
						<h1 style="font-size:25px;padding-top:15px;padding-bottom:15px;margin:0px;">New Affiliate Status</h1>
						<form name="affiliate_status_form" id="affiliate_status_form" method="post">
						<input type="hidden" name="callstatus_form_id" id="callstatus_form_id" value="<?php echo $status_id;?>">
						<table cellspacing="15" cellpadding="0" border="0" width="70%">
							<tr>
								<td style="width:138px;">
									 Affiliate Status<font color="red">*</font>:
								</td>
								<td>
									<input type="text" name="callstatus" id="callstatus" class="inp_feild" value="<?php echo $callstatus;?>">
								</td>
							</tr>
							<tr>
								<td style="width:138px;">
									  Affiliate Status Color<font color="red">*</font>: 
								</td>
								<td>
									<input type="text" name="call_stauts_color" id="call_stauts_color" class="inp_feild" value="<?php echo $Callstatus_color;?>">
									<SCRIPT LANGUAGE="JavaScript">
									$('#call_stauts_color').ColorPicker({
									eventName:'focus',
									 onSubmit: function(hsb, hex, rgb, el) {
									  $(el).val(hex);
									  $(el).ColorPickerHide();
									 },
									  onBeforeShow: function () {
									   $(this).ColorPickerSetColor(this.value);
									  }
									 })
									 .bind('keyup', function(){
									  $(this).ColorPickerSetColor(this.value);
									 });
									</SCRIPT>
								</td>
							</tr>
							<tr>
							     <td>
									<div class="form_actions" style="text-align:left;">
										<input type="button" value="Back to Affiliate Status List" class="add_btn"  onclick="document.location='affiliate_status_list.php'">
									
								</td>
								<td>
									  <div class="form_actions" style="text-align:right;">
										<input type="button" value="<?php echo $value;?> Affiliate Status" class="add_btn" id="add_affiliate_status">
									</div>
								</td>
							</tr>
						</table>
						</form>
					</div>
					
				</div>
			</div>
		</div>
	<?php
include("includes/footer.php");
?>