<?php
include("../includes/configure.php");
include("../includes/session_check.php");
$business_id=$_GET["id"];
if($business_id!=""){
	$deleteQry="delete from tbl_email where id=:id";
	$prepdeleteQry=$DBCONN->prepare($deleteQry);
	$deleteRes=$prepdeleteQry->execute(array(":id"=>$business_id));
	if($deleteRes){
		header("Location:email_list.php");
		exit;
	}
}
 
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<title>MAR Pipeline System</title>
		<link rel="shortcut icon" href="images/Fav.ico" type="image/ico">
		<meta name="Generator" content="EditPlus">
		<meta name="Author" content="">
		<meta name="Keywords" content="">
		<meta name="Description" content="">
		<style>
			body{
				margin:0;
				color:black;
				background:#455A68;
				font-family:arial;
			}
			.header{
				height:70px;
				background:#1C242A;
			}
			.content{
				background:#455A68;
				min-height:600px;
			}
			
			.form_actions{
				padding-top:15px;
				padding-left:5px;
				padding-bottom:30px;
			}
			.form_actions .add_btn{
				cursor:pointer;
				border-radius:0px;
				background:#0D0D0D;
				color:#D9D9D9;
				border-color:#D9D9D9;
				padding:5px 15px 5px 15px;
				font-family:arial;
			}
			.list_content{
				width:950px;
				margin-left:40px;
				/*
				margin-left:auto;
				margin-right:auto;
				*/
			}
			.tbl_header th{
				font-size:13px;
				border-bottom:1px solid #D9D9D9;
				text-align:left;
				font-family:arial;
				color:white;
			}
			.tbl-body{
				font-size:12px;
				line-height:25px;
				font-family:arial;
			}
			a{
				color:black;
			}
		</style>
	</head>
	<body>
		<div>
			<div style="margin-left:auto;margin-right:auto;">
				<div class="header">
					<span style="float:right;margin-right:20px;margin-top:5px;"><a href="logout.php" style="color:white;text-decoration:none;">Logout</a></span>
					<img src="images/myappyrestaurants.png" style="margin-top:10px;margin-left:40px;">
				</div>
				<div class="content">
					<div class="list_content">
						<div class="form_actions" style="padding-bottom:45px;">
							<input type="button" value="Back To Admin Features" class="add_btn" onclick="document.location='admin_features.php'" style="float:left;">
							<input type="button" value="Add Business" class="add_btn" onclick="document.location='edit_business.php'" style="float:right;">
						</div>
						<div style="padding-bottom:12px;">
							<table cellspacing="0" cellpadding="0" width="100%" class="tbl_header" border="0">
							  <tr>
									
									<th width="5%">No</th>
									<th width="20%"  style="cursor:pointer">Email&nbsp;&nbsp;</th>
									<th width="15%"  style="cursor:pointer">Country</th>
								   <th width="10%">Delete?</th>
								
							 </tr>
							</table>
						</div>
						<table cellspacing="0" cellpadding="0" width="100%" class="tbl-body">
						<?php
                         $get_business_qry="select * from tbl_email order by id desc";
						 $prepget_business_qry=$DBCONN->prepare($get_business_qry);
						 $prepget_business_qry->execute();
                         $getCnt =$prepget_business_qry->rowCount();
					    if($getCnt>0){
								$i=1;
								while($getRow=$prepget_business_qry->fetch()){
									//$id = $getRow['special_one_id'];
									//$display_order=stripslashes($getRow['display_order']);
									if($i%2==1){
										$bgcolor="#a5a5a5";
									}
									else{
										$bgcolor="#d2d1d1";
									}
                                     
						?>
							<tr bgcolor="<?php echo $bgcolor;?>">
							   	   <td width="5%"><?php echo $i;?></td>
									<td width="20%"   ><?php echo stripslashes($getRow["email_address"]);?></td>	
									<td width="20%" style="cursor:pointer"><?php echo stripslashes($getRow["country"]);?></td> 
									<td width="10%">&nbsp;<a href="mail_list.php?id=<?php echo $getRow["id"];?>"  onclick="return confirm('Are you sure want to delete this record? ')">Delete</a></td>
							</tr>
							
						<?php
							$i++;
								}
								?>
							<tr><td height="10px"></td></tr>
							
								<?php
							}
							else{
								echo "<tr bgcolor='#a5a5a5'><td colspan=\"3\"><center>No Record(s) found.</center></td></tr>";
							}
						?>
						<tr>
								<td colspan="4">
								<div class="form_actions" style="text-align:left;position:relative;">
								<input type="button" value="Back To Admin Features" class="add_btn" onclick="document.location='admin_features.php'">
								</td>
							</tr>
						</table>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>
